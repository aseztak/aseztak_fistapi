# app
fastapi==0.78.0
uvicorn==0.16.0
gunicorn
loguru==0.5.3
requests
aiofiles

#rate Limiter
ratelimit
Pillow
python-multipart
boto3
#db
sqlalchemy==1.4.25
pymysql

# auth
pyjwt==1.7.1

#web
jinja2==3.0.2
aiofile==3.7.2


passlib


pycryptodome

python-jose[cryptography]
cryptography

#PAYTM
paytmchecksum

pandas

openpyxl


#PDF GENERATE
pdfkit

#Fastapi Debugger
fastapi-debug-toolbar

slowapi