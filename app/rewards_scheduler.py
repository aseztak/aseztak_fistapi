from datetime import datetime, timedelta
from urllib.parse import quote
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker
# from sqlalchemy.ext.declarative import as_declarative
from sqlalchemy import Column, Integer, String, TIMESTAMP, ForeignKey, func, case, Float, DATETIME, Enum, Date
# import requests
# import json
import pymysql
from sqlalchemy.sql.elements import Null
# import asyncio
from typing import Optional
import enum
from datetime import date
import itertools
import math
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import Session

REWARDS_PERCENTAGE = 1

COIN_EXPIRING_DAYS = 90


pymysql.install_as_MySQLdb()

PWD = quote("Asez@0107#")
USR = "asez"
HOST = "172.105.62.141"
PORT = 3030
DATABASE = "aseztak"
# PWD = quote("Asez@321")
# USR = "root"
# HOST = "192.168.0.105"
# PORT = 3030
# DATABASE = "aseztak_fastapi4"

# UPDATE ORDER STATUS EVERY 30 MINUTES

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}/{}'.format(
    USR, PWD, HOST, DATABASE)

engine = create_engine(SQLALCHEMY_DATABASE_URI, future=True,
                       echo=True, pool_pre_ping=True, pool_recycle=300)


SessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine, expire_on_commit=False)


Base = declarative_base()

# TABLES (MODELS)


class WalletModel(Base):
    __tablename__ = 'wallet'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    user_id = Column(Integer)
    reff_id = Column(Integer)

    reff_type = Column(String)

    description = Column(String)

    amount = Column(Float)

    status = Column(Integer)

    valid_upto = Column(Date)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)


class OrdModel(Base):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True, index=True)
    order_items = relationship(
        'OrdItemsModel',  back_populates='orders', lazy="dynamic")
    reff = Column(String)
    user_id = Column(Integer)
    order_number = Column(String)
    payment_status = Column(Integer)
    app_version = Column(String)
    address_id = Column(Integer)
    created_at = Column(TIMESTAMP)


class OrdStatusModel(Base):
    __tablename__ = 'order_status'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)


class OrdItemsModel(Base):
    __tablename__ = 'order_items'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)
    orders = relationship(
        'OrdModel',  back_populates='order_items', uselist=False)
    product_id = Column(Integer)
    images = Column(String, default=Null)
    status = Column(Integer)
    refund = Column(Integer, default=0)
    return_expiry = Column(Integer)
    quantity = Column(Integer)

    tax = Column(Float)

    price = Column(Float)
    product_discount = Column(Float)
    uuid = Column(String)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)


class PModel(Base):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    userid = Column(Integer, ForeignKey('users.id'), index=True)
    brand_id = Column(Integer, default=None)
    title = Column(String)
    slug = Column(String, unique=True)
    category = Column(Integer)
    short_description = Column(String)
    status = Column(Integer, default=1)
    featured = Column(Integer, default=0)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)


class PPricingModel(Base):
    __tablename__ = 'product_pricing'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    product_id = Column(Integer, ForeignKey('products.id'), index=True)
    price = Column(Float)
    mrp = Column(Float)
    tax = Column(Float)
    moq = Column(Integer)
    items = Column(Integer)
    unit = Column(String)
    hsn = Column(String)
    description = Column(String)
    default_image = Column(Integer, default=0)
    deleted_at = Column(TIMESTAMP, default=None)


class AsezServiceModel(Base):
    __tablename__ = 'aseztak_service'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    commission_date = Column(DATETIME)

    rate = Column(Float)

    gst_on_rate = Column(Float)

    tds_rate = Column(Float)

    tcs_rate = Column(Float)

    round_off = Column(Float)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)


class TypeValue(enum.Enum):
    ALL = 'ALL'
    INDIVIDUAL = 'INDIVIDUAL'
    CATEGORY = 'CATEGORY'


class PDiscountModel(Base):
    __tablename__ = 'product_discount'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    seller_id = Column(Integer, ForeignKey('users.id'), index=True)

    product_id = Column(Integer, ForeignKey('products.id'), index=True)

    type = Column(Enum(TypeValue), default='ALL')

    category_id = Column(Integer)

    discount = Column(Float)
    valid_upto = Column(TIMESTAMP)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)


def roundOf(price: float):
    if (float(price) % 1) >= 0.5:
        price = math.ceil(price)
    else:
        price = round(price)

    return price


class PHelper:
    def getPrice(product: PModel, pricing: PPricingModel, asez_service: AsezServiceModel, app_version: Optional[str] = 'V4', order_item_id: Optional[int] = 0):
        db: Session = SessionLocal()
        try:
            today = date.today()

            if(order_item_id == 0):
                # Check Product Discount
                product_discount = 0
                check_product_discount = product.product_discount.filter(
                    func.date_format(PDiscountModel.valid_upto, "%Y-%m-%d") >= today).order_by(PDiscountModel.id.desc()).first()
                if(check_product_discount is not None):
                    product_discount = check_product_discount.discount

                price = float(pricing.price)
                if(product_discount != 0):
                    discount = (price * float(product_discount)) / 100
                    price = (price - discount)
                    price = float(price)

                tax = float(pricing.tax)
                commission = float(asez_service.rate)
                gst_on_commission = float(asez_service.gst_on_rate)
                tds = float(asez_service.tds_rate)
                tcs = float(asez_service.tcs_rate)
                round_off = float(asez_service.round_off)
            else:
                item_data = db.query(OrdItemsModel).filter(
                    OrdItemsModel.id == order_item_id).first()

                price = float(item_data.price)
                if(item_data.product_discount != 0):
                    discount = (
                        price * float(item_data.product_discount)) / 100
                    price = (price - discount)
                    price = float(price)

                tax = float(item_data.tax)
                commission = float(asez_service.rate)
                gst_on_commission = float(asez_service.gst_on_rate)
                tds = float(asez_service.tds_rate)
                tcs = float(asez_service.tcs_rate)
                round_off = float(asez_service.round_off)

            if(commission != 0):
                if(round_off == 1):

                    commission_rate = (
                        commission * gst_on_commission) / 100 + commission

                    total_commission = (commission_rate + tds + tcs) + 100

                    product_price = (price * total_commission) / 100

                    product_price = roundOf(product_price)

                    product_price = product_price + (product_price * tax) / 100

                    product_price = roundOf(product_price)

                    return product_price

                else:

                    commission_rate = (
                        commission * gst_on_commission) / 100 + commission

                    total_commission = (commission_rate + tds + tcs)
                    # Asez Service
                    asez_service = (price * total_commission) / 100

                    # GST ON Product
                    gst_on_product = (price * tax) / 100

                    product_price = (asez_service + gst_on_product + price)

                    if(app_version == 'V4'):
                        product_price = round(product_price, 2)
                    else:
                        product_price = roundOf(product_price)

                    return product_price
            else:
                price = (price * tax) / 100 + price
                product_price = roundOf(price)

                return product_price

        except Exception as e:
            print(e)
        finally:
            db.close()

class AsezServices:

    def aseztak_services(commission_date: str):
        db: Session = SessionLocal()
        try:
            service = db.query(AsezServiceModel).filter(
                AsezServiceModel.commission_date <= commission_date).order_by(AsezServiceModel.id.desc()).first()

            return service
        except Exception as e:
            print(e)
        finally:
            db.close()    


def updateOrderRewardsPoints():
    db: Session = SessionLocal()
    try:
        wallets = db.query(WalletModel).filter(WalletModel.status == 0).filter(
            WalletModel.reff_type == 'ORDER_REWARDS')

        wallet_reff_ids = []
        walletreffids = []
        wallet_reff_final_ids = []
        today = datetime.now()
        today = today.strftime("%Y-%m-%d")
        orderdata = []
        if(wallets.count() > 0):
            for wallet in wallets.all():
                check_order_exist = db.query(OrdModel).join(OrdStatusModel, OrdStatusModel.order_id == OrdModel.id).filter(
                    OrdModel.reff == wallet.reff_id).having(func.max(OrdStatusModel.status) < 70).group_by(OrdStatusModel.order_id).order_by(OrdModel.id.desc()).count()
                if(check_order_exist == 0):
                    wallet_reff_ids.append(wallet.reff_id)

        if(len(wallet_reff_ids) > 0):
            for wallet in wallet_reff_ids:

                check_order_exist = db.query(OrdModel).join(
                    OrdItemsModel, OrdItemsModel.order_id == OrdModel.id).join(OrdStatusModel, OrdStatusModel.order_id == OrdModel.id).filter(OrdModel.reff == wallet).filter(OrdItemsModel.status >= 70).filter(OrdItemsModel.status != 80).filter(OrdItemsModel.status != 90).filter(OrdItemsModel.status != 100).filter(OrdItemsModel.status != 110).filter(OrdItemsModel.status != 980).filter(OrdStatusModel.status >= 70).filter(
                    func.ADDDATE(func.date_format(OrdStatusModel.created_at, "%Y-%m-%d"),  case([(OrdItemsModel.return_expiry != 0, OrdItemsModel.return_expiry - 1)])) < today).group_by(OrdItemsModel.order_id).order_by(OrdModel.id.desc())
                if(check_order_exist.count() != 0):
                    walletreffids.append(wallet)

        if(len(walletreffids) > 0):
            for wallet_reff in walletreffids:
                check_order_exist = db.query(OrdModel).join(
                    OrdItemsModel, OrdItemsModel.order_id == OrdModel.id).join(OrdStatusModel, OrdStatusModel.order_id == OrdModel.id).filter(OrdModel.reff == wallet_reff).filter(OrdItemsModel.status >= 70).filter(OrdItemsModel.status != 80).filter(OrdItemsModel.status != 90).filter(OrdItemsModel.status != 100).filter(OrdItemsModel.status != 110).filter(OrdItemsModel.status != 980).filter(OrdStatusModel.status >= 70).filter(
                    func.ADDDATE(func.date_format(OrdStatusModel.created_at, "%Y-%m-%d"),  case([(OrdItemsModel.return_expiry != 0, OrdItemsModel.return_expiry - 1)])) > today).group_by(OrdItemsModel.order_id).order_by(OrdModel.id.desc())
                if(check_order_exist.count() == 0):
                    wallet_reff_final_ids.append(wallet_reff)
        if(len(wallet_reff_final_ids) > 0):
            for wallet in wallet_reff_final_ids:
                today = datetime.now()
                today = today.strftime("%Y-%m-%d")
                orders = db.query(OrdModel).join(
                    OrdItemsModel, OrdItemsModel.order_id == OrdModel.id).filter(OrdModel.reff == wallet).filter(OrdItemsModel.status.between(70, 81)).filter(OrdItemsModel.status != 80).group_by(OrdItemsModel.order_id).order_by(OrdModel.id.desc()).all()
                order_ids = []
                for order in orders:
                    items = db.query(OrdItemsModel.order_id).filter(OrdItemsModel.order_id == order.id).join(
                        OrdStatusModel, OrdStatusModel.order_id == OrdItemsModel.order_id).filter(
                        OrdItemsModel.status >= 70).filter(OrdItemsModel.status != 80).filter(OrdItemsModel.status != 90).filter(OrdItemsModel.status != 100).filter(OrdItemsModel.status != 110).filter(OrdItemsModel.status != 980).filter(OrdStatusModel.status >= 70).filter(
                        func.ADDDATE(func.date_format(OrdStatusModel.created_at, "%Y-%m-%d"),  case([(OrdItemsModel.return_expiry != 0, OrdItemsModel.return_expiry - 1)])) < today).group_by(OrdItemsModel.order_id).order_by(OrdItemsModel.id.desc()).all()
                    if(len(items) > 0):
                        for item in items:
                            order_ids.append(item.order_id)
                orderdata.append(order_ids)
            orderids = list(
                itertools.chain(*orderdata))
            if(len(orderids) > 0):
                order_reff_list = db.query(OrdModel.reff).filter(
                    OrdModel.id.in_(orderids)).group_by(OrdModel.reff).all()
                for orderreff in order_reff_list:
                    order_list = db.query(OrdModel).filter(OrdModel.reff == orderreff.reff).filter(
                        OrdModel.id.in_(orderids)).all()
                    total_rewards_points = 0
                    calculatewallet_amount = 0
                    for ord in order_list:
                        # Aseztak Service
                        today_date = ord.created_at.strftime('%Y-%m-%d')

                        aseztak_service = AsezServices.aseztak_services(
                            commission_date=today_date)

                        items = ord.order_items.filter(OrdItemsModel.status.between(
                            70, 81)).filter(OrdItemsModel.status != 80).all()
                        total_amount = 0

                        for item in items:
                            # Pricing Object
                            pricingdata = item.uuid.split('-')
                            pricingdata = db.query(PPricingModel).filter(
                                PPricingModel.id == pricingdata[1]).first()
                            product = db.query(PModel).filter(
                                PModel.id == item.product_id).first()
                            # Get Product Price
                            product_price: PModel = PHelper.getPrice(
                                product, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item.id)

                            total_amount += product_price * item.quantity
                            totalamount = product_price * item.quantity
                            calculateforwalletamount = (
                                float(totalamount) * float(REWARDS_PERCENTAGE) / 100)
                            calculateforwalletamount = int(
                                calculateforwalletamount)
                            calculatewallet_amount += calculateforwalletamount

                    #     total_rewards_points += total_amount
                    # rewards_points = (float(total_rewards_points) *
                    #                   float(REWARDS_PERCENTAGE)) / 100
                    rewards_points = int(calculatewallet_amount)
                    # Check Wallet
                    todaydate = date.today()
                    td = timedelta(days=COIN_EXPIRING_DAYS)
                    valid_upto = todaydate + td
                    valid_upto = valid_upto.strftime("%Y-%m-%d")
                    dbwallet = db.query(WalletModel).filter(WalletModel.reff_type == 'ORDER_REWARDS').filter(
                        WalletModel.status == 0).filter(WalletModel.reff_id == ord.reff).first()
                    dbwallet.amount = rewards_points
                    dbwallet.status = 1
                    dbwallet.valid_upto = valid_upto
                    db.flush()
                    db.commit()

        return "UPDATED"

    except Exception as e:
        print(e)
    finally:
        db.close()    


if __name__ == '__main__':
    # insertTestData()
    updateOrderRewardsPoints()
    print("UPDATED")

# class TestInsertDataModel(Base):

#     __tablename__ = 'test'

#     id = Column(Integer, primary_key=True, index=True)
#     name = Column(String)
#     mobile = Column(String)
#     status = Column(Integer)
#     created_at = Column(TIMESTAMP)


# def insertTestData():
#     db = SessionLocal()
#     try:
#         insert_data = TestInsertDataModel(
#             name='Rahul SINGH',
#             mobile='8617518041',
#             status=1,
#             created_at=datetime.now()
#         )
#         db.add(insert_data)
#         db.commit()
#         db.refresh(insert_data)
#         return "Added"
#     except Exception as e:
#         print(e)


# if __name__ == '__main__':
#     insertTestData()
#     print("UPDATED")
