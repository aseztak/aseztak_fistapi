# from fastapi.templating import Jinja2Templates
from app.services.templating import Jinja2Templates
from pathlib import Path

# template directory
BASE_PATH = Path().resolve()

templates = Jinja2Templates(directory=str(BASE_PATH / "app/admin/templates"))
