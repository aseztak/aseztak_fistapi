
from starlette.config import Config
from starlette.datastructures import Secret
from loguru import logger
import logging
import sys
from typing import List
from app.core.logging import InterceptHandler

config = Config(".env")

# API_PREFIX = config("API_PREFIX")
# API_PREFIX_SELLER = config("API_PREFIX_SELLER")

DEBUG: bool = config("DEBUG", cast=bool, default=False)

PROJECT_NAME = config("ROJECT_NAME", cast=str, default="MYAPP")
VERSION = config("VERSION", cast=str, default="1.0.0")
API_PREFIX = config("API_PREFIX", cast=str, default="/api")
# API_PREFIX_SELLER = config(
#     "API_PREFIX_SELLER", cast=str, default="/api/seller")
ADMIN_PREFIX = config("ADMIN_PREFIX", cast=str, default="/admin")
SECRET_KEY = config("SECRET_KEY", cast=Secret, default=config("SECRET_KEY"))
ALGORITHM = config("ALGORITHM")

MYSQL_DB: str = config("MYSQL_DB", cast=str, default="")
MYSQL_PORT: int = config("MYSQL_PORT", cast=int, default=3306)
MYSQL_USER: str = config("MYSQL_USER", cast=str, default="root")
MYSQL_PASSWORD: str = config("MYSQL_PASSWORD", cast=str, default="")
MYSQL_HOST: str = config("MYSQL_HOST", cast=str, default="127.0.0.1")


# MSG91
API_ENDPOINT_MSG91: str = "https://api.msg91.com/api/v5/otp?"
AUTHKEY_MSG91: str = '347198A1zlxIECi5fb23f5aP1'
TEMPLATE_ID_MSG91: str = '606b178171f0fd25b11b6c96'
MOBILE_CHANGE_TEMPLATE_ID_MSG91: str = '60646589c2b4c72bc040ac67'

# PAYTM
PAYTM_MID_KEY = config("PAYTM_MID_KEY")
PAYTM_SECRET_KEY = config("PAYTM_SECRET_KEY")

# APP MODE
APP_MODE = config("APP_MODE")

# FREE DELIVERY
FREE_DELIVERY = config("FREE_DELIVERY")

#API SECRET KEY
API_SECRET_KEY = config("API_SECRET_KEY")

# REWARDS
REWARDS_PERCENTAGE = 1

# REWARDS COIN EXPIRING DAYS
COIN_EXPIRING_DAYS = 90

# logging configuration
LOGGING_LEVEL = logging.DEBUG if config("DEBUG") else logger.INFO
logging.basicConfig(
    handlers=[InterceptHandler(level=LOGGING_LEVEL)], level=LOGGING_LEVEL
)
logger.configure(handlers=[{"sink": sys.stderr, "level": LOGGING_LEVEL}])
