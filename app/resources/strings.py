# API messages

USER_DOES_NOT_EXIST_ERROR = "user does not exist"
ARTICLE_DOES_NOT_EXIST_ERROR = "article does not exist"
ARTICLE_ALREADY_EXISTS = "article already exists"
USER_IS_NOT_AUTHOR_OF_ARTICLE = "you are not an author of this article"

INCORRECT_LOGIN_INPUT = "incorrect email or password"
USERNAME_TAKEN = "user with this username already exists"
EMAIL_TAKEN = "user with this email already exists"

UNABLE_TO_FOLLOW_YOURSELF = "user can not follow him self"
UNABLE_TO_UNSUBSCRIBE_FROM_YOURSELF = "user can not unsubscribe from him self"
USER_IS_NOT_FOLLOWED = "you don't follow this user"
USER_IS_ALREADY_FOLLOWED = "you follow this user already"

WRONG_TOKEN_PREFIX = "unsupported authorization type"  # noqa: S105
MALFORMED_PAYLOAD = "could not validate credentials"

ARTICLE_IS_ALREADY_FAVORITED = "you are already marked this articles as favorite"
ARTICLE_IS_NOT_FAVORITED = "article is not favorited"

COMMENT_DOES_NOT_EXIST = "comment does not exist"

AUTHENTICATION_REQUIRED = "authentication required"


# Product Static Text
PRODUCT_DELIVERY_HEADING_TEXT = "PRODUCT DELIVERY"

PRODUCT_DELIVERY_BODY_TEXT = "Flat Delivery Charge"

PRODUCT_FREE_DELIVERY_TEXT = "Free Shipping"

FREE_DELIVERY_ON_ONLINE_PAYMENT = "Free Delivery on Online Payment"

PAYMENT_OPTION_HEADING_TEXT = "PAYMENT OPTIONS"

PAYMENT_OPTION_BODY_TEXT = "Cash on Delivery & Online Payment Available"

RETURN_DAYS_HEADING = "PRODUCT RETURN"

RETURN_DAYS_BODY = "Days return policy available. If there is any issue with your product, you can raise a return request"

RETURN_DAYS_NOT_APPLICABLE = "Return not applicable on this Product."

DISCOUNT_TEXT = "Extra Discount on Online Payment"
# OTP MOBILE VERIFICATION
AUTHKEY = "347198A1zlxIECi5fb23f5aP1"

# TEMPLATE_ID = "606b178171f0fd25b11b6c96"
TEMPLATE_ID = "6657215dd6fc051283289212"

OTP_SEND_URL = "https://api.msg91.com/api/v5/otp?"

MESSAGE_API_ENDPOINT_MSG91 = "https://api.msg91.com/api/v5/flow/"


# MESSAGES TEMPLATE ID
ORDER_MESSAGE_BUYER_TEMPLATE_ID = "5fbb7a96bf4d21012c023c15"

FREE_DELIVERY_MESSAGE_TO_BUYER_TEMPLATE_ID = "633553560b6cb9513a4585d3"

ORDER_MESSAGE_SELLER_TEMPLATE_ID = "606ac0da6e1c4874d83cbf77"


# Order Cancel Message to Buyer and Seller Template ID
ORDER_CANCELLED_ITEM_TEMPLATE_ID = "606ac80008dcad20ea4f0fb5"
ORDER_CANCELLED_TEMPLATE_ID = "606ac90e5842ff4bfc1e1d03"

# ORDER DELIVERED MESSAGE
ORDER_DELIVERED_TEMPLATE_ID = "606af414de02f4145d000d52"

# ORDER SHIPPED MESSAGE TEMPLATE ID
ORDER_SHIPPED_TEMPLATE_ID = "606ac16f5eab5f3d004d8f79"


# NEW ORDERS MESSAGE TEMPLATE FOR SELLER
NEW_ORDER_MSZ_TEMPLATE_ID = "6274d7be588f4402b63ffa57"

# Return Declined Message
RETURN_DECLINED_MESSAGE = "606c03c118fc304af72539b8"

# DELHIVERY API
API_TOKEN = "4b7022d7f350abad9312bbf36e400af9f834fde1"
API_TEST_TOKEN = "578c82a72a51741582d9e3a0783bdd44e5c65c04"
API_URL = "https://track.delhivery.com/"

API_TEST_CANCEL_URL = "https://staging-express.delhivery.com/api/p/edit"

API_CANCEL_URL = "https://track.delhivery.com/api/p/edit"

API_TEST_WAREHOUSE_URL = "https://staging-express.delhivery.com/api/backend/clientwarehouse/create/"

API_WAREHOUSE_URL = "https://track.delhivery.com/api/backend/clientwarehouse/create/"


API_TEST_WAREHOUSE_EDIT_URL = "https://staging-express.delhivery.com/api/backend/clientwarehouse/edit/"

API_WAREHOUSE_EDIT_URL = "https://track.delhivery.com/api/backend/clientwarehouse/edit/"


ORDER_CREATION_API_TEST = 'https://staging-express.delhivery.com/api/cmu/create.json'

ORDER_CREATION_API_LIVE = 'https://track.delhivery.com/api/cmu/create.json'

ORDER_SLIP_CREATION_API_TEST = 'https://staging-express.delhivery.com/api/p/packing_slip'

ORDER_SLIP_CREATION_API_LIVE = 'https://track.delhivery.com/api/p/packing_slip'

ORDER_TRACK_API_LIVE = "https://track.delhivery.com/api/v1/packages/json/"

TDS = 1

TCS = 1

TCS_NAME_TEXT = 'TCS Policy',
TCS_BODY_TEXT = "As per the Section 52 of CGST Act 2017 TCS: Tax Collection at Source (TCS) E-commerce aggregators are made responsible under the GST law for deducting and depositing tax at the rate of 1% from each of the transaction. Any dealers/traders selling goods/services online would get the payment after deduction of 1% tax",

TDS_NAME_TEXT = 'TDS Policy',
TDS_BODY_TEXT = 'As per the Section 194 O of Income Tax Act 1961 TDS: Tax Deduction at Source (TDS) E-commerce Operator that facilitates [through its digital/ electronic facility/ platform] sale of goods or provision of services of an E-commerce Participant shall at the time of credit/ payment of amount of consideration for goods sold/ services deduct tax @ 1.0 % ( Rate has been reduced to 0.75 % upto March 31, 2021 and shall be 1 % after April 2021) on the gross amount of sale/ service consideration.'


FCM_TOKEN = 'key=AAAAzdN3OzE:APA91bHpBCu-SDR_ThhTau0ipewzis-z8rzxsd2jLdjU0KL25hADh4xw-tdVeE-2RotY6-_sp4UpYeH_PJP6vmfdROKt24xiIP6hdRCERAcI0QE0AyH-8THDB7hWFYjOB8AQ27bq6RKh',

WP_API_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0ZGRjNWJkMTU4ZTMxMWIyOWJlZDA0MiIsIm5hbWUiOiJBc2V6dGFrIEJyb2FkY2FzdGluZyIsImFwcE5hbWUiOiJBaVNlbnN5IiwiY2xpZW50SWQiOiI2NGNhNTJlNWEzOTc5MTUyYzkxNDYxMjkiLCJhY3RpdmVQbGFuIjoiQkFTSUNfTU9OVEhMWSIsImlhdCI6MTY5MjI2MzczN30.PG_Lit0zm35qiy4rmfzjir-qb8YgUSiDuwf0BFI9gVI"

# Message
AUTHKEY_MSG91 = '347198A1zlxIECi5fb23f5aP1'
MESSAGE_API_ENDPOINT_MSG91 = "https://api.msg91.com/api/v5/flow/"
BUYER_ACTIVATE_TEMMPLATE = "606c641359d0cd555405ff18"

# LINODE AWS OBJECT STORAGE
linode_obj_config = {
    "aws_access_key_id": "0XBXPG8SM540B9QFAYWT",
    "aws_secret_access_key": "qSVQpbfNuTIwN6eYPPy4R3DkT66nd5MMKFptv4hX",
    "endpoint_url": "https://aseztak.ap-south-1.linodeobjects.com",
}


# ZERO PERCENTAGE COMMISSION
NEW_COMMISSION_DATE = '2022-01-19'

NEW_COMMISSION_PERCENT = 12

NEW_TDS_PERCENT = 0

NEW_TCS_PERCENT = 0

TDS_TCS_PERCENT_DATE = '2022-01-25'


# TCS AND TDS POLICY FOR PRODUCT PRICING PRICE BREAKUP
TCS_POLICY_TITLE = 'TCS Policy'
TCS_POLICY_BODY = 'As per the Section 52 of CGST Act 2017 TCS: Tax Collection at Source (TCS) E-commerce aggregators are made responsible under the GST law for deducting and depositing tax at the rate of 1% from each of the transaction. Any dealers/traders selling goods/services online would get the payment after deduction of 1% tax'

TDS_POLICTY_TITLE = 'TDS Policy'
TDS_POLICY_BODY = 'As per the Section 194 O of Income Tax Act 1961 TDS: Tax Deduction at Source (TDS) E-commerce Operator that facilitates [through its digital/ electronic facility/ platform] sale of goods or provision of services of an E-commerce Participant shall at the time of credit/ payment of amount of consideration for goods sold/ services deduct tax @ 1.0 % ( Rate has been reduced to 0.75 % upto March 31, 2021 and shall be 1 % after April 2021) on the gross amount of sale/ service consideration.'


CUSTOMER_WILL_SEE = 'Customer Will See'
THIS_PRICE_WILL_BE_SHOWN_IN_THE_APP = 'This price will be shown in the Aseztak Wholesale App'


SERVICE_CHARGE_ON_PRODUCT_VALUE_TEXT = 'Payment gateway or cash collection charges for every sale. Note: The final amount (You will get amount) always shows while seller listing products on aseztak’s seller panel. A small payment collection fee is charged to you for all prepaid and postpaid orders that you receive. For a prepaid order - Based on Payment gateway, For a postpaid order - Based on cash collection charges. Transportation: Third Party Logistics (3PL) charges for every order which order PICKED by the courier partner. In case the order is Return To Origin (RTO) When Customer do not receive the product, Destination To Origin (DTO) When customer raise a return in this case Aseztak does not charge from the seller and manages itself.'


PRICE_BREAKUP = 'Price Breakup'

YOU_WILL_GET = 'You will get'


FREE_DELIVERY_TEXT = 'Free Delivery'


DOCUMENT_TYPE = {
    'AADHAAR',
    'PAN',
    'GSTIN',
    'UDYOG AADHAAR',
    'TRADE LICENSE'
}
