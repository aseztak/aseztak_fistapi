import time
from typing import Dict

import jwt

from app.core import config


JWT_SECRET = config.SECRET_KEY
JWT_ALGORITHM = config.ALGORITHM


def token_response(token: str):
    return {
        "access_token": token
    }


def signJWT(data: dict):

    data = {
        "id": data.id,
        # "name": data.name,
        # "mobile": data.mobile,
        # "email": data.email,
        # "city": data.city,
        # "region": data.region,
        # "country": data.country,
        # "pincode": data.pincode,
        # "status": data.status
    }

    # will create a new dictionary
    payload = {**data, **{'expires': time.time() + 600 * 60 * 60 * 60}}

    token = jwt.encode(
        payload, key=f"{JWT_SECRET}", algorithm=f"{JWT_ALGORITHM}").decode('utf-8')

    return token_response(token)


def loginJWT(data: dict):

    data = {
        "email": data.email,
        'username': data.username,
        'full_name': data.full_name,
        "password": data.hashed_password
    }

    # will create a new dictionary
    payload = {**data, **{'expires': time.time() + 600 * 60 * 60 * 60}}

    token = jwt.encode(
        payload, key=f"{JWT_SECRET}", algorithm=f"{JWT_ALGORITHM}").decode('utf-8')

    return token_response(token)


def decodeJWT(token: str) -> dict:

    try:

        decoded_token = jwt.decode(
            token, key=f"{JWT_SECRET}", algorithm=f"{JWT_ALGORITHM}",)

        return decoded_token if decoded_token["expires"] >= time.time() else None
    except Exception as e:
        print(e)
        return {}
