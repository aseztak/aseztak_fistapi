from fastapi import Request, HTTPException

from app.services.auth_handler import decodeJWT


def auth(request: Request) -> dict:
    header_token = request.headers.get('Authorization')

    header_token = header_token.replace('Bearer ', '')
    userdata = decodeJWT(header_token)

    return userdata
