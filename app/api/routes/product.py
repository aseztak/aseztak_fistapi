

from datetime import date
from starlette.status import HTTP_202_ACCEPTED, HTTP_200_OK
from app.db.models.categories import CategoriesModel, ProductCategories
from app.db.models.media import ProductMediaModel
from app.db.models.products import InventoryModel, ProductModel, ProductPricingModel
from app.api.helpers.products import ProductModel, ProductsHelper

from app.db.schemas.product_schema import ProductData, ProductBuyDataSchema

from app.services.auth_bearer import JWTBearer
from app.db.config import  get_db
from sqlalchemy.orm.session import Session
from starlette.requests import Request
from app.services.auth import auth
from fastapi import APIRouter,  Depends
from app.api.util.calculation import productPricecalculation, floatingValue, calculatediscountprice
from app.db.models.carts import CartModel
from app.db.models.user import UserModel
from sqlalchemy.orm import joinedload
from app.api.helpers.services import AsezServices
from app.api.helpers.products import ProductsHelper
from sqlalchemy import func
from app.api.helpers.discount import Discount
from app.db.models.product_discount import ProductDiscountModel

product_router = APIRouter()


# Product Buying Option mens-casual-full-sleeves-t-shirt soker-ls-bio-wash-sando-set-of-10pcs
@product_router.get("/buy/{p_id}", response_model=ProductBuyDataSchema, dependencies=[Depends(JWTBearer())])
async def product_buying_option(request: Request, p_id: int, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        data: ProductModel = await ProductsHelper.get_product_buy(
            db=db, p_id=p_id)

        # calculating Price
        # Check Pricing
        pricing = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == data.id).filter(
            ProductPricingModel.deleted_at.is_(None)).order_by(ProductPricingModel.id.asc()).all()

        pricings = []
        for p in pricing:
            # Get Product Detail
            productdata = db.query(ProductModel).where(
                ProductModel.id == p.product_id).first()

            # unique ID
            uuid = "sku-" + str(p.id) + '-variable-' + str(p.items)

            if(pricing):
                checkinventory = db.query(InventoryModel).where(
                    InventoryModel.pricing_id == p.id).first()

            # Check Stock
            stock = True
            if(checkinventory and checkinventory.out_of_stock == 0):
                stock = True
            else:
                stock = False

            # Check Unlimited Stock
            unlimited = True
            if(checkinventory and checkinventory.unlimited == 1):
                unlimited = True
            else:
                unlimited = False

            # Check Total Stock
            total_stock = 0
            if(checkinventory and checkinventory.stock != 0):
                total_stock = checkinventory.stock

            # Check Out Of Stock Quantity with Cart data
            cart = db.query(CartModel).where(CartModel.product_id == data.id).where(
                CartModel.uuid == uuid).where(CartModel.user_id == userdata['id']).first()

            out_of_quantity = False

            message = ''

            if(cart and cart.quantity > checkinventory.stock and checkinventory.unlimited == 0):
                out_of_quantity = True
                message = 'Only '+str(checkinventory.stock) + \
                    ' '+str(p.unit)+' left'

            # Aseztak Service
            # aseztak_service = Services.aseztak_services(
            #     p.updated_at, db=db)
            today_date = p.updated_at.strftime(
                '%Y-%m-%d')
            aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
            product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, p, asez_service=aseztak_service, app_version='V4')
            # product_price = productPricecalculation(price=p.price, tax=p.tax, commission=aseztak_service.rate,
            #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

            total = round(product_price, 2)

            item_total = 0
            quantity = 0
            if(cart):
                item_total = (cart.quantity * total)
                quantity = cart.quantity

            # Product Image
            if(p.default_image != 0):
                image = db.query(ProductMediaModel).filter(ProductMediaModel.id == p.default_image).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()

                if(image is not None):
                    filename = image.file_path
                else:
                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == p.product_id).filter(
                        ProductMediaModel.deleted_at.is_(None)).first()
                    if(image is not None):
                        filename = image.file_path
                    else:
                        filename = ''
            else:
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == p.product_id).filter(ProductMediaModel.default_img == 1).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()

                if(image is None):
                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == p.product_id).filter(
                        ProductMediaModel.deleted_at.is_(None)).first()

                filename = image.file_path

            img = filename

            pdata = {
                'id': p.id,
                'product_id': p.product_id,
                'image': img,
                'price': floatingValue(total),
                'moq': p.moq,
                'tax': floatingValue(p.tax),
                'quantity': quantity,
                'sale_price': floatingValue(p.price),
                'description': p.description,
                'stock': stock,
                'unlimited':  unlimited,
                'out_of_quantity': out_of_quantity,
                'total_stock': total_stock,
                'message': message,
                'unit': p.unit,
                'uuid': uuid,
                'hsn_code': p.hsn,
                'item_total': floatingValue(round(item_total, 2)),
                'attributes': await ProductsHelper.getPricingAttribjutes(data=p, db=db)

            }

            pricings.append(pdata)

        return {'status_code': HTTP_202_ACCEPTED, 'product': data, 'pricing': pricings}
    except Exception as e:
        print(e)

# Seller Wise Products (Category Wise) View All


@product_router.get("/seller-wise/view-all/{category_id}/{seller_id}", response_model=ProductData, dependencies=[Depends(JWTBearer())])
async def sellerWiseProductsViewAll(request: Request, category_id: int, seller_id: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        userdata = auth(request=request)
        # Check User
        check_user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).join(ProductCategories, ProductCategories.product_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(ProductPricingModel.deleted_at.is_(None)).filter(
            ProductCategories.category_id == category_id).filter(ProductModel.userid == seller_id).filter(UserModel.status == 1).filter(ProductModel.status == 51).group_by(ProductModel.id).order_by(ProductModel.id.desc())

        # Category
        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == category_id).first()

        if(products.count() == 0):
            return {"status_code": HTTP_200_OK, "is_registered": True, "products": [], "total_products": 0, "current_page": page, "total_pages": 0}

        total_records = products.count()

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != "0"):
            total_pages = int(total_pages[0]) + 1
        else:
            total_pages = int(total_pages[0])

        productdata: ProductModel = products.limit(
            limit=limit).offset((page - 1) * limit).all()

        # calculating Price
        # Aseztak Service
        today = date.today()
        # aseztak_service = Services.aseztak_services(
        #     today, db=db)
        today_date = today.strftime(
            '%Y-%m-%d')
        aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
        for product in productdata:
            # CHECK PRODUCT DISCOUNT
            check_product_discount = product.product_discount.filter(
                func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
            product_discount = {
                'rate': '0.00',
                'product_sale_price': '0.00',
                'discount_amount': '0.00',
            }

            # Check Pricing
            pricing = product.product_pricing[0]
            if(pricing):
                product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=aseztak_service, app_version='V4')
                # Get Product Sale Price
                product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
                                                             gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                # Check ONLINE Discounted Price
                discount_price = float(0.00)
                checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                if(checkdiscount != 0):
                    discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                # Check Product Discount
                if(check_product_discount is not None):
                    product_discount = {
                        'rate': floatingValue(check_product_discount.discount),
                        'product_sale_price': floatingValue(product_sale_price),
                        'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                    }
                # Restructure Price data
                price_data = {
                    'id': pricing.id,
                    'price': floatingValue(product_price),
                    'product_discount': product_discount,
                    'discount': str(checkdiscount),
                    'discount_price': floatingValue(discount_price),
                    'moq': pricing.moq,
                    'unit': pricing.unit
                }
                product.pricing = price_data

             # Favourite
            product.favourite = False

            # Product Image
            productimage = product.images[0]

            product.image = ''
            if(productimage):
                product.image = productimage.file_path
        if(check_user.status == 1):
            is_registered = True
        else:
            is_registered = False
        return {"status_code": HTTP_200_OK, "is_registered": is_registered, "title": category.name, "products": productdata, "total_products": total_records, "current_page": page, "total_pages": total_pages}

    except Exception as e:
        print(e)

