
from starlette.requests import Request

from app.api.helpers.orders import OrderHelper
from app.db.models.acconts import AccountsModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.orders import OrdersModel
from app.services.auth import auth
from starlette.status import HTTP_200_OK
from app.db.models.products import ProductModel
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.products import *
from app.services.auth import auth
from app.db.schemas.orders_schema import CommissionOrderSchema
from fastapi import APIRouter,  Depends
from os import getcwd
from starlette.requests import Request
from app.db.models.shipping_address import ShippingAddressModel
from app.db.models.user import UserProfileModel
import pandas as pd
from app.api.helpers.media import MediaHelper
from os import getcwd, unlink
from app.api.util.calculation import *
from app.db.models.seller_payment import SellerPaymentModel
from app.db.models.category_commission import CategoryCommissionModel
seller_payments_router = APIRouter()

# upcoming order list


@seller_payments_router.get("/upcoming-list", dependencies=[Depends(JWTBearer())])
async def upcomingList(request: Request, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        if(userdata['id'] != 65):
            # orders = await OrderHelper.getOrderCommissionList(
            #     user_id=userdata['id'], db=db)

            ordersdata: OrdersModel = await OrderHelper.getCommissionSellerListwithReturnDays(db=db)

            orderItems: OrdersModel = ordersdata.filter(
                ProductModel.userid == userdata['id']).all()

            order_items = await OrderHelper.getCommissionSellerListwithZeroReturn(db=db, seller_id=userdata['id'])

            merged_orders = (order_items + orderItems)

            final_merged = []
            for merged_order in merged_orders:
                final_merged.append(merged_order.order_id)

            orders = db.query(OrderItemsModel).filter(OrderItemsModel.order_id.in_(final_merged)).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

        else:
            # orders = await OrderHelper.getOrderCommissionListRajdhaniGarments(
            #     user_id=userdata['id'], db=db)

            ordersdata: OrdersModel = await OrderHelper.getCommissionSellerListwithReturnDaysRajdhani(db=db)

            orderItems: OrdersModel = ordersdata.filter(
                ProductModel.userid == userdata['id']).all()

            order_items = await OrderHelper.getCommissionSellerListwithZeroReturnRajdhani(db=db)

            merged_orders = (order_items + orderItems)

            final_merged = []
            for merged_order in merged_orders:
                final_merged.append(merged_order.order_id)

            orders = db.query(OrderItemsModel).filter(OrderItemsModel.order_id.in_(final_merged)).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

        total_order = []
        paid_amount = 0
        deduction_total_amount = 0
        addition_total_amount = 0
        total_amount = 0

        for orderitems in orders:

            # Order
            order = db.query(OrdersModel).filter(
                OrdersModel.id == orderitems.order_id).first()

            # Check Lost Items Check
            checklostItems = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status.between(80, 90)).filter(
                OrderItemsModel.status != 81).first()

            if(checklostItems is None):
                total_order.append(orderitems.order_id)

                if(order.app_version == 'V4'):
                    # taxable Amount
                    taxable_amount = calculateTaxableAmount(
                        price=orderitems.price, quantity=orderitems.quantity, discount=orderitems.product_discount)

                    # GST AMOUNt
                    gst_amount = calculateGstAmount(
                        price=taxable_amount, tax=orderitems.tax)

                    # amount after tax
                    amount_after_tax = round(taxable_amount + gst_amount, 2)

                    # Service Charge
                    service_charge = calculateCommissionSeller(
                        amount=taxable_amount, commission=orderitems.commission_seller)

                    service_charge_on_tax = calculateCommissionTaxSeller(
                        amount=service_charge, tax=orderitems.commission_seller_tax)

                    total_service_charge = round(
                        service_charge + service_charge_on_tax, 2)

                    # Tcs Amount
                    tcs_amount = calculateSellerTcsAmount(
                        amount=taxable_amount, tax=orderitems.tcs_rate)

                    # Tds Amount
                    tds_amount = calculateSellerTdsAmount(
                        amount=amount_after_tax, tax=orderitems.tds_rate)

                    # Paid Amount
                    seller_will_get = (amount_after_tax -
                                       total_service_charge - tcs_amount - tds_amount)

                    #--------------DEDUCTION----------------------#
                    # Check Deduction Amount
                    checkDeductionAmount = db.query(AccountsModel).filter(
                        AccountsModel.txn_description == str('DEDUCTION (')+order.order_number+str(')')).first()

                    if(checkDeductionAmount is not None):
                        # count order items
                        count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                            OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                        deduction_amount = (
                            checkDeductionAmount.txn_amount / count_order_items)

                        deduction_total_amount += deduction_amount

                    checkAdditionAmount = db.query(AccountsModel).filter(
                        AccountsModel.txn_description == str('ADDITION (')+order.order_number+str(')')).first()

                    if(checkAdditionAmount is not None):
                        # count order items
                        count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                            OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                        addition_amount = (
                            checkAdditionAmount.txn_amount / count_order_items)

                        addition_total_amount += addition_amount

                    #--------------------DEDUCTION-----------------------------#

                    paid_amount += round(seller_will_get, 2)
                    total_amount += round(amount_after_tax, 2)

                else:

                    # taxable Amount
                    taxable_amount = calculateOldTaxableAmount(
                        price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, discount=orderitems.product_discount)

                    # GST AMOUNt
                    gst_amount = calculateGstAmount(
                        price=taxable_amount, tax=orderitems.tax)

                    # amount after tax
                    amount_after_tax = round(taxable_amount + gst_amount, 2)
                    # Service Charge

                    orderdate = order.created_at.strftime("%Y-%m-%d")
                    # product
                    productdata = db.query(ProductModel).filter(
                        ProductModel.id == orderitems.product_id).first()
                    categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
                                                                                  productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

                    totalamount = calculateWithoutTaxAmount(
                        price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=orderitems.product_discount)

                    orderTotalamoutwithouttax = totalamount['total_amount']

                    tcs_amount = (orderTotalamoutwithouttax *
                                  TCS) / 100
                    tcs_amount = round(tcs_amount, 2)

                    tds_amount = (orderTotalamoutwithouttax * TDS) / 100
                    tds_amount = round(tds_amount, 2)

                    if(orderdate < '2021-11-09'):

                        tcs_amount = 0.00
                        tds_amount = 0.00

                        seller_will_get = calculateTotalWithTax(
                            price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

                        total_amount += seller_will_get['tota_amount']

                        seller_will_get = (taxable_amount -
                                           seller_will_get['total_commision'])

                        #--------------DEDUCTION----------------------#
                        # Check Deduction Amount
                        checkDeductionAmount = db.query(AccountsModel).filter(
                            AccountsModel.txn_description == str('DEDUCTION (')+order.order_number+str(')')).first()

                        if(checkDeductionAmount is not None):
                            # count order items
                            count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                                OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                            deduction_amount = (
                                checkDeductionAmount.txn_amount / count_order_items)

                            deduction_total_amount += deduction_amount

                        checkAdditionAmount = db.query(AccountsModel).filter(
                            AccountsModel.txn_description == str('ADDITION (')+order.order_number+str(')')).first()

                        if(checkAdditionAmount is not None):
                            # count order items
                            count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                                OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                            addition_amount = (
                                checkAdditionAmount.txn_amount / count_order_items)

                            addition_total_amount += addition_amount

                        #--------------------DEDUCTION-----------------------------#

                        paid_amount += round(seller_will_get, 2)

                    else:

                        tcs_amount = tcs_amount
                        tds_amount = tds_amount

                        seller_will_get = calculateTotal(price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity,
                                                         commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=orderitems.product_discount)

                        total_amount += seller_will_get['tota_amount']

                        seller_will_get = (
                            seller_will_get['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

                        #--------------DEDUCTION----------------------#
                        # Check Deduction Amount
                        checkDeductionAmount = db.query(AccountsModel).filter(
                            AccountsModel.txn_description == str('DEDUCTION (')+order.order_number+str(')')).first()

                        if(checkDeductionAmount is not None):
                            # count order items
                            count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                                OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                            deduction_amount = (
                                checkDeductionAmount.txn_amount / count_order_items)

                            deduction_total_amount += deduction_amount

                        checkAdditionAmount = db.query(AccountsModel).filter(
                            AccountsModel.txn_description == str('ADDITION (')+order.order_number+str(')')).first()

                        if(checkAdditionAmount is not None):
                            # count order items
                            count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                                OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                            addition_amount = (
                                checkAdditionAmount.txn_amount / count_order_items)

                            addition_total_amount += addition_amount

                        #--------------------DEDUCTION-----------------------------#

                        paid_amount += round(seller_will_get, 2)

        #--------------------------DEDUCTION------------------------------#
        # CHECK DEDUCTION
        paid_amount = (
            paid_amount - deduction_total_amount + addition_total_amount)

        upcoming_payments = [{
            'created_at': '',
            'reff_no': '',
            'total_orders': len(total_order),
            'total_amounts': floatingValue(total_amount),
            'commission_amount': floatingValue(0.00),
            'seller_amount': floatingValue(0.00),
            'tcs_rate': TCS,
            'tcs_amount': floatingValue(0.00),
            'tds_rate': TCS,
            'tds_amount': floatingValue(0.00),
            'seller_will_get': floatingValue(paid_amount),
            'payment_date': '',
            'status': 'Pedning',
            'commission_generated': ''
        }]

        return {"status_code": HTTP_200_OK, "payment_list": upcoming_payments,  "page": 1, "total_records": len(total_order), "total_pages": 0}

    except Exception as e:
        print(e)


# Seller Upcomming Payments Order Item List
@seller_payments_router.get("/upcoming-order-items", dependencies=[Depends(JWTBearer())])
async def upCommingOrderItems(request: Request, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        if(userdata['id'] != 65):
            # ordersItems = await OrderHelper.getOrderCommissionList(
            #     user_id=userdata['id'], db=db)

            ordersdata: OrdersModel = await OrderHelper.getCommissionSellerListwithReturnDays(db=db)

            orderItems: OrdersModel = ordersdata.filter(
                ProductModel.userid == userdata['id']).all()

            order_items = await OrderHelper.getCommissionSellerListwithZeroReturn(db=db, seller_id=userdata['id'])

            merged_orders = (order_items + orderItems)
        else:
            # ordersItems = await OrderHelper.getOrderCommissionListRajdhaniGarments(
            #     user_id=userdata['id'], db=db)
            ordersdata: OrdersModel = await OrderHelper.getCommissionSellerListwithReturnDaysRajdhani(db=db)

            orderItems: OrdersModel = ordersdata.filter(
                ProductModel.userid == userdata['id']).all()

            order_items = await OrderHelper.getCommissionSellerListwithZeroReturnRajdhani(db=db)

            merged_orders = (order_items + orderItems)

        final_merged = []
        for merged_order in merged_orders:
            final_merged.append(merged_order.order_id)

        ordersItems = db.query(OrderItemsModel).filter(OrderItemsModel.order_id.in_(final_merged)).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
            OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

        download_csv = ''
        commission_pdf = ''

        item_data = []
        for item in ordersItems:

            # Product
            product = db.query(ProductModel).filter(
                ProductModel.id == item.product_id).first()

            # Order
            order = db.query(OrdersModel).filter(
                OrdersModel.id == item.order_id).first()

            # Check Lost Items Check
            checklostItems = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status.between(80, 90)).filter(
                OrderItemsModel.status != 81).first()

            if(checklostItems is None):
                # Buyer
                buyer = db.query(UserProfileModel).filter(
                    UserProfileModel.user_id == order.user_id).first()

                buyergstIN = ''
                if(buyer.proof_type == 'GSTIN'):
                    buyergstIN = buyer.proof

                # shipping_address
                shipping_address = db.query(ShippingAddressModel).filter(
                    ShippingAddressModel.user_id == order.user_id).first()

                # Address
                address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

                if(order.app_version == 'V4'):
                    # taxable Amount
                    taxable_amount = calculateTaxableAmount(
                        price=item.price, quantity=item.quantity, discount=item.product_discount)

                    # GST AMOUNt
                    gst_amount = calculateGstAmount(
                        price=taxable_amount, tax=item.tax)

                    # amount after tax
                    amount_after_tax = round(taxable_amount + gst_amount, 2)

                    # Service Charge
                    service_charge = calculateCommissionSeller(
                        amount=taxable_amount, commission=item.commission_seller)

                    service_charge_on_tax = calculateCommissionTaxSeller(
                        amount=service_charge, tax=item.commission_seller_tax)

                    total_service_charge = round(
                        service_charge + service_charge_on_tax, 2)

                    service_charge_amount = str(
                        total_service_charge) + str(' (')+str(item.commission_seller)+str('% + Tax)')

                    # Tcs Amount
                    tcs_amount = calculateSellerTcsAmount(
                        amount=taxable_amount, tax=item.tcs_rate)

                    # Tds Amount
                    tds_amount = calculateSellerTdsAmount(
                        amount=amount_after_tax, tax=item.tds_rate)

                    # Paid Amount
                    paid_amount = (amount_after_tax -
                                   total_service_charge - tcs_amount - tds_amount)

                    paid_amount = round(paid_amount, 2)

                else:
                    # taxable Amount
                    taxable_amount = calculateOldTaxableAmount(
                        price=item.price, tax=item.tax, quantity=item.quantity, discount=item.product_discount)

                    # GST AMOUNt
                    gst_amount = calculateGstAmount(
                        price=taxable_amount, tax=item.tax)

                    # amount after tax
                    amount_after_tax = round(taxable_amount + gst_amount, 2)
                    # Service Charge

                    orderdate = order.created_at.strftime("%Y-%m-%d")
                    # product
                    productdata = db.query(ProductModel).filter(
                        ProductModel.id == item.product_id).first()
                    categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
                                                                                  productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

                    totalamount = calculateWithoutTaxAmount(
                        price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=item.product_discount)

                    orderTotalamoutwithouttax = totalamount['total_amount']

                    tcs_amount = (orderTotalamoutwithouttax *
                                  TCS) / 100
                    tcs_amount = round(tcs_amount, 2)

                    tds_amount = (orderTotalamoutwithouttax * TDS) / 100
                    tds_amount = round(tds_amount, 2)

                    if(orderdate < '2021-11-09'):
                        category_commission = round(
                            categorycommission.commission + 5)
                        tcs_amount = 0.00
                        tds_amount = 0.00
                        tcs_rate = 0
                        tds_rate = 0
                        total_amount = calculateTotalWithTax(
                            price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
                        taxable_amount = total_amount['tota_amount']

                        service_charge_amount = str(
                            total_amount['total_commision']) + str(' (')+str(category_commission)+str('% + Tax)')

                        paid_amount = (taxable_amount -
                                       total_amount['total_commision'])

                    else:
                        category_commission = round(
                            categorycommission.commission)
                        tcs_rate = TCS
                        tds_rate = TDS
                        tcs_amount = tcs_amount
                        tds_amount = tds_amount

                        service_charge_amount = str(
                            totalamount['total_commisison']) + str(' (')+str(category_commission)+str('% + Tax)')

                        paid_amount = calculateTotal(price=item.price, tax=item.tax, quantity=item.quantity,
                                                     commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=item.product_discount)
                        paid_amount = (
                            paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

                        paid_amount = round(paid_amount, 2)

                #--------------DEDUCTION----------------------#
                # Check Deduction Amount
                checkDeductionAmount = db.query(AccountsModel).filter(
                    AccountsModel.txn_description == str('DEDUCTION (')+order.order_number+str(')')).first()

                deduction_amount = 0
                if(checkDeductionAmount is not None):
                    # count order items
                    count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                        OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                    deduction_amount = (
                        checkDeductionAmount.txn_amount / count_order_items)

                checkAdditionAmount = db.query(AccountsModel).filter(
                    AccountsModel.txn_description == str('ADDITION (')+order.order_number+str(')')).first()

                addition_amount = 0

                if(checkAdditionAmount is not None):
                    # count order items
                    count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                        OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                    addition_amount = (
                        checkAdditionAmount.txn_amount / count_order_items)

                #----------------------------------DEDUCTION----------------------------------------#
                paid_amount = (
                    paid_amount - deduction_amount + addition_amount)
                #--------------------DEDUCTION-----------------------------#

                deduction_remarks = ''
                if(deduction_amount != 0):
                    deduction_remarks = 'Wrong Items'

                itm = {
                    'order_id': order.id,
                    'created_at': order.created_at.strftime("%Y-%m-%d"),
                    'reff_no': '',
                    'order_number': order.order_number,
                    'buyer': shipping_address.ship_to,
                    'address': address,
                    'state': shipping_address.state,
                    'product': product.title,
                    'buyer_gst': buyergstIN,
                    'taxable_amount': floatingValue(round(taxable_amount, 2)),
                    'gst_rate': str(round(item.tax))+str("%"),
                    'gst_amount': floatingValue(round(gst_amount, 2)),
                    'amount_after_tax': floatingValue(amount_after_tax),
                    'service_charge': service_charge_amount,
                    'tcs_rate': floatingValue(TCS),
                    'tcs_amount': floatingValue(round(tcs_amount, 2)),
                    'tds_rate': floatingValue(TDS),
                    'tds_amount': floatingValue(round(tds_amount, 2)),
                    'status': '',
                    'deduction_amount': floatingValue(deduction_amount),
                    'deduction_remarks': deduction_remarks,
                    'addition_amount':  floatingValue(addition_amount),
                    'you_will_get': floatingValue(paid_amount),
                    'paid_amount': floatingValue(0.00)
                }

                item_data.append(itm)

        return {"status_code": HTTP_200_OK, "items_data": item_data, "page": 1, "total_records": len(item_data), "download_csv": download_csv, 'commission_pdf': commission_pdf,  "total_pages": 0}

    except Exception as e:
        print(e)

# Seller Locked Payments List


@seller_payments_router.get("/locked-list", dependencies=[Depends(JWTBearer())])
async def lockedPayments(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:
        userdata = auth(request=request)

        data: SellerPaymentModel = await OrderHelper.getGeneratedOrderCommissionList(user_id=userdata['id'], db=db)
        locked_payment_list = []
        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "payment_list": locked_payment_list, "page": page, "total_records": 0, "total_pages": 0}

        payments_list: SellerPaymentModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        for payments in payments_list:

            # Orders
            orders = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(
                OrdersModel.commission_reff == payments.reff_no).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

            total_amount = 0
          # Order
            order = db.query(OrdersModel).filter(
                OrdersModel.id == orders[0].order_id).first()
            if(order.app_version == 'V4'):

                for orderitems in orders:

                    # taxable Amount
                    taxable_amount = calculateTaxableAmount(
                        price=orderitems.price, quantity=orderitems.quantity, discount=orderitems.product_discount)

                    # GST AMOUNt
                    gst_amount = calculateGstAmount(
                        price=taxable_amount, tax=orderitems.tax)

                    # amount after tax
                    amount_after_tax = round(taxable_amount + gst_amount, 2)

                    total_amount += round(amount_after_tax, 2)

                totalamount = round(payments.total_amount, 2) + \
                    round(payments.commission_amount, 2) + \
                    round(payments.tcs_amount, 2) + \
                    round(payments.tds_amount, 2)

                # commission_amount = (totalamount - total_amount)

                commission_amount = (round(payments.commission_amount, 2)
                                     + round(payments.commission_tax_amount, 2))

                seller_amount = (round(total_amount, 2) -
                                 round(payments.commission_amount, 2))
            else:
                for orderitems in orders:

                    # taxable Amount
                    taxable_amount = calculateOldTaxableAmount(
                        price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, discount=orderitems.product_discount)

                    # GST AMOUNt
                    gst_amount = calculateGstAmount(
                        price=taxable_amount, tax=orderitems.tax)

                    # amount after tax
                    amount_after_tax = round(taxable_amount + gst_amount, 2)

                    total_amount += round(amount_after_tax, 2)

                totalamount = round(payments.total_amount, 2) + \
                    round(payments.commission_amount, 2) + \
                    round(payments.tcs_amount, 2) + \
                    round(payments.tds_amount, 2)

                commission_amount = (totalamount - total_amount)

                commission_amount = (round(payments.commission_amount, 2)
                                     - round(commission_amount, 2))

                seller_amount = total_amount - \
                    round(payments.commission_amount, 2)

            payment = {
                'created_at': payments.commission_date.strftime("%B %d %Y"),
                'reff_no': payments.reff_no,
                'total_orders': payments.total_orders,
                'total_amounts': floatingValue(round(total_amount, 2)),
                'commission_amount': floatingValue(round(commission_amount, 2)),
                'seller_amount': floatingValue(round(seller_amount, 2)),
                'tcs_rate': TCS,
                'tcs_amount': floatingValue(payments.tcs_amount),
                'tds_rate': TCS,
                'tds_amount': floatingValue(payments.tds_amount),
                'seller_will_get': floatingValue(payments.total_amount),
                'payment_date': '',
                'status': 'Pedning',
                'commission_generated': ''
            }

            locked_payment_list.append(payment)

        total_records = data.count()

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK, "payment_list": locked_payment_list, "page": page, "total_records": total_records, "total_pages": total_pages}

    except Exception as e:
        print(e)


# Seller Locked Payment Order Items List
@seller_payments_router.get("/locked-order-item-list/{reff_no}", dependencies=[Depends(JWTBearer())])
async def lockedOrderItemList(request: Request, reff_no: str, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        data: OrdersModel = await OrderHelper.getGeneratedOrderItemsList(reff_no=reff_no, db=db)
        item_data = []

        download_csv = ''
        commission_pdf = ''

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "items_data": item_data, "page": page, "total_records": 0,  "total_pages": 0}

        order_item_list: OrdersModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        for item in order_item_list:

            # Order
            order = db.query(OrdersModel).filter(
                OrdersModel.id == item.order_id).first()

            # Buyer
            buyer = db.query(UserProfileModel).filter(
                UserProfileModel.user_id == order.user_id).first()

            buyergstIN = ''
            if(buyer.proof_type == 'GSTIN'):
                buyergstIN = buyer.proof

            # shipping_address
            shipping_address = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.user_id == order.user_id).first()

            # Address
            address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

            if(order.app_version == 'V4'):
                # taxable Amount
                taxable_amount = calculateTaxableAmount(
                    price=item.price, quantity=item.quantity, discount=item.product_discount)

                # GST AMOUNt
                gst_amount = calculateGstAmount(
                    price=taxable_amount, tax=item.tax)

                # amount after tax
                amount_after_tax = round(taxable_amount + gst_amount, 2)
                # Service Charge

                service_charge = calculateCommissionSeller(
                    amount=taxable_amount, commission=item.commission_seller)

                service_charge_on_tax = calculateCommissionTaxSeller(
                    amount=service_charge, tax=item.commission_seller_tax)

                total_service_charge = round(
                    service_charge + service_charge_on_tax, 2)

                tcs_rate = round(item.tcs_rate)
                tds_rate = round(item.tds_rate)

                # Tcs Amount
                tcs_amount = calculateSellerTcsAmount(
                    amount=taxable_amount, tax=item.tcs_rate)

                # Tds Amount
                tds_amount = calculateSellerTdsAmount(
                    amount=amount_after_tax, tax=item.tds_rate)

                service_charge_amount = str(total_service_charge) + \
                    ' ('+str(round(item.commission_seller))+'% + Tax)'
                # Paid Amount
                paid_amount = (amount_after_tax -
                               total_service_charge - tcs_amount - tds_amount)

                paid_amount = round(paid_amount, 2)

            else:
                # taxable Amount
                taxable_amount = calculateOldTaxableAmount(
                    price=item.price, tax=item.tax, quantity=item.quantity, discount=item.product_discount)

                # GST AMOUNt
                gst_amount = calculateGstAmount(
                    price=taxable_amount, tax=item.tax)

                # amount after tax
                amount_after_tax = round(taxable_amount + gst_amount, 2)
                # Service Charge

                orderdate = order.created_at.strftime("%Y-%m-%d")
                # product
                productdata = db.query(ProductModel).filter(
                    ProductModel.id == item.product_id).first()
                categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
                                                                              productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

                totalamount = calculateWithoutTaxAmount(
                    price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=item.product_discount)

                orderTotalamoutwithouttax = totalamount['total_amount']

                tcs_amount = (orderTotalamoutwithouttax *
                              TCS) / 100
                tcs_amount = round(tcs_amount, 2)

                tds_amount = (orderTotalamoutwithouttax * TDS) / 100
                tds_amount = round(tds_amount, 2)

                if(orderdate < '2021-11-09'):
                    category_commission = round(
                        categorycommission.commission + 5)
                    tcs_amount = 0.00
                    tds_amount = 0.00
                    tcs_rate = 0
                    tds_rate = 0
                    total_amount = calculateTotalWithTax(
                        price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
                    taxable_amount = total_amount['tota_amount']

                    service_charge_amount = str(
                        total_amount['total_commision']) + str(' (')+str(category_commission)+str('% + Tax)')

                    paid_amount = (taxable_amount -
                                   total_amount['total_commision'])

                else:
                    category_commission = round(categorycommission.commission)
                    tcs_rate = TCS
                    tds_rate = TDS
                    tcs_amount = tcs_amount
                    tds_amount = tds_amount

                    service_charge_amount = str(
                        totalamount['total_commisison']) + str(' (')+str(category_commission)+str('% + Tax)')

                    paid_amount = calculateTotal(price=item.price, tax=item.tax, quantity=item.quantity,
                                                 commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=item.product_discount)
                    paid_amount = (
                        paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

                    paid_amount = round(paid_amount, 2)

            # Product
            product = db.query(ProductModel).filter(
                ProductModel.id == item.product_id).first()

            #--------------DEDUCTION----------------------#
            # Check Deduction Amount
            checkDeductionAmount = db.query(AccountsModel).filter(
                AccountsModel.txn_description == str('DEDUCTION (')+order.order_number+str(')')).first()

            deduction_amount = 0
            if(checkDeductionAmount is not None):
                # count order items
                count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                    OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                deduction_amount = (
                    checkDeductionAmount.txn_amount / count_order_items)

            checkAdditionAmount = db.query(AccountsModel).filter(
                AccountsModel.txn_description == str('ADDITION (')+order.order_number+str(')')).first()

            addition_amount = 0

            if(checkAdditionAmount is not None):
                # count order items
                count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                    OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                addition_amount = (
                    checkAdditionAmount.txn_amount / count_order_items)

            #----------------------------------DEDUCTION---------------------------------#
            paid_amount = (
                paid_amount - deduction_amount + addition_amount)
            #--------------------DEDUCTION-----------------------------#
            deduction_remarks = ''
            if(deduction_amount != 0):
                deduction_remarks = 'Wrong Items'

            itm = {
                'order_id': order.id,
                'created_at': order.created_at.strftime("%Y-%m-%d"),
                'reff_no': reff_no,
                'order_number': order.order_number,
                'buyer': shipping_address.ship_to,
                'address': address,
                'state': shipping_address.state,
                'product': product.title,
                'buyer_gst': buyergstIN,
                'taxable_amount': floatingValue(round(taxable_amount, 2)),
                'gst_rate': str(round(item.tax))+str("%"),
                'gst_amount': floatingValue(round(gst_amount, 2)),
                'amount_after_tax': floatingValue(amount_after_tax),
                'service_charge': service_charge_amount,
                'tcs_rate': round(tcs_rate),
                'tcs_amount': floatingValue(round(tcs_amount, 2)),
                'tds_rate': round(tds_rate),
                'tds_amount': floatingValue(round(tds_amount, 2)),
                'status': 'Pending',
                'deduction_amount': floatingValue(deduction_amount),
                'deduction_remarks': deduction_remarks,
                'addition_amount':  floatingValue(addition_amount),
                'you_will_get': floatingValue(paid_amount),
                'paid_amount': floatingValue(0.00)
            }

            item_data.append(itm)
        total_records = data.count()

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1
        return {"status_code": HTTP_200_OK, "items_data": item_data, "page": page, "total_records": total_records, 'download_csv': download_csv, 'commission_pdf': commission_pdf, "total_pages": total_pages}
    except Exception as e:
        print(e)


# Seller Paid Payments List
@seller_payments_router.get("/paid-list", dependencies=[Depends(JWTBearer())])
async def lockedPayments(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:
        userdata = auth(request=request)

        data: SellerPaymentModel = await OrderHelper.getGeneratedOrderCommissionPaidList(user_id=userdata['id'], db=db)
        paid_payment_list = []

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "payment_list": paid_payment_list, "page": page, "total_records": 0, "total_pages": 0}

        payments_list: SellerPaymentModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        for payments in payments_list:

            # Orders
            orders = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(
                OrdersModel.commission_reff == payments.reff_no).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

            total_amount = 0
            # Order
            order = db.query(OrdersModel).filter(
                OrdersModel.id == orders[0].order_id).first()
            if(order.app_version == 'V4'):
                for orderitems in orders:

                    # taxable Amount
                    taxable_amount = calculateTaxableAmount(
                        price=orderitems.price, quantity=orderitems.quantity, discount=orderitems.product_discount)

                    # GST AMOUNt
                    gst_amount = calculateGstAmount(
                        price=taxable_amount, tax=orderitems.tax)

                    # amount after tax
                    amount_after_tax = round(taxable_amount + gst_amount, 2)

                    total_amount += round(amount_after_tax, 2)
                totalamount = round(payments.total_amount, 2) + \
                    round(payments.commission_amount, 2) + \
                    round(payments.tcs_amount, 2) + \
                    round(payments.tds_amount, 2)

                # commission_amount = (totalamount - total_amount)

                commission_amount = (round(payments.commission_amount, 2)
                                     + round(payments.commission_tax_amount, 2))

                seller_amount = (round(total_amount, 2) -
                                 round(payments.commission_amount, 2))
            else:
                for orderitems in orders:

                    # taxable Amount
                    taxable_amount = calculateOldTaxableAmount(
                        price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, discount=orderitems.product_discount)

                    # GST AMOUNt
                    gst_amount = calculateGstAmount(
                        price=taxable_amount, tax=orderitems.tax)

                    # amount after tax
                    amount_after_tax = round(taxable_amount + gst_amount, 2)

                    total_amount += round(amount_after_tax, 2)

                totalamount = round(payments.total_amount, 2) + \
                    round(payments.commission_amount, 2) + \
                    round(payments.tcs_amount, 2) + \
                    round(payments.tds_amount, 2)

                commission_amount = (totalamount - total_amount)

                commission_amount = (round(payments.commission_amount, 2)
                                     - round(commission_amount, 2))

                seller_amount = total_amount - \
                    round(payments.commission_amount, 2)

            if(payments.commission_generated is not None):
                commission_generated = payments.commission_generated
            else:
                commission_generated = ''

            payment = {
                'created_at': payments.commission_date.strftime("%B %d %Y"),
                'reff_no': payments.reff_no,
                'total_orders': payments.total_orders,
                'total_amounts': floatingValue(round(total_amount, 2)),
                'commission_amount': floatingValue(round(commission_amount, 2)),
                'seller_amount': floatingValue(round(seller_amount, 2)),
                'tcs_rate': TCS,
                'tcs_amount': floatingValue(payments.tcs_amount),
                'tds_rate': TCS,
                'tds_amount': floatingValue(payments.tds_amount),
                'seller_will_get': floatingValue(payments.total_amount),
                'payment_date': payments.payment_date.strftime("%B %d %Y"),
                'status': payments.txn_id,
                'commission_generated': commission_generated
            }

            paid_payment_list.append(payment)

        total_records = data.count()

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK, "payment_list": paid_payment_list, "page": page, "total_records": total_records, "total_pages": total_pages}

    except Exception as e:
        print(e)


# Seller Locked Payment Order Items List
@seller_payments_router.get("/paid-order-item-list/{reff_no}", dependencies=[Depends(JWTBearer())])
async def paidOrderItemList(request: Request, reff_no: str, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        data: OrdersModel = await OrderHelper.getGeneratedOrderItemsList(reff_no=reff_no, db=db)
        item_data = []

        download_csv = ''
        commission_pdf = ''

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "items_data": item_data, "page": page, "total_records": 0,  "total_pages": 0}

        reff = db.query(SellerPaymentModel).filter(
            SellerPaymentModel.reff_no == reff_no).first()

        download_csv = reff.payment_csv
        commission_pdf = reff.commission_generated

        order_item_list: OrdersModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        for item in order_item_list:

            # Order
            order = db.query(OrdersModel).filter(
                OrdersModel.id == item.order_id).first()

            # Buyer
            buyer = db.query(UserProfileModel).filter(
                UserProfileModel.user_id == order.user_id).first()

            buyergstIN = ''
            if(buyer.proof_type == 'GSTIN'):
                buyergstIN = buyer.proof

            # shipping_address
            shipping_address = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.user_id == order.user_id).first()

            # Address
            address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

            if(order.app_version == 'V4'):
                # taxable Amount
                taxable_amount = calculateTaxableAmount(
                    price=item.price, quantity=item.quantity, discount=item.product_discount)

                # GST AMOUNt
                gst_amount = calculateGstAmount(
                    price=taxable_amount, tax=item.tax)

                # amount after tax
                amount_after_tax = round(taxable_amount + gst_amount, 2)
                # Service Charge
                service_charge = calculateCommissionSeller(
                    amount=taxable_amount, commission=item.commission_seller)

                service_charge_on_tax = calculateCommissionTaxSeller(
                    amount=service_charge, tax=item.commission_seller_tax)

                total_service_charge = round(
                    service_charge + service_charge_on_tax, 2)

                tcs_rate = round(item.tcs_rate)
                tds_rate = round(item.tds_rate)

                # Tcs Amount
                tcs_amount = calculateSellerTcsAmount(
                    amount=taxable_amount, tax=item.tcs_rate)

                # Tds Amount
                tds_amount = calculateSellerTdsAmount(
                    amount=amount_after_tax, tax=item.tds_rate)

                service_charge_amount = str(total_service_charge) + \
                    ' ('+str(round(item.commission_seller))+'% + Tax)'
                # Paid Amount
                paid_amount = (amount_after_tax -
                               total_service_charge - tcs_amount - tds_amount)

                paid_amount = round(paid_amount, 2)

            else:

                # taxable Amount
                taxable_amount = calculateOldTaxableAmount(
                    price=item.price, tax=item.tax, quantity=item.quantity, discount=item.product_discount)

                # GST AMOUNt
                gst_amount = calculateGstAmount(
                    price=taxable_amount, tax=item.tax)

                # amount after tax
                amount_after_tax = round(taxable_amount + gst_amount, 2)

                orderdate = order.created_at.strftime("%Y-%m-%d")
                # product
                productdata = db.query(ProductModel).filter(
                    ProductModel.id == item.product_id).first()
                categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
                                                                              productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

                totalamount = calculateWithoutTaxAmount(
                    price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=item.product_discount)

                orderTotalamoutwithouttax = totalamount['total_amount']

                tcs_amount = (orderTotalamoutwithouttax *
                              TCS) / 100
                tcs_amount = round(tcs_amount, 2)

                tds_amount = (orderTotalamoutwithouttax * TDS) / 100
                tds_amount = round(tds_amount, 2)

                if(orderdate < '2021-11-09'):
                    category_commission = categorycommission.commission + 5
                    tcs_amount = 0.00
                    tds_amount = 0.00
                    tcs_rate = 0
                    tds_rate = 0
                    total_amount = calculateTotalWithTax(
                        price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
                    taxable_amount = total_amount['tota_amount']

                    service_charge_amount = str(
                        total_amount['total_commision']) + str(' (') + str(category_commission)+str('% + Tax)')

                    paid_amount = (taxable_amount -
                                   total_amount['total_commision'])

                else:
                    category_commission = categorycommission.commission
                    tcs_rate = TCS
                    tds_rate = TDS
                    tcs_amount = tcs_amount
                    tds_amount = tds_amount

                    service_charge_amount = str(
                        totalamount['total_commisison']) + str(' (')+str(category_commission)+str('% + Tax)')

                    paid_amount = calculateTotal(price=item.price, tax=item.tax, quantity=item.quantity,
                                                 commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=item.product_discount)
                    paid_amount = (
                        paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

                    paid_amount = round(paid_amount, 2)

            # Product
            product = db.query(ProductModel).filter(
                ProductModel.id == item.product_id).first()

            #--------------DEDUCTION----------------------#
            # Check Deduction Amount
            checkDeductionAmount = db.query(AccountsModel).filter(
                AccountsModel.txn_description == str('DEDUCTION (')+order.order_number+str(')')).first()

            deduction_amount = 0
            if(checkDeductionAmount is not None):
                # count order items
                count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                    OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                deduction_amount = (
                    checkDeductionAmount.txn_amount / count_order_items)

            checkAdditionAmount = db.query(AccountsModel).filter(
                AccountsModel.txn_description == str('ADDITION (')+order.order_number+str(')')).first()

            addition_amount = 0

            if(checkAdditionAmount is not None):
                # count order items
                count_order_items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                    OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).count()

                addition_amount = (
                    checkAdditionAmount.txn_amount / count_order_items)

            #----------------------------DEDUCTION-------------------------#
            paid_amount = (
                paid_amount - deduction_amount + addition_amount)
            #--------------------DEDUCTION-----------------------------#

            deduction_remarks = ''
            if(deduction_amount != 0):
                deduction_remarks = 'Wrong Items'

            itm = {
                'order_id': order.id,
                'created_at': order.created_at.strftime("%Y-%m-%d"),
                'reff_no': reff_no,
                'order_number': order.order_number,
                'buyer': shipping_address.ship_to,
                'address': address,
                'state': shipping_address.state,
                'product': product.title,
                'buyer_gst': buyergstIN,
                'taxable_amount': floatingValue(round(taxable_amount, 2)),
                'gst_rate': str(round(item.tax))+str("%"),
                'gst_amount': floatingValue(round(gst_amount, 2)),
                'amount_after_tax': floatingValue(amount_after_tax),
                'service_charge': service_charge_amount,
                'tcs_rate': floatingValue(round(tcs_rate)),
                'tcs_amount': floatingValue(round(tcs_amount, 2)),
                'tds_rate': floatingValue(round(tds_rate)),
                'tds_amount': floatingValue(round(tds_amount, 2)),
                'status': reff.txn_id,
                'deduction_amount': floatingValue(deduction_amount),
                'deduction_remarks': deduction_remarks,
                'addition_amount':  floatingValue(addition_amount),
                'you_will_get': floatingValue(0.00),
                'paid_amount': floatingValue(paid_amount)
            }

            item_data.append(itm)

        total_records = data.count()

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK, "items_data": item_data, "download_csv": download_csv, 'commission_pdf': commission_pdf, "page": page, "total_records": total_records,  "total_pages": total_pages}
    except Exception as e:
        print(e)


# Download PAID CSV Date Wise
@seller_payments_router.post("/download/paid/order/csv/date-wise", dependencies=[Depends(JWTBearer())])
async def DownloadPaidCSVOrdersDateWise(request: Request, data: CommissionOrderSchema, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        # Seller

        # Commissions
        orders = db.query(OrdersModel).join(SellerPaymentModel, SellerPaymentModel.reff_no == OrdersModel.commission_reff).filter(SellerPaymentModel.seller_id == userdata['id']).filter(SellerPaymentModel.txn_id.isnot(None)).filter(func.date_format(
            OrdersModel.created_at, '%Y-%m-%d') >= data.from_date).filter(func.date_format(
                OrdersModel.created_at, '%Y-%m-%d') <= data.to_date).group_by(OrdersModel.id).all()

        item_data = []
        for order in orders:

            # Commission
            commission_reff = db.query(SellerPaymentModel).filter(
                SellerPaymentModel.reff_no == order.commission_reff).first()

            # Buyer
            buyer = db.query(UserProfileModel).filter(
                UserProfileModel.user_id == order.user_id).first()

            buyergstIN = ''
            if(buyer.proof_type == 'GSTIN'):
                buyergstIN = buyer.proof

            # shipping_address
            shipping_address = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.user_id == order.user_id).first()

            # Address
            address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

            # Items Data
            items_data = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
                OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

            # Total GST Amount
            total_gst_amount = 0
            # Total Amount After Tax
            total_amount_after_tax = 0
            # Total TCS AMOUNT
            total_tcs_amount = 0
            # Total TDS AMOUNT
            total_tds_amount = 0
            # Total Paid Amount
            total_paid_amount = 0

            # Products
            total_products = ''

            # GST Rates Amount
            gst_rates_amount = []

            # GST RAtes
            gst_rates = []

            # Service Charge Amount
            asez_service_charge_amount = []

            # Service Rates
            asez_service_rates = []

            # Order Date
            orderdate = order.created_at.strftime("%Y-%m-%d")

            if(order.app_version == 'V4'):

                for itemdata in items_data:

                    # Product
                    product = db.query(ProductModel).filter(
                        ProductModel.id == itemdata.product_id).first()

                    # Total Products
                    total_products += str(product.title)+', '

                    # taxable Amount
                    taxable_amount = calculateTaxableAmount(
                        price=itemdata.price, quantity=itemdata.quantity, discount=itemdata.product_discount)

                    # GST RATES
                    gst_rates.append(itemdata.tax)

                    gstrates = {
                        'rate': itemdata.tax,
                        'amount': taxable_amount
                    }
                    gst_rates_amount.append(gstrates)

                    # GST AMOUNt
                    gst_amount = calculateGstAmount(
                        price=taxable_amount, tax=itemdata.tax)

                    # Total GST Amount
                    total_gst_amount += gst_amount

                    # amount after tax
                    amount_after_tax = round(taxable_amount + gst_amount, 2)

                    # Total Amount After Tax
                    total_amount_after_tax += amount_after_tax

                    # Service Charge
                    service_charge = calculateCommissionSeller(
                        amount=taxable_amount, commission=itemdata.commission_seller)

                    service_charge_on_tax = calculateCommissionTaxSeller(
                        amount=service_charge, tax=itemdata.commission_seller_tax)

                    total_service_charge = round(
                        service_charge + service_charge_on_tax, 2)

                    # Total Service Charge AMount
                    # Service RATES
                    asez_service_rates.append(itemdata.commission_seller)

                    aszrates = {
                        'rate': itemdata.commission_seller,
                        'amount': total_service_charge
                    }
                    asez_service_charge_amount.append(aszrates)

                    # Tcs Amount
                    tcs_amount = calculateSellerTcsAmount(
                        amount=taxable_amount, tax=itemdata.tcs_rate)

                    # Total TCS AMOUNT
                    total_tcs_amount += tcs_amount

                    # Tds Amount
                    tds_amount = calculateSellerTdsAmount(
                        amount=amount_after_tax, tax=itemdata.tds_rate)

                    # Total Tds Amount
                    total_tds_amount += tds_amount

                    # Paid Amount
                    paid_amount = (amount_after_tax -
                                   total_service_charge - tcs_amount - tds_amount)

                    paid_amount = round(paid_amount, 2)

                    # Total Paid Amount
                    total_paid_amount += paid_amount

            else:

                for olditemdata in items_data:
                    # Product
                    product = db.query(ProductModel).filter(
                        ProductModel.id == olditemdata.product_id).first()

                    # Total Products
                    total_products += str(product.title)+', '

                    # taxable Amount
                    taxable_amount = calculateOldTaxableAmount(
                        price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity, discount=olditemdata.product_discount)

                    # GST RATES
                    gst_rates.append(olditemdata.tax)

                    gstrates = {
                        'rate': olditemdata.tax,
                        'amount': taxable_amount
                    }
                    gst_rates_amount.append(gstrates)

                    # GST AMOUNt
                    gst_amount = calculateGstAmount(
                        price=taxable_amount, tax=olditemdata.tax)

                    # Total GST Amount
                    total_gst_amount += gst_amount

                    # amount after tax
                    amount_after_tax = round(taxable_amount + gst_amount, 2)

                    # Total AMount after tax
                    total_amount_after_tax += amount_after_tax

                    # product
                    productdata = db.query(ProductModel).filter(
                        ProductModel.id == olditemdata.product_id).first()
                    categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
                                                                                  productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

                    totalamount = calculateWithoutTaxAmount(
                        price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=olditemdata.product_discount)

                    orderTotalamoutwithouttax = totalamount['total_amount']

                    tcs_amount = (orderTotalamoutwithouttax *
                                  TCS) / 100
                    tcs_amount = round(tcs_amount, 2)

                    tds_amount = (orderTotalamoutwithouttax * TDS) / 100
                    tds_amount = round(tds_amount, 2)

                    if(orderdate < '2021-11-09'):
                        pass

                    else:
                        category_commission = categorycommission.commission

                        tcs_amount = tcs_amount
                        # Total TCS Amount
                        total_tcs_amount += tcs_amount

                        tds_amount = tds_amount
                        # Total Tds Amount
                        total_tds_amount += tds_amount

                        # Total Service Charge AMount
                        # Service RATES
                        asez_service_rates.append(category_commission)

                        aszrates = {
                            'rate': category_commission,
                            'amount': round(
                                totalamount['total_commisison'], 2)
                        }
                        asez_service_charge_amount.append(aszrates)

                        paid_amount = calculateTotal(price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity,
                                                     commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=olditemdata.product_discount)
                        paid_amount = (
                            paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

                        paid_amount = round(paid_amount, 2)

                        # Total Paid Amount
                        total_paid_amount += paid_amount

            # Customize GST RATES
            gst_rates = set(gst_rates)
            final_gst_rates = []
            amount_before_tax = 0
            for g in gst_rates:
                gstam = 0
                for gm in gst_rates_amount:
                    if(gm['rate'] == g):
                        gstam += gm['amount']
                        amount_before_tax += gm['amount']
                gs = {
                    'GST Rate '+str(int(g))+'%': gstam
                }

                final_gst_rates.append(gs)

            second_obj = {}
            for obj in final_gst_rates:
                second_obj.update(obj)

            # Customize Service Rates
            asez_service_rates = set(asez_service_rates)
            final_asez_rates = []
            for s in asez_service_rates:
                aszam = 0
                for am in asez_service_charge_amount:
                    if(am['rate'] == s):
                        aszam += am['amount']

                asm = {
                    'Service Charge': str(round(aszam, 2)) + '  ('+str(s)+'% + Tax)'
                }
                final_asez_rates.append(asm)

            fourth_obj = {}
            for obj4 in final_asez_rates:
                fourth_obj.update(obj4)

            merged = dict()
            firstObject = {
                'Created At': order.created_at.strftime("%Y-%m-%d"),
                'Reff No': commission_reff.reff_no,
                'Order Number': order.order_number,
                'Buyer': shipping_address.ship_to,
                'Address': address,
                'State': shipping_address.state,
                'Product': total_products,
                'Buyer GST': buyergstIN,
            }

            merged.update(firstObject)
            merged.update(second_obj)

            thirdobject = {
                'Amount Before Tax': amount_before_tax,
                'Tax Amount': floatingValue(round(total_gst_amount, 2)),
                'Amount After Tax': floatingValue(total_amount_after_tax),
            }

            merged.update(thirdobject)

            merged.update(fourth_obj)

            lastobject = {
                'Tcs Amount ('+str(TCS)+'%)': floatingValue(round(total_tcs_amount, 2)),
                'Tds Amount ('+str(TDS)+'%)': floatingValue(round(total_tds_amount, 2)),
                'Status': commission_reff.txn_id,
                'Paid Amount': floatingValue(total_paid_amount)
            }
            merged.update(lastobject)

            item_data.append(merged)

        item_data.sort(key=len, reverse=False)

        marks_data = pd.DataFrame(item_data)

        statement = 'payments-csv-' + \
            str(data.from_date)+str(data.to_date) + \
            str('-')+str(userdata['id'])+str('.csv')

        # saving the csv
        uploaded_file = getcwd()+"/app/static/paid_payments/"+str(statement)
        marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

        # Delete File From Account Transaction
        MediaHelper.deleteFile(
            bucket_name='paid_payments', file_name=statement)

        # Upload FILE
        MediaHelper.uploadPaidPaymentsCsv(
            uploaded_file=uploaded_file, file_name=statement)

        # Remove file from Path
        unlink(uploaded_file)

        file_path = str(
            linode_obj_config['endpoint_url'])+'/paid_payments/'+str(statement)

        return {"status_code": HTTP_200_OK,  "file": file_path}

        # return file_path
    except Exception as e:
        print(e)


# Seller Export Locked Payments
@seller_payments_router.get("/export-locked-payments/{reff_no}", dependencies=[Depends(JWTBearer())])
async def exportLockedPayments(request: Request, reff_no: str, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        data: OrdersModel = await OrderHelper.getGeneratedOrderItemsList(reff_no=reff_no, db=db)
        item_data = []

        order_item_list: OrdersModel = data.all()

        for item in order_item_list:

            # Order
            order = db.query(OrdersModel).filter(
                OrdersModel.id == item.order_id).first()

            # Buyer
            buyer = db.query(UserProfileModel).filter(
                UserProfileModel.user_id == order.user_id).first()

            buyergstIN = ''
            if(buyer.proof_type == 'GSTIN'):
                buyergstIN = buyer.proof

            # shipping_address
            shipping_address = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.user_id == order.user_id).first()

            # Address
            address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

            if(order.app_version == 'V4'):
                # taxable Amount
                taxable_amount = calculateTaxableAmount(
                    price=item.price, quantity=item.quantity, discount=item.product_discount)

                # GST AMOUNt
                gst_amount = calculateGstAmount(
                    price=taxable_amount, tax=item.tax)

                # amount after tax
                amount_after_tax = round(taxable_amount + gst_amount, 2)
                # Service Charge

                service_charge = calculateCommissionSeller(
                    amount=taxable_amount, commission=item.commission_seller)

                service_charge_on_tax = calculateCommissionTaxSeller(
                    amount=service_charge, tax=item.commission_seller_tax)

                total_service_charge = round(
                    service_charge + service_charge_on_tax, 2)

                tcs_rate = round(item.tcs_rate)
                tds_rate = round(item.tds_rate)

                # Tcs Amount
                tcs_amount = calculateSellerTcsAmount(
                    amount=taxable_amount, tax=item.tcs_rate)

                # Tds Amount
                tds_amount = calculateSellerTdsAmount(
                    amount=amount_after_tax, tax=item.tds_rate)

                service_charge_amount = str(total_service_charge) + \
                    ' ('+str(round(item.commission_seller))+'% + Tax)'
                # Paid Amount
                paid_amount = (amount_after_tax -
                               total_service_charge - tcs_amount - tds_amount)

                paid_amount = round(paid_amount, 2)

            else:
                # taxable Amount
                taxable_amount = calculateOldTaxableAmount(
                    price=item.price, tax=item.tax, quantity=item.quantity, discount=item.product_discount)

                # GST AMOUNt
                gst_amount = calculateGstAmount(
                    price=taxable_amount, tax=item.tax)

                # amount after tax
                amount_after_tax = round(taxable_amount + gst_amount, 2)

                orderdate = order.created_at.strftime("%Y-%m-%d")
                # product
                productdata = db.query(ProductModel).filter(
                    ProductModel.id == item.product_id).first()
                categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
                                                                              productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

                totalamount = calculateWithoutTaxAmount(
                    price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=item.product_discount)

                orderTotalamoutwithouttax = totalamount['total_amount']

                tcs_amount = (orderTotalamoutwithouttax *
                              TCS) / 100
                tcs_amount = round(tcs_amount, 2)

                tds_amount = (orderTotalamoutwithouttax * TDS) / 100
                tds_amount = round(tds_amount, 2)

                if(orderdate < '2021-11-09'):
                    category_commission = categorycommission.commission + 5
                    tcs_amount = 0.00
                    tds_amount = 0.00
                    tcs_rate = 0
                    tds_rate = 0
                    total_amount = calculateTotalWithTax(
                        price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
                    taxable_amount = total_amount['tota_amount']

                    service_charge_amount = str(
                        round(total_amount['total_commision'], 2)) + str(' (') + str(category_commission)+'% + Tax)'

                    paid_amount = (taxable_amount -
                                   total_amount['total_commision'])

                else:
                    category_commission = categorycommission.commission
                    tcs_rate = TCS
                    tds_rate = TDS
                    tcs_amount = tcs_amount
                    tds_amount = tds_amount

                    service_charge_amount = str(
                        round(totalamount['total_commisison'], 2)) + str(' (')+str(category_commission)+str('% + Tax)')

                    paid_amount = calculateTotal(price=item.price, tax=item.tax, quantity=item.quantity,
                                                 commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=item.product_discount)
                    paid_amount = (
                        paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

                    paid_amount = round(paid_amount, 2)

            # Product
            product = db.query(ProductModel).filter(
                ProductModel.id == item.product_id).first()

            itm = {
                'Created At': order.created_at.strftime("%Y-%m-%d"),
                'Reff No': reff_no,
                'Order Number': order.order_number,
                'Buyer': shipping_address.ship_to,
                'Address': address,
                'State': shipping_address.state,
                'Product': product.title,
                'Buyer GST': buyergstIN,
                'Taxable Amount': floatingValue(round(taxable_amount, 2)),
                'GST Rate': str(round(item.tax))+str("%"),
                'Gst Amount': floatingValue(round(gst_amount, 2)),
                'Amount After Tax': floatingValue(amount_after_tax),
                'Service Charge': service_charge_amount,
                'Tcs Rate': round(tcs_rate),
                'Tcs Amount': floatingValue(round(tcs_amount, 2)),
                'Tds Rate': round(tds_rate),
                'Tds Amount': floatingValue(round(tds_amount, 2)),
                'Status': 'Pending',
                'You Will Get': floatingValue(paid_amount)
            }

            item_data.append(itm)

        reff = db.query(SellerPaymentModel).filter(
            SellerPaymentModel.reff_no == reff_no).first()

        if(reff.commission_generated is None):

            marks_data = pd.DataFrame(item_data)

            statement = 'locked-payments-' + \
                str(reff_no)+str('-')+str(userdata['id'])+str('.csv')
            #         # saving the csv
            uploaded_file = getcwd()+"/app/static/locked_payments/"+str(statement)
            marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

            # Delete File From Account Transaction
            MediaHelper.deleteFile(
                bucket_name='locked_payments', file_name=statement)

            # Upload FILE
            MediaHelper.uploadLockedPaymentsCsv(
                uploaded_file=uploaded_file, file_name=statement)

            # Remove file from Path
            unlink(uploaded_file)

            file_path = str(
                linode_obj_config['endpoint_url'])+'/locked_payments/'+str(statement)

            reff.commission_generated = file_path
            db.flush()
            db.commit()
        else:
            file_path = reff.commission_generated

        return {"file": file_path}
    except Exception as e:
        print(e)


# Seller Paid Payment Order Items List
@seller_payments_router.get("/export-paid-payments/{reff_no}", dependencies=[Depends(JWTBearer())])
async def exportPaidPayments(request: Request, reff_no: str, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:
        userdata = auth(request=request)

        data: OrdersModel = await OrderHelper.getGeneratedOrderItemsList(reff_no=reff_no, db=db)
        item_data = []

        reff = db.query(SellerPaymentModel).filter(
            SellerPaymentModel.reff_no == reff_no).first()

        order_item_list: OrdersModel = data.all()

        for item in order_item_list:

            # Order
            order = db.query(OrdersModel).filter(
                OrdersModel.id == item.order_id).first()

            # Buyer
            buyer = db.query(UserProfileModel).filter(
                UserProfileModel.user_id == order.user_id).first()

            buyergstIN = ''
            if(buyer.proof_type == 'GSTIN'):
                buyergstIN = buyer.proof

            # shipping_address
            shipping_address = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.user_id == order.user_id).first()

            # Address
            address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

            if(order.app_version == 'V4'):
                # taxable Amount
                taxable_amount = calculateTaxableAmount(
                    price=item.price, quantity=item.quantity, discount=item.product_discount)

                # GST AMOUNt
                gst_amount = calculateGstAmount(
                    price=taxable_amount, tax=item.tax)

                # amount after tax
                amount_after_tax = round(taxable_amount + gst_amount, 2)

                # Service Charge
                service_charge = calculateCommissionSeller(
                    amount=taxable_amount, commission=item.commission_seller)

                service_charge_on_tax = calculateCommissionTaxSeller(
                    amount=service_charge, tax=item.commission_seller_tax)

                total_service_charge = round(
                    service_charge + service_charge_on_tax, 2)

                tcs_rate = round(item.tcs_rate)
                tds_rate = round(item.tds_rate)

                # Tcs Amount
                tcs_amount = calculateSellerTcsAmount(
                    amount=taxable_amount, tax=item.tcs_rate)

                # Tds Amount
                tds_amount = calculateSellerTdsAmount(
                    amount=amount_after_tax, tax=item.tds_rate)

                service_charge_amount = str(total_service_charge) + \
                    ' ('+str(round(item.commission_seller))+'% + Tax)'
                # Paid Amount
                paid_amount = (amount_after_tax -
                               total_service_charge - tcs_amount - tds_amount)

                paid_amount = round(paid_amount, 2)

            else:
                # taxable Amount
                taxable_amount = calculateOldTaxableAmount(
                    price=item.price, tax=item.tax, quantity=item.quantity, discount=item.product_discount)

                # GST AMOUNt
                gst_amount = calculateGstAmount(
                    price=taxable_amount, tax=item.tax)

                # amount after tax
                amount_after_tax = round(taxable_amount + gst_amount, 2)

                orderdate = order.created_at.strftime("%Y-%m-%d")

                # product
                productdata = db.query(ProductModel).filter(
                    ProductModel.id == item.product_id).first()
                categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
                                                                              productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

                totalamount = calculateWithoutTaxAmount(
                    price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=item.product_discount)

                orderTotalamoutwithouttax = totalamount['total_amount']

                tcs_amount = (orderTotalamoutwithouttax *
                              TCS) / 100
                tcs_amount = round(tcs_amount, 2)

                tds_amount = (orderTotalamoutwithouttax * TDS) / 100
                tds_amount = round(tds_amount, 2)

                if(orderdate < '2021-11-09'):
                    category_commission = categorycommission.commission + 5
                    tcs_amount = 0.00
                    tds_amount = 0.00
                    tcs_rate = 0
                    tds_rate = 0
                    total_amount = calculateTotalWithTax(
                        price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
                    taxable_amount = total_amount['tota_amount']

                    service_charge_amount = str(
                        round(total_amount['total_commision'], 2)) + str(' (') + str(category_commission)+'% + Tax)'

                    paid_amount = (taxable_amount -
                                   total_amount['total_commision'])

                else:
                    category_commission = categorycommission.commission
                    tcs_rate = TCS
                    tds_rate = TDS
                    tcs_amount = tcs_amount
                    tds_amount = tds_amount

                    service_charge_amount = str(
                        round(totalamount['total_commisison'], 2)) + str(' (')+str(category_commission)+str('% + Tax)')

                    paid_amount = calculateTotal(price=item.price, tax=item.tax, quantity=item.quantity,
                                                 commission=categorycommission.commission, commission_tax=categorycommission.commission_tax, discount=item.product_discount)
                    paid_amount = (
                        paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

                    paid_amount = round(paid_amount, 2)

            # Product
            product = db.query(ProductModel).filter(
                ProductModel.id == item.product_id).first()

            itm = {
                'Created At': order.created_at.strftime("%Y-%m-%d"),
                'Reff No': reff_no,
                'Order Number': order.order_number,
                'Buyer': shipping_address.ship_to,
                'Address': address,
                'State': shipping_address.state,
                'Product': product.title,
                'Buyer GST': buyergstIN,
                'Taxable Amount': floatingValue(round(taxable_amount, 2)),
                'Gst Rate': str(round(item.tax))+str("%"),
                'Gst Amount': floatingValue(round(gst_amount, 2)),
                'Amount After Tax': floatingValue(amount_after_tax),
                'Service Charge': service_charge_amount,
                'Tcs Rate': round(tcs_rate),
                'Tcs Amount': floatingValue(round(tcs_amount, 2)),
                'Tds Rate': round(tds_rate),
                'Tds Amount': floatingValue(round(tds_amount, 2)),
                'Status': reff.txn_id,
                'Paid Amount': floatingValue(paid_amount)
            }

            item_data.append(itm)

        if(reff.commission_generated is None):
            marks_data = pd.DataFrame(item_data)

            statement = 'paid-payments-' + \
                str(reff_no)+str('-')+str(userdata['id'])+str('.csv')
            #             # saving the csv
            uploaded_file = getcwd()+"/app/static/paid_payments/"+str(statement)
            marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

            # Delete File From Account Transaction
            MediaHelper.deleteFile(
                bucket_name='paid_payments', file_name=statement)

            # Upload FILE
            MediaHelper.uploadPaidPaymentsCsv(
                uploaded_file=uploaded_file, file_name=statement)

            # Remove file from Path
            unlink(uploaded_file)

            file_path = str(
                linode_obj_config['endpoint_url'])+'/paid_payments/'+str(statement)

            reff.commission_generated = file_path
            db.flush()
            db.commit()
        else:
            file_path = reff.commission_generated

        return {"file": file_path}
    except Exception as e:
        print(e)
