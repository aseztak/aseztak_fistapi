from fastapi import APIRouter

from fastapi import APIRouter
seller_router = APIRouter()


# # Update All User bank Account details
# @seller_router.get("/user/bank/account/details")
# async def userbankAccountDetails(db: Session = Depends(get_db)):
#     try:

#         users = db.query(UserModel).filter(
#             UserModel.id.between(20001, 22000)).all()
#         # profile = db.query(UserProfileModel).filter(
#         #     UserProfileModel.user_id == 524).first()

#         # if(profile.account_type is ""):
#         #     return "asd"
#         # else:
#         #     return "d"
#         for user in users:
#             profile = db.query(UserProfileModel).filter(
#                 UserProfileModel.user_id == user.id).first()
#             if(profile is not None):
#                 if(profile.acc_no is not None):
#                     if(profile.bank is None):
#                         profile.bank = ''
#                     if(profile.branch is None):
#                         profile.branch = ''

#                     if(profile.ifsc_code is None):
#                         profile.ifsc_code = ''

#                     if(profile.account_holder_name is None):
#                         profile.account_holder_name = ''

#                     if(profile.account_type is None or profile.account_type is ""):
#                         profile.account_type = 'savings'

#                     db_bank_account = BankAccountsModel(
#                         user_id=user.id,
#                         account_holder_name=profile.account_holder_name,
#                         account_type=profile.account_type,
#                         bank=profile.bank,
#                         branch=profile.branch,
#                         acc_no=profile.acc_no,
#                         ifsc_code=profile.ifsc_code
#                     )

#                     db.add(db_bank_account)
#                     db.commit()
#                     db.refresh(db_bank_account)

#         return "COMPLETED"

#     except Exception as e:
#         print(e)

# Upload Seller CSV


# @seller_router.get("/upload/seller/paymnet/csv")
# async def uploadBrands(db: Session = Depends(get_db)):
#     folders = os.listdir("app/static/paid_payments")
#     # Brands Uploaded IN OBJECT STORAGE
#     try:
#         for files in folders:

#             commission_generate = files.split('.')

#             # client.delete_object(
#             #     Bucket='brands',
#             #     Key=files)
#             img = getcwd()+"/app/static/paid_payments/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='paid_payments',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#             # Check Cmmission
#             check_commission = db.query(SellerPaymentModel).filter(
#                 SellerPaymentModel.reff_no == commission_generate[0]).filter(SellerPaymentModel.txn_id.isnot(None)).first()

#             if(check_commission is not None):
#                 key = str('paid_payments/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 check_commission.payment_csv = url
#                 db.flush()
#                 db.commit()

#         return "CSV UPDATED"
#     except Exception as e:
#         return "FAILED"

#     # Brands uploaded in database
#     try:
#         for files in folders:
#             brand = db.query(BrandsModel).filter(
#                 BrandsModel.logo == str("brands/")+str(files)).first()

#             if(brand is not None):
#                 key = str('brands/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 brand.logo = url
#                 db.flush()
#                 db.commit()

#         return "BRAND SUCCESS"
#     except Exception as e:
#         return "FAILED"

# Pincode Converter
# @seller_router.get("/pincode")
# async def getPincode(db: Session = Depends(get_db)):
#     try:
#         pincodes = getcwd()+"/app/static/pincodes.json"

#         # Opening JSON file
#         f = open(pincodes)

#         # returns JSON object as
#         # a dictionary
#         data = json.load(f)

#         # Iterating through the json
#         # list
#         for i in data:
#             db_pincode = PincodeModel(
#                 officename=i['officeName'],
#                 pincode=i['pincode'],
#                 taluk=i['taluk'],
#                 districtname=i['districtName'],
#                 statename=i['stateName']
#             )
#             db.add(db_pincode)
#             db.commit()
#             db.refresh(db_pincode)

#         return "SUCCESS FULLy ADDED"
#     except Exception as e:
#         return "FAILED"

# @seller_router.get("/accounts")
# async def sellerAccounts(db: Session = Depends(get_db)):
# try:

#     search = "%{}%".format('RETURNED')
#     ordered_amounts = db.query(AccountsModel).filter(
#         AccountsModel.txn_description.ilike(search)).filter(AccountsModel.user_id == 65).all()

#     if(len(ordered_amounts) > 0):
#         for amount in ordered_amounts:
#             order_number = amount.txn_description.replace(
#                 "RETURNED (", "")
#             order_number = order_number.replace(")", "")

#             order = db.query(OrdersModel).filter(
#                 OrdersModel.order_number == order_number).first()

#             if(order is not None):

#                 # items = db.query(OrderItemsModel).filter(
#                 #     OrderItemsModel.order_id == order.id).all()
#                 # CANCELLED
#                 # items = db.query(OrderItemsModel).filter(
#                 #     OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 980).all()

#                 # REVERSED
#                 # items = db.query(OrderItemsModel).filter(
#                 #     OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 61).all()

#                 # RETURNED
#                 items = db.query(OrderItemsModel).filter(
#                     OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status.between(90, 110)).all()

#                 total_amount = 0
#                 for item in items:

#                     checkprice = (item.price * item.tax) / 100 + item.price
#                     checkprice = round(checkprice) * item.quantity

#                     if(float(checkprice) == float(amount.txn_amount)):

#                         price = (item.price * item.tax) / 100 + item.price
#                         price = (round(price, 0) * 100) / (100 + item.tax)
#                         total_amount += round((price * item.quantity), 2)

#                         amount.txn_amount = total_amount
#                         db.flush()
#                         db.commit()

#     print("SUCCESS")
#     return "SUCCESS"

# except Exception as e:
#     print(e)


# Seller order items
# @seller_router.get("/order/items")
# async def sellerOrderItems(db: Session = Depends(get_db)):
#     try:
#         items = db.query(OrderItemsModel).filter(OrderItemsModel.id.between(1, 1999)).filter(
#             OrderItemsModel.status != 980).all()

#         if(len(items) > 0):
#             for item in items:

#                 product = db.query(ProductModel).filter(
#                     ProductModel.id == item.product_id).first()

#                 date = item.created_at.strftime("%Y-%m-%d")

#                 returnDays = db.query(CategoryReturnDaysModel).filter(
#                     CategoryReturnDaysModel.category_id == product.category).filter(CategoryReturnDaysModel.start_date <= date).order_by(CategoryReturnDaysModel.start_date.desc()).first()

#                 item.return_expiry = returnDays.return_days
#                 db.flush()
#                 db.commit()

#             return "SUCCESS"

#     except Exception as e:
#         print(e)


# @seller_router.get("/seller/payments")
# async def sellerPayments(db: Session = Depends(get_db)):
#     try:

#         payements_of_orders = db.query(SellerPaymentModel).filter(SellerPaymentModel.seller_id == 2023).filter(
#             SellerPaymentModel.txn_id != '').all()

#         for payment in payements_of_orders:
#             dbaccounts = AccountsModel(
#                 user_id=2023,
#                 txn_description=str('PAYMENT (')+payment.reff_no+str(')'),
#                 txn_amount=payment.total_amount,
#                 txn_date=payment.payment_date,
#                 txn_type='CR',
#                 created_at=payment.payment_date,
#                 updated_at=payment.payment_date
#             )
#             db.add(dbaccounts)
#             db.commit()
#             db.refresh(dbaccounts)

#         return "SUCCESS"
#     except Exception as e:
#         print(e)


# def checkCategoryCommission(order_id: int, db: Session):

#     items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order_id).filter(
#         OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#     order = db.query(OrdersModel).filter(OrdersModel.id == order_id).first()
#     final_commission = []
#     for item in items:
#         product = db.query(ProductModel).filter(
#             ProductModel.id == item.product_id).first()
#         category = db.query(CategoriesModel).filter(
#             CategoriesModel.id == product.category).first()

#         orderdate = order.created_at.strftime("%Y-%m-%d")

#         categoryCommission = db.query(CategoryCommissionModel).filter(
#             CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.id.desc()).first()

#         final_commission.append(categoryCommission.commission)

#     final = {
#         order.id,
#         final_commission[0]
#     }
#     return final


# def calculateTotalWithTax(order_id: int, db: Session):
#     try:
#         total_amount = 0

#         items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order_id).filter(
#             OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#         order = db.query(OrdersModel).filter(
#             OrdersModel.id == order_id).first()

#         final_amount = 0

#         for item in items:
#             product = db.query(ProductModel).filter(
#                 ProductModel.id == item.product_id).first()

#             # Get Commission
#             price = (item.price * item.tax) / 100 + item.price
#             price = (round(price) * 100) / (100 + item.tax)
#             price = round(price, 2)

#             price = price + (price * item.tax / 100)
#             price = round(price) * item.quantity

#             orderdate = order.created_at.strftime("%Y-%m-%d")

#             categoryCommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id == product.category).filter(
#                 CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.id.desc()).first()

#             commission = (price * categoryCommission.commission) / 100

#             total_commission = (commission * 18) / 100

#             marketplace = (price * 5) / 100
#             marketplace_fee = (marketplace * 18) / 100
#             thismarketplace = marketplace + marketplace_fee

#             thiscommission = commission + total_commission

#             final_amount += price - thiscommission - thismarketplace

#             price = (item.price * item.tax) / 100 + item.price
#             price = (round(price) * 100) / (100 + item.tax)
#             price = round(price, 2)

#             price = price + (price * item.tax / 100)
#             total_amount += round(price) * item.quantity

#             total_commision = total_amount - final_amount

#         return {"total_amoun": total_amount, "total_commission":  total_commision}

#     except Exception as e:
#         print(e)


# def calculateTotalWithoutTax(order_id: int, db: Session):
#     try:
#         total_amount = 0

#         items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order_id).filter(
#             OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#         order = db.query(OrdersModel).filter(
#             OrdersModel.id == order_id).first()

#         final_amount = 0

#         for item in items:
#             product = db.query(ProductModel).filter(
#                 ProductModel.id == item.product_id).first()

#             # Get Commission
#             price = (item.price * item.tax) / 100 + item.price
#             price = (round(price) * 100) / (100 + item.tax)
#             price = round((price * item.quantity), 2)

#             orderdate = order.created_at.strftime("%Y-%m-%d")

#             categoryCommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id == product.category).filter(
#                 CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.id.desc()).first()

#             commission = (price * categoryCommission.commission) / 100

#             total_commission = (commission * 18) / 100

#             thiscommission = commission + total_commission

#             final_amount += price - thiscommission

#             price = (item.price * item.tax) / 100 + item.price
#             price = (round(price) * 100) / (100 + item.tax)
#             total_amount += round((price * item.quantity), 2)

#             total_commision = total_amount - final_amount

#         return {"total_amoun": total_amount, "total_commission":  total_commision}

#     except Exception as e:
#         print(e)


# def calculateTotal(order_id: int, db: Session):
#     try:
#         total_amount = 0

#         items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order_id).filter(
#             OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#         order = db.query(OrdersModel).filter(
#             OrdersModel.id == order_id).first()

#         final_amount = 0

#         for item in items:
#             product = db.query(ProductModel).filter(
#                 ProductModel.id == item.product_id).first()

#             # Get Commission
#             price = (item.price * item.tax) / 100 + item.price
#             price = (round(price, 0) * 100) / (100 + item.tax)
#             price = round(price, 2)

#             price = price + (price * item.tax / 100)
#             price = round(price, 0) * item.quantity

#             orderdate = order.created_at.strftime("%Y-%m-%d")

#             categoryCommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id == product.category).filter(
#                 CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.id.desc()).first()

#             commission = (price * categoryCommission.commission) / 100

#             total_commission = (commission * 18) / 100

#             thiscommission = commission + total_commission

#             final_amount += price - thiscommission

#             price = (item.price * item.tax) / 100 + item.price
#             price = (round(price, 0) * 100) / (100 + item.tax)
#             price = round(price, 2)

#             price = price + (price * item.tax / 100)

#             total_amount += round(price, 0) * item.quantity

#             total_commision = total_amount - final_amount

#         return {"total_amoun": total_amount, "total_commission":  total_commision}

#     except Exception as e:
#         print(e)
# Upload Widget Image
# @seller_router.post("/upload/widget")
# async def uploadWidget(title: str = Form(...), description: str = Form(...), no_of_images: int = Form(...), images: List[UploadFile] = File(...), db: Session = Depends(get_db)):
#     try:

#         dbwidget = WidgetsModel(
#             title=title,
#             description=description,
#             no_of_images=no_of_images,
#             created_at=datetime.now(),
#             updated_at=datetime.now()
#         )
#         db.add(dbwidget)
#         db.commit()
#         db.refresh(dbwidget)

#         for image in images:
#             # Rename Image
#             file = image.filename.split(".")
#             uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
#                 datetime.now().strftime('%Y-%d-%M-%h-%s')

#             filename = f"widget_banner_{uniqueID}.{file[1]}"

#             # Set Path Of image
#             file_name = getcwd()+"/app/static/widgets/"+filename

#             # Upload image in path
#             with open(file_name, 'wb+') as f:
#                 f.write(image.file.read())
#                 f.close()

#             uploaded_file = getcwd()+"/app/static/widgets/"+str(filename)

#             # Upload Photo
#             MediaHelper.uploadPhoto(
#                 uploaded_file=uploaded_file, file_name=filename)

#             file_path = str(
#                 linode_obj_config['endpoint_url'])+'/widgets/'+str(filename)

#             dbwidgetimage = WidgetImagesModel(
#                 widget_id=dbwidget.id,
#                 image=file_path,
#                 created_at=datetime.now(),
#                 updated_at=datetime.now()
#             )

#             db.add(dbwidgetimage)
#             db.commit()
#             db.refresh(dbwidgetimage)

#         return "Success"

#     except Exception as e:
#         print(e)

# @seller_router.get("/update/banner")
# async def banners(db: Session = Depends(get_db)):
#     try:

#         banners = db.query(BannersModel).filter(BannersModel.status == 1).all()

#         for banner in banners:

#             uploaded_file = getcwd()+"/app/static/banners/"+str(banner.banner)

#             # Upload Photo
#             MediaHelper.uploadPhoto(
#                 uploaded_file=uploaded_file, file_name=banner.banner)

#             file_path = str(
#                 linode_obj_config['endpoint_url'])+'/banners/'+str(banner.banner)

#             banner.banner = file_path
#             db.flush()
#             db.commit()

#         return "SUCCESS"

#     except Exception as e:
#         print(e)


# @seller_router.get("/test")
# async def test(db: Session = Depends(get_db)):
#     folders = os.listdir("app/static/categories")
#     try:
#         for files in folders:
#             category = db.query(CategoriesModel).filter(
#                 CategoriesModel.image == str("categories/")+str(files)).first()
#             if(category is not None):
#                 key = str('categories/')+str(files)
#                 url = str(linode_obj_config['endpoint_url'])+'/'+str(key)
#                 category.image_path = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"


# # Proof
# @seller_router.get("/test")
# async def test(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/proof")

#     try:
#         for files in folders:

#             # client.delete_object(
#             #     Bucket='banners',
#             #     Key=files)
#             img = getcwd()+"/app/static/proof/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='proof',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#         return "PROOF UDATED"
#     except Exception as e:
#         return "FAILED"


# Hero Banners
# @seller_router.get("/test")
# async def test(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/hero")

# try:
#     for files in folders:
#         banner = db.query(BannersModel).filter(
#             BannersModel.banner == str("hero/")+str(files)).first()

#         if(banner is not None):
#             key = str('banners/')+str(files)
#             url = str(
#                 linode_obj_config['endpoint_url'])+'/'+str(key)
#             banner.banner = url
#             db.flush()
#             db.commit()

#     return "SUCCESS"
# except Exception as e:
# return "FAILED"
# try:
#     for files in folders:

#         # client.delete_object(
#         #     Bucket='banners',
#         #     Key=files)
#         img = getcwd()+"/app/static/hero/"+str(files)
#         client.upload_file(
#             Filename=img,
#             Bucket='banners',
#             Key=files,
#             ExtraArgs={'ACL': 'public-read'})

#     return "BANNERS UDATED"
# except Exception as e:
#     return "FAILED"


# Brands
# @seller_router.get("/test")
# async def test(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/brands")

#     try:
#         for files in folders:
#             brand = db.query(BrandsModel).filter(
#                 BrandsModel.logo == str("brands/")+str(files)).first()

#             if(brand is not None):
#                 key = str('brands/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 brand.logo = url
#                 db.flush()
#                 db.commit()

#         return "BRAND SUCCESS"
#     except Exception as e:
#         return "FAILED"

#     try:
#         for files in folders:

#             # client.delete_object(
#             #     Bucket='brands',
#             #     Key=files)
#             img = getcwd()+"/app/static/brands/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='brands',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#         return "BRANDS UPDATED"
#     except Exception as e:
#         return "FAILED"


# Delete Trash Data
# @seller_router.get("/delete")
# async def delete(db: Session = Depends(get_db)):
#     media = db.query(ProductMediaModel).all()

#     images = []
#     for image in media:

#         product = db.query(ProductModel).filter(
#             ProductModel.id == image.model_id).first()

#         if(product is None):
#             db.query(ProductMediaModel).filter(
#                 ProductMediaModel.model_id == image.model_id).delete()

#             db.commit()

#     return images

# UPLOAD ALL PRODUCTS IMAGES
# # test
# @seller_router.get("/upload/files")
# async def test(db: Session = Depends(get_db)):

#     try:

#         media = db.query(ProductMediaModel).all()
#         for image in media:


        # media = db.query(ProductMediaModel).filter(ProductMediaModel.file_path.is_(None)).filter(ProductMediaModel.deleted_at.is_(None)).all()
        
        # for image in media:

#             product = db.query(ProductModel).filter(
#                 ProductModel.id == image.model_id).first()


#             if(product):
#                 if(image.custom_properties):

#                     checkthumnail = image.custom_properties
#                     checkthumnail = json.loads(checkthumnail)
#                     if(checkthumnail['generated_conversions']['thumbnail'] == True):
#                         file = image.file_name.rsplit(".", 1)
#                         if(file[1] == 'jpeg'):
#                             f = 'jpg'
#                         else:
#                             f = file[1]
#                         filename = str(file[0]) + str('-thumbnail')+str('.'+f)

#                         checkfileexist = os.path.exists(
#                             'app/static/products/'+filename)

#                         if(checkfileexist == False):
#                             rename = filename.rsplit(".", 1)
#                             filename = str(rename[0]) + str('.jpg')

#                 else:

#                     filename = image.file_name

#                 checkfile = os.path.exists(
#                     'app/static/products/'+filename)

#                 if(checkfile == True):
#                     img = getcwd()+"/app/static/products/"+str(filename)

#                 client.upload_file(
#                     Filename=img,
#                     Bucket='products',
#                     Key=filename,
#                     ExtraArgs={'ACL': 'public-read'})

#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/products/'+str(filename)
#                 image.file_path = url
#                 db.flush()
#                 db.commit()


#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/products/'+str(filename)
#                 image.file_path = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"

# # Transfer All Images to products folder from public folder


# @seller_router.get("/transfer/files")
# async def test(db: Session = Depends(get_db)):
    
#     # filename = os.getcwd()+"/app/static/today_backup_01/app/public/29340/"
#     # return filename
                
#     folders = os.listdir("app/static/today_backup_01/app/public")
#     # 29340
   
#     try:
#         ff = []
#         for folder in folders:
            
#             checkfolder = os.path.exists("app/static/today_backup_01/app/public/" +
#                                str(folder)+"/conversions")                    

#             if(checkfolder == True):
#                 files = os.listdir("app/static/today_backup_01/app/public/" +
#                                str(folder)+"/conversions")

#                 filename = os.getcwd()+"/app/static/today_backup_01/app/public/"+str(folder) + \
#                     "/conversions/"+str(files[0])
#                 ff.append(filename)
#                 dst = os.getcwd()+"/app/static/products"
#                 shutil.copy(filename, dst)
                
#             else:
#                 filess = os.listdir("app/static/today_backup_01/app/public/" +
#                                str(folder))

#                 filename = os.getcwd()+"/app/static/today_backup_01/app/public/"+str(folder)+"/"+str(filess[0])
                
#                 ff.append(filename)
#                 dst = os.getcwd()+"/app/static/products"
#                 shutil.copy(filename, dst)

#         return "COMPLETED"
#     except Exception as e:

#         print(e)

# # Upload Brands


# @seller_router.get("/upload/brands")
# async def uploadBrands(db: Session = Depends(get_db)):
#     folders = os.listdir("app/static/brands")
#     # Brands Uploaded IN OBJECT STORAGE
#     try:
#         for files in folders:

#             # client.delete_object(
#             #     Bucket='brands',
#             #     Key=files)
#             img = getcwd()+"/app/static/brands/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='brands',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#         return "BRANDS UPDATED"
#     except Exception as e:
#         return "FAILED"

#     # Brands uploaded in database
#     try:
#         for files in folders:
#             brand = db.query(BrandsModel).filter(
#                 BrandsModel.logo == str("brands/")+str(files)).first()

#             if(brand is not None):
#                 key = str('brands/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 brand.logo = url
#                 db.flush()
#                 db.commit()

#         return "BRAND SUCCESS"
#     except Exception as e:
#         return "FAILED"


# # CATEGORIES
# @seller_router.get("/upload/categories")
# async def test(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/categories")

#     # Upload categories in Object Storage
#     try:
#         for files in folders:
#             img = getcwd()+"/app/static/categories/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='categories',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#         return "Deleted"
#     except Exception as e:
#         return "FAILED"

#     # Update data in categories table
#     try:
#         for files in folders:
#             category = db.query(CategoriesModel).filter(
#                 CategoriesModel.image == str("categories/")+str(files)).first()
#             if(category is not None):
#                 key = str('categories/')+str(files)
#                 url = str(linode_obj_config['endpoint_url'])+'/'+str(key)
#                 category.image_path = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"

# # COMMISSION


# @seller_router.get("/upload/commission")
# async def test(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/commission")

#     # Upload Commission in Object Storage
#     try:
#         for files in folders:
#             img = getcwd()+"/app/static/commission/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='commission',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#         return "Deleted"
#     except Exception as e:
#         return "FAILED"

#     # Update data in seller payment table
#     try:
#         for files in folders:
#             commission = db.query(SellerPaymentModel).filter(
#                 SellerPaymentModel.commission_generated == files).first()
#             if(commission is not None):
#                 key = files
#                 url = str(linode_obj_config['endpoint_url'])+'/'+str(key)
#                 commission.commission_generated = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"

# # Transfer All Images to products folder from public folder



# @seller_router.get("/transfer/files")
# async def test(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/public")

#     try:
#         ff = []
#         for folder in folders:
#             files = os.listdir("app/static/public/" +
#                                str(folder)+"/conversions")

#             filename = os.getcwd()+"/app/static/public/"+str(folder) + \
#                 "/conversions/"+str(files[0])
#             ff.append(filename)
#             dst = os.getcwd()+"/app/static/products"
#             shutil.copy(filename, dst)

#         return len(ff)
#     except Exception as e:

#         print('==============')

# # Upload Brands


# @seller_router.get("/upload/brands")
# async def uploadBrands(db: Session = Depends(get_db)):
#     folders = os.listdir("app/static/brands")
#     # Brands Uploaded IN OBJECT STORAGE
#     # try:
#     #     for files in folders:

#     #         img = getcwd()+"/app/static/brands/"+str(files)
#     #         client.upload_file(
#     #             Filename=img,
#     #             Bucket='brands',
#     #             Key=files,
#     #             ExtraArgs={'ACL': 'public-read'})

#     #     return "BRANDS UPDATED"
#     # except Exception as e:
#     #     return "FAILED"

#     # Brands uploaded in database
#     try:
#         for files in folders:
#             brand = db.query(BrandsModel).filter(
#                 BrandsModel.logo == str("brands/")+str(files)).first()

#             if(brand is not None):
#                 key = str('brands/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 brand.logo = url
#                 db.flush()
#                 db.commit()

#         return "BRAND SUCCESS"
#     except Exception as e:
#         return "FAILED"


# # CATEGORIES
# @seller_router.get("/upload/categories")
# async def test(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/categories")

#     # Upload categories in Object Storage
#     # try:
#     #     for files in folders:
#     #         img = getcwd()+"/app/static/categories/"+str(files)
#     #         client.upload_file(
#     #             Filename=img,
#     #             Bucket='categories',
#     #             Key=files,
#     #             ExtraArgs={'ACL': 'public-read'})

#     #     return "Updated"
#     # except Exception as e:
#     #     return "FAILED"

#     # Update data in categories table
#     try:
#         for files in folders:
#             category = db.query(CategoriesModel).filter(
#                 CategoriesModel.image == str("categories/")+str(files)).first()
#             if(category is not None):
#                 key = str('categories/')+str(files)
#                 url = str(linode_obj_config['endpoint_url'])+'/'+str(key)
#                 category.image_path = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"

# # COMMISSION


# @seller_router.get("/upload/commission")
# async def test(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/commission")

#     # Upload Commission in Object Storage
#     try:
#         for files in folders:
#             img = getcwd()+"/app/static/commission/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='commission',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#         return "Deleted"
#     except Exception as e:
#         return "FAILED"

#     # Update data in seller payment table
#     try:
#         for files in folders:
#             commission = db.query(SellerPaymentModel).filter(
#                 SellerPaymentModel.commission_generated == files).first()
#             if(commission is not None):
#                 key = files
#                 url = str(linode_obj_config['endpoint_url'])+'/'+str(key)
#                 commission.commission_generated = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"


# # Hero Banners
# @seller_router.get("/upload/hero/banners")
# async def test(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/hero")

#     try:
#         for files in folders:
#             banner = db.query(BannersModel).filter(
#                 BannersModel.banner == str("hero/")+str(files)).first()

#             if(banner is not None):
#                 key = str('banners/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 banner.banner = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"
#     # try:
#     #     for files in folders:

#     #         img = getcwd()+"/app/static/hero/"+str(files)
#     #         client.upload_file(
#     #             Filename=img,
#     #             Bucket='banners',
#     #             Key=files,
#     #             ExtraArgs={'ACL': 'public-read'})

#     #     return "BANNERS UDATED"
#     # except Exception as e:
#     #     return "FAILED"


# # Orders Invoices
# @seller_router.get("/upload/order/invoices")
# async def testUploadInvoice(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/invoice")

#     try:
#         for files in folders:
#             invoice = db.query(OrderShippingModel).filter(
#                 OrderShippingModel.invoice == files).first()

#             if(invoice is not None):
#                 key = str('invoices/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 invoice.invoice = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS UPDATED"
#     except Exception as e:
#         return "FAILED"
#     # try:
#     #     for files in folders:

#     #         img = getcwd()+"/app/static/inv/"+str(files)
#     #         client.upload_file(
#     #             Filename=img,
#     #             Bucket='invoices',
#     #             Key=files,
#     #             ExtraArgs={'ACL': 'public-read'})

#     #     return "INVOICES UDATED"
#     # except Exception as e:
#     #     return "FAILED"


# # Orders Lebel
# @seller_router.get("/upload/order/lebel")
# async def testLebel(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/lebel")

#     try:
#         for files in folders:
#             invoice = db.query(OrderShippingModel).filter(
#                 OrderShippingModel.slip == files).first()

#             if(invoice is not None):
#                 key = str('slip/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 invoice.slip = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"
#     # try:
#     #     for files in folders:

#     #         img = getcwd()+"/app/static/lebel/"+str(files)
#     #         client.upload_file(
#     #             Filename=img,
#     #             Bucket='slip',
#     #             Key=files,
#     #             ExtraArgs={'ACL': 'public-read'})

#     #     return "LEBEL UDATED"
#     # except Exception as e:
#     #     return "FAILED"


# # Notofication Image
# @seller_router.get("/upload/notification/image")
# async def NotificationImage(db: Session = Depends(get_db)):
#     folders = os.listdir("app/static/notification")

#     # Upload categories in Object Storage
#     try:
#         for files in folders:
#             img = getcwd()+"/app/static/notification/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='notification',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#         return "Uploaded"
#     except Exception as e:
#         return "FAILED"

#     # Update data in Notification Image table
#     try:
#         for files in folders:
#             notification = db.query(NotificationImageModel).filter(
#                 NotificationImageModel.image == str("notification/")+str(files)).first()
#             if(notification is not None):
#                 key = str('notification/')+str(files)
#                 url = str(linode_obj_config['endpoint_url'])+'/'+str(key)
#                 notification.image = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"

# # Order Return Proof Image


# @seller_router.get("/upload/order/return/image")
# async def ReturnProof(db: Session = Depends(get_db)):
#     folders = os.listdir("app/static/proof")

#     # Upload categories in Object Storage
#     try:
#         for files in folders:
#             img = getcwd()+"/app/static/proof/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='return_proof',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#         return "Uploaded"
#     except Exception as e:
#         return "FAILED"

#     # Update data in Notification Image table
#     try:
#         for files in folders:
#             return_proof = db.query(OrderReturnProofModel).filter(
#                 OrderReturnProofModel.image == files).first()
#             if(return_proof is not None):
#                 key = str('return_proof/')+str(files)
#                 url = str(linode_obj_config['endpoint_url'])+'/'+str(key)
#                 return_proof.image = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"


# @seller_router.get("/test/svg")
# async def tesst():
#     img = getcwd()+"/app/static/fulfillments/tape.png"
#     client.upload_file(
#         Filename=img,
#         Bucket='svg_icons',
#         Key='tape.png',
#         ExtraArgs={'ACL': 'public-read'})

# # @seller_router.get("/pincode")
# # async def getPincode(request: Request):
# #     try:
# #         pincodes = getcwd()+"/app/static/pincodes.json"
# #         return pincodes
# #     except Exception as e:
# #         print(e)


# # Orders Lebel
# @seller_router.get("/upload/seller/payments/csv")
# async def sellerPaymentCsv(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/paid_payments")

#     try:
#         for files in folders:
#             csv = files.rsplit(".", 1)

#             csiinvoice = db.query(SellerPaymentModel).filter(
#                 SellerPaymentModel.reff_no == csv[0]).first()
#             if(csiinvoice is not None):
#                 key = str('paid_payments/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 csiinvoice.payment_csv = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"
#     # try:
#     #     for files in folders:

#     #         img = getcwd()+"/app/static/paid_payments/"+str(files)
#     #         client.upload_file(
#     #             Filename=img,
#     #             Bucket='paid_payments',
#     #             Key=files,
#     #             ExtraArgs={'ACL': 'public-read'})

#     #     return "PAYMENT CSV UPDATED"
#     # except Exception as e:
#     #     return "FAILED"


# @seller_router.get("/upload/buyer/payments/csv")
# async def sellerPaymentCsv(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/buyer_payments")

#     try:
#         for files in folders:
#             csv = files.rsplit(".", 1)

#             csiinvoice = db.query(SellerPaymentModel).filter(
#                 SellerPaymentModel.reff_no == csv[0]).first()
#             if(csiinvoice is not None):
#                 key = str('buyer_payments/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 csiinvoice.buyer_payment_csv = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"
#     # try:
#     #     for files in folders:

#     #         img = getcwd()+"/app/static/buyer_payments/"+str(files)
#     #         client.upload_file(
#     #             Filename=img,
#     #             Bucket='buyer_payments',
#     #             Key=files,
#     #             ExtraArgs={'ACL': 'public-read'})

#     #     return "PAYMENT CSV UPDATED"
#     # except Exception as e:
#     #     return "FAILED"


# @seller_router.get("/upload/seller/commission/pdf")
# async def sellerPaymentCsv(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/commission")

#     try:
#         ff = []
#         for files in folders:
#             csv = files.rsplit(".", 1)

#             csiinvoice = db.query(SellerPaymentModel).filter(
#                 SellerPaymentModel.reff_no == csv[0]).first()
#             if(csiinvoice):
#                 filename = str(csiinvoice.seller_id)+'_'+str(files)

#                 checkfileexist = os.path.exists(
#                     'app/static/commission/'+filename)

#                 if(checkfileexist == True):

#                     csv = files.rsplit(".", 1)
#                     key = str('commission/')+str(filename)
#                     url = str(
#                         linode_obj_config['endpoint_url'])+'/'+str(key)
#                     csiinvoice.commission_generated = url
#                     db.flush()
#                     db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         print(e)
#     # try:
#     #     for files in folders:

#     #         img = getcwd()+"/app/static/commission/"+str(files)
#     #         client.upload_file(
#     #             Filename=img,
#     #             Bucket='commission',
#     #             Key=files,
#     #             ExtraArgs={'ACL': 'public-read'})

#     #     return "PAYMENT CSV UPDATED"
#     # except Exception as e:
#     #     return "FAILED"


# @seller_router.get("/upload/order/return/proof")
# async def sellerPaymentCsv(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/proof")

#     try:
#         for files in folders:

#             proof = db.query(OrderReturnProofModel).filter(
#                 OrderReturnProofModel.image == files).first()
#             if(proof is not None):
#                 key = str('proof/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 proof.image = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"
#     # try:
#     #     for files in folders:

#     #         img = getcwd()+"/app/static/proof/"+str(files)
#     #         client.upload_file(
#     #             Filename=img,
#     #             Bucket='proof',
#     #             Key=files,
#     #             ExtraArgs={'ACL': 'public-read'})

#     #     return "PAYMENT CSV UPDATED"
#     # except Exception as e:
#     #     return "FAILED"


# @seller_router.get("/upload/user/document/proof")
# async def sellerPaymentCsv(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/userdocument")

#     try:
#         for files in folders:

#             proof = db.query(UserDocumentModel).filter(
#                 UserDocumentModel.filename == str("userdocument/")+str(files)).first()
#             if(proof is not None):
#                 key = str('userdocument/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 proof.file_path = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"
#     try:
#         for files in folders:

#             img = getcwd()+"/app/static/userdocument/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='userdocument',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#         return "PAYMENT CSV UPDATED"
#     except Exception as e:
#         return "FAILED"


# @seller_router.get("/upload/notification/image/banner")
# async def sellerPaymentCsv(db: Session = Depends(get_db)):

#     folders = os.listdir("app/static/notification")

#     try:
#         for files in folders:

#             notification = db.query(NotificationImageModel).filter(
#                 NotificationImageModel.image == str("notification/")+str(files)).first()
#             if(notification is not None):
#                 key = str('notification/')+str(files)
#                 url = str(
#                     linode_obj_config['endpoint_url'])+'/'+str(key)
#                 notification.file_path = url
#                 db.flush()
#                 db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"
#     try:
#         for files in folders:

#             img = getcwd()+"/app/static/notification/"+str(files)
#             client.upload_file(
#                 Filename=img,
#                 Bucket='notification',
#                 Key=files,
#                 ExtraArgs={'ACL': 'public-read'})

#         return "PAYMENT CSV UPDATED"
#     except Exception as e:
#         return "FAILED"
