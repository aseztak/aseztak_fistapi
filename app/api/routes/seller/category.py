
# from fastapi.param_functions import Depends
# from fastapi.routing import APIRouter
# from starlette.requests import Request
# from sqlalchemy.orm.session import Session
# from app.api.helpers.categories import CategoriesHelper
# from app.db.config import SessionLocal, get_db
# from app.db.models.categories import CategoriesModel
# from app.db.schemas.category_schema import SelectableCategorySchema
# from starlette.status import HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED, HTTP_404_NOT_FOUND, HTTP_200_OK
# from app.services.auth import auth

# seller_categories_route = APIRouter()


# @seller_categories_route.get("/selectable/{search}", response_model=SelectableCategorySchema)
# async def getCategories(request: Request, search: str, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:

#         data: CategoriesModel = await CategoriesHelper.selectableCategories(db=db, search=search)
#         if(data.count() == 0):

#             return {"status_code": HTTP_200_OK, "categories": [], "current_page": page, "total_pages": 0}
#         else:

#             categories_data: CategoriesModel = data.limit(
#                 limit=limit).offset((page - 1) * limit).all()
#             categories = []
#             for category in categories_data:

#                 if(category.image_path == None):
#                     image = ''
#                 else:
#                     image = category.image_path

#                 cat = {
#                     'id': category.id,
#                     'name': category.name,
#                     'image': image,
#                     'status': category.status
#                 }
#                 categories.append(cat)

#             # Total Records
#             total_records = data.count()

#             # Number of Total Pages
#             total_pages = str(round((total_records/limit), 2))

#             total_pages = total_pages.split('.')

#             if(total_pages[1] != 0):
#                 total_pages = int(total_pages[0]) + 1

#             return {"status_code": HTTP_200_OK, "categories": categories, "current_page": page, "total_pages": total_pages}

#     except Exception as e:

#         return {"status_code": HTTP_200_OK, "categories": [], "current_page": page, "total_pages": 0}
