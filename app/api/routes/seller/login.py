# from datetime import datetime, timedelta
# from typing import Optional

# from fastapi import Depends, FastAPI, HTTPException, status
# from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
# from jose import JWTError, jwt
# from passlib.context import CryptContext
# from pydantic import BaseModel
# from fastapi import APIRouter,  Depends, status
# from starlette.status import HTTP_401_UNAUTHORIZED, HTTP_404_NOT_FOUND

# from app.db.schemas.login_schema import Login, Token, TokenData, User, UserInDB
# from sqlalchemy.orm.session import Session
# from app.db.config import SessionLocal, get_db

# from app.db.models.login import LoginModel

# seller_login_router = APIRouter()
# # to get a string like this run:
# # openssl rand -hex 32
# SECRET_KEY = "ee8908722a79d50dd5cdf9aeb29339a6c28ec31450144cf607a4cc5932eaf6b0"
# ALGORITHM = "HS256"
# ACCESS_TOKEN_EXPIRE_MINUTES = 30


# pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


# app = FastAPI()


# def verify_password(plain_password, hashed_password):
#     return pwd_context.verify(plain_password, hashed_password)


# def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
#     to_encode = data.copy()
#     if expires_delta:
#         expire = datetime.utcnow() + expires_delta
#     else:
#         expire = datetime.utcnow() + timedelta(minutes=15)
#     to_encode.update({"exp": expire})
#     encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
#     return encoded_jwt


# @seller_login_router.post("/login")
# async def login(form_data: Login, db: Session = Depends(get_db)):
#     try:

#         user = db.query(LoginModel).filter(
#             LoginModel.email == form_data.email).first()

#         if not user:
#             return {"status_code": HTTP_404_NOT_FOUND, "message": "user not found"}
#         if not verify_password(form_data.password, user.hashed_password):
#             return {"status_code": HTTP_401_UNAUTHORIZED, "message": "Incorrect username or password"}

#         if not user:
#             raise HTTPException(
#                 status_code=status.HTTP_401_UNAUTHORIZED,
#                 detail="Incorrect username or password",
#                 headers={"WWW-Authenticate": "Bearer"},
#             )
#         access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
#         access_token = create_access_token(
#             data={"sub": user.email}, expires_delta=access_token_expires
#         )

#         credentials_exception = HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail="Could not validate credentials",
#             headers={"WWW-Authenticate": "Bearer"},
#         )
#         try:
#             payload = jwt.decode(access_token, SECRET_KEY,
#                                  algorithms=[ALGORITHM])
#             email: str = payload.get("sub")
#             if email is None:
#                 raise credentials_exception
#             token_data = TokenData(email=email)
#         except JWTError:
#             raise credentials_exception

#         db_user = db.query(LoginModel).filter(
#             LoginModel.email == token_data.email).first()

#         if db_user is None:
#             raise credentials_exception

#         user = {
#             'username': db_user.username,
#             'email': db_user.email,
#             'full_name': db_user.full_name
#         }

#         return {"access_token": access_token, "token_type": "bearer", 'user': User(**user)}

#     except Exception as e:
#         print(e)
