from fastapi import APIRouter,  Depends, Request
from sqlalchemy.orm.session import Session

from app.api.helpers.media import MediaHelper
from app.api.util.calculation import floatingValue
from app.api.helpers.auth import *
from app.api.helpers.orderstaticstatus import OrderStatus

from app.db.config import get_db
from app.db.models.order_status import OrderStatusModel
from app.db.models.orders import OrdersModel
from app.db.models.shipping import OrderShippingModel

from app.db.schemas.auth_schema import *
from app.db.schemas.invoice_schema import ExportInvoiceListSchema, InvoiceListSchema, SearchInvoiceListSchema
from app.services.auth import auth
from app.resources.strings import *
from app.api.helpers.users import *
from app.services.auth_bearer import JWTBearer
from starlette.status import HTTP_200_OK
from starlette.requests import Request
from app.db.models.order_items import OrderItemsModel
from app.api.helpers.orders import OrderHelper
import pandas as pd
from app.db.models.user import UserModel
from os import getcwd, unlink


seller_invoice_router = APIRouter()


# Get Invoice List
@seller_invoice_router.post("/list", response_model=InvoiceListSchema, dependencies=[Depends(JWTBearer())])
async def getList(request: Request, search: SearchInvoiceListSchema, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)
        invoices = []
        if(search.from_date != '' and search.to_date != ''):
            data: OrderItemsModel = await OrderHelper.InvoiceList(db=db, user_id=userdata['id'], from_date=search.from_date, to_date=search.to_date)
        else:
            data: OrderItemsModel = await OrderHelper.InvoiceList(db=db, user_id=userdata['id'])

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "total_items": 0, "invoices": invoices, "current_page": search.page, "total_pages": 0}

        orders: OrderItemsModel = data.limit(
            limit=search.limit).offset((search.page - 1) * search.limit).all()

        for order in orders:
            # Order Detail
            order = db.query(OrdersModel).filter(
                OrdersModel.id == order.order_id).first()

            # Latest Status
            status = db.query(OrderStatusModel).filter(
                OrderStatusModel.order_id == order.id).order_by(OrderStatusModel.id.desc()).first()

            total_amount = 0
            grand_total = 0
            total_items = []
            gst = 0
            # Aseztak Service Amount
            aseztak_service = 0
            # Aseztak Service Tax
            cgst_on_service_tax = 0
            sgst_on_service_tax = 0
            igst_on_service_tax = 0
            # Tax
            cgst_on_tax = 0
            sgst_on_tax = 0
            igst_on_tax = 0

            # Discount Amount
            discount_amount = 0
            items = db.query(OrderItemsModel).filter(
                    OrderItemsModel.order_id == order.id).all()

            for item in items:

                    if(item.product_discount != 0):
                        product_discounted_price = (
                            float(item.price) * float(item.product_discount)) / 100
                        product_discounted_price = (float(item.price) -
                                                    float(product_discounted_price))
                        items_price = round(product_discounted_price, 2)

                        # Recalculate item taxable amount
                        total_amount += float(
                            items_price) * float(item.quantity)

                        # Recalculate Asez Discount amount
                        if(item.discount_amount != 0):
                            recalculate_discount_amount = (
                                float(items_price) * item.discount_rate) / 100
                            recalculate_discount_amount = (
                                float(recalculate_discount_amount) * float(item.quantity))
                            items_discount_amount = recalculate_discount_amount
                        else:
                            items_discount_amount = 0.00

                        discount_amount += items_discount_amount
                        # Price Tax
                        item_cgst_on_tax = (
                            float(item.cgst_on_tax) * float(item.product_discount) / 100)
                        item_cgst_on_tax = (
                            float(item.cgst_on_tax) - float(item_cgst_on_tax))
                        cgst_on_tax += float(item_cgst_on_tax)

                        item_sgst_on_tax = (
                            float(item.sgst_on_tax) * float(item.product_discount) / 100)
                        item_sgst_on_tax = (
                            float(item.sgst_on_tax) - float(item_sgst_on_tax))
                        sgst_on_tax += float(item_sgst_on_tax)

                        item_igst_on_tax = (
                            float(item.igst_on_tax) * float(item.product_discount) / 100)
                        item_igst_on_tax = (
                            float(item.igst_on_tax) - float(item_igst_on_tax))
                        igst_on_tax += float(item_igst_on_tax)

                        # Aseztak Service Amount
                        item_asez_service_amount = (
                            float(item.asez_service_amount) * float(item.product_discount) / 100)
                        item_asez_service_amount = (
                            float(item.asez_service_amount) - float(item_asez_service_amount))

                        aseztak_service += float(item_asez_service_amount)

                        # Aseztak Service Tax
                        item_asez_service_on_cgst = (
                            float(item.asez_service_on_cgst) * float(item.product_discount) / 100)
                        item_asez_service_on_cgst = (
                            float(item.asez_service_on_cgst) - float(item_asez_service_on_cgst))
                        cgst_on_service_tax += float(item_asez_service_on_cgst)

                        item_asez_service_on_sgst = (
                            float(item.asez_service_on_sgst) * float(item.product_discount) / 100)
                        item_asez_service_on_sgst = (
                            float(item.asez_service_on_sgst) - float(item_asez_service_on_sgst))
                        sgst_on_service_tax += float(item_asez_service_on_sgst)

                        item_asez_service_on_igst = (
                            float(item.asez_service_on_igst) * float(item.product_discount) / 100)
                        item_asez_service_on_igst = (
                            float(item.asez_service_on_igst) - float(item_asez_service_on_igst))
                        igst_on_service_tax += float(item_asez_service_on_igst)

                    else:
                        total_amount += float(item.taxable_total_amount)
                        discount_amount += item.discount_amount

                        # Price Tax
                        cgst_on_tax += float(item.cgst_on_tax)
                        sgst_on_tax += float(item.sgst_on_tax)
                        igst_on_tax += float(item.igst_on_tax)

                        # Aseztak Service Amount
                        aseztak_service += item.asez_service_amount

                        # Aseztak Service Tax
                        cgst_on_service_tax += item.asez_service_on_cgst
                        sgst_on_service_tax += item.asez_service_on_sgst
                        igst_on_service_tax += item.asez_service_on_igst
                    if(item.product_discount != 0):
                        grand_item_total = (
                            float(item.item_total_amount) * float(item.product_discount) / 100)
                        grand_item_total = (
                            float(item.item_total_amount) - float(grand_item_total))
                        grand_total += float(grand_item_total)
                    else:
                        grand_total += float(item.item_total_amount)

                    total_items.append(item)

            # Check Shipping
            shipping = db.query(OrderShippingModel).filter(
                OrderShippingModel.order_id == order.id).first()

            if(shipping):
                # Customize Order Reff
                reff_id = 'ASEZ'+str(order.reff)+"U"+str(order.user_id)

                statuslist = OrderStatus.AllstatusList()
                statustitle = ''
                for s_title in statuslist:
                    if(status.status == s_title['status_id']):
                        statustitle = s_title['status_title']

                status = {
                    'status_id': status.status,
                    'status_title': statustitle
                }
                # return floatingValue(float(igst_on_service_tax) + float(igst_on_tax))
                cgst = floatingValue(0)
                sgst = floatingValue(0)
                igst = floatingValue(0)
                if(float(igst_on_tax) == 0.00):
                    cgst = floatingValue(
                        float(cgst_on_tax) + float(cgst_on_service_tax))
                    sgst = floatingValue(
                        float(sgst_on_tax) + float(sgst_on_service_tax))
                else:
                    igst = floatingValue(
                        float(igst_on_service_tax) + float(igst_on_tax))

                # Check Shipping Charge
                shipping_charge = order.delivery_charge

                # Grand Total
                final_grand_total = (float(grand_total) +
                                     float(shipping_charge))

                checkinvoice = db.execute("SELECT orders.id, orders.invoice FROM orders WHERE orders.id =:param AND orders.invoice REGEXP '^[0-9]+$'  and app_version = 'V4'", {
                    "param": order.id
                }).first()
                if(checkinvoice is not None):
                    if(int(checkinvoice.invoice) <= 9):
                        ord_invoice = str('ASEZ202320240') + \
                            str(checkinvoice.invoice)
                    else:
                        ord_invoice = str('ASEZ20232024') + \
                            str(checkinvoice.invoice)

                else:
                    ord_invoice = order.invoice
                ord = {
                    'order_id': order.id,
                    'invoice_date': order.invoice_date.strftime("%B %d %Y"),
                    'order_reff': reff_id,
                    'order_number': order.order_number,
                    'total_amount': floatingValue(total_amount),
                    'discount': floatingValue(discount_amount),
                    'service': floatingValue(aseztak_service),
                    'total_items': len(total_items),
                    'invoice': ord_invoice,
                    'invoice_pdf': str(shipping.invoice),
                    'status': status,
                    'igst': igst,
                    'cgst': cgst,
                    'sgst': sgst,
                    'grand_total': floatingValue(final_grand_total)
                }

                invoices.append(ord)

        # Count Total Products
        total_records = data.count()

        # Number of Total Pages
        total_pages = round((total_records/search.limit), 0)

        # Check total count Similarity of records
        check = (total_pages * search.limit)

        if(check == total_records):
            pass
        else:
            total_pages = total_pages + 1

        return {"status_code": HTTP_200_OK, "total_items": total_records, "invoices": invoices, "current_page": search.page, "total_pages": total_pages}

    except Exception as e:
        print(e)
        return {"status_code": HTTP_200_OK, "total_items": 0, "invoices": [], "current_page": search.page, "total_pages": 0}


# Export Invoice List
@seller_invoice_router.post("/export", dependencies=[Depends(JWTBearer())])
async def exportInvoiceList(request: Request, search: ExportInvoiceListSchema, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)
        # New Changes
        userdata = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()
        userdata = {
            'id': userdata.id,
            'name': userdata.name,
            'mobile': userdata.mobile,
            'region': userdata.region,
            'city': userdata.city,
            'pincode': userdata.pincode
        }

        invoices = []
        if(search.from_date != '' and search.to_date != ''):
            data: OrderItemsModel = await OrderHelper.InvoiceList(db=db, user_id=userdata['id'], from_date=search.from_date, to_date=search.to_date)
            # rename csv file
            username = userdata['name']
            username = username.replace(" ", "-").lower()

            statement = str('SellerInvoiceList-') + \
                str(username) + '-' + \
                str(userdata['id'])+str(search.from_date) + \
                str(search.to_date)+str('.csv')
        else:
            data: OrderItemsModel = await OrderHelper.InvoiceList(db=db, user_id=userdata['id'], from_date='', to_date='')
            # rename csv file
            username = userdata['name']
            username = username.replace(" ", "-").lower()

            statement = str('SellerInvoiceList-') + \
                str(username) + '-'+str(userdata['id'])+str('.csv')

        if(data.count() == 0):
            return {"file": ""}

        orders: OrderItemsModel = data.all()

        for order in orders:
            # Order Detail
            order = db.query(OrdersModel).filter(
                OrdersModel.id == order.order_id).first()

            # Latest Status
            status = db.query(OrderStatusModel).filter(
                OrderStatusModel.order_id == order.id).order_by(OrderStatusModel.id.desc()).first()

            total_amount = 0
            grand_total = 0
            total_items = []
            gst = 0
            # Aseztak Service Amount
            aseztak_service = 0
            # Aseztak Service Tax
            cgst_on_service_tax = 0
            sgst_on_service_tax = 0
            igst_on_service_tax = 0
            # Tax
            cgst_on_tax = 0
            sgst_on_tax = 0
            igst_on_tax = 0

            # Discount Amount
            discount_amount = 0
            if(status.status <= 70):
                items = db.query(OrderItemsModel).filter(
                    OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status < 80).all()

                for item in items:
                    if(item.product_discount != 0):
                        product_discounted_price = (
                            float(item.price) * float(item.product_discount)) / 100
                        product_discounted_price = (float(item.price) -
                                                    float(product_discounted_price))
                        items_price = round(product_discounted_price, 2)

                        # Recalculate item taxable amount
                        total_amount += float(
                            items_price) * float(item.quantity)

                        # Recalculate Asez Discount amount
                        if(item.discount_amount != 0):
                            recalculate_discount_amount = (
                                float(items_price) * item.discount_rate) / 100
                            recalculate_discount_amount = (
                                float(recalculate_discount_amount) * float(item.quantity))
                            items_discount_amount = recalculate_discount_amount
                        else:
                            items_discount_amount = 0.00

                        discount_amount += items_discount_amount
                        # Price Tax
                        item_cgst_on_tax = (
                            float(item.cgst_on_tax) * float(item.product_discount) / 100)
                        item_cgst_on_tax = (
                            float(item.cgst_on_tax) - float(item_cgst_on_tax))
                        cgst_on_tax += float(item_cgst_on_tax)

                        item_sgst_on_tax = (
                            float(item.sgst_on_tax) * float(item.product_discount) / 100)
                        item_sgst_on_tax = (
                            float(item.sgst_on_tax) - float(item_sgst_on_tax))
                        sgst_on_tax += float(item_sgst_on_tax)

                        item_igst_on_tax = (
                            float(item.igst_on_tax) * float(item.product_discount) / 100)
                        item_igst_on_tax = (
                            float(item.igst_on_tax) - float(item_igst_on_tax))
                        igst_on_tax += float(item_igst_on_tax)

                        # Aseztak Service Amount
                        item_asez_service_amount = (
                            float(item.asez_service_amount) * float(item.product_discount) / 100)
                        item_asez_service_amount = (
                            float(item.asez_service_amount) - float(item_asez_service_amount))

                        aseztak_service += float(item_asez_service_amount)

                        # Aseztak Service Tax
                        item_asez_service_on_cgst = (
                            float(item.asez_service_on_cgst) * float(item.product_discount) / 100)
                        item_asez_service_on_cgst = (
                            float(item.asez_service_on_cgst) - float(item_asez_service_on_cgst))
                        cgst_on_service_tax += float(item_asez_service_on_cgst)

                        item_asez_service_on_sgst = (
                            float(item.asez_service_on_sgst) * float(item.product_discount) / 100)
                        item_asez_service_on_sgst = (
                            float(item.asez_service_on_sgst) - float(item_asez_service_on_sgst))
                        sgst_on_service_tax += float(item_asez_service_on_sgst)

                        item_asez_service_on_igst = (
                            float(item.asez_service_on_igst) * float(item.product_discount) / 100)
                        item_asez_service_on_igst = (
                            float(item.asez_service_on_igst) - float(item_asez_service_on_igst))
                        igst_on_service_tax += float(item_asez_service_on_igst)

                    else:
                        total_amount += float(item.taxable_total_amount)
                        discount_amount += item.discount_amount

                        # Price Tax
                        cgst_on_tax += float(item.cgst_on_tax)
                        sgst_on_tax += float(item.sgst_on_tax)
                        igst_on_tax += float(item.igst_on_tax)

                        # Aseztak Service Amount
                        aseztak_service += item.asez_service_amount

                        # Aseztak Service Tax
                        cgst_on_service_tax += item.asez_service_on_cgst
                        sgst_on_service_tax += item.asez_service_on_sgst
                        igst_on_service_tax += item.asez_service_on_igst

                    if(item.product_discount != 0):
                        grand_item_total = (
                            float(item.item_total_amount) * float(item.product_discount) / 100)
                        grand_item_total = (
                            float(item.item_total_amount) - float(grand_item_total))
                        grand_total += float(grand_item_total)
                    else:
                        grand_total += float(item.item_total_amount)

                    total_items.append(item)

            else:
                items = db.query(OrderItemsModel).filter(
                    OrderItemsModel.order_id == order.id).filter(OrderItemsModel.weight != 0.00).all()

                for item in items:
                    if(item.product_discount != 0):
                        product_discounted_price = (
                            float(item.price) * float(item.product_discount)) / 100
                        product_discounted_price = (float(item.price) -
                                                    float(product_discounted_price))
                        items_price = round(product_discounted_price, 2)

                        # Recalculate item taxable amount
                        total_amount += float(
                            items_price) * float(item.quantity)

                        # Recalculate Asez Discount amount
                        if(item.discount_amount != 0):
                            recalculate_discount_amount = (
                                float(items_price) * item.discount_rate) / 100
                            recalculate_discount_amount = (
                                float(recalculate_discount_amount) * float(item.quantity))
                            items_discount_amount = recalculate_discount_amount
                        else:
                            items_discount_amount = 0.00

                        discount_amount += items_discount_amount
                        # Price Tax
                        item_cgst_on_tax = (
                            float(item.cgst_on_tax) * float(item.product_discount) / 100)
                        item_cgst_on_tax = (
                            float(item.cgst_on_tax) - float(item_cgst_on_tax))
                        cgst_on_tax += float(item_cgst_on_tax)

                        item_sgst_on_tax = (
                            float(item.sgst_on_tax) * float(item.product_discount) / 100)
                        item_sgst_on_tax = (
                            float(item.sgst_on_tax) - float(item_sgst_on_tax))
                        sgst_on_tax += float(item_sgst_on_tax)

                        item_igst_on_tax = (
                            float(item.igst_on_tax) * float(item.product_discount) / 100)
                        item_igst_on_tax = (
                            float(item.igst_on_tax) - float(item_igst_on_tax))
                        igst_on_tax += float(item_igst_on_tax)

                        # Aseztak Service Amount
                        item_asez_service_amount = (
                            float(item.asez_service_amount) * float(item.product_discount) / 100)
                        item_asez_service_amount = (
                            float(item.asez_service_amount) - float(item_asez_service_amount))

                        aseztak_service += float(item_asez_service_amount)

                        # Aseztak Service Tax
                        item_asez_service_on_cgst = (
                            float(item.asez_service_on_cgst) * float(item.product_discount) / 100)
                        item_asez_service_on_cgst = (
                            float(item.asez_service_on_cgst) - float(item_asez_service_on_cgst))
                        cgst_on_service_tax += float(item_asez_service_on_cgst)

                        item_asez_service_on_sgst = (
                            float(item.asez_service_on_sgst) * float(item.product_discount) / 100)
                        item_asez_service_on_sgst = (
                            float(item.asez_service_on_sgst) - float(item_asez_service_on_sgst))
                        sgst_on_service_tax += float(item_asez_service_on_sgst)

                        item_asez_service_on_igst = (
                            float(item.asez_service_on_igst) * float(item.product_discount) / 100)
                        item_asez_service_on_igst = (
                            float(item.asez_service_on_igst) - float(item_asez_service_on_igst))
                        igst_on_service_tax += float(item_asez_service_on_igst)

                    else:
                        total_amount += float(item.taxable_total_amount)
                        discount_amount += item.discount_amount

                        # Price Tax
                        cgst_on_tax += float(item.cgst_on_tax)
                        sgst_on_tax += float(item.sgst_on_tax)
                        igst_on_tax += float(item.igst_on_tax)

                        # Aseztak Service Amount
                        aseztak_service += item.asez_service_amount

                        # Aseztak Service Tax
                        cgst_on_service_tax += item.asez_service_on_cgst
                        sgst_on_service_tax += item.asez_service_on_sgst
                        igst_on_service_tax += item.asez_service_on_igst

                    if(item.product_discount != 0):
                        grand_item_total = (
                            float(item.item_total_amount) * float(item.product_discount) / 100)
                        grand_item_total = (
                            float(item.item_total_amount) - float(grand_item_total))
                        grand_total += float(grand_item_total)
                    else:
                        grand_total += float(item.item_total_amount)

                    total_items.append(item)

            # Check Shipping
            shipping = db.query(OrderShippingModel).filter(
                OrderShippingModel.order_id == order.id).first()

            if(shipping):
                # Customize Order Reff
                reff_id = 'ASEZ'+str(order.reff)+"U"+str(order.user_id)

                statuslist = OrderStatus.AllstatusList()
                statustitle = ''
                for s_title in statuslist:
                    if(status.status == s_title['status_id']):
                        statustitle = s_title['status_title']

                status = {
                    'status_id': status.status,
                    'status_title': statustitle
                }

                cgst = floatingValue(0)
                sgst = floatingValue(0)
                igst = floatingValue(0)
                if(float(igst_on_tax) == 0.00):
                    cgst = floatingValue(
                        float(cgst_on_tax) + float(cgst_on_service_tax))
                    sgst = floatingValue(
                        float(sgst_on_tax) + float(sgst_on_service_tax))
                else:
                    igst = floatingValue(
                        float(igst_on_service_tax) + float(igst_on_tax))

                # Check Shipping Charge
                shipping_charge = order.delivery_charge

                # Grand Total
                final_grand_total = (float(grand_total) +
                                     float(shipping_charge))

                checkinvoice = db.execute("SELECT orders.id, orders.invoice FROM orders WHERE orders.id =:param AND orders.invoice REGEXP '^[0-9]+$'  and app_version = 'V4'", {
                    "param": order.id
                }).first()
                if(checkinvoice is not None):
                    if(int(checkinvoice.invoice) <= 9):
                        ord_invoice = str('ASEZ202320240') + \
                            str(checkinvoice.invoice)
                    else:
                        ord_invoice = str('ASEZ20232024') + \
                            str(checkinvoice.invoice)

                else:
                    ord_invoice = order.invoice
                ord = {
                    'Order Id': order.id,
                    'Invoice Date': order.invoice_date.strftime("%B %d %Y"),
                    'Order Reff': reff_id,
                    'Order Number': order.order_number,
                    'Total Amount': floatingValue(total_amount),
                    'Discount': floatingValue(discount_amount),
                    'Service': floatingValue(aseztak_service),
                    'Total Items': len(total_items),
                    'Invoice': ord_invoice,
                    'Invoice Pdf': str(shipping.invoice),
                    'Status': status['status_title'],
                    'Igst': igst,
                    'Cgst': cgst,
                    'Sgst': sgst,
                    'Grand Total': floatingValue(final_grand_total)
                }

                invoices.append(ord)

        marks_data = pd.DataFrame(invoices
                                  )

        # saving the csv
        uploaded_file = getcwd()+"/app/static/seller_invoice_list/"+str(statement)
        marks_data.to_csv(uploaded_file, encoding='utf-8',
                          index=False)

        # Delete File From Account Transaction
        MediaHelper.deleteFile(
            bucket_name='seller_invoice_list', file_name=statement)

        # Upload FILE
        MediaHelper.uploadSellerInvoiceList(
            uploaded_file=uploaded_file, file_name=statement)

        # Remove file from Path
        unlink(uploaded_file)

        file_path = str(
            linode_obj_config['endpoint_url'])+'/seller_invoice_list/'+str(statement)

        return {"file": file_path}

    except Exception as e:
        print(e)
