# from os import stat
# from typing import Dict, List
# from fastapi import APIRouter
# from requests.api import head
# from sqlalchemy.orm.session import Session
# from sqlalchemy.sql.operators import is_
# from starlette import responses
# from app.api.helpers.auth import *
# from app.api.helpers.test import create_test

# from app.db.config import SessionLocal, get_db
# from app.db.schemas.auth_schema import *
# from app.db.schemas.test_schema import TestSchema
# from datetime import datetime
# from app.services.auth_handler import signJWT, decodeJWT
# from app.services.auth import auth
# from app.resources.strings import *
# from app.api.helpers.users import *
# import requests
# from app.services.auth_bearer import JWTBearer
# import jwt
# from app.db.models.billing_address import BillingAddressModel
# from app.db.models.bank_accounts import BankAccountsModel

# seller_auth_router = APIRouter()

# Register

#CLOSED 2024-01-08
# @seller_auth_router.post("/register")
# async def register(data: RegisterSchema, db: Session = Depends(get_db)):
#     return create_seller(user=data, db=db)

# Mobile Login - Send OTP


# @seller_auth_router.post("/send_otp")
# async def send_otp(data: SendOtpSchema, db: Session = Depends(get_db)):

#     mobile = data.mobile

#     # Check Seller
#     checkSeller = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id ==
#                                            UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.mobile == mobile).filter(UserModel.deleted_at.is_(None)).first()

#     # Check User
#     checkUser = db.query(UserModel).filter(UserModel.mobile == mobile).filter(
#         UserModel.deleted_at.is_(None)).first()

#     if(checkUser is None and checkSeller is None):
#         return {"status_code": status.HTTP_200_OK, "user_exist": False, "is_seller": False, "status": True, "message": "No record found."}

#     if(checkUser is not None and checkSeller is None):
#         return {"status_code": status.HTTP_200_OK, "user_exist": True, "is_seller": False, "status": False, "message": "Sorry! Your account has been registered as Buyer"}

#     if(checkUser is not None and checkUser.status == 0):
#         return {"status_code": status.HTTP_200_OK, "user_exist": True, "is_seller": False, "status": False, "message": "Sorry! Your account has not been activated yet!"}

#     if(checkUser is not None and checkUser.status == 90):
#         return {"status_code": status.HTTP_200_OK, "user_exist": True, "is_seller": False, "status": False, "message": "Sorry! Your account has been suspended!"}

#     otp = random.randint(746008, 999999)

#     signature = data.signature

#     extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
#         AUTHKEY+'&template_id='+TEMPLATE_ID + \
#         '&mobile='+str(91)+mobile+'&otp='+str(otp)

#     sendurl = OTP_SEND_URL+extra_param

#     try:

#         if(mobile != '9797979797'):
#             # Update Otp User
#             await SellerOtpUpdate(db=db, mobile=mobile, otp=otp)
#             requests.get(sendurl)

#         return {'status_code': HTTP_200_OK, "user_exist": True, "is_seller": True, "status": True, "message": "Otp Sent", 'signature': signature, 'otp_sent_at': datetime.now(), 'otp_expires': 60}

#     except Exception as e:
#         print(e)


# Mobile Login - with Mobile no & OTP


# @seller_auth_router.post("/login", )
# def user_login(data: verifyOtp, db: Session = Depends(get_db)):

#     try:

#         user = check_seller_user(data=data, db=db)

#         if user:

#             is_profile = user['is_profile']

#             profile = user['profile']

#             user = user['user']

#             token = signJWT(user)

#             if(is_profile == True):

#                 if(profile.business_type is None):
#                     business_type = ''
#                 else:
#                     business_type = profile.business_type

#                 userdata = {
#                     "id": user.id,
#                     "name": user.name,
#                     "email": user.email,
#                     "mobile": user.mobile,
#                     "country": user.country,
#                     "region": user.region,
#                     "pincode": user.pincode,
#                     "city": user.city,
#                     "business_type": business_type,
#                     "proof_type": 'GSTIN',
#                     "proof": profile.proof,
#                     "status": user.status

#                 }
#             else:

#                 userdata = {
#                     "id": user.id,
#                     "name": user.name,
#                     "email": user.email,
#                     "mobile": user.mobile,
#                     "country": user.country,
#                     "region": user.region,
#                     "pincode": user.pincode,
#                     "city": user.city,
#                     'business_type': '',
#                     "proof_type": 'GSTIN',
#                     "proof": '',
#                     "status": user.status

#                 }

#              # billing
#             billing_address = db.query(BillingAddressModel).filter(
#                 BillingAddressModel.user_id == user.id).first()

#             billing_address_exist = False
#             if(billing_address):
#                 billing_address_exist = True

#             # bank account
#             bank_account = db.query(BankAccountsModel).filter(
#                 BankAccountsModel.user_id == user.id).first()
#             bank_account_exist = False

#             if(bank_account):
#                 bank_account_exist = True

#             # Update Otp as Null
#             if(user.mobile != '9797979797'):
#                 user.otp = None
#                 db.flush()
#                 db.commit()

#             if(user.status == 0):

#                 return {'status_code': status.HTTP_200_OK, 'error': False, 'status': False, 'message': 'Sorry! Your account has not been activated yet!', 'token': token['access_token'], 'profile_exist': is_profile, 'billing_address_exist': billing_address_exist, 'bank_account_exist': bank_account_exist, 'user': userdata}

#             elif(user.status == 90):

#                 return {'status_code': status.HTTP_200_OK, 'error': False, 'status': False, 'message': 'Sorry! Your account has been suspended!', 'token': token['access_token'], 'profile_exist': is_profile, 'billing_address_exist': billing_address_exist, 'bank_account_exist': bank_account_exist, 'user': userdata}

#             else:
#                 return {'status_code': status.HTTP_200_OK, 'error': False, 'status': True, 'message': 'Success', 'token': token['access_token'], 'profile_exist': is_profile, 'billing_address_exist': billing_address_exist, 'bank_account_exist': bank_account_exist, 'user': userdata}

#             # return token['access_token']
#             # return {'status_code': status.HTTP_202_ACCEPTED, 'error': False, 'token': token['access_token'], 'profile_exist': is_profile, 'user': user}

#         else:
#             return {'status_code': status.HTTP_200_OK, 'error':  True, 'message': 'Invalid Otp'}

#     except Exception as e:
#         print(e)


# set user fcm_token
# @seller_auth_router.post("/set_fcm_token", dependencies=[Depends(JWTBearer())])
# async def Userfcm_token(request: Request, data: Userfcm_tokenSchema, db: Session = Depends(get_db)):

#     try:
#         userdata = auth(request=request)

#         user = db.query(UserModel).filter(
#             UserModel.id == userdata['id']).first()

#         user.fcm_token = data.fcm_token

#         db.flush()
#         db.commit()

#         if(user.fcm_token != ""):

#             return True

#         else:

#             return False

#     except Exception as e:
#         print(e)
