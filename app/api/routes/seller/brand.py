# from os import getcwd, unlink

# from typing import Dict, List
# from fastapi import APIRouter


# from sqlalchemy.orm.session import Session


# from app.api.helpers.media import MediaHelper
# from app.api.helpers.auth import *
# from app.api.helpers.brands import BrandsHelper

# from app.db.config import get_db
# from app.db.models.brand import BrandsModel


# from app.db.schemas.auth_schema import *
# from app.db.schemas.brand_schema import BrandListSchema, UpdateBrandSchema
# from app.db.schemas.test_schema import TestSchema
# from datetime import datetime
# from app.services.auth import auth
# from app.resources.strings import *
# from app.api.helpers.users import *
# from app.services.auth_bearer import JWTBearer
# from datetime import date, timedelta
# from starlette.status import HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED, HTTP_404_NOT_FOUND
# from starlette.requests import Request
# from fastapi import File, UploadFile
# import uuid
# from PIL import Image, ImageOps
# seller_brand_router = APIRouter()

#CLOSED 2024-01-08
# List of All Brands
# @seller_brand_router.get("/list", response_model=BrandListSchema, dependencies=[Depends(JWTBearer())])
# async def getBrands(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:

#         userdata = auth(request=request)

#         # data: BrandsModel = await BrandsHelper.getBrands(db=db, user_id=userdata['id'])
#         data: BrandsModel = db.query(BrandsModel, UserModel.name.label('seller')).join(UserModel, UserModel.id == BrandsModel.user_id).filter(
#             BrandsModel.user_id == userdata['id']).order_by(BrandsModel.id.desc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_202_ACCEPTED, "total_brands": 0, "brands": [], "current_page": page, "total_pages": 0}

#         brands: BrandsModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         branddata = []

#         for brand in brands:

#             status = False
#             if(brand.BrandsModel.status == 1):
#                 status = True

#             b = {
#                 'id': brand.BrandsModel.id,
#                 'name': brand.BrandsModel.name,
#                 'logo': brand.BrandsModel.logo,
#                 'status': status
#             }
#             branddata.append(b)

#         # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = round((total_records/limit), 0)

#         # Check total count Similarity of records
#         check = (total_pages * limit)

#         if(check == total_records):
#             pass
#         else:
#             total_pages = total_pages + 1

#         return {"status_code": HTTP_202_ACCEPTED, "total_brands": total_records, "brands": branddata, "current_page": page, "total_pages": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_202_ACCEPTED, "total_brands": 0, "brands": [], "current_page": page, "total_pages": 0}


# Add New Brand
# @seller_brand_router.post("/add", dependencies=[Depends(JWTBearer())])
# async def addBrands(request: Request, name: str = Form(...),  logo: UploadFile = File(...), db: Session = Depends(get_db)):
#     try:

#         userdata = auth(request=request)

#         file = logo.filename.split(".")
#         extension = file[1]
#         uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
#             datetime.now().strftime('%Y-%d-%M-%h-%s')

#         filename = f"{uniqueID}.{file[1]}"

#         # Set Path Of image
#         file_name = getcwd()+"/app/static/brands/"+filename

#         # Upload image in path
#         with open(file_name, 'wb+') as f:
#             f.write(logo.file.read())
#             f.close()

#         uploaded_file = getcwd()+"/app/static/brands/" + str(filename)
#         filename = str(filename)
#         # Upload Photo
#         MediaHelper.uploadBrandPhoto(
#             uploaded_file=uploaded_file, file_name=filename)

#         # Remove Original Image from Path

#         unlink(uploaded_file)

#         file_path = str(
#             linode_obj_config['endpoint_url'])+'/brands/'+str(filename)

#         dbbrand = BrandsModel(
#             user_id=userdata['id'],
#             name=name,
#             logo=file_path,
#             slug=name.replace(" ", "-").lower(),
#             status=0
#         )
#         db.add(dbbrand)
#         db.commit()
#         db.refresh(dbbrand)

#         return {"status_code": HTTP_202_ACCEPTED, "message": "Brand added successfully..."}

#     except Exception as e:
#         print(e)


# Edit Brand Data
# @seller_brand_router.get("/edit/{brand_id}", dependencies=[Depends(JWTBearer())])
# async def editBrands(request: Request, brand_id: int, db: Session = Depends(get_db)):
#     try:

#         brand = db.query(BrandsModel).filter(
#             BrandsModel.id == brand_id).first()

#         brand = {
#             "id": brand.id,
#             'name': brand.name,
#             'logo': brand.logo
#         }

#         return {"status_code": HTTP_202_ACCEPTED, "brand": brand}
#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "brand": {}}

# Update Brand Data


# @seller_brand_router.post("/update", dependencies=[Depends(JWTBearer())])
# async def updateBrand(request: Request, logo: Optional[UploadFile] = File(None), id: int = Form(...), name: str = Form(...), db: Session = Depends(get_db)):
#     try:

#         # User
#         user = auth(request=request)

#         brand = db.query(BrandsModel).filter(BrandsModel.id == id).first()

#         if(user['id'] != brand.user_id):
#             return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

#         if(logo != None):
#             file = logo.filename.split(".")
#             extension = file[1]
#             uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
#                 datetime.now().strftime('%Y-%d-%M-%h-%s')

#             filename = f"{uniqueID}.{file[1]}"

#             # Set Path Of image
#             file_name = getcwd()+"/app/static/brands/"+filename

#             # Upload image in path
#             with open(file_name, 'wb+') as f:
#                 f.write(logo.file.read())
#                 f.close()

#             uploaded_file = getcwd()+"/app/static/brands/" + str(filename)
#             filename = str(filename)
#             # Upload Photo
#             MediaHelper.uploadBrandPhoto(
#                 uploaded_file=uploaded_file, file_name=filename)

#             # Remove Original Image from Path

#             unlink(uploaded_file)

#             file_path = str(
#                 linode_obj_config['endpoint_url'])+'/brands/'+str(filename)

#         brand.name = name
#         brand.slug = name.replace(" ", "-").lower(),
#         if(logo != None):
#             brand.logo = file_path

#         db.flush()
#         db.commit()

#         return {"status_code": HTTP_202_ACCEPTED, "message": "Brand Updated Successfully"}

#     except Exception as e:
#         print(e)
