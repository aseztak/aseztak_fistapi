from fastapi import APIRouter,  Depends
from app.api.helpers.page import PageHelper
from app.db.config import get_db
from sqlalchemy.orm.session import Session

from app.db.schemas.page_schema import PageDataSchema

seller_page_router = APIRouter()


@seller_page_router.get("/get/{pageSlug}", response_model=PageDataSchema)
async def get_page(pageSlug: str, db: Session = Depends(get_db)):

    try:
        data = await PageHelper.get_page(db=db, slug=pageSlug)

        return {'page': data}

    except Exception as e:
        print(e)
