from starlette.requests import Request
from app.db.models.categories import CategoriesModel
from app.db.models.media import ProductMediaModel
from app.services.auth import auth
from starlette.status import HTTP_200_OK
from app.db.models.products import InventoryModel, ProductModel, ProductPricingModel
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.db.schemas.product_schema import SellerAllProductList, SellerProductSearchSchema, SellerSearchAllProducts, SellerAllProductListSchema
from app.api.helpers.products import *
from app.services.auth import auth
from fastapi import APIRouter,  Depends
from app.api.util.calculation import floatingValue
from app.db.models.product_rejection import ProductRejectionModel

seller_product_router = APIRouter()



# Get Product Rejecttion messages
@seller_product_router.get("/rejection/message/{product_id}", dependencies=[Depends(JWTBearer())])
async def RejectMessages(request: Request, product_id: int, db: Session = Depends(get_db)):
    try:

        reject_messages = db.query(ProductRejectionModel).filter(
            ProductRejectionModel.product_id == product_id).all()

        messages = []
        for message in reject_messages:
            msz = {
                'type': message.type,
                'message': message.message
            }
            messages.append(msz)

        return {"status_code": HTTP_200_OK, "reject_messages": messages}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "reject_messages": []}

# Search Products


@seller_product_router.post("/search", response_model=SellerAllProductListSchema, dependencies=[Depends(JWTBearer())])
async def searchProducts(request: Request, search: SellerProductSearchSchema, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        data: ProductModel = await ProductsHelper.getProductList(db=db, category_id=search.category_id, seller_id=userdata['id'], status=search.status, search=search.search, from_date=search.from_date, to_date=search.to_date)

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "total_items": 0, "category": "", "items": [], "current_page": search.page, "total_pages": 0}

        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == search.category_id).first()

        products: ProductModel = data.limit(
            limit=search.limit).offset((search.page - 1) * search.limit).all()

        productdata = []
        for product in products:

            price = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == product.id).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            stock = False
            total = 0.00
            moq = 0
            unit = ''
            if(price):
                total = (price.price * price.tax) / 100 + price.price

                # Check Product for Stock or out Stock
                checkinventory = db.query(ProductPricingModel).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == product.id).filter(InventoryModel.out_of_stock == 0).filter(ProductPricingModel.deleted_at.is_(None)).first()

                stock = False
                if(checkinventory is not None):
                    stock = True

                moq = price.moq
                unit = price.unit

            status = 'Approved'
            if(product.status == 1):
                status = 'Inactive'
            elif(product.status == 98):
                status = 'Rejected'

            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                ProductMediaModel.default_img == 1).filter(ProductMediaModel.deleted_at.is_(None)).first()

            if(image == None):
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()

            pro = {
                'id': product.id,
                'name': product.title,
                'description': product.short_description,
                'image': image.file_path,
                'price': floatingValue(round(total, 2)),
                'moq': moq,
                'unit': unit,
                'stock': stock,
                'status': status,
                'created_at': product.created_at.strftime("%B %d %Y"),


            }

            productdata.append(pro)

        total_records = data.count()
        # Number of Total Pages
        total_pages = str(round((total_records/search.limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK,  "total_items": total_records, "category": category.name, "items": productdata, "current_page": search.page, "total_pages": round(total_pages)}

    except Exception as e:
        print(e)


# All Products
@seller_product_router.get("/all", response_model=SellerAllProductList, dependencies=[Depends(JWTBearer())])
async def AllProducts(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        userdata = auth(request=request)

        data: ProductModel = await ProductsHelper.getSellerAllProducts(db=db, seller_id=userdata['id'])
        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "total_items": 0, "category": "", "items": [], "current_page": page, "total_pages": 0}

        products: ProductModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        productdata = []
        for product in products:

            price = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == product.id).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            stock = False
            total = 0.00
            moq = 0
            unit = ''
            if(price):
                total = (price.price * price.tax) / 100 + price.price

                # Check Product for Stock or out Stock
                checkinventory = db.query(ProductPricingModel).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == product.id).filter(InventoryModel.out_of_stock == 0).first()

                stock = False
                if(checkinventory is not None):
                    stock = True

                moq = price.moq
                unit = price.unit

            status = 'Approved'
            if(product.status == 1):
                status = 'Inactive'
            elif(product.status == 98):
                status = 'Rejected'

            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                ProductMediaModel.default_img == 1).filter(ProductMediaModel.deleted_at.is_(None)).first()

            if(image == None):
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()

            pro = {
                'id': product.id,
                'name': product.title,
                'description': product.short_description,
                'image': image.file_path,
                'price': floatingValue(round(total, 2)),
                'moq': moq,
                'unit': unit,
                'stock': stock,
                'status': status,
                'created_at': product.created_at.strftime("%B %d %Y"),


            }

            productdata.append(pro)

        total_records = data.count()
        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK,  "total_items": total_records, "items": productdata, "current_page": page, "total_pages": round(total_pages)}

    except Exception as e:
        print(e)


# Search All Products
@seller_product_router.post("/search/all", response_model=SellerAllProductList, dependencies=[Depends(JWTBearer())])
async def AllProducts(request: Request, search: SellerSearchAllProducts, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        data: ProductModel = await ProductsHelper.getSellerAllProducts(db=db, seller_id=userdata['id'], search=search.search, from_date=search.from_date, to_date=search.to_date)

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "total_items": 0, "category": "", "items": [], "current_page": search.page, "total_pages": 0}

        products: ProductModel = data.limit(
            limit=search.limit).offset((search.page - 1) * search.limit).all()

        productdata = []
        for product in products:

            price = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == product.id).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            stock = False
            total = 0.00
            moq = 0
            unit = ''
            if(price):
                total = (price.price * price.tax) / 100 + price.price

                # Check Product for Stock or out Stock
                checkinventory = db.query(ProductPricingModel).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == product.id).filter(InventoryModel.out_of_stock == 0).first()

                stock = False
                if(checkinventory is not None):
                    stock = True

                moq = price.moq
                unit = price.unit

            status = 'Approved'
            if(product.status == 1):
                status = 'Inactive'
            elif(product.status == 98):
                status = 'Rejected'

            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                ProductMediaModel.default_img == 1).filter(ProductMediaModel.deleted_at.is_(None)).first()

            if(image == None):
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()

            pro = {
                'id': product.id,
                'name': product.title,
                'description': product.short_description,
                'image': image.file_path,
                'price': floatingValue(round(total, 2)),
                'moq': moq,
                'unit': unit,
                'stock': stock,
                'status': status,
                'created_at': product.created_at.strftime("%B %d %Y"),


            }

            productdata.append(pro)

        total_records = data.count()
        # Number of Total Pages
        total_pages = str(round((total_records/search.limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK,  "total_items": total_records, "items": productdata, "current_page": search.page, "total_pages": round(total_pages)}

    except Exception as e:
        print(e)

