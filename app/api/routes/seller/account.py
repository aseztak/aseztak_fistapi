
# from starlette.requests import Request
# from starlette.status import HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED, HTTP_404_NOT_FOUND, HTTP_200_OK
# from app.api.helpers.users import get_users, me, userExist
# from app.db.models.bank_accounts import BankAccountsModel
# from app.db.models.billing_address import BillingAddressModel


# from app.db.schemas.user_schema import BillingAddressSchema, UpdateBankDetailSchema, UpdateBillingAddressSchema, UserBankDetailsSchema, UserSchema, UpdateProfile, UpdateMobileSchema, UpdateMobileNumberSchema
# from typing import Dict
# from app.services.auth_bearer import JWTBearer
# from app.services.auth_handler import signJWT
# from app.db.config import SessionLocal, get_db
# from sqlalchemy.orm.session import Session
# from app.api.helpers.users import *
# from app.services.auth import auth

# from fastapi import APIRouter

# seller_account_router = APIRouter()


#CLOSED 2024-01-08
# @seller_account_router.get("/bank/account", response_model=UserBankDetailsSchema, dependencies=[Depends(JWTBearer())])
# async def bankAccount(request: Request, db: Session = Depends(get_db)):
#     try:
#         userdata = auth(request=request)

#         data: BankAccountsModel = db.query(BankAccountsModel).filter(
#             BankAccountsModel.user_id == userdata['id']).first()

#         if(data is None):
#             bank_account = {
#                 'account_holder_name': '',
#                 'acc_no': '',
#                 'bank': '',
#                 'branch': '',
#                 'ifsc_code': '',
#                 'account_type': ''
#             }

#             return {"status_code": HTTP_200_OK, "account": bank_account}

#         if(data):

#             if(data.account_holder_name is None):
#                 data.account_holder_name = ''
#             if(data.acc_no is None):
#                 data.acc_no = ''
#             if(data.bank is None):
#                 data.bank = ''

#             if(data.branch is None):
#                 data.branch = ''
#             if(data.ifsc_code is None):
#                 data.ifsc_code = ''

#             if('savings' in str(data.account_type)):
#                 account_type = 'savings'
#             else:
#                 account_type = 'current'

#             bank_account = {
#                 'account_holder_name': data.account_holder_name,
#                 'acc_no': data.acc_no,
#                 'bank': data.bank,
#                 'branch': data.branch,
#                 'ifsc_code': data.ifsc_code,
#                 'account_type': account_type
#             }

#             return {"status_code": HTTP_200_OK, "account": bank_account}
#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "account": {}}

# # Update user bank Account details


# @seller_account_router.post("/update/bank/account", dependencies=[Depends(JWTBearer())])
# async def updateBankAccount(request: Request, data: UpdateBankDetailSchema, db: Session = Depends(get_db)):
#     try:

#         userdata = auth(request=request)

#         dbbank = db.query(BankAccountsModel).filter(
#             BankAccountsModel.user_id == userdata['id']).first()

#         # if(userdata['id'] != dbbank.user_id):
#         #     return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

#         if(dbbank is not None):
#             dbbank.account_holder_name = data.account_holder_name
#             dbbank.acc_no = data.acc_no
#             dbbank.bank = data.bank
#             dbbank.branch = data.branch
#             dbbank.ifsc_code = data.ifsc_code
#             dbbank.account_type = data.account_type

#             db.flush()
#             db.commit()
#         else:
#             dbbank = BankAccountsModel(
#                 user_id=userdata['id'],
#                 account_holder_name=data.account_holder_name,
#                 acc_no=data.acc_no,
#                 bank=data.bank,
#                 branch=data.branch,
#                 ifsc_code=data.ifsc_code,
#                 account_type=data.account_type
#             )
#             db.add(dbbank)
#             db.commit()
#             db.refresh(dbbank)

#         bank_account = {
#             'account_holder_name': dbbank.account_holder_name,
#             'acc_no': dbbank.acc_no,
#             'bank': dbbank.bank,
#             'branch': dbbank.branch,
#             'ifsc_code': dbbank.ifsc_code,
#             'account_type': dbbank.account_type
#         }

#         return {"status_code": HTTP_200_OK, "account": bank_account}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Input"}

# # user Billing Address


# @seller_account_router.get("/billing/address", response_model=BillingAddressSchema, dependencies=[Depends(JWTBearer())])
# async def UserBillingAddress(request: Request, db: Session = Depends(get_db)):
#     try:

#         userdata = auth(request=request)

#         data: BillingAddressModel = db.query(BillingAddressModel).filter(
#             BillingAddressModel.user_id == userdata['id']).first()

#         if(data):

#             billing = {
#                 'name': data.name,
#                 'address': data.address,
#                 'city': data.city,
#                 'state': data.state,
#                 'pincode': data.pincode
#             }

#         else:
#             billing = {
#                 'name': '',
#                 'address': '',
#                 'city': '',
#                 'state': '',
#                 'pincode': ''
#             }

#         return {"status_code": HTTP_202_ACCEPTED, "billing_address": billing}

#     except Exception as e:
#         return {"status_code": HTTP_404_NOT_FOUND, "message": "No data found"}

# # update billing address


# @seller_account_router.post("/update/billing/address", dependencies=[Depends(JWTBearer())])
# async def updateBankAccount(request: Request, data: UpdateBillingAddressSchema, db: Session = Depends(get_db)):
#     try:

#         userdata = auth(request=request)

#         dbbilling = db.query(BillingAddressModel).filter(
#             BillingAddressModel.user_id == userdata['id']).first()

#         # if(userdata['id'] != dbbilling.user_id):
#         #     return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

#         if(dbbilling):

#             dbbilling.name = data.name
#             dbbilling.address = data.address
#             dbbilling.city = data.city
#             dbbilling.state = data.state
#             dbbilling.pincode = data.pincode

#             db.flush()
#             db.commit()
#         else:
#             dbbilling = BillingAddressModel(
#                 user_id=userdata['id'],
#                 name=data.name,
#                 address=data.address,
#                 city=data.city,
#                 state=data.state,
#                 pincode=data.pincode
#             )

#             db.add(dbbilling)
#             db.commit()
#             db.refresh(dbbilling)

#         billing_address = {
#             'id': dbbilling.id,
#             'name': dbbilling.name,
#             'address': dbbilling.address,
#             'city': dbbilling.city,
#             'state': dbbilling.state,
#             'pincode': dbbilling.pincode
#         }

#         return {"status_code": HTTP_200_OK, "billing_address": billing_address}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Input"}
