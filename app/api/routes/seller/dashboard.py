from fastapi import APIRouter,  Depends,  Request
from sqlalchemy.orm.session import Session
from sqlalchemy.sql.functions import func
from app.api.helpers.auth import *
from app.api.helpers.orderstaticstatus import OrderStatus
from app.api.util.calculation import productPricecalculation, roundOf

from app.db.config import get_db
from app.db.models.media import ProductMediaModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.order_status import OrderStatusModel
from app.db.models.orders import OrdersModel
from app.db.models.products import InventoryModel, ProductModel, ProductPricingModel
from app.db.schemas.auth_schema import *
from datetime import datetime
from app.services.auth import auth
from app.resources.strings import *
from app.api.helpers.users import *
from app.services.auth_bearer import JWTBearer
from datetime import date, timedelta
from starlette.requests import Request
from sqlalchemy.orm import joinedload
from app.api.helpers.services import AsezServices
from app.api.helpers.products import ProductsHelper
seller_dashboard_router = APIRouter()


@seller_dashboard_router.get("/", dependencies=[Depends(JWTBearer())])
async def dashboard(request: Request, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        # Active Order
        active_orders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
            ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == userdata['id']).filter(OrderItemsModel.status == 0).group_by(OrderItemsModel.order_id).count()

        active_orders = {
            'icon': str(request.base_url)+"static/icons/Active Order.svg",
            'count': active_orders
        }
        # total products
        total_products = db.query(ProductModel).filter(
            ProductModel.userid == userdata['id']).filter(ProductModel.status != 98).count()

        total_products = {
            'icon': str(request.base_url)+"static/icons/TotalProducts.svg",
            'count': total_products
        }
        # Sale So Far (Total Sale)
        totalsales = db.execute("SELECT order_items.quantity, (order_items.price * order_items.tax) / 100 + order_items.price as price FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN products ON products.id = order_items.product_id LEFT JOIN order_status ON order_status.order_id = orders.id WHERE order_status.status >= 70 and order_status.status != 980 and products.userid =:param", {
            "param": userdata['id']
        }).all()
        totalsalesamount = 0
        for totalsale in totalsales:
            total = round(totalsale.price) * totalsale.quantity
            totalsalesamount += total

        total_sale = round(totalsalesamount)

        total_sale = {
            'icon': str(request.base_url)+"static/icons/Sales.svg",
            'sale': total_sale
        }

        # Today Orders
        today = date.today()
        # YY-mm-dd
        today = today.strftime("%Y-%m-%d")

        # Today Orders
        todayorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
            ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == userdata['id']).filter(func.date_format(OrdersModel.created_at, '%Y-%m-%d') == today).group_by(OrdersModel.id).count()

        # Today Cancelled Orders
        todaycancelledorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
            ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == userdata['id']).filter(func.date_format(OrderStatusModel.created_at, '%Y-%m-%d') == today).filter(OrderStatusModel.status == 980).group_by(OrdersModel.id).count()

        # Total Return Orders
        todayreturnOrders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == userdata['id']).filter(func.date_format(OrderStatusModel.created_at, '%Y-%m-%d') == today).filter(
            OrderStatusModel.status >= 90).filter(OrderStatusModel.status != 980).group_by(OrderStatusModel.order_id).count()

        # Today Sale
        todaysales = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
            ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == userdata['id']).filter(func.date_format(OrdersModel.created_at, '%Y-%m-%d') == today).filter(OrderItemsModel.status < 980).group_by(OrdersModel.id).all()

        item_total_amount = 0
        for todaysale in todaysales:

            items = todaysale.order_items.filter(
                OrderItemsModel.status != 980).all()
            total_amount = 0
            for item in items:
                pricingdata = item.uuid.split('-')
                pricingdata = db.query(ProductPricingModel).where(ProductPricingModel.id == pricingdata[1]).where(
                    ProductPricingModel.deleted_at.is_(None)).first()
                # Get Product Detail
                productdata = db.query(ProductModel).where(
                    ProductModel.id == item.product_id).first()

                # Aseztak Service
                # aseztak_service = Services.aseztak_services(
                #     item.created_at, db=db)
                today_date = item.created_at.strftime(
                    '%Y-%m-%d')
                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                if(aseztak_service is None):

                    # Calculate Product Price
                    product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
                                                            gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=todaysale.app_version)

                    total_amount += product_price * item.quantity
                else:
                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=todaysale.app_version, order_item_id=item.id)
                    # Calculate Product Price
                    # product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
                    #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=todaysale.app_version)

                    total_amount += product_price * item.quantity

            item_total_amount += total_amount

        todaytotalsales = round(item_total_amount)

        # Today
        today = {
            'orders': {
                "icon": str(request.base_url)+"static/icons/order.svg",
                "count": todayorders,
            },
            'cancelled_orders': {
                'icon': str(request.base_url)+"static/icons/cancel_orders.svg",
                'count': todaycancelledorders
            },

            'return_orders': {
                'icon': str(request.base_url)+"static/icons/return_orders.svg",
                'count': todayreturnOrders
            },
            'sales': {
                'icon': str(request.base_url)+"static/icons/Sales.svg",
                'count': todaytotalsales
            }
        }

        # Overall
        # Total Orders
        totalorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
            ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == userdata['id']).group_by(OrdersModel.id).count()

        # total Cancelled orders
        totalcancelledorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == userdata['id']).filter(
            OrderStatusModel.status == 980).group_by(OrderStatusModel.order_id).count()

        # Total Return Orders
        totalreturnOrders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == userdata['id']).filter(
            OrderStatusModel.status >= 90).filter(OrderStatusModel.status != 980).group_by(OrderStatusModel.order_id).count()

        overall = {
            'orders': {
                'icon': str(request.base_url)+"static/icons/order.svg",
                'count': totalorders,
            },
            'cancelled_orders': {
                'icon': str(request.base_url)+"static/icons/cancel_orders.svg",
                'count': totalcancelledorders,
            },
            'returned_orders': {
                'icon': str(request.base_url)+"static/icons/return_orders.svg",
                'count': totalreturnOrders,
            },
            'sales': {
                'icon': str(request.base_url)+"static/icons/Sales.svg",
                'count': total_sale['sale']
            }
        }

        # Recent products
        recent_products = []
        productdata = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(
            ProductModel.userid == userdata['id']).filter(ProductModel.status != 98).group_by(ProductModel.id).order_by(ProductModel.id.desc()).limit(5).all()

        for pro in productdata:
            if(len(pro.product_pricing) != 0):
                price = pro.product_pricing[0]
            else:
                price = None

            # image = pro.images[0]

            if(len(pro.images) != 0):
                img = pro.images[0].file_path
            else:
                img = ''
            if(price):

                invetory = db.query(InventoryModel).filter(
                    InventoryModel.pricing_id == price.id).first()

                stock = True
                if(invetory is not None and invetory.out_of_stock == 1):
                    stock = False

                pp = (price.price * price.tax) / 100 + price.price

                p = {
                    'id': pro.id,
                    'name': pro.title,
                    'description': pro.short_description,
                    'created_at': pro.created_at.strftime("%B %d %Y"),
                    'price': round(pp),
                    'moq': price.moq,
                    'unit': price.unit,
                    'stock': stock,
                    'image': img
                }

                recent_products.append(p)

        # Recent Orders
        recent_orers = []
        ordersdata = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
            ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == userdata['id']).group_by(OrdersModel.id).order_by(OrdersModel.id.desc()).limit(5).all()

        for ord in ordersdata:
            items = ord.order_items.all()

            # Current Status (Order) --Rahul
            current_status = ord.order_status.order_by(
                OrderStatusModel.id.desc()).first()
            statuslist = OrderStatus.AllstatusList()
            statustitle = ''
            for s_title in statuslist:
                if(current_status.status == s_title['status_id']):
                    statustitle = s_title['status_title']

            total_amount = 0
            img = ''

            for item in items:
                pricingdata = item.uuid.split('-')
                pricingdata = db.query(ProductPricingModel).where(ProductPricingModel.id == pricingdata[1]).where(
                    ProductPricingModel.deleted_at.is_(None)).first()
                # Get Product Detail
                productdata = db.query(ProductModel).where(
                    ProductModel.id == item.product_id).first()

                filename = ''
                if(item.images != None):
                    image = db.query(ProductMediaModel).filter(
                        ProductMediaModel.id == item.images.strip("[]")).first()

                    if(image is not None):
                        filename = image.file_path

                    else:
                        image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == item.product_id).order_by(
                            ProductMediaModel.default_img.desc()).filter(ProductMediaModel.deleted_at.is_(None)).first()

                        filename = image.file_path

                else:

                    if(product_price and product_price.default_image != 0):
                        image = db.query(ProductMediaModel).filter(
                            ProductMediaModel.id == product_price.default_image).first()

                        if(image is not None):
                            filename = image.file_path
                    else:

                        image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == item.product_id).order_by(
                            ProductMediaModel.default_img.desc()).filter(ProductMediaModel.deleted_at.is_(None)).first()
                        if(image is not None):
                            filename = image.file_path

                img = filename
                # Aseztak Service
                # aseztak_service = Services.aseztak_services(
                #     item.created_at, db=db)
                today_date = item.created_at.strftime(
                    '%Y-%m-%d')
                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                if(aseztak_service is None):

                    # Calculate Product Price
                    product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
                                                            gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=ord.app_version)

                    total_amount += product_price * item.quantity
                else:
                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=ord.app_version, order_item_id=item.id)
                    # Calculate Product Price
                    # product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
                    #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=ord.app_version)

                    total_amount += product_price * item.quantity

            # Order Confirmed
            buyer = db.query(UserModel).filter(
                UserModel.id == ord.user_id).first()
            if(buyer.confirm == 'No'):
                order_confirmed = {
                    'value': 'No',
                    'text': 'Not Confirmed'
                }
            else:
                order_confirmed = {
                    'value': 'Yes',
                    'text': 'Confirmed'
                }

            orderdetail = {
                'id': ord.id,
                'order_number': ord.order_number,
                'image': img,
                'order_confirmed': order_confirmed,
                'total_amount': round(total_amount),
                'items': len(items),
                'created_at':  ord.created_at.strftime("%B %d %Y %I:%M%p"),
                'status': statustitle

            }

            recent_orers.append(orderdetail)

        return {"active_orders": active_orders, "total_products": total_products, "total_sale": total_sale, "today": today, "overall": overall, "recent_products": recent_products, "recent_orders": recent_orers}

    except Exception as e:
        print(e)


@seller_dashboard_router.get("/statistic", dependencies=[Depends(JWTBearer())])
async def statistic(request: Request, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        # Statistic Products
        m = datetime.now().month
        y = datetime.now().year
        if(m == 12):
            m = 11

        ndays = (date(y, m+1, 1) - date(y, m, 1)).days
        d1 = date(y, m, 1)
        d2 = date(y, m, ndays)
        delta = d2 - d1

        products = []
        for d in range(delta.days - 17 + 1):
            newdate = (d1 + timedelta(days=d+1)).strftime('%Y-%m-%d')

            month = (d1 + timedelta(days=d)).strftime('%b')

            product = db.execute('SELECT COUNT(*) as total_products FROM products WHERE DATE_FORMAT(created_at, "%Y-%m-%d") >=:param and DATE_FORMAT(created_at, "%Y-%m-%d") <=:param and userid =:param2', {
                "param": ''+str(newdate)+'', "param2": userdata['id']
            }).first()

            pr = {
                'day': d+1,
                'month': month,
                'count': product.total_products
            }

            products.append(pr)

        # Statistic Orders
        orders = []

        sevendays = (datetime.now() -
                     timedelta(days=6))

        for d in range(7):

            dayint = (sevendays + timedelta(days=d)).strftime('%d')

            datestr = (sevendays + timedelta(days=d)).strftime('%d %b')

            newdate = (sevendays + timedelta(days=d)).strftime('%Y-%m-%d')

            order = db.execute('SELECT orders.id, orders.app_version FROM orders LEFT JOIN order_items as item ON item.order_id = orders.id LEFT JOIN products as product ON product.id = item.product_id  WHERE orders.status = 1 AND product.userid =:param and DATE_FORMAT(orders.created_at, "%Y-%m-%d") >=:param1 AND DATE_FORMAT(orders.created_at, "%Y-%m-%d") <=:param1 and item.status != 980 GROUP BY orders.id', {
                "param": userdata['id'], "param1": newdate
            }).all()
            order_total_amount = 0

            if(len(order) > 0):
                for orderdata in order:

                    items = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == orderdata.id).filter(
                        OrderItemsModel.status != 980).all()
                    total_amount = 0

                    for item in items:
                        pricingdata = item.uuid.split('-')
                        pricingdata = db.query(ProductPricingModel).filter(
                            ProductPricingModel.id == pricingdata[1]).first()
                        # Get Product Detail
                        productdata = db.query(ProductModel).where(
                            ProductModel.id == item.product_id).first()

                        # Aseztak Service
                        # aseztak_service = Services.aseztak_services(
                        #     item.created_at, db=db)
                        today_date = item.created_at.strftime(
                            '%Y-%m-%d')
                        aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                        if(aseztak_service is None):

                            # Calculate Product Price
                            product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
                                                                    gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=orderdata.app_version)

                            total_amount += product_price * item.quantity
                        else:
                            product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=orderdata.app_version, order_item_id=item.id)
                            # Calculate Product Price
                            # product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
                            #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=orderdata.app_version)

                            total_amount += product_price * item.quantity

                    if(total_amount != 0):
                        order_total_amount += total_amount
            pr = {
                'date': str(datestr),
                'day': int(dayint),
                'month': 4,
                'count': str(len(order)),
                'sale': str(roundOf(order_total_amount))
            }

            orders.append(pr)

        statistic = {
            'products': products,
            'orders': orders
        }

        # return [(d1 + timedelta(days=i)).strftime('%Y-%m-%d') for i in range(delta.days + 1)]

        # return d.strftime("%m")

        # for month in range(1, 13):

        #     first_day = first_day_of_month(date(2021, month, 1))
        #     last_day = last_day_of_month(date(2021, month, 1))

        #     product = db.execute('SELECT COUNT(*) as total_products FROM products WHERE DATE_FORMAT(created_at, "%Y-%m-%d") BETWEEN :param AND :param1 and userid =:param2', {
        #         "param": ''+str(first_day)+'', "param1": ''+str(last_day)+'', "param2": 65
        #     }).first()

        #     if(product.total_products != 0):
        #         monthlywise = {
        #             'month': calendar.month_abbr[month],
        #             'count': product.total_products
        #         }

        #         total_products.append(monthlywise)

        # return {"products": total_products}

        return {"statistic": statistic}
    except Exception as e:
        print(e)


async def calculateTotalAmount(order_id: int, db: Session = Depends(get_db)):
    try:
        items = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == order_id).filter(OrderItemsModel.status < 980).all()

        total_amount = 0
        for item in items:
            price = (item.price * item.tax) / 100 + item.price
            total = round(price) * item.quantity
            total_amount += total

        return total_amount

    except Exception as e:
        print(e)


def first_day_of_month(any_day):
    first_date = any_day.replace(day=1)

    return first_date


def last_day_of_month(any_day):
    last_date = any_day.replace(day=28) + timedelta(days=4)
    return last_date - timedelta(days=last_date.day)
