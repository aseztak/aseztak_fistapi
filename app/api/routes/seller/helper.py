from starlette.requests import Request
from starlette.status import HTTP_200_OK
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.products import *
from fastapi import APIRouter,  Depends
from app.db.models.faqs import FaqsModel
from starlette.status import HTTP_200_OK
from app.db.schemas.seller_helper import FaqsDataSchema


seller_helper_router = APIRouter()


@seller_helper_router.get("/", response_model=FaqsDataSchema, dependencies=[Depends(JWTBearer())])
async def getHelper(request: Request, page: int = 1, limit: int = 10, db: Session = Depends(get_db)):
    try:
        faqs = db.query(FaqsModel).filter(FaqsModel.flag == 'Seller').order_by(
            FaqsModel.id.desc())

        if(faqs.count() == 0):
            return {"status_code": HTTP_200_OK, "message": "Not found", "faqs": faq_data, "page": page, "total_records": total_records, "total_pages": total_pages}
        else:
            faqdata: FaqsModel = faqs.limit(
                limit=limit).offset((page - 1) * limit).all()

            # Total Records
            total_records = faqs.count()

            # Number of Total Pages
            total_pages = round((total_records/limit), 0)

            # Check total count Similarity of records
            check = (total_pages * limit)
            if(check == total_records):
                pass
            else:
                total_pages = total_pages + 1

            faq_data = []
            for faq in faqdata:
                video = ''
                if(faq.video is not None):
                    video = faq.video

                faq = {
                    'question': faq.question,
                    'answer': faq.answer,
                    'video': video
                }

                faq_data.append(faq)

            return {"status_code": HTTP_200_OK, "message": "Success", "faqs": faq_data, "page": page, "total_records": total_records, "total_pages": total_pages}

    except Exception as e:
        print(e)


# @seller_helper_router.get("/", dependencies=[Depends(JWTBearer())])
# async def getHelper(request: Request, db: Session = Depends(get_db)):
#     try:

#         videos = db.query(SellerHelperModel).order_by(
#             SellerHelperModel.id.desc()).all()

#         video_list = []
#         for vidoe in videos:
#             v = {
#                 'id': vidoe.id,
#                 'title': vidoe.title,
#                 'description': vidoe.description,
#                 'video': vidoe.video
#             }
#             video_list.append(v)

#         return {"status_code": HTTP_200_OK, "videos": video_list, "total": len(videos)}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "videos": {}, "total": 0}
