
from datetime import datetime
from starlette.requests import Request
from starlette.types import Message
from app.api.util.check_user import CheckUser
from app.db.models.order_items import OrderItemsModel
from app.db.models.order_status import OrderStatusModel
from app.db.models.orders import OrdersModel
from starlette.status import HTTP_200_OK, HTTP_304_NOT_MODIFIED
from app.db.models.products import ProductModel
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.products import *
from app.db.schemas.orders_schema import PackedOrders
from fastapi import APIRouter,  Depends
from starlette.requests import Request
from app.api.util.message import Message
seller_order_router = APIRouter()



# Packed Order
@seller_order_router.post("/packed", dependencies=[Depends(JWTBearer())])
async def packedOrders(request: Request, data: PackedOrders, db: Session = Depends(get_db)):
    try:

        # Check user
        checkuser = CheckUser.CheckSellerOrder(
            request=request, order_id=data.order_id, db=db)
        if(checkuser == False):
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

        db_orderstatus = OrderStatusModel(
            order_id=data.order_id,
            status=40,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(db_orderstatus)
        db.commit()
        db.refresh(db_orderstatus)

        # Order Details
        order = db.query(OrdersModel).filter(
            OrdersModel.id == data.order_id).first()
        # Buyer
        buyerdetails = db.query(UserModel).filter(
            UserModel.id == order.user_id).first()
        # Check Order Items
        checkorderItems = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status != 980).all()
        product_title = db.query(ProductModel).filter(
            ProductModel.id == checkorderItems[0].product_id).first()
        notification_image = db.query(ProductMediaModel).filter(
            ProductMediaModel.model_id == product_title.id).filter(ProductMediaModel.deleted_at.is_(None)).first()

        checkorderItems = len(checkorderItems)
        if(checkorderItems > 1):
            checkorderItems = (checkorderItems - 1)

            product_title = str(
                product_title.title)

            product_title = product_title[:15]

            product_title = str(
                product_title)+' and '+str(checkorderItems)+' more'

        else:
            product_title = str(product_title.title)
            product_title = product_title[:15]
        # Send Notification to Buyer
        b_notification_body = 'Order for ' + str(product_title) + ' with Order ID ' + str(
            order.order_number) + ' has been packed.'
        b_notification_title = 'Order Packed'
        b_path = '/order-detail'
        b_notification_image = notification_image.file_path

        if(buyerdetails.fcm_token is not None):
            await Message.SendOrderNotificationtoBuyer(user_token=buyerdetails.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=order.id)

        return {"status_code": HTTP_200_OK, "message": "Order Packed"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "message": "Invalid Input"}
