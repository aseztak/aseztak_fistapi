from starlette.status import HTTP_202_ACCEPTED, HTTP_404_NOT_FOUND

from app.services.auth_bearer import JWTBearer
from fastapi import APIRouter,  Depends
from app.resources.strings import *
import requests
from requests.structures import CaseInsensitiveDict
import json

address_router = APIRouter()



# Check Shipping
@address_router.get("/check/pincode/{pincode}", dependencies=[Depends(JWTBearer())])
async def checkPincode(pincode: str):
    try:

        pincode = pincode
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/json"

        get_pincode_url = f"{API_URL}c/api/pin-codes/json/?token={API_TOKEN}&filter_codes={pincode}"
        get_data = requests.get(
            get_pincode_url, headers=headers)
        if get_data.status_code == 200:
            json_data = json.loads(get_data.content.decode('utf-8'))

            if(len(json_data['delivery_codes']) == 0):
                return {"status_code": HTTP_404_NOT_FOUND, "message": "Currently no delivery in this area"}
            else:
                return {"status_code": HTTP_202_ACCEPTED, "message": "Success"}
        else:
            return {"status_code": HTTP_404_NOT_FOUND, "message": "Something went wrong"}

        # test_get_response = requests.get(get_test_url)
        # # # inp_post_response = requests.post(get_inp_url , json=request_example)
        # # if inp_post_response .status_code == 200:
        # #     print(json.loads(test_get_response.content.decode('utf-8')))
        return

    except Exception as e:
        print(e)
