from fastapi import APIRouter
test_router = APIRouter()

# @test_router.post("/insert")
# async def insertData(request: Request, name: str = Form(...), mobile: str = Form(...), db: Session = Depends(get_db)):
#     try:
#         db.execute("INSERT INTO test (name, mobile, status) VALUES(:name,:mobile,1)",{
#             "name": name,
#             "mobile": mobile
#         })
#         db.commit()
#         return
#     except Exception as e:
#         raise ValueError(e)


# @test_router.get("/insert", dependencies=[Depends(JWTBearer())])
# async def insertdata(request: Request, db: Session = Depends(get_db), ratelimit: RatelimitClass = Depends(RatelimitClass.rate_limited_handler)):
#     try:
#         db.execute("INSERT INTO test (name, mobile, status) VALUES('tina','1234567890',1)")
#         db.commit()
#         print(request)
#         return "ADDEDD"
#     except Exception as e:
#         print(e)


# @test_router.get("/failed/orders")
# async def failedOrders(db: Session = Depends(get_db)):
#     try:
#         # ONLINE
#         # orders = db.execute("SELECT orders.id, orders.reff, orders.payment_status, orders.payment_method, orders.created_at FROM orders LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN transactions as T ON T.order_ref_id = orders.reff WHERE orders.payment_method != 'PARTIAL' AND (CASE WHEN orders.payment_method = 'ONLINE' THEN orders.payment_status = 1 ELSE orders.payment_method != 'COD' END) GROUP BY orders.id ORDER BY `partial_amount`  DESC").all()
#         # PARTIAL
#         orders = db.execute("SELECT orders.id, orders.reff, orders.payment_status, orders.payment_method, orders.created_at FROM orders LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN transactions as T ON T.order_ref_id = orders.reff WHERE orders.payment_method != 'ONLINE' AND (CASE WHEN orders.payment_method = 'PARTIAL' THEN orders.reff IN (SELECT transactions.order_ref_id FROM transactions WHERE transactions.order_ref_id = orders.reff AND transactions.status != 'TXN_SUCCESS') ELSE orders.payment_method != 'COD' END ) GROUP BY orders.id ORDER BY `partial_amount`  DESC").all()
#         # orders = db.execute(
#         #     "SELECT * FROM orders WHERE orders.payment_method = 'PARTIAL' AND orders.reff NOT IN (SELECT transactions.order_ref_id FROM transactions WHERE transactions.order_ref_id = orders.reff)").all()
#         if(len(orders) > 0):
#             for order in orders:
#                 db_order_failed = OrderFailsModel(
#                     order_id=order.id,
#                     payment_type=order.payment_method,
#                     created_at=order.created_at,
#                     updated_at=order.created_at
#                 )
#                 db.add(db_order_failed)
#                 db.commit()

#                 # Check Transaxtion
#                 checkTxn = db.query(TransactionsModel).filter(TransactionsModel.order_ref_id == order.reff).filter(
#                     TransactionsModel.status != 'TXN_SUCCESS').first()
#                 if(checkTxn is not None):
#                     paytmParams = dict()
#                     paytmParams["body"] = {
#                         "mid": config.PAYTM_MID_KEY,
#                         "orderId": checkTxn.txn_order_id,
#                     }
#                     checksum = PaytmChecksum.generateSignature(
#                         json.dumps(paytmParams["body"]), config.PAYTM_SECRET_KEY)
#                     paytmParams["head"] = {
#                         "signature": checksum
#                     }

#                     post_data = json.dumps(paytmParams)

#                     # for Staging
#                     # url = "https://securegw-stage.paytm.in/v3/order/status"

#                     # for Production
#                     url = "https://securegw.paytm.in/v3/order/status"

#                     response = requests.post(url, data=post_data, headers={
#                         "Content-type": "application/json"}).json()

#                     db_payment_failed = PaymentFailedModel(
#                         order_fail_id=db_order_failed.id,
#                         txn_payment_id=checkTxn.txn_payment_id,
#                         txn_order_id=checkTxn.txn_order_id,
#                         txn_status=response['body']['resultInfo']['resultStatus'],
#                         reason=response['body']['resultInfo']['resultMsg'],
#                         created_at=checkTxn.created_at,
#                         updated_at=checkTxn.updated_at,
#                     )
#                     db.add(db_payment_failed)
#                     db.commit()
#         return "SUCCESS"
#     except Exception as e:
#         print(e)
#         return "FAILED"


# @test_router.get("/payments")
# async def payments():
#     paytmParams = dict()
#     paytmParams["body"] = {
#         "mid": config.PAYTM_MID_KEY,
#         "orderId": '13219211087246423696',
#     }
#     checksum = PaytmChecksum.generateSignature(
#         json.dumps(paytmParams["body"]), config.PAYTM_SECRET_KEY)
#     paytmParams["head"] = {
#         "signature": checksum
#     }

#     post_data = json.dumps(paytmParams)

#     # for Staging
#     # url = "https://securegw-stage.paytm.in/v3/order/status"

#     # for Production
#     url = "https://securegw.paytm.in/v3/order/status"

#     response = requests.post(url, data=post_data, headers={
#         "Content-type": "application/json"}).json()

#     return response
# check_txn_status = response['body']['resultInfo']['resultStatus']

# @test_router.get("/packed/orders")
# async def packedOrders(db: Session = Depends(get_db)):
#     try:
#         check_shipped_orders = db.query(OrdersModel, OrderShippingModel).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).join(
#             OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
#             OrderShippingModel.courier_partner == 'DELHIVERY').having(func.max(OrderStatusModel.status) == 30).group_by(OrderStatusModel.order_id).all()
#         products = []
#         pp = []
#         for order in check_shipped_orders:
#             # Order Items
#             order_items = db.query(OrderItemsModel).filter(
#                 OrderItemsModel.order_id == order.OrdersModel.id).filter(OrderItemsModel.status != 980)
#             count_order_items = order_items.count()

#             # Single Image Item
#             imageitem = order_items.first()
#             productdata = db.query(ProductModel).filter(
#                 ProductModel.id == imageitem.product_id).first()
#             if(productdata):
#                 products.append(imageitem.product_id)
#             else:
#                 pp.append(imageitem.product_id)

#             return "hhhh"
#     except Exception as e:
#         print(e)

# @test_router.get("/update/product/price")
#UPDATE PRICE SQL QUERY (2024-03-04)
# UPDATE product_pricing LEFT JOIN products ON products.id = product_pricing.product_id SET product_pricing.price = product_pricing.price + (product_pricing.price * .02) WHERE products.userid = 71714
# async def updateProductPrice(db: Session = Depends(get_db)):
#     try:
#         products = db.query(ProductModel).filter(
#             ProductModel.status == 51).all()
#         for product in products:
#             pricings = db.query(ProductPricingModel).filter(
#                 ProductPricingModel.product_id == product.id).filter(ProductPricingModel.deleted_at.is_(None))
#             if(pricings.count() > 0):
#                 for pricing in pricings.all():
#                     price = (float(pricing.price) * float(2)) / 100
#                     price = (float(price) + float(pricing.price))
#                     price = float(round(price, 2))
#                     pricing.price = price
#                     db.flush()
#                     db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         print(e)
#         return "FAILED"
#         print(e)

# @test_router.get("/accounts-orders/{user_id}")
# async def accountsOrders(user_id: int, db: Session = Depends(get_db)):
#     try:
#         accounts = db.query(AccountsModel).filter(
#             AccountsModel.txn_description.like('%ORDERED%')).filter(AccountsModel.user_id == user_id).filter(func.date_format(AccountsModel.created_at, '%Y-%b-%d') >= '2022-01-01').all()
#         if(len(accounts) > 0):
#             for account in accounts:
#                 # return account
#                 order_number = account.txn_description.split('ORDERED')
#                 order_number = order_number[1].split(' (')
#                 order_number = order_number[1].split(')')
#                 orderitems = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(
#                     OrdersModel.order_number == order_number[0]).all()
#                 itemtotalamount = 0
#                 if(len(orderitems) > 0):
#                     for item in orderitems:
#                         amounttax = (float(item.price) * float(item.tax)) / 100
#                         amount = (float(amounttax) + float(item.price))
#                         total = float(amount) * float(item.quantity)
#                         itemtotalamount += float(total)
#                     account.txn_amount = round(itemtotalamount, 2)
#                     db.flush()
#                     db.commit()
#         return "UPDATED...."
#         # items
#     except Exception as e:
#         print(e)

# @test_router.get("/wallet-rewards")
# async def walletRewards(db: Session = Depends(get_db)):
#     try:
#         wallets = db.query(WalletModel).filter(WalletModel.status == 0).filter(
#             WalletModel.reff_type == 'ORDER_REWARDS')

#         wallet_reff_ids = []
#         walletreffids = []
#         wallet_reff_final_ids = []
#         today = datetime.now()
#         today = today.strftime("%Y-%m-%d")
#         orderdata = []
#         if(wallets.count() > 0):
#             for wallet in wallets.all():
#                 check_order_exist = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
#                     OrdersModel.reff == wallet.reff_id).having(func.max(OrderStatusModel.status) < 70).group_by(OrderStatusModel.order_id).order_by(OrdersModel.id.desc()).count()
#                 if(check_order_exist == 0):
#                     wallet_reff_ids.append(wallet.reff_id)

#         if(len(wallet_reff_ids) > 0):
#             for wallet in wallet_reff_ids:

#                 check_order_exist = db.query(OrdersModel).join(
#                     OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrdersModel.reff == wallet).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(
#                     func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1)])) < today).group_by(OrderItemsModel.order_id).order_by(OrdersModel.id.desc())
#                 if(check_order_exist.count() != 0):
#                     walletreffids.append(wallet)

#         if(len(walletreffids) > 0):
#             for wallet_reff in walletreffids:
#                 check_order_exist = db.query(OrdersModel).join(
#                     OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrdersModel.reff == wallet_reff).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(
#                     func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1)])) > today).group_by(OrderItemsModel.order_id).order_by(OrdersModel.id.desc())
#                 if(check_order_exist.count() == 0):
#                     wallet_reff_final_ids.append(wallet_reff)
#         if(len(wallet_reff_final_ids) > 0):
#             for wallet in wallet_reff_final_ids:
#                 today = datetime.now()
#                 today = today.strftime("%Y-%m-%d")
#                 orders = db.query(OrdersModel).join(
#                     OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrdersModel.reff == wallet).filter(OrderItemsModel.status.between(70, 81)).filter(OrderItemsModel.status != 80).group_by(OrderItemsModel.order_id).order_by(OrdersModel.id.desc()).all()
#                 order_ids = []
#                 for order in orders:
#                     items = db.query(OrderItemsModel.order_id).filter(OrderItemsModel.order_id == order.id).join(
#                         OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(
#                         OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(
#                         func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1)])) < today).group_by(OrderItemsModel.order_id).order_by(OrderItemsModel.id.desc()).all()
#                     if(len(items) > 0):
#                         for item in items:
#                             order_ids.append(item.order_id)
#                 orderdata.append(order_ids)
#             orderids = list(
#                 itertools.chain(*orderdata))
#             if(len(orderids) > 0):
#                 order_reff_list = db.query(OrdersModel.reff).filter(
#                     OrdersModel.id.in_(orderids)).group_by(OrdersModel.reff).all()
#                 for orderreff in order_reff_list:
#                     order_list = db.query(OrdersModel).filter(OrdersModel.reff == orderreff.reff).filter(
#                         OrdersModel.id.in_(orderids)).all()
#                     total_rewards_points = 0
#                     for ord in order_list:
#                         # Aseztak Service
#                         today_date = ord.created_at.strftime('%Y-%m-%d')
#                         aseztak_service = await AsezServices.aseztak_services(
#                             commission_date=today_date, db=db)
#                         items = ord.order_items.filter(OrderItemsModel.status.between(
#                             70, 81)).filter(OrderItemsModel.status != 80).all()
#                         total_amount = 0
#                         for item in items:
#                             # Pricing Object
#                             pricingdata = item.uuid.split('-')
#                             pricingdata = db.query(ProductPricingModel).filter(
#                                 ProductPricingModel.id == pricingdata[1]).first()
#                             product = db.query(ProductModel).filter(
#                                 ProductModel.id == item.product_id).first()
#                             # Get Product Price
#                             product_price: ProductModel = await ProductsHelper.getPrice(
#                                 db, product, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item.id)

#                             total_amount += product_price * item.quantity

#                         total_rewards_points += total_amount
#                     rewards_points = (float(total_rewards_points) *
#                                       float(REWARDS_PERCENTAGE)) / 100
#                     rewards_points = int(rewards_points)
#                     # Check Wallet
#                     dbwallet = db.query(WalletModel).filter(WalletModel.reff_type == 'ORDER_REWARDS').filter(
#                         WalletModel.status == 0).filter(WalletModel.reff_id == ord.reff).first()

#                     dbwallet.amount = rewards_points
#                     dbwallet.status = 1
#                     db.flush()
#                     db.commit()

#         return "UPDATED"
#     except Exception as e:
#         print(e)
# @test_router.get("/getproducts")
# def getProducts(db: Session = Depends(get_db)):
#     sql = select(ProductModel).limit(10).order_by(ProductModel.id.desc())
#     db_query = (db.execute(sql)).scalars().all()
#     return db_query

# @test_router.get("/export-buyer")
# async def exoortBuyer(db: Session = Depends(get_db)):
#     try:
#         state = "%{}%".format('Tripura')
#         buyers = db.query(UserModel).join(
#             OrdersModel, OrdersModel.user_id == UserModel.id).filter(UserModel.region.like(state)).group_by(UserModel.id).order_by(UserModel.id.desc()).all()

#         buyer_list = []
#         for buyer in buyers:
#             b = {
#                 'Name': buyer.name,
#                 'Mobile': buyer.mobile,
#                 # 'City': str(buyer.city),
#                 'State': str(buyer.region),
#                 # 'Pincode': str(buyer.pincode)
#             }
#             buyer_list.append(b)

#         marks_data = pd.DataFrame(buyer_list)

#         statement = 'buyer-export.csv'

#         #             # saving the csv
#         uploaded_file = getcwd()+"/app/static/paid_payments/"+str(statement)
#         marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

#         return len(buyer_list)
#     except Exception as e:
#         print(e)


# @test_router.get("/export-all-buyer")
# async def exoortBuyer(db: Session = Depends(get_db)):
#     try:

#         buyers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id ==
#                                           UserModel.id).group_by(UserModel.id).order_by(UserModel.id.desc()).all()

#         buyer_list = []
#         for buyer in buyers:
#             b = {
#                 'Name': buyer.name,
#                 'Mobile': str(91)+buyer.mobile,
#                 'City': str(buyer.city),
#                 'State': str(buyer.region),
#                 'Pincode': str(buyer.pincode)
#             }
#             buyer_list.append(b)

#         marks_data = pd.DataFrame(buyer_list)

#         statement = 'all-buyer-export.csv'

#         #             # saving the csv
#         uploaded_file = getcwd()+"/app/static/paid_payments/"+str(statement)
#         marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

#         return len(buyer_list)
#     except Exception as e:
#         print(e)

# @test_router.get("/")
# async def UserBank(request: Request, db: Session = Depends(get_db)):
#     try:

#         profiles = db.query(UserProfileModel).all()

#         for profile in profiles:
#             if(profile.account_type == ""):
#                 account_type = 'savings'

#             else:
#                 account_type = profile.account_type

#             dbAddBankdetails = BankAccountsModel(
#                 user_id=profile.user_id,
#                 account_holder_name=profile.account_holder_name,
#                 account_type=account_type,
#                 bank=profile.bank,
#                 branch=profile.branch,
#                 acc_no=profile.acc_no,
#                 ifsc_code=profile.ifsc_code
#             )

#             db.add(dbAddBankdetails)
#             db.commit()
#             db.refresh(dbAddBankdetails)

#     except Exception as e:
#         print(e)


# @test_router.post("/send/msz")
# async def sendMsz(request: Request, data: mszSchema):
#     try:

#         headers = CaseInsensitiveDict()
#         headers["Content-Type"] = "application/json"

#         get_pincode_url = str(data.data)
#         get_data = requests.get(
#             get_pincode_url, headers=headers)

#         return json.loads(get_data.content.decode('utf-8'))

#     except Exception as e:
#         print(e)


# @test_router.get("/check/msz/credit")
# async def checkMszCredit(request: Request):
#     try:
#         headers = CaseInsensitiveDict()
#         headers["Content-Type"] = "application/json"

#         get_pincode_url = str(
#             "http://123.108.46.13/sms-panel/api/http/index.php?username=Aseztak1&apikey=BE739-A9471&apirequest=CreditCheck&route=DND&format=JSON")
#         get_data = requests.get(
#             get_pincode_url, headers=headers)

#         return json.loads(get_data.content.decode('utf-8'))
#     except Exception as e:
#         print(e)

# @test_router.get("/generate/pdf")
# async def generatePdf():
#     try:
#         html_sample = """<h1><b>This is a heading 1</b></h1>
#             <p>1st line ...</p>
#             <p>2nd line ...</p>
#             <p>3rd line ...</p>
#             <p>4th line ...</p>
#             """

#         order_number = 'ORD-001'
#         style_sheet = """<style>
#                             .main-container {
#                                 float: left;
#                                 height: 600px;
#                                 border: 2px solid black;
#                                 margin: auto;
#                                 border-style: dashed;
#                                 width: 480px;
#                             }

#                             .textStyle {
#                                 font-size: 9px;
#                             }

#                             table {

#                                 padding-left: 10px;
#                                 padding-right: 10px;

#                                 border-spacing: 0 5px;
#                                 background-color: #fff;
#                                 width: 100%;
#                                 border-collapse: separate;

#                             }

#                             .table-bordered td,
#                             .table-bordered th {
#                                 border: 1px solid black;
#                                 border-width: 0.1px;
#                                 line-height: 15px;
#                             }

#                             .bold-text {
#                                 font-weight: bold;
#                                 font-size: 18px;
#                             }
#                         </style>"""
#         html_sample_2 = f"""<!DOCTYPE html>
#                         <html lang="en">

#                         <head>
#                             <meta charset="UTF-8">
#                             <meta http-equiv="X-UA-Compatible" content="IE=edge">
#                             <meta name="viewport" content="width=device-width, initial-scale=1.0">
#                             <title>1199310005110</title>
#                         </head>

#                         { style_sheet}
#                         <body>
#                             <div class="main-container">
#                                 <table class="table-bordered" style="margin:0px;height: 35px;text-align: center;">
#                                 <tbody style=" background-color: lightgray">
#                                 <tr>
#                                 <td class="textStyle" style="text-align: start;vertical-align:text-top;">
#                                     <div style="float: left;  margin-top: 3px">
#                                         <img alt="" src="http://172.105.62.141:8080/static/logo.png" height="25" width="96">
#                                     </div>
#                                     <div style="float: right; margin-top: 12px">
#                                     <b>

#                                         PREPAID - DO NOT COLLECT CASH

#                                     </b>
#                                     </div>


#                                 </td>
#                                 </tr>
#                                 </tbody>
#                                 </table>
#                                 <table class="table-bordered" style="margin:0px;height: 50px;">
#                                 <tbody>
#                                 <tr>
#                                 <td class="textStyle" style="text-align: start;vertical-align:text-top;">
#                                     <div class="bold-text" style="font-size: 1em; margin-bottom: 5px;">SHIPPING ADDRESS</div>
#                                     <b>Suman</b><br>

#                                         salugara , Siliguri, Siliguri, West Bengal,
#                                         India<br>
#                                         PIN: 734008

#                                 </td>

#                                 <td style="text-align: center;">
#                                     <img src="http://172.105.62.141:8080/static/asezqr.png" alt=""
#                                     style="width: 100px;height: 100px;">
#                                 </td>
#                                 </tr>
#                                 </tbody>
#                                 </table>


#                                 <table class="" style="margin:0px;height: 20px;text-align: center;">
#                                 <tbody>
#                                 <tr>
#                                 <td class="textStyle" style="text-align: start;vertical-align:text-top;">


#                                 <div style="border: 1px solid black;text-align: center;padding:7px; font-size: 1.5em">
#                                     None</div>
#                                 </td>

#                                 <td class="textStyle" style="text-align: start;vertical-align:text-top;text-align: center;">
#                                     <div style="border: 1px solid black;padding:7px; font-size: 1.5em"> Pre-paid</div>


#                                 </td>


#                                 </tr>
#                                 </tbody>
#                                 </table>
#                                 <table class="table-bordered" style="margin:0px;height: 20px;text-align: center;">
#                                 <tbody>
#                                 <tr>
#                                 <td class="textStyle" style="text-align: start;vertical-align:text-top;">
#                                     <div style="float: left;">
#                                     <b>Courier WBN: </b> 1199310005110
#                                     </div>
#                                     <div style="float: right;">
#                                     <b>Order No: </b> { order_number }
#                                     </div>
#                                 </td>

#                                 </tr>
#                                 </tbody>
#                                 </table>
#                                 <table class="table-bordered" style="margin:0px;height: 20px;text-align: center;">
#                                 <tbody>
#                                 <tr>
#                                 <td class="textStyle" style="text-align: start;vertical-align:text-top;">

#                                     <b>Sold By: </b>Rajdhani Garments, Siliguri, West Bengal, India - 734001


#                                 </td>

#                                 </tr>
#                                 </tbody>
#                                 </table>
#                                 <table class="table-bordered" style="margin:0px;height: 20px;text-align: center;">
#                                 <tbody>
#                                 <tr>
#                                     <td class="textStyle" style="text-align: start;vertical-align:text-top;">

#                                         <b>GSTIN No: </b> FSEPS9549M


#                                     </td>

#                                 </tr>
#                                 </tbody>
#                                 </table>
#                                 <table class="table-bordered">

#                                 <tbody>

#                                 <tr>
#                                     <td colspan="3" class="text-right textStyle"><b>Product</b></td>
#                                     <td class="textStyle" style="text-align: center;"><b>QTY</b></td>
#                                 </tr>
#                                 <tr>
#                                 <td colspan="3" class="textStyle">
#                                     <span>


#                                         Blenders Fashion Branded Dry Goods New T-Shirt <br>


#                                         Blenders Fashion Branded Dry Goods New T-Shirt <br>


#                                         Blenders Fashion Branded Dry Goods New T-Shirt <br>


#                                         ZS Z Signature Matty Fabric H/S T-Shirts <br>


#                                             and more....

#                                         </span>

#                                         </td>
#                                     <td class="textStyle" style="text-align: center;"> 5</td>
#                                     </tr>
#                                     <tr>
#                                     <td colspan="3" class="text-right textStyle"><b>Total Items</b></td>
#                                     <td class="textStyle" style="text-align: center;">5</td>
#                                     </tr>


#                                 </tbody>
#                                 </table>

#                                 <table class="table-bordered" style="margin:0px;height: 20px;text-align: center;">
#                                 <tbody>
#                                 <tr>
#                                 <td colspan="3" class="textStyle" style="background-color: black;color:white; font-size: 0.7em">
#                                 <b>Handover to Delhivery</b>
#                                 </td>

#                             </tr>
#                             </tbody>
#                             </table>
#                             <table class="" style="margin:0px;height: 40px;">
#                             <tbody>

#                             <tr>
#                             <td colspan="3" class="textStyle">

#                         <img alt=""
#                             src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOkAAAB6CAIAAADVp+NZAAAOqUlEQVR4nO3be0xTdxsH8F+vIAVRrnUCVrwwQblMrIMM5wUMI2g2I5eKCjjWQaJsTg3I3CVMt4mioENDILosi6gzm3N4IUOckkmZDMoodFynS7WiKSooSKXt+8d535OT32kPHfV9X3/J8/kLHp7+znNOv7an51RkmRD0H01NTU1NTQih8vJy6k/l5eVUHXsIVlcqlYiBXVcqlRaLZeHChQsXLrTaT20Xg9XprXOsw9yurfnZG6L2l2MdmtXjgxDC5qH2l+5nz291u9g67O3aOg52Pi/s9en5uY8D+7jZWgebx3589rMCABEgu4BUkF1AKsguIBVkF5AKsgtIBdkFpILsAlJBdgGpILuAVJBdQCrILiAVZBeQCrILSAXZBaSC7AJSQXYBqSC7gFSQXUAqyC4gFWQXkAqyC0gF2QWkguwCUkF2Aakgu4BUkF1AKsguIBVkF5AKsgtIBdkFpILsAlJBdgGpILuAVJBdQCrILiAVZBeQCrILSAXZBaSC7AJSQXYBqSC7gFSQXUAqyC4gFWQXkAqyC0gF2QWkguwCUkF2Aakgu4BUkF1AKsguIBVkF5AKsgtIBdkFpILsAlJBdgGpILuAVJBdQCrILiAVZBeQCrILSAXZBaSC7AJSQXYBqSC7gFSQXUAqyC4gFWQXkAqyC0gF2QWkguwCUkF2Aakgu4BUkF1AKsguIBVkF5AKsgtIBdkFpILsAlJBdgGpILuAVJBdQCrILiAVZBeQCrILSAXZBaSC7AJSQXYBqSC7gFSQXUAqyC4gFWQXkAqyC0gF2QWkguwCUkF2Aakgu4BUwok9zMnJifqBx+NRvwoEAqoiEAicnJyoOhNWFwqF9CL0Osy6UChECInFYo5+ZoW7zr0Os251fvaC1P5ix4G9PvUn9vGhR8L2l+5nz0+Ph82DrYNt19ZxYO4Ix/PCXp+en/s4UDjmxI7DBPAsFsvEHgnA/xecMwBSTfCcAXPz5k2VSqVSqeRy+bp167ibDQZDfX29SqXy8fH54IMPuJu1Wu3Ro0dbW1uFQmFkZGR2dvbMmTNtNf/9999Hjhy5ceOGyWSaP39+VlZWeHi41c6Ojo6qqqqmpqZHjx7NnDkzIyMjLi6OYwyTyfTHH39Q+/jpp59yzEA5e/ZsVVWVTqfz8vJSKBSpqakczfX19ZWVlT09Pe7u7omJiUqlkj5/YNNoNEeOHNFoNGKxeMWKFVu3bnV2dma3/fjjj+3t7VZXeP/9911cXNj14eFhagf/+uuviooK7h00Go1NTU0NDQ0qleq7777jbjaZTMeOHfvpp58MBkNAQIBSqVy2bBn3Q+xicUBra+tbb73l5uZGr7Znzx5bzTqdTqFQMJ/1TZs2ca9//PhxkUgkk8k2bNiQkpLi4+Pj5OR09uxZq82XLl1ycXHx8vJKSUlZv369TCYTCARlZWVYm9lsjo+PRwiFhIRkZGRkZWWFhIQghHbs2GF12RMnTixdupR5TqbRaLjH3r59O0Jo0aJFmZmZixYtQght3rzZVvPRo0d5PF5wcHBmZib1jCYkJIyNjVltrqmpEYvFAQEBGzduTEhIEAqFoaGhg4OD7E6FQiEWi32tMRgMWHN+fn5oaCif/+834VmzZnHsXXFxsVwup0/fx42Q2WxOTEzk8/lxcXEZGRlz587l8Xjl5eXcj7KHQ9k9d+5cfHz8rl27zp49q9PpfH19ObKrVquXLl26ffv2U6dO9fb2RkdHc2e3tbVVIBBs27aNfiKHh4cVCoWLi4tOp8Oa+/v73dzcFArF06dPqcrY2FheXh5CqLGxEWsuLCzE8rdnzx6EUH19PXuMbdu2paam7t+//5dffqmtrR03u1TPgQMH6Ep5eTlCqLq6mt3c1dUlEAg2b95sNpupysWLF0UiUUlJCbt5cHDQ09Nz9erV9D62tLRMmTLF6j8MhUKxYsUKjjmZoqKisrOzKysr1Wr1jh07uLO7Zs2ajIyMsrKyxsbGsrKycbNbWlrK5/PPnz9P/Wo2m9977z2RSNTZ2WnneLY4lF0Md3Yx42Y3Ozs7JCQEK46MjPj4+OTl5WH1ffv2TZ06dWRkBKtHRESsXbt23GHMZvOMGTNsvfTSGhoaxs1uQkJCdHQ0Vly5cmVMTAy7OTc319/f32g0Mos5OTl+fn7s5tLSUicnp3v37jGLxcXFQqFwYGAAa/5H2WXauXMnd3aZKisrubNrNpv9/PyUSiWz+OzZs4CAgJycnAmMx/Tiflbr6Oh49dVXsaKzs3NycvKlS5ewularDQsLY5/5paen19TUWMa7lsLj8by9vZ88eeLgzEajsba2dv369Vh93bp1v/7666NHj7D6hQsXkpOTRSIRs5iWlqbT6dRqNbs5Pj7e29sbW3lsbKyurs7Byf9LtFqtTqdLS0tjFoVCYUpKyoULFxxc/MXN7uTJk+/fv8+uh4SE/Pnnn1jRzc3t3r177Obg4OChoSGdTse9La1Wq1arIyIiJjwtpaury2g0Uue4TJGRkWazuaOjg1kcGRnp7e1lNy9cuJDH47W1tWF1jUYjl8uxolQq9fPzYze/INra2ng8XmRkJFaPjIy8devW4OCgI4u/uNldvnx5TU1NV1cXs2gwGA4fPjw6Ojo8PIw1d3R0YC8/T58+LSoqQgg9fPjQ1lYGBwdPnDixYsWKsLCwjRs3OjjznTt3EEL+/v5Yffr06Qihu3fvYs0Wi4Xd7Ozs7OHh0d/fzyyazWa9Xu/n58fe6PTp07FmyvDw8P79+2NjY2fOnCmTyRISEsa9IPDc3blzx9PTk31ZgzogVse234ub3Xfeeeell156/fXXKyoq2tvbW1tbS0tLQ0NDqXdY7H02MTExKipq1apVRUVFbW1tWq322LFjoaGh1Cs31kwrKChwd3dPS0u7f/9+RUXFhG/w0KizAvZTRVWMRqM9zQghiUQyOjrKrDx+/NhsNkskEnazi4sL1kxpaGj46quvZs+evWnTpjfffLO7uzs5Ofntt9/+Z7vkmEePHtmaGSFkdWz7PZ/ru/8Nrq6udXV16enpSqWSqnh4eHz88cdGo/HOnTtYHPl8fnV1tVKpzM/Ppy4vTJo0acuWLa+88kpqauq0adOsbiIxMdHb21uv19fU1Mjl8kOHDuXk5DgyM3VdFssoQshkMiHWPyFbzVQ/1kxdk7KzGSH0ySefrF27ds2aNXTlwIEDH3744Zdffrly5cqUlBS798khAoHA1szI9muKnV7c7CKEZDLZ1atXb9682dPTM3ny5IiICJFItHbtWqt3HDw8PM6cOXP37l2tVisSiSIiIiQSSV5eXmBgoLu7u9X1o6Ojo6OjEUJ79+4tKCjYsmVLbGzsnDlzJjywl5cXQqi/v9/T05NZf/DgAUJo6tSp7GbsRILux5olEomLi4vV5oGBAawZIRQUFBQUFMSs8Pn8L7744sqVK19//fX/LLve3t737t0bGxvD7rZYPSD/1It7zkCTyWSxsbFyuVwkEj18+PDixYurVq2y1SyVSpctW/baa69JJBKTyXT69GmOZhqPx9u9e7erq+vPP//syKhUXLDPZAgh6qwduxs3bdo0Nzc3rVaLNet0uuHh4cDAQKw+d+5cdrPJZOrt7WU327J8+fK+vj47mx03d+5ck8nU3d2N1bu6ulxdXX18fBxZnIDsMu3cuXPSpEmZmZn2NB88ePD27du5ubn2NPN4PB6PNzY25sh4Pj4+QUFB165dw+qXL1+WSqUymQzbYkxMDLu5trZWIBCwP5vHxMRcvXoVKzY0NIyMjLAvJtpy+/ZtX19fO5sdt3jxYrFYXF9fj9UvX768ePFiR1d38Pow0/O9N4F59uzZjh07EEJVVVX29B89elQgEBQWFmJ1+g4W5tChQwihlpYWjjXtuTexe/fuKVOm6PV6umIwGLy9vbdu3cpurqqqEgqFarWaroyOjs6fP3/16tXs5ubmZoTQ999/z9yXN954Izg4GOscHR21Oltzc7Ozs7PVm3a053tvwmKxKBSKefPm0fcCLRaLRqMRCoWVlZV2bsUWh74D2dnZybxN8NFHHy1ZsoT+Uouvry/zOyj9/f0nT56kfy0tLfXx8VEoFNSvzs7O7777LnPx7u5uPp/P5/Nv3759/fr1ioqK3t7ekpISq6+jt27dMhqNYrFYr9c3NjYeP368tbV169atxcXF2DdKf/vtt40bNyYlJS1evDgwMFAoFPb09Jw+ffqbb77JycmhbnIyXbx4kb5Od+vWrYMHD+bn50ulUqoSFRWFXXN98uRJaGioQCAoKiqaP39+T09PXl6eXq/XaDTst0iLxRITE9PZ2blv377o6Oi7d+8WFhZeu3bt999/X7BgAXs3N2zY8MMPP3z++edxcXGPHz8+ePDgyZMnL126tHLlSmbbmTNn8vPzk5KSoqKiAgMDJ02apNfrz58/X1paGhYWdvXqVeyKSllZGf2GU11drVard+3aRf81LS2NOjWnfPvttwaDgfpZpVKdPHmypKSE/mtiYuKsWbOYi/f19YWHhwcHB3/22WcymaylpWXbtm2enp5NTU0cXzmyiyPBP336tKttS5YsYTbfuHGDo9nf3x9bnHm8XF1dk5KSmpubbU3CfAMSi8Xx8fFXrlyx2jkwMFBQUIDdhvD19T18+LDV/vT0dI6x9+7dy35IX18fc57w8PCOjg5bkz948GD16tV084wZM+rq6mw1j4yMZGVl0d+D8fb2PnXqlNUBcnNzsQy5urrm5uY+fvyY3e/l5cWxj9j7jFwu52g+d+4ce/3r168zz5fi4uKY70sT9uJ+97yurs5oNDo5Ofn5+c2ePZv9HzGYVCrVw4cPRSKRVCqdM2eOPVdqh4aGuru7h4aGpFIp9pH8uejt7aW+n/Tyyy+P26zT6Xp6ejw8PBYsWMC9pwihgYGB9vZ2iUQSHh5Of/nLqsHBwc7OzidPnnh4eMybN8/Ba1KOsFgsbW1tAwMDs2bNYt+OmZgXN7sAcCPsOgMANMguIBVkF5DqX2pYcDJGu1CVAAAAAElFTkSuQmCC"
#                             height="30" width="200">
#                     </td>

#                 </tr>

#             </tbody>
#         </table>
#         <table class="table-bordered" style="margin:0px;height: 50px;">
#             <tbody>
#                 <tr>
#                     <td colspan="3" class="textStyle"> ASEZTAK WHOLESALE ONLINE PRIVATE LIMITED<br>
#                         India's B2B portal to make business easier.<br>
#                         Web: wwww.aseztak.com<br>
#                         Email: mail@aseztak.com

#                     </td>

#                 </tr>

#             </tbody>
#         </table>


#     </div>

# </body>

# </html>"""

#         # configuring pdfkit to point to our installation of wkhtmltopdf
#         config = pdfkit.configuration(
#             wkhtmltopdf=r"/usr/bin/wkhtmltopdf")

#         # converting html file to pdf file
#         # pdfkit.from_file('sample.html', 'output.pdf', configuration=config)
#         pdfkit.from_string(
#             html_sample_2, output_path="app/static/craftmy.pdf", configuration=config)

#         return
#     except Exception as e:
#         print(e)


# @test_router.get("/test")
# async def protected_endpoint(user_agent: str = Header(...)):
#     print(user_agent)
#     if "bot" in user_agent.lower():
#         raise HTTPException(status_code=403, detail="Bots not allowed.")
#     return {"message": "This is a protected endpoint."}