from datetime import date, datetime
from starlette.requests import Request
from starlette.status import HTTP_304_NOT_MODIFIED, HTTP_200_OK
from app.api.util.check_user import CheckUser
from app.api.helpers.calculation import *
from app.db.models.order_status import OrderStatusModel
from app.db.models.orders import OrdersModel
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from fastapi import APIRouter,  Depends
from app.api.helpers.orders import OrderHelper
from app.db.schemas.orders_schema import ReturnCancel
from app.api.helpers.users import *
from app.services.auth import auth
from app.api.util.calculation import orderDeliveryCalculation, productPricecalculation, floatingValue, orderDiscountCalculation, roundOf
from app.api.util.message import Message
from app.api.helpers.services import AsezServices
order_router = APIRouter()


# All Orders RAHUl
@order_router.get("/all", dependencies=[Depends(JWTBearer())])
async def getAllOrders(request: Request, db: Session = Depends(get_db)):
    userdata = auth(request=request)
    try:

        all_orders: OrdersModel = await OrderHelper.getOrders(db=db, user_id=userdata['id'])

        # Pending Orders
        pending_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='0')

        # Approved Orders
        approved_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='30')

        # Packed Orders
        packed_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='40')

        # Shipped Orders
        shipped_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='60')

        # Delivered Orders
        delivered_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='70')

        # Reversed Orders
        reversed_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='61')

        # Return Initiated Orders
        return_initiated_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='80')

        # Return Approved Orders
        return_approved_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='90')

        # Return Declined Orders
        return_declined_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='81')

        # Return Completed Orders
        return_completed_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='100')

        # Refunded Orders
        return_refunded_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='110')

        # Cancelled Orders
        cancelled_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='980')

        orders_count = [
            {
                'status': 'all',
                'count': all_orders.count(),
                'title': 'All Orders',
                'description': 'All Orders',
                'icon': ''
            },
            {
                'status': '0',
                'count': pending_orders.count(),
                'title': 'Ordered',
                'description': 'Your all pending orders',
                'icon': ''
            },

            {
                'status': '30',
                'count': approved_orders.count(),
                'title': 'Processing',
                'description': 'System has approved your orders',
                'icon': ''
            },
            {
                'status': '40',
                'count': packed_orders.count(),
                'title': 'Packed',
                'description': 'Packed Orders',
                'icon': ''
            },
            {
                'status': '60',
                'count': shipped_orders.count(),
                'title': 'Shipped',
                'description': 'Shipped Orders',
                'icon': ""
            },

            {
                'status': '70',
                'count': delivered_orders.count(),
                'title': 'Delivered',
                'description': 'Delivered Orders',
                'icon': ''
            },

            {
                'status': '61',
                'count': reversed_orders.count(),
                'title': 'Reverse',
                'description': 'Reversed Orders',
                'icon': ''
            },
            {
                'status': '80',
                'count': return_initiated_orders.count(),
                'title': 'Return Initiated',
                'description': 'Return Initiated Orders',
                'icon': ''
            },
            {
                'status': '90',
                'count': return_approved_orders.count(),
                'title': 'Return Approved',
                'description': 'Return Approved Orders',
                'icon': ''
            },
            {
                'status': '81',
                'count': return_declined_orders.count(),
                'title': 'Return Declined',
                'description': 'Return Declined Orders',
                'icon': ''
            },
            {
                'status': '100',
                'count': return_completed_orders.count(),
                'title': 'Return Completed',
                'description': 'Return Completed Orders',
                'icon': ''
            },
            {
                'status': '110',
                'count': return_refunded_orders.count(),
                'title': 'Refunded',
                'description': 'Refunded Orders',
                'icon': ''
            },
            {
                'status': '980',
                'count': cancelled_orders.count(),
                'title': 'Cancelled',
                'description': 'Cancelled Orders',
                'icon': ""

            }
        ]
        return {"status_code": HTTP_200_OK, "orders": orders_count}
    except Exception as e:
        return {"status_code": HTTP_200_OK, "orders": []}

@order_router.post("/return/cancel", dependencies=[Depends(JWTBearer())])
async def returnCancel(request: Request, data: ReturnCancel, db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.CheckBuyerOrder(
            request=request, order_id=data.order_id, db=db)
        if(checkuser == False):
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

        order = db.query(OrdersModel).filter(
            OrdersModel.id == data.order_id).first()

        # Check Current Status
        current_status = db.query(OrderStatusModel).filter(
            OrderStatusModel.order_id == order.id).order_by(OrderStatusModel.id.desc()).first()

        if(current_status.status > 70):
            db_status = OrderStatusModel(
                order_id=order.id,
                status=81,
                created_at=datetime.now(),
                updated_at=datetime.now()

            )

            db.add(db_status)
            db.commit()
            db.refresh(db_status)

        # Items
        items = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 80).all()
        for item in items:
            item.status = 81
            item.return_declined_by = order.user_id,
            item.return_declined_date = datetime.now(),
            item.update_at = datetime.now()
            db.flush()
            db.commit()

        # wait for message

        ichecktems = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 81).all()
        # Send Return Initiated Notification to buyer
        if(len(ichecktems) == 1):

            productTitle = db.query(ProductModel).filter(
                ProductModel.id == ichecktems[0].product_id).first()

            product_title = str(productTitle.title)
            product_title = product_title[:15]
        else:

            c_total_items = len(ichecktems)

            checkItemdata = db.query(OrderItemsModel).filter(
                OrderItemsModel.id == ichecktems[0].id).first()
            checkItemProduct = db.query(ProductModel).filter(
                ProductModel.id == checkItemdata.product_id).first()

            if(c_total_items > 1):
                c_total_items = (c_total_items - 1)

                product_title = str(
                    checkItemProduct.title)

                product_title = product_title[:15]

                product_title = str(
                    product_title)+' and '+str(c_total_items)+' more'
            else:
                product_title = str(checkItemProduct.title)
                product_title = product_title[:15]

        itemData = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 81).first()
        # Buyer
        buyerdetails = db.query(UserModel).filter(
            UserModel.id == order.user_id).first()

        # Check Order Items
        notification_image = db.query(ProductMediaModel).filter(
            ProductMediaModel.model_id == itemData.product_id).filter(ProductMediaModel.deleted_at.is_(None)).first()

        # Send Notification to Buyer
        b_notification_body = 'Return request for ' + str(product_title) + ' with Order ID ' + str(
            order.order_number) + ' has been Cancelled.'
        b_notification_title = 'Return Cancelled'
        b_path = '/order-detail'
        b_notification_image = notification_image.file_path

        # Send Buyer Notification
        if(buyerdetails.fcm_token is not None):
            await Message.SendOrderNotificationtoBuyer(user_token=buyerdetails.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=order.id)

        # Send Cancel Return Message
        await Message.SendOrderReturnDeclinedMessageToBuyer(mobile=buyerdetails.mobile, product_name=product_title, order_id=order.order_number)
        return {"status_code": HTTP_200_OK, "message": "Return Cancelled"}

    except Exception as e:
        print(e)


# Buyer Order Shopping
@order_router.get("/buyer-shopping-amount", dependencies=[Depends(JWTBearer())])
async def buyerShoppingOrder(request: Request, db: Session = Depends(get_db)):
    try:

        # user
        userdata = auth(request=request)
        # Shopping Amount
        current_month = date.today().month
        monthdate = current_month

        # start_date = date(date.today().year, monthdate, 1)
        start_date = '2023-08-01'
        # end_date = datetime.today().date()
        end_date = '2023-10-31'

        # buyer orders
        # buyer_orders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.status.between(0, 81)).filter(OrderItemsModel.status != 61).filter(OrderItemsModel.status != 80).filter(func.date_format(
        #     OrdersModel.created_at, '%Y-%m-%d') >= start_date).filter(func.date_format(
        #         OrdersModel.created_at, '%Y-%m-%d') <= end_date).filter(OrdersModel.user_id == userdata['id']).group_by(OrdersModel.id).all()

        buyer_orders = db.execute("SELECT orders.id FROM `orders` LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_fails  ON order_fails.order_id = orders.id WHERE order_fails.order_id is NULL AND orders.user_id =:user_id AND OT.status  BETWEEN 0 AND 81 AND OT.status != 61 AND OT.status != 80 AND DATE_FORMAT(orders.created_at, '%Y-%m-%d') >=:today AND DATE_FORMAT(orders.created_at, '%Y-%m-%d') <=:end_date GROUP BY orders.id", {
            "user_id": userdata['id'],
            "today": start_date,
            "end_date": end_date
        }).all()
        buyer_order_total_amount = 0
        for buyer_order in buyer_orders:
            buyer_order = db.query(OrdersModel).filter(
                OrdersModel.id == buyer_order.id).first()
            total_amount = 0
            order_items = buyer_order.order_items.filter(OrderItemsModel.status.between(
                0, 81)).filter(OrderItemsModel.status != 61).filter(OrderItemsModel.status != 80).all()
            for order_item in order_items:
                pricingdata = order_item.uuid.split('-')
                pricingdata = db.query(ProductPricingModel).where(ProductPricingModel.id == pricingdata[1]).where(
                    ProductPricingModel.deleted_at.is_(None)).first()
                # Get Product Detail
                productdata = db.query(ProductModel).where(
                    ProductModel.id == order_item.product_id).first()

                if(order_item.status != 61 and order_item.status != 80 and order_item.status <= 81):
                    # Aseztak Service
                    # aseztak_service = Services.aseztak_services(
                    #     order_item.created_at, db=db)
                    today_date = order_item.created_at.strftime(
                        '%Y-%m-%d')
                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                    if(aseztak_service is None):

                        # Calculate Product Price
                        product_price = productPricecalculation(price=order_item.price, tax=order_item.tax, commission=0,
                                                                gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=buyer_order.app_version)

                        total_amount += product_price * order_item.quantity
                    else:
                        product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=buyer_order.app_version, order_item_id=order_item.id)
                        # Calculate Product Price
                        # product_price = productPricecalculation(price=order_item.price, tax=order_item.tax, commission=aseztak_service.rate,
                        #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=buyer_order.app_version)

                        total_amount += product_price * order_item.quantity

            discount_amount = 0
            order_date = buyer_order.created_at.strftime("%Y-%m-%d")
            if(buyer_order.discount != 0):
                # CHECK ORDER DISCOUNT
                order_discount = db.query(OrderDiscountModel).filter(
                    OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

                discount_rate = order_discount.discount
                if(buyer_order.discount_rate != 0):
                    discount_rate = buyer_order.discount_rate

                discount_amount = orderDiscountCalculation(app_version=buyer_order.app_version,
                                                           order_amount=total_amount, discount_amount=buyer_order.discount, discount_rate=discount_rate)

            # Check Shipping Charge
            # delivery_charge = 0
            # if(buyer_order.delivery_charge != 0): CHANGES RAHUL
            shipping_charge = db.query(ShippingChargeModel).filter(
                ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

            delivery_charge = orderDeliveryCalculation(db=db, free_delivery=buyer_order.free_delivery, user_id=buyer_order.user_id, order_date=buyer_order.created_at,
                                                       app_version=buyer_order.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=buyer_order.payment_method)

            grand_total = (
                total_amount + delivery_charge) - discount_amount

            buyer_order_total_amount += grand_total

        buyer_order_total_amount = roundOf(buyer_order_total_amount)

        if(buyer_order_total_amount == 0):
            buyer_order_total_amount = 1

        return {"amount": floatingValue(buyer_order_total_amount)}
    except Exception as e:
        print(e)
