from fastapi import APIRouter,  Depends
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.services.auth_bearer import JWTBearer
from starlette.status import HTTP_200_OK

from app.db.models.faqs import FaqsModel
from app.db.schemas.faqs_schema import FaqsSchema, FaqsSchemaList


buyer_faq_router = APIRouter()


@buyer_faq_router.get("/", response_model=FaqsSchemaList, dependencies=[Depends(JWTBearer())])
async def Faqs(db: Session = Depends(get_db)):
    try:

        buyer_faqs = db.query(FaqsModel).filter(
            FaqsModel.flag == 'Buyer').all()

        for faqs in buyer_faqs:
            faqs.answer = faqs.answer
            if(faqs.video is not None):

                faqs.video = faqs.video
            else:
                faqs.video = ''

        return {'status_code': HTTP_200_OK, 'faqs': buyer_faqs}

    except Exception as e:
        print(e)


@buyer_faq_router.get("/faqs/{id}", response_model=FaqsSchema, dependencies=[Depends(JWTBearer())])
async def FaqsDetails(id: int, db: Session = Depends(get_db)):
    try:

        buyer_faqs = db.query(FaqsModel).filter(
            FaqsModel.flag == 'Buyer').filter(FaqsModel.id == id).first()
        if(buyer_faqs.video is not None):
            buyer_faqs.video = buyer_faqs.video
        else:
            buyer_faqs.video = ''

        return {'status_code': HTTP_200_OK, 'faqs': buyer_faqs}

    except Exception as e:
        print(e)
