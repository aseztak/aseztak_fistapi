from datetime import timedelta
import time

from fastapi import APIRouter,  Depends
from sqlalchemy.orm.session import Session
from app.api.helpers.admin_app_auth import *

from app.db.config import  get_db
from app.db.schemas.auth_schema import *
from app.resources.strings import *
from app.api.helpers.admin_app_users import *

import jwt
from app.db.models.admin import AdminModel
from app.core import config
from passlib.context import CryptContext
from starlette.status import HTTP_200_OK


new_admin_app_auth_router = APIRouter()

SECRET = config.SECRET_KEY
ALGORITHM = config.ALGORITHM
ACCESS_TOKEN_EXPIRE_MINUTES = 1440
# To obtain a suitable secret key you can run | import os; print(os.urandom(24).hex())


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def create_new_access_token(data: dict):
    # will create a new dictionary
    payload = {**data, **{'expires': time.time() + 600 * 60 * 60 * 60}}

    token = jwt.encode(
        payload, key=f"{SECRET}", algorithm=f"{ALGORITHM}").decode('utf-8')
    return {"access_token": token}


@new_admin_app_auth_router.post("/login")
async def Login(data: AdminLoginSchema, db: Session = Depends(get_db)):
    try:

        user = db.query(AdminModel).filter(
            AdminModel.email == data.username).first()

        if not user:
            return {"error": True, 'message': 'Invalid'}

        # password = pwd_context.verify(data.password, user.password)
        if(data.password != user.password):

            # if(password == False):
            return {"error": True, 'message': 'Invalid'}

        access_token_expires = timedelta(
            minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_new_access_token(
            data={"email": user.email}
        )

        return {"access_token": access_token['access_token'], "error": False, 'message': 'Logged In'}

    except Exception as e:
        print(e)
