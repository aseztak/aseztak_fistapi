from app.api.helpers.admin_app_products import *
from fastapi import APIRouter

from app.resources.strings import *


admin_app_banners_router = APIRouter()


# @admin_app_banners_router.get("/{status}", response_model=AdminBannerListSchema, dependencies=[Depends(JWTBearer())])
# async def getbanner(request: Request, status: int, page: int = 1, limit: int = 15, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data = db.query(BannersModel).filter(BannersModel.status == status).order_by(
#             BannersModel.id.desc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK,  "banners": [], "total_records": 0, "current_page": page, "total_page": 0}

#         banners: BannersModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         banner_data = []

#         for banner in banners:
#             banner = {
#                 'id': banner.id,
#                 'banner': banner.banner,
#                 'type': banner.type.value,
#                 'status': banner.status
#             }
#             banner_data.append(banner)

#         # Total Records
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_202_ACCEPTED,  "banners": banner_data, "total_records": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         print(e)


# # upload photo
# @admin_app_banners_router.post("/upload", dependencies=[Depends(JWTBearer())])
# async def UploadBanner(request: Request, image: UploadFile = File(...), type: str = Form(...), db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         file = image.filename.split(".")
#         extension = file[1]
#         uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
#             datetime.now().strftime('%Y-%d-%M-%h-%s')

#         filename = f"{uuid.uuid4()}.{file[1]}"

#         # Set Path Of image
#         file_name = getcwd()+"/app/static/banners/"+filename

#         # Upload image in path
#         with open(file_name, 'wb+') as f:
#             f.write(image.file.read())
#             f.close()

#         # open the image
#         uploaded_file = getcwd()+"/app/static/banners/" + str(filename)
#         filename = str(filename)
#         # Upload Photo
#         MediaHelper.uploadBannersPhoto(
#             uploaded_file=uploaded_file, file_name=filename)

#         # Remove Original Image from Path

#         unlink(uploaded_file)

#         file_path = str(
#             linode_obj_config['endpoint_url'])+'/banners/'+str(filename)

#         # Insert Data in Brand table
#         dbbanner = BannersModel(

#             banner=file_path,
#             type=type,
#             status=0
#         )
#         db.add(dbbanner)
#         db.commit()
#         db.refresh(dbbanner)

#         return {"status_code": HTTP_200_OK, "message": "Banner added successfully..."}

#     except Exception as e:
#         print(e)


# # status active/inactive
# @admin_app_banners_router.get("/status/{banner_id}", dependencies=[Depends(JWTBearer())])
# async def statusCheck(request: Request, banner_id: int, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         banner = db.query(BannersModel).filter(
#             BannersModel.id == banner_id).first()

#         if(banner.status == 0):

#             banner.status = 1
#             db.flush()
#             db.commit()

#             return {"status_code": HTTP_202_ACCEPTED, "message": "Banner Activate..."}

#         elif(banner.status == 1):

#             banner.status = 0
#             db.flush()
#             db.commit()

#             return {"status_code": HTTP_202_ACCEPTED, "message": "Banner Inactivate..."}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not modified"}


# # banner delete
# @admin_app_banners_router.delete("/delete/banner", dependencies=[Depends(JWTBearer())])
# async def deletebanner(request: Request, data: AdminBannerDeleteSchema, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         db.query(BannersModel).filter(
#             BannersModel.id == data.id).delete()

#         db.commit()
#         return {"status_code": HTTP_200_OK, "message": "Banner Deleted"}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not modified"}
