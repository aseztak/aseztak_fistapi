from fastapi import APIRouter
admin_app_dashboard_router = APIRouter()


# @admin_app_dashboard_router.get("/", dependencies=[Depends(JWTBearer())])
# async def dashboard(request: Request, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         # Weekly Orders and Buyers Report
#         a_date = date.today()
#         days = timedelta(6)
#         new_date = a_date - days
#         # Statistic Orders

#         # Statistic Buyers
#         buyers = []
#         for b in range(7):
#             newdate = (new_date + timedelta(days=b)).strftime('%Y-%m-%d')
#             day = (new_date + timedelta(days=b)).strftime('%d')
#             month = (new_date + timedelta(days=b)).strftime('%b')
#             buyer = db.execute('SELECT users.id FROM users LEFT JOIN model_has_roles as role ON role.model_id = users.id WHERE DATE_FORMAT(users.created_at, "%Y-%m-%d") >=:param1 AND DATE_FORMAT(users.created_at, "%Y-%m-%d") <=:param1 GROUP BY users.id', {
#                 "param1": newdate
#             }).all()
#             ur = {
#                 'day': day,
#                 'month': month,
#                 'count': len(buyer)
#             }
#             buyers.append(ur)

#         weekly_report = {
#             'buyers': buyers,

#         }

#         # OverView
#         # Today Orders
#         today = date.today()

#         thirty_days = (today -
#                        timedelta(days=29))
#         last_thirty_days = (thirty_days -
#                             timedelta(days=29))

#         # Today Products
#         todayproducts = db.query(ProductModel).filter(ProductModel.status != 98).filter(
#             func.date_format(ProductModel.created_at, '%Y-%m-%d') == today).count()

#         # Total Products
#         totalproducts = db.query(ProductModel).filter(
#             ProductModel.status != 98).group_by(ProductModel.id).count()

#         thirty_days_products = db.query(ProductModel).filter(func.date_format(
#             ProductModel.created_at, '%Y-%m-%d') >= thirty_days).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= today).count()

#         last_thirty_days_products = db.query(ProductModel).filter(func.date_format(
#             ProductModel.created_at, '%Y-%m-%d') >= last_thirty_days).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= thirty_days).count()

#         thirty_days_percentees_products = (
#             thirty_days_products - last_thirty_days_products) * 100 / last_thirty_days_products

#         product_overview = {
#             'today': todayproducts,
#             'total': totalproducts,
#             'percent': round(thirty_days_percentees_products, 2)
#         }

#         # Todays Buyers
#         todaysbuyers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 5).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') == today).filter(UserModel.deleted_at.is_(None)).group_by(UserModel.id).count()

#         # Total Buyers
#         totalbuyers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 5).filter(UserModel.deleted_at.is_(None)).group_by(UserModel.id).count()

#         thirty_days_buyers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 5).filter(UserModel.deleted_at.is_(None)).filter(func.date_format(
#                 UserModel.created_at, '%Y-%m-%d') >= thirty_days).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') <= today).group_by(UserModel.id).count()

#         last_thirty_days_buyers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 5).filter(UserModel.deleted_at.is_(None)).filter(func.date_format(
#                 UserModel.created_at, '%Y-%m-%d') >= last_thirty_days).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') <= thirty_days).count()

#         thirty_days_percentees_buyers = (
#             thirty_days_buyers - last_thirty_days_buyers) * 100 / last_thirty_days_buyers

#         buyer_overview = {
#             'today': todaysbuyers,
#             'total': totalbuyers,
#             'percent': round(thirty_days_percentees_buyers, 2)
#         }

#         # Todays Sellers
#         todayssellers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 6).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') == today).filter(UserModel.deleted_at.is_(None)).group_by(UserModel.id).count()

#         # Total Sellers
#         totalsellers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 6).filter(UserModel.deleted_at.is_(None)).group_by(UserModel.id).count()

#         thirty_days_sellers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 6).filter(UserModel.deleted_at.is_(None)).filter(func.date_format(
#                 UserModel.created_at, '%Y-%m-%d') >= thirty_days).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') <= today).group_by(UserModel.id).count()

#         last_thirty_days_sellers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 6).filter(UserModel.deleted_at.is_(None)).filter(func.date_format(
#                 UserModel.created_at, '%Y-%m-%d') >= last_thirty_days).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') <= thirty_days).count()

#         thirty_days_percentees_sellers = (
#             thirty_days_sellers - last_thirty_days_sellers) * 100 / last_thirty_days_sellers

#         seller_overview = {
#             'today': todayssellers,
#             'total': totalsellers,
#             'percent': round(thirty_days_percentees_sellers, 2)
#         }

#         # Order Overview Today
#         # Today Orders
#         # todayorders = db.query(OrdersModel).filter(func.date_format(
#         #     OrdersModel.created_at, '%Y-%m-%d') == today).count()

#         todayorders = db.execute(
#             "SELECT COUNT(*) as total_count FROM orders WHERE date_format(orders.created_at, '%Y-%m-%d') =:today AND orders.id NOT IN (SELECT order_fails.order_id FROM order_fails WHERE order_fails.order_id = orders.id)", {
#                 "today": today
#             }).first()

#         # Today pending
#         # todaypendingorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(
#         #     OrderItemsModel.status == 0).filter(func.date_format(OrdersModel.created_at, '%Y-%m-%d') == today).group_by(OrdersModel.id).count()
#         todaypendingorders = db.execute(
#             "SELECT ( SELECT COUNT(*)  FROM ( select orders.id FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE order_fails.order_id is NULL AND order_items.status = 0 AND date_format(orders.created_at, '%Y-%m-%d') =:today GROUP BY orders.id) as count) as total_count", {
#                 "today": today
#             }).first()
#         # Today Cancelled
#         todaycancelledorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
#             func.date_format(OrderStatusModel.created_at, '%Y-%m-%d') == today).filter(OrderStatusModel.status == 980).group_by(OrdersModel.id).count()

#         # Today Accepted
#         data: OrdersModel = db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
#             func.date_format(OrderStatusModel.created_at, '%Y-%m-%d') == today).group_by(
#             OrderStatusModel.order_id)

#         order_overview_today = [
#             {
#                 'name': 'Today Order',
#                 'value':   todayorders.total_count,
#                 'status': 'all',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Pending',
#                 'value':   todaypendingorders.total_count,
#                 'status': '0',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Accepted',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 10).count(),
#                 'status': '10',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Approved',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 30).count(),
#                 'status': '30',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Cancelled',
#                 'value':   todaycancelledorders,
#                 'status': '980',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Packed',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 40).count(),
#                 'status': '40',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Shipped',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 60).count(),
#                 'status': '60',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Reveres',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 61).count(),
#                 'status': '61',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Delivered',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 70).count(),
#                 'status': '70',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Return Initiated',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 80).count(),
#                 'status': '80',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Return Approved',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 90).count(),
#                 'status': '90',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Return Declined',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 81).count(),
#                 'status': '81',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Return Completed',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 100).count(),
#                 'status': '100',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Today Refunded',
#                 'value':  data.having(func.max(OrderStatusModel.status) == 110).count(),
#                 'status': '110',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             }

#         ]

#         # Order Overview Total
#         # Total Orders
#         # totalorders = db.query(OrdersModel).count()
#         totalorders = db.execute(
#             "SELECT COUNT(*) as total_count FROM orders LEFT JOIN order_fails ON order_fails.order_id  = orders.id WHERE order_fails.order_id is NULL").first()

#         # Total pending
#         # totalpendingorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(
#         #     OrderItemsModel.status == 0).group_by(OrdersModel.id).count()

#         totalpendingorders = db.execute(
#             "SELECT ( SELECT COUNT(*)  FROM ( select orders.id FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE order_fails.order_id is NULL AND order_items.status = 0 GROUP BY orders.id) as count) as total_count", {
#                 "today": today
#             }).first()
#         # Total Cancelled
#         totalcancelledorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
#             OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrderStatusModel.status == 980).group_by(OrdersModel.id).count()

#         # Total Calculate
#         data: OrdersModel = db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(
#             OrderStatusModel.order_id)

#         order_overview_total = [
#             {
#                 'name': 'Total Orders',
#                 'value':   totalorders.total_count,
#                 'status': 'all',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Pending',
#                 'value':   totalpendingorders.total_count,
#                 'status': '0',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Accepted',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 10).count(),
#                 'status': '10',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Approved',
#                 'value':  data.having(func.max(OrderStatusModel.status) == 30).count(),
#                 'status': '30',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Cancelled',
#                 'value':   totalcancelledorders,
#                 'status': '980',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Packed',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 40).count(),
#                 'status': '40',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Shipped',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 60).count(),
#                 'status': '60',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Reveres',
#                 'value':  data.having(func.max(OrderStatusModel.status) == 61).count(),
#                 'status': '61',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Delivered',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 70).count(),
#                 'status': '70',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Return Initiated',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 80).count(),
#                 'status': '80',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Return Approved',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 90).count(),
#                 'status': '90',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Return Declined',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 81).count(),
#                 'status': '81',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total  Return Completed',
#                 'value':   data.having(func.max(OrderStatusModel.status) == 100).count(),
#                 'status': '100',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             },
#             {
#                 'name': 'Total Refunded',
#                 'value':  data.having(func.max(OrderStatusModel.status) == 110).count(),
#                 'status': '110',
#                 'icons': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
#             }

#         ]

#         # Today Orders
#         # thirty_days_orders = db.query(OrdersModel).filter(func.date_format(
#         #     OrdersModel.created_at, '%Y-%m-%d') >= thirty_days).filter(func.date_format(OrdersModel.created_at, '%Y-%m-%d') <= today).count()
#         thirty_days_orders = db.execute("SELECT COUNT(*) as total_count FROM orders WHERE date_format(orders.created_at, '%Y-%m-%d') >=:thirty_days AND date_format(orders.created_at, '%Y-%m-%d') <=:today", {
#             "thirty_days": thirty_days,
#             "today": today
#         }).first()
#         thirty_days_orders = thirty_days_orders.total_count

#         # last_thirty_days_orders = db.query(OrdersModel).filter(func.date_format(
#         #     OrdersModel.created_at, '%Y-%m-%d') >= last_thirty_days).filter(func.date_format(OrdersModel.created_at, '%Y-%m-%d') <= thirty_days).count()
#         last_thirty_days_orders = db.execute("SELECT COUNT(*) as total_count FROM orders WHERE date_format(orders.created_at, '%Y-%m-%d') >=:thirty_days AND date_format(orders.created_at, '%Y-%m-%d') <=:today", {
#             "thirty_days": last_thirty_days,
#             "today": thirty_days
#         }).first()
#         last_thirty_days_orders = last_thirty_days_orders.total_count

#         thirty_days_percentees_orders = (
#             thirty_days_orders - last_thirty_days_orders) * 100 / last_thirty_days_orders

#         today_total_orders = {
#             'today': todayorders.total_count,
#             'total': totalorders.total_count,
#             'percent': round(thirty_days_percentees_orders, 2)
#         }
#         overview = {
#             'orders': today_total_orders,
#             'products': product_overview,
#             'buyers': buyer_overview,
#             'sellers': seller_overview
#         }

#         return {"weekly_report": weekly_report, "overview": overview, "today_order_overview": order_overview_today, "total_order_overview": order_overview_total}

#     except Exception as e:
#         print(e)


# search order
# @admin_app_dashboard_router.post("/search", dependencies=[Depends(JWTBearer())])
# async def dashboard(request: Request, data: AdminOrderSearchSchema, page: int = 1, limit: int = 15, db: Session = Depends(get_db)):
#     # Check Not Admin
#     checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#     if(checkNotadmin == False):
#         return {"status_code": HTTP_304_NOT_MODIFIED}

#     # Weekly Orders Report

#     # Statistic Orders
#     start_date = data.from_date
#     end_date = data.to_date
#     count = end_date - start_date

#     orders = []

#     for o in range((count.days) + 1):
#         newdate = (start_date + timedelta(days=o)).strftime('%Y-%m-%d')

#         day = (start_date + timedelta(days=o)).strftime('%d')

#         month = (start_date + timedelta(days=o)).strftime('%b')

#         week = (start_date + timedelta(days=o)).strftime('%A')

#         grand = 0

#         # order = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel,
#         #                                                                                                        OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).having(func.max(OrderStatusModel.status) != 980).filter(func.date_format(
#         #                                                                                                            OrdersModel.created_at, '%Y-%m-%d') >= newdate).filter(func.date_format(
#         #                                                                                                                OrdersModel.created_at, '%Y-%m-%d') <= newdate).group_by(OrdersModel.id).all()
#         order = db.execute("SELECT * FROM orders LEFT JOIN order_status as OS ON OS.order_id = orders.id WHERE date_format(orders.created_at, '%Y-%m-%d') >=:newdate AND date_format(orders.created_at, '%Y-%m-%d') <=:newdate AND orders.id NOT IN (SELECT order_fails.order_id FROM order_fails WHERE order_fails.order_id = orders.id) GROUP BY orders.id HAVING MAX(OS.status) != 980 ORDER BY orders.id DESC", {
#             "newdate": newdate
#         }).all()
#         for order_value in order:
#             order_value = db.query(OrdersModel).filter(
#                 OrdersModel.id == order_value.order_id).first()
#             # items = db.query(OrderItemsModel).filter(
#             #     OrderItemsModel.order_id == order_value.id)
#             items = order_value.order_items
#             item = items.first()

#             if(item is not None):

#                 # Latest Status
#                 current_status = order_value.order_status.order_by(
#                     OrderStatusModel.id.desc()).first()

#                 statuslist = OrderStatus.AllstatusList()
#                 statustitle = ''
#                 for s_title in statuslist:
#                     if(current_status.status == s_title['status_id']):
#                         statustitle = s_title['status_title']

#                 # Calculate Items
#                 total = 0
#                 for item_data in items.all():
#                     pricingdata = item_data.uuid.split('-')
#                     pricingdata = db.query(ProductPricingModel).where(ProductPricingModel.id == pricingdata[1]).where(
#                         ProductPricingModel.deleted_at.is_(None)).first()
#                     # Get Product Detail
#                     productdata = db.query(ProductModel).where(
#                         ProductModel.id == item_data.product_id).first()

#                     # Aseztak Service
#                     # aseztak_service = Services.aseztak_services(
#                     #     item.created_at, db=db)
#                     today_date = item_data.created_at.strftime(
#                         '%Y-%m-%d')
#                     aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#                     if(aseztak_service is None):

#                         # Calculate Product Price
#                         product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=0,
#                                                                 gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order_value.app_version)

#                         total += product_price * item_data.quantity
#                     else:
#                         product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=order_value.app_version, order_item_id=item_data.id)
#                         # Calculate Product Price
#                         # product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=aseztak_service.rate,
#                         #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order_value.app_version)

#                         total += product_price * item_data.quantity

#                 # Check Order Discount
#             discount_amount = 0
#             order_date = order_value.created_at.strftime("%Y-%m-%d")
#             if(order_value.discount != 0):
#                 # CHECK ORDER DISCOUNT
#                 order_discount = db.query(OrderDiscountModel).filter(
#                     OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                 discount_rate = order_discount.discount
#                 if(order_value.discount_rate != 0):
#                     discount_rate = order_value.discount_rate

#                 discount_amount = orderDiscountCalculation(app_version=order_value.app_version,
#                                                            order_amount=total, discount_amount=order_value.discount, discount_rate=discount_rate)

#             # Check Shipping Charge
#             # delivery_charge = 0
#             # if(order_value.delivery_charge != 0): CHANGES RAHUL
#             shipping_charge = db.query(ShippingChargeModel).filter(
#                 ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#             delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order_value.free_delivery, user_id=order_value.user_id, order_date=order_value.created_at,
#                                                        app_version=order_value.app_version, order_amount=total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order_value.payment_method)

#             # Grand Total
#             if(order_value.app_version == 'V4'):
#                 grand_total = order_value.grand_total
#             else:
#                 grand_total = (
#                     total + delivery_charge) - discount_amount

#             grand_total = round(grand_total)

#             grand += grand_total

#         ordr = {
#             'day': day,
#             'month': month,
#             'week': week,
#             'sale': grand,
#             'count': len(order),

#         }
#         orders.append(ordr)

#     products = []

#     for p in range((count.days) + 1):
#         newdate = (start_date + timedelta(days=p)).strftime('%Y-%m-%d')

#         day = (start_date + timedelta(days=p)).strftime('%d')

#         month = (start_date + timedelta(days=p)).strftime('%b')

#         week = (start_date + timedelta(days=p)).strftime('%A')

#         product = db.execute('SELECT products.id FROM products WHERE DATE_FORMAT(products.created_at, "%Y-%m-%d") >=:param1 AND DATE_FORMAT(products.created_at, "%Y-%m-%d") <=:param1 GROUP BY products.id', {
#             "param1": newdate
#         }).all()

#         pr = {
#             'day': day,
#             'month': month,
#             'week': week,
#             'count': len(product),

#         }
#         products.append(pr)

#     return {"status_code": HTTP_202_ACCEPTED, "order_list": orders, "product_list": products}


# # recent Orders
# @admin_app_dashboard_router.get("/recent/orders", response_model=AdminOrderListSchema,  dependencies=[Depends(JWTBearer())])
# async def RecentOrders(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 20):

#     # Check Not Admin
#     checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#     if(checkNotadmin == False):
#         return {"status_code": HTTP_304_NOT_MODIFIED}

#     userdata = auth(request=request)

#     checkUser = db.query(AdminModel).filter(
#         AdminModel.email == userdata['email']).first()

#     if(checkUser is not None and checkUser.level == 1):
#         try:

#             # Today Orders

#             data = db.query(OrdersModel).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 return {"status_code": HTTP_200_OK, "total_records": 0, "orders": [], "current_page": page, "total_page": 0}

#             orders: OrdersModel = data.limit(
#                 limit=limit).offset((page - 1) * limit).all()

#             # Calculation Total Amount of items
#             orderdata: OrdersModel = await Calculations.CalculateTotalAmountAllOrders(db=db, data=orders)

#             # Order Status
#             orderdata: OrdersModel = await OrderHelper.customizeOrderData(db=db, data=orderdata)

#             # Count Total Products

#             return {"status_code": HTTP_202_ACCEPTED, "orders": orderdata, "total_records": 0,  "current_page": page, "total_page": 0}

#         except Exception as e:
#             print(e)

#     else:
#         return {"status_code": HTTP_401_UNAUTHORIZED}


# # recent delivered orders
# @admin_app_dashboard_router.get("/recent_delivered/orders", response_model=AdminOrderListSchema,  dependencies=[Depends(JWTBearer())])
# async def Delivered_orders(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 20):

#     # Check Not Admin
#     checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#     if(checkNotadmin == False):
#         return {"status_code": HTTP_304_NOT_MODIFIED}

#     userdata = auth(request=request)

#     checkUser = db.query(AdminModel).filter(
#         AdminModel.email == userdata['email']).first()

#     if(checkUser is not None and checkUser.level == 1):
#         try:

#             # Today Orders

#             data = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(
#                 OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 70).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 return {"status_code": HTTP_200_OK, "total_records": 0, "orders": [], "current_page": page, "total_page": 0}

#             orders: OrdersModel = data.limit(
#                 limit=limit).offset((page - 1) * limit).all()

#             # Calculation Total Amount of items
#             orderdata: OrdersModel = await Calculations.CalculateTotalAmountAllOrders(db=db, data=orders)

#             # Order Status
#             orderdata: OrdersModel = await OrderHelper.customizeOrderData(db=db, data=orderdata)

#             return {"status_code": HTTP_202_ACCEPTED, "orders": orderdata, "total_records": 0, "current_page": page, "total_page": 0}

#         except Exception as e:
#             print(e)

#     else:
#         return {"status_code": HTTP_401_UNAUTHORIZED}


# # recent products
# @admin_app_dashboard_router.get("/recent/productlist", response_model=AdminProductData, dependencies=[Depends(JWTBearer())])
# async def Recentproducts(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 20):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data: ProductModel = await ProductsHelper.AllProducts(db=db)

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK, "total_records": 0, "products": [], "current_page": page, "total_page": 0}

#         else:

#             productdata: ProductModel = data.limit(
#                 limit=limit).offset((page - 1) * limit).all()

#             product_data = []
#             # calculating Price
#             # Aseztak Service
#             today = date.today()
#             # aseztak_service = Services.aseztak_services(
#             #     today, db=db)
#             today_date = today.strftime(
#                 '%Y-%m-%d')
#             aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#             for product in productdata:

#                 # Check Pricing
#                 pricing = product.product_pricing[0]
#                 if(pricing):
#                     product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=aseztak_service, app_version='V4')
#                     # product_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
#                     #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

#                     # Restructure Price data
#                     price_data = {
#                         'id': pricing.id,
#                         'price': floatingValue(product_price),
#                         'moq': pricing.moq,
#                         'unit': pricing.unit
#                     }

#                     product.pricing = price_data

#                 else:
#                     pricing = ""

#                 image = product.images[0]

#                 img = ''
#                 if(image is not None):
#                     if(image.file_path is not None):
#                         filename = image.file_path

#                     img = filename

#                 if(product.short_description is not None):
#                     short_description = product.short_description
#                 else:
#                     short_description = ''
#                 products = {
#                     'id': product.id,
#                     'title': product.title,
#                     'image': img,
#                     'short_description': short_description,
#                     'category': product.category,
#                     'pricing': pricing,
#                     'created_at': product.created_at,
#                     'status': product.status
#                 }
#                 product_data.append(products)

#         return {"status_code": HTTP_202_ACCEPTED, "products": product_data, "total_products": 0, "current_page": page, "total_pages": 0}

#     except Exception as e:
#         print(e)


# # Monthly Orders
# @admin_app_dashboard_router.get("/yearly/orders/{year}", dependencies=[Depends(JWTBearer())])
# async def MonthlyOrders(request: Request, year: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(year == datetime.now().year):
#             year = datetime.now().year
#             month = datetime.now().month

#         else:
#             year = year
#             month = 12

#         monthly_sale = []
#         for m in range(month):
#             ym = date(year, m+1, 1).strftime("%Y-%m")

#             total_sale = 0
#             order_count = 0
#             orders = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                 OrdersModel.created_at,  "%Y-%m") == ym).group_by(OrderStatusModel.order_id).filter(OrderStatusModel.status.between(70, 81)).filter(OrderStatusModel.status != 80).order_by(OrdersModel.id.desc())

#             order_count += orders.count()
#             if(orders.count() > 0):
#                 for order in orders:
#                     if(order.app_version == 'V4'):
#                         total_sale += float(order.grand_total)

#                     else:
#                         total_sale += (float(order.grand_total) +
#                                        float(order.delivery_charge)) - float(order.discount)

#             mm = {
#                 'month': date(year, m+1, 1).strftime('%B'),
#                 'order_count': order_count,
#                 'total_sale': total_sale
#             }
#             monthly_sale.append(mm)

#         return {"status_code": HTTP_200_OK, "orders_data": monthly_sale}

#     except Exception as e:
#         print(e)


# @admin_app_dashboard_router.get("/monthly/orders/{year_month}", dependencies=[Depends(JWTBearer())])
# async def MonthlyOrders(request: Request, year_month: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         # order_states = db.query(OrdersModel.id, PincodeModel.statename).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(PincodeModel, PincodeModel.pincode == ShippingAddressModel.pincode).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#         #     OrdersModel.created_at,  "%Y-%m-%d") >= '2022-01-01').filter(func.date_format(
#         #         OrdersModel.created_at,  "%Y-%m-%d") <= '2022-01-31').group_by(PincodeModel.statename).all()

#         order_states = db.execute('SELECT DISTINCT pincodes.statename FROM orders LEFT JOIN buyer_address ON buyer_address.id = orders.address_id LEFT JOIN pincodes ON pincodes.pincode = buyer_address.pincode LEFT JOIN order_status ON order_status.order_id = orders.id WHERE orders.created_at BETWEEN "2021-12-01" AND "2021-12-31" GROUP BY order_status.order_id HAVING MAX(order_status.status) = 70').all()

#         order_data = []
#         for order_state in order_states:
#             orders = db.query(OrdersModel).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(PincodeModel, PincodeModel.pincode == ShippingAddressModel.pincode).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                 OrdersModel.created_at,  "%Y-%m-%d") >= '2021-12-01').filter(func.date_format(
#                     OrdersModel.created_at,  "%Y-%m-%d") <= '2021-21-31').filter(PincodeModel.statename == order_state.statename).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == 70)

#             total_order_amount = 0
#             for order in orders.all():

#                 order = db.query(OrdersModel).filter(
#                     OrdersModel.id == order.id).first()

#                 items = order.order_items

#                 item = items.first()
#                 total_amount = 0
#                 if(item is not None):
#                     # Calculate Items
#                     for item_data in items.all():
#                         pricingdata = item_data.uuid.split('-')
#                         pricingdata = db.query(ProductPricingModel).where(ProductPricingModel.id == pricingdata[1]).where(
#                             ProductPricingModel.deleted_at.is_(None)).first()
#                         # Get Product Detail
#                         productdata = db.query(ProductModel).where(
#                             ProductModel.id == item_data.product_id).first()

#                         # Aseztak Service
#                         # aseztak_service = Services.aseztak_services(
#                         #     item.created_at, db=db)
#                         today_date = item_data.created_at.strftime(
#                             '%Y-%m-%d')
#                         aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#                         if(aseztak_service is None):
#                             # Calculate Product Price
#                             product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=0,
#                                                                     gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)

#                             total_amount += product_price * item_data.quantity
#                         else:
#                             product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item_data.id)
#                             # Calculate Product Price
#                             # product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=aseztak_service.rate,
#                             #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order.app_version)

#                             total_amount += product_price * item_data.quantity

#                     # Check Order Discount
#                     discount_amount = 0
#                     order_date = order.created_at.strftime("%Y-%m-%d")
#                     if(order.discount != 0):
#                         # CHECK ORDER DISCOUNT
#                         order_discount = db.query(OrderDiscountModel).filter(
#                             OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                         discount_rate = order_discount.discount
#                         if(order.discount_rate != 0):
#                             discount_rate = order.discount_rate

#                         discount_amount = orderDiscountCalculation(app_version=order.app_version,
#                                                                    order_amount=total_amount, discount_amount=order.discount, discount_rate=discount_rate)

#                     # Check Shipping Charge
#                     # delivery_charge = 0
#                     # if(order.delivery_charge != 0): CHANGES RAHUL
#                     shipping_charge = db.query(ShippingChargeModel).filter(
#                         ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#                     delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order.free_delivery, user_id=order.user_id, order_date=order.created_at,
#                                                                app_version=order.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

#                     # Grand Total
#                     if(order.app_version == 'V4'):
#                         grand_total = order.grand_total
#                     else:
#                         grand_total = (
#                             total_amount + delivery_charge) - discount_amount

#                     grand_total = round(grand_total)

#                     total_order_amount += grand_total

#             ord = {
#                 'month': 'Jan-2022',
#                 'state': order_state.statename,
#                 'order_count': orders.count(),
#                 'total_sale': total_order_amount
#             }
#             order_data.append(ord)
#         return order_data
#     except Exception as e:
#         print(e)


# @admin_app_dashboard_router.get("/get/users")
# async def getUsers(db: Session = Depends(get_db)):
#     try:

#         data = db.query(UserModel).filter(UserModel.deleted_at.is_(
#             None)).filter(func.date_format(
#                 UserModel.created_at, '%Y-%m-%d') >= '2022-04-01').filter(func.date_format(
#                     UserModel.created_at, '%Y-%m-%d') <= '2022-04-29').order_by(UserModel.id.desc()).all()

#         u = []
#         for user in data:
#             checkProfile = db.query(UserProfileModel).filter(
#                 UserProfileModel.user_id == user.id).first()
#             checkBuyerAddress = db.query(ShippingAddressModel).filter(
#                 ShippingAddressModel.user_id == user.id).first()

#             if(checkProfile is None and checkBuyerAddress is None):

#                 ii = {
#                     'id': user.id,
#                     'name': user.name

#                 }
#                 u.append(ii)

#                 dbprofile = UserProfileModel(
#                     user_id=user.id,
#                     proof='AADHAAR',
#                     proof_type='AADHAAR'
#                 )
#                 db.add(dbprofile)
#                 db.commit()

#         return u
#     except Exception as e:
#         print(e)


# @admin_app_dashboard_router.get("/all-list", response_model=AllListSchema, dependencies=[Depends(JWTBearer())])
# async def GetALlList(db: Session = Depends(get_db)):
#     try:
#         # Tag List
#         tags = db.query(TagModel).filter(TagModel.status ==
#                                          1).order_by(TagModel.id.desc()).all()
#         # Seller List
#         seller = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 6).order_by(UserModel.id.desc()).all()
#         # Selectable Category List
#         sel_category_list = db.query(CategoriesModel).filter(
#             CategoriesModel.selectable == 1).order_by(CategoriesModel.id.desc()).all()
#         # Parent Category List
#         parent_category_list = db.query(CategoriesModel).filter(
#             CategoriesModel.parent_id == 1).order_by(CategoriesModel.id.desc()).all()
#         # Brand List
#         brand_list = db.query(BrandsModel).order_by(
#             BrandsModel.id.desc()).all()
#         return {"status_code": HTTP_200_OK, "tag_list": tags, "seller_list": seller, "selectable_category_list": sel_category_list, "parent_category_list": parent_category_list, "brand_list": brand_list}
#     except Exception as e:
#         print(e)
