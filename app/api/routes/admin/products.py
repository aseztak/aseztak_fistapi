from app.api.helpers.admin_app_products import *
from fastapi import APIRouter

admin_app_products_router = APIRouter()


# @admin_app_products_router.get("/count_products", dependencies=[Depends(JWTBearer())])
# async def CountProducts(request: Request, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         all: ProductModel = await ProductsHelper.AllProductsAdmin(db=db, status='all')
#         approved: ProductModel = await ProductsHelper.AllProductsAdmin(db=db, status='51')
#         pending: ProductModel = await ProductsHelper.AllProductsAdmin(db=db, status='1')
#         deleted: ProductModel = await ProductsHelper.AllProductsAdmin(db=db, status='98')
#         rejected: ProductModel = await ProductsHelper.AllProductsAdmin(db=db, status='99')

#         count_products = [
#             {
#                 'status': 'all',
#                 'name': 'All Products',
#                 'value': all.count()
#             },
#             {
#                 'status': '1',
#                 'name': 'Pending Products',
#                 'value': pending.count()
#             },
#             {
#                 'status': '51',
#                 'name': 'Approved Products',
#                 'value': approved.count()
#             },
#             {
#                 'status': '98',
#                 'name': 'Deleted Products',
#                 'value': deleted.count()
#             },
#             {
#                 'status': '99',
#                 'name': 'Rejected Products',
#                 'value': rejected.count()
#             }
#         ]
#         return {"status_code": HTTP_200_OK, "products_count": count_products}

#     except Exception as e:
#         print(e)


# # product list
# @admin_app_products_router.post("/product_list", response_model=AdminProductData, dependencies=[Depends(JWTBearer())])
# async def getProduct(request: Request, search: AdminproductSearch,  db: Session = Depends(get_db), page: int = 1, limit: int = 15):

#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(search.search != ''):

#             data: ProductModel = await ProductsHelper.AllProductsSearchAdmin(db=db, search=search.search, status=search.status)

#         if(search.search == ''):

#             data: ProductModel = await ProductsHelper.AllProductsAdmin(db=db, status=search.status)

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK, "total_records": 0, "products": [], "current_page": page, "total_page": 0}

#         else:

#             productdata: ProductModel = data.limit(
#                 limit=limit).offset((page - 1) * limit).all()

#             product_data = []
#             # calculating Price
#             for product in productdata:

#                 # Check Pricing
#                 pricing = db.query(ProductPricingModel).where(ProductPricingModel.product_id == product.id).where(
#                     ProductPricingModel.deleted_at.is_(None)).first()
#                 if(pricing):
#                     # Aseztak Service
#                     # aseztak_service = Services.aseztak_services(
#                     #     pricing.updated_at, db=db)
#                     today_date = pricing.updated_at.strftime(
#                         '%Y-%m-%d')
#                     aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#                     product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=aseztak_service, app_version='V4')
#                     # product_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
#                     #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

#                     # Restructure Price data
#                     pricing.price = floatingValue(product_price)
#                     pricing.id = pricing.id
#                     pricing.moq = pricing.moq
#                     pricing.unit = pricing.unit

#                 else:
#                     pricing = ""

#                 image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(ProductMediaModel.default_img == 1).filter(
#                     ProductMediaModel.deleted_at.is_(None)).first()

#                 if(image == None):
#                     image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
#                         ProductMediaModel.deleted_at.is_(None)).first()

#                 img = ''
#                 if(image is not None):
#                     if(image.file_path is not None):
#                         filename = image.file_path

#                     img = filename

#                 if(product.short_description is not None):
#                     short_description = product.short_description
#                 else:
#                     short_description = ''
#                 products = {
#                     'id': product.id,
#                     'title': product.title,
#                     'image': img,
#                     'short_description': short_description,
#                     'category': product.category,
#                     'pricing': pricing,
#                     'created_at': product.created_at,
#                     'status': product.status
#                 }
#                 product_data.append(products)
#         # Total Records
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_202_ACCEPTED, "products": product_data, "total_products": total_records, "current_page": page, "total_pages": round(total_pages, 0)}

#     except Exception as e:
#         print(e)


# # add product
# @admin_app_products_router.post("/add/new", dependencies=[Depends(JWTBearer())])
# async def addProduct(request: Request, user_id: int = Form(...), title: str = Form(...), category: int = Form(...), description: str = Form(...), brand: Optional[int] = Form(None), images: List[UploadFile] = File(...), db: Session = Depends(get_db)):
#     try:
#         if(brand is None):
#             brand = 1

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         slug = title.replace(" ", "-").lower()

#         checkProduct = db.query(ProductModel).filter(
#             ProductModel.slug == slug).first()
#         if(checkProduct):
#             return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Product Already Exist"}

#         dbproduct = ProductModel(
#             userid=user_id,
#             title=title,
#             slug=slug,
#             category=category,
#             brand_id=brand,
#             short_description=description,
#             created_at=datetime.now(),
#             updated_at=datetime.now()

#         )

#         db.add(dbproduct)
#         db.commit()
#         db.refresh(dbproduct)

#         # Store Categories
#         cats = []
#         categorydata = db.query(CategoriesModel).filter(
#             CategoriesModel.id == category).first()
#         cats.append(categorydata.id)
#         if(categorydata.parent_id > 1):

#             checkparent = db.query(CategoriesModel).filter(
#                 CategoriesModel.id == categorydata.parent_id).first()
#             cats.append(checkparent.id)

#         if(checkparent is not None and checkparent.parent_id > 1):

#             checkparent1 = db.query(CategoriesModel).filter(
#                 CategoriesModel.id == checkparent.parent_id).first()
#             cats.append(checkparent1.id)

#             if(checkparent1 is not None and checkparent1.parent_id > 1):
#                 checkparent2 = db.query(CategoriesModel).filter(
#                     CategoriesModel.id == checkparent1.parent_id).first()
#                 cats.append(checkparent2.id)

#                 if(checkparent2 is not None and checkparent2.parent_id > 1):
#                     checkparent3 = db.query(CategoriesModel).filter(
#                         CategoriesModel.id == checkparent2.parent_id).first()
#                     cats.append(checkparent3.id)

#         for cat in cats:
#             productcategories = ProductCategories(
#                 category_id=cat,
#                 product_id=dbproduct.id,
#                 created_at=datetime.now(),
#                 updated_at=datetime.now()
#             )
#             db.add(productcategories)
#             db.commit()
#             db.refresh(productcategories)

#         if(len(images) > 0):
#             for image in images:
#                 # Rename Image
#                 file = image.filename.split(".")
#                 extension = file[1]
#                 uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
#                     datetime.now().strftime('%Y-%d-%M-%h-%s')

#                 filename = f"{uniqueID}.{file[1]}"

#                 # Set Path Of image
#                 file_name = getcwd()+"/app/static/products/"+filename

#                 # Upload image in path
#                 with open(file_name, 'wb+') as f:
#                     f.write(image.file.read())
#                     f.close()
#                 # open the image
#                 uploaded_file = getcwd()+"/app/static/products/" + str(filename)
#                 filename = str(filename)
#                 # Upload Photo
#                 MediaHelper.uploadPhoto(
#                     uploaded_file=uploaded_file, file_name=filename)

#                 # Remove Original Image from Path

#                 unlink(uploaded_file)

#                 file_path = str(
#                     linode_obj_config['endpoint_url'])+'/products/'+str(filename)

#                 name = filename.split(".")
#                 dbmedia = ProductMediaModel(
#                     model_id=dbproduct.id,
#                     collection_name='product',
#                     name=name[0],
#                     file_name=filename,
#                     file_path=file_path,
#                     default_img=0,
#                     created_at=datetime.now(),
#                     updated_at=datetime.now()
#                 )

#                 db.add(dbmedia)
#                 db.commit()
#                 db.refresh(dbmedia)

#         return {"status_code": HTTP_202_ACCEPTED, "product": dbproduct.id, "message": "Product Added Successfully"}

#     except Exception as e:
#         print(e)


# # edit product
# @admin_app_products_router.get('/edit/{product_id}', dependencies=[Depends(JWTBearer())])
# async def editProduct(request: Request, product_id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data: ProductModel = await ProductsHelper.getProduct(db=db, id=product_id)

#         if(data):

#             brand = 0
#             if(data.brand_id == None):
#                 data.brand_id = brand

#             images = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product_id).filter(
#                 ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.id.desc()).all()

#             imagedata = []
#             if(len(images) > 0):
#                 for image in images:

#                     filename = image.file_path
#                     img = filename

#                     img = {
#                         'id': image.id,
#                         'path': img,

#                     }
#                     imagedata.append(img)

#             product = {
#                 'title': data.title,
#                 'brand': data.brand_id,
#                 'short_description': data.short_description,
#                 'images': imagedata
#             }

#             return {"status_code": HTTP_200_OK, "product": product}

#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Error"}

#     except Exception as e:
#         print(e)


# @admin_app_products_router.post('/update/{product_id}', dependencies=[Depends(JWTBearer())])
# async def updateProduct(request: Request, product_id: int, title: str = Form(...), description: str = Form(None), brand: Optional[int] = Form(None), images: Optional[List[UploadFile]] = File([]), db: Session = Depends(get_db)):

#     try:

#         if(brand is None):
#             brand = 1

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         product = db.query(ProductModel).filter(
#             ProductModel.id == product_id).first()

#         if(product):
#             slug = title.replace(" ", "-").lower()

#             checkproduct = db.query(ProductModel).filter(
#                 ProductModel.slug == slug).first()
#             if(checkproduct):
#                 if(checkproduct.id != product.id):
#                     return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Already Exist"}

#             product.title = title
#             product.slug = slug
#             product.brand_id = brand
#             product.short_description = description

#             db.flush()
#             db.commit()

#             if(len(images) > 0):
#                 for image in images:
#                     # Rename Image
#                     file = image.filename.split(".")
#                     extension = file[1]
#                     uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
#                         datetime.now().strftime('%Y-%d-%M-%h-%s')

#                     filename = f"{uniqueID}.{file[1]}"

#                     # Set Path Of image
#                     file_name = getcwd()+"/app/static/products/"+filename

#                     # Upload image in path
#                     with open(file_name, 'wb+') as f:
#                         f.write(image.file.read())
#                         f.close()
#                     # open the image
#                     picture = Image.open(file_name)

#                     picture = ImageOps.exif_transpose(picture)

#                     if(extension.upper() == 'PNG'):
#                         picture.save("app/static/products/product_"+str(filename),
#                                      'PNG',
#                                      optimize=True,
#                                      quality=10)

#                     if(extension.upper() == 'JPG' or extension.upper() == 'JPEG'):
#                         picture.save("app/static/products/product_"+str(filename),
#                                      'JPEG',
#                                      optimize=True,
#                                      quality=10)

#                     uploaded_file = getcwd()+"/app/static/products/"+str("product_") + str(filename)
#                     filename = str("product_") + str(filename)

#                     # Upload Photo
#                     MediaHelper.uploadPhoto(
#                         uploaded_file=uploaded_file, file_name=filename)

#                     # Remove Original Image from Path
#                     unlink(file_name)
#                     unlink(uploaded_file)

#                     file_path = str(
#                         linode_obj_config['endpoint_url'])+'/products/'+str(filename)

#                     name = filename.split(".")
#                     dbmedia = ProductMediaModel(
#                         model_id=product.id,
#                         collection_name='product',
#                         name=name[0],
#                         file_name=filename,
#                         file_path=file_path,
#                         default_img=0,
#                         created_at=datetime.now(),
#                         updated_at=datetime.now()
#                     )

#                     db.add(dbmedia)
#                     db.commit()
#                     db.refresh(dbmedia)

#         return {"status_code": HTTP_200_OK, "product": product.id, "message": "Successfully Updated"}
#     except Exception as e:
#         print(e)


# # add product attribute
# @admin_app_products_router.get("/attributes/{product_id}", dependencies=[Depends(JWTBearer())])
# async def attributes(request: Request, product_id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         # Product
#         product = db.query(ProductModel).filter(
#             ProductModel.id == product_id).first()

#         # Category
#         category = db.query(CategoriesModel).filter(
#             CategoriesModel.id == product.category).first()

#         # Attribute Family
#         attributeFamilyValues = db.query(AttriFamilyValuesModel).filter(
#             AttriFamilyValuesModel.family_id == category.family_id).all()

#         attributesdata = []
#         for atfv in attributeFamilyValues:
#             # Attribute
#             attribute = db.query(AttributeModel).filter(
#                 AttributeModel.id == atfv.attribute_id).first()

#             values = []

#             value = db.query(AttributeValueModel).filter(
#                 AttributeValueModel.attribute_id == attribute.id).order_by(asc(AttributeValueModel.id)).all()

#             selected_values = []
#             if(attribute.frontend_type == 'select_multiple' or attribute.frontend_type == 'select_single'):

#                 for item in value:

#                     checkProductAttribute = db.query(ProductAttributeModel).filter(
#                         ProductAttributeModel.product_id == product_id).filter(ProductAttributeModel.attribute_value_id == item.id).first()
#                     selected = False
#                     if(checkProductAttribute):
#                         selected = True
#                         selected_values.append(str(
#                             checkProductAttribute.attribute_value_id))

#                     itemdata = {
#                         'selected': selected,
#                         'code': attribute.code,
#                         'id': str(item.id),
#                         'value': item.value
#                     }

#                     values.append(itemdata)

#             else:
#                 checkProductTextAttribute = db.query(ProductAttributeModel).filter(
#                     ProductAttributeModel.product_id == product_id).filter(ProductAttributeModel.attribute_id == attribute.id).first()

#                 if(checkProductTextAttribute):

#                     selected_values.append(str(
#                         checkProductTextAttribute.attribute_value))

#             is_required = False
#             if(attribute.is_required == 1):
#                 is_required = True

#             if(attribute.frontend_type == 'text'):
#                 attributedata = {
#                     'is_required': is_required,
#                     'code': attribute.code,
#                     'name': attribute.name,
#                     'type': attribute.frontend_type,
#                     'selected_values': selected_values,
#                 }
#             else:
#                 attributedata = {
#                     'is_required': is_required,
#                     'code': attribute.code,
#                     'name': attribute.name,
#                     'type': attribute.frontend_type,
#                     'selected_values': selected_values,
#                     'values': values
#                 }

#             attributesdata.append(attributedata)

#         # return attributesdata
#         return {"status_code": HTTP_200_OK,  "attributes": attributesdata}

#     except Exception as e:
#         print(e)


# @admin_app_products_router.post("/add/attributes", dependencies=[Depends(JWTBearer())])
# async def addAttributes(request: Request, data: AdminAttributeAddSchema, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(len(data.attributes) != 0):
#             # Re-Structure Attributes
#             attrdata = []
#             for attribute in data.attributes:
#                 attr = db.query(AttributeModel).filter(
#                     AttributeModel.code == attribute.code).first()

#                 # Delete Old attributes
#                 checkattributes = db.query(ProductAttributeModel).filter(
#                     ProductAttributeModel.product_id == data.product_id).filter(ProductAttributeModel.attribute_id == attr.id).first()
#                 if(checkattributes):
#                     db.query(ProductAttributeModel).filter(
#                         ProductAttributeModel.product_id == data.product_id).filter(ProductAttributeModel.attribute_id == attr.id).delete()

#                 if(attr.frontend_type == 'text'):
#                     attrd = {
#                         'attribute_id':  attr.id,
#                         'attribute_value_id': 0,
#                         'attribute_value': attribute.values[0]
#                     }
#                     attrdata.append(attrd)

#                 else:

#                     for value in attribute.values:

#                         attrvalue = db.query(AttributeValueModel).filter(
#                             AttributeValueModel.id == value).first()

#                         attrd = {
#                             'attribute_id':  attr.id,
#                             'attribute_value_id': value,
#                             'attribute_value': attrvalue.value
#                         }
#                         attrdata.append(attrd)

#             # Add Attributes
#             for attrbt in attrdata:
#                 dbproductattribute = ProductAttributeModel(
#                     product_id=data.product_id,
#                     attribute_id=attrbt['attribute_id'],
#                     attribute_value_id=attrbt['attribute_value_id'],
#                     attribute_value=attrbt['attribute_value']
#                 )

#                 db.add(dbproductattribute)
#                 db.commit()
#                 db.refresh(dbproductattribute)

#             # Update Pricing
#             pricing = db.query(ProductPricingModel).filter(
#                 ProductPricingModel.product_id == data.product_id).filter(ProductPricingModel.deleted_at.is_(None)).all()

#             if(len(pricing) > 0):
#                 attribute_values = []
#                 for price in pricing:
#                     pricing_attributes = db.query(ProductPricingAttributeModel).filter(
#                         ProductPricingAttributeModel.product_pricing_id == price.id).all()

#                     for price_attribute in pricing_attributes:
#                         attribute_values.append(price_attribute)

#                 for attr in attribute_values:

#                     productattribute = db.query(ProductAttributeModel).filter(ProductAttributeModel.product_id == data.product_id).filter(
#                         ProductAttributeModel.attribute_value_id == attr.attribute_id).first()
#                     if(productattribute is None):
#                         db.query(ProductPricingModel).filter(
#                             ProductPricingModel.id == attr.product_pricing_id).update({'deleted_at': datetime.now()})
#                         db.commit()

#         return {"status_code": HTTP_200_OK,  "message": "Attributes updated successfully"}

#     except Exception as e:
#         print(e)


# # add product pricing
# @admin_app_products_router.get('/pricing/{product_id}', dependencies=[Depends(JWTBearer())])
# async def productPricing(request: Request, product_id: int, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         # Product Detail
#         product = db.query(ProductModel).filter(
#             ProductModel.id == product_id).first()

#         # Pricing Units
#         pricing_units = {
#             'Pcs',
#             'Box',
#             'Mtr',
#             'Pair',
#             'kg',
#             'Bnd',
#             'Dzn',
#             'Ltr',
#             'Qts',
#         }

#         # Product Attributes
#         product_attributs = db.query(ProductAttributeModel).filter(
#             ProductAttributeModel.product_id == product_id).group_by(ProductAttributeModel.attribute_id).all()

#         price_attributes = []
#         for attributes in product_attributs:
#             # Attribute
#             attribute = db.query(AttributeModel).filter(
#                 AttributeModel.id == attributes.attribute_id).first()

#             if(attribute.is_price_variable == 1):
#                 p_attributes = db.query(ProductAttributeModel).filter(
#                     ProductAttributeModel.product_id == product_id).all()

#                 price_variable_attributes = []
#                 for pattribute in p_attributes:
#                     pattr = db.query(AttributeModel).filter(
#                         AttributeModel.id == pattribute.attribute_id).first()

#                     if(pattr.is_price_variable == 1 and attribute.id == pattribute.attribute_id):
#                         p_attributes = {
#                             'id': pattribute.attribute_value_id,
#                             'value': pattribute.attribute_value
#                         }
#                         price_variable_attributes.append(p_attributes)

#                 pp_attributes = {
#                     'name': attribute.name,
#                     'code': attribute.code,
#                     'values': price_variable_attributes
#                 }

#                 price_attributes.append(pp_attributes)

#         # Category Commission
#         category_commision = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id == product.category).order_by(
#             CategoryCommissionModel.id.desc()).first()
#         category_commision = category_commision.commission

#         # GST
#         category = db.query(CategoriesModel).filter(
#             CategoriesModel.id == product.category).first()
#         gst = category.commission
#         # TCS
#         tcs = 1
#         # TDS
#         tds = 1

#         # Image List
#         images = []
#         imagedata = db.query(ProductMediaModel).filter(
#             ProductMediaModel.model_id == product_id).filter(ProductMediaModel.deleted_at.is_(None)).all()

#         if(len(imagedata) > 0):
#             for image in imagedata:
#                 img = {
#                     'id': image.id,
#                     'image': image.file_path
#                 }
#                 images.append(img)

#         image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
#             ProductMediaModel.deleted_at.is_(None)).first()

#         file_name = ''
#         if(image is not None):
#             file_name = image.file_path
#         product = {
#             'name': product.title,
#             'description': product.short_description,
#             'image': file_name
#         }
#         return {"product": product, "pricing_units": pricing_units, "category_commission": category_commision, "gst": gst, "tcs": tcs, "tds": tds, "price_varible_attributes": price_attributes, "images": images, }

#     except Exception as e:
#         print(e)


# @admin_app_products_router.post('/add/pricing', dependencies=[Depends(JWTBearer())])
# async def addPricing(request: Request, data: AdminAddPricingSchema, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         # Minimum Order Qty
#         moq = data.moq

#         # No of Sets
#         nos = data.items

#         # No of stock
#         stock = data.stock

#         # Check Remainder of Moq and No of Sets

#         remainder = moq % nos

#         if (moq < nos or remainder != 0):
#             return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Pack of Moq"}

#         # Check Remainder of Stock and Moq
#         if(data.unlimited == False):
#             remainder1 = stock % nos
#             if (stock < moq or remainder1 != 0):
#                 return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Stock Quantity"}

#         # Check Duplicate Entry
#         pricings = db.query(ProductPricingModel).filter(
#             ProductPricingModel.product_id == data.product_id).filter(ProductPricingModel.deleted_at.is_(None)).all()
#         if(len(pricings) > 0):
#             for prices in pricings:
#                 priceattributes = db.query(ProductPricingAttributeModel).filter(
#                     ProductPricingAttributeModel.product_pricing_id == prices.id).filter(ProductPricingAttributeModel.attribute_id.in_(data.attributes)).all()

#                 priceAttributes = db.query(ProductPricingAttributeModel).filter(
#                     ProductPricingAttributeModel.product_pricing_id == prices.id).all()

#                 if(len(priceattributes) == len(priceAttributes) and len(priceAttributes) == len(data.attributes)):
#                     return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Already Exist"}

#         # Insert product pricing
#         dbprice = ProductPricingModel(
#             product_id=data.product_id,
#             price=data.price,
#             tax=data.tax,
#             hsn=data.hsn,
#             mrp=data.mrp,
#             moq=data.moq,
#             unit=data.unit,
#             items=data.items,
#             description=data.description,
#             seller_amount=data.seller_amount,
#             default_image=data.default_image,
#             created_at=datetime.now(),
#             updated_at=datetime.now()
#         )

#         db.add(dbprice)
#         db.commit()
#         db.refresh(dbprice)

#         # Insert Inventory
#         if(data.unlimited == False):
#             addunltd = 0
#         else:
#             addunltd = 1
#         dbinventory = InventoryModel(
#             pricing_id=dbprice.id,
#             stock=data.stock,
#             unlimited=addunltd,
#         )

#         db.add(dbinventory)
#         db.commit()
#         db.refresh(dbinventory)

#         # Insert into product price attributes
#         for attribute in data.attributes:
#             dbpriceattributes = ProductPricingAttributeModel(
#                 product_pricing_id=dbprice.id,
#                 attribute_id=attribute
#             )

#             db.add(dbpriceattributes)
#             db.commit()
#             db.refresh(dbpriceattributes)

#         return {"status_code": HTTP_202_ACCEPTED, "message": "Created Successfully"}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Input"}


# edit pricing
# @admin_app_products_router.get('/edit/pricing/{price_id}', dependencies=[Depends(JWTBearer())])
# async def getPricingDetails(request: Request, price_id: int, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#      # Pricing Units
#         pricing_units = {
#             'Pcs',
#             'Box',
#             'Mtr',
#             'Pair',
#             'kg',
#             'Bnd',
#             'Dzn',
#             'Ltr',
#             'Qts',
#         }
#         # Pricing Detail
#         price = db.query(ProductPricingModel).filter(
#             ProductPricingModel.id == price_id).first()

#         # Check Inventory
#         inventory = db.query(InventoryModel).filter(
#             InventoryModel.pricing_id == price.id).first()

#         unlimited = False
#         if(inventory.unlimited == 1):
#             unlimited = True

#         out_of_stock = False

#         if(inventory.out_of_stock == 1):

#             out_of_stock = True
#         # price image
#         price_image = {
#             'id': '0',
#             'url': ''
#         }

#         if(price.default_image is not 0):
#             price_image = {
#                 'id': '0',
#                 'url': ''
#             }
#             priceimage = db.query(ProductMediaModel).filter(
#                 ProductMediaModel.id == price.default_image).filter(ProductMediaModel.deleted_at.is_(None)).first()

#             if(priceimage is not None):

#                 price_image = {
#                     'id': str(price.default_image),
#                     'url': priceimage.file_path
#                 }

#         # Product
#         product = db.query(ProductModel).filter(
#             ProductModel.id == price.product_id).first()
#         # GST
#         category = db.query(CategoriesModel).filter(
#             CategoriesModel.id == product.category).first()
#         gst = category.commission

#         pricing = {
#             'id': price.id,
#             'stock': inventory.stock,
#             'unlimited': unlimited,
#             'out_of_stock': out_of_stock,
#             'price': floatingValue(price.price),
#             'items': price.items,
#             'tax': floatingValue(price.tax),
#             'moq': price.moq,
#             'mrp': floatingValue(price.mrp),
#             'hsn': price.hsn,
#             'unit': price.unit,
#             'seller_amount': floatingValue(price.seller_amount),
#             'description': price.description,
#             'default_image': price_image
#         }

#         # Product Attributes
#         product_attributs = db.query(ProductAttributeModel).filter(
#             ProductAttributeModel.product_id == price.product_id).group_by(ProductAttributeModel.attribute_id).all()

#         price_attributes = []
#         for attributes in product_attributs:
#             # Attribute
#             attribute = db.query(AttributeModel).filter(
#                 AttributeModel.id == attributes.attribute_id).first()

#             if(attribute.is_price_variable == 1):
#                 p_attributes = db.query(ProductAttributeModel).filter(
#                     ProductAttributeModel.product_id == price.product_id).all()

#                 price_variable_attributes = []
#                 for pattribute in p_attributes:
#                     pattr = db.query(AttributeModel).filter(
#                         AttributeModel.id == pattribute.attribute_id).first()

#                     if(pattr.is_price_variable == 1 and attribute.id == pattribute.attribute_id):

#                         # Check Selected Attribute Value of pricing
#                         selected = False
#                         checkproattr = db.query(ProductPricingAttributeModel).filter(
#                             ProductPricingAttributeModel.product_pricing_id == price.id).filter(ProductPricingAttributeModel.attribute_id == pattribute.attribute_value_id).first()

#                         if(checkproattr):
#                             selected = True

#                         p_attributes = {
#                             'selected': selected,
#                             'id': pattribute.attribute_value_id,
#                             'value': pattribute.attribute_value
#                         }
#                         price_variable_attributes.append(p_attributes)

#                 pp_attributes = {
#                     'name': attribute.name,
#                     'code': attribute.code,
#                     'values': price_variable_attributes
#                 }

#                 price_attributes.append(pp_attributes)

#         commission = db.query(CategoryCommissionModel).filter(
#             CategoryCommissionModel.category_id == category.id).order_by(CategoryCommissionModel.id.desc()).first()

#         category_commission = commission.commission

#         commission_tax = commission.commission_tax

#         # Image List
#         images = []
#         imagedata = db.query(ProductMediaModel).filter(
#             ProductMediaModel.model_id == product.id).filter(ProductMediaModel.deleted_at.is_(None)).all()

#         if(len(imagedata) > 0):
#             for image in imagedata:
#                 img = {
#                     'id': image.id,
#                     'image': image.file_path
#                 }
#                 images.append(img)

#         return {"status_code": HTTP_202_ACCEPTED, "pricing_units": pricing_units, "pricing": pricing, "price_variable_attributes": price_attributes, "images": images, "category_commission": category_commission, "gst": gst,  "commission_tax": commission_tax}

#     except Exception as e:
#         print(e)


# @admin_app_products_router.post('/update/pricing/data', dependencies=[Depends(JWTBearer())])
# async def updatePricing(request: Request, data: AdminUpdatePricingSchema, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         pricedata = db.query(ProductPricingModel).filter(
#             ProductPricingModel.id == data.id).first()

#         # Minimum Order Qty
#         moq = data.moq

#         # No of Sets
#         nos = data.items

#         # No of stock
#         stock = data.stock

#         # Check Remainder of Moq and No of Sets

#         remainder = moq % nos

#         if (moq < nos or remainder != 0):
#             return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Pack of Moq"}

#         # Check Remainder of Stock and Moq
#         if(data.unlimited == False):
#             remainder1 = stock % nos
#             if (stock < moq or remainder1 != 0):
#                 return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Stock Quantity"}

#         # Check Duplicate Entry
#         pricings = db.query(ProductPricingModel).filter(ProductPricingModel.id != pricedata.id).filter(
#             ProductPricingModel.product_id == pricedata.product_id).filter(ProductPricingModel.deleted_at.is_(None)).all()

#         if(len(pricings) > 0):
#             for prices in pricings:
#                 priceattributes = db.query(ProductPricingAttributeModel).filter(
#                     ProductPricingAttributeModel.product_pricing_id == prices.id).filter(ProductPricingAttributeModel.attribute_id.in_(data.attributes)).all()

#                 priceAttributes = db.query(ProductPricingAttributeModel).filter(
#                     ProductPricingAttributeModel.product_pricing_id == prices.id).all()

#                 if(len(priceattributes) == len(priceAttributes) and len(priceAttributes) == len(data.attributes)):
#                     return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Already Exist"}

#         # Check Existing Default Image
#         defaultImg = pricedata.default_image
#         if(pricedata.default_image == 0 and data.default_image == 0):
#             defaultImg = data.default_image

#         # Update product pricing
#         pricedata.price = data.price,
#         pricedata.hsn = data.hsn,
#         pricedata.mrp = data.mrp,
#         pricedata.moq = data.moq,
#         pricedata.unit = data.unit,
#         pricedata.items = data.items,
#         pricedata.description = data.description,
#         pricedata.seller_amount = data.seller_amount,
#         pricedata.default_image = defaultImg,
#         pricedata.updated_at = datetime.now()
#         db.flush()
#         db.commit()

#         # Insert Inventory
#         if(data.unlimited == False):
#             addunltd = 0
#         else:
#             addunltd = 1

#         dbinventory = db.query(InventoryModel).filter(
#             InventoryModel.pricing_id == pricedata.id).first()

#         out_of_stock = 0
#         stock = data.stock
#         if(data.out_of_stock == True):
#             out_of_stock = 1
#             stock = 0

#         dbinventory.stock = stock,
#         dbinventory.unlimited = addunltd,
#         dbinventory.out_of_stock = out_of_stock

#         db.flush()
#         db.commit()

#         # delete first pricing attributes

#         db.query(ProductPricingAttributeModel).filter(
#             ProductPricingAttributeModel.product_pricing_id == pricedata.id).delete()

#         # Insert into product price attributes
#         for attribute in data.attributes:
#             dbpriceattributes = ProductPricingAttributeModel(
#                 product_pricing_id=pricedata.id,
#                 attribute_id=attribute
#             )

#             db.add(dbpriceattributes)
#             db.commit()
#             db.refresh(dbpriceattributes)

#         return {"status_code": HTTP_200_OK, "message": "Updated Successfully"}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}


# # seller product details
# @admin_app_products_router.get("/product_overview/seller/{product_id}", response_model=AdminProductOverViewSchema, dependencies=[Depends(JWTBearer())])
# async def Overviewproduct(request: Request, product_id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         product = db.query(ProductModel).filter(
#             ProductModel.id == product_id).first()

#         # Check Brand
#         brand = {
#             'name': '',
#             'logo': ''
#         }
#         if(product.brand_id is not None):
#             brand = db.query(BrandsModel).filter(
#                 BrandsModel.id == product.brand_id).first()
#             brand = {
#                 'name': brand.name,
#                 'logo': str(brand.logo),
#             }

#         # images
#         images = await ProductsHelper.ProductImages(
#             request=request, db=db, product_id=product_id)

#         # Specifications
#         specification: ProductAttributeModel = await ProductsHelper.getAttributes(data=product, db=db)

#         # Pricing List
#         pricings: ProductModel = await ProductsHelper.pricingList(db=db, product_id=product_id)

#         pricing_data = []
#         if(len(pricings) > 0):
#             for prices in pricings:
#                 # Check Inventory
#                 inventory = db.query(InventoryModel).filter(
#                     InventoryModel.pricing_id == prices.id).first()

#                 # Check Stock
#                 stock = 0
#                 if(inventory is not None):
#                     stock = inventory.stock

#                 unlimited = False
#                 if(inventory is not None and inventory.unlimited == 1):
#                     unlimited = True

#                 # Product Image
#                 filename = ''
#                 if(prices.default_image != 0):

#                     image = db.query(ProductMediaModel).filter(ProductMediaModel.id == prices.default_image).filter(
#                         ProductMediaModel.deleted_at.is_(None)).first()

#                     if(image is not None):
#                         filename = image.file_path

#                 else:
#                     image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == prices.product_id).filter(ProductMediaModel.default_img == 1).filter(
#                         ProductMediaModel.deleted_at.is_(None)).first()

#                     if(image == None):
#                         image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == prices.product_id).filter(
#                             ProductMediaModel.deleted_at.is_(None)).first()

#                     if(image is not None):
#                         filename = image.file_path

#                 if(filename != ''):
#                     img = filename
#                 else:
#                     img = ''
#                 pdata = {
#                     'id': prices.id,
#                     'image': img,
#                     'stock': stock,
#                     'unlimited': unlimited,
#                     'price': prices.price,
#                     'mrp': prices.mrp,
#                     'tax': prices.tax,
#                     'moq': prices.moq,
#                     'items': prices.items,
#                     'unit': prices.unit,
#                     'hsn': prices.hsn,
#                     'description': prices.description,
#                     'seller_amount': prices.seller_amount,
#                     'price_attributes': await ProductsHelper.getPricingAttribjutes(data=prices, db=db)
#                 }

#                 pricing_data.append(pdata)

#         has_attributes = True
#         if(len(specification) == 0):
#             has_attributes = False

#         status = ''
#         if(product.status == 1):
#             status = 'Pending'
#         elif(product.status == 51):
#             status = 'Approved'
#         else:
#             status = 'Rejected'

#         # category
#         category = db.query(CategoriesModel).filter(
#             CategoriesModel.id == product.category).first()

#         seller = db.query(UserModel).filter(
#             UserModel.id == product.userid).first()

#         seller = {
#             'seller_id': seller.id,
#             'seller': seller.name,
#             'address': str(seller.city)+', '+str(seller.region)+', '+str(seller.country)+' - '+str(seller.pincode),

#         }
#         product = {
#             'seller': seller,
#             'title': product.title,
#             'category': category.name,
#             'description': product.short_description,
#             'brand': brand,
#             'images': images,
#             'has_attribute': has_attributes,
#             'attributes': specification,
#             'pricing': pricing_data,
#             'status': status,
#             'created_at': product.created_at.strftime("%B %d %Y")
#         }

#         return {"status_code": HTTP_200_OK, "product": product}

#     except Exception as e:
#         print(e)


# buyer product details
# @admin_app_products_router.get("/details/buyer/{product_id}", response_model=AdminProductDetail, dependencies=[Depends(JWTBearer())])
# async def deatailproduct(request: Request, product_id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         products = db.query(ProductModel).filter(
#             ProductModel.id == product_id).first()

#         if(products == False):

#             return {"status_code": HTTP_404_NOT_FOUND}

#         else:

#             # calculating Price
#             # Check Pricing
#             pricing = db.query(ProductPricingModel).where(ProductPricingModel.product_id == products.id).where(
#                 ProductPricingModel.deleted_at.is_(None)).all()
#             if(pricing):
#                 pricings = []
#                 for p in pricing:
#                     # Get Product Detail
#                     productdata = db.query(ProductModel).where(
#                         ProductModel.id == p.product_id).first()

#                     # unique ID
#                     uuid = "sku-" + str(p.id) + '-variable-' + str(p.items)

#                     if(pricing):
#                         checkinventory = db.query(InventoryModel).where(
#                             InventoryModel.pricing_id == p.id).first()

#                     # Check Stock
#                     stock = True
#                     if(checkinventory and checkinventory.out_of_stock == 0):
#                         stock = True
#                     else:
#                         stock = False

#                     # Check Unlimited Stock
#                     unlimited = True
#                     if(checkinventory and checkinventory.unlimited == 1):
#                         unlimited = True
#                     else:
#                         unlimited = False

#                     # Check Total Stock
#                     total_stock = 0
#                     if(checkinventory and checkinventory.stock != 0):
#                         total_stock = checkinventory.stock

#                     # Check Out Of Stock Quantity with Cart data
#                     cart = db.query(CartModel).where(CartModel.product_id == products.id).where(
#                         CartModel.uuid == uuid).first()

#                     out_of_quantity = False

#                     message = ''

#                     if(cart and cart.quantity > checkinventory.stock and checkinventory.unlimited == 0):
#                         out_of_quantity = True
#                         message = 'Only '+str(checkinventory.stock) + \
#                             ' '+str(p.unit)+' left'

#                     # Aseztak Service
#                     # aseztak_service = Services.aseztak_services(
#                     #     p.updated_at, db=db)
#                     today_date = p.updated_at.strftime(
#                         '%Y-%m-%d')
#                     aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#                     product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, p, asez_service=aseztak_service, app_version='V4')
#                     # product_price = productPricecalculation(price=p.price, tax=p.tax, commission=aseztak_service.rate,
#                     #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

#                     total = round(product_price, 2)

#                     item_total = 0
#                     quantity = 0
#                     if(cart):
#                         item_total = (cart.quantity * total)
#                         quantity = cart.quantity

#                     # Product Image
#                     if(p.default_image != 0):
#                         image = db.query(ProductMediaModel).filter(ProductMediaModel.id == p.default_image).filter(
#                             ProductMediaModel.deleted_at.is_(None)).first()
#                         if(image is not None):
#                             filename = image.file_path
#                     else:
#                         image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == p.product_id).filter(ProductMediaModel.default_img == 1).filter(
#                             ProductMediaModel.deleted_at.is_(None)).first()

#                         if(image == None):
#                             image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == p.product_id).filter(
#                                 ProductMediaModel.deleted_at.is_(None)).first()

#                         filename = image.file_path

#                     img = filename

#                     pdata = {
#                         'id': p.id,
#                         'product_id': p.product_id,
#                         'image': img,
#                         'price': floatingValue(total),
#                         'moq': p.moq,
#                         'tax': floatingValue(p.tax),
#                         'quantity': quantity,
#                         'sale_price': floatingValue(p.price),
#                         'description': p.description,
#                         'stock': stock,
#                         'unlimited':  unlimited,
#                         'out_of_quantity': out_of_quantity,
#                         'total_stock': total_stock,
#                         'message': message,
#                         'unit': p.unit,
#                         'uuid': uuid,
#                         'hsn_code': p.hsn,
#                         'item_total': floatingValue(round(item_total, 2)),
#                         'attributes': await ProductsHelper.getPricingAttribjutes(data=p, db=db)

#                     }

#                     pricings.append(pdata)

#             else:
#                 pricing = ''

#             # Custome Static Text

#             products: ProductModel = await ProductsHelper.staticText(request=request, db=db, data=products)

#             # Product Image
#             images = db.query(ProductMediaModel).filter(
#                 ProductMediaModel.model_id == product_id).filter(ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).all()

#             imagedata = []
#             if(len(images) > 0):

#                 for image in images:
#                     img = {
#                         'id': image.id,
#                         'image': image.file_path
#                     }
#                     imagedata.append(img)

#             # Check Stock
#             checkStock = db.query(ProductPricingModel.id).join(
#                 InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == products.id).filter(InventoryModel.out_of_stock == 0).filter(ProductPricingModel.deleted_at.is_(None)).first()

#             # checkStock = data.pricing.inventory
#             stock = True
#             if(checkStock is not None):
#                 stock = True
#             else:
#                 stock = False

#             # Specifications
#             specification: ProductAttributeModel = await ProductsHelper.getAttributes(data=products, db=db)

#             seller = db.query(UserModel).filter(
#                 UserModel.id == products.userid).first()

#             seller = {
#                 'seller_id': seller.id,
#                 'seller': seller.name,
#                 'address': str(seller.city)+', '+str(seller.region)+', '+str(seller.country)+' - '+str(seller.pincode),
#             }

#             pro = {
#                 'seller_detail': seller,
#                 'id': products.id,
#                 'title': products.title,
#                 'images': imagedata,
#                 'slug': products.slug,
#                 'short_description': products.short_description,
#                 'category': products.category,
#                 'created_at': products.created_at,
#                 'stock': stock,
#                 'status': products.status
#             }

#             return {"status_code": HTTP_202_ACCEPTED, "product": pro, "specification": specification, "pricing": pricings}

#     except Exception as e:
#         print(e)


# # trending products
# @admin_app_products_router.get("/trending/products", dependencies=[Depends(JWTBearer())])
# async def trendingproducts(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 15):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data = db.query(ProductModel).join(OrderItemsModel, OrderItemsModel.product_id == ProductModel.id).join(
#             OrdersModel, OrdersModel.id == OrderItemsModel.order_id).group_by(
#             OrderItemsModel.product_id).having(func.count(func.distinct(OrderItemsModel.order_id)) >= 5).order_by(ProductModel.id.desc())

#         # data = db.execute("SELECT products.* FROM `products` INNER JOIN order_items on order_items.product_id= products.id LEFT JOIN orders on orders.id = order_items.order_id GROUP BY order_items.order_id HAVING COUNT(orders.id) > 6 ORDER BY products.id DESC limit = 15")
#         # data = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
#         #     ProductModel, ProductModel.id == OrderItemsModel.product_id).group_by(OrderItemsModel.order_id).order_by(OrdersModel.id.desc())

#         # if(data.count() == 0):
#         #     return {"status_code": HTTP_200_OK, "total_records": 0, "trending_products": [], "current_page": page, "total_page": 0}

#         products: ProductModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         product_list = []

#         for product in products:

#             order = db.query(OrderItemsModel).filter(
#                 OrderItemsModel.product_id == product.id).group_by(OrderItemsModel.order_id).all()

#             image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(ProductMediaModel.default_img == 1).filter(
#                 ProductMediaModel.deleted_at.is_(None)).first()

#             if(image == None):
#                 image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
#                     ProductMediaModel.deleted_at.is_(None)).first()

#             img = ''
#             if(image is not None):
#                 if(image.file_path is not None):
#                     filename = image.file_path

#                 img = filename

#             pr = {
#                 'id': product.id,
#                 'title': product.title,
#                 'image': img,
#                 'total_orders': len(order)
#             }
#             product_list.append(pr)

#          # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "trending_products": product_list, "total_records": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         print(e)


# # Low Stock products
# @admin_app_products_router.get("/low/stock/products")
# async def lowStockProducts(db: Session = Depends(get_db)):
#     try:

#         productCount = db.query(ProductModel).filter(
#             ProductModel.status != 98).filter(ProductModel.status != 99).count()

#         products = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(
#             InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductModel.status != 98).filter(ProductModel.status != 99).filter(InventoryModel.out_of_stock == 0).filter(InventoryModel.unlimited == 0).filter(InventoryModel.stock <= (ProductPricingModel.items * 2)).group_by(ProductModel.id).count()

#         return products

#     except Exception as e:
#         print(e)
