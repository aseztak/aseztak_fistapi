from app.api.helpers.admin_app_products import *
from fastapi import APIRouter
admin_app_users_router = APIRouter()


# @admin_app_users_router.post("/weekly-buyers", dependencies=[Depends(JWTBearer())])
# async def Buyers(request: Request, data: SearchList, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         start_date = data.from_date
#         end_date = data.to_date
#         total = end_date - start_date
#         list = []
#         value = 0
#         for date in range((total.days) + 1):
#             date_ = (start_date + timedelta(days=date)).strftime('%Y-%m-%d')
#             day = (start_date + timedelta(days=date)).strftime('%d')
#             month = (start_date + timedelta(days=date)).strftime('%b')
#             buyers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#                 UserRoleModel.role_id == 5).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') >= date_).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') <= date_).all()
#             value += len(buyers)
#             buyer = {
#                 'day': day,
#                 'month': month,
#                 'count': len(buyers)
#             }
#             list.append(buyer)

#         return {"status_code": HTTP_200_OK, "total_buyers": value, "buyers": list}
#     except Exception as e:
#         print(e)
# # Count All Users

# # Weekly Seller


# @admin_app_users_router.post("/weekly-sellers", dependencies=[Depends(JWTBearer())])
# async def Sellers(request: Request, data: SearchList, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         start_date = data.from_date
#         end_date = data.to_date
#         total = end_date - start_date
#         list = []
#         value = 0
#         for date in range((total.days) + 1):
#             date_ = (start_date + timedelta(days=date)).strftime('%Y-%m-%d')
#             day = (start_date + timedelta(days=date)).strftime('%d')
#             month = (start_date + timedelta(days=date)).strftime('%b')
#             sellers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#                 UserRoleModel.role_id == 6).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') >= date_).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') <= date_).all()

#             value += len(sellers)
#             seller = {
#                 'day': day,
#                 'month': month,
#                 'count': len(sellers)
#             }
#             list.append(seller)
#         return {"status_code": HTTP_200_OK, "total_sellers": value, "sellers": list}

#     except Exception as e:
#         print(e)


# @admin_app_users_router.post("/weekly-products", dependencies=[Depends(JWTBearer())])
# async def Products(request: Request, data: SearchList, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         start_date = data.from_date
#         end_date = data.to_date
#         total = end_date - start_date
#         list = []
#         value = 0
#         for date in range((total.days) + 1):
#             date_ = (start_date + timedelta(days=date)).strftime('%Y-%m-%d')
#             day = (start_date + timedelta(days=date)).strftime('%d')
#             month = (start_date + timedelta(days=date)).strftime('%b')
#             products = db.query(ProductModel).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d')
#                                                      >= date_).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= date_).all()

#             value += len(products)
#             product = {
#                 'day': day,
#                 'month': month,
#                 'count': len(products)
#             }
#             list.append(product)
#         return {"status_code": HTTP_200_OK, "total_products": value, "products": list}

#     except Exception as e:
#         print(e)


# @admin_app_users_router.post("/buyers-list", response_model=AdminBuyersListSchema, dependencies=[Depends(JWTBearer())])
# async def BuyersList(request: Request, data: SearchList,  page: int = 1, limit: int = 100, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         buyer_list = []

#         buyers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 5).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') >= data.from_date).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') <= data.to_date).filter(UserModel.status == 1)

#         if(buyers.count() == 0):
#             return {"status_code": HTTP_200_OK, "users": [], "current_page": 0, "total_records": 0, "total_page": 0}

#         users: UserModel = buyers.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         for user in users:
#             if(user.status == 1):
#                 userstatus = "Active"
#             else:
#                 userstatus = "Inactive"

#             if(user.created_at is None):
#                 created_at = ''
#             else:
#                 created_at = user.created_at.strftime("%Y-%m-%d")

#             usr = {
#                 'id': user.id,
#                 'name': user.name,
#                 'email_id': user.email,
#                 'address': str(user.city)+', '+str(user.region)+', '+str(user.country)+' - '+str(user.pincode),
#                 'phone_no': user.mobile,
#                 'roles': 'Buyer',
#                 'created_at': created_at,
#                 'status': userstatus
#             }
#             buyer_list.append(usr)

#         total_records = buyers.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "users": buyer_list, "current_page": page, "total_records": total_records, "total_page": total_pages}

#     except Exception as e:
#         print(e)


# @admin_app_users_router.post("/sellers-list", response_model=AdminBuyersListSchema, dependencies=[Depends(JWTBearer())])
# async def SellersList(request: Request, data: SearchList,  page: int = 1, limit: int = 100, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         start_date = data.from_date
#         end_date = data.to_date
#         total = end_date - start_date
#         seller_list = []
#         value = 0
#         for date in range((total.days) + 1):
#             date_ = (start_date + timedelta(days=date)).strftime('%Y-%m-%d')
#             day = (start_date + timedelta(days=date)).strftime('%d')
#             month = (start_date + timedelta(days=date)).strftime('%b')

#             sellers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#                 UserRoleModel.role_id == 6).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') >= date_).filter(func.date_format(UserModel.created_at, '%Y-%m-%d') <= date_)

#             if(sellers.count() != 0):

#                 users: UserModel = sellers.limit(
#                     limit=limit).offset((page - 1) * limit).all()

#                 for user in users:
#                     role = db.query(UserRoleModel, Roles).join(Roles, Roles.id == UserRoleModel.role_id).filter(
#                         UserRoleModel.model_id == user.id).first()

#                     rolename = ''
#                     if(role is not None):
#                         rolename = role.Roles.name

#                     if(user.created_at is not None):
#                         user.created_at = user.created_at.strftime('%B %d %Y')
#                     else:
#                         user.created_at = ''

#                     if(user.status == 1):
#                         userstatus = "Active"
#                     else:
#                         userstatus = "Inactive"
#                     usr = {
#                         'id': user.id,
#                         'name': user.name,
#                         'email_id': user.email,
#                         'address': str(user.city)+', '+str(user.region)+', '+str(user.country)+' - '+str(user.pincode),
#                         'phone_no': user.mobile,
#                         'roles': rolename,
#                         'created_at': user.created_at,
#                         'status': userstatus
#                     }
#                     seller_list.append(usr)

#         total_records = sellers.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "users": seller_list, "current_page": page, "total_records": total_records, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "users": [], "current_page": 0, "total_records": 0, "total_page": 0}


# @admin_app_users_router.post("/products-list", response_model=AdminProductlistData, dependencies=[Depends(JWTBearer())])
# async def productsList(request: Request, data: SearchList,  page: int = 1, limit: int = 100, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         start_date = data.from_date
#         end_date = data.to_date
#         total = end_date - start_date
#         product_list = []
#         value = 0
#         for date in range((total.days) + 1):
#             date_ = (start_date + timedelta(days=date)).strftime('%Y-%m-%d')
#             day = (start_date + timedelta(days=date)).strftime('%d')
#             month = (start_date + timedelta(days=date)).strftime('%b')

#             db_product = db.query(ProductModel).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d')
#                                                        >= date_).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= date_)

#             productdata: UserModel = db_product.limit(
#                 limit=limit).offset((page - 1) * limit).all()

#             # calculating Price
#             for product in productdata:

#                 # Check Pricing
#                 pricing = db.query(ProductPricingModel).where(ProductPricingModel.product_id == product.id).where(
#                     ProductPricingModel.deleted_at.is_(None)).first()
#                 if(pricing):
#                     # Aseztak Service
#                     # aseztak_service = Services.aseztak_services(
#                     #     pricing.updated_at, db=db)
#                     today_date = pricing.updated_at.strftime(
#                         '%Y-%m-%d')
#                     aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#                     product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=aseztak_service, app_version='V4')
#                     # product_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
#                     #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

#                     # Restructure Price data
#                     pricing.price = floatingValue(product_price)
#                     pricing.id = pricing.id
#                     pricing.moq = pricing.moq
#                     pricing.unit = pricing.unit

#                 else:
#                     pricing = ""

#                 image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(ProductMediaModel.default_img == 1).filter(
#                     ProductMediaModel.deleted_at.is_(None)).first()

#                 if(image == None):
#                     image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
#                         ProductMediaModel.deleted_at.is_(None)).first()

#                 img = ''
#                 if(image is not None):
#                     if(image.file_path is not None):
#                         filename = image.file_path

#                     img = filename

#                 if(product.short_description is not None):
#                     short_description = product.short_description
#                 else:
#                     short_description = ''
#                 products = {
#                     'id': product.id,
#                     'title': product.title,
#                     'image': img,
#                     'short_description': short_description,
#                     'category': product.category,
#                     'pricing': pricing,
#                     'created_at': product.created_at,
#                     'status': product.status
#                 }
#                 product_list.append(products)

#         total_records = db_product.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "products": product_list, "current_page": page, "total_records": total_records, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "products": [], "current_page": 0, "total_records": 0, "total_page": 0}


# @admin_app_users_router.get("/count_users", dependencies=[Depends(JWTBearer())])
# async def CountUsers(request: Request, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         # All users
#         all = await getAllUsersAdmin(db=db, status='all', page=1, limit=1)

#         # All Buyers
#         buyer = await getAllUsersAdmin(db=db, status='buyer', page=1, limit=1)

#         # All Sellers
#         seller = await getAllUsersAdmin(db=db, status='seller', page=1, limit=1)

#         # Suspended Users
#         suspended = await getAllUsersAdmin(db=db, status='90', page=1, limit=1)

#         # Deleted Users
#         deleted = await getAllUsersAdmin(db=db, status='delete', page=1, limit=1)

#         # Count Users
#         count_users = [
#             {
#                 'status': 'all',
#                 'name': 'All Users',
#                 'value': all.count()
#             },
#             {
#                 'status': 'buyer',
#                 'name': 'Buyer',
#                 'value': buyer.count()
#             },
#             {
#                 'status': 'seller',
#                 'name': 'Seller',
#                 'value': seller.count()
#             },
#             {
#                 'status': '90',
#                 'name': 'Suspended',
#                 'value': suspended.count()
#             },
#             {
#                 'status': 'delete',
#                 'name': 'Deleted',
#                 'value': deleted.count()
#             }
#         ]

#         return {"status_code": HTTP_200_OK, "count_users": count_users}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "count_users": []}

# Count Users Type


# @admin_app_users_router.get("/count/{type}", dependencies=[Depends(JWTBearer())])
# async def CountUsers(request: Request, type: str, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         # all
#         allusers = await getAdminAllusers(db=db, status='all', type=type)
#         # All Active Users
#         activeuser = await getAdminAllusers(db=db, status='1', type=type)
#         # All Inactive Users
#         inactiveuser = await getAdminAllusers(db=db, status='0', type=type)
#         # All Suspended Users
#         suspended = await getAdminAllusers(db=db, status='90', type=type)
#         # All Deleted Users
#         deleted = await getAdminAllusers(db=db, status='deleted', type=type)

#         # Count users
#         count_users = [
#             {
#                 'status': 'all',
#                 'name': type,
#                 'value': allusers.count()
#             },
#             {
#                 'status': '1',
#                 'name': 'Active'+" "+type,
#                 'value': activeuser.count()
#             },
#             {
#                 'status': '0',
#                 'name': 'Inactive'+" "+type,
#                 'value': inactiveuser.count()
#             },
#             {
#                 'status': '90',
#                 'name': 'Suspended'+" "+type,
#                 'value': suspended.count()
#             },
#             {
#                 'status': 'deleted',
#                 'name': 'Deleted'+" "+type,
#                 'value': deleted.count()
#             },
#         ]

#         return {"status_code": HTTP_200_OK, "type": type, "count_users": count_users}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "type": type, "count_users": []}


# All users List
# @admin_app_users_router.post('/list', response_model=AdminUserListSchema, dependencies=[Depends(JWTBearer())])
# async def getUsers(request: Request, status: AdminUserStatusSchema,  page: int = 1, limit: int = 15, db: Session = Depends(get_db)):

#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(status.search != ''):
#             data = await getAdminAllusersSearch(db=db, search=status.search, status=status.status, type=status.type)

#         if(status.search == ''):
#             data = await getAdminAllusers(db=db, status=status.status, type=status.type)
#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK, "users": [], "current_page": 0, "total_records": 0, "total_page": 0}

#         users: UserModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         userlist = []
#         for user in users:

#             role = db.query(UserRoleModel, Roles).join(Roles, Roles.id == UserRoleModel.role_id).filter(
#                 UserRoleModel.model_id == user.id).first()

#             rolename = ''
#             if(role is not None):
#                 rolename = role.Roles.name

#             if(user.created_at is not None):
#                 user.created_at = user.created_at.strftime('%B %d %Y')
#             else:
#                 user.created_at = ''

#             if(user.status == 1):
#                 userstatus = "Active"
#             else:
#                 userstatus = "Inactive"

#             usr = {
#                 'id': user.id,
#                 'name': user.name,
#                 'email_id': user.email,
#                 'address': str(user.city)+', '+str(user.region)+', '+str(user.country)+' - '+str(user.pincode),
#                 'phone_no': user.mobile,
#                 'roles': rolename,
#                 'created_at': user.created_at,
#                 'status': userstatus
#             }
#             userlist.append(usr)

#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "users": userlist, "current_page": page, "total_records": total_records, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "users": [], "current_page": 0, "total_records": 0, "total_page": 0}


# edit user
# @admin_app_users_router.get("/edit/{user_id}", dependencies=[Depends(JWTBearer())])
# async def EditUser(request: Request, user_id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         user = db.query(UserModel, UserRoleModel, Roles).join(
#             UserRoleModel, UserRoleModel.model_id == UserModel.id).join(Roles, Roles.id == UserRoleModel.role_id).filter(UserModel.id == user_id).first()

#         user = {
#             'id': user.UserModel.id,
#             'name': user.UserModel.name,
#             'email': user.UserModel.email,
#             'mobile': user.UserModel.mobile,
#             'country': user.UserModel.country,
#             'region': user.UserModel.region,
#             'city': user.UserModel.city,
#             'pincode': user.UserModel.pincode,
#             'status': user.UserModel.status,
#             'role': user.UserRoleModel.role_id
#         }

#         bank = db.query(BankAccountsModel).filter(
#             BankAccountsModel.user_id == user_id).first()
#         if(bank is not None):
#             if(bank.account_type == 'savings'):
#                 account_type = 'savings'
#             else:
#                 account_type = 'current'
#             bank = {
#                 'account_holder_name': bank.account_holder_name,
#                 'account_type': account_type,
#                 'bank': bank.bank,
#                 'branch': bank.branch,
#                 'acc_no': bank.acc_no,
#                 'ifsc_code': bank.ifsc_code
#             }
#         else:
#             bank = {
#                 'account_holder_name': '',
#                 'account_type': '',
#                 'bank': '',
#                 'branch': '',
#                 'acc_no': '',
#                 'ifsc_code': '',
#             }

#         roles = db.query(Roles).all()

#         user_status = [
#             {
#                 'status': 1,
#                 'value': "Active"
#             },
#             {
#                 'status': 0,
#                 'value': "Inactive"
#             },
#             {
#                 'status': 90,
#                 'value': "Suspended"
#             }

#         ]

#         Account_type = [
#             {
#                 'type': "Savings"

#             },
#             {
#                 'type': "Current"
#             }

#         ]

#         return {"status_code": HTTP_200_OK, "users": user, "bank_details": bank, "roles": roles, "user_status": user_status, "account_type": Account_type}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "users": {}, "bank_details": {}, "roles": {}}
#         # print(e)


# @admin_app_users_router.post("/update/user", dependencies=[Depends(JWTBearer())])
# async def UpdateUser(request: Request, data: AdminUpdateUserSchema, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         user = db.query(UserModel).filter(
#             UserModel.id == data.id).first()

#         user.name = data.name,
#         user.email = data.email,
#         user.country = data.country,
#         user.city = data.city,
#         user.region = data.region,
#         user.mobile = data.mobile,
#         user.pincode = data.pincode,
#         user.status = data.status
#         user.updated_at = datetime.now()

#         db.flush()
#         db.commit()

#         userrole = db.query(UserRoleModel).filter(
#             UserRoleModel.model_id == data.id).first()

#         userrole.role_id = data.role

#         db.flush()
#         db.commit()

#         dbbank = db.query(BankAccountsModel).filter(
#             BankAccountsModel.user_id == data.id).first()

#         if(dbbank is not None):
#             dbbank.account_holder_name = data.account_holder_name
#             dbbank.account_type = data.account_type
#             dbbank.acc_no = data.acc_no
#             dbbank.branch = data.branch
#             dbbank.bank = data.bank
#             dbbank.ifsc_code = data.ifsc_code

#             db.flush()
#             db.commit()

#         else:
#             dbbank = BankAccountsModel(
#                 user_id=data.id,
#                 account_holder_name=data.account_holder_name,
#                 account_type=data.account_type,
#                 acc_no=data.acc_no,
#                 branch=data.branch,
#                 bank=data.bank,
#                 ifsc_code=data.ifsc_code
#             )

#             db.add(dbbank)
#             db.commit()
#             db.refresh(dbbank)

#         return {"status_code": HTTP_200_OK, "user": data.id, "message": "Successfully Updated"}

#     except Exception as e:
#         return {"status":  HTTP_304_NOT_MODIFIED, "message": "Not modified"}


# delete user
# @admin_app_users_router.delete("/delete/{id}", dependencies=[Depends(JWTBearer())])
# async def Deleteuser(request: Request, id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         db.query(UserModel).filter(UserModel.id == id).update(
#             {'deleted_at': datetime.now()})

#         db.commit()

#         return {"status_code": HTTP_200_OK, "message": "Successfully Deleted"}

#     except Exception as e:
#         print(e)


# @admin_app_users_router.get("/facilities/{id}", dependencies=[Depends(JWTBearer())])
# async def Userfacility(request: Request, id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         user = db.query(UserModel).filter(
#             UserModel.id == id).first()

#         user = {
#             'id': user.id,
#             'name': user.name
#         }

#         facility = db.query(UserFacilityModel).filter(
#             UserFacilityModel.user_id == id).first()

#         if(facility is not None):

#             facility = {
#                 'key_name': facility.key_name,
#                 'key_value': facility.key_value
#             }

#         else:
#             facility = {
#                 'key_name': '',
#                 'key_value': ''
#             }
#         return {"status_code": HTTP_200_OK, "user": user, "facility": facility}
#     except Exception as e:
#         print(e)


# user facility update
# @admin_app_users_router.post("/update/facility", dependencies=[Depends(JWTBearer())])
# async def updatefacility(request: Request, data: AdminUpdateFacilityUserSchema, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         dbfacility = db.query(UserFacilityModel).filter(
#             UserFacilityModel.user_id == data.user_id).first()

#         if(dbfacility is not None):

#             dbfacility.key_value = data.value
#             dbfacility.updated_at = datetime.now()

#             db.flush()
#             db.commit()

#         else:
#             dbfacility = UserFacilityModel(
#                 user_id=data.user_id,
#                 key_name='cod',
#                 key_value=data.value,
#                 created_at=datetime.now(),
#                 updated_at=datetime.now()
#             )
#             db.add(dbfacility)
#             db.commit()
#             db.refresh(dbfacility)

#         return {"status_code": HTTP_200_OK, "message": "Successfully User Facility Updated"}

#     except Exception as e:
#         return {"status":  HTTP_304_NOT_MODIFIED, "message": "Not modified"}


# # Send Notitication to Single User
# @admin_app_users_router.post("/send/notification", dependencies=[Depends(JWTBearer())])
# async def sendNotification(request: Request, data: SendNotiFicationtoBuyer, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         user = db.query(UserModel).filter(UserModel.id == data.id).first()
#         if(user.fcm_token is not None):
#             payload = {
#                 "notification": {
#                     "body": data.body,
#                     "title": data.title,
#                     "image": data.image_url
#                 },
#                 "priority": "high",
#                 "data": {
#                     "id": "1",
#                     "page": data.app_url,
#                     "image": data.image_url
#                 },
#                 "to": user.fcm_token,
#                 "content_availaable": True,
#                 "apns-priority": 5,
#             }

#             url = 'https://fcm.googleapis.com/fcm/send'
#             fcm_token = FCM_TOKEN

#             headers = {
#                 "Authorization": fcm_token[0],
#                 "Content-Type": "application/json",
#                 "Accept": "application/json"
#             }

#             response = requests.request(
#                 "POST", url, json=payload, headers=headers)

#             response = json.loads(response.content.decode('utf-8'))

#             # then later, in your endpoint:
#             return {"status_code": HTTP_200_OK, "response": response, "message": "Send Successfully"}
#         else:
#             return {"status_code": HTTP_200_OK, "response": '', "message": "Not Found"}

#     except Exception as e:
#         print(e)


# # Send All Buyer Notification
# @admin_app_users_router.post("/send/user/all/notification", dependencies=[Depends(JWTBearer())])
# async def sendNotification(request: Request, data: SendNotificaitonToAllBuyers, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(data.user_type.lower() == 'seller'):
#             topics = '/topics/TopicToSeller'
#         else:
#             topics = '/topics/TopicToBuyer'

#         today = datetime.now()

#         payload = {

#             "to": topics,
#             "priority": "high",
#             "content_availaable": True,
#             "apns-priority": 5,

#             "notification": {
#                 "body": data.body,
#                 "title": data.title,
#                 "image": data.image_url
#             },

#             "data": {
#                 "id": data.id,
#                 "page": data.app_url,
#                 "date": today.strftime("%Y-%m-%d %H:%i:%s"),
#                 "seen": False,
#                 "removedate": today.strftime("%Y-%m-%d"),
#                 "image": data.image_url
#             },

#         }

#         url = 'https://fcm.googleapis.com/fcm/send'
#         fcm_token = FCM_TOKEN

#         headers = {
#             "Authorization": fcm_token[0],
#             "Content-Type": "application/json",
#             "Accept": "application/json"
#         }

#         response = requests.request(
#             "POST", url, json=payload, headers=headers)

#         response = json.loads(response.content.decode('utf-8'))

#         # then later, in your endpoint:

#         return {"status_code": HTTP_200_OK, "response": response, "message": "Send Successfully"}

#     except Exception as e:
#         print(e)

# # Powered by Tina


# @admin_app_users_router.post("/all/buyers_list", response_model=BuyersStateSchema, dependencies=[Depends(JWTBearer())])
# async def BuyersList(request: Request, data: BuyerList, db: Session = Depends(get_db)):
#     try:

#         page = data.page
#         limit = data.limit

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)
#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}
#         search = "%{}%".format(data.state.replace("+", " ").strip())
#         buyers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#             UserRoleModel.role_id == 5).filter(UserModel.region.ilike(search)).filter(UserModel.status == 1)
#         users: UserModel = buyers.limit(
#             limit=limit).offset((page - 1) * limit).all()
#         buyer_list = []
#         for buyer in users:
#             data = {
#                 'name': buyer.name,
#                 'mobile': buyer.mobile,
#                 'state': buyer.region

#             }
#             buyer_list.append(data)
#         total_records = buyers.count()
#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))
#         total_pages = total_pages.split('.')
#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1
#         return {"status_code": HTTP_200_OK, "total_count": total_records, "buyers": buyer_list, "total_pages": total_pages}
#     except Exception as e:
#         print(e)
