
from fastapi import APIRouter
from pathlib import Path
from app.services.auth_bearer import JWTBearer
BASE_PATH = Path().resolve()


admin_app_commission_router = APIRouter()

# (Done 2022-04-28)


# @admin_app_commission_router.get("/commission", dependencies=[Depends(JWTBearer())])
# async def sellerCommission(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 15):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data: OrdersModel = await OrderHelper.getCommissionSellerList(db=db)

#         sellers: OrdersModel = data.group_by(ProductModel.userid).limit(
#             limit=limit).offset((page - 1) * limit).all()

#         seller_data = []
#         # final amount calculation
#         final_total_amount = await CalculateTotalAmount(db=db)

#         for seller in sellers:
#             total_order = []
#             paid_amount = 0

#             total_amount = 0
#             seller_total_amount = 0
#             orders = await OrderHelper.getOrderCommissionList(user_id=seller.userid, db=db)

#             for orderitems in orders:
#                 total_order.append(orderitems.order_id)

#                 # Order
#                 order = db.query(OrdersModel).filter(
#                     OrdersModel.id == orderitems.order_id).first()

#                 # Check Lost Items Check
#                 checklostItems = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status.between(80, 90)).filter(
#                     OrderItemsModel.status != 81).first()

#                 if(checklostItems is None):

#                     if(order.app_version == 'V4'):
#                         # taxable Amount
#                         taxable_amount = calculateTaxableAmount(
#                             price=orderitems.price, quantity=orderitems.quantity)

#                         # GST AMOUNt
#                         gst_amount = calculateGstAmount(
#                             price=taxable_amount, tax=orderitems.tax)

#                         # amount after tax
#                         amount_after_tax = round(
#                             taxable_amount + gst_amount, 2)

#                         # Service Charge
#                         service_charge = calculateCommissionSeller(
#                             amount=taxable_amount, commission=orderitems.commission_seller)

#                         service_charge_on_tax = calculateCommissionTaxSeller(
#                             amount=service_charge, tax=orderitems.commission_seller_tax)

#                         total_service_charge = round(
#                             service_charge + service_charge_on_tax, 2)

#                         # Tcs Amount
#                         tcs_amount = calculateSellerTcsAmount(
#                             amount=taxable_amount, tax=orderitems.tcs_rate)

#                         # Tds Amount
#                         tds_amount = calculateSellerTdsAmount(
#                             amount=amount_after_tax, tax=orderitems.tds_rate)

#                         # Paid Amount
#                         seller_will_get = (amount_after_tax -
#                                            total_service_charge - tcs_amount - tds_amount)

#                         paid_amount += round(seller_will_get, 2)
#                         total_amount += round(amount_after_tax, 2)

#                     else:

#                         # taxable Amount
#                         taxable_amount = calculateOldTaxableAmount(
#                             price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity)

#                         # GST AMOUNt
#                         gst_amount = calculateGstAmount(
#                             price=taxable_amount, tax=orderitems.tax)

#                         # amount after tax
#                         amount_after_tax = round(
#                             taxable_amount + gst_amount, 2)
#                         # Service Charge

#                         orderdate = order.created_at.strftime("%Y-%m-%d")
#                         # product
#                         productdata = db.query(ProductModel).filter(
#                             ProductModel.id == orderitems.product_id).first()
#                         categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                                       productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                         totalamount = calculateWithoutTaxAmount(
#                             price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                         orderTotalamoutwithouttax = totalamount['total_amount']

#                         tcs_amount = (orderTotalamoutwithouttax *
#                                       TCS) / 100
#                         tcs_amount = round(tcs_amount, 2)

#                         tds_amount = (orderTotalamoutwithouttax * TDS) / 100
#                         tds_amount = round(tds_amount, 2)

#                         if(orderdate < '2021-11-09'):

#                             tcs_amount = 0.00
#                             tds_amount = 0.00

#                             seller_will_get = calculateTotalWithTax(
#                                 price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                             total_amount += seller_will_get['tota_amount']

#                             seller_will_get = (taxable_amount -
#                                                seller_will_get['total_commision'])

#                             paid_amount += round(seller_will_get, 2)
#                             seller_total_amount += round(seller_will_get, 2)

#                         else:

#                             tcs_amount = tcs_amount
#                             tds_amount = tds_amount

#                             seller_will_get = calculateTotal(price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity,
#                                                              commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                             total_amount += seller_will_get['tota_amount']

#                             seller_will_get = (
#                                 seller_will_get['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

#                             paid_amount += round(seller_will_get, 2)

#             s = seller.userid
#             s = db.query(UserModel).filter(UserModel.id == s).first()
#             upcomming = {
#                 'seller': {
#                     'id': s.id,
#                     'name': s.name
#                 },
#                 'address': str(s.city)+', '+str(s.region)+', '+str(s.country)+' - '+str(s.pincode),
#                 'mobile': s.mobile,
#                 'total_orders': len(set(total_order)),
#                 'total_amount': floatingValue(total_amount),
#                 'amount_will_get': floatingValue(paid_amount)


#             }
#             seller_data.append(upcomming)

#         # Rajdjhani Seller
#         rajdhanidata: OrdersModel = await OrderHelper.getCommissionSellerRajdhaniList(db=db)

#         rajdhanisellers: OrdersModel = rajdhanidata.group_by(ProductModel.userid).limit(
#             limit=limit).offset((page - 1) * limit).all()

#         rajdhaniseller_data = []
#         for rajdhaniseller in rajdhanisellers:
#             rajtotal_order = []
#             rajpaid_amount = 0
#             rajtotal_amount = 0
#             rajorders = await OrderHelper.getOrderCommissionListRajdhani(user_id=rajdhaniseller.userid, db=db)

#             for rajorderitems in rajorders:

#                 # Order
#                 rajorder = db.query(OrdersModel).filter(
#                     OrdersModel.id == rajorderitems.order_id).first()

#                 # Check Lost Items Check
#                 checklostItems = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.order_id == rajorder.id).filter(OrderItemsModel.status.between(80, 90)).filter(
#                     OrderItemsModel.status != 81).first()

#                 if(checklostItems is None):
#                     rajtotal_order.append(rajorderitems.order_id)
#                     if(rajorder.app_version == 'V4'):
#                         # taxable Amount
#                         rajtaxable_amount = calculateTaxableAmount(
#                             price=rajorderitems.price, quantity=rajorderitems.quantity)

#                         # GST AMOUNt
#                         rajgst_amount = calculateGstAmount(
#                             price=rajtaxable_amount, tax=rajorderitems.tax)

#                         # amount after tax
#                         rajamount_after_tax = round(
#                             rajtaxable_amount + rajgst_amount, 2)

#                         # Service Charge
#                         rajservice_charge = calculateCommissionSeller(
#                             amount=rajtaxable_amount, commission=rajorderitems.commission_seller)

#                         rajservice_charge_on_tax = calculateCommissionTaxSeller(
#                             amount=rajservice_charge, tax=rajorderitems.commission_seller_tax)

#                         rajtotal_service_charge = round(
#                             rajservice_charge + rajservice_charge_on_tax, 2)

#                         # Tcs Amount
#                         rajtcs_amount = calculateSellerTcsAmount(
#                             amount=rajtaxable_amount, tax=rajorderitems.tcs_rate)

#                         # Tds Amount
#                         rajtds_amount = calculateSellerTdsAmount(
#                             amount=rajamount_after_tax, tax=rajorderitems.tds_rate)

#                         # Paid Amount
#                         rajseller_will_get = (rajamount_after_tax -
#                                               rajtotal_service_charge - rajtcs_amount - rajtds_amount)

#                         rajpaid_amount += round(rajseller_will_get, 2)
#                         rajtotal_amount += round(rajamount_after_tax, 2)

#                     else:

#                         # taxable Amount
#                         rajtaxable_amount = calculateOldTaxableAmount(
#                             price=rajorderitems.price, tax=rajorderitems.tax, quantity=rajorderitems.quantity)

#                         # GST AMOUNt
#                         rajgst_amount = calculateGstAmount(
#                             price=rajtaxable_amount, tax=rajorderitems.tax)

#                         # amount after tax
#                         rajamount_after_tax = round(
#                             rajtaxable_amount + rajgst_amount, 2)
#                         # Service Charge

#                         rajorderdate = rajorder.created_at.strftime("%Y-%m-%d")
#                         # product
#                         rajproductdata = db.query(ProductModel).filter(
#                             ProductModel.id == rajorderitems.product_id).first()
#                         rajcategorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                                          rajproductdata.category).filter(CategoryCommissionModel.start_date <= rajorderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                         rajtotalamount = calculateWithoutTaxAmount(
#                             price=rajorderitems.price, tax=rajorderitems.tax, quantity=rajorderitems.quantity, commission=rajcategorycommission.commission, commission_tax=rajcategorycommission.commission_tax)

#                         rajorderTotalamoutwithouttax = rajtotalamount['total_amount']

#                         rajtcs_amount = (rajorderTotalamoutwithouttax *
#                                          TCS) / 100
#                         rajtcs_amount = round(rajtcs_amount, 2)

#                         rajtds_amount = (
#                             rajorderTotalamoutwithouttax * TDS) / 100
#                         rajtds_amount = round(rajtds_amount, 2)

#                         if(rajorderdate < '2021-11-09'):

#                             rajtcs_amount = 0.00
#                             rajtds_amount = 0.00

#                             rajseller_will_get = calculateTotalWithTax(
#                                 price=rajorderitems.price, tax=rajorderitems.tax, quantity=rajorderitems.quantity, commission=rajcategorycommission.commission, commission_tax=rajcategorycommission.commission_tax)

#                             rajtotal_amount += rajseller_will_get['tota_amount']

#                             rajseller_will_get = (rajtaxable_amount -
#                                                   rajseller_will_get['total_commision'])

#                             rajpaid_amount += round(rajseller_will_get, 2)

#                         else:

#                             rajtcs_amount = rajtcs_amount
#                             rajtds_amount = rajtds_amount

#                             rajseller_will_get = calculateTotal(price=rajorderitems.price, tax=rajorderitems.tax, quantity=rajorderitems.quantity,
#                                                                 commission=rajcategorycommission.commission, commission_tax=rajcategorycommission.commission_tax)

#                             rajtotal_amount += rajseller_will_get['tota_amount']

#                             rajseller_will_get = (
#                                 rajseller_will_get['tota_amount'] - rajtotalamount['total_commisison'] - rajtcs_amount - rajtds_amount)

#                             rajpaid_amount += round(rajseller_will_get, 2)

#             if(len(rajtotal_order) != 0):
#                 s = rajdhaniseller.userid
#                 s = db.query(UserModel).filter(UserModel.id == s).first()

#                 rajupcomming = {
#                     'seller': {
#                         'id': s.id,
#                         'name': s.name
#                     },
#                     'address': str(s.city)+', '+str(s.region)+', '+str(s.country)+' - '+str(s.pincode),
#                     'mobile': s.mobile,
#                     'total_orders': len(set(rajtotal_order)),
#                     'total_amount': floatingValue(rajtotal_amount),
#                     'amount_will_get': floatingValue(rajpaid_amount)
#                 }
#                 rajdhaniseller_data.append(rajupcomming)

#         final_data = (seller_data + rajdhaniseller_data)

#         # Total Records
#         total_records = data.group_by(ProductModel.userid).count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         # Pagination
#         # paginate = Pagination.paginate(request=request, total_records=data.count(
#         # ), data=len(sellers), page=page, limit=limit)

#         # return {"sellers": final_data}
#         return {"status_code": HTTP_202_ACCEPTED, "total_amount": str(final_total_amount), "sellers": final_data, "total_items": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "sellers": [], "total_items": 0, "current_page": page, "total_page": 0}


# async def CalculateTotalAmount(db: Session):
#     try:
#         data: OrdersModel = await OrderHelper.getCommissionSellerList(db=db)

#         sellers: OrdersModel = data.group_by(ProductModel.userid).all()

#         final_total_amount = 0
#         for seller in sellers:
#             total_order = []
#             paid_amount = 0

#             total_amount = 0
#             seller_total_amount = 0
#             orders = await OrderHelper.getOrderCommissionList(user_id=seller.userid, db=db)

#             for orderitems in orders:
#                 total_order.append(orderitems.order_id)

#                 # Order
#                 order = db.query(OrdersModel).filter(
#                     OrdersModel.id == orderitems.order_id).first()

#                 # Check Lost Items Check
#                 checklostItems = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status.between(80, 90)).filter(
#                     OrderItemsModel.status != 81).first()

#                 if(checklostItems is None):

#                     if(order.app_version == 'V4'):
#                         # taxable Amount
#                         taxable_amount = calculateTaxableAmount(
#                             price=orderitems.price, quantity=orderitems.quantity)

#                         # GST AMOUNt
#                         gst_amount = calculateGstAmount(
#                             price=taxable_amount, tax=orderitems.tax)

#                         # amount after tax
#                         amount_after_tax = round(
#                             taxable_amount + gst_amount, 2)

#                         # Service Charge
#                         service_charge = calculateCommissionSeller(
#                             amount=taxable_amount, commission=orderitems.commission_seller)

#                         service_charge_on_tax = calculateCommissionTaxSeller(
#                             amount=service_charge, tax=orderitems.commission_seller_tax)

#                         total_service_charge = round(
#                             service_charge + service_charge_on_tax, 2)

#                         # Tcs Amount
#                         tcs_amount = calculateSellerTcsAmount(
#                             amount=taxable_amount, tax=orderitems.tcs_rate)

#                         # Tds Amount
#                         tds_amount = calculateSellerTdsAmount(
#                             amount=amount_after_tax, tax=orderitems.tds_rate)

#                         # Paid Amount
#                         seller_will_get = (amount_after_tax -
#                                            total_service_charge - tcs_amount - tds_amount)

#                         paid_amount += round(seller_will_get, 2)
#                         total_amount += round(amount_after_tax, 2)
#                         # total_amount_ += str(round(paid_amount, 2))+', '
#                         seller_total_amount += round(seller_will_get, 2)

#                     else:

#                         # taxable Amount
#                         taxable_amount = calculateOldTaxableAmount(
#                             price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity)

#                         # GST AMOUNt
#                         gst_amount = calculateGstAmount(
#                             price=taxable_amount, tax=orderitems.tax)

#                         # amount after tax
#                         amount_after_tax = round(
#                             taxable_amount + gst_amount, 2)
#                         # Service Charge

#                         orderdate = order.created_at.strftime("%Y-%m-%d")
#                         # product
#                         productdata = db.query(ProductModel).filter(
#                             ProductModel.id == orderitems.product_id).first()
#                         categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                                       productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                         totalamount = calculateWithoutTaxAmount(
#                             price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                         orderTotalamoutwithouttax = totalamount['total_amount']

#                         tcs_amount = (orderTotalamoutwithouttax *
#                                       TCS) / 100
#                         tcs_amount = round(tcs_amount, 2)

#                         tds_amount = (orderTotalamoutwithouttax * TDS) / 100
#                         tds_amount = round(tds_amount, 2)

#                         if(orderdate < '2021-11-09'):

#                             tcs_amount = 0.00
#                             tds_amount = 0.00

#                             seller_will_get = calculateTotalWithTax(
#                                 price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                             total_amount += seller_will_get['tota_amount']

#                             seller_will_get = (taxable_amount -
#                                                seller_will_get['total_commision'])

#                             paid_amount += round(seller_will_get, 2)
#                             seller_total_amount += round(seller_will_get, 2)

#                         else:

#                             tcs_amount = tcs_amount
#                             tds_amount = tds_amount

#                             seller_will_get = calculateTotal(price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity,
#                                                              commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                             total_amount += seller_will_get['tota_amount']

#                             seller_will_get = (
#                                 seller_will_get['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

#                             paid_amount += round(seller_will_get, 2)

#                             seller_total_amount += round(seller_will_get, 2)

#             final_total_amount += seller_total_amount

#         # Rajdjhani Seller
#         rajdhanidata: OrdersModel = await OrderHelper.getCommissionSellerRajdhaniList(db=db)
#         rajdhanisellers: OrdersModel = rajdhanidata.group_by(
#             ProductModel.userid).all()

#         rajdhani_final_total_amount = 0
#         for rajdhaniseller in rajdhanisellers:
#             total_order = []
#             paid_amount = 0

#             total_amount = 0
#             seller_total_amount = 0
#             orders = await OrderHelper.getOrderCommissionListRajdhani(user_id=rajdhaniseller.userid, db=db)

#             for orderitems in orders:
#                 total_order.append(orderitems.order_id)

#                 # Order
#                 order = db.query(OrdersModel).filter(
#                     OrdersModel.id == orderitems.order_id).first()

#                 # Check Lost Items Check
#                 checklostItems = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status.between(80, 90)).filter(
#                     OrderItemsModel.status != 81).first()

#                 if(checklostItems is None):
#                     if(order.app_version == 'V4'):
#                         # taxable Amount
#                         taxable_amount = calculateTaxableAmount(
#                             price=orderitems.price, quantity=orderitems.quantity)

#                         # GST AMOUNt
#                         gst_amount = calculateGstAmount(
#                             price=taxable_amount, tax=orderitems.tax)

#                         # amount after tax
#                         amount_after_tax = round(
#                             taxable_amount + gst_amount, 2)

#                         # Service Charge
#                         service_charge = calculateCommissionSeller(
#                             amount=taxable_amount, commission=orderitems.commission_seller)

#                         service_charge_on_tax = calculateCommissionTaxSeller(
#                             amount=service_charge, tax=orderitems.commission_seller_tax)

#                         total_service_charge = round(
#                             service_charge + service_charge_on_tax, 2)

#                         # Tcs Amount
#                         tcs_amount = calculateSellerTcsAmount(
#                             amount=taxable_amount, tax=orderitems.tcs_rate)

#                         # Tds Amount
#                         tds_amount = calculateSellerTdsAmount(
#                             amount=amount_after_tax, tax=orderitems.tds_rate)

#                         # Paid Amount
#                         seller_will_get = (amount_after_tax -
#                                            total_service_charge - tcs_amount - tds_amount)

#                         paid_amount += round(seller_will_get, 2)
#                         total_amount += round(amount_after_tax, 2)
#                         # total_amount_ += str(round(paid_amount, 2))+', '
#                         seller_total_amount += round(seller_will_get, 2)

#                     else:

#                         # taxable Amount
#                         taxable_amount = calculateOldTaxableAmount(
#                             price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity)

#                         # GST AMOUNt
#                         gst_amount = calculateGstAmount(
#                             price=taxable_amount, tax=orderitems.tax)

#                         # amount after tax
#                         amount_after_tax = round(
#                             taxable_amount + gst_amount, 2)
#                         # Service Charge

#                         orderdate = order.created_at.strftime("%Y-%m-%d")
#                         # product
#                         productdata = db.query(ProductModel).filter(
#                             ProductModel.id == orderitems.product_id).first()
#                         categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                                       productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                         totalamount = calculateWithoutTaxAmount(
#                             price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                         orderTotalamoutwithouttax = totalamount['total_amount']

#                         tcs_amount = (orderTotalamoutwithouttax *
#                                       TCS) / 100
#                         tcs_amount = round(tcs_amount, 2)

#                         tds_amount = (orderTotalamoutwithouttax * TDS) / 100
#                         tds_amount = round(tds_amount, 2)

#                         if(orderdate < '2021-11-09'):

#                             tcs_amount = 0.00
#                             tds_amount = 0.00

#                             seller_will_get = calculateTotalWithTax(
#                                 price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                             total_amount += seller_will_get['tota_amount']

#                             seller_will_get = (taxable_amount -
#                                                seller_will_get['total_commision'])

#                             paid_amount += round(seller_will_get, 2)
#                             seller_total_amount += round(seller_will_get, 2)

#                         else:

#                             tcs_amount = tcs_amount
#                             tds_amount = tds_amount

#                             seller_will_get = calculateTotal(price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity,
#                                                              commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                             total_amount += seller_will_get['tota_amount']

#                             seller_will_get = (
#                                 seller_will_get['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

#                             paid_amount += round(seller_will_get, 2)

#                             seller_total_amount += round(seller_will_get, 2)

#             rajdhani_final_total_amount += seller_total_amount

#         return round(final_total_amount + rajdhani_final_total_amount, 2)
#     except Exception as e:
#         print(e)
# Generate Commission


# @admin_app_commission_router.get('/generate/commission/{seller_id}')
# async def generateCommission(request: Request, seller_id: int, db: Session = Depends(get_db)):
#     try:

#         if(seller_id == 65):

#             data: OrdersModel = await OrderHelper.getCommissionSellerListRajdhani(db=db)

#             orderItems: OrdersModel = data.filter(
#                 ProductModel.userid == seller_id).all()
#         else:
#             data: OrdersModel = await OrderHelper.getCommissionSellerList(db=db)

#             orderItems: OrdersModel = data.filter(
#                 ProductModel.userid == seller_id).all()

#         total_order = []
#         paid_amount = 0
#         total_tcs_amount = 0
#         total_tds_amount = 0
#         total_commission_amount = 0
#         total_commission_tax_amount = 0
#         total_amount = 0

#         commission_reff = 'REF-' + \
#             str(uuid.uuid4().hex.upper()[0:13])+str(seller_id)

#         for orderitems in orderItems:
#             orderitems = orderitems.OrderItemsModel

#             total_order.append(orderitems.order_id)

#             # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == orderitems.order_id).first()
#             if(order.app_version == 'V4'):
#                 # taxable Amount
#                 taxable_amount = calculateTaxableAmount(
#                     price=orderitems.price, quantity=orderitems.quantity)

#                 # GST AMOUNt
#                 gst_amount = calculateGstAmount(
#                     price=taxable_amount, tax=orderitems.tax)

#                 # amount after tax
#                 amount_after_tax = round(taxable_amount + gst_amount, 2)

#                 # Service Charge
#                 service_charge = calculateCommissionSeller(
#                     amount=taxable_amount, commission=orderitems.commission_seller)

#                 service_charge_on_tax = calculateCommissionTaxSeller(
#                     amount=service_charge, tax=orderitems.commission_seller_tax)

#                 total_commission_amount += service_charge

#                 total_commission_tax_amount += service_charge_on_tax

#                 total_service_charge = round(
#                     service_charge + service_charge_on_tax, 2)

#                 # Tcs Amount
#                 tcs_amount = calculateSellerTcsAmount(
#                     amount=taxable_amount, tax=orderitems.tcs_rate)

#                 # Tds Amount
#                 tds_amount = calculateSellerTdsAmount(
#                     amount=amount_after_tax, tax=orderitems.tds_rate)

#                 # Paid Amount
#                 seller_will_get = (amount_after_tax -
#                                    total_service_charge - tcs_amount - tds_amount)

#                 paid_amount += round(seller_will_get, 2)
#                 total_amount += round(amount_after_tax, 2)

#             else:

#                 # taxable Amount
#                 taxable_amount = calculateOldTaxableAmount(
#                     price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity)

#                 # GST AMOUNt
#                 gst_amount = calculateGstAmount(
#                     price=taxable_amount, tax=orderitems.tax)

#                 # amount after tax
#                 amount_after_tax = round(taxable_amount + gst_amount, 2)
#                 # Service Charge

#                 orderdate = order.created_at.strftime("%Y-%m-%d")
#                 # product
#                 productdata = db.query(ProductModel).filter(
#                     ProductModel.id == orderitems.product_id).first()
#                 categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                               productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                 totalamount = calculateWithoutTaxAmount(
#                     price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                 orderTotalamoutwithouttax = totalamount['total_amount']

#                 tcs_amount = (orderTotalamoutwithouttax *
#                               TCS) / 100
#                 tcs_amount = round(tcs_amount, 2)

#                 tds_amount = (orderTotalamoutwithouttax * TDS) / 100
#                 tds_amount = round(tds_amount, 2)

#                 if(orderdate < '2021-11-09'):

#                     tcs_amount = 0.00
#                     tds_amount = 0.00

#                     seller_will_get = calculateTotalWithTax(
#                         price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                     total_commission_amount += seller_will_get['commission_amount']

#                     total_commission_tax_amount += seller_will_get['tax_amount_on_commission']

#                     total_amount += seller_will_get['tota_amount']

#                     seller_will_get = (taxable_amount -
#                                        seller_will_get['total_commision'])

#                     paid_amount += round(seller_will_get, 2)

#                 else:

#                     tcs_amount = tcs_amount
#                     tds_amount = tds_amount

#                     seller_will_get = calculateTotal(price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity,
#                                                      commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                     total_commission_amount += seller_will_get['commission_amount']

#                     total_commission_tax_amount += seller_will_get['tax_amount_on_commision']

#                     total_amount += seller_will_get['tota_amount']

#                     seller_will_get = (
#                         seller_will_get['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

#                     paid_amount += round(seller_will_get, 2)

#             # Totol Tcs Amount
#             total_tcs_amount += tcs_amount

#             # Total Tds Amount
#             total_tds_amount += tds_amount

#         # Insert Seller Commission
#         today = datetime.now()
#         today = today.strftime("%Y-%m-%d")
#         order_list = set(total_order)

#         db_commission = SellerPaymentModel(
#             seller_id=seller_id,
#             reff_no=commission_reff,
#             total_amount=paid_amount,
#             commission_date=today,
#             commission_amount=total_commission_amount,
#             total_orders=len(order_list),
#             commission_tax_amount=total_commission_tax_amount,
#             tcs_amount=total_tcs_amount,
#             tds_amount=total_tds_amount,
#         )
#         db.add(db_commission)
#         db.commit()
#         db.refresh(db_commission)

#         # Insert Commission Reff orders
#         for ord in order_list:
#             order_data = db.query(OrdersModel).filter(
#                 OrdersModel.id == ord).first()

#             order_data.commission_reff = db_commission.reff_no
#             order_data.updated_at = datetime.now()
#             db.flush()
#             db.commit()

#         # Generate Locked Payments CSV FILE
#         generateddata: OrdersModel = await OrderHelper.getGeneratedOrderItemsList(reff_no=db_commission.reff_no, db=db)
#         item_data = []

#         reff = db.query(SellerPaymentModel).filter(
#             SellerPaymentModel.reff_no == db_commission.reff_no).first()

#         seller = db.query(UserModel).filter(
#             UserModel.id == reff.seller_id).first()

#         order_item_list: OrdersModel = generateddata.group_by(
#             OrderItemsModel.order_id).all()

#         for item in order_item_list:

#             # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == item.order_id).first()

#             # Buyer
#             buyer = db.query(UserProfileModel).filter(
#                 UserProfileModel.user_id == order.user_id).first()

#             buyergstIN = ''
#             if(buyer.proof_type == 'GSTIN'):
#                 buyergstIN = buyer.proof

#             # shipping_address
#             shipping_address = db.query(ShippingAddressModel).filter(
#                 ShippingAddressModel.user_id == order.user_id).first()

#             # Address
#             address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

#             # Items Data
#             items_data = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
#                 OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             # Total GST Amount
#             total_gst_amount = 0
#             # Total Amount After Tax
#             total_amount_after_tax = 0
#             # Total TCS AMOUNT
#             total_tcs_amount = 0
#             # Total TDS AMOUNT
#             total_tds_amount = 0
#             # Total Paid Amount
#             total_paid_amount = 0

#             # Products
#             total_products = ''

#             # GST Rates Amount
#             gst_rates_amount = []

#             # GST RAtes
#             gst_rates = []

#             # Service Charge Amount
#             asez_service_charge_amount = []

#             # Service Rates
#             asez_service_rates = []

#             # Order Date
#             orderdate = order.created_at.strftime("%Y-%m-%d")

#             if(order.app_version == 'V4'):

#                 for itemdata in items_data:

#                     # Product
#                     product = db.query(ProductModel).filter(
#                         ProductModel.id == itemdata.product_id).first()

#                     # Total Products
#                     total_products += str(product.title)+', '

#                     # taxable Amount
#                     taxable_amount = calculateTaxableAmount(
#                         price=itemdata.price, quantity=itemdata.quantity)

#                     # GST RATES
#                     gst_rates.append(itemdata.tax)

#                     gstrates = {
#                         'rate': itemdata.tax,
#                         'amount': taxable_amount
#                     }
#                     gst_rates_amount.append(gstrates)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=itemdata.tax)

#                     # Total GST Amount
#                     total_gst_amount += gst_amount

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     # Total Amount After Tax
#                     total_amount_after_tax += amount_after_tax

#                     # Service Charge
#                     service_charge = calculateCommissionSeller(
#                         amount=taxable_amount, commission=itemdata.commission_seller)

#                     service_charge_on_tax = calculateCommissionTaxSeller(
#                         amount=service_charge, tax=itemdata.commission_seller_tax)

#                     total_service_charge = round(
#                         service_charge + service_charge_on_tax, 2)

#                     # Total Service Charge AMount
#                     # Service RATES
#                     asez_service_rates.append(itemdata.commission_seller)

#                     aszrates = {
#                         'rate': itemdata.commission_seller,
#                         'amount': total_service_charge
#                     }
#                     asez_service_charge_amount.append(aszrates)

#                     # Tcs Amount
#                     tcs_amount = calculateSellerTcsAmount(
#                         amount=taxable_amount, tax=itemdata.tcs_rate)

#                     # Total TCS AMOUNT
#                     total_tcs_amount += tcs_amount

#                     # Tds Amount
#                     tds_amount = calculateSellerTdsAmount(
#                         amount=amount_after_tax, tax=itemdata.tds_rate)

#                     # Total Tds Amount
#                     total_tds_amount += tds_amount

#                     # Paid Amount
#                     paid_amount = (amount_after_tax -
#                                    total_service_charge - tcs_amount - tds_amount)

#                     paid_amount = round(paid_amount, 2)

#                     # Total Paid Amount
#                     total_paid_amount += paid_amount

#             else:

#                 for olditemdata in items_data:
#                     # Product
#                     product = db.query(ProductModel).filter(
#                         ProductModel.id == olditemdata.product_id).first()

#                     # Total Products
#                     total_products += str(product.title)+', '

#                     # taxable Amount
#                     taxable_amount = calculateOldTaxableAmount(
#                         price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity)

#                     # GST RATES
#                     gst_rates.append(olditemdata.tax)

#                     gstrates = {
#                         'rate': olditemdata.tax,
#                         'amount': taxable_amount
#                     }
#                     gst_rates_amount.append(gstrates)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=olditemdata.tax)

#                     # Total GST Amount
#                     total_gst_amount += gst_amount

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     # Total AMount after tax
#                     total_amount_after_tax += amount_after_tax

#                     # product
#                     productdata = db.query(ProductModel).filter(
#                         ProductModel.id == olditemdata.product_id).first()
#                     categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                                   productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                     totalamount = calculateWithoutTaxAmount(
#                         price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                     orderTotalamoutwithouttax = totalamount['total_amount']

#                     tcs_amount = (orderTotalamoutwithouttax *
#                                   TCS) / 100
#                     tcs_amount = round(tcs_amount, 2)

#                     tds_amount = (orderTotalamoutwithouttax * TDS) / 100
#                     tds_amount = round(tds_amount, 2)

#                     if(orderdate < '2021-11-09'):
#                         pass

#                     else:
#                         category_commission = categorycommission.commission

#                         tcs_amount = tcs_amount
#                         # Total TCS Amount
#                         total_tcs_amount += tcs_amount

#                         tds_amount = tds_amount
#                         # Total Tds Amount
#                         total_tds_amount += tds_amount

#                         # Total Service Charge AMount
#                         # Service RATES
#                         asez_service_rates.append(category_commission)

#                         aszrates = {
#                             'rate': category_commission,
#                             'amount': round(
#                                 totalamount['total_commisison'], 2)
#                         }
#                         asez_service_charge_amount.append(aszrates)

#                         paid_amount = calculateTotal(price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity,
#                                                      commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
#                         paid_amount = (
#                             paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

#                         paid_amount = round(paid_amount, 2)

#                         # Total Paid Amount
#                         total_paid_amount += paid_amount

#             # Customize GST RATES
#             gst_rates = set(gst_rates)
#             final_gst_rates = []
#             amount_before_tax = 0
#             for g in gst_rates:
#                 gstam = 0
#                 for gm in gst_rates_amount:
#                     if(gm['rate'] == g):
#                         gstam += gm['amount']
#                         amount_before_tax += gm['amount']
#                 gs = {
#                     'GST Rate '+str(int(g))+'%': gstam
#                 }

#                 final_gst_rates.append(gs)

#             second_obj = {}
#             for obj in final_gst_rates:
#                 second_obj.update(obj)

#             # Customize Service Rates
#             asez_service_rates = set(asez_service_rates)
#             final_asez_rates = []
#             for s in asez_service_rates:
#                 aszam = 0
#                 for am in asez_service_charge_amount:
#                     if(am['rate'] == s):
#                         aszam += am['amount']

#                 asm = {
#                     'Service Charge': str(round(aszam, 2)) + str(' (')+str(s)+str('% + Tax)')
#                 }
#                 final_asez_rates.append(asm)

#             fourth_obj = {}
#             for obj4 in final_asez_rates:
#                 fourth_obj.update(obj4)

#             merged = dict()
#             firstObject = {
#                 'Created At': order.created_at.strftime("%Y-%m-%d"),
#                 'Reff No': db_commission.reff_no,
#                 'Order Number': order.order_number,
#                 'Buyer': shipping_address.ship_to,
#                 'Address': address,
#                 'State': shipping_address.state,
#                 'Product': total_products,
#                 'Buyer GST': buyergstIN,
#             }

#             merged.update(firstObject)
#             merged.update(second_obj)

#             thirdobject = {
#                 'Amount Before Tax': amount_before_tax,
#                 'Tax Amount': floatingValue(round(total_gst_amount, 2)),
#                 'Amount After Tax': floatingValue(total_amount_after_tax),
#             }

#             merged.update(thirdobject)

#             merged.update(fourth_obj)

#             lastobject = {
#                 'Tcs Amount ('+str(TCS)+'%)': floatingValue(round(total_tcs_amount, 2)),
#                 'Tds Amount ('+str(TDS)+'%)': floatingValue(round(total_tds_amount, 2)),
#                 'Status': 'Pending',
#                 'Paid Amount': floatingValue(total_paid_amount)
#             }
#             merged.update(lastobject)

#             item_data.append(merged)

#         item_data.sort(key=len, reverse=True)

#         if(reff.payment_csv is None or reff.payment_csv == 'NULL'):

#             marks_data = pd.DataFrame(item_data)

#             statement = 'payments-csv' + \
#                 str(reff.reff_no)+str('-')+str(seller.id)+str('.csv')

#             #             # saving the csv
#             uploaded_file = getcwd()+"/app/static/paid_payments/"+str(statement)
#             marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

#             # Delete File From Account Transaction
#             MediaHelper.deleteFile(
#                 bucket_name='paid_payments', file_name=statement)

#             # Upload FILE
#             MediaHelper.uploadPaidPaymentsCsv(
#                 uploaded_file=uploaded_file, file_name=statement)

#             # Remove file from Path
#             unlink(uploaded_file)

#             file_path = str(
#                 linode_obj_config['endpoint_url'])+'/paid_payments/'+str(statement)

#             reff.payment_csv = file_path
#             db.flush()
#             db.commit()
#         else:
#             file_path = reff.payment_csv

#         return RedirectResponse(url=f"/admin/seller/commission", status_code=303)

#     except Exception as e:
#         print(e)


# # Generate Commission PDF FILE
# @admin_app_commission_router.post("/generate/commission/generate-pdf/{reff_no}")
# async def generateCommissionPdf(request: Request,  reff_no: str, seller_id: Optional[str] = Form(0), invoice_number: str = Form(...), db: Session = Depends(get_db)):
#     try:

#         today = datetime.now()
#         today = today.strftime("%Y-%m-%d")

#         # Commission Reff
#         commisison_reff = db.query(SellerPaymentModel).filter(
#             SellerPaymentModel.reff_no == reff_no).first()

#         # Seller
#         seller = db.query(UserModel).filter(
#             UserModel.id == commisison_reff.seller_id).first()

#         # Orders
#         orders = await OrderHelper.getGeneratedOrderItemsList(
#             reff_no=reff_no, db=db)

#         # Customize Platform Services
#         commissions = []
#         for orderitems in orders:
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == orderitems.order_id).first()

#             orderdate = order.created_at.strftime("%Y-%m-%d")
#             # product
#             productdata = db.query(ProductModel).filter(
#                 ProductModel.id == orderitems.product_id).first()
#             categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                           productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#             if(order.app_version == 'V4'):
#                 commissions.append(orderitems.commission_seller)
#             else:
#                 commissions.append(categorycommission.commission)

#         commissions = set(commissions)

#         platform_services = []
#         for commission in commissions:
#             orderItems = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(
#                 CategoryCommissionModel, CategoryCommissionModel.category_id == ProductModel.category).filter(OrdersModel.commission_reff == reff_no).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= CategoryCommissionModel.start_date).filter(CategoryCommissionModel.commission == commission).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             total_commission_amount = 0

#             if(len(orderItems) > 0):

#                 for item in orderItems:
#                     orderd = db.query(OrdersModel).filter(
#                         OrdersModel.id == item.order_id).first()

#                     if(orderd.app_version == 'V4'):
#                         # taxable Amount
#                         taxable_amount = calculateTaxableAmount(
#                             price=item.price, quantity=item.quantity)

#                         # Service Charge
#                         service_charge = calculateCommissionSeller(
#                             amount=taxable_amount, commission=item.commission_seller)

#                         total_commission_amount += service_charge

#                     else:

#                         # taxable Amount
#                         taxable_amount = calculateOldTaxableAmount(
#                             price=item.price, tax=item.tax, quantity=item.quantity)

#                         orderdate = orderd.created_at.strftime("%Y-%m-%d")
#                         # product
#                         productdata = db.query(ProductModel).filter(
#                             ProductModel.id == item.product_id).first()
#                         categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                                       productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                         if(orderdate < '2021-11-09'):

#                             seller_will_get = calculateTotalWithTax(
#                                 price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
#                             total_commission_amount += seller_will_get['commission_amount']

#                         else:

#                             seller_will_get = calculateTotal(price=item.price, tax=item.tax, quantity=item.quantity,
#                                                              commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                             total_commission_amount += seller_will_get['commission_amount']

#                 platform = {
#                     'platform_services': commission,
#                     'unit_cost': round(total_commission_amount, 2),
#                     'seller': seller.name
#                 }
#                 platform_services.append(platform)

#         # Update Commission reff
#         commisison_reff.commission_invoice = invoice_number
#         commisison_reff.invoice_date = today
#         db.flush()
#         db.commit()

#         # Customize Invoice Number
#         invoice_number = 'ASEZ-00'+str(commisison_reff.commission_invoice)

#         # Check Seller Billing Address
#         check_billing_address = db.query(BillingAddressModel).filter(
#             BillingAddressModel.user_id == seller.id).first()

#         if(check_billing_address is not None):
#             checkstate = db.query(PincodeModel).filter(
#                 PincodeModel.pincode == check_billing_address.pincode).first()

#         else:
#             checkstate = db.query(PincodeModel).filter(
#                 PincodeModel.pincode == seller.pincode).first()

#         # Check Seller State for igst cgst sgst
#         if(checkstate.statename != 'WEST BENGAL'):

#             igst = commisison_reff.commission_tax_amount
#             cgst = 0.00
#             sgst = 0.00
#         else:
#             igst = 0.00
#             cgst = round(commisison_reff.commission_tax_amount / 2, 2)
#             sgst = round(commisison_reff.commission_tax_amount / 2, 2)

#         # Summary
#         grand_total = (commisison_reff.commission_amount + igst + cgst + sgst +
#                        commisison_reff.tcs_amount + commisison_reff.tds_amount)

#         summary = {
#             'sub_total': commisison_reff.commission_amount,
#             'igst': igst,
#             'cgst': cgst,
#             'sgst': sgst,
#             'tcs_rate': TCS,
#             'tds_rate': TDS,
#             'tcs_amount': commisison_reff.tcs_amount,
#             'tds_amount': commisison_reff.tds_amount,
#             'grand_total': round(grand_total, 2)
#         }

#         pdf_path = getcwd() + '/app/static/commission/' + \
#             str(commisison_reff.reff_no)+'.pdf'

#         TEMPLATE_FILE = "seller/commission_pdf.html"
#         template = templates.get_template(TEMPLATE_FILE)
#         output_text = template.render(
#             platform_services=platform_services,
#             invoice_number=invoice_number,
#             commission_reff=commisison_reff,
#             seller=seller,
#             summary=summary
#         )

#         html_path = BASE_PATH / "app/admin/templates/orders/created_commission_pdf.html"
#         html_file = open(html_path, 'w')
#         html_file.write(output_text)
#         html_file.close()
#         pdf_path = getcwd() + '/app/static/commission/' + \
#             str(commisison_reff.reff_no)+'.pdf'
#         html2pdf(html_path, pdf_path)

#         filename = str(commisison_reff.reff_no)+'.pdf'
#         # Upload file
#         MediaHelper.uploadCommissionPdf(
#             uploaded_file=pdf_path, file_name=filename)

#         # Remove Original File from Path

#         unlink(pdf_path)

#         file_path = str(
#             linode_obj_config['endpoint_url'])+'/commission/'+str(filename)

#         commisison_reff.commission_generated = file_path
#         db.flush()
#         db.commit()

#         if(seller_id != 0):
#             return RedirectResponse(url=f"/admin/seller/commission/generated-list/"+str(seller_id), status_code=303)
#         else:
#             return RedirectResponse(url=f"/admin/seller/commission/generated-list", status_code=303)

#     except Exception as e:
#         print(e)


# def html2pdf(html_path, pdf_path):
#     """
#     Convert html to pdf using pdfkit which is a wrapper of wkhtmltopdf
#     """
#     options = {
#         'page-size': 'A4',
#         'margin-top': '0.0in',
#         'margin-right': '0.75in',
#         'margin-bottom': '0.75in',
#         'margin-left': '0.0in',
#         'encoding': "UTF-8",
#         'no-outline': None,
#         'enable-local-file-access': None
#     }
#     with open(html_path) as f:
#         pdfkit.from_file(f, pdf_path, options=options)

# # (Done 2022-04-28)


# @admin_app_commission_router.get("/order/item/list/{seller_id}", response_model=ListItemListSchema, dependencies=[Depends(JWTBearer())])
# async def ItemList(request: Request, seller_id: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(seller_id != 65):
#             data: OrdersModel = await OrderHelper.getCommissionSellerList(db=db)
#         else:
#             data: OrdersModel = await OrderHelper.getCommissionSellerListRajdhani(db=db)

#         total_records = data.filter(
#             ProductModel.userid == seller_id).all()

#         orderItems: OrdersModel = data.filter(
#             ProductModel.userid == seller_id).group_by(OrderItemsModel.id).limit(
#             limit=limit).offset((page - 1) * limit).all()

#         item_data = []
#         for item in orderItems:
#             item = item.OrderItemsModel

#             # Product
#             product = db.query(ProductModel).filter(
#                 ProductModel.id == item.product_id).first()

#             # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == item.order_id).first()

#             # Check Lost Items Check
#             checklostItems = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status.between(80, 90)).filter(
#                 OrderItemsModel.status != 81).first()

#             # Buyer
#             buyer = db.query(UserProfileModel).filter(
#                 UserProfileModel.user_id == order.user_id).first()

#             buyergstIN = ''
#             if(buyer.proof_type == 'GSTIN'):
#                 buyergstIN = buyer.proof

#             # shipping_address
#             shipping_address = db.query(ShippingAddressModel).filter(
#                 ShippingAddressModel.user_id == order.user_id).first()

#             # Address
#             address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

#             if(checklostItems is None):
#                 if(order.app_version == 'V4'):
#                     # taxable Amount
#                     taxable_amount = calculateTaxableAmount(
#                         price=item.price, quantity=item.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=item.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)
#                     # Service Charge

#                     service_charge = calculateCommissionSeller(
#                         amount=taxable_amount, commission=item.commission_seller)

#                     service_charge_on_tax = calculateCommissionTaxSeller(
#                         amount=service_charge, tax=item.commission_seller_tax)

#                     total_service_charge = round(
#                         service_charge + service_charge_on_tax, 2)

#                     tcs_rate = round(item.tcs_rate)
#                     tds_rate = round(item.tds_rate)

#                     # Tcs Amount
#                     tcs_amount = calculateSellerTcsAmount(
#                         amount=taxable_amount, tax=item.tcs_rate)

#                     # Tds Amount
#                     tds_amount = calculateSellerTdsAmount(
#                         amount=amount_after_tax, tax=item.tds_rate)

#                     service_charge_amount = str(total_service_charge)

#                     service_charge_text = str(
#                         ' (')+str(item.commission_seller)+str('% + Tax)')

#                     # Paid Amount
#                     paid_amount = (amount_after_tax -
#                                    total_service_charge - tcs_amount - tds_amount)

#                     paid_amount = round(paid_amount, 2)

#                 else:
#                     # taxable Amount
#                     taxable_amount = calculateOldTaxableAmount(
#                         price=item.price, tax=item.tax, quantity=item.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=item.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)
#                     # Service Charge

#                     orderdate = order.created_at.strftime("%Y-%m-%d")
#                     # product
#                     productdata = db.query(ProductModel).filter(
#                         ProductModel.id == item.product_id).first()
#                     categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                                   productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                     totalamount = calculateWithoutTaxAmount(
#                         price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                     orderTotalamoutwithouttax = totalamount['total_amount']

#                     tcs_amount = (orderTotalamoutwithouttax *
#                                   TCS) / 100
#                     tcs_amount = round(tcs_amount, 2)

#                     tds_amount = (orderTotalamoutwithouttax * TDS) / 100
#                     tds_amount = round(tds_amount, 2)

#                     if(orderdate < '2021-11-09'):
#                         category_commission = round(
#                             categorycommission.commission + 5)
#                         tcs_amount = 0.00
#                         tds_amount = 0.00
#                         tcs_rate = 0
#                         tds_rate = 0
#                         total_amount = calculateTotalWithTax(
#                             price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
#                         taxable_amount = total_amount['tota_amount']

#                         service_charge_amount = str(
#                             total_amount['total_commision'])

#                         service_charge_text = str(
#                             ' (')+str(category_commission)+str('% + Tax)')

#                         paid_amount = (taxable_amount -
#                                        total_amount['total_commision'])

#                     else:
#                         category_commission = round(
#                             categorycommission.commission)
#                         tcs_rate = TCS
#                         tds_rate = TDS
#                         tcs_amount = tcs_amount
#                         tds_amount = tds_amount

#                         service_charge_amount = str(
#                             totalamount['total_commisison'])

#                         service_charge_text = str(
#                             ' (')+str(category_commission)+str('% + Tax)')

#                         paid_amount = calculateTotal(price=item.price, tax=item.tax, quantity=item.quantity,
#                                                      commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
#                         paid_amount = (
#                             paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

#                         paid_amount = round(paid_amount, 2)

#             itm = {
#                 'order_id': order.id,
#                 'created_at': order.created_at.strftime("%Y-%m-%d"),
#                 'reff_no': '',
#                 'order_number': order.order_number,
#                 'buyer': shipping_address.ship_to,
#                 'address': address,
#                 'state': shipping_address.state,
#                 'product': product.title,
#                 'buyer_GST': buyergstIN,
#                 'taxable_amount': floatingValue(round(taxable_amount, 2)),
#                 'gst_rate': str(round(item.tax))+str("%"),
#                 'gst_amount': floatingValue(round(gst_amount, 2)),
#                 'amount_after_tax': floatingValue(amount_after_tax),
#                 'service_charge': service_charge_amount,
#                 'service_charge_text': service_charge_text,
#                 'tcs_rate': round(tcs_rate),
#                 'tcs_amount': floatingValue(round(tcs_amount, 2)),
#                 'tds_rate': round(tds_rate),
#                 'tds_amount': floatingValue(round(tds_amount, 2)),
#                 'status': '',
#                 'you_will_get': floatingValue(paid_amount),
#                 'paid_amount': floatingValue(0.00)
#             }

#             item_data.append(itm)
#          # Total Records
#         total_records = data.filter(
#             ProductModel.userid == seller_id).count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         # Pagination
#         # paginate = Pagination.paginate(request=request, total_records=len(total_records), data=len(
#         #     orderItems), page=page, limit=limit)

#         reff_no = ''
#         txn_id = ''

#         # Seller Bank Account Detail
#         seller = db.query(UserProfileModel).filter(
#             UserProfileModel.user_id == seller_id).first()
#         seller_bank_account = {
#             'acc_no': seller.acc_no,
#             'account_holder_name': seller.account_holder_name,
#             'bank': seller.bank,
#             'branch': seller.branch,
#             'ifsc_code': seller.ifsc_code,
#             'account_type': seller.account_type,
#         }

#         # return {"reff_no": reff_no, "txn_id": txn_id, "item_data": item_data, "seller_bank_account": seller_bank_account}
#         return {"status_code": HTTP_200_OK, "reff_no": reff_no, "txn_id": txn_id, "item_data": item_data, "seller_bank_account": seller_bank_account, "total_items": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "item_data": [], "seller_bank_account": {}, "total_items": 0, "current_page": page, "total_page": 0}


# # Commission Generated List (Done 2022-04-28)
# @admin_app_commission_router.get("/commission/generated-list", response_model=ListcommissionGeneratedList, dependencies=[Depends(JWTBearer())])
# async def commissionGeneratedList(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data: SellerPaymentModel = await OrderHelper.getAllGeneratedOrderCommissionList(db=db)

#         payments_list: SellerPaymentModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         # Pagination
#         # paginate = Pagination.paginate(request=request, total_records=data.count(), data=len(
#         #     payments_list), page=page, limit=limit)

#         payment_list = []

#         maxref = db.query(func.max(SellerPaymentModel.commission_invoice),
#                           ).one()

#         for payments in payments_list:

#             # Orders
#             orders = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(
#                 OrdersModel.commission_reff == payments.reff_no).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             total_amount = 0
#           # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == orders[0].order_id).first()
#             if(order.app_version == 'V4'):

#                 for orderitems in orders:

#                     # taxable Amount
#                     taxable_amount = calculateTaxableAmount(
#                         price=orderitems.price, quantity=orderitems.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=orderitems.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     total_amount += round(amount_after_tax, 2)

#                 totalamount = round(payments.total_amount, 2) + \
#                     round(payments.commission_amount, 2) + \
#                     round(payments.tcs_amount, 2) + \
#                     round(payments.tds_amount, 2)

#                 commission_amount = (totalamount - total_amount)

#                 commission_amount = (round(payments.commission_amount, 2)
#                                      - round(commission_amount, 2))

#                 seller_amount = (round(total_amount, 2) -
#                                  round(payments.commission_amount, 2))
#             else:
#                 for orderitems in orders:

#                     # taxable Amount
#                     taxable_amount = calculateOldTaxableAmount(
#                         price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=orderitems.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     total_amount += round(amount_after_tax, 2)

#                 totalamount = round(payments.total_amount, 2) + \
#                     round(payments.commission_amount, 2) + \
#                     round(payments.tcs_amount, 2) + \
#                     round(payments.tds_amount, 2)

#                 commission_amount = (totalamount - total_amount)

#                 commission_amount = (round(payments.commission_amount, 2)
#                                      - round(commission_amount, 2))

#                 seller_amount = total_amount - \
#                     round(payments.commission_amount, 2)

#             # Seller
#             seller = db.query(UserModel).filter(
#                 UserModel.id == payments.seller_id).first()
#             seller_address = str(seller.city)+', '+str(seller.region) + \
#                 ', '+str(seller.country)+' - '+str(seller.pincode)

#             commission_invoice = ''
#             if(payments.commission_invoice is not None):
#                 commission_invoice = payments.commission_invoice

#             # Commission Generated PDF
#             commission_generated = ''
#             if(payments.commission_generated is not None):
#                 commission_generated = payments.commission_generated

#             payment_csv = ''
#             if(payments.payment_csv is not None):
#                 payment_csv = payments.payment_csv

#             payment = {
#                 'created_at': payments.commission_date.strftime("%B %d %Y"),
#                 'reff_no': payments.reff_no,
#                 'seller': seller.name,
#                 'mobile': seller.mobile,
#                 'seller_address': seller_address,
#                 'total_orders': payments.total_orders,
#                 'total_amounts': floatingValue(round(total_amount, 2)),
#                 'commission_amount': floatingValue(round(commission_amount, 2)),
#                 'invoice_no': commission_invoice,
#                 'seller_amount': floatingValue(round(seller_amount, 2)),
#                 'tcs_rate': TCS,
#                 'tcs_amount': floatingValue(payments.tcs_amount),
#                 'tds_rate': TCS,
#                 'tds_amount': floatingValue(payments.tds_amount),
#                 'seller_will_get': floatingValue(payments.total_amount),
#                 'payment_date': '',
#                 'status': 'Pedning',
#                 'payment_csv': payment_csv,
#                 'commission_generated': commission_generated
#             }

#             payment_list.append(payment)

#             seller_id = ''

#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         # return {"seller_id": seller_id, "maxref": maxref[0], "payment_list": payment_list}
#         return {"status_code": HTTP_200_OK, "seller_id": seller_id, "maxref": maxref[0], "payment_list": payment_list, "total_items": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "maxref": 0, "payment_list": [], "total_items": 0, "current_page": page, "total_page": 0}


# # Seller List (Done 2022-04-28)
# @admin_app_commission_router.get("/list", response_model=SellerListSchemaList, dependencies=[Depends(JWTBearer())])
# async def SellerList(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):

#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data: SellerPaymentModel = await OrderHelper.getAllSellerCommission(db=db)

#         sellers: SellerPaymentModel = data.group_by(
#             SellerPaymentModel.seller_id).order_by(SellerPaymentModel.seller_id.desc()).limit(
#             limit=limit).offset((page - 1) * limit).all()

#         seller_data = []
#         for seller in sellers:
#             selle = db.query(UserModel).filter(
#                 UserModel.id == seller.seller_id).first()

#             total_count = db.query(SellerPaymentModel).filter(
#                 SellerPaymentModel.seller_id == selle.id).count()

#             s = {
#                 'id': selle.id,
#                 'name': selle.name,
#                 'address': str(selle.city)+', '+str(selle.region)+', '+str(selle.country)+' - '+str(selle.pincode),
#                 'mobile': selle.mobile,
#                 'total_count': total_count
#             }
#             seller_data.append(s)
#         total_records = data.group_by(
#             SellerPaymentModel.seller_id).order_by(SellerPaymentModel.seller_id.desc()).count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         # return {"seller_data": seller_data, "pagination": paginate}
#         return {"status_code": HTTP_200_OK, "seller_data": seller_data, "total_items": total_records, "current_page": page, "total_page": total_pages}
#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "seller_data": [], "total_items": 0, "current_page": page, "total_page": 0}


# # Generated COmmission Item List (Done 2022-04-28)
# @admin_app_commission_router.get("/generated/commission/item/list/{reff_no}", response_model=generatedCommissionItemListSchema, dependencies=[Depends(JWTBearer())])
# async def generatedCommissionItemList(request: Request, reff_no: str, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data: OrdersModel = await OrderHelper.getGeneratedOrderItemsList(reff_no=reff_no, db=db)
#         item_data = []
#         # order_item_list: OrdersModel = data.limit(
#         #     limit=limit).offset((page - 1) * limit).all()

#         order_item_list: OrdersModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         item_total_amount = 0

#         for item in order_item_list:

#             # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == item.order_id).first()

#             # Buyer
#             buyer = db.query(UserProfileModel).filter(
#                 UserProfileModel.user_id == order.user_id).first()

#             buyergstIN = ''
#             if(buyer.proof_type == 'GSTIN'):
#                 buyergstIN = buyer.proof

#             # shipping_address
#             shipping_address = db.query(ShippingAddressModel).filter(
#                 ShippingAddressModel.user_id == order.user_id).first()

#             # Address
#             address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

#             if(order.app_version == 'V4'):
#                 # taxable Amount
#                 taxable_amount = calculateTaxableAmount(
#                     price=item.price, quantity=item.quantity)

#                 # GST AMOUNt
#                 gst_amount = calculateGstAmount(
#                     price=taxable_amount, tax=item.tax)

#                 # amount after tax
#                 amount_after_tax = round(taxable_amount + gst_amount, 2)
#                 # Service Charge

#                 service_charge = calculateCommissionSeller(
#                     amount=taxable_amount, commission=item.commission_seller)

#                 service_charge_on_tax = calculateCommissionTaxSeller(
#                     amount=service_charge, tax=item.commission_seller_tax)

#                 total_service_charge = round(
#                     service_charge + service_charge_on_tax, 2)

#                 tcs_rate = round(item.tcs_rate)
#                 tds_rate = round(item.tds_rate)

#                 # Tcs Amount
#                 tcs_amount = calculateSellerTcsAmount(
#                     amount=taxable_amount, tax=item.tcs_rate)

#                 # Tds Amount
#                 tds_amount = calculateSellerTdsAmount(
#                     amount=amount_after_tax, tax=item.tds_rate)

#                 service_charge_amount = str(total_service_charge)

#                 service_charge_text = str(
#                     ' (')+str(item.commission_seller)+str('% + Tax)')

#                 # Paid Amount
#                 paid_amount = (amount_after_tax -
#                                total_service_charge - tcs_amount - tds_amount)

#                 paid_amount = round(paid_amount, 2)

#                 # Item Total Amount
#                 item_total_amount += paid_amount

#             else:
#                 # taxable Amount
#                 taxable_amount = calculateOldTaxableAmount(
#                     price=item.price, tax=item.tax, quantity=item.quantity)

#                 # GST AMOUNt
#                 gst_amount = calculateGstAmount(
#                     price=taxable_amount, tax=item.tax)

#                 # amount after tax
#                 amount_after_tax = round(taxable_amount + gst_amount, 2)
#                 # Service Charge

#                 orderdate = order.created_at.strftime("%Y-%m-%d")
#                 # product
#                 productdata = db.query(ProductModel).filter(
#                     ProductModel.id == item.product_id).first()
#                 categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                               productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                 totalamount = calculateWithoutTaxAmount(
#                     price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                 orderTotalamoutwithouttax = totalamount['total_amount']

#                 tcs_amount = (orderTotalamoutwithouttax *
#                               TCS) / 100
#                 tcs_amount = round(tcs_amount, 2)

#                 tds_amount = (orderTotalamoutwithouttax * TDS) / 100
#                 tds_amount = round(tds_amount, 2)

#                 if(orderdate < '2021-11-09'):
#                     category_commission = round(
#                         categorycommission.commission + 5)
#                     tcs_amount = 0.00
#                     tds_amount = 0.00
#                     tcs_rate = 0
#                     tds_rate = 0
#                     total_amount = calculateTotalWithTax(
#                         price=item.price, tax=item.tax, quantity=item.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
#                     taxable_amount = total_amount['tota_amount']

#                     service_charge_amount = str(
#                         total_amount['total_commision'])

#                     service_charge_text = str(
#                         ' (')+str(category_commission)+str('% + Tax)')

#                     paid_amount = (taxable_amount -
#                                    total_amount['total_commision'])

#                     # Item Total Amount
#                     item_total_amount += paid_amount

#                 else:
#                     category_commission = round(categorycommission.commission)
#                     tcs_rate = TCS
#                     tds_rate = TDS
#                     tcs_amount = tcs_amount
#                     tds_amount = tds_amount

#                     service_charge_amount = str(
#                         totalamount['total_commisison'])

#                     service_charge_text = str(
#                         ' (')+str(category_commission)+str('% + Tax)')

#                     paid_amount = calculateTotal(price=item.price, tax=item.tax, quantity=item.quantity,
#                                                  commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
#                     paid_amount = (
#                         paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

#                     paid_amount = round(paid_amount, 2)

#                     # Item Total Amount
#                     item_total_amount += paid_amount

#             # Product
#             product = db.query(ProductModel).filter(
#                 ProductModel.id == item.product_id).first()

#             itm = {
#                 'order_id': order.id,
#                 'created_at': order.created_at.strftime("%Y-%m-%d"),
#                 'reff_no': reff_no,
#                 'order_number': order.order_number,
#                 'buyer': shipping_address.ship_to,
#                 'address': address,
#                 'state': shipping_address.state,
#                 'product': product.title,
#                 'buyer_GST': buyergstIN,
#                 'taxable_amount': floatingValue(round(taxable_amount, 2)),
#                 'gst_rate': str(round(item.tax))+str("%"),
#                 'gst_amount': floatingValue(round(gst_amount, 2)),
#                 'amount_after_tax': floatingValue(amount_after_tax),
#                 'service_charge': service_charge_amount,
#                 'service_charge_text': service_charge_text,
#                 'tcs_rate': round(tcs_rate),
#                 'tcs_amount': floatingValue(round(tcs_amount, 2)),
#                 'tds_rate': round(tds_rate),
#                 'tds_amount': floatingValue(round(tds_amount, 2)),
#                 'status': 'Pending',
#                 'you_will_get': floatingValue(paid_amount),
#                 'paid_amount': floatingValue(0.00)
#             }

#             item_data.append(itm)

#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#          # Pagination
#         # paginate = Pagination.paginate(request=request, total_records=data.count(), data=len(
#         #     order_item_list), page=page, limit=limit)

#         # commission reff
#         commission_reff = db.query(SellerPaymentModel).filter(
#             SellerPaymentModel.reff_no == reff_no).first()

#         # Invoice
#         invoice_id = ''
#         if(commission_reff.commission_invoice is not None):
#             invoice_id = commission_reff.commission_invoice

#         txn_id = ''
#         txn_date = ''
#         if(commission_reff.txn_id is not None):
#             txn_id = commission_reff.txn_id
#             txn_date = commission_reff.payment_date

#         # Seller Bank Account Detail
#         seller = db.query(UserProfileModel).filter(
#             UserProfileModel.user_id == commission_reff.seller_id).first()
#         seller_bank_account = {
#             'acc_no': seller.acc_no,
#             'account_holder_name': seller.account_holder_name,
#             'bank': seller.bank,
#             'branch': seller.branch,
#             'ifsc_code': seller.ifsc_code,
#             'account_type': seller.account_type,
#         }

#         # return {"reff_no": reff_no, "txn_id": txn_id, "txn_date": txn_date, "invoice_id": invoice_id, "item_data": item_data, "item_total_amount": round(item_total_amount, 2), "seller_bank_account": seller_bank_account}
#         return {"status_code": HTTP_200_OK, "reff_no": reff_no, "txn_id": txn_id, "txn_date": txn_date, "invoice_id": invoice_id, "item_data": item_data, "item_total_amount": round(item_total_amount, 2), "seller_bank_account": seller_bank_account, "total_items": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "item_data": [], "total_items": 0, "current_page": page, "total_page": 0}

# Generated Invoice List

# (Done 2022-04-28)


# @admin_app_commission_router.get("/commission/invoice-list", response_model=ListcommissionInvoiceList, dependencies=[Depends(JWTBearer())])
# async def commissionGeneratedList(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data: SellerPaymentModel = await OrderHelper.OrderCommissionInvoiceList(db=db)

#         payments_list: SellerPaymentModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         # Pagination
#         # paginate = Pagination.paginate(request=request, total_records=data.count(), data=len(
#         #     payments_list), page=page, limit=limit)

#         payment_list = []

#         for payments in payments_list:

#             # Orders
#             orders = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(
#                 OrdersModel.commission_reff == payments.reff_no).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             total_amount = 0
#           # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == orders[0].order_id).first()
#             if(order.app_version == 'V4'):

#                 for orderitems in orders:

#                     # taxable Amount
#                     taxable_amount = calculateTaxableAmount(
#                         price=orderitems.price, quantity=orderitems.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=orderitems.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     total_amount += round(amount_after_tax, 2)

#                 totalamount = round(payments.total_amount, 2) + \
#                     round(payments.commission_amount, 2) + \
#                     round(payments.tcs_amount, 2) + \
#                     round(payments.tds_amount, 2)

#                 commission_amount = (totalamount - total_amount)

#                 commission_amount = (round(payments.commission_amount, 2)
#                                      - round(commission_amount, 2))

#                 seller_amount = (round(total_amount, 2) -
#                                  round(payments.commission_amount, 2))
#             else:
#                 for orderitems in orders:

#                     # taxable Amount
#                     taxable_amount = calculateOldTaxableAmount(
#                         price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=orderitems.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     total_amount += round(amount_after_tax, 2)

#                 totalamount = round(payments.total_amount, 2) + \
#                     round(payments.commission_amount, 2) + \
#                     round(payments.tcs_amount, 2) + \
#                     round(payments.tds_amount, 2)

#                 commission_amount = (totalamount - total_amount)

#                 commission_amount = (round(payments.commission_amount, 2)
#                                      - round(commission_amount, 2))

#                 seller_amount = total_amount - \
#                     round(payments.commission_amount, 2)

#             # Seller
#             seller = db.query(UserModel).filter(
#                 UserModel.id == payments.seller_id).first()
#             seller_address = str(seller.city)+', '+str(seller.region) + \
#                 ', '+str(seller.country)+' - '+str(seller.pincode)

#             payment_csv = ''
#             if(payments.payment_csv is not None):
#                 payment_csv = payments.payment_csv
#             buyer_payment_csv = ''
#             if(payments.buyer_payment_csv is not None):
#                 buyer_payment_csv = payments.buyer_payment_csv

#             payment = {
#                 'created_at': payments.commission_date.strftime("%Y-%m-%d"),
#                 'reff_no': payments.reff_no,
#                 'seller': seller.name,
#                 'mobile': seller.mobile,
#                 'seller_address': seller_address,
#                 'total_orders': payments.total_orders,
#                 'total_amounts': floatingValue(round(total_amount, 2)),
#                 'commission_amount': floatingValue(round(commission_amount, 2)),
#                 'seller_amount': floatingValue(round(seller_amount, 2)),
#                 'tcs_rate': TCS,
#                 'tcs_amount': floatingValue(payments.tcs_amount),
#                 'tds_rate': TCS,
#                 'tds_amount': floatingValue(payments.tds_amount),
#                 'seller_will_get': floatingValue(payments.total_amount),
#                 'payment_date': payments.payment_date,
#                 'status': payments.txn_id,
#                 'payment_csv': payment_csv,
#                 'buyer_payment_csv': buyer_payment_csv,
#                 'commission_generated': payments.commission_generated
#             }

#             payment_list.append(payment)

#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         # return {"payment_list": payment_list}
#         return {"status_code": HTTP_200_OK, "payment_list": payment_list, "total_items": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "payment_list": [], "total_items": 0, "current_page": page, "total_page": 0}


# # Search Invoice List
# @admin_app_commission_router.post("/commission/invoice/search", dependencies=[Depends(JWTBearer())])
# async def SearchInvoices(request: Request, search: Optional[str] = Form(None), from_date: str = Form(None), to_date: str = Form(None), db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(from_date is None):
#             from_date = 'from_date'
#         if(to_date is None):
#             to_date = 'to_date'

#         if(search is None):
#             search = 'search'

#         return RedirectResponse(url=f"/api/admin/seller-commission/commission/search/invoice-list/"+str(search)+"/"+str(from_date)+"/"+str(to_date), status_code=303)

#     except Exception as e:
#         print(e)

# # Search Invoice List Result (Done 2022-04-28)


# @admin_app_commission_router.get("/commission/search/invoice-list/{search}/{from_date}/{to_date}", response_model=commissionGeneratedListSchema, dependencies=[Depends(JWTBearer())])
# async def commissionGeneratedList(request: Request, search: str, from_date: str, to_date: str, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(search != 'search'):
#             search = search.replace("+", "%")
#             search = search.rstrip()
#             search = "%{}%".format(search)

#         if(search != 'search' and from_date != 'from_date' and to_date != 'to_date'):

#             data: SellerPaymentModel = await OrderHelper.SearchOrderCommissionInvoiceList(search=search.rstrip(), from_date=from_date, to_date=to_date, db=db)
#         elif(search != '' and from_date == 'from_date' and to_date == 'to_date'):
#             data: SellerPaymentModel = await OrderHelper.SearchOrderCommissionInvoiceList(search=search.rstrip(), from_date=from_date, to_date=to_date, db=db)

#         else:
#             data: SellerPaymentModel = await OrderHelper.SearchOrderCommissionInvoiceList(search=search.rstrip(), from_date=from_date, to_date=to_date, db=db)

#         payments_list: SellerPaymentModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         # Pagination
#         # paginate = Pagination.paginate(request=request, total_records=data.count(), data=len(
#         #     payments_list), page=page, limit=limit)

#         payment_list = []

#         for payments in payments_list:

#             # Orders
#             orders = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(
#                 OrdersModel.commission_reff == payments.reff_no).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             total_amount = 0
#           # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == orders[0].order_id).first()
#             if(order.app_version == 'V4'):

#                 for orderitems in orders:

#                     # taxable Amount
#                     taxable_amount = calculateTaxableAmount(
#                         price=orderitems.price, quantity=orderitems.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=orderitems.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     total_amount += round(amount_after_tax, 2)

#                 totalamount = round(payments.total_amount, 2) + \
#                     round(payments.commission_amount, 2) + \
#                     round(payments.tcs_amount, 2) + \
#                     round(payments.tds_amount, 2)

#                 commission_amount = (totalamount - total_amount)

#                 commission_amount = (round(payments.commission_amount, 2)
#                                      - round(commission_amount, 2))

#                 seller_amount = (round(total_amount, 2) -
#                                  round(payments.commission_amount, 2))
#             else:
#                 for orderitems in orders:

#                     # taxable Amount
#                     taxable_amount = calculateOldTaxableAmount(
#                         price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=orderitems.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     total_amount += round(amount_after_tax, 2)

#                 totalamount = round(payments.total_amount, 2) + \
#                     round(payments.commission_amount, 2) + \
#                     round(payments.tcs_amount, 2) + \
#                     round(payments.tds_amount, 2)

#                 commission_amount = (totalamount - total_amount)

#                 commission_amount = (round(payments.commission_amount, 2)
#                                      - round(commission_amount, 2))

#                 seller_amount = total_amount - \
#                     round(payments.commission_amount, 2)

#             # Seller
#             seller = db.query(UserModel).filter(
#                 UserModel.id == payments.seller_id).first()
#             seller_address = str(seller.city)+', '+str(seller.region) + \
#                 ', '+str(seller.country)+' - '+str(seller.pincode)

#             payment_csv = ''
#             if(payments.payment_csv is not None):
#                 payment_csv = payments.payment_csv
#             buyer_payment_csv = ''
#             if(payments.buyer_payment_csv is not None):
#                 buyer_payment_csv = payments.buyer_payment_csv

#             payment = {
#                 'created_at': payments.commission_date.strftime("%Y-%m-%d"),
#                 'reff_no': payments.reff_no,
#                 'seller': seller.name,
#                 'mobile': seller.mobile,
#                 'seller_address': seller_address,
#                 'total_orders': payments.total_orders,
#                 'total_amounts': floatingValue(round(total_amount, 2)),
#                 'commission_amount': floatingValue(round(commission_amount, 2)),
#                 'seller_amount': floatingValue(round(seller_amount, 2)),
#                 'tcs_rate': TCS,
#                 'tcs_amount': floatingValue(payments.tcs_amount),
#                 'tds_rate': TCS,
#                 'tds_amount': floatingValue(payments.tds_amount),
#                 'seller_will_get': floatingValue(payments.total_amount),
#                 'payment_date': payments.payment_date,
#                 'status': payments.txn_id,
#                 'payment_csv': payment_csv,
#                 'buyer_payment_csv': buyer_payment_csv,
#                 'commission_generated': payments.commission_generated
#             }

#             payment_list.append(payment)

#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         # return {"payment_list": payment_list}
#         return {"status_code": HTTP_200_OK, "payment_list": payment_list, "total_items": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "payment_list": [], "total_items": 0, "current_page": page, "total_page": 0}


# # Send Payments to seller
# @admin_app_commission_router.post("/update/payments/{reff_no}")
# async def updatePayments(request: Request, reff_no: str, seller_amount: str = Form(...), txn_id: str = Form(...), db: Session = Depends(get_db)):
#     try:

#         commission_reff = db.query(SellerPaymentModel).filter(SellerPaymentModel.reff_no == reff_no).filter(
#             SellerPaymentModel.commission_invoice.isnot(None)).first()

#         today = datetime.now()
#         today = today.strftime("%Y-%m-%d")

#         commission_reff.txn_id = txn_id
#         commission_reff.payment_date = today
#         db.flush()
#         db.commit()

#         data: OrdersModel = await OrderHelper.getGeneratedOrderItemsList(reff_no=reff_no, db=db)
#         item_data = []

#         seller = db.query(UserModel).filter(
#             UserModel.id == commission_reff.seller_id).first()

#         order_item_list: OrdersModel = data.group_by(
#             OrderItemsModel.order_id).all()

#         for item in order_item_list:

#             # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == item.order_id).first()

#             # Buyer
#             buyer = db.query(UserProfileModel).filter(
#                 UserProfileModel.user_id == order.user_id).first()

#             buyergstIN = ''
#             if(buyer.proof_type == 'GSTIN'):
#                 buyergstIN = buyer.proof

#             # shipping_address
#             shipping_address = db.query(ShippingAddressModel).filter(
#                 ShippingAddressModel.user_id == order.user_id).first()

#             # Address
#             address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

#             # Items Data
#             items_data = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
#                 OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             # Total GST Amount
#             total_gst_amount = 0
#             # Total Amount After Tax
#             total_amount_after_tax = 0
#             # Total TCS AMOUNT
#             total_tcs_amount = 0
#             # Total TDS AMOUNT
#             total_tds_amount = 0
#             # Total Paid Amount
#             total_paid_amount = 0

#             # Products
#             total_products = ''

#             # GST Rates Amount
#             gst_rates_amount = []

#             # GST RAtes
#             gst_rates = []

#             # Service Charge Amount
#             asez_service_charge_amount = []

#             # Service Rates
#             asez_service_rates = []

#             # Order Date
#             orderdate = order.created_at.strftime("%Y-%m-%d")

#             if(order.app_version == 'V4'):

#                 for itemdata in items_data:

#                     # Product
#                     product = db.query(ProductModel).filter(
#                         ProductModel.id == itemdata.product_id).first()

#                     # Total Products
#                     total_products += str(product.title)+', '

#                     # taxable Amount
#                     taxable_amount = calculateTaxableAmount(
#                         price=itemdata.price, quantity=itemdata.quantity)

#                     # GST RATES
#                     gst_rates.append(itemdata.tax)

#                     gstrates = {
#                         'rate': itemdata.tax,
#                         'amount': taxable_amount
#                     }
#                     gst_rates_amount.append(gstrates)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=itemdata.tax)

#                     # Total GST Amount
#                     total_gst_amount += gst_amount

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     # Total Amount After Tax
#                     total_amount_after_tax += amount_after_tax

#                     # Service Charge
#                     service_charge = calculateCommissionSeller(
#                         amount=taxable_amount, commission=itemdata.commission_seller)

#                     service_charge_on_tax = calculateCommissionTaxSeller(
#                         amount=service_charge, tax=itemdata.commission_seller_tax)

#                     total_service_charge = round(
#                         service_charge + service_charge_on_tax, 2)

#                     # Total Service Charge AMount
#                     # Service RATES
#                     asez_service_rates.append(itemdata.commission_seller)

#                     aszrates = {
#                         'rate': itemdata.commission_seller,
#                         'amount': total_service_charge
#                     }
#                     asez_service_charge_amount.append(aszrates)

#                     # Tcs Amount
#                     tcs_amount = calculateSellerTcsAmount(
#                         amount=taxable_amount, tax=itemdata.tcs_rate)

#                     # Total TCS AMOUNT
#                     total_tcs_amount += tcs_amount

#                     # Tds Amount
#                     tds_amount = calculateSellerTdsAmount(
#                         amount=amount_after_tax, tax=itemdata.tds_rate)

#                     # Total Tds Amount
#                     total_tds_amount += tds_amount

#                     # Paid Amount
#                     paid_amount = (amount_after_tax -
#                                    total_service_charge - tcs_amount - tds_amount)

#                     paid_amount = round(paid_amount, 2)

#                     # Total Paid Amount
#                     total_paid_amount += paid_amount

#             else:

#                 for olditemdata in items_data:
#                     # Product
#                     product = db.query(ProductModel).filter(
#                         ProductModel.id == olditemdata.product_id).first()

#                     # Total Products
#                     total_products += str(product.title)+', '

#                     # taxable Amount
#                     taxable_amount = calculateOldTaxableAmount(
#                         price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity)

#                     # GST RATES
#                     gst_rates.append(olditemdata.tax)

#                     gstrates = {
#                         'rate': olditemdata.tax,
#                         'amount': taxable_amount
#                     }
#                     gst_rates_amount.append(gstrates)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=olditemdata.tax)

#                     # Total GST Amount
#                     total_gst_amount += gst_amount

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     # Total AMount after tax
#                     total_amount_after_tax += amount_after_tax

#                     # product
#                     productdata = db.query(ProductModel).filter(
#                         ProductModel.id == olditemdata.product_id).first()
#                     categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                                   productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                     totalamount = calculateWithoutTaxAmount(
#                         price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                     orderTotalamoutwithouttax = totalamount['total_amount']

#                     tcs_amount = (orderTotalamoutwithouttax *
#                                   TCS) / 100
#                     tcs_amount = round(tcs_amount, 2)

#                     tds_amount = (orderTotalamoutwithouttax * TDS) / 100
#                     tds_amount = round(tds_amount, 2)

#                     if(orderdate < '2021-11-09'):
#                         pass

#                     else:
#                         category_commission = categorycommission.commission

#                         tcs_amount = tcs_amount
#                         # Total TCS Amount
#                         total_tcs_amount += tcs_amount

#                         tds_amount = tds_amount
#                         # Total Tds Amount
#                         total_tds_amount += tds_amount

#                         # Total Service Charge AMount
#                         # Service RATES
#                         asez_service_rates.append(category_commission)

#                         aszrates = {
#                             'rate': category_commission,
#                             'amount': round(
#                                 totalamount['total_commisison'], 2)
#                         }
#                         asez_service_charge_amount.append(aszrates)

#                         paid_amount = calculateTotal(price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity,
#                                                      commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
#                         paid_amount = (
#                             paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

#                         paid_amount = round(paid_amount, 2)

#                         # Total Paid Amount
#                         total_paid_amount += paid_amount

#             # Customize GST RATES
#             gst_rates = set(gst_rates)
#             final_gst_rates = []
#             amount_before_tax = 0
#             for g in gst_rates:
#                 gstam = 0
#                 for gm in gst_rates_amount:
#                     if(gm['rate'] == g):
#                         gstam += gm['amount']
#                         amount_before_tax += gm['amount']
#                 gs = {
#                     'GST Rate '+str(int(g))+'%': gstam
#                 }

#                 final_gst_rates.append(gs)

#             second_obj = {}
#             for obj in final_gst_rates:
#                 second_obj.update(obj)

#             # Customize Service Rates
#             asez_service_rates = set(asez_service_rates)
#             final_asez_rates = []
#             for s in asez_service_rates:
#                 aszam = 0
#                 for am in asez_service_charge_amount:
#                     if(am['rate'] == s):
#                         aszam += am['amount']

#                 asm = {
#                     'Service Charge': str(round(aszam, 2)) + str(' (')+str(s)+str('% + Tax)')
#                 }
#                 final_asez_rates.append(asm)

#             fourth_obj = {}
#             for obj4 in final_asez_rates:
#                 fourth_obj.update(obj4)

#             merged = dict()
#             firstObject = {
#                 'Created At': order.created_at.strftime("%Y-%m-%d"),
#                 'Reff No': commission_reff.reff_no,
#                 'Order Number': order.order_number,
#                 'Buyer': shipping_address.ship_to,
#                 'Address': address,
#                 'State': shipping_address.state,
#                 'Product': total_products,
#                 'Buyer GST': buyergstIN,
#             }

#             merged.update(firstObject)
#             merged.update(second_obj)

#             thirdobject = {
#                 'Amount Before Tax': amount_before_tax,
#                 'Tax Amount': floatingValue(round(total_gst_amount, 2)),
#                 'Amount After Tax': floatingValue(total_amount_after_tax),
#             }

#             merged.update(thirdobject)

#             merged.update(fourth_obj)

#             lastobject = {
#                 'Tcs Amount ('+str(TCS)+'%)': floatingValue(round(total_tcs_amount, 2)),
#                 'Tds Amount ('+str(TDS)+'%)': floatingValue(round(total_tds_amount, 2)),
#                 'Status': commission_reff.txn_id,
#                 'Paid Amount': floatingValue(total_paid_amount)
#             }
#             merged.update(lastobject)

#             item_data.append(merged)

#         item_data.sort(key=len, reverse=True)

#         if(commission_reff.payment_csv is not None):

#             marks_data = pd.DataFrame(item_data)

#             statement = 'paid-payments-csv' + \
#                 str(commission_reff.reff_no) + \
#                 str('-')+str(seller.id)+str('.csv')

#             #             # saving the csv
#             uploaded_file = getcwd()+"/app/static/paid_payments/"+str(statement)
#             marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

#             # Delete File From Account Transaction
#             MediaHelper.deleteFile(
#                 bucket_name='paid_payments', file_name=statement)

#             # Upload FILE
#             MediaHelper.uploadPaidPaymentsCsv(
#                 uploaded_file=uploaded_file, file_name=statement)

#             # Remove file from Path
#             unlink(uploaded_file)

#             file_path = str(
#                 linode_obj_config['endpoint_url'])+'/paid_payments/'+str(statement)

#             commission_reff.payment_csv = file_path
#             db.flush()
#             db.commit()

#         if(commission_reff.buyer_payment_csv is None or commission_reff.buyer_payment_csv == 'NULL'):

#             # Payment Reff
#             payment_reff = db.query(SellerPaymentModel).filter(
#                 SellerPaymentModel.reff_no == reff_no).filter(SellerPaymentModel.txn_id.isnot(None)).first()

#             # orders items
#             orderItems = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff == payment_reff.reff_no).filter(OrderItemsModel.status >= 70).filter(
#                 OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).group_by(OrderItemsModel.order_id).order_by(OrderItemsModel.order_id.desc()).all()

#             items = []
#             for orderitem in orderItems:
#                 order = db.query(OrdersModel).filter(
#                     OrdersModel.id == orderitem.order_id).first()

#                 # Buyer
#                 buyer = db.query(ShippingAddressModel).filter(
#                     ShippingAddressModel.id == order.address_id).first()

#                 # Seller
#                 sellerproduct = db.query(ProductModel).filter(
#                     ProductModel.id == orderitem.product_id).first()
#                 seller = db.query(UserModel).filter(
#                     UserModel.id == sellerproduct.userid).first()

#                 # BuyerPincode
#                 buyerpincode = db.query(PincodeModel).filter(
#                     PincodeModel.pincode == buyer.pincode).first()

#                 # seller pincode
#                 sellerpincode = db.query(PincodeModel).filter(
#                     PincodeModel.pincode == seller.pincode).first()

#                 # Buyer Profile
#                 profile = db.query(UserProfileModel).filter(
#                     UserProfileModel.user_id == order.user_id).first()

#                 # orderdate
#                 order_date = order.created_at.strftime("%Y-%m-%d")

#                 # Items Data
#                 items_data = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
#                     OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#                 # Total taxable Amount
#                 total_taxable_amount = 0
#                 # Total Discount Amount
#                 total_discount_amount = 0
#                 # Buyer Total Amount
#                 buyer_total_amount = 0
#                 # Total GST AMount
#                 buyer_total_gst_amount = 0
#                 # Total Asezservice_amount
#                 buyer_total_asez_service_amount = 0

#                 # Buyer Asez Service GST AMOUNT
#                 buyer_total_asez_service_gst_amount = 0

#                 # Aseztak Service
#                 buyer_aseztak_service = Services.aseztak_services(
#                     order.created_at, db=db)

#                 if(order.app_version == 'V4'):
#                     for item_data in items_data:
#                         # Aseztak Service
#                         aseztak_service = Services.aseztak_services(
#                             item_data.created_at, db=db)

#                         # Taxable Amount
#                         taxable_amount = calculateTaxableAmountItemWise(app_version=order.app_version, price=item_data.price, tax=item_data.tax, quantity=item_data.quantity, commission=item_data.commission_buyer,
#                                                                         gst_on_commission=item_data.commission_buyer_tax, tds_rate=item_data.tds_rate, tcs_rate=item_data.tcs_rate, round_of=aseztak_service.round_off)

#                         # Total Taxable Amount
#                         total_taxable_amount += taxable_amount

#                         # Discount on Product Value
#                         discount = 0
#                         if(order.discount != 0):
#                             discount = calculateDiscountAmountItemWise(
#                                 app_version=order.app_version, total_amount=item_data.discount_amount, discount_rate=item_data.discount_rate)

#                         # Total Discount Amount
#                         total_discount_amount += discount

#                         # Total Amount
#                         total_amount = (taxable_amount - discount)
#                         total_amount = round(total_amount, 2)

#                         # Buyer Total Amount
#                         buyer_total_amount += total_amount

#                         # GST On Item
#                         if(item_data.igst_on_tax == 0):
#                             gstonitems = (item_data.cgst_on_tax +
#                                           item_data.sgst_on_tax)
#                         else:
#                             gstonitems = (item_data.igst_on_tax)

#                         gst_on_items = calculateGstOnItems(
#                             app_version=order.app_version, total_amount=gstonitems, tax=item_data.tax)

#                         # Total GST Amount
#                         buyer_total_gst_amount += gst_on_items

#                         # Total Asezservice Amount
#                         buyer_total_asez_service_amount += item_data.asez_service_amount

#                         if(item_data.asez_service_on_igst == 0):
#                             gstonasezservice = (
#                                 item_data.asez_service_on_sgst + item_data.asez_service_on_cgst)
#                         else:
#                             gstonasezservice = item_data.asez_service_on_igst

#                         buyer_total_asez_service_gst_amount += gstonasezservice

#                 else:
#                     for olditem_data in items_data:

#                         # Aseztak Service
#                         aseztak_service = Services.aseztak_services(
#                             olditem_data.created_at, db=db)

#                         if(aseztak_service is not None):

#                             # Taxable Amount
#                             taxable_amount = calculateTaxableAmountItemWise(app_version=order.app_version, price=olditem_data.price, tax=olditem_data.tax, quantity=olditem_data.quantity, commission=aseztak_service.rate,
#                                                                             gst_on_commission=aseztak_service.gst_on_rate, tds_rate=aseztak_service.tds_rate, tcs_rate=aseztak_service.tcs_rate, round_of=aseztak_service.round_off)

#                             # Total Taxable Amount
#                             total_taxable_amount += taxable_amount

#                             # Discount on Product Value
#                             discount = 0
#                             if(order.discount != 0):
#                                 discount = db.query(OrderDiscountModel).filter(
#                                     OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                                 discount = calculateDiscountAmountItemWise(
#                                     app_version=order.app_version, total_amount=taxable_amount, discount_rate=discount.discount)

#                             # Total Discount Amount
#                             total_discount_amount += discount

#                             # Total Amount
#                             total_amount = (taxable_amount - discount)
#                             total_amount = round(total_amount, 2)

#                             # Buyer Total Amount
#                             buyer_total_amount += total_amount

#                             # GST On Item
#                             gst_on_items = calculateGstOnItems(
#                                 app_version=order.app_version, total_amount=total_amount, tax=olditem_data.tax)

#                             # Total GST Amount
#                             buyer_total_gst_amount += gst_on_items

#                             # Asez Service
#                             asez_service_on_items = aseztakServiceOnItems(
#                                 app_version=order.app_version, total_amount=total_amount, rate=aseztak_service.rate, gst_on_rate=aseztak_service.gst_on_rate, tcs_rate=aseztak_service.tcs_rate, tds_rate=aseztak_service.tds_rate)

#                             # Total Asez Amount
#                             asezServiceAmount = asez_service_on_items['asez_service']
#                             asezServiceGst = asez_service_on_items['gst_on_service']
#                             buyer_total_asez_service_amount += asezServiceAmount

#                             buyer_total_asez_service_gst_amount += asezServiceGst

#                 # Grand Total
#                 grand_total = (
#                     buyer_total_amount + buyer_total_gst_amount + buyer_total_asez_service_amount + buyer_total_asez_service_gst_amount)

#                 shipping_charge = db.query(ShippingChargeModel).filter(
#                     ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#                 # Delivery Charge
#                 delivery_charge = orderDeliveryCalculation(
#                     app_version=order.app_version, order_amount=grand_total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

#                 grand_total = round(grand_total, 2)

#                 round_of = round(grand_total, 0) - grand_total

#                 proof = ''
#                 if(profile.proof_type == 'GSTIN'):
#                     proof = profile.proof

#                 # asezigst = buyer_total_asez_service_gst_amount
#                 if(str(buyerpincode.statename) == str(sellerpincode.statename)):
#                     asezcgst = (buyer_total_asez_service_gst_amount / 2)
#                     asezcgst = round(asezcgst, 2)
#                     asesgst = asezcgst
#                     asezigst = 0.00
#                 else:
#                     asezigst = round(buyer_total_asez_service_gst_amount, 2)
#                     asezcgst = 0.00
#                     asesgst = 0.00

#                 itms = {
#                     'Order Date': order_date,
#                     'Order Number': order.order_number,
#                     'Buyer Name': buyer.ship_to,
#                     'Address': str(buyer.address) + ', '+str(buyer.locality)+', '+str(buyer.city)+', '+str(buyer.city)+', '+str(buyer.state)+' - '+str(buyer.pincode),
#                     'state': str(buyer.state),
#                     'Buyer GSTIN': proof,
#                     'Product Value': total_taxable_amount,
#                     'Discount': total_discount_amount,
#                     'Total': buyer_total_amount,
#                     'GST on Products': buyer_total_gst_amount,
#                     'Aseztak Service (' + str(
#                         round(buyer_aseztak_service.rate))+'%)': buyer_total_asez_service_amount,
#                     'GST on Service (' + str(round(buyer_aseztak_service.gst_on_rate))+'%)': buyer_total_asez_service_gst_amount,
#                     'Central GST': asezcgst,
#                     'State GST': asesgst,
#                     'Intrastate GST': asezigst,
#                     'Shipping Charge': round(delivery_charge, 2),
#                     'GST on Shipping': '',
#                     'Round r/Off': round(round_of, 2),
#                     'Paid Amount': round(grand_total + delivery_charge, 0)
#                 }

#                 items.append(itms)

#             marks_data = pd.DataFrame(items)

#             statement = 'new-buyers-payments-csv' + \
#                 str(payment_reff.reff_no)+str('.csv')

#             #             # saving the csv
#             uploaded_file = getcwd()+"/app/static/buyer_payments/"+str(statement)
#             marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

#             # Delete File From Account Transaction
#             MediaHelper.deleteFile(
#                 bucket_name='buyer_payments', file_name=statement)

#             # Upload FILE
#             MediaHelper.uploadBuyerPaymentsCsv(
#                 uploaded_file=uploaded_file, file_name=statement)

#             # Remove file from Path
#             unlink(uploaded_file)

#             file_path = str(
#                 linode_obj_config['endpoint_url'])+'/buyer_payments/'+str(statement)

#             payment_reff.buyer_payment_csv = file_path
#             db.flush()
#             db.commit()

#             return RedirectResponse(url=f"/admin/seller/generated/commission/item/list/"+str(commission_reff.reff_no), status_code=303)
#     except Exception as e:
#         print(e)


# Commission Generated List Seller Wise (Done 2022-04-28)
# @admin_app_commission_router.get("/commission/generated-list/{seller_id}", response_model=ListCommissionGeneratedSellerWiseList, dependencies=[Depends(JWTBearer())])
# async def commissionGeneratedList(request: Request, seller_id: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data: SellerPaymentModel = await OrderHelper.getAllGeneratedOrderCommissionListSellerWise(db=db, seller_id=seller_id)

#         payments_list: SellerPaymentModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         # # Pagination
#         # paginate = Pagination.paginate(request=request, total_records=data.count(), data=len(
#         #     payments_list), page=page, limit=limit)

#         payment_list = []

#         maxref = db.query(func.max(SellerPaymentModel.commission_invoice),
#                           ).one()

#         for payments in payments_list:

#             # Orders
#             orders = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(
#                 OrdersModel.commission_reff == payments.reff_no).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             total_amount = 0
#           # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == orders[0].order_id).first()
#             if(order.app_version == 'V4'):

#                 for orderitems in orders:

#                     # taxable Amount
#                     taxable_amount = calculateTaxableAmount(
#                         price=orderitems.price, quantity=orderitems.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=orderitems.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     total_amount += round(amount_after_tax, 2)

#                 totalamount = round(payments.total_amount, 2) + \
#                     round(payments.commission_amount, 2) + \
#                     round(payments.tcs_amount, 2) + \
#                     round(payments.tds_amount, 2)

#                 commission_amount = (totalamount - total_amount)

#                 commission_amount = (round(payments.commission_amount, 2)
#                                      - round(commission_amount, 2))

#                 seller_amount = (round(total_amount, 2) -
#                                  round(payments.commission_amount, 2))
#             else:
#                 for orderitems in orders:

#                     # taxable Amount
#                     taxable_amount = calculateOldTaxableAmount(
#                         price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=orderitems.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     total_amount += round(amount_after_tax, 2)

#                 totalamount = round(payments.total_amount, 2) + \
#                     round(payments.commission_amount, 2) + \
#                     round(payments.tcs_amount, 2) + \
#                     round(payments.tds_amount, 2)

#                 commission_amount = (totalamount - total_amount)

#                 commission_amount = (round(payments.commission_amount, 2)
#                                      - round(commission_amount, 2))

#                 seller_amount = total_amount - \
#                     round(payments.commission_amount, 2)

#             # Seller
#             seller = db.query(UserModel).filter(
#                 UserModel.id == payments.seller_id).first()
#             seller_address = str(seller.city)+', '+str(seller.region) + \
#                 ', '+str(seller.country)+' - '+str(seller.pincode)

#             commission_invoice = ''
#             if(payments.commission_invoice is not None):
#                 commission_invoice = payments.commission_invoice

#             # Commission Generated PDF
#             commission_generated = ''
#             if(payments.commission_generated is not None):
#                 commission_generated = payments.commission_generated

#             payment_csv = ''
#             if(payments.payment_csv is not None):
#                 payment_csv = payments.payment_csv

#             payment = {
#                 'created_at': payments.commission_date.strftime("%B %d %Y"),
#                 'reff_no': payments.reff_no,
#                 'seller': seller.name,
#                 'mobile': seller.mobile,
#                 'seller_address': seller_address,
#                 'total_orders': payments.total_orders,
#                 'total_amounts': floatingValue(round(total_amount, 2)),
#                 'commission_amount': floatingValue(round(commission_amount, 2)),
#                 'invoice_no': commission_invoice,
#                 'seller_amount': floatingValue(round(seller_amount, 2)),
#                 'tcs_rate': TCS,
#                 'tcs_amount': floatingValue(payments.tcs_amount),
#                 'tds_rate': TCS,
#                 'tds_amount': floatingValue(payments.tds_amount),
#                 'seller_will_get': floatingValue(payments.total_amount),
#                 'payment_date': '',
#                 'status': 'Pedning',
#                 'payment_csv': payment_csv,
#                 'commission_generated': commission_generated
#             }

#             payment_list.append(payment)

#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         # return {"seller_id": seller_id, "maxref": maxref[0], "payment_list": payment_list}
#         return {"status_code": HTTP_200_OK, "seller_id": seller_id, "maxref": maxref[0], "payment_list": payment_list, "total_items": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "seller_id": seller_id, "maxref": 0, "payment_list": [], "total_items": 0, "current_page": page, "total_page": 0}

# # Commission Invoice List Seller Wise

# # (Done 2022-04-28)


# @admin_app_commission_router.get("/commission/invoice-list/{seller_id}", response_model=commissionGeneratedListSchema, dependencies=[Depends(JWTBearer())])
# async def commissionGeneratedList(request: Request, seller_id: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data: SellerPaymentModel = await OrderHelper.OrderCommissionInvoiceListSellerWise(db=db, seller_id=seller_id)

#         payments_list: SellerPaymentModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         # Pagination
#         # paginate = Pagination.paginate(request=request, total_records=data.count(), data=len(
#         #     payments_list), page=page, limit=limit)

#         payment_list = []

#         for payments in payments_list:

#             # Orders
#             orders = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(
#                 OrdersModel.commission_reff == payments.reff_no).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             total_amount = 0
#           # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == orders[0].order_id).first()
#             if(order.app_version == 'V4'):

#                 for orderitems in orders:

#                     # taxable Amount
#                     taxable_amount = calculateTaxableAmount(
#                         price=orderitems.price, quantity=orderitems.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=orderitems.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     total_amount += round(amount_after_tax, 2)

#                 totalamount = round(payments.total_amount, 2) + \
#                     round(payments.commission_amount, 2) + \
#                     round(payments.tcs_amount, 2) + \
#                     round(payments.tds_amount, 2)

#                 commission_amount = (totalamount - total_amount)

#                 commission_amount = (round(payments.commission_amount, 2)
#                                      - round(commission_amount, 2))

#                 seller_amount = (round(total_amount, 2) -
#                                  round(payments.commission_amount, 2))
#             else:
#                 for orderitems in orders:

#                     # taxable Amount
#                     taxable_amount = calculateOldTaxableAmount(
#                         price=orderitems.price, tax=orderitems.tax, quantity=orderitems.quantity)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=orderitems.tax)

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     total_amount += round(amount_after_tax, 2)

#                 totalamount = round(payments.total_amount, 2) + \
#                     round(payments.commission_amount, 2) + \
#                     round(payments.tcs_amount, 2) + \
#                     round(payments.tds_amount, 2)

#                 commission_amount = (totalamount - total_amount)

#                 commission_amount = (round(payments.commission_amount, 2)
#                                      - round(commission_amount, 2))

#                 seller_amount = total_amount - \
#                     round(payments.commission_amount, 2)

#             # Seller
#             seller = db.query(UserModel).filter(
#                 UserModel.id == payments.seller_id).first()
#             seller_address = str(seller.city)+', '+str(seller.region) + \
#                 ', '+str(seller.country)+' - '+str(seller.pincode)

#             payment_csv = ''
#             if(payments.payment_csv is not None):
#                 payment_csv = payments.payment_csv

#             buyer_payment_csv = ''
#             if(payments.buyer_payment_csv is not None):
#                 buyer_payment_csv = payments.buyer_payment_csv

#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#             payment = {
#                 'created_at': payments.commission_date.strftime("%B %d %Y"),
#                 'reff_no': payments.reff_no,
#                 'seller': seller.name,
#                 'mobile': seller.mobile,
#                 'seller_address': seller_address,
#                 'total_orders': payments.total_orders,
#                 'total_amounts': floatingValue(round(total_amount, 2)),
#                 'commission_amount': floatingValue(round(commission_amount, 2)),
#                 'seller_amount': floatingValue(round(seller_amount, 2)),
#                 'tcs_rate': TCS,
#                 'tcs_amount': floatingValue(payments.tcs_amount),
#                 'tds_rate': TCS,
#                 'tds_amount': floatingValue(payments.tds_amount),
#                 'seller_will_get': floatingValue(payments.total_amount),
#                 'payment_date': payments.payment_date,
#                 'status': payments.txn_id,
#                 'payment_csv': payment_csv,
#                 'buyer_payment_csv': buyer_payment_csv,
#                 'commission_generated': payments.commission_generated
#             }

#             payment_list.append(payment)

#         # return {"payment_list": payment_list}
#         return {"status_code": HTTP_200_OK, "payment_list": payment_list, "total_items": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "payment_list": [], "total_items": 0, "current_page": page, "total_page": 0}


# # Send Payments to seller
# @admin_app_commission_router.get("/generate/payment/csv/{reff_no}")
# async def updatePayments(request: Request, reff_no: str, db: Session = Depends(get_db)):
#     try:

#         commission_reff = db.query(SellerPaymentModel).filter(SellerPaymentModel.reff_no == reff_no).filter(
#             SellerPaymentModel.commission_invoice.isnot(None)).first()

#         data: OrdersModel = await OrderHelper.getGeneratedOrderItemsList(reff_no=reff_no, db=db)
#         item_data = []

#         seller = db.query(UserModel).filter(
#             UserModel.id == commission_reff.seller_id).first()

#         order_item_list: OrdersModel = data.group_by(
#             OrderItemsModel.order_id).all()

#         for item in order_item_list:

#             # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == item.order_id).first()

#             # Buyer
#             buyer = db.query(UserProfileModel).filter(
#                 UserProfileModel.user_id == order.user_id).first()

#             buyergstIN = ''
#             if(buyer.proof_type == 'GSTIN'):
#                 buyergstIN = buyer.proof

#             # shipping_address
#             shipping_address = db.query(ShippingAddressModel).filter(
#                 ShippingAddressModel.user_id == order.user_id).first()

#             # Address
#             address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

#             # Items Data
#             items_data = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
#                 OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             # Total GST Amount
#             total_gst_amount = 0
#             # Total Amount After Tax
#             total_amount_after_tax = 0
#             # Total TCS AMOUNT
#             total_tcs_amount = 0
#             # Total TDS AMOUNT
#             total_tds_amount = 0
#             # Total Paid Amount
#             total_paid_amount = 0

#             # Products
#             total_products = ''

#             # GST Rates Amount
#             gst_rates_amount = []

#             # GST RAtes
#             gst_rates = []

#             # Service Charge Amount
#             asez_service_charge_amount = []

#             # Service Rates
#             asez_service_rates = []

#             # Order Date
#             orderdate = order.created_at.strftime("%Y-%m-%d")

#             if(order.app_version == 'V4'):

#                 for itemdata in items_data:

#                     # Product
#                     product = db.query(ProductModel).filter(
#                         ProductModel.id == itemdata.product_id).first()

#                     # Total Products
#                     total_products += str(product.title)+', '

#                     # taxable Amount
#                     taxable_amount = calculateTaxableAmount(
#                         price=itemdata.price, quantity=itemdata.quantity)

#                     # GST RATES
#                     gst_rates.append(itemdata.tax)

#                     gstrates = {
#                         'rate': itemdata.tax,
#                         'amount': taxable_amount
#                     }
#                     gst_rates_amount.append(gstrates)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=itemdata.tax)

#                     # Total GST Amount
#                     total_gst_amount += gst_amount

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     # Total Amount After Tax
#                     total_amount_after_tax += amount_after_tax

#                     # Service Charge
#                     service_charge = calculateCommissionSeller(
#                         amount=taxable_amount, commission=item.commission_seller)

#                     service_charge_on_tax = calculateCommissionTaxSeller(
#                         amount=service_charge, tax=itemdata.commission_seller_tax)

#                     total_service_charge = round(
#                         service_charge + service_charge_on_tax, 2)

#                     # Total Service Charge AMount
#                     # Service RATES
#                     asez_service_rates.append(itemdata.commission_seller)

#                     aszrates = {
#                         'rate': itemdata.commission_seller,
#                         'amount': total_service_charge
#                     }
#                     asez_service_charge_amount.append(aszrates)

#                     # Tcs Amount
#                     tcs_amount = calculateSellerTcsAmount(
#                         amount=taxable_amount, tax=itemdata.tcs_rate)

#                     # Total TCS AMOUNT
#                     total_tcs_amount += tcs_amount

#                     # Tds Amount
#                     tds_amount = calculateSellerTdsAmount(
#                         amount=amount_after_tax, tax=itemdata.tds_rate)

#                     # Total Tds Amount
#                     total_tds_amount += tds_amount

#                     # Paid Amount
#                     paid_amount = (amount_after_tax -
#                                    total_service_charge - tcs_amount - tds_amount)

#                     paid_amount = round(paid_amount, 2)

#                     # Total Paid Amount
#                     total_paid_amount += paid_amount

#             else:

#                 for olditemdata in items_data:
#                     # Product
#                     product = db.query(ProductModel).filter(
#                         ProductModel.id == olditemdata.product_id).first()

#                     # Total Products
#                     total_products += str(product.title)+', '

#                     # taxable Amount
#                     taxable_amount = calculateOldTaxableAmount(
#                         price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity)

#                     # GST RATES
#                     gst_rates.append(olditemdata.tax)

#                     gstrates = {
#                         'rate': olditemdata.tax,
#                         'amount': taxable_amount
#                     }
#                     gst_rates_amount.append(gstrates)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=olditemdata.tax)

#                     # Total GST Amount
#                     total_gst_amount += gst_amount

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     # Total AMount after tax
#                     total_amount_after_tax += amount_after_tax

#                     # product
#                     productdata = db.query(ProductModel).filter(
#                         ProductModel.id == olditemdata.product_id).first()
#                     categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                                   productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                     totalamount = calculateWithoutTaxAmount(
#                         price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                     orderTotalamoutwithouttax = totalamount['total_amount']

#                     tcs_amount = (orderTotalamoutwithouttax *
#                                   TCS) / 100
#                     tcs_amount = round(tcs_amount, 2)

#                     tds_amount = (orderTotalamoutwithouttax * TDS) / 100
#                     tds_amount = round(tds_amount, 2)

#                     if(orderdate < '2021-11-09'):
#                         pass

#                     else:
#                         category_commission = categorycommission.commission

#                         tcs_amount = tcs_amount
#                         # Total TCS Amount
#                         total_tcs_amount += tcs_amount

#                         tds_amount = tds_amount
#                         # Total Tds Amount
#                         total_tds_amount += tds_amount

#                         # Total Service Charge AMount
#                         # Service RATES
#                         asez_service_rates.append(category_commission)

#                         aszrates = {
#                             'rate': category_commission,
#                             'amount': round(
#                                 totalamount['total_commisison'], 2)
#                         }
#                         asez_service_charge_amount.append(aszrates)

#                         paid_amount = calculateTotal(price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity,
#                                                      commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
#                         paid_amount = (
#                             paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

#                         paid_amount = round(paid_amount, 2)

#                         # Total Paid Amount
#                         total_paid_amount += paid_amount

#             # Customize GST RATES
#             gst_rates = set(gst_rates)
#             final_gst_rates = []
#             amount_before_tax = 0
#             for g in gst_rates:
#                 gstam = 0
#                 for gm in gst_rates_amount:
#                     if(gm['rate'] == g):
#                         gstam += gm['amount']
#                         amount_before_tax += gm['amount']
#                 gs = {
#                     'GST Rate '+str(int(g))+'%': gstam
#                 }

#                 final_gst_rates.append(gs)

#             second_obj = {}
#             for obj in final_gst_rates:
#                 second_obj.update(obj)

#             # Customize Service Rates
#             asez_service_rates = set(asez_service_rates)
#             final_asez_rates = []
#             for s in asez_service_rates:
#                 aszam = 0
#                 for am in asez_service_charge_amount:
#                     if(am['rate'] == s):
#                         aszam += am['amount']

#                 asm = {
#                     'Service Charge': str(round(aszam, 2)) + str(' (')+str(s)+str('% + Tax)')
#                 }
#                 final_asez_rates.append(asm)

#             fourth_obj = {}
#             for obj4 in final_asez_rates:
#                 fourth_obj.update(obj4)

#             merged = dict()
#             firstObject = {
#                 'Created At': order.created_at.strftime("%Y-%m-%d"),
#                 'Reff No': commission_reff.reff_no,
#                 'Order Number': order.order_number,
#                 'Buyer': shipping_address.ship_to,
#                 'Address': address,
#                 'State': shipping_address.state,
#                 'Product': total_products,
#                 'Buyer GST': buyergstIN,
#             }

#             merged.update(firstObject)
#             merged.update(second_obj)

#             thirdobject = {
#                 'Amount Before Tax': amount_before_tax,
#                 'Tax Amount': floatingValue(round(total_gst_amount, 2)),
#                 'Amount After Tax': floatingValue(total_amount_after_tax),
#             }

#             merged.update(thirdobject)

#             merged.update(fourth_obj)

#             lastobject = {
#                 'Tcs Amount ('+str(TCS)+'%)': floatingValue(round(total_tcs_amount, 2)),
#                 'Tds Amount ('+str(TDS)+'%)': floatingValue(round(total_tds_amount, 2)),
#                 'Status': commission_reff.txn_id,
#                 'Paid Amount': floatingValue(total_paid_amount)
#             }
#             merged.update(lastobject)

#             item_data.append(merged)

#         item_data.sort(key=len, reverse=True)

#         if(commission_reff.payment_csv is not None):

#             marks_data = pd.DataFrame(item_data)

#             statement = 'new-payments-csv' + \
#                 str(commission_reff.reff_no) + \
#                 str('-')+str(seller.id)+str('.csv')

#             #             # saving the csv
#             uploaded_file = getcwd()+"/app/static/paid_payments/"+str(statement)
#             marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

#             # Delete File From Account Transaction
#             MediaHelper.deleteFile(
#                 bucket_name='paid_payments', file_name=statement)

#             # Upload FILE
#             MediaHelper.uploadPaidPaymentsCsv(
#                 uploaded_file=uploaded_file, file_name=statement)

#             # Remove file from Path
#             unlink(uploaded_file)

#             file_path = str(
#                 linode_obj_config['endpoint_url'])+'/paid_payments/'+str(statement)

#             commission_reff.payment_csv = file_path
#             db.flush()
#             db.commit()
#         return
#     except Exception as e:
#         print(e)
# # Generate Buyer CSV of payments


# @admin_app_commission_router.get("/commission/generate/buyer-csv/{reff_no}")
# async def generateBuyerCSV(request: Request, reff_no: str, db: Session = Depends(get_db)):
#     try:
#         # Payment Reff
#         payment_reff = db.query(SellerPaymentModel).filter(
#             SellerPaymentModel.reff_no == reff_no).filter(SellerPaymentModel.txn_id.isnot(None)).first()

#         # orders items
#         orderItems = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff == payment_reff.reff_no).filter(OrderItemsModel.status >= 70).filter(
#             OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).group_by(OrderItemsModel.order_id).order_by(OrderItemsModel.order_id.desc()).all()

#         items = []
#         for orderitem in orderItems:
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == orderitem.order_id).first()

#             # Buyer
#             buyer = db.query(ShippingAddressModel).filter(
#                 ShippingAddressModel.id == order.address_id).first()

#             # Seller
#             sellerproduct = db.query(ProductModel).filter(
#                 ProductModel.id == orderitem.product_id).first()
#             seller = db.query(UserModel).filter(
#                 UserModel.id == sellerproduct.userid).first()

#             # BuyerPincode
#             buyerpincode = db.query(PincodeModel).filter(
#                 PincodeModel.pincode == buyer.pincode).first()

#             # seller pincode
#             sellerpincode = db.query(PincodeModel).filter(
#                 PincodeModel.pincode == seller.pincode).first()

#             # Buyer Profile
#             profile = db.query(UserProfileModel).filter(
#                 UserProfileModel.user_id == order.user_id).first()

#             # orderdate
#             order_date = order.created_at.strftime("%Y-%m-%d")

#             # Items Data
#             items_data = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
#                 OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             # Total taxable Amount
#             total_taxable_amount = 0
#             # Total Discount Amount
#             total_discount_amount = 0
#             # Buyer Total Amount
#             buyer_total_amount = 0
#             # Total GST AMount
#             buyer_total_gst_amount = 0
#             # Total Asezservice_amount
#             buyer_total_asez_service_amount = 0

#             # Buyer Asez Service GST AMOUNT
#             buyer_total_asez_service_gst_amount = 0

#             # Aseztak Service
#             buyer_aseztak_service = Services.aseztak_services(
#                 order.created_at, db=db)

#             if(order.app_version == 'V4'):
#                 for item_data in items_data:
#                     # Aseztak Service
#                     aseztak_service = Services.aseztak_services(
#                         item_data.created_at, db=db)

#                     # Taxable Amount
#                     taxable_amount = calculateTaxableAmountItemWise(app_version=order.app_version, price=item_data.price, tax=item_data.tax, quantity=item_data.quantity, commission=item_data.commission_buyer,
#                                                                     gst_on_commission=item_data.commission_buyer_tax, tds_rate=item_data.tds_rate, tcs_rate=item_data.tcs_rate, round_of=aseztak_service.round_off)

#                     # Total Taxable Amount
#                     total_taxable_amount += taxable_amount

#                     # Discount on Product Value
#                     discount = 0
#                     if(order.discount != 0):
#                         discount = calculateDiscountAmountItemWise(
#                             app_version=order.app_version, total_amount=item_data.discount_amount, discount_rate=item_data.discount_rate)

#                     # Total Discount Amount
#                     total_discount_amount += discount

#                     # Total Amount
#                     total_amount = (taxable_amount - discount)
#                     total_amount = round(total_amount, 2)

#                     # Buyer Total Amount
#                     buyer_total_amount += total_amount

#                     # GST On Item
#                     if(item_data.igst_on_tax == 0):
#                         gstonitems = (item_data.cgst_on_tax +
#                                       item_data.sgst_on_tax)
#                     else:
#                         gstonitems = (item_data.igst_on_tax)

#                     gst_on_items = calculateGstOnItems(
#                         app_version=order.app_version, total_amount=gstonitems, tax=item_data.tax)

#                     # Total GST Amount
#                     buyer_total_gst_amount += gst_on_items

#                     # Total Asezservice Amount
#                     buyer_total_asez_service_amount += item_data.asez_service_amount

#                     if(item_data.asez_service_on_igst == 0):
#                         gstonasezservice = (
#                             item_data.asez_service_on_cgst + item_data.asez_service_on_sgst)
#                     else:
#                         gstonasezservice = (item_data.asez_service_on_igst)

#                     buyer_total_asez_service_gst_amount += gstonasezservice

#             else:
#                 for olditem_data in items_data:

#                     # Aseztak Service
#                     aseztak_service = Services.aseztak_services(
#                         olditem_data.created_at, db=db)

#                     if(aseztak_service is not None):

#                         # Taxable Amount
#                         taxable_amount = calculateTaxableAmountItemWise(app_version=order.app_version, price=olditem_data.price, tax=olditem_data.tax, quantity=olditem_data.quantity, commission=aseztak_service.rate,
#                                                                         gst_on_commission=aseztak_service.gst_on_rate, tds_rate=aseztak_service.tds_rate, tcs_rate=aseztak_service.tcs_rate, round_of=aseztak_service.round_off)

#                         # Total Taxable Amount
#                         total_taxable_amount += taxable_amount

#                         # Discount on Product Value
#                         discount = 0
#                         if(order.discount != 0):
#                             discount = db.query(OrderDiscountModel).filter(
#                                 OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                             discount = calculateDiscountAmountItemWise(
#                                 app_version=order.app_version, total_amount=taxable_amount, discount_rate=discount.discount)

#                         # Total Discount Amount
#                         total_discount_amount += discount

#                         # Total Amount
#                         total_amount = (taxable_amount - discount)
#                         total_amount = round(total_amount, 2)

#                         # Buyer Total Amount
#                         buyer_total_amount += total_amount

#                         # GST On Item
#                         gst_on_items = calculateGstOnItems(
#                             app_version=order.app_version, total_amount=total_amount, tax=olditem_data.tax)

#                         # Total GST Amount
#                         buyer_total_gst_amount += gst_on_items

#                         # Asez Service
#                         asez_service_on_items = aseztakServiceOnItems(
#                             app_version=order.app_version, total_amount=total_amount, rate=aseztak_service.rate, gst_on_rate=aseztak_service.gst_on_rate, tcs_rate=aseztak_service.tcs_rate, tds_rate=aseztak_service.tds_rate)

#                         # Total Asez Amount
#                         asezServiceAmount = asez_service_on_items['asez_service']
#                         asezServiceGst = asez_service_on_items['gst_on_service']
#                         buyer_total_asez_service_amount += asezServiceAmount

#                         buyer_total_asez_service_gst_amount += asezServiceGst

#             # Grand Total
#             grand_total = (
#                 buyer_total_amount + buyer_total_gst_amount + buyer_total_asez_service_amount + buyer_total_asez_service_gst_amount)

#             shipping_charge = db.query(ShippingChargeModel).filter(
#                 ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#             # Delivery Charge
#             delivery_charge = orderDeliveryCalculation(
#                 app_version=order.app_version, order_amount=grand_total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

#             grand_total = round(grand_total, 2)

#             round_of = round(grand_total, 0) - grand_total

#             proof = ''
#             if(profile.proof_type == 'GSTIN'):
#                 proof = profile.proof

#             # asezigst = buyer_total_asez_service_gst_amount
#             if(str(buyerpincode.statename) == str(sellerpincode.statename)):
#                 asezcgst = (buyer_total_asez_service_gst_amount / 2)
#                 asezcgst = round(asezcgst, 2)
#                 asesgst = asezcgst
#                 asezigst = 0.00
#             else:
#                 asezigst = round(buyer_total_asez_service_gst_amount, 2)
#                 asezcgst = 0.00
#                 asesgst = 0.00

#             itms = {
#                 'Order Date': order_date,
#                 'Order Number': order.order_number,
#                 'Buyer Name': buyer.ship_to,
#                 'Address': str(buyer.address) + ', '+str(buyer.locality)+', '+str(buyer.city)+', '+str(buyer.city)+', '+str(buyer.state)+' - '+str(buyer.pincode),
#                 'state': str(buyer.state),
#                 'Buyer GSTIN': proof,
#                 'Product Value': total_taxable_amount,
#                 'Discount': total_discount_amount,
#                 'Total': buyer_total_amount,
#                 'GST on Products': buyer_total_gst_amount,
#                 'Aseztak Service (' + str(
#                     round(buyer_aseztak_service.rate))+'%)': buyer_total_asez_service_amount,
#                 'GST on Service (' + str(round(buyer_aseztak_service.gst_on_rate))+'%)': buyer_total_asez_service_gst_amount,
#                 'Central GST': asezcgst,
#                 'State GST': asesgst,
#                 'Intrastate GST': asezigst,
#                 'Shipping Charge': round(delivery_charge, 2),
#                 'GST on Shipping': '',
#                 'Round r/Off': round(round_of, 2),
#                 'Paid Amount': round(grand_total + delivery_charge, 0)
#             }

#             items.append(itms)

#             marks_data = pd.DataFrame(items)

#             statement = 'new-buyer-payments-csv' + \
#                 str(payment_reff.reff_no)+str('.csv')

#             #             # saving the csv
#             uploaded_file = getcwd()+"/app/static/buyer_payments/"+str(statement)
#             marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

#             # Delete File From Account Transaction
#             MediaHelper.deleteFile(
#                 bucket_name='buyer_payments', file_name=statement)

#             # Upload FILE
#             MediaHelper.uploadBuyerPaymentsCsv(
#                 uploaded_file=uploaded_file, file_name=statement)

#             # Remove file from Path
#             unlink(uploaded_file)

#             file_path = str(
#                 linode_obj_config['endpoint_url'])+'/buyer_payments/'+str(statement)

#             payment_reff.buyer_payment_csv = file_path
#             db.flush()
#             db.commit()

#     except Exception as e:
#         print(e)


# # Generate LOCKED PAYMENT CSV
# @admin_app_commission_router.get("/commission/generate/locked/csv/{reff_no}")
# async def generateLockedCSV(request: Request, reff_no: str, db: Session = Depends(get_db)):
#     try:

#         # Generate Locked Payments CSV FILE
#         generateddata: OrdersModel = await OrderHelper.getGeneratedOrderItemsList(reff_no=reff_no, db=db)
#         item_data = []

#         reff = db.query(SellerPaymentModel).filter(
#             SellerPaymentModel.reff_no == reff_no).first()

#         seller = db.query(UserModel).filter(
#             UserModel.id == reff.seller_id).first()

#         order_item_list: OrdersModel = generateddata.group_by(
#             OrderItemsModel.order_id).all()

#         for item in order_item_list:

#             # Order
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.id == item.order_id).first()

#             # Buyer
#             buyer = db.query(UserProfileModel).filter(
#                 UserProfileModel.user_id == order.user_id).first()

#             buyergstIN = ''
#             if(buyer.proof_type == 'GSTIN'):
#                 buyergstIN = buyer.proof

#             # shipping_address
#             shipping_address = db.query(ShippingAddressModel).filter(
#                 ShippingAddressModel.user_id == order.user_id).first()

#             # Address
#             address = f"{shipping_address.address}, {shipping_address.locality}, {shipping_address.city}, {shipping_address.state}, {shipping_address.country} - {shipping_address.pincode}"

#             # Items Data
#             items_data = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(
#                 OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).all()

#             # Total GST Amount
#             total_gst_amount = 0
#             # Total Amount After Tax
#             total_amount_after_tax = 0
#             # Total TCS AMOUNT
#             total_tcs_amount = 0
#             # Total TDS AMOUNT
#             total_tds_amount = 0
#             # Total Paid Amount
#             total_paid_amount = 0

#             # Products
#             total_products = ''

#             # GST Rates Amount
#             gst_rates_amount = []

#             # GST RAtes
#             gst_rates = []

#             # Service Charge Amount
#             asez_service_charge_amount = []

#             # Service Rates
#             asez_service_rates = []

#             # Order Date
#             orderdate = order.created_at.strftime("%Y-%m-%d")

#             if(order.app_version == 'V4'):

#                 for itemdata in items_data:

#                     # Product
#                     product = db.query(ProductModel).filter(
#                         ProductModel.id == itemdata.product_id).first()

#                     # Total Products
#                     total_products += str(product.title)+', '

#                     # taxable Amount
#                     taxable_amount = calculateTaxableAmount(
#                         price=itemdata.price, quantity=itemdata.quantity)

#                     # GST RATES
#                     gst_rates.append(itemdata.tax)

#                     gstrates = {
#                         'rate': itemdata.tax,
#                         'amount': taxable_amount
#                     }
#                     gst_rates_amount.append(gstrates)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=itemdata.tax)

#                     # Total GST Amount
#                     total_gst_amount += gst_amount

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     # Total Amount After Tax
#                     total_amount_after_tax += amount_after_tax

#                     # Service Charge
#                     service_charge = calculateCommissionSeller(
#                         amount=taxable_amount, commission=itemdata.commission_seller)

#                     service_charge_on_tax = calculateCommissionTaxSeller(
#                         amount=service_charge, tax=itemdata.commission_seller_tax)

#                     total_service_charge = round(
#                         service_charge + service_charge_on_tax, 2)

#                     # Total Service Charge AMount
#                     # Service RATES
#                     asez_service_rates.append(itemdata.commission_seller)

#                     aszrates = {
#                         'rate': itemdata.commission_seller,
#                         'amount': total_service_charge
#                     }
#                     asez_service_charge_amount.append(aszrates)

#                     # Tcs Amount
#                     tcs_amount = calculateSellerTcsAmount(
#                         amount=taxable_amount, tax=itemdata.tcs_rate)

#                     # Total TCS AMOUNT
#                     total_tcs_amount += tcs_amount

#                     # Tds Amount
#                     tds_amount = calculateSellerTdsAmount(
#                         amount=amount_after_tax, tax=itemdata.tds_rate)

#                     # Total Tds Amount
#                     total_tds_amount += tds_amount

#                     # Paid Amount
#                     paid_amount = (amount_after_tax -
#                                    total_service_charge - tcs_amount - tds_amount)

#                     paid_amount = round(paid_amount, 2)

#                     # Total Paid Amount
#                     total_paid_amount += paid_amount

#             else:

#                 for olditemdata in items_data:
#                     # Product
#                     product = db.query(ProductModel).filter(
#                         ProductModel.id == olditemdata.product_id).first()

#                     # Total Products
#                     total_products += str(product.title)+', '

#                     # taxable Amount
#                     taxable_amount = calculateOldTaxableAmount(
#                         price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity)

#                     # GST RATES
#                     gst_rates.append(olditemdata.tax)

#                     gstrates = {
#                         'rate': olditemdata.tax,
#                         'amount': taxable_amount
#                     }
#                     gst_rates_amount.append(gstrates)

#                     # GST AMOUNt
#                     gst_amount = calculateGstAmount(
#                         price=taxable_amount, tax=olditemdata.tax)

#                     # Total GST Amount
#                     total_gst_amount += gst_amount

#                     # amount after tax
#                     amount_after_tax = round(taxable_amount + gst_amount, 2)

#                     # Total AMount after tax
#                     total_amount_after_tax += amount_after_tax

#                     # product
#                     productdata = db.query(ProductModel).filter(
#                         ProductModel.id == olditemdata.product_id).first()
#                     categorycommission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id ==
#                                                                                   productdata.category).filter(CategoryCommissionModel.start_date <= orderdate).order_by(CategoryCommissionModel.start_date.desc()).first()

#                     totalamount = calculateWithoutTaxAmount(
#                         price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity, commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)

#                     orderTotalamoutwithouttax = totalamount['total_amount']

#                     tcs_amount = (orderTotalamoutwithouttax *
#                                   TCS) / 100
#                     tcs_amount = round(tcs_amount, 2)

#                     tds_amount = (orderTotalamoutwithouttax * TDS) / 100
#                     tds_amount = round(tds_amount, 2)

#                     if(orderdate < '2021-11-09'):
#                         pass

#                     else:
#                         category_commission = categorycommission.commission

#                         tcs_amount = tcs_amount
#                         # Total TCS Amount
#                         total_tcs_amount += tcs_amount

#                         tds_amount = tds_amount
#                         # Total Tds Amount
#                         total_tds_amount += tds_amount

#                         # Total Service Charge AMount
#                         # Service RATES
#                         asez_service_rates.append(category_commission)

#                         aszrates = {
#                             'rate': category_commission,
#                             'amount': round(
#                                 totalamount['total_commisison'], 2)
#                         }
#                         asez_service_charge_amount.append(aszrates)

#                         paid_amount = calculateTotal(price=olditemdata.price, tax=olditemdata.tax, quantity=olditemdata.quantity,
#                                                      commission=categorycommission.commission, commission_tax=categorycommission.commission_tax)
#                         paid_amount = (
#                             paid_amount['tota_amount'] - totalamount['total_commisison'] - tcs_amount - tds_amount)

#                         paid_amount = round(paid_amount, 2)

#                         # Total Paid Amount
#                         total_paid_amount += paid_amount

#             # Customize GST RATES
#             gst_rates = set(gst_rates)
#             final_gst_rates = []
#             amount_before_tax = 0
#             for g in gst_rates:
#                 gstam = 0
#                 for gm in gst_rates_amount:
#                     if(gm['rate'] == g):
#                         gstam += gm['amount']
#                         amount_before_tax += gm['amount']
#                 gs = {
#                     'GST Rate '+str(int(g))+'%': gstam
#                 }

#                 final_gst_rates.append(gs)

#             second_obj = {}
#             for obj in final_gst_rates:
#                 second_obj.update(obj)

#             # Customize Service Rates
#             asez_service_rates = set(asez_service_rates)
#             final_asez_rates = []
#             for s in asez_service_rates:
#                 aszam = 0
#                 for am in asez_service_charge_amount:
#                     if(am['rate'] == s):
#                         aszam += am['amount']

#                 asm = {
#                     'Service Charge': str(round(aszam, 2)) + str(' (')+str(s)+str('% + Tax)')
#                 }
#                 final_asez_rates.append(asm)

#             fourth_obj = {}
#             for obj4 in final_asez_rates:
#                 fourth_obj.update(obj4)

#             merged = dict()
#             firstObject = {
#                 'Created At': order.created_at.strftime("%Y-%m-%d"),
#                 'Reff No': reff_no,
#                 'Order Number': order.order_number,
#                 'Buyer': shipping_address.ship_to,
#                 'Address': address,
#                 'State': shipping_address.state,
#                 'Product': total_products,
#                 'Buyer GST': buyergstIN,
#             }

#             merged.update(firstObject)
#             merged.update(second_obj)

#             thirdobject = {
#                 'Amount Before Tax': amount_before_tax,
#                 'Tax Amount': floatingValue(round(total_gst_amount, 2)),
#                 'Amount After Tax': floatingValue(total_amount_after_tax),
#             }

#             merged.update(thirdobject)

#             merged.update(fourth_obj)

#             lastobject = {
#                 'Tcs Amount ('+str(TCS)+'%)': floatingValue(round(total_tcs_amount, 2)),
#                 'Tds Amount ('+str(TDS)+'%)': floatingValue(round(total_tds_amount, 2)),
#                 'Status': 'Pending',
#                 'Paid Amount': floatingValue(total_paid_amount)
#             }
#             merged.update(lastobject)

#             item_data.append(merged)

#         item_data.sort(key=len, reverse=True)

#         marks_data = pd.DataFrame(item_data)

#         statement = 'locked-payments-csv' + \
#             str(reff.reff_no)+str('-')+str(seller.id)+str('.csv')

#         # saving the csv
#         uploaded_file = getcwd()+"/app/static/paid_payments/"+str(statement)
#         marks_data.to_csv(uploaded_file, encoding='utf-8', index=False)

#         # Delete File From Account Transaction
#         MediaHelper.deleteFile(
#             bucket_name='paid_payments', file_name=statement)

#         # Upload FILE
#         MediaHelper.uploadPaidPaymentsCsv(
#             uploaded_file=uploaded_file, file_name=statement)

#         # Remove file from Path
#         unlink(uploaded_file)

#         file_path = str(
#             linode_obj_config['endpoint_url'])+'/paid_payments/'+str(statement)

#         reff.payment_csv = file_path
#         db.flush()
#         db.commit()

#         return "DONE"
#     except Exception as e:
#         print(e)
