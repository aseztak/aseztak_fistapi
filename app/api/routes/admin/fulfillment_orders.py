from app.api.helpers.admin_app_products import *
from fastapi import APIRouter

admin_app_fulfillment_orders_router = APIRouter()


# @admin_app_fulfillment_orders_router.get("/", dependencies=[Depends(JWTBearer())])
# async def AdminFulfillment(request: Request, page: int = 1, limit: int = 15, db: Session = Depends(get_db)):
#     # Check Not Admin
#     checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#     if(checkNotadmin == False):
#         return {"status_code": HTTP_304_NOT_MODIFIED}

#     order = db.query(FulFillmentOrdersModel).order_by(
#         FulFillmentOrdersModel.id.desc())

#     if(order.count() == 0):
#         return {"status_code": HTTP_200_OK, "total_orders": 0, "orders": [], "current_page": page, "total_page": 0}

#     orderdata: FulFillmentOrdersModel = order.limit(
#         limit=limit).offset((page - 1) * limit).all()

#     total_records = order.count()

#     # Number of Total Pages
#     total_pages = str(round((total_records/limit), 2))

#     total_pages = total_pages.split('.')

#     if(total_pages[1] != 0):
#         total_pages = int(total_pages[0]) + 1

#     orderlist = []
#     for orders in orderdata:
#         user = db.query(UserModel).filter(
#             UserModel.id == orders.seller_id).first()
#         items = db.query(FulFillmentOrdersItemsModel).filter(
#             FulFillmentOrdersItemsModel.order_id == orders.id).all()

#         total_amount = 0
#         for item in items:
#             price = item.price * item.quantity
#             total_amount += round(price)
#         orders = {
#             'id': orders.id,
#             'order_number': orders.order_number,
#             'placed_by': user.name,
#             'mobile': user.mobile,
#             'total_amount': total_amount,
#             'items': len(items),
#             'created_at': orders.created_at.strftime("%B %m %Y")
#         }
#         orderlist.append(orders)
#     return {"status_code": HTTP_200_OK, "orders": orderlist, "total_orders": total_records, "current_page": page, "total_page": total_pages}


# @admin_app_fulfillment_orders_router.get("/order_details/{order_id}", dependencies=[Depends(JWTBearer())])
# async def AdminFulfillmentOrders(request: Request, order_id: int, db: Session = Depends(get_db)):

#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         order = db.query(FulFillmentOrdersModel).filter(
#             FulFillmentOrdersModel.id == order_id).first()

#         seller = db.query(UserModel).filter(
#             UserModel.id == order.seller_id).first()

#         sellers = {
#             'name': seller.name,
#             'mobile': seller.mobile,
#             'email': seller.email.lower()

#         }

#         seller_account = db.query(UserProfileModel).filter(
#             UserProfileModel.user_id == seller.id).first()

#         profile = {
#             'account_number': seller_account.acc_no,
#             'ifsc_code': seller_account.ifsc_code,
#             'bank': seller_account.bank,
#             'proof_type': seller_account.proof_type
#         }

#         items = db.query(FulFillmentOrdersItemsModel).filter(
#             FulFillmentOrdersItemsModel.order_id == order.id).all()
#         itemlist = []
#         total_amount = 0
#         for item in items:
#             price = item.price * item.quantity
#             total_amount += round(price)

#             product = db.query(FulFillmentItemsModel).filter(
#                 FulFillmentItemsModel.id == item.item_id).first()

#             products = {
#                 'id': product.id,
#                 'title': product.title,
#                 'description': product.description,
#                 'image':  str(request.base_url)+str(product.image)
#             }

#             itm = {
#                 'id': item.id,
#                 'product': products,
#                 'price': item.price,
#                 'total_amount': round(total_amount),
#                 'quantity': item.quantity,
#             }
#             itemlist.append(itm)

#             # return itemlist
#         order = {
#             'id': order.id,
#             'seller': sellers,
#             'order_number': order.order_number,
#             'items': itemlist,
#             'total_items': len(items),
#             'invoice_date': order.invoice_date,
#             'invoice': order.invoice,
#             'created_at': order.created_at.strftime("%B %d %Y")
#         }

#         return {"status_code": HTTP_200_OK, "seller": sellers, "order_details": order, "items": itemlist}

#     except Exception as e:
#         print(e)
