import calendar
from datetime import datetime as DT
from typing import Optional
from starlette.requests import Request
from app.api.helpers.orderstaticstatus import OrderStatus
from app.api.util.admin_app_calculation import currentDate
from app.api.util.check_user import CheckUser
from app.db.models.media import ProductMediaModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.order_status import OrderStatusModel
from app.db.models.orders import OrdersModel
from app.db.models.products import ProductModel
from app.db.models.user import UserModel, UserRoleModel
from starlette.status import HTTP_304_NOT_MODIFIED
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from sqlalchemy.sql.functions import func
from fastapi import APIRouter,  Depends
from starlette.requests import Request
from app.api.util.calculation import aseztakServiceOnItems, calculateCommissionSeller, calculateCommissionTaxSeller, calculateDiscountAmountItemWise, calculateGstAmount, calculateGstOnItems, calculateOldTaxableAmount, calculateSellerTcsAmount, calculateSellerTdsAmount, calculateTaxableAmount, calculateTaxableAmountItemWise, calculateTotal, calculateTotalWithTax, calculateWithoutTaxAmount, floatingValue, orderDeliveryCalculation, productPricecalculation, roundOf

from app.db.models.acconts import AccountsModel


new_admin_app_dashboard_router = APIRouter()


@new_admin_app_dashboard_router.get("/orders/{today}", dependencies=[Depends(JWTBearer())])
async def dashboard(request: Request, today: Optional[str] = '', db: Session = Depends(get_db)):
    try:

        # Check Not Admin
        checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

        if(checkNotadmin == False):
            return {"status_code": HTTP_304_NOT_MODIFIED}
        # DateWise Orders
        # orders = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
        #     OrdersModel.created_at, '%Y-%m-%d') == currentDate(today)).filter(OrderStatusModel.status != 980).group_by(OrderStatusModel.order_id).all()

        orders = db.execute(
            "SELECT SUM(orders.grand_total) as grandtotal, COUNT(orders.id) as total_orders FROM orders LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE date_format(orders.created_at, '%Y-%m-%d') =:date_ AND orders.id IN(SELECT order_items.order_id from order_items WHERE order_items.status != 980 AND order_fails.order_id is NULL GROUP BY order_items.order_id)", {
                "date_": currentDate(today)
            }).first()
        cancelledorders = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
            func.date_format(OrdersModel.created_at, '%Y-%m-%d') == currentDate(today)).having(func.max(OrderStatusModel.status) == 980).group_by(OrderStatusModel.order_id).count()

        # total_amount = 0
        # for order in orders:
        #     total_amount += order.grand_total

        currentMonth = DT.strptime(
            today, "%Y-%m-%d %H:%M:%S.%f").strftime('%Y-%m')

        todaydt = DT.now().strftime("%Y-%m")
        if(currentMonth == todaydt):
            monthdt = todaydt
        else:
            monthdt = currentMonth
        m = DT.strptime(monthdt, "%Y-%m").strftime('%m')
        y = DT.strptime(monthdt, "%Y-%m").strftime('%y')
        no_of_days = calendar.monthrange(int(y), int(m))[1]

        monthlyorders = db.execute("SELECT SUM(orders.grand_total) as grandtotal FROM orders LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE order_fails.order_id is NULL AND date_format(orders.created_at, '%Y-%m') =:date AND orders.id IN(SELECT order_items.order_id from order_items WHERE order_items.status != 980 AND order_items.status != 61 AND order_items.status != 80 AND order_items.status != 90 AND order_items.status != 91 AND order_items.status != 100 AND order_items.status != 110 GROUP BY order_items.order_id)", {
            "date": monthdt
        }).first()
        if(monthlyorders is None):
            month_average = 0
        else:
            average = float(monthlyorders.grandtotal)/float(no_of_days)
            month_average = round(average, 2)
        totalamount = 0
        if(orders.grandtotal is not None):
            totalamount = round(orders.grandtotal, 2)
        totalorders = 0
        if(orders.total_orders is not None):
            totalorders = orders.total_orders
        ordersdata = {
            'orders': totalorders,
            'orders_cancelled': int(cancelledorders),
            'total': totalamount,
            'grand_total': round(monthlyorders.grandtotal, 2),
            'month_avarage': month_average

        }
        return ordersdata
    except Exception as e:
        print(e)


@new_admin_app_dashboard_router.get("/statistics/{today}", dependencies=[Depends(JWTBearer())])
async def statistics(request: Request, today: Optional[str] = '', db: Session = Depends(get_db)):
    try:
        # All Buyer List
        buyers_order = db.execute("SELECT COUNT(*) as total_count FROM (SELECT orders.id FROM orders LEFT JOIN order_fails ON order_fails.order_id = orders.id LEFT JOIN order_items ON order_items.order_id = orders.id WHERE date_format(orders.created_at, '%Y-%m-%d') =:date AND order_fails.order_id is NULL AND order_items.status != 980 GROUP BY orders.user_id ) as counts", {
            "date": currentDate(today)
        }).first()
        if(buyers_order is None):
            buyers_order = 0
        else:
            buyers_order = buyers_order.total_count
        # All Seller List
        # sellers_order = db.query(UserModel).join(ProductModel, ProductModel.userid == UserModel.id).join(OrderItemsModel, OrderItemsModel.product_id == ProductModel.id).filter(
        #     func.date_format(OrderItemsModel.created_at, '%Y-%m-%d') == currentDate(today)).group_by(UserModel.id).count()
        sellers_order = db.execute("SELECT COUNT(*) as total_count FROM (SELECT products.userid FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN products on products.id = order_items.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE date_format(order_items.created_at, '%Y-%m-%d') =:date AND order_items.status != 980 AND order_fails.order_id is NULL GROUP BY products.userid) as counts", {
            "date": currentDate(today)
        }).first()
        if(sellers_order is None):
            sellers_order = 0
        else:
            sellers_order = sellers_order.total_count
        # Today Buyers
        today_buyers = db.query(UserModel).filter(func.date_format(
            UserModel.created_at,  "%Y-%m-%d") == currentDate(today)).count()
        registered_buyer = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).filter(UserModel.status == 1).filter(func.date_format(
            UserModel.created_at,  "%Y-%m-%d") == currentDate(today)).count()

        # Today Seller
        today_sellers = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 6).filter(func.date_format(
            UserModel.created_at,  "%Y-%m-%d") == currentDate(today)).count()

        products = db.query(ProductModel).filter(func.date_format(
            ProductModel.created_at,  "%Y-%m-%d") == currentDate(today)).filter(ProductModel.status != 98).count()
        approved_products = db.query(ProductModel).filter(ProductModel.status == 51).filter(func.date_format(
            ProductModel.created_at,  "%Y-%m-%d") == currentDate(today)).count()
        pending_products = db.query(ProductModel).filter(ProductModel.status == 1).filter(func.date_format(
            ProductModel.created_at,  "%Y-%m-%d") == currentDate(today)).count()
        rejected_products = db.query(ProductModel).filter(ProductModel.status == 99).filter(func.date_format(
            ProductModel.created_at,  "%Y-%m-%d") == currentDate(today)).count()
        market = {
            'buyer': buyers_order,
            'seller': sellers_order
        }
        data = {
            'market': market,
            'users': {
                'downloaded': today_buyers,
                'registered_buyer': registered_buyer,
                'seller': today_sellers
            },
            'products': {
                'new': products,
                'approved': approved_products,
                'pending': pending_products,
                'rejected': rejected_products
            }
        }
        return data
    except Exception as e:
        print(e)

# Seller List by orders


@new_admin_app_dashboard_router.get("/seller/order/list/{today}", dependencies=[Depends(JWTBearer())])
async def sellerListByOrder(request: Request, today: Optional[str] = '', db: Session = Depends(get_db)):
    try:
        # sellers = db.query(UserModel).join(ProductModel, ProductModel.userid == UserModel.id).join(OrderItemsModel, OrderItemsModel.product_id == ProductModel.id).join(
        #     OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(func.date_format(OrderItemsModel.created_at, '%Y-%m-%d') == currentDate(today)).filter(OrderItemsModel.status != 980).group_by(UserModel.id).all()

        sellers = db.execute("SELECT products.userid, SUM(DISTINCT(orders.grand_total)) as total_amount FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN products on products.id = order_items.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE date_format(order_items.created_at, '%Y-%m-%d') =:date AND order_items.status != 980 AND order_fails.order_id is NULL GROUP BY products.userid ORDER BY total_amount DESC", {
            "date": currentDate(today)
        }).all()
        sellerdata = []
        for order in sellers:
            user = db.query(UserModel).filter(
                UserModel.id == order.userid).first()

            orders = db.execute("SELECT SUM(DISTINCT(orders.grand_total)) as total_amount, COUNT(distinct(orders.id)) AS total_count FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN products ON products.id = order_items.product_id LEFT JOIN order_fails ON  order_fails.order_id = orders.id LEFT JOIN users ON users.id = products.userid WHERE order_fails.order_id is NULL AND DATE_FORMAT(orders.created_at, '%Y-%m-%d')  =:date AND products.userid =:id AND order_items.status != 980 GROUP BY users.id order by users.id desc", {
                "date": currentDate(today),
                "id": user.id
            }).first()
            total_amount = 0
            total_count = 0
            if(orders):
                total_amount = orders.total_amount
                total_count = orders.total_count

            allorders = db.execute("SELECT COUNT(*) as total_count FROM (SELECT orders.id FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id  LEFT JOIN products ON products.id = order_items.product_id LEFT JOIN order_status ON order_status.order_id = orders.id WHERE products.userid =:user_id AND order_items.status >= 70 AND order_items.status != 80 AND order_items.status != 90 AND order_items.status != 100 AND order_items.status != 110 AND order_items.status != 980 GROUP BY orders.id) as counts", {
                "user_id": user.id
            }).first()
            if(allorders is None):
                allorders = 0
            else:
                allorders = allorders.total_count

            returnedorders = db.execute("SELECT COUNT(*) as total_count FROM (SELECT orders.id FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN products ON products.id = order_items.product_id LEFT JOIN users ON users.id = products.userid  LEFT JOIN order_status ON order_status.order_id = orders.id WHERE products.userid =:id AND order_status.status >= 90 AND order_status.status != 91 AND order_status.status != 980 GROUP BY orders.id) as counts", {
                "id": user.id
            }).first()
            if(returnedorders is None):
                returnedorders = 0
            else:
                returnedorders = returnedorders.total_count

            if(returnedorders > 0 and allorders > 0):
                returnedorder_percentage = (
                    returnedorders / allorders) * 100
                delivered_percentage = 100 - returnedorder_percentage
            elif(returnedorders == 0 and allorders != 0):
                returnedorder_percentage = (returnedorders / allorders) * 100
                delivered_percentage = 100 - returnedorder_percentage    
            else:
                returnedorder_percentage = 0
                delivered_percentage = 0    
            data = {
                'id': user.id,
                'name': user.name,
                'location': str(user.city)+', '+str(user.region),
                'total': total_amount,
                'total_count': total_count,
                'statistic': {
                    'delivered_orders': allorders,
                    'returned_orders': returnedorders,
                    'delivered_order_percentage': round(delivered_percentage),
                    'returned_order_percentage': round(returnedorder_percentage)
                }
            }
            sellerdata.append(data)

        return sellerdata
    except Exception as e:
        print(e)

# TODAYS Orders


@new_admin_app_dashboard_router.get("/todays/order/{today}", dependencies=[Depends(JWTBearer())])
async def todaysOrder(request: Request, today: Optional[str] = '', db: Session = Depends(get_db)):
    try:
        today_orders = db.execute("SELECT orders.id, orders.order_number, orders.payment_method, orders.created_at, orders.item_count, orders.grand_total,  (SELECT OS.status FROM order_status as OS WHERE OS.order_id = orders.id ORDER BY OS.id DESC LIMIT 1) AS order_status, (SELECT users.name FROM users WHERE users.id = orders.user_id) as buyer, (SELECT users.region FROM users WHERE users.id = orders.user_id) as buyer_region, (SELECT users.city FROM users WHERE users.id = orders.user_id) as buyer_city, (SELECT u.name FROM order_items LEFT JOIN products as P ON P.id = order_items.product_id LEFT JOIN users as u ON u.id = P.userid WHERE order_items.order_id = orders.id LIMIT 1) as seller_name, (SELECT u.city FROM order_items LEFT JOIN products as P ON P.id = order_items.product_id LEFT JOIN users as u ON u.id = P.userid WHERE order_items.order_id = orders.id LIMIT 1) as seller_city, (SELECT u.region FROM order_items LEFT JOIN products as P ON P.id = order_items.product_id LEFT JOIN users as u ON u.id = P.userid WHERE order_items.order_id = orders.id LIMIT 1) as seller_region FROM orders LEFT JOIN order_fails ON order_fails.order_id = orders.id LEFT JOIN order_status ON order_status.order_id = orders.id WHERE order_fails.order_id is NULL AND DATE_FORMAT(orders.created_at, '%Y-%m-%d')  =:today GROUP BY orders.id HAVING MAX(order_status.status) != 980 ORDER BY orders.id DESC LIMIT 10", {
            "today": currentDate(today)
        }).all()
        ord_data = []
        for order in today_orders:
            order_items = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == order.id).first()
            if(order_items.images != None):
                image = db.query(ProductMediaModel).filter(
                    ProductMediaModel.id == order_items.images.strip("[]")).first()
            # Latest Status
            current_status = db.query(OrderStatusModel).filter(
                OrderStatusModel.order_id == order.id).order_by(OrderStatusModel.id.desc()).first()

            statuslist = OrderStatus.AllstatusList()
            statustitle = ''
            for s_title in statuslist:
                if(current_status.status == s_title['status_id']):
                    statustitle = s_title['status_title']
            data = {
                'id': order.id,
                'order_number': order.order_number,
                'order_date': order.created_at.strftime('%Y-%m-%d  %H:%M:%S'),
                'image': image.file_path,
                'total': order.grand_total,
                'items_count': order.item_count,
                'payment_method': order.payment_method,
                'status': statustitle,
                'seller': {
                    'name': order.seller_name,
                    'location': str(order.seller_city)+', '+str(order.seller_region),
                },
                'buyer': {
                    'name': order.buyer,
                    'location': str(order.buyer_city)+', '+str(order.buyer_region),
                }
            }
            ord_data.append(data)
        return ord_data
    except Exception as e:
        print(e)

# HOME PAGE BANNER


@new_admin_app_dashboard_router.get("/banner", dependencies=[Depends(JWTBearer())])
async def HomePageBanner(request: Request, db: Session = Depends(get_db)):
    try:
        banners = db.execute(
            "SELECT banners.id, banners.banner, banners.link, banners.link_id, banners.type FROM banners WHERE banners.type = 'INNER' AND banners.status = 1 ORDER BY banners.id DESC").all()
        bannerdata = []
        for banner in banners:
            data = {
                "image": banner.banner
            }
            bannerdata.append(data)
        return bannerdata
    except Exception as e:
        print(e)

# Outstanding Payment to Seller


@new_admin_app_dashboard_router.get("/seller/payment/{date}", dependencies=[Depends(JWTBearer())])
async def paymentToSeller(request: Request, date: Optional[str] = '', db: Session = Depends(get_db)):
    try:
        sellers = db.execute("SELECT orders.id,orders.order_number, order_items.price, order_items.quantity, order_items.product_discount,(((((((order_items.price * order_items.tax) / 100) + order_items.price) * order_items.quantity))) - (((((order_items.taxable_total_amount * order_items.commission_seller) / 100) + (((order_items.taxable_total_amount * order_items.commission_seller) / 100)* order_items.commission_seller_tax) / 100))) - ((((order_items.taxable_total_amount * order_items.tcs_rate) / 100))) - ((((((((order_items.price * order_items.tax) / 100) + order_items.price) * order_items.quantity) * order_items.tds_rate) / 100) )) ) AS total_amount FROM order_items INNER JOIN orders ON orders.id = order_items.order_id INNER JOIN products ON products.id = order_items.product_id INNER JOIN order_status ON order_status.order_id = order_items.order_id WHERE orders.commission_reff IS NULL AND order_items.status >= 70 AND order_items.status != 80 AND order_items.status != 90 AND order_items.status != 100  AND order_items.status != 110 AND order_items.status != 980 AND order_status.status >= 70 AND CASE WHEN  products.userid = 65 THEN DATE_FORMAT(orders.created_at, '%Y-%m-%d') >= '2022-01-01' ELSE ( ADDDATE(date_format(order_status.created_at, '%Y-%m-%d'), CASE WHEN (order_items.return_expiry != 0) THEN order_items.return_expiry - 1 WHEN (order_items.return_expiry = 0) THEN order_items.return_expiry + 1   END) <:today_param) END GROUP BY order_items.order_id", {
            'today_param': currentDate(date)
        }).all()
        deduction_total_amount = 0.00
        for seller in sellers:
            checkDeductionAmount = db.query(AccountsModel).filter(
                AccountsModel.txn_description == str('DEDUCTION (')+seller.order_number+str(')')).first()
            if(checkDeductionAmount is not None):
                deduction_total_amount += (checkDeductionAmount.txn_amount)
        total_amount = db.execute('''
        SELECT products.userid, order_items.id as itemid, orders.id, order_items.price, order_items.quantity, order_items.product_discount, order_items.tax, order_items.commission_seller, order_items.commission_seller_tax, order_items.tcs_rate, order_items.tds_rate
        FROM order_items INNER JOIN orders ON orders.id = order_items.order_id
        INNER JOIN products ON products.id = order_items.product_id
        INNER JOIN order_status ON order_status.order_id = order_items.order_id WHERE orders.commission_reff IS NULL AND order_items.status >= 70 AND order_items.status != 80 AND order_items.status != 90 AND order_items.status != 100  AND order_items.status != 110 AND order_items.status != 980 AND order_status.status >= 70
        AND CASE WHEN  products.userid = 65 THEN DATE_FORMAT(orders.created_at, "%Y-%m-%d") >= '2022-01-01' AND (ADDDATE(date_format(order_status.created_at, "%Y-%m-%d"), CASE WHEN order_items.return_expiry != 0 THEN order_items.return_expiry - 1  WHEN order_items.return_expiry = 0 THEN order_items.return_expiry + 2 END) <:today_param) WHEN  products.userid != 65 THEN (ADDDATE(date_format(order_status.created_at, "%Y-%m-%d"), CASE WHEN order_items.return_expiry != 0 THEN order_items.return_expiry - 1  WHEN order_items.return_expiry = 0 THEN order_items.return_expiry + 2 END) <:today_param) END GROUP BY order_items.id''', {
            'today_param': currentDate(date)
        }).all()
        total_order = []
        sum_total_amount = 0
        for tamount in total_amount:
            # Order
            order = db.execute("SELECT orders.id, orders.app_version, orders.order_number,  (SELECT orders.id FROM orders AS o INNER JOIN order_items ON order_items.order_id = o.id WHERE order_items.order_id = orders.id AND order_items.status BETWEEN 80 AND 90 AND order_items.status != 81 LIMIT 1) AS checklostItems FROM orders WHERE orders.id =:order_id", {
                "order_id": tamount.id
            }).first()
            if(order.checklostItems is None):
                total_order.append(tamount)

        for item in total_order:
            # taxable Amount
            taxable_amount = calculateTaxableAmount(
                price=item.price, quantity=item.quantity, discount=item.product_discount)
            # GST AMOUNt
            gst_amount = calculateGstAmount(
                price=float(taxable_amount), tax=float(item.tax))
            # amount after tax
            amount_after_tax = round(float(taxable_amount) + float(gst_amount), 2)
            # Service Charge
            service_charge = calculateCommissionSeller(
                amount=float(taxable_amount), commission=float(item.commission_seller))

            service_charge_on_tax = calculateCommissionTaxSeller(
                amount=float(service_charge), tax=float(item.commission_seller_tax))
            total_service_charge = round(
                float(service_charge) + float(service_charge_on_tax), 2)

            # Tcs Amount
            tcs_amount = calculateSellerTcsAmount(
                amount=float(taxable_amount), tax=float(item.tcs_rate))

            # Tds Amount
            tds_amount = calculateSellerTdsAmount(
                amount=float(amount_after_tax), tax=float(item.tds_rate))

            # Paid Amount
            paid_amount = (float(amount_after_tax) -
                           float(total_service_charge) - float(tcs_amount) - float(tds_amount))
            sum_total_amount += round(paid_amount, 2)
        data = {'total': 0}
        if(sum_total_amount is not None):
            data = {
                'total': round(float(sum_total_amount) - deduction_total_amount, 2)
            }
        return data
    except Exception as e:
        print(e)


# Commission Amount
@new_admin_app_dashboard_router.get("/commission", dependencies=[Depends(JWTBearer())])
async def paymentToSeller(request: Request, db: Session = Depends(get_db)):
    try:
        commission = db.execute("SELECT Sum(CTE.grand_total) as total_amount FROM(SELECT DISTINCT orders.grand_total, orders.id FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN order_status ON order_status.order_id = orders.id LEFT JOIN order_fails ON order_fails.order_id = orders.id LEFT JOIN products ON products.id = order_items.product_id WHERE (CASE WHEN products.userid = 65 THEN date_format(orders.created_at, '%Y-%m-%d') >= '2022-01-01' ELSE orders.created_at is not NULL END) AND order_status.status BETWEEN 70 AND 81 AND order_status.status != 90 AND order_status.status != 100 AND order_status.status != 110 AND order_status.status != 980 AND order_fails.order_id is NULL AND orders.commission_reff is not NULL GROUP BY orders.id ) CTE").first()
        commission_amount = db.execute("SELECT SUM(seller_payment.commission_amount) as total_commission FROM seller_payment").first()
        data = {
            'total_commission': floatingValue(commission_amount.total_commission),
            'total_amount' : floatingValue(commission.total_amount)
        }
        return data
    except Exception as e:
        print(e)