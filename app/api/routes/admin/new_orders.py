from datetime import date, timedelta
import datetime
import json
from typing import Optional
from fastapi import APIRouter, Depends, Request
from requests import Session
from app.api.helpers.orderstaticstatus import OrderStatus
from app.api.helpers.products import ProductsHelper
from app.api.helpers.services import AsezServices
from app.api.util.admin_app_calculation import currentDate
from app.api.util.calculation import floatingValue, orderDeliveryCalculation, productPricecalculation
from app.api.util.check_user import CheckUser
from app.db.config import get_db
from app.db.models.admin import AdminModel
from app.db.models.media import ProductMediaModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.order_status import OrderStatusModel
from app.db.models.orderdiscount import OrderDiscountModel
from app.db.models.orders import OrdersModel
from app.db.models.pickup_address import PickupLocationModel
from app.db.models.products import ProductModel, ProductPricingModel
from starlette.status import HTTP_200_OK, HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED

from sqlalchemy.sql.functions import func
from app.db.models.shipping import OrderShippingModel
from app.db.models.shipping_address import ShippingAddressModel
from app.db.models.shippingcharge import ShippingChargeModel
from app.db.models.user import UserModel
from app.services.auth import auth
from app.services.auth_bearer import JWTBearer
from decimal import *


new_admin_app_order_router = APIRouter()


@new_admin_app_order_router.get("/seller/order/{seller_id}/{from_date}/{to_date}", dependencies=[Depends(JWTBearer())])
async def sellerWiseOrder(request: Request, seller_id: int, from_date: Optional[str] = '', to_date: Optional[str] = '', db: Session = Depends(get_db)):
    try:
        # Check Not Admin
        checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

        if(checkNotadmin == False):
            return {"status_code": HTTP_304_NOT_MODIFIED}
        # orders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
        #     ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(func.date_format(OrderItemsModel.created_at, '%Y-%m-%d') >= currentDate(from_date)).filter(func.date_format(OrderItemsModel.created_at, '%Y-%m-%d') <= currentDate(to_date)).filter(ProductModel.userid == seller_id).group_by(OrdersModel.id).order_by(OrdersModel.id.desc()).all()

        orders = db.execute("SELECT orders.id FROM orders LEFT JOIN order_items ON order_items.order_id = orders.id LEFT JOIN products on products.id = order_items.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE date_format(order_items.created_at, '%Y-%m-%d') >=:from_date AND date_format(order_items.created_at, '%Y-%m-%d') <=:to_date AND products.userid =:seller_id AND order_fails.order_id is NULL GROUP BY orders.id ORDER BY orders.id DESC", {
            "from_date": currentDate(from_date),
            "to_date": currentDate(to_date),
            "seller_id": seller_id
        }).all()
        orderdata = []
        for order in orders:
            order = db.query(OrdersModel).filter(
                OrdersModel.id == order.id).first()
            order_items = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == order.id).first()
            if(order_items.images != None):
                image = db.query(ProductMediaModel).filter(
                    ProductMediaModel.id == order_items.images.strip("[]")).first()
            # Latest Status
            current_status = db.query(OrderStatusModel).filter(
                OrderStatusModel.order_id == order.id).order_by(OrderStatusModel.id.desc()).first()

            statuslist = OrderStatus.AllstatusList()
            statustitle = ''
            for s_title in statuslist:
                if(current_status.status == s_title['status_id']):
                    statustitle = s_title['status_title']

            buyer = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.id == order.address_id).first()
            # Seller
            seller = db.query(OrderItemsModel, ProductModel).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(
                OrderItemsModel.order_id == order.id).first()

            if(seller is not None):

                seller = db.query(UserModel).filter(
                    UserModel.id == seller.ProductModel.userid).first()
                seller_name = seller.name
            else:
                seller_name = ''

            data = {
                'id': order.id,
                'order_number': order.order_number,
                'order_date': order.created_at.strftime('%Y-%m-%d %H:%M:%S'),
                'image': image.file_path,
                'total': order.grand_total,
                'items_count': order.item_count,
                'payment_method': order.payment_method,
                'status': statustitle,
                'seller': {
                    'name': seller_name,
                    'location': str(seller.city)+', '+str(seller.region),
                },
                'buyer': {
                    'name': buyer.ship_to,
                    'location': str(buyer.city)+', '+str(buyer.state),
                }
            }
            orderdata.append(data)
        return orderdata
    except Exception as e:
        print(e)

# Order Detail


@new_admin_app_order_router.get("/order_details/{order_id}", dependencies=[Depends(JWTBearer())])
async def Orderdetails(request: Request, order_id: int, db: Session = Depends(get_db)):
    try:
        # Check Not Admin
        checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

        if(checkNotadmin == False):
            return {"status_code": HTTP_304_NOT_MODIFIED}

        orderdata = db.query(OrdersModel, ShippingAddressModel).join(ShippingAddressModel,
                                                                     ShippingAddressModel.id == OrdersModel.address_id).where(OrdersModel.id == order_id).first()

        # Order
        order = orderdata.OrdersModel
        # Buyer
        # Shipping Address
        address = orderdata.ShippingAddressModel

        if(order.meta_data is not None):
            meta_data = json.loads(order.meta_data)

            address = {
                'ship_to': meta_data['address']['ship_to'],
                'address': str(meta_data['address']['address']),
                'city': meta_data['address']['city'],
                'pincode': meta_data['address']['pincode'],
                'state': meta_data['address']['state'],
                'locality': meta_data['address']['locality'],
            }
        else:
            address = {
                'ship_to': address.ship_to,
                'address': address.address,
                'city': address.city,
                'pincode': address.pincode,
                'state': address.state,
                'locality': address.locality,
            }
        # Order Current Status
        current_status = db.query(OrderStatusModel).filter(
            OrderStatusModel.order_id == order.id).order_by(OrderStatusModel.id.desc()).first()
        status = ''
        for st in OrderStatus.AllstatusList():
            if(current_status.status == st['status_id']):
                status = st['status_title']

        # Calculate Items
        order_items = []
        items = db.query(OrderItemsModel).where(
            OrderItemsModel.order_id == order_id).all()
        total_amount = 0
        cancel_total_amount = 0
        return_total_amount = 0
        discount_amount = 0
        # Aseztak Service
        today_date = order.created_at.strftime('%Y-%m-%d')
        aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
        for item_data in items:
            # Pricing Object
            pricingdata = item_data.uuid.split('-')
            pricingdata = db.query(ProductPricingModel).filter(
                ProductPricingModel.id == pricingdata[1]).first()
            # product details
            product = db.query(ProductModel).where(
                ProductModel.id == item_data.product_id).first()
            filename = ''
            if(item_data.images != None):

                image = db.query(ProductMediaModel).filter(
                    ProductMediaModel.id == item_data.images.strip("[]")).first()

                filename = image.file_path
            else:
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == item_data.product_id).order_by(
                    ProductMediaModel.default_img.desc()).first()

                if(image is not None):
                    filename = image.file_path

            img = filename
            if(img is None):
                img = ''

            # item price
            item_total_amount = 0
            # # Aseztak Service

            if(aseztak_service is None):
                # Calculate Product Price
                product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=0,
                                                        gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)
                item_total_amount = product_price * item_data.quantity

                total_amount += product_price * item_data.quantity
            else:
                # Get Product Price
                product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item_data.id)

                item_total_amount = product_price * item_data.quantity

                total_amount += product_price * item_data.quantity

            # Check Order Item Discount for V4
            # Check Cancle Items Amount
            if(item_data.status == 980):
                cancel_total_amount += product_price * item_data.quantity

                # # Check Return Items Amount
            if(item_data.status >= 90 and item_data.status != 91 and item_data.status != 980):
                return_total_amount += product_price * item_data.quantity

            itm_data = {
                "image": img,
                "name": product.title,
                "short_description": product.short_description,
                "price": product_price,
                "quantity": item_data.quantity,
                "total": round(item_total_amount, 2),
                'attributes': json.loads(item_data.attributes),
            }
            order_items.append(itm_data)
        # Seller
        pickup_address = db.query(PickupLocationModel).filter(
            PickupLocationModel.id == order.pickup_address_id).first()
        seller = db.query(ProductModel).filter(
            ProductModel.id == items[0].product_id).first()

        seller = db.query(UserModel).filter(
            UserModel.id == seller.userid).first()

        if pickup_address:

            city = pickup_address.city
            state = pickup_address.return_state

        else:
            city = seller.city
            state = seller.region

        if(order.commission_reff is None):
            order.commission_reff = ""
        # Check Shipping Charge
        # Calculate Total Active Amount
        if(current_status.status >= 90 and current_status.status != 91):
            cancel_total_amount = 0
            return_total_amount = 0
        order_date = order.created_at.strftime("%Y-%m-%d")
        rest_total_amount = (
            total_amount - cancel_total_amount - return_total_amount)
        shipping_charge = db.query(ShippingChargeModel).filter(
            ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

        delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order.free_delivery, user_id=order.user_id, order_date=order.created_at,
                                                   app_version=order.app_version, order_amount=rest_total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

        checkinvoice = db.execute("SELECT orders.id, orders.invoice FROM orders WHERE orders.id =:param AND orders.invoice REGEXP '^[0-9]+$'  and app_version = 'V4'", {
            "param": order.id
        }).first()
        if(checkinvoice is not None):
            if(int(checkinvoice.invoice) <= 9):
                ord_invoice = str('ASEZ202320240')+str(checkinvoice.invoice)
            else:
                ord_invoice = str('ASEZ20232024')+str(checkinvoice.invoice)

        else:
            ord_invoice = order.invoice
        discount_rate = order.discount_rate

        # Check Order Discount for V3
        order_date = order.created_at.strftime("%Y-%m-%d")

        if(order.discount != 0):
            order_discount = db.query(OrderDiscountModel).filter(
                OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

            discount_rate = order_discount.discount
            if(order.discount_rate != 0):
                discount_rate = order.discount_rate
        shipping = db.query(OrderShippingModel).filter(
            OrderShippingModel.order_id == order.id).first()

        shippingdata = {}
        if (shipping is not None):
            shippingdata = {
                'wbns': shipping.wbns,
                'courier_partner': shipping.courier_partner,
            }

        order_data = {
            'id': order.id,
            'order_amount': order.grand_total,
            'items_count': order.item_count,
            'order_no': order.order_number,
            'reff_no': order.commission_reff,
            'invoice': ord_invoice,
            'invoice_date': order.invoice_date,
            'order_date': order.created_at.strftime("%Y-%m-%d %H:%M:%S "),
            'current_status': status,
            'shipping_charge': delivery_charge,
            'payment_mode': order.payment_method,
            'order_total_amount': round(total_amount, 2),
            'cancel_total_amount': round(cancel_total_amount, 2),
            'return_total_amount': round(return_total_amount, 2),
            'total_tax': order.total_tax,
            'discount_rate': discount_rate,
            'discount_amount': order.discount,
            'seller_detail': {
                'name': seller.name,
                'location': str(city) + ', ' + str(state)
            },
            'buyer_detail': {
                'name': address['ship_to'],
                'location': str(address['city']) + ', ' + str(address['state'])
            },
            "items": order_items,
            "shipping": shippingdata
        }

        return order_data
    except Exception as e:
        print(e)

# Order Detail


@new_admin_app_order_router.get("/statistics/{date}", dependencies=[Depends(JWTBearer())])
async def Orderstatistics(request: Request, date: str = '', db: Session = Depends(get_db)):
    try:
        entered_date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
        entered_date = entered_date.date()
        new_date = (entered_date-datetime.timedelta(days=9))

        date_wise_order_list = []
        for order in (range(10)):
            date_ = (new_date + timedelta(days=order)).strftime('%Y-%m-%d')
            day = (new_date + timedelta(days=order)).strftime('%d %b %Y')
            daydate = (new_date + timedelta(days=order)).strftime('%d')
            orders = db.execute("SELECT SUM((orders.grand_total)) as grandtotal, COUNT((orders.id)) AS total_order  FROM orders LEFT JOIN order_fails  ON order_fails.order_id = orders.id WHERE  date_format(orders.created_at, '%Y-%m-%d') =:date_ AND orders.id IN(SELECT order_items.order_id from order_items WHERE order_items.status != 980 AND order_fails.order_id is NULL GROUP BY order_items.order_id) ORDER BY orders.id DESC", {
                "date_": date_
            }).first()

            totalorder = 0
            totalorderamount = 0
            if(orders.grandtotal is not None):
                totalorder = orders.total_order
                totalorderamount = orders.grandtotal

            order_data = {
                'date': day,
                'day': daydate,
                'orders': totalorder,
                'order_amount': totalorderamount,
            }
            date_wise_order_list.append(order_data)
        return date_wise_order_list
    except Exception as e:
        print(e)
