from app.api.helpers.admin_app_products import *
from fastapi import APIRouter
from app.resources.strings import *


admin_app_notification_image_router = APIRouter()


# @admin_app_notification_image_router.get("/", dependencies=[Depends(JWTBearer())])
# async def getImage(request: Request, page: int = 1, limit: int = 15, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data = db.query(NotificationImageModel).order_by(
#             NotificationImageModel.id.desc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK,  "notidication_image": [], "total_records": 0, "current_page": page, "total_page": 0}

#         images: NotificationImageModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         notification_image = []

#         for image in images:

#             if(image.created_at is not None):
#                 created_at = image.created_at

#             else:
#                 created_at = ''

#             image = {
#                 'id': image.id,
#                 'image': image.file_path,
#                 'created_at': created_at
#             }

#             notification_image.append(image)

#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_202_ACCEPTED, "notidication_image": notification_image, "total_records": total_records,  "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         print(e)


# # upload notification image
# @admin_app_notification_image_router.post("/upload_image", dependencies=[Depends(JWTBearer())])
# async def Uploadimage(request: Request, image: UploadFile = File(...), db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         file = image.filename.split(".")
#         extension = file[1]
#         uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
#             datetime.now().strftime('%Y-%d-%M-%h-%s')

#         filename = f"{uuid.uuid4()}.{file[1]}"

#         # Set Path Of image
#         file_name = getcwd()+"/app/static/notification/"+filename

#         # Upload image in path
#         with open(file_name, 'wb+') as f:
#             f.write(image.file.read())
#             f.close()

#         # open the image
#         uploaded_file = getcwd()+"/app/static/notification/" + str(filename)
#         filename = str(filename)
#         # Upload Photo
#         MediaHelper.uploadNotificationImage(
#             uploaded_file=uploaded_file, file_name=filename)

#         # Remove Original Image from Path

#         unlink(uploaded_file)

#         file_path = str(
#             linode_obj_config['endpoint_url'])+'/notification/'+str(filename)

#         dbadd = NotificationImageModel(
#             image=filename,
#             file_path=file_path,
#             created_at=datetime.now(),
#             updated_at=datetime.now()
#         )

#         db.add(dbadd)
#         db.commit()
#         db.refresh(dbadd)

#         return {"status_code": HTTP_202_ACCEPTED, "message": "Notification Image added successfully..."}

#     except Exception as e:
#         print(e)


# # delete notification image
# @admin_app_notification_image_router.get("/delete/{image_id}", dependencies=[Depends(JWTBearer())])
# async def deleteimage(request: Request, image_id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         db.query(NotificationImageModel).filter(
#             NotificationImageModel.id == image_id).delete()
#         db.commit()
#         return{"status_code": HTTP_200_OK, "message": "Notification Image Deleted..."}
#     except Exception as e:
#         return {"Status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified..."}
