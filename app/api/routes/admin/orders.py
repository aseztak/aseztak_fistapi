
from app.api.helpers.admin_app_products import *
from fastapi import APIRouter

admin_app_order_router = APIRouter()


# Count All Orders
# @admin_app_order_router.get("/count-orders", dependencies=[Depends(JWTBearer())])
# async def countOrders(request: Request, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         all = await OrderHelper.getAllOrdersAdminNew(db=db, status=999)
#         pending = await OrderHelper.getAllOrdersAdminNew(db=db, status=0)
#         accepted = await OrderHelper.getAllOrdersAdminNew(db=db, status=10)
#         approved = await OrderHelper.getAllOrdersAdminNew(db=db, status=30)
#         packed = await OrderHelper.getAllOrdersAdminNew(db=db, status=40)
#         shipped = await OrderHelper.getAllOrdersAdminNew(db=db, status=60)
#         delivered = await OrderHelper.getAllOrdersAdminNew(db=db, status=70)

#         count_orders = [
#             {
#                 'status': 'all',
#                 'name': 'All Orders',
#                 'value': all
#             },
#             {
#                 'status': '0',
#                 'name': 'Pending Orders',
#                 'value': pending
#             },
#             {
#                 'status': '10',
#                 'name':  'Accepted Orders',
#                 'value': accepted
#             },
#             {
#                 'status': '30',
#                 'name': 'Approved Orders',
#                 'value': approved
#             },
#             {
#                 'status': '40',
#                 'name': 'Packed Orders',
#                 'value': packed
#             },
#             {
#                 'status': '60',
#                 'name': 'Shipped Orders',
#                 'value': shipped
#             },
#             {
#                 'status': '70',
#                 'name': 'Delivered Orders',
#                 'value': delivered
#             }
#         ]
#         return {"status_code": HTTP_200_OK, "orders_count": count_orders}

#     except Exception as e:
#         print(e)

# # Get all orders


# @admin_app_order_router.post("/order_list", response_model=AdminOrderListSchema, dependencies=[Depends(JWTBearer())])
# async def getOrders(request: Request, param: AdminorderSearch, db: Session = Depends(get_db), page: int = 1, limit: int = 15):
#     userdata = auth(request=request)

#     # Check Not Admin
#     checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#     if(checkNotadmin == False):
#         return {"status_code": HTTP_304_NOT_MODIFIED}

#     checkUser = db.query(AdminModel).filter(
#         AdminModel.email == userdata['email']).first()

#     if(checkUser is not None and checkUser.level == 1):
#         try:
#             if(param.search != ''):
#                 search = "%{}%".format(param.search.replace(
#                     "+", " ").lower().lstrip('asez'))
#                 # search = re.sub(r'\W+', '', search)
#                 search = search[search.find(search):search.lower().find('u')]
#                 # search = re.compile('.*[A-Za-z].*')

#                 # search = search[search.find("ASEZ")+4:search.lower().find('u')]
#                 # matcher = re.compile('ASEZ', re.IGNORECASE)
#                 # return search
#             if(param.search != '' and param.from_date != ''):
#                 if(param.status != 'all'):
#                     data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(
#                         OrdersModel.order_number.ilike(search) | OrdersModel.reff.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search) | ProductModel.title.ilike(search) | UserModel.name.like(search) | UserModel.mobile.like(search)).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == param.status).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).filter(
#                             OrderShippingModel.wbns.ilike(search)).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                     OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == param.status).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderRevreseModel, OrderRevreseModel.order_id == OrdersModel.id).filter(
#                             OrderRevreseModel.wbns.ilike(search)).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                     OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == param.status).order_by(OrdersModel.id.desc())

#                 else:
#                     data = db.query(OrdersModel).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(
#                         OrdersModel.order_number.like(search) | OrdersModel.reff.ilike(search) | ShippingAddressModel.ship_to.like(search) | ShippingAddressModel.phone.like(search) | ProductModel.title.like(search)).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).filter(
#                             OrderShippingModel.wbns.like(search)).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                     OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).join(OrderRevreseModel, OrderRevreseModel.order_id == OrdersModel.id).filter(
#                             OrderRevreseModel.wbns.like(search)).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                     OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#             elif(param.search != '' and param.from_date == ''):
#                 if(param.status != 'all'):

#                     # data = db.query(OrdersModel).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).having(func.max(OrderStatusModel.status) == param.status).filter(
#                     #         OrdersModel.order_number.ilike(search) | OrdersModel.reff.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search) | ProductModel.title.ilike(search) | UserModel.name.like(search) | UserModel.mobile.like(search)).group_by(OrderStatusModel.order_id).order_by(OrdersModel.id.desc())

#                     data = db.query(OrdersModel).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).having(func.max(
#                         OrderStatusModel.status) == param.status).filter(OrdersModel.order_number.like(search) | OrdersModel.reff.ilike(search) | ShippingAddressModel.ship_to.like(search) | ShippingAddressModel.phone.like(search) | ProductModel.title.like(search)).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).having(func.max(
#                             OrderStatusModel.status) == param.status).filter(
#                             OrderShippingModel.wbns.ilike(search)).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).join(OrderRevreseModel, OrderRevreseModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).having(func.max(
#                             OrderStatusModel.status) == param.status).filter(
#                             OrderRevreseModel.wbns.ilike(search)).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#                 else:
#                     data = db.query(OrdersModel).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(
#                         OrdersModel.order_number.like(search) | OrdersModel.reff.ilike(search) | ShippingAddressModel.ship_to.like(search) | ShippingAddressModel.phone.like(search) | ProductModel.title.like(search)).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).filter(
#                             OrderShippingModel.wbns.like(search)).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).join(OrderRevreseModel, OrderRevreseModel.order_id == OrdersModel.id).filter(
#                             OrderRevreseModel.wbns.like(search)).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#             else:
#                 if(param.status != 'all'):
#                     data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == param.status).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == param.status).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == param.status).order_by(OrdersModel.id.desc())

#                 else:
#                     data = db.query(OrdersModel).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#                     if(data.count() == 0):
#                         data = db.query(OrdersModel).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#             if(param.search == '' and param.from_date == ''):
#                 data: OrdersModel = await OrderHelper.getAllOrdersAdmin(db=db, status=param.status, page=page, limit=limit)

#             if(data.count() == 0):
#                 return {"status_code": HTTP_200_OK, "total_records": 0, "orders": [], "current_page": page, "total_page": 0}

#             orders: OrdersModel = data.limit(
#                 limit=limit).offset((page - 1) * limit).all()

#             # Calculation Total Amount of items
#             orderdata: OrdersModel = await Calculations.CalculateTotalAmountAllOrders(db=db, data=orders)

#             # Order Status
#             orderdata: OrdersModel = await OrderHelper.customizeOrderData(db=db, data=orderdata)

#             # Count Total Products
#             total_records = data.count()

#             # Number of Total Pages
#             total_pages = str(round((total_records/limit), 2))

#             total_pages = total_pages.split('.')

#             if(total_pages[1] != 0):
#                 total_pages = int(total_pages[0]) + 1

#             return {"status_code": HTTP_202_ACCEPTED, "orders": orderdata, "total_records": total_records,  "current_page": page, "total_page": total_pages}

#         except Exception as e:
#             print(e)

#     else:
#         return {"status_code": HTTP_401_UNAUTHORIZED}


# @admin_app_order_router.get("/order_details/{order_id}", response_model=AdminOrderDetailsSchema, dependencies=[Depends(JWTBearer())])
# async def Orderdetails(request: Request, order_id: int, db: Session = Depends(get_db)):

#     # Check Not Admin
#     checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#     if(checkNotadmin == False):
#         return {"status_code": HTTP_304_NOT_MODIFIED}

#     # Order Data
#     data = db.query(OrdersModel, ShippingAddressModel).join(ShippingAddressModel,
#                                                             ShippingAddressModel.id == OrdersModel.address_id).where(OrdersModel.id == order_id).first()

#     # Shipping Address
#     shipping_address = data.ShippingAddressModel

#     # Buyer
#     buyer = db.query(UserModel).filter(
#         UserModel.id == shipping_address.user_id).first()

#     # Shipping Address
#     shipped_address = {
#         'ship_to': shipping_address.ship_to,
#         'email': buyer.email,
#         'address': shipping_address.address,
#         'city': shipping_address.city,
#         'pincode': shipping_address.pincode,
#         'state': shipping_address.state,
#         'locality': shipping_address.locality,
#         'country': shipping_address.country,
#         'phone': shipping_address.phone,
#     }

#     # Order
#     order = data.OrdersModel

#     # Pickup Address
#     sellersaddress = db.query(UserModel).join(ProductModel, ProductModel.userid == UserModel.id).join(
#         OrderItemsModel, OrderItemsModel.product_id == ProductModel.id).filter(OrderItemsModel.order_id == order.id).first()

#     seller_address = {


#         'seller': sellersaddress.name,
#         'city': sellersaddress.city,
#         'region': sellersaddress.region,
#         'country': sellersaddress.country,
#         'pincode': sellersaddress.pincode,
#         'mobile': sellersaddress.mobile,
#         'email': sellersaddress.email
#     }

#     invoice = ""
#     invdate = ""
#     if(order.invoice_date is None):
#         order.invoice_date = invdate
#         order.invcoice = invoice
#     else:
#         order.invoice_date = order.invoice_date.strftime("%Y-%m-%d")

#     # Order Current Status
#     current_status = order.order_status.order_by(
#         OrderStatusModel.id.desc()).first()
#     status = ''
#     for st in OrderStatus.AllstatusList():
#         if(current_status.status == st['status_id']):
#             status = st['status_title']

#     order_status = status

#     # shipping details
#     shipping = ""
#     if(order_status >= 'Picked up Competed' and order_status <= 'Shipped'):
#         shipping = order.order_shipping.first()

#         if(shipping is not None):
#             if(shipping.courier_partner == 'DELHIVERY'):
#                 tracking_link = 'https://www.delhivery.com/track/package/' + \
#                     str(shipping.wbns)

#             elif(shipping.courier_partner == 'ECOMEXPRESS'):
#                 tracking_link = 'https://shiprocket.co/tracking/' + \
#                     str(shipping.wbns)

#             elif(shipping.courier_partner == 'XPRESSBEES'):
#                 tracking_link = 'https://ship.xpressbees.com/shipping/tracking/' + \
#                     str(shipping.wbns)

#             elif(shipping.courier_partner == 'UDAAN EXPRESS'):
#                 tracking_link = 'https://udaanexpress.com/track/' + \
#                     str(shipping.wbns)

#             elif(shipping.courier_partner == 'DTDC'):
#                 tracking_link = 'https://www.trackingmore.com/track/en/' + \
#                     str(shipping.wbns)+str('?express=dtdc')

#             shipping = {
#                 'wbns': shipping.wbns,
#                 'courier_partner': shipping.courier_partner,
#                 'tracking_link': tracking_link
#             }

#     # Order Items
#     order_items = []
#     items = order.order_items.all()
#     total_amount = 0
#     cancel_total_amount = 0
#     return_total_amount = 0
#     discount_amount = 0
#     cancel_total_discount_amount = 0
#     return_total_discount_amount = 0
#     for item_data in items:

#         status = ''
#         for st in OrderStatus.AllstatusList():
#             if(item_data.status == st['status_id']):
#                 status = st['status_title']
#         pricingdata = item_data.uuid.split('-')
#         pricingdata = db.query(ProductPricingModel).filter(
#             ProductPricingModel.id == pricingdata[1]).first()
#         # product details
#         product = db.query(ProductModel).where(
#             ProductModel.id == item_data.product_id).first()

#         image = db.query(ProductMediaModel).filter(ProductMediaModel.deleted_at.is_(
#             None)).filter(ProductMediaModel.model_id == product.id).first()

#         # item price
#         item_total_amount = 0
#         # Aseztak Service
#         # aseztak_service = Services.aseztak_services(
#         #     item_data.created_at, db=db)
#         today_date = item_data.created_at.strftime(
#             '%Y-%m-%d')
#         aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#         if(aseztak_service is None):
#             # Calculate Product Price
#             product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=0,
#                                                     gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)

#             item_total_amount = product_price * item_data.quantity

#             total_amount += product_price * item_data.quantity
#         else:
#             product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item_data.id)
#             # Calculate Product Price
#             # product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=aseztak_service.rate,
#             #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order.app_version)

#             item_total_amount = product_price * item_data.quantity

#             total_amount += product_price * item_data.quantity

#         # Check Order Item Discount for V4
#         if(item_data.discount_amount != 0):
#             discount_amount += item_data.discount_amount

#         # Check Cancle Items Amount
#         if(item_data.status == 980):
#             if(item_data.discount_amount != 0):
#                 cancel_total_discount_amount += item_data.discount_amount

#             cancel_total_amount += product_price * item_data.quantity

#             # # Check Return Items Amount
#         if(item_data.status >= 90 and item_data.status != 91 and item_data.status != 980):
#             if(item_data.discount_amount != 0):
#                 return_total_discount_amount += item_data.discount_amount

#             return_total_amount += product_price * item_data.quantity

#         message = ""
#         if(item_data.message is None):

#             item_data.message = message

#         else:

#             item_data.message = item_data.message
#         # Item Details
#         item = {
#             'product_id': product.id,
#             'product': product.title,
#             'quantity': item_data.quantity,
#             'attributes': item_data.attributes,
#             'image': image.file_path,
#             'price': product_price,
#             'total_amount': item_total_amount,
#             'status': status,
#             'message': item_data.message

#         }

#         order_items.append(item)

#     if(current_status.status >= 90 and current_status.status != 91):
#         cancel_total_amount = 0
#         return_total_amount = 0
#         cancel_total_discount_amount = 0
#         return_total_discount_amount = 0

#     # Calculate Total Active Amount
#     rest_total_amount = (
#         total_amount - cancel_total_amount - return_total_amount)

#     # Discount Amount
#     discount_rate = order.discount_rate
#     discount_amount = (
#         discount_amount - cancel_total_discount_amount - return_total_discount_amount)

#     # Check Order Discount for V3
#     order_date = order.created_at.strftime("%Y-%m-%d")

#     if(order.app_version == 'V3' and order.discount != 0):
#         order_discount = db.query(OrderDiscountModel).filter(
#             OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#         discount_rate = order_discount.discount
#         if(order.discount_rate != 0):
#             discount_rate = order.discount_rate

#         discount_amount = orderDiscountCalculation(app_version=order.app_version,
#                                                    order_amount=rest_total_amount, discount_amount=order.discount, discount_rate=discount_rate)

#     # Check Shipping Charge
#     # delivery_charge = 0
#     # if(order.delivery_charge != 0): CHANGES RAHUL
#     shipping_charge = db.query(ShippingChargeModel).filter(
#         ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#     delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order.free_delivery, user_id=order.user_id, order_date=order.created_at,
#                                                app_version=order.app_version, order_amount=rest_total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

#     # Grand Total
#     if(order.app_version == 'V4'):
#         if(cancel_total_amount != 0 or return_total_amount != 0):
#             grand_total = (
#                 rest_total_amount + delivery_charge) - discount_amount
#         else:
#             grand_total = order.grand_total
#     else:
#         grand_total = (
#             rest_total_amount + delivery_charge) - discount_amount

#         grand_total = round(grand_total)

#     # Check Transaction
#     if(order.payment_method == 'ONLINE'):

#         transaction = db.query(TransactionsModel).filter(
#             TransactionsModel.order_ref_id == order.reff).first()

#         transaction = transaction.txn_payment_id
#     else:
#         transaction = ''
#     order = {
#         'id': order.id,
#         'order_number': order.order_number,
#         'invoice': order.invoice,
#         'invoice_date': order.invoice_date,
#         'payment_method': order.payment_method,
#         'transaction': transaction,
#         'order_total_amount': round(total_amount, 2),
#         'cancel_total_amount': cancel_total_amount,
#         'return_total_amount': return_total_amount,
#         'message': order.message,
#         'delivery_charge': delivery_charge,
#         'total_tax': order.total_tax,
#         'discount': round(discount_amount, 2),
#         'discount_rate': discount_rate,
#         'grand_total': round(grand_total, 2),
#         'created_at': order.created_at.strftime("%B %d %Y"),

#     }

#     allorderstatus = []

#     statusvalue = db.query(OrderStatusModel).filter(
#         OrderStatusModel.order_id == order['id']).all()
#     for orderstatus in statusvalue:
#         stats = ''
#         for sts in OrderStatus.AllstatusList():
#             if(orderstatus.status == sts['status_id']):
#                 stats = sts['status_title']

#         if(stats == 'Cancelled' or stats == 'Return Initiated'):
#             message = order['message']
#         stts = {
#             'created_at': orderstatus.created_at.strftime(" %d %B %Y"),
#             'status': stats,
#             'message': message
#         }
#         allorderstatus.append(stts)

#     return {"status_code": HTTP_200_OK, "shipping": shipping, "shipping_address": shipped_address, "seller_address": seller_address,  "order_summery": order, "items": order_items, "current_status": order_status, "all_status": allorderstatus}


# # buyer list
# @admin_app_order_router.post("/buyer/list", response_model=AdminUserListSchema, dependencies=[Depends(JWTBearer())])
# async def buyerslist(request: Request, data: AdminUserSearch,  db: Session = Depends(get_db), page: int = 1, limit: int = 15):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(data.name != ''):
#             search = "%{}%".format(data.name.replace("+", " ").strip())

#         if(data.name != ''):
#             buyer = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#                 UserRoleModel.role_id == 5).join(OrdersModel, OrdersModel.user_id == UserModel.id).filter(UserModel.name.like(search) | UserModel.mobile.like(search)).group_by(OrdersModel.user_id).order_by(UserModel.id.desc())

#         if(data.name == ''):
#             buyer = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
#                 UserRoleModel.role_id == 5).join(OrdersModel, OrdersModel.user_id == UserModel.id).group_by(OrdersModel.user_id).order_by(UserModel.id.desc())

#         if(buyer.count() == 0):
#             return {"status_code": HTTP_200_OK,  "users": [], "total_records": 0, "current_page": page, "total_page": 0}

#         buyers: UserModel = buyer.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         buyer_data = []

#         for buyerlist in buyers:
#             order = db.query(OrdersModel).filter(
#                 OrdersModel.user_id == buyerlist.id).count()

#             role = db.query(UserRoleModel, Roles).join(Roles, Roles.id == UserRoleModel.role_id).filter(
#                 UserRoleModel.model_id == buyerlist.id).first()

#             rolename = ''
#             if(role is not None):
#                 rolename = role.Roles.name

#             if(buyerlist.created_at is not None):
#                 buyerlist.created_at = buyerlist.created_at.strftime(
#                     '%B %d %Y')
#             else:
#                 buyerlist.created_at = ''

#             if('No' in str(buyerlist.confirm)):
#                 confirm = 'No'
#             else:
#                 confirm = 'Yes'

#             buyerlist = {
#                 'id': buyerlist.id,
#                 'name': buyerlist.name,
#                 'email_id': buyerlist.email,
#                 'address': str(buyerlist.city)+', '+str(buyerlist.region)+', '+str(buyerlist.country)+' - '+str(buyerlist.pincode),
#                 'total_order': order,
#                 'confirm': confirm,
#                 'phone_no': buyerlist.mobile,
#                 'roles': rolename,
#                 'created_at': buyerlist.created_at


#             }
#             buyer_data.append(buyerlist)

#         # Count Total Products
#         total_records = buyer.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "users": buyer_data, "total_records": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "users": [], "total_records": 0, "current_page": 0,  "total_pages": 0}


# # buyer wise orders
# @admin_app_order_router.post("/buyer_wise_orders", response_model=AdminOrderListSchema, dependencies=[Depends(JWTBearer())])
# async def buyerOrders(request: Request, user: AdminUserWiseOrdersSchema, db: Session = Depends(get_db), page: int = 1, limit: int = 15):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(user.search != ''):
#             search = "%{}%".format(user.search.replace(
#                 "+", " ").lower().lstrip('asez'))
#             # search = re.sub(r'\W+', '', search)
#             search = search[search.find(search):search.lower().find('u')]
#             # search = search[search.find("ASEZ")+4:search.lower().find('u')]
#             # matcher = re.compile('ASEZ', re.IGNORECASE)
#             # return search
#         if(user.search != '' and user.from_date != ''):

#             data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(
#                 OrdersModel.order_number.ilike(search) | OrdersModel.reff.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search) | ProductModel.title.ilike(search) | UserModel.name.like(search) | UserModel.mobile.like(search)).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).filter(
#                     OrderShippingModel.wbns.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderRevreseModel, OrderRevreseModel.order_id == OrdersModel.id).filter(
#                     OrderRevreseModel.wbns.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#         elif(user.search != '' and user.from_date == ''):

#             data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(
#                 OrdersModel.order_number.ilike(search) | OrdersModel.reff.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search) | ProductModel.title.ilike(search) | UserModel.name.like(search) | UserModel.mobile.like(search)).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).filter(
#                     OrderShippingModel.wbns.ilike(search)).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderRevreseModel, OrderRevreseModel.order_id == OrdersModel.id).filter(
#                     OrderRevreseModel.wbns.ilike(search)).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#         else:

#             data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                 OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#         if(user.search == '' and user.from_date == ''):
#             data: OrdersModel = await OrderHelper.getAllBuyersOrdersAdmin(db=db, user_id=user.user_id, status=user.param, page=page, limit=limit)

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK,  "orders": [], "total_records": 0, "current_page": page, "total_page": 0}

#         orders: OrdersModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         # Calculation Total Amount of items

#         orderdata: OrdersModel = await Calculations.CalculateTotalAmountAllOrders(db=db, data=orders)

#         # Order Status
#         orderdata: OrdersModel = await OrderHelper.customizeOrderData(db=db, data=orderdata)

#         # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_202_ACCEPTED, "orders": orderdata, "total_records": total_records,  "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "orders": [], "total_records": 0, "current_page": 0,  "total_page": 0}


# seller list
# @admin_app_order_router.post("/seller/list", response_model=AdminUserListSchema,  dependencies=[Depends(JWTBearer())])
# async def Sellerlist(request: Request, data: AdminUserSearch,  db: Session = Depends(get_db), page: int = 1, limit: int = 15):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(data.name != ''):
#             search = "%{}%".format(data.name.replace("+", " ").strip())

#         if(data.name != ''):
#             seller = db.query(UserModel).join(ProductModel, ProductModel.userid == UserModel.id).join(OrderItemsModel,
#                                                                                                       OrderItemsModel.product_id == ProductModel.id).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(UserModel.name.like(search) | UserModel.mobile.like(search)).group_by(ProductModel.userid).order_by(UserModel.id.desc())

#         if(data.name == ''):
#             seller = db.query(UserModel).join(ProductModel, ProductModel.userid == UserModel.id).join(OrderItemsModel,
#                                                                                                       OrderItemsModel.product_id == ProductModel.id).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).group_by(ProductModel.userid).order_by(UserModel.id.desc())

#         if(seller.count() == 0):
#             return {"status_code": HTTP_200_OK,  "users": [], "total_records": 0, "current_page": page, "total_page": 0}

#         sellers: UserModel = seller.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         seller_data = []

#         for sellerlist in sellers:
#             order = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
#                 ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(
#                 ProductModel.userid == sellerlist.id).group_by(OrderItemsModel.order_id).count()

#             role = db.query(UserRoleModel, Roles).join(Roles, Roles.id == UserRoleModel.role_id).filter(
#                 UserRoleModel.model_id == sellerlist.id).first()

#             rolename = ''
#             if(role is not None):
#                 rolename = role.Roles.name

#             if(sellerlist.created_at is not None):
#                 sellerlist.created_at = sellerlist.created_at.strftime(
#                     '%B %d %Y')
#             else:
#                 sellerlist.created_at = ''

#             if('No' in str(sellerlist.confirm)):
#                 confirm = 'No'
#             else:
#                 confirm = 'Yes'

#             sellerlist = {
#                 'id': sellerlist.id,
#                 'name': sellerlist.name,
#                 'email_id': sellerlist.email,
#                 'address': str(sellerlist.city)+', '+str(sellerlist.region)+', '+str(sellerlist.country)+' - '+str(sellerlist.pincode),
#                 'total_order': order,
#                 'confirm': confirm,
#                 'phone_no': sellerlist.mobile,
#                 'roles': rolename,
#                 'created_at': sellerlist.created_at


#             }
#             seller_data.append(sellerlist)

#         # Count Total Products
#         total_records = seller.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "users": seller_data, "total_records": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "users": [], "total_records": 0, "current_page": 0,  "total_pages": 0}


# # buyer wise orders
# @admin_app_order_router.post("/seller_wise_orders", response_model=AdminOrderListSchema, dependencies=[Depends(JWTBearer())])
# async def SellerOrders(request: Request, user: AdminUserWiseOrdersSchema, db: Session = Depends(get_db), page: int = 1, limit: int = 15):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         if(user.search != ''):
#             search = "%{}%".format(user.search.replace(
#                 "+", " ").lower().lstrip('asez'))
#             # search = re.sub(r'\W+', '', search)
#             search = search[search.find(search):search.lower().find('u')]
#             # search = search[search.find("ASEZ")+4:search.lower().find('u')]
#             # matcher = re.compile('ASEZ', re.IGNORECASE)
#             # return search
#         if(user.search != '' and user.from_date != ''):

#             data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(
#                 OrdersModel.order_number.ilike(search) | OrdersModel.reff.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search) | ProductModel.title.ilike(search) | UserModel.name.like(search) | UserModel.mobile.like(search)).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).filter(
#                     OrderShippingModel.wbns.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderRevreseModel, OrderRevreseModel.order_id == OrdersModel.id).filter(
#                     OrderRevreseModel.wbns.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#         elif(user.search != '' and user.from_date == ''):

#             data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(
#                 OrdersModel.order_number.ilike(search) | OrdersModel.reff.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search) | ProductModel.title.ilike(search) | UserModel.name.like(search) | UserModel.mobile.like(search)).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).filter(
#                     OrderShippingModel.wbns.ilike(search)).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderRevreseModel, OrderRevreseModel.order_id == OrdersModel.id).filter(
#                     OrderRevreseModel.wbns.ilike(search)).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#         else:

#             data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                 OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#             if(data.count() == 0):
#                 data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= user.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= user.to_date).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == user.param).order_by(OrdersModel.id.desc())

#         if(user.search == '' and user.from_date == ''):
#             data: OrdersModel = await OrderHelper.getAllSellersOrdersAdmin(db=db, user_id=user.user_id, status=user.param, page=page, limit=limit)

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK,  "orders": [], "total_records": 0, "current_page": page, "total_page": 0}

#         orders: OrdersModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         # Calculation Total Amount of items

#         orderdata: OrdersModel = await Calculations.CalculateTotalAmountAllOrders(db=db, data=orders)

#         # Order Status
#         orderdata: OrdersModel = await OrderHelper.customizeOrderData(db=db, data=orderdata)

#         # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_202_ACCEPTED, "orders": orderdata, "total_records": total_records,  "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, "orders": [], "total_records": 0, "current_page": 0,  "total_page": 0}


# # today Orders
# @admin_app_order_router.get("/today/orders", response_model=AdminOrderListSchema, dependencies=[Depends(JWTBearer())])
# async def orders(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 15):

#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         today = datetime.now()
#         today = today.strftime("%Y-%m-%d")

#         data = db.query(OrdersModel).filter(func.date_format(
#             OrdersModel.created_at,  "%Y-%m-%d") == today).order_by(OrdersModel.id.desc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK, "total_records": 0, "orders": [], "current_page": page, "total_page": 0}

#         orders: OrdersModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         # Calculation Total Amount of items
#         orderdata: OrdersModel = await Calculations.CalculateTotalAmountAllOrders(db=db, data=orders)

#         # Order Status
#         orderdata: OrdersModel = await OrderHelper.customizeOrderData(db=db, data=orderdata)

#         # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_202_ACCEPTED, "orders": orderdata, "total_records": total_records,  "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         print(e)


# today seller wise order
# @admin_app_order_router.get("/today/seller_wise_orders", dependencies=[Depends(JWTBearer())])
# async def todaySeller(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 15):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         today = datetime.now()
#         today = today.strftime("%Y-%m-%d")

#         data = db.query(UserModel).join(ProductModel, ProductModel.userid == UserModel.id).join(
#             OrderItemsModel, OrderItemsModel.product_id == ProductModel.id).join(OrdersModel,
#                                                                                  OrdersModel.id == OrderItemsModel.order_id).group_by(ProductModel.userid).filter(func.date_format(
#                                                                                      OrdersModel.created_at,  "%Y-%m-%d") == today).order_by(UserModel.id.desc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK,  "sellers": [],  "total_records": 0, "current_page": page, "total_page": 0}

#         sellers: UserModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         sellerlist = []
#         for seller in sellers:

#             today_total_orders = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
#                 ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == seller.id).filter(func.date_format(
#                     OrdersModel.created_at,  "%Y-%m-%d") == today).group_by(OrderItemsModel.order_id).all()

#             seller_list = {
#                 'seller': seller.name,
#                 'id': seller.id,
#                 'address': str(seller.city)+', '+str(seller.region)+', '+str(seller.country)+' - '+str(seller.pincode),
#                 'total_items': len(today_total_orders)
#             }
#             sellerlist.append(seller_list)

#          # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_202_ACCEPTED,  "sellers": sellerlist, "total_records": total_records,  "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         print(e)


# @admin_app_order_router.get("/today/seller_order/{seller_id}", response_model=AdminOrderListSchema, dependencies=[Depends(JWTBearer())])
# async def Ordertoday(request: Request, seller_id: int, page: int = 1, limit: int = 15, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         today = datetime.now()
#         today = today.strftime("%Y-%m-%d")

#         data = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
#             ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == seller_id).filter(func.date_format(
#                 OrdersModel.created_at,  "%Y-%m-%d") == today)

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK, "total_records": 0, "orders": [], "current_page": page, "total_page": 0}

#         orders: OrdersModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         # Calculation Total Amount of items
#         orderdata: OrdersModel = await Calculations.CalculateTotalAmountAllOrders(db=db, data=orders)

#         # Order Status
#         orderdata: OrdersModel = await OrderHelper.customizeOrderData(db=db, data=orderdata)

#         # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_202_ACCEPTED, "orders": orderdata, "total_records": total_records,  "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         print(e)


# top buyer list
# response_model= AdminTopBuyersListSchema,
# @admin_app_order_router.get("/top/buyer_list", dependencies=[Depends(JWTBearer())])
# async def buyerlist(request: Request, page: int = 1, limit: int = 15, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
#             OrderStatusModel.status.between(70, 81)).group_by(OrdersModel.user_id).having(func.count(func.distinct(OrderStatusModel.order_id)) >= 10)

#         # data = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).join(
#         #     OrdersModel, OrdersModel.user_id == UserModel.id).join(
#         #     OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(
#         #     OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).order_by(OrderStatusModel.id.desc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK,  "top_buyers": [], "total_records": 0, "current_page": page, "total_page": 0}

#         users: UserModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         top_buyer = []
#         for user in users:

#             user = db.query(UserModel).filter(
#                 UserModel.id == user.user_id).first()

#             order = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrdersModel.user_id == user.id).group_by(
#                 OrderStatusModel.order_id).filter(OrderStatusModel.status.between(70, 81)).order_by(OrderStatusModel.id.desc()).count()

#             # return order

#             if(user.created_at is not None):
#                 created_at = user.created_at.strftime('%B %d %Y')

#             else:
#                 created_at = ''
#             user = {
#                 'id': user.id,
#                 'buyer': user.name,
#                 'email_id': user.email,
#                 'address': str(user.city)+', '+str(user.region)+', '+str(user.country)+' - '+str(user.pincode),
#                 'total_order': order,
#                 'phone_no': user.mobile,
#                 'created_at': created_at
#             }
#             top_buyer.append(user)

#          # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "top_buyers": top_buyer, "total_records": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         print(e)


# # monthly sale
# @admin_app_order_router.get("/monthly_sale/{year}", dependencies=[Depends(JWTBearer())])
# async def monthlysale(request: Request, year: int, page: int = 1, limit: int = 15, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         current_year = date.today().year

#         if(year == current_year):
#             yeardate = current_year
#             start_date = date(date.today().year, 1, 1)
#             end_date = datetime.today().date()
#             count = end_date - start_date

#             for o in range((count.days) + 1):

#                 monthname = int(
#                     (start_date + timedelta(days=o)).strftime('%m'))

#             monthint = list(range(1, (monthname+1)))
#         else:
#             yeardate = year

#             monthint = list(range(1, 13))

#         monthlist = []

#         for X in monthint:

#             month = datetime(yeardate, X, 1).strftime('%B')
#             mnth = datetime.strptime(month, '%B').month

#             num_days = calendar.monthrange(yeardate, mnth)[1]

#             days = [datetime(yeardate, mnth, day).date()
#                     for day in range(1, num_days+1)]

#             # monthlist.append(num_days)
#             grand = 0
#         # return monthlist
#             for datewise in days:

#                 order = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= datewise).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= datewise).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).all()

#                 for order_value in order:

#                     items = db.query(OrderItemsModel).filter(
#                         OrderItemsModel.order_id == order_value.id).all()

#                     total_amount = 0
#                     if(items is not None):

#                         # Latest Status

#                         for item in items:
#                             pricingdata = item.uuid.split('-')
#                             pricingdata = db.query(ProductPricingModel).where(ProductPricingModel.id == pricingdata[1]).where(
#                                 ProductPricingModel.deleted_at.is_(None)).first()
#                             # Get Product Detail
#                             productdata = db.query(ProductModel).where(
#                                 ProductModel.id == item.product_id).first()

#                             # Aseztak Service
#                             # aseztak_service = Services.aseztak_services(
#                             #     item.created_at, db=db)
#                             today_date = item.created_at.strftime(
#                                 '%Y-%m-%d')
#                             aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#                             if(aseztak_service is None):

#                                 # Calculate Product Price
#                                 product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
#                                                                         gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order_value.app_version)

#                                 total_amount += product_price * item.quantity
#                             else:
#                                 product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=order_value.app_version, order_item_id=item.id)
#                                 # Calculate Product Price
#                                 # product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
#                                 #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order_value.app_version)

#                                 total_amount += product_price * item.quantity

#                         discount_amount = 0
#                         order_date = order_value.created_at.strftime(
#                             "%Y-%m-%d")
#                         if(order_value.discount != 0):
#                             # CHECK ORDER DISCOUNT
#                             order_discount = db.query(OrderDiscountModel).filter(
#                                 OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                             discount_rate = order_discount.discount
#                             if(order_value.discount_rate != 0):
#                                 discount_rate = order_value.discount_rate

#                             discount_amount = orderDiscountCalculation(app_version=order_value.app_version,
#                                                                        order_amount=total_amount, discount_amount=order_value.discount, discount_rate=discount_rate)

#                     # Check Shipping Charge
#                         # delivery_charge = 0
#                         # if(order_value.delivery_charge != 0): CHANGES RAHUL
#                         shipping_charge = db.query(ShippingChargeModel).filter(
#                             ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#                         delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order_value.free_delivery, user_id=order_value.user_id, order_date=order_value.created_at,
#                                                                    app_version=order_value.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order_value.payment_method)

#                     # Item Total Amount
#                     # order.item_total_amount = floatingValue(total_amount)

#                     # order.total_items = len(items)

#                         if(order_value.app_version == 'V4'):

#                             grand_total = roundOf(order_value.grand_total)
#                         else:
#                             grand_total = (
#                                 total_amount + delivery_charge) - discount_amount

#                             grand_total = roundOf(grand_total)

#                             grand += grand_total

#                 orderlist = {
#                     'month': month,
#                     'sale': grand
#                 }
#                 # orders.append(orderlist)

#             monthlist.append(orderlist)

#         return {"status_code": HTTP_200_OK, "monthly Sale": monthlist}

#     except Exception as e:
#         print(e)


# # state wise monthly sale
# @admin_app_order_router.get("/state_wise/monthlysale/{month}", dependencies=[Depends(JWTBearer())])
# async def sale(month: str, db: Session = Depends(get_db)):
#     try:
#         total_sale = 0
#         totalorder = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(
#             PincodeModel, PincodeModel.pincode == ShippingAddressModel.pincode).filter(func.date_format(
#                 OrdersModel.created_at, '%Y-%m') == month).group_by(
#             OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).order_by(OrdersModel.id.desc()).count()

#         userstate = db.query(PincodeModel.statename).distinct().join(ShippingAddressModel, ShippingAddressModel.pincode == PincodeModel.pincode).join(
#             OrdersModel, OrdersModel.address_id == ShippingAddressModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                 OrdersModel.created_at, '%Y-%m') == month).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).all()

#         statename = []
#         total_order = 0
#         for state in userstate:
#             grand = 0

#             order = db.query(OrdersModel).distinct(OrdersModel.address_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(PincodeModel, PincodeModel.pincode == ShippingAddressModel.pincode).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(PincodeModel.statename == state.statename).filter(func.date_format(
#                 OrdersModel.created_at, '%Y-%m') == month).group_by(
#                 OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).order_by(OrdersModel.id.desc())

#             ordercount = order.count()
#             total_order += ordercount

#             orders = order.all()
#             # statename.append(orders)
#         # return statename
#             for order_value in orders:

#                 items = db.query(OrderItemsModel).filter(
#                     OrderItemsModel.order_id == order_value.id).all()

#                 total_amount = 0
#                 if(items is not None):

#                     # Latest Status

#                     for item in items:

#                         # Aseztak Service
#                         aseztak_service = Services.aseztak_services(
#                             item.created_at, db=db)

#                         if(aseztak_service is None):

#                             # Calculate Product Price
#                             product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
#                                                                     gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order_value.app_version)

#                             total_amount += product_price * item.quantity
#                         else:
#                             # Calculate Product Price
#                             product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
#                                                                     gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order_value.app_version)

#                             total_amount += product_price * item.quantity

#                     discount_amount = 0
#                     order_date = order_value.created_at.strftime("%Y-%m-%d")
#                     if(order_value.discount != 0):
#                         # CHECK ORDER DISCOUNT
#                         order_discount = db.query(OrderDiscountModel).filter(
#                             OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                         discount_rate = order_discount.discount
#                         if(order_value.discount_rate != 0):
#                             discount_rate = order_value.discount_rate

#                         discount_amount = orderDiscountCalculation(app_version=order_value.app_version,
#                                                                    order_amount=total_amount, discount_amount=order_value.discount, discount_rate=discount_rate)

#                     # Check Shipping Charge
#                     delivery_charge = 0
#                     if(order_value.delivery_charge != 0):
#                         shipping_charge = db.query(ShippingChargeModel).filter(
#                             ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#                         delivery_charge = orderDeliveryCalculation(
#                             app_version=order_value.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order_value.payment_method)

#                     # Item Total Amount
#                     # order.item_total_amount = floatingValue(total_amount)

#                     # order.total_items = len(items)

#                     if(order_value.app_version == 'V4'):

#                         grand_total = roundOf(order_value.grand_total)
#                     else:
#                         grand_total = (
#                             total_amount + delivery_charge) - discount_amount

#                         grand_total = roundOf(grand_total)

#                         grand += grand_total

#             percent = ((ordercount * 100) / totalorder)
#             total_sale += grand

#             states = {
#                 'state': state.statename,
#                 'sale': grand,
#                 'order_count': ordercount,
#                 'percent': round((percent), 2)
#             }
#             statename.append(states)

#         return {"status_code": HTTP_200_OK, "total_orders": total_order, "total_sale": total_sale, "statewise_monthlysale": statename}

#     except Exception as e:
#         print(e)


# state wise monthly sale - Rahul
# @admin_app_order_router.get("/state_wise/monthlysale/{month}", dependencies=[Depends(JWTBearer())])
# async def sale(request: Request, month: str, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         totalorder = db.execute("SELECT count(*) AS total_count FROM (SELECT orders.id AS orders_id FROM orders INNER JOIN order_status ON order_status.order_id = orders.id  WHERE date_format(orders.created_at, '%Y-%m') =:param GROUP BY order_status.order_id  HAVING max(order_status.status) = 70 ORDER BY orders.id DESC) AS anon_1", {
#             "param": month
#         }).first()
#         total_orders = totalorder['total_count']

#         order_states = db.execute('SELECT DISTINCT pincodes.statename FROM orders LEFT JOIN buyer_address ON buyer_address.id = orders.address_id LEFT JOIN pincodes ON pincodes.pincode = buyer_address.pincode LEFT JOIN order_status ON order_status.order_id = orders.id WHERE DATE_FORMAT(orders.created_at, "%Y-%m") BETWEEN :param1 AND :param2 GROUP BY order_status.order_id HAVING MAX(order_status.status) = 70', {
#             "param1": month, "param2": month
#         }).all()

#         order_data = []
#         total_sale_amount = 0
#         for order_state in order_states:
#             orders = db.query(OrdersModel).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(PincodeModel, PincodeModel.pincode == ShippingAddressModel.pincode).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                 OrdersModel.created_at,  "%Y-%m") == month).filter(PincodeModel.statename == order_state.statename).group_by(OrdersModel.id).having(func.max(OrderStatusModel.status) == 70)

#             total_order_amount = 0
#             for order in orders.all():

#                 order = db.query(OrdersModel).filter(
#                     OrdersModel.id == order.id).first()

#                 total_amount = 0
#                 # Grand Total
#                 if(order.app_version == 'V4'):
#                     grand_total = order.grand_total
#                 else:
#                     grand_total = (
#                         float(order.grand_total) + float(order.delivery_charge)) - float(order.discount)

#                 total_order_amount += grand_total

#             total_sale_amount += total_order_amount

#             percent = ((orders.count() * 100) / total_orders)
#             ord = {
#                 'state': order_state.statename,
#                 'sale': total_order_amount,
#                 'order_count': orders.count(),
#                 'percent': round(percent, 2)
#             }

#             order_data.append(ord)
#         return {"status_code": HTTP_200_OK, "total_orders": total_orders, "total_sale": total_sale_amount, "statewise_monthlysale": order_data}

#     except Exception as e:
#         print(e)


# state wise yearly record
# @admin_app_order_router.get("/state_wise/yearly/sale/{year}", dependencies=[Depends(JWTBearer())])
# async def yearlysale(request: Request, year: str, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         statename = []

#         total_sale = 0
#         totalorder = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(
#             PincodeModel, PincodeModel.pincode == ShippingAddressModel.pincode).filter(func.date_format(
#                 OrdersModel.created_at, '%Y') == year).group_by(
#             OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).order_by(OrdersModel.id.desc()).count()

#         userstate = db.query(PincodeModel.statename).distinct().join(ShippingAddressModel, ShippingAddressModel.pincode == PincodeModel.pincode).join(
#             OrdersModel, OrdersModel.address_id == ShippingAddressModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                 OrdersModel.created_at, '%Y') == year).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).all()

#         total_order = 0

#         for state in userstate:
#             grand = 0
#             order = db.query(OrdersModel).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(PincodeModel, PincodeModel.pincode == ShippingAddressModel.pincode).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(PincodeModel.statename == state.statename).filter(func.date_format(
#                 OrdersModel.created_at, '%Y') == year).group_by(
#                 OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).order_by(OrdersModel.id.desc())

#             ordercount = order.count()
#             total_order += ordercount

#             orders = order.all()
#             # statename.append(orders)
#         # return statename
#             for order_value in orders:

#                 items = db.query(OrderItemsModel).filter(
#                     OrderItemsModel.order_id == order_value.id).all()

#                 total_amount = 0
#                 if(items is not None):

#                     # Latest Status

#                     for item in items:
#                         pricingdata = item.uuid.split('-')
#                         pricingdata = db.query(ProductPricingModel).where(ProductPricingModel.id == pricingdata[1]).where(
#                             ProductPricingModel.deleted_at.is_(None)).first()
#                         # Get Product Detail
#                         productdata = db.query(ProductModel).where(
#                             ProductModel.id == item.product_id).first()

#                         # Aseztak Service
#                         # aseztak_service = Services.aseztak_services(
#                         #     item.created_at, db=db)
#                         today_date = item.created_at.strftime(
#                             '%Y-%m-%d')
#                         aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#                         if(aseztak_service is None):

#                             # Calculate Product Price
#                             product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
#                                                                     gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order_value.app_version)

#                             total_amount += product_price * item.quantity
#                         else:
#                             product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=order_value.app_version, order_item_id=item.id)
#                             # Calculate Product Price
#                             # product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
#                             #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order_value.app_version)

#                             total_amount += product_price * item.quantity

#                     discount_amount = 0
#                     order_date = order_value.created_at.strftime("%Y-%m-%d")
#                     if(order_value.discount != 0):

#                         # CHECK ORDER DISCOUNT
#                         order_discount = db.query(OrderDiscountModel).filter(
#                             OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                         discount_rate = order_discount.discount
#                         if(order_value.discount_rate != 0):
#                             discount_rate = order_value.discount_rate

#                         discount_amount = orderDiscountCalculation(app_version=order_value.app_version,
#                                                                    order_amount=total_amount, discount_amount=order_value.discount, discount_rate=discount_rate)

#                     # Check Shipping Charge
#                     # delivery_charge = 0
#                     # if(order_value.delivery_charge != 0): CHANGES RAHUL
#                     shipping_charge = db.query(ShippingChargeModel).filter(
#                         ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#                     delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order_value.free_delivery, user_id=order_value.user_id, order_date=order_value.created_at,
#                                                                app_version=order_value.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order_value.payment_method)

#                     # Item Total Amount
#                     # order.item_total_amount = floatingValue(total_amount)

#                     # order.total_items = len(items)

#                     if(order_value.app_version == 'V4'):

#                         grand_total = roundOf(order_value.grand_total)
#                     else:
#                         grand_total = (
#                             total_amount + delivery_charge) - discount_amount

#                         grand_total = roundOf(grand_total)

#                         grand += grand_total

#             percent = ((ordercount * 100) / totalorder)
#             total_sale += grand
#             states = {
#                 'state': state.statename,
#                 'sale': grand,
#                 'order_count': ordercount,
#                 'percent': round((percent), 2)
#             }
#             statename.append(states)

#         return {"status_code": HTTP_200_OK, "total_orders": total_order, "total_sale": total_sale, "statewise_monthlysale": statename}

#     except Exception as e:
#         print(e)


# @admin_app_order_router.get("/state_wise/yearly/sale/{year}", dependencies=[Depends(JWTBearer())])
# async def yearlysale(request: Request, year: str, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         orders_list = []

#         total_sale = 0
#         totalorder = db.execute("SELECT count(*) AS total_count FROM (SELECT orders.id AS orders_id FROM orders INNER JOIN order_status ON order_status.order_id = orders.id  WHERE date_format(orders.created_at, '%Y') =:param GROUP BY order_status.order_id  HAVING max(order_status.status) = 70 ORDER BY orders.id DESC) AS anon_1", {
#             "param": year
#         }).first()

#         userstate = db.query(PincodeModel.statename).distinct().join(ShippingAddressModel, ShippingAddressModel.pincode == PincodeModel.pincode).join(
#             OrdersModel, OrdersModel.address_id == ShippingAddressModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                 OrdersModel.created_at, '%Y') == year).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).all()
#         total_order = 0

#         for state in userstate:
#             grand = 0
#             order_count = db.execute("SELECT count(*) AS order_count FROM (SELECT orders.id AS orders_id FROM orders INNER JOIN buyer_address ON buyer_address.id = orders.address_id INNER JOIN pincodes ON pincodes.pincode = buyer_address.pincode INNER JOIN order_status ON order_status.order_id = orders.id WHERE pincodes.statename =:param AND date_format(orders.created_at, '%Y') =:param1 GROUP BY order_status.order_id HAVING max(order_status.status) = 70 ORDER BY orders.id DESC) AS anon_1", {
#                 "param": state.statename,
#                 "param1": year
#             }).first()

#             order_all = db.execute("SELECT * FROM (SELECT orders.id AS orders_id FROM orders INNER JOIN buyer_address ON buyer_address.id = orders.address_id INNER JOIN pincodes ON pincodes.pincode = buyer_address.pincode INNER JOIN order_status ON order_status.order_id = orders.id WHERE pincodes.statename =:param AND date_format(orders.created_at, '%Y') =:param1 GROUP BY order_status.order_id HAVING max(order_status.status) = 70 ORDER BY orders.id DESC) AS anon_1", {
#                 "param": state.statename,
#                 "param1": year
#             }).all()

#             ordercount = order_count['order_count']
#             total_order += ordercount

#             for order_value in order_all:
#                 order_value = db.query(OrdersModel).filter(
#                     OrdersModel.id == order_value['orders_id']).first()
#                 if(order_value.app_version == 'V4'):
#                     grand += float(order_value.grand_total)
#                 else:
#                     grand += (float(order_value.grand_total) +
#                               float(order_value.delivery_charge))

#             percent = ((ordercount * 100) / totalorder['total_count'])

#             total_sale += grand

#             states = {
#                 'state': state.statename,
#                 'sale': grand,
#                 'order_count': ordercount,
#                 'percent': round((percent), 2)
#             }
#             orders_list.append(states)
#         return {"status_code": HTTP_200_OK, "total_orders": totalorder['total_count'], "total_sale": total_sale, "statewise_monthlysale": orders_list}

#     except Exception as e:
#         print(e)
