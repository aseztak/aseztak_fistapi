from app.api.helpers.admin_app_products import *
from fastapi import APIRouter
from app.resources.strings import *


admin_app_faqs_router = APIRouter()


# @admin_app_faqs_router.get("/", response_model=AdminFaqsDataListSchema, dependencies=[Depends(JWTBearer())])
# async def getfaqs(request: Request, page: int = 1, limit: int = 15, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data = db.query(FaqsModel).order_by(
#             FaqsModel.id.desc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK,  "faqs": [], "total_records": 0, "current_page": page, "total_page": 0}

#         faqs: FaqsModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         faq_data = []
#         for faq in faqs:

#             faq = {
#                 'id': faq.id,
#                 'flag': faq.flag.value,
#                 'question': faq.question,
#                 'answer': faq.answer,
#             }
#             faq_data.append(faq)

#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "faqs": faq_data, "total_records": total_records, "current_page": page, "total_page": total_pages}
#     except Exception as e:
#         print(e)


# @admin_app_faqs_router.post("/add", dependencies=[Depends(JWTBearer())])
# async def addpage(request: Request, data: AdminAddFaqsSchema, db: Session = Depends(get_db)):
#     try:

#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         dbfaq = FaqsModel(
#             flag=data.flag,
#             question=data.question,
#             answer=data.answer
#         )

#         db.add(dbfaq)
#         db.commit()
#         db.refresh(dbfaq)

#         return {"status_code": HTTP_202_ACCEPTED, "message": "Added Successfully"}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Input"}


# @admin_app_faqs_router.get("/edit/{id}", dependencies=[Depends(JWTBearer())])
# async def editfaq(request: Request, id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         faqs = db.query(FaqsModel).filter(FaqsModel.id == id).first()

#         if(faqs.flag.value == 'Seller'):
#             flag = 'Seller'

#         else:
#             flag = 'Buyer'

#         faq = {
#             'id': faqs.id,
#             'flag': flag,
#             'question': faqs.question,
#             'answer': faqs.answer
#         }

#         return {"status_code": HTTP_200_OK, "faq": faq}

#     except Exception as e:
#         print(e)


# @admin_app_faqs_router.post("/update", dependencies=[Depends(JWTBearer())])
# async def update(request: Request, data: AdminUpdateFaqsSchema, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         faq = db.query(FaqsModel).filter(FaqsModel.id == data.id).first()

#         faq.flag = data.flag
#         faq.question = data.question
#         faq.answer = data.answer

#         db.flush()
#         db.commit()

#         return {"status_code": HTTP_200_OK, "message": "Updated Successfully"}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}
