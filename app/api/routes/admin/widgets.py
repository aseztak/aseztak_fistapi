from fastapi import APIRouter

from app.resources.strings import *
admin_app_widgets_router = APIRouter()


# @admin_app_widgets_router.get("/{status}", response_model=AdminWidgetDataSchema, dependencies=[Depends(JWTBearer())])
# async def getwidgets(request: Request, status: int, page: int = 1, limit: int = 15, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         data = db.query(WidgetsModel).filter(WidgetsModel.slno != 0).filter(
#             WidgetsModel.enabled == status).order_by(WidgetsModel.slno.asc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK, "widgets": [], "total_records": 0, "current_page": page, "total_pages": 0}

#         widgets: WidgetsModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         widget_data = []

#         if(len(widgets) > 0):
#             for widget in widgets:

#                 images = db.query(WidgetImagesModel).filter(
#                     WidgetImagesModel.widget_id == widget.id).all()

#                 image_data = []
#                 for image in images:
#                     img = {
#                         'image': image.image,

#                     }

#                     image_data.append(img)

#                 wd = {
#                     'id': widget.id,
#                     'slno': widget.slno,
#                     'widget_name': widget.widget_name,
#                     'title': widget.title,
#                     'description': widget.description,
#                     'enabled': widget.enabled,
#                     'no_of_images': widget.no_of_images,
#                     'images': image_data
#                 }

#                 widget_data.append(wd)
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1
#         return {"status_code": HTTP_200_OK, "widgets": widget_data, "total_records": total_records, "current_page": page, "total_pages": total_pages}

#     except Exception as e:
#         print(e)


# # add widgets
# @admin_app_widgets_router.post("/add", dependencies=[Depends(JWTBearer())])
# async def widgets(request: Request, data: AdminWidgetSchema, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         dbwidgets = WidgetsModel(
#             slno=data.slno,
#             title=data.title,
#             widget_name=data.title.replace(" ", "_").lower(),
#             description=data.description,
#             widget_type=data.widget_type,
#             no_of_images=data.no_of_images,
#             enabled=data.enabled,
#             created_at=datetime.now(),
#             updated_at=datetime.now()

#         )
#         db.add(dbwidgets)
#         db.commit()
#         db.refresh(dbwidgets)

#         return {"status_code": HTTP_200_OK, "message": "Added successfully..."}
#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Input"}


# # edit
# @admin_app_widgets_router.get("/edit/{widget_id}", dependencies=[Depends(JWTBearer())])
# async def editwidget(request: Request, widget_id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         widget = db.query(WidgetsModel).filter(
#             WidgetsModel.id == widget_id).first()

#         images = db.query(WidgetImagesModel).filter(
#             WidgetImagesModel.widget_id == widget.id).all()

#         image_data = []
#         for image in images:

#             img = {
#                 'id': image.id,
#                 'image': image.image,
#                 'link': image.go_to

#             }

#             image_data.append(img)
#         # return image_data
#         if(widget.widget_type.value == 'HOME'):
#             widget_type = 'HOME'

#         else:
#             widget_type = 'INNER'
#         wd = {
#             'id': widget.id,
#             'title': widget.title,
#             'widget_type': widget_type,
#             'description': widget.description,
#             'no_of_images': widget.no_of_images,
#             'images': image_data,
#             'slno': widget.slno,

#         }

#         return {"status_code": HTTP_200_OK, "widget": wd}

#     except Exception as e:
#         print(e)


# # update
# @admin_app_widgets_router.post("/update", dependencies=[Depends(JWTBearer())])
# async def updatewidgets(request: Request, data: AdminUpdateWidgetSchema, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         widget = db.query(WidgetsModel).filter(
#             WidgetsModel.id == data.id).first()

#         widget.title = data.title
#         widget.description = data.description
#         widget.slno = data.slno
#         widget.widget_name = data.title.replace(" ", "_").lower()
#         widget.widget_type = data.widget_type
#         widget.no_of_images = data.no_of_images
#         widget.enabled = data.enabled
#         widget.created_at = datetime.now()
#         widget.updated_at = datetime.now()

#         db.flush()
#         db.commit()

#         return {"status_code": HTTP_200_OK, "message": "Updated Successfully"}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}


# # add images
# @admin_app_widgets_router.get("/images/{widget_id}", dependencies=[Depends(JWTBearer())])
# async def imagewidget(request: Request, widget_id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         widget = db.query(WidgetsModel).filter(
#             WidgetsModel.id == widget_id).first()

#         data = db.query(WidgetImagesModel).filter(
#             WidgetImagesModel.widget_id == widget_id).all()

#         upload_image = True
#         if(widget.no_of_images == len(data)):
#             upload_image = False
#         image_data = []
#         for image in data:

#             img = {
#                 'id': image.id,
#                 'image': image.image,
#                 'link': image.go_to,

#             }

#             image_data.append(img)

#         return {"status_code": HTTP_200_OK, "images": image_data, "upload_image": upload_image}
#     except Exception as e:
#         print(e)


# @admin_app_widgets_router.post("/add_images/{widget_id}", dependencies=[Depends(JWTBearer())])
# async def imageupload(request: Request, widget_id: int, go_to: str = Form(...), go_to_id: int = Form(...), images: Optional[UploadFile] = File(None), db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         widget = db.query(WidgetsModel).filter(
#             WidgetsModel.id == widget_id).first()

#         if(images != None):
#             file = images.filename.split(".")
#             extension = file[1]
#             uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
#                 datetime.now().strftime('%Y-%d-%M-%h-%s')

#             filename = f"{uniqueID}.{file[1]}"

#             # Set Path Of image
#             file_name = getcwd()+"/app/static/widgets/"+filename
#             # Upload image in path
#             with open(file_name, 'wb+') as f:
#                 f.write(images.file.read())
#                 f.close()

#             uploaded_file = getcwd()+"/app/static/widgets/" + str(filename)
#             filename = str(filename)
#             # Upload Photo
#             MediaHelper.uploadWidgetsPhoto(
#                 uploaded_file=uploaded_file, file_name=filename)

#             # Remove Original Image from Path

#             unlink(uploaded_file)

#             file_path = str(
#                 linode_obj_config['endpoint_url'])+'/widgets/'+str(filename)

#             dbmedia = WidgetImagesModel(
#                 widget_id=widget.id,
#                 image=file_path,
#                 go_to=go_to,
#                 go_to_id=go_to_id,
#                 created_at=datetime.now(),
#                 updated_at=datetime.now()
#             )
#             db.add(dbmedia)
#             db.commit()
#             db.refresh(dbmedia)

#         return {"status_code": HTTP_200_OK, "message": "Added successfully..."}
#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Input"}


# # status change
# @admin_app_widgets_router.get("/status/{widget_id}", dependencies=[Depends(JWTBearer())])
# async def widgetenabled(request: Request, widget_id: int, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         widget = db.query(WidgetsModel).filter(
#             WidgetsModel.id == widget_id).first()

#         if(widget.enabled == 1):
#             widget.enabled = 0
#             db.flush()
#             db.commit()

#         else:
#             widget.enabled = 1
#             db.flush()
#             db.commit()

#         return {"status_code": HTTP_200_OK, "message": "Widget Status Changed..."}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified..."}


# # delete
# @admin_app_widgets_router.delete("/delete", dependencies=[Depends(JWTBearer())])
# async def deletewidgets(request: Request, data: AdminWidgetDeleteSchema, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         db.query(WidgetsModel).filter(
#             WidgetsModel.id == data.id).delete()

#         db.commit()

#         return {"status_code": HTTP_200_OK, "message": "Deleted Successfully..."}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified..."}
