from app.api.helpers.admin_app_products import *
from fastapi import APIRouter
from app.resources.strings import *


admin_app_page_router = APIRouter()


# @admin_app_page_router.get("/", response_model=AdminPageDataListSchema, dependencies=[Depends(JWTBearer())])
# async def getpage(db: Session = Depends(get_db), page: int = 1, limit: int = 15):
#     try:
#         data = db.query(PageModel).order_by(
#             PageModel.id.desc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK,  "pages": [], "total_records": 0, "current_page": page, "total_page": 0}

#         pages: PageModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         page_list = []
#         for pagelist in pages:

#             pagelist = {
#                 'id': pagelist.id,
#                 'name': pagelist.name,
#                 'slug': pagelist.slug,
#                 'content': pagelist.content,
#                 'created_at': pagelist.created_at,
#                 'updated_at': pagelist.updated_at
#             }
#             page_list.append(pagelist)

#          # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_202_ACCEPTED,  "pages": page_list, "total_records": total_records, "current_page": page, "total_page": total_pages}

#     except Exception as e:
#         print(e)


# # add
# @admin_app_page_router.post("/add", dependencies=[Depends(JWTBearer())])
# async def addpage(request: Request, data: AdminAddPageSchema, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         dbpage = PageModel(
#             name=data.name,
#             slug=data.name.replace(" ", "-").lower(),
#             content=data.content,
#             is_menu=data.is_menu,
#             created_at=datetime.now(),
#             updated_at=datetime.now()
#         )
#         db.add(dbpage)
#         db.commit()
#         db.refresh(dbpage)

#         return {"status_code": HTTP_202_ACCEPTED, "message": "Added Successfully"}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Input"}


# edit
# @admin_app_page_router.get("/edit/{page_id}", dependencies=[Depends(JWTBearer())])
# async def editpages(page_id: int, db: Session = Depends(get_db)):
#     try:
#         page = db.query(PageModel).filter(
#             PageModel.id == page_id).first()

#         return {"status_code": HTTP_200_OK, "page": page}

#     except Exception as e:
#         print(e)


# @admin_app_page_router.post("/update", dependencies=[Depends(JWTBearer())])
# async def updatepage(request: Request, data: AdminUpdatePageSchema, db: Session = Depends(get_db)):
#     try:
#         # Check Not Admin
#         checkNotadmin = CheckUser.checkAdmin(request=request, db=db)

#         if(checkNotadmin == False):
#             return {"status_code": HTTP_304_NOT_MODIFIED}

#         page = db.query(PageModel).filter(
#             PageModel.id == data.id).first()

#         page.name = data.name
#         page.slug = data.name.replace(" ", "-").lower()
#         page.content = data.content

#         db.flush()
#         db.commit()

#         return {"status_code": HTTP_200_OK, "message": "Updated Successfully"}

#     except Exception as e:
#         return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}
