from app.api.helpers.tags import get_tags, search_tags
from app.db.schemas.tags_schema import TagsDataSchema
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.users import *
from starlette.status import HTTP_202_ACCEPTED, HTTP_404_NOT_FOUND
from fastapi import APIRouter,  Depends


tags_router = APIRouter()


# list of default tags


@tags_router.get("/default", response_model=TagsDataSchema, dependencies=[Depends(JWTBearer())])
async def default(db: Session = Depends(get_db)):

    try:
        tags = await get_tags(db=db)
        return {"status_code": HTTP_202_ACCEPTED, 'tags': tags}

    except Exception as e:
        return {"status_code": HTTP_404_NOT_FOUND}

# Tag search result


@tags_router.get("/search", response_model=TagsDataSchema, dependencies=[Depends(JWTBearer())])
async def search(nameLike: str, db: Session = Depends(get_db)):

    try:
        tags = await search_tags(nameLike=nameLike,  db=db)
        return {"tags": tags}

    except Exception as e:
        print(e)
