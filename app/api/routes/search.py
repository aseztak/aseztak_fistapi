# from app.db.models.product_tags import ProductTagModel
# from app.api.helpers.services import AsezServices
# search_router = APIRouter()


#CLOSED 2024-01-05
# Search Products
# @search_router.get("/{tag_id}", response_model=ProductData, dependencies=[Depends(JWTBearer())])
# async def searchProducts(request: Request, tag_id: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:
#         userdata = auth(request=request)

#         tag: TagModel = await get_tag(db=db, tag_id=tag_id)

#         # Title
#         title = tag.name

#         data: ProductModel = await ProductsHelper.getProductsbytags(db=db, tag_id=tag.id)

#         product_count = data.count()
#         if(product_count == 0):
#             return {"status_code": HTTP_404_NOT_FOUND}
#         else:

#             productdata: ProductModel = data.limit(
#                 limit=limit).offset((page - 1) * limit).all()

#             # calculating Price
#             # Aseztak Service
#             today = date.today()
#             # aseztak_service = Services.aseztak_services(
#             #     today, db=db)
#             today_date = today.strftime(
#                 '%Y-%m-%d')
#             aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#             for product in productdata:

#                 # Check Pricing
#                 pricing = product.product_pricing[0]
#                 if(pricing):
#                     product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=aseztak_service, app_version='V4')
#                     # product_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
#                     #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

#                    # Restructure Price data
#                     price_data = {
#                         'id': pricing.id,
#                         'price': floatingValue(product_price),
#                         'moq': pricing.moq,
#                         'unit': pricing.unit
#                     }
#                     product.pricing = price_data

#                  # New Changes (TINA)
#                 start_time = date.today() - timedelta(days=14)
#                 start_date = start_time.strftime("%Y-%m-%d")
#                 product_date = product.created_at.strftime("%Y-%m-%d")
#                 is_new_arrival = False
#                 if (product_date >= start_date):
#                     is_new_arrival = True

#                 product.is_new_arrival = is_new_arrival

#                 # Check Best Selling and Treding Product
#                 check_best_selling = db.query(TagModel).join(ProductTagModel, ProductTagModel.tag_id == TagModel.id).filter(
#                     ProductTagModel.product_id == product.id).filter(TagModel.name == 'Best Selling').first()

#                 if (check_best_selling is not None):
#                     product.is_best_selling = True

#                 check_trending = db.query(TagModel).join(ProductTagModel, ProductTagModel.tag_id == TagModel.id).filter(
#                     ProductTagModel.product_id == product.id).filter(TagModel.name == 'Trending').first()

#                 if (check_trending is not None):
#                     product.is_trending = True
#                 # NEW Change (TINA)
#                 # Favourite
#                 product.favourite = False

#                 # Product Image
#                 productimage = product.images[0]

#                 product.image = ''
#                 if(productimage):
#                     product.image = productimage.file_path

#             # Total Records
#             total_records = product_count

#             # Number of Total Pages
#             total_pages = str(round((total_records/limit), 2))

#             total_pages = total_pages.split('.')

#             if(total_pages[1] != "0"):
#                 total_pages = int(total_pages[0]) + 1
#             else:
#                 total_pages = int(total_pages[0])

#             return {"status_code": HTTP_202_ACCEPTED, "title": title, "products": productdata, "total_products": total_records, "current_page": page, "total_pages": round(total_pages, 0)}

#     except Exception as e:
#         print(e)


# @search_router.get("/filter-attributes/{tag_id}", dependencies=[Depends(JWTBearer())])
# async def filterAttributes(tag_id: int, db: Session = Depends(get_db)):
#     try:

#         productPrice: ProductModel = await ProductsHelper.productMinMaxPircebyTag(db=db, tag_id=tag_id)

#         pricing = {
#             'min_price': round(productPrice.min_price),
#             'max_price': round(productPrice.max_price)
#         }

#         data: ProductModel = await ProductsHelper.filterAttributesbyTags(db=db, tag_id=tag_id)

#         if(data == False):
#             return {"status_code": HTTP_404_NOT_FOUND}

#         return {"status_code": HTTP_202_ACCEPTED, "pricing": pricing, "filter_attributes": data}

#     except Exception as e:
#         print(e)


# @search_router.post("/filter-search", response_model=ProductData, dependencies=[Depends(JWTBearer())])
# async def filterSearchProducts(request: Request, filterdata: FiltertagDataSchema, db: Session = Depends(get_db)):
#     try:
#         title = 'Filter Search'
#         userdata = auth(request=request)

#         if(filterdata.min_price == 0 and filterdata.max_price == 0):
#             # Get Min and Max Price
#             productPrice: ProductModel = await ProductsHelper.productMinMaxPircebyTag(db=db, tag_id=filterdata.id)

#             filterdata.min_price = round(productPrice.min_price)
#             filterdata.max_price = round(productPrice.max_price)

#         data: ProductModel = await ProductsHelper.filerSearchProductsbyTags(db=db, tag_id=filterdata.id, sort=filterdata.sort, min_price=filterdata.min_price, max_price=filterdata.max_price, attributes=filterdata.attributes)

#         if(data.count() == 0):
#             return {"status_code": HTTP_404_NOT_FOUND}

#         else:

#             productdata: ProductModel = data.limit(
#                 limit=filterdata.limit).offset((filterdata.page - 1) * filterdata.limit).all()

#             # calculating Price
#             products: ProductModel = await Calculations.productPrice(db=db, data=productdata)

#             # Check product favourite
#             products: ProductModel = await ProductsHelper.isFavourite(products, user_id=userdata['id'], db=db)

#             # Check product Stock
#             products: ProductModel = await ProductsHelper.isStock(products)

#             # Custome Static Text
#             products: ProductModel = await ProductsHelper.staticText(db=db, data=products)

#             # Total Records
#             total_records = data.count()

#             # Number of Total Pages
#             total_pages = round((total_records/filterdata.limit), 0)

#             # Check total count Similarity of records
#             check = (total_pages * filterdata.limit)
#             if(check == total_records):
#                 pass
#             else:
#                 total_pages = total_pages + 1

#             return {"status_code": HTTP_202_ACCEPTED, "title": title, "products": products, "total_products": total_records, "current_page": filterdata.page, "total_pages": round(total_pages, 0)}

#         return

#     except Exception as e:
#         print(e)
# # Filter Attributes by tags
