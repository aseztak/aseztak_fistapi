from starlette.requests import Request
from app.services.auth_bearer import JWTBearer
from app.db.config import  get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.users import *
from app.services.auth import auth

from fastapi import APIRouter,  Depends, Query
from app.services.auth_handler import decodeJWT

users_router = APIRouter()



@users_router.get("/", dependencies=[Depends(JWTBearer())])
async def getUser(request: Request, db: Session = Depends(get_db)):
    try:
        # Check Admin
        userdata = auth(request=request)
        user = db.execute("SELECT * FROM users WHERE id=:user_id",{
            "user_id": userdata['id']
        }).first()
        profile = db.execute("SELECT * FROM profiles WHERE user_id=:user_id",{
            "user_id": user.id
        }).first()
        user = {
            'id': user.id,
            'name': user.name,
            'mobile': user.mobile,
            'email': user.email,
            'proof': profile.proof,
            'proof_type': str(profile.proof_type)
        }
        return user
    except Exception as e:
        print(e)


@users_router.post("/status")
async def UserStatus(token: str = Query(None), db: Session = Depends(get_db)):
    try:
        userdata = decodeJWT(token)
        user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        if(user is not None):
            message = ''
            if(user.status == 90):
                message = 'Your account has been suspended'
            if(user.status == 98):
                message = "Your account has been deleted"    
            return {"status": user.status, "message": message}
        else:
            return {"status": 404}

    except Exception as e:
        return {"status": 404}
