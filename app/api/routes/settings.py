from app.db.config import get_db
from sqlalchemy.orm.session import Session

from fastapi import APIRouter,  Depends

from app.db.models.settings import SettingsModel
settings_router = APIRouter()


@settings_router.get("/settings")
def read_settings(db: Session = Depends(get_db)):

    try:
        settings = db.query(SettingsModel).all()
        return {"data": settings}

    except Exception as e:
        print(e)


@settings_router.get("/get/{key}")
async def settings(key: str, db: Session = Depends(get_db)):
    try:

        setting = db.query(SettingsModel).filter(
            SettingsModel.key == key).first()

        if(setting.value is not None):
            value = setting.value

        else:
            value = ""

        value = value

        return {"value": value}

    except Exception as e:
        print(e)
