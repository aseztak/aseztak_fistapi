
from starlette.status import  HTTP_200_OK
from app.db.models.products import FavouriteModel
from app.api.helpers.calculation import *
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from starlette.requests import Request

from fastapi import APIRouter,  Depends

from app.services.auth import auth
from app.db.schemas.wishlist_schema import DeleteWishList

wishlist_router = APIRouter()


@wishlist_router.post("/add/{product_id}", dependencies=[Depends(JWTBearer())])
async def create_wishlist(request: Request, product_id: int, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)
        checkWishlist = db.query(FavouriteModel).filter(FavouriteModel.user_id == userdata['id']).filter(
            FavouriteModel.product_id == product_id).first()

        if(checkWishlist is None):

            dbwishlist = FavouriteModel(
                user_id=userdata['id'], product_id=product_id)

            db.add(dbwishlist)
            db.commit()
            db.refresh(dbwishlist)

            wish = True

        else:
            dbwishlist = db.query(FavouriteModel).filter(FavouriteModel.user_id == userdata['id']).filter(
                FavouriteModel.product_id == product_id).delete()
            db.commit()

            wish = False

        return {"status_code": HTTP_200_OK, "message": "Success", "wish": wish}
    except Exception as e:
        return {"status_code": HTTP_200_OK, "message": "Invalid Input"}


# Delete Wish List
@wishlist_router.delete("/delete", dependencies=[Depends(JWTBearer())])
async def deleteWishlist(request: Request, data: DeleteWishList, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        # check data
        checkdata = db.query(FavouriteModel).filter(
            FavouriteModel.user_id == userdata['id']).filter(FavouriteModel.id == data.id).first()

        if(checkdata is not None):

            db.query(FavouriteModel).filter(FavouriteModel.user_id ==
                                            userdata['id']).filter(FavouriteModel.id == data.id).delete()
            db.commit()

            return {"status_code": HTTP_200_OK, "message": "Success"}
        else:
            return {"status_code": HTTP_200_OK, "message": "Invalid Input"}

    except Exception as e:

        return {"status_code": HTTP_200_OK, "message": "Invalid Input"}
