
from datetime import datetime
from fastapi import APIRouter,  Depends
from app.db.config import  get_db
from fastapi import APIRouter,  Depends, status, Request
from sqlalchemy.orm.session import Session

from app.api.helpers.auth import *
from app.db.schemas.auth_schema import *
from datetime import datetime
from app.services.auth_handler import signJWT
from app.services.auth import auth
from app.resources.strings import *
from app.api.helpers.users import *
import requests
from app.services.auth_bearer import JWTBearer
from app.db.models.orders import OrdersModel
# from fastapi import Header, HTTPException
# from ratelimit import limits, sleep_and_retry
# from starlette.status import HTTP_429_TOO_MANY_REQUESTS

#CLOSED ON 2024-06-19
#SlowAPI
# from slowapi import Limiter
# def customheader(request: Request):
#     return request.headers.get('X-Identification-Id')

# limiter = Limiter(key_func=customheader)


# import socket
v4_auth_router = APIRouter()


@v4_auth_router.post("/send_otp")
# @limiter.limit("5/day")
async def send_otp(request: Request, data: SendOtpSchema, db: Session = Depends(get_db)):

    mobile = data.mobile

    # Check Buyer
    checkbuyer = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id ==
                                          UserModel.id).where(UserRoleModel.role_id == 5).filter(UserModel.mobile == mobile).filter(UserModel.deleted_at.is_(None)).first()

    # Check User
    checkUser = db.query(UserModel).filter(UserModel.mobile == mobile).filter(
        UserModel.deleted_at.is_(None)).first()

    if(checkUser is not None and checkbuyer is None):
        return {"status_code": status.HTTP_200_OK, "user_exist": True, "is_seller": True, "is_buyer": False, "message": "Sorry! Your account has been registered as Seller"}

    otp = random.randint(746008, 999999)

    signature = ''

    extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
        AUTHKEY+'&template_id='+TEMPLATE_ID + \
        '&mobile='+str(91)+mobile+'&otp='+str(otp)

    sendurl = OTP_SEND_URL+extra_param

    #Send OTP to WhatsApp
    await sendotpwhatsapp(mobile=data.mobile, otp=otp)

    try:

        if(mobile != '9898989898'):
            # Update Otp User -- SUBHA -- rahul

            await userOtpUpdate(db=db, mobile=mobile, otp=otp)
            requests.get(sendurl)

        return {'signature': signature, "user_exist": True, "is_buyer": True, "is_seller": False, "message": "Otp Sent", 'otp_sent_at': datetime.now(), 'otp_expires': 60}

    except Exception as e:
        print(e)


# Resend OTP
@v4_auth_router.post("/resend-otp")
# @limiter.limit("5/day")
async def resend_otp(request: Request, data: SendOtpSchema, db: Session = Depends(get_db)):
    try:
        mobile = data.mobile

        # Check User
        checkUser = db.query(UserModel).filter(UserModel.mobile == mobile).filter(
            UserModel.deleted_at.is_(None)).first()

        if(checkUser is not None):
            otp = random.randint(746008, 999999)

            signature = ''

            extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
                AUTHKEY+'&template_id='+TEMPLATE_ID + \
                '&mobile='+str(91)+mobile+'&otp='+str(otp)

            sendurl = OTP_SEND_URL+extra_param
            requests.get(sendurl)

            #Send OTP to WhatsApp
            await sendotpwhatsapp(mobile=data.mobile, otp=otp)

            checkUser.otp = otp
            db.flush()
            db.commit()

        return {"status_code": HTTP_200_OK, "message": "Otp Sent"}
    except Exception as e:
        print(e)




# set user fcm_token
@v4_auth_router.post("/set_fcm_token", dependencies=[Depends(JWTBearer())])
async def Userfcm_token(request: Request, data: Userfcm_tokenSchema, db: Session = Depends(get_db)):

    try:
        userdata = auth(request=request)

        user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        user.fcm_token = data.fcm_token

        db.flush()
        db.commit()

        if(user.fcm_token != ""):

            return True

        else:

            return False

    except Exception as e:
        print(e)

    # # Resnd OTP
    # @auth_router.post("/resend_otp", )
    # def resend_otp(mobile: str, db: SessionLocal = Depends(get_db)):
    #     pass

    # # Expired OTP

    # @auth_router.post("/token_expired", )
    # def token_expired(mobile: str, db: SessionLocal = Depends(get_db)):
    #     pass

    # # Logout

    # @auth_router.post("/logout",)
    # def logout(db: SessionLocal = Depends(get_db)):
    #     pass




#Check new user
@v4_auth_router.post("/check/new/user")
# @limiter.limit("5/day")
async def checknewuser(request: Request, data: NewLogin, db: Session = Depends(get_db)):
    try:
        user = db.query(UserModel).filter(UserModel.mobile == data.mobile).first()

        #Document Proof
        document_type = {
            'AADHAAR',
            'PAN',
            'GSTIN',
            'TRADE LICENSE',
            'UDYOG AADHAAR'
        }
        if(user):
            if(user.role.role_id == 6):
                data = {
                    'status_code': HTTP_200_OK,
                    'is_user': False,
                    'is_register': True,
                    'document_type': document_type,
                    'message': 'Sorry! your account has been registered as a seller',
                }
            elif(user.status == 90):
                data = {
                    'status_code': HTTP_200_OK,
                    'is_user': False,
                    'is_register': True,
                    'document_type': document_type,
                    'message': 'Your account has been suspended',
                }
            elif(user.status == 98):
                data = {
                    'status_code': HTTP_200_OK,
                    'is_user': False,
                    'is_register': True,
                    'document_type': document_type,
                    'message': 'Your account has been deleted',
                }    
            else:
                #Insert Bot Data
                ipaddress = request.headers.get('X-Identification-Id')
                if(ipaddress is None):
                    ipaddress = request.client.host
                await insert_bot_data(db=db, user_agent=ipaddress, mobile=data.mobile)

                # Send and Update OTP Message
                if(data.mobile != 9898989898):
                    otp = random.randint(746008, 999999)
                    user.otp = otp
                    db.flush()
                    db.commit()
                    sendotp(otp=otp, mobile=data.mobile, signature='')
                data = {
                    'status_code': HTTP_200_OK,
                    'is_user': True,
                    'is_register': True,
                    'document_type': document_type,
                    'message': 'Otp sent to your mobile',
                }   
        else:
            data = {
                'status_code': HTTP_200_OK,
                'is_user': False,
                'is_register': False,
                'document_type': document_type,
                'message': 'You are not registered with us. Please sign up',
            }

        return data
    except Exception as e:
        print(e)


#Register New User
@v4_auth_router.post("/signup/new/account")
# @limiter.limit("5/day")
async def signupnewaccount(request: Request, data: RegisterSchema, db: Session = Depends(get_db)):
    try:

        #check user
        checkuserexist = db.query(UserModel).filter(UserModel.mobile == data.mobile).first()
        if(checkuserexist is None):

            otp = random.randint(746008, 999999)

            await create_user(data=data, otp=otp, db=db)

            #Insert Bot Data
            ipaddress = request.headers.get('X-Identification-Id')
            if(ipaddress is None):
                ipaddress = request.client.host
            await insert_bot_data(db=db, user_agent=ipaddress, mobile=data.mobile)

            # send OTP Message
            sendotp(otp=otp, mobile=data.mobile, signature='')

            data = {
                'status_code': HTTP_200_OK,
                'is_user': True,
                'message': 'Otp sent to your mobile',
                'signature': data.signature,
                'otp_sent_at': datetime.now(),
                'otp_expires': 60
            }
        else:
            data = {
                'status_code': HTTP_200_OK,
                'is_user': True,
                'message': 'User Exist',
                'signature': '',
                'otp_sent_at': datetime.now(),
                'otp_expires': 0
            }
        return data
    except Exception as e:
        print(e)

# new Registration Login
# @v4_auth_router.post("/new/register")
# async def register(request: Request, data: NewLogin, db: Session = Depends(get_db), user_agent: str = Header('http://api.aseztak.in/api/v4/auth/new/register')):
#     try:
#         # today = date.today()
#         # check_data = db.execute("SELECT COUNT(*) AS total_count FROM test WHERE test.mobile =:mobile AND DATE_FORMAT(test.created_at, '%Y-%m-%d') =:today",{
#         #     "mobile": data.mobile,
#         #     "today": today
#         # }).first()
#         # if(check_data.total_count >= 5):
#         #     return {
#         #         'status_code': HTTP_200_OK,
#         #         'is_user': False,
#         #         'message': 'Limit Exceeded',
#         #     }

#         # ipaddress = request.headers.get('X-Request-Ip')
#         # if(ipaddress is None):
#         #     ipaddress = request.client.host
#         # #NEW CHANGES
#         # await insert_bot_data(db=db, user_agent=ipaddress, mobile=data.mobile)
#         ipaddress = request.client.host
#         if(ipaddress != '172.105.62.141'):
#             return create_new_user(user=data, db=db)
#     except Exception as e:
#         print(e)

# Mobile Login - with Mobile no & OTP
@v4_auth_router.post("/new/login")
def user_login(data: VerifyOtpV4, db: Session = Depends(get_db)):

    try:

        user = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).filter(UserModel.mobile ==
                                                                                                                                         data.mobile).filter(UserModel.otp == data.otp).filter(UserModel.deleted_at.is_(None)).first()

        if(user is None):
            return {'status_code': status.HTTP_200_OK, 'error': True, 'user_status': False, 'message': 'Invalid Otp'}

        if(user and user.status == 90):
            return {'status_code': status.HTTP_200_OK, 'error': True, 'user_status': False, "message": 'Your account has been suspended'}

        document_type = {
            'AADHAAR',
            'PAN',
            'GSTIN',
            'TRADE LICENSE',
            'UDYOG AADHAAR'
        }
        if user:

            #App Version
            app_version = db.execute("SELECT * FROM settings WHERE settings.key = 'buyer_app_build_no_android'").first()
            # Store IP Address
            user.last_login_at = datetime.now()
            user.last_login_ip = data.ip
            # user.platform = data.platform
            # user.platform = 'android-655'
            user.platform = str('android-')+str(app_version.value)
            user.fcm_token = data.fcm_token

            if(user.mobile != '9898989898'):
                user.otp = None

            db.flush()
            db.commit()

            #Check Profile Exist
            profile = user.profile

            if(profile):
                is_profile = True
                profile = profile
            else:
                is_profile = False
                profile = {}

            user = user

            token = signJWT(user)

            if(is_profile == True):

                userdata = {
                    "id": user.id,
                    "name": user.name,
                    "email": user.email,
                    "mobile": user.mobile,
                    "country": user.country,
                    "region": user.region,
                    "pincode": user.pincode,
                    "city": user.city,
                    "proof": profile.proof,
                    "proof_type": profile.proof_type
                }
            else:

                userdata = {
                    "id": user.id,
                    "name": user.name,
                    "email": user.email,
                    "mobile": user.mobile,
                    "country": user.country,
                    "region": user.region,
                    "pincode": user.pincode,
                    "city": user.city,
                    "proof": '',
                    "proof_type": ''
                }

            # Check Order Count and Send Notification
            check_order_count = db.query(OrdersModel).filter(
                OrdersModel.user_id == user.id).count()
            if(check_order_count == 0):
                payload = {
                    "notification": {
                        "body": 'We have a perfect offer for you! Grab Free Delivery & Massive Discounts on your first purchase',
                        "title": 'Welcome to the Wholesale World',
                        "image": '/'
                    },
                    "priority": "high",
                    "data": {
                        "id": "1",
                        "page": '/home',
                        "image": '/'
                    },
                    "to": user.fcm_token,
                    "content_availaable": True,
                    "apns-priority": 5,
                }

                url = 'https://fcm.googleapis.com/fcm/send'
                fcm_token = FCM_TOKEN

                headers = {
                    "Authorization": fcm_token[0],
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                }

                requests.request(
                    "POST", url, json=payload, headers=headers)

            return {'status_code': status.HTTP_200_OK, 'error': False, 'user_status': True, 'message': 'Success',  'token': token['access_token'], 'profile_exist': is_profile, 'user': userdata, 'document_type': document_type}

            # return token['access_token']
            # return {'status_code': status.HTTP_202_ACCEPTED, 'error': False, 'token': token['access_token'], 'profile_exist': is_profile, 'user': user}

        else:
            return {'status_code': status.HTTP_404_NOT_FOUND, 'error':  True, 'token': '', 'data': {}}

    except Exception as e:
        return e
