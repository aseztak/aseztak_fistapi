from fastapi import APIRouter,  Depends, Request
from sqlalchemy.orm.session import Session
from app.api.helpers.auth import *
from app.api.helpers.fulfillment import FulfillmentHelper

from app.db.config import get_db
from app.db.models.fulfillment_items import FulFillmentItemsModel
from app.db.models.fulfillment_order_items import FulFillmentOrdersItemsModel
from app.db.models.fulfillment_orders import FulFillmentOrdersModel

from app.db.schemas.auth_schema import *
from app.db.schemas.fulfillmentschema import FulfillmentItemsSchema, FulfillmentOrdersSchema, SaveFulfillmentOrdersSchema

from datetime import datetime
from app.services.auth import auth
from app.resources.strings import *
from app.api.helpers.users import *
from app.services.auth_bearer import JWTBearer
from starlette.status import HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED, HTTP_404_NOT_FOUND, HTTP_200_OK
import uuid
from starlette.requests import Request
from paytmchecksum import PaytmChecksum
import json
from app.core import config


v4_seller_fulfillment_router = APIRouter()


# Get FulFillmentItems
@v4_seller_fulfillment_router.get("/items", response_model=FulfillmentItemsSchema, dependencies=[Depends(JWTBearer())])
async def getfulfillmentItems(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        data: FulFillmentItemsModel = await FulfillmentHelper.getFulfillItems(db=db, page=page, limit=limit)

        if(data.count() == 0):
            return {"status_code": HTTP_404_NOT_FOUND, "total_items": 0, "items": [], "current_page": page, "total_pages": 0}

        items: FulFillmentItemsModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        fulfillmentitems = []
        for fulfillment in items:
            item = {
                'id': fulfillment.id,
                'title': fulfillment.title,
                'description': fulfillment.description,
                'price': fulfillment.price,
                'delivery': fulfillment.delivery,
                'quantity': 0,
                'total': 0,
                'image': fulfillment.image
            }

            fulfillmentitems.append(item)

        # Count Total Items
        total_records = data.count()

        # Number of Total Pages
        total_pages = round((total_records/limit), 0)

        # Check total count Similarity of records
        check = (total_pages * limit)

        if(check == total_records):
            pass
        else:
            total_pages = total_pages + 1

        return {"status_code": HTTP_202_ACCEPTED, "total_items": total_records, "items": fulfillmentitems, "current_page": page, "total_pages": total_pages}

    except Exception as e:
        return {"status_code": HTTP_404_NOT_FOUND, "total_items": 0, "items": [], "current_page": page,  "total_pages": 0}


# Order Checkout
@v4_seller_fulfillment_router.post("/items/checkout", dependencies=[Depends(JWTBearer())])
async def checkOutItems(request: Request, data: SaveFulfillmentOrdersSchema, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        paytmParams = dict()
        paytmParams["body"] = {
            "mid": config.PAYTM_MID_KEY,
            "orderId": data.payment_order_id,
        }

        checksum = PaytmChecksum.generateSignature(
            json.dumps(paytmParams["body"]), config.PAYTM_SECRET_KEY)
        paytmParams["head"] = {
            "signature": checksum
        }

        post_data = json.dumps(paytmParams)

        url = "https://securegw.paytm.in/v3/order/status"

        response = requests.post(url, data=post_data, headers={
            "Content-type": "application/json"}).json()

        if(response['body']['resultInfo']['resultStatus'] == 'TXN_FAILURE'):
            response = response['body']

            if('paymentMode' in response):
                payment_mode = response['paymentMode']
                payment_status = 'PENDING'
            else:
                payment_mode = ''
                payment_status = 'FAILED'

            if(len(data.items) > 0):

                # Store Order
                total_amount = 0
                for item in data.items:
                    price = (item.price * item.quantity)
                    total_amount += price

                dborder = FulFillmentOrdersModel(
                    seller_id=userdata['id'],
                    order_number='AZF-'+str(uuid.uuid4().hex.upper()[0:13]),
                    total_amount=total_amount,
                    payment_status=payment_status,
                    payment_id=response['txnId'],
                    bank_txn_id='',
                    payment_order_id=response['orderId'],
                    payment_mode=payment_mode,
                    invoice=0,
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )

                db.add(dborder)
                db.commit()
                db.refresh(dborder)

                # Store Order Items
                for item in data.items:
                    dborderitem = FulFillmentOrdersItemsModel(
                        order_id=dborder.id,
                        item_id=item.id,
                        quantity=item.quantity,
                        price=item.price,
                    )
                    db.add(dborderitem)
                    db.commit()
                    db.refresh(dborderitem)
                return {"status_code": HTTP_200_OK, "is_placed": False, "order_reff": dborder.order_number, "message": "Order Failed"}
        else:
            response = response['body']

            if(len(data.items) > 0):

                # Store Order
                total_amount = 0
                for item in data.items:
                    price = (item.price * item.quantity)
                    total_amount += price

                dborder = FulFillmentOrdersModel(
                    seller_id=userdata['id'],
                    order_number='AZF-'+str(uuid.uuid4().hex.upper()[0:13]),
                    total_amount=total_amount,
                    payment_status='SUCCESS',
                    payment_id=response['txnId'],
                    bank_txn_id=response['bankTxnId'],
                    payment_order_id=response['orderId'],
                    payment_mode=response['paymentMode'],
                    invoice=0,
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )
                db.add(dborder)
                db.commit()
                db.refresh(dborder)

                # Store Order Items
                for item in data.items:
                    dborderitem = FulFillmentOrdersItemsModel(
                        order_id=dborder.id,
                        item_id=item.id,
                        quantity=item.quantity,
                        price=item.price,
                    )
                    db.add(dborderitem)
                    db.commit()
                    db.refresh(dborderitem)

            return {"status_code": HTTP_200_OK, "is_placed": True, "order_reff": dborder.order_number, "message": "Order Placed Successfully"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "is_placed": False, "order_reff": '', "message": "Not modified"}

# Orders


@v4_seller_fulfillment_router.get("/orders", response_model=FulfillmentOrdersSchema, dependencies=[Depends(JWTBearer())])
async def getOrders(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        userdata = auth(request=request)

        data: FulFillmentOrdersModel = await FulfillmentHelper.getOrders(db=db, user_id=userdata['id'])

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "total_items": 0, "items": [], "current_page": page, "total_pages": 0}

        orders: FulFillmentOrdersModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        ordersdata = []
        for order in orders:

            # items
            items = db.query(FulFillmentOrdersItemsModel).filter(
                FulFillmentOrdersItemsModel.order_id == order.id).all()
            total_amount = 0
            images = []
            for item in items:
                price = item.price * item.quantity
                total_amount += round(price)

                image = db.query(FulFillmentItemsModel).filter(
                    FulFillmentItemsModel.id == item.item_id).first()

                image = str(image.image)

            if('SUCCESS' in str(order.payment_status)):
                status = 'Order Confirmed'
            else:
                status = 'Order Failed'

            invoice = order.invoice_pdf
            if(order.invoice_pdf is None):
                invoice = ''
            orderdata = {
                'id': order.id,
                'order_number': order.order_number,
                'total_amount': total_amount,
                'items': len(items),
                'invoice': invoice,
                'images': image,
                'status': status,
                'created_at': order.created_at.strftime('%B %d %Y')
            }

            ordersdata.append(orderdata)

         # Count Total Orders
        total_records = data.count()

        # Number of Total Pages
        total_pages = round((total_records/limit), 0)

        # Check total count Similarity of records
        check = (total_pages * limit)

        if(check == total_records):
            pass
        else:
            total_pages = total_pages + 1

        return {"status_code": HTTP_200_OK, "total_items": total_records, "items": ordersdata, "current_page": page, "total_pages": total_pages}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "total_items": 0, "items": [], "current_page": page, "total_pages": 0}


# Order Items
@v4_seller_fulfillment_router.get("/order/details/{order_id}", dependencies=[Depends(JWTBearer())])
async def getOrderItems(request: Request, order_id: int, db: Session = Depends(get_db)):
    try:

        # Order Detail
        order = db.query(FulFillmentOrdersModel).filter(
            FulFillmentOrdersModel.id == order_id).first()

        # User
        user = auth(request=request)

        if(user['id'] != order.seller_id):
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

        # Items
        items = db.query(FulFillmentOrdersItemsModel).filter(
            FulFillmentOrdersItemsModel.order_id == order_id).all()
        total_amount = 0
        item_data = []
        for item in items:
            price = item.price * item.quantity
            total_amount += round(price)

            pro = db.query(FulFillmentItemsModel).filter(
                FulFillmentItemsModel.id == item.item_id).first()

            product = {
                'id': pro.id,
                'title': pro.title,
                'description': pro.description,
                'image': str(pro.image)
            }

            itm = {
                'id': item.id,
                'product': product,
                'price': item.price,
                'quantity': item.quantity,

            }
            item_data.append(itm)

        order = {
            'id': order.id,
            'order_number': order.order_number,
            'total_amount': total_amount,
            'items': len(items),
            'invoice': order.invoice_pdf,
            'created_at': order.created_at.strftime("%B %d %Y")
        }

        return {"status_code": HTTP_202_ACCEPTED, "order": order, "order_items": item_data}

    except Exception as e:
        print(e)
