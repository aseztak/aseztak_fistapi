from datetime import date, datetime
from app.db.config import get_db
from starlette.requests import Request
from starlette.status import HTTP_200_OK, HTTP_202_ACCEPTED, HTTP_404_NOT_FOUND, HTTP_304_NOT_MODIFIED
from sqlalchemy.orm.session import Session
from app.services.auth import auth
from fastapi import APIRouter,  Depends
from app.db.models.user import UserModel
from app.services.auth_bearer import JWTBearer
from app.db.models.products import InventoryModel, ProductModel, ProductPricingModel
from app.db.models.categories import CategoriesModel
from app.db.schemas.product_discount_schema import AddNewDiscount, ProductDiscountSearchSchema, RemoveProductDiscountSchema, SellerCategoryDiscountList, SellerProductDiscountList, AddProductDiscountSchema, SellerAllDiscountList
from app.db.models.product_discount import ProductDiscountModel
from sqlalchemy.sql.functions import func
from app.api.util.calculation import floatingValue
from app.db.models.seller_discount import SellerDiscountModel
v4_seller_product_discount_router = APIRouter()

# Seller New Discount


@v4_seller_product_discount_router.get("/list", dependencies=[Depends(JWTBearer())])
async def discountList(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 20):
    try:
        userdata = auth(request=request)
        # Today Date
        today = date.today()
        data = db.query(SellerDiscountModel).filter(
            SellerDiscountModel.seller_id == userdata['id']).order_by(SellerDiscountModel.id.desc())
        # Count Total Products
        total_records = data.count()

        # Number of Total Pages
        total_pages = round((total_records/limit), 0)

        # Check total count Similarity of records
        check = (total_pages * limit)

        if(check == total_records):
            pass
        else:
            total_pages = total_pages + 1
        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "discounts": [], "total_pages": int(total_pages)}
        else:
            discounts: SellerDiscountModel = data.limit(
                1).offset((page - 1) * limit).all()
            discount_list = []

            for i in range(len(discounts)):
                valid_upto = 'Expired'
                if(len(discounts) > 1):
                    if(i == 0):
                        if(discounts[i].valid_upto >= today):
                            valid_upto = discounts[i].valid_upto.strftime(
                                "%Y-%m-%d")
                    else:
                        valid_upto = 'Expired'
                if(len(discounts) == 1):
                    if(today > discounts[i].valid_upto):
                        valid_upto = 'Expired'
                    else:
                        valid_upto = discounts[i].valid_upto.strftime(
                            "%Y-%m-%d")

                d = {
                    'id': discounts[i].id,
                    'discount': floatingValue(discounts[i].rate),
                    'order_amount': floatingValue(discounts[i].limit_amount),
                    'valid_upto': valid_upto
                }
                discount_list.append(d)
            # for discount in discounts:
            #     if(today > discount.valid_upto):
            #         valid_upto = 'Expired'
            #     else:
            #         valid_upto = discount.valid_upto.strftime("%Y-%m-%d")
            #     d = {
            #         'id': discount.id,
            #         'discount': floatingValue(discount.rate),
            #         'order_amount': floatingValue(discount.limit_amount),
            #         'valid_upto': valid_upto
            #     }
            #     discount_list.append(d)

        return {"status_code": HTTP_200_OK, "discounts": discount_list, "total_pages": int(total_pages)}
    except Exception as e:
        print(e)


@v4_seller_product_discount_router.post("/add/new", dependencies=[Depends(JWTBearer())])
async def addDiscount(reques: Request, data: AddNewDiscount, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=reques)
        db_add_discount = SellerDiscountModel(
            seller_id=userdata['id'],
            rate=data.discount,
            limit_amount=data.order_limit,
            valid_upto=data.valid_upto,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(db_add_discount)
        db.commit()
        return {"status_code": HTTP_200_OK, "message": "Added Successfully"}
    except Exception as e:
        print(e)
        return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not modified"}


@v4_seller_product_discount_router.get("/", dependencies=[Depends(JWTBearer())])
async def RemoveProductDiscount(request: Request, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)
        # Today Date
        today = date.today()
        # YY-mm-dd
        today = today.strftime("%Y-%m-%d")
        # Check Product Discount
        check_all_pro_discount = db.execute("SELECT id, product_id, category_id, discount, valid_upto  FROM product_discount u1 WHERE u1.seller_id =:seller_id AND u1.valid_upto >=:param1 AND u1.type = 'ALL' AND u1.created_at = (SELECT MAX(u2.created_at) FROM product_discount u2 WHERE u2.product_id = u1.product_id) ORDER BY `id`  DESC LIMIT 1", {
            "seller_id": userdata['id'],
            "param1": today
        }).all()
        all_discount = True
        individual_discount = True
        category_discount = True
        if(len(check_all_pro_discount) > 0):
            individual_discount = False
            category_discount = False
        individual_data = {
            'name': 'Individual Product',
            'value': 'INDIVIDUAL',
            'description': 'Set discount on all product with valid upto',
            'add_discount': individual_discount
        }
        category_data = {
            'name': 'Category Wise',
            'value': 'CATEGORY',
            'description': 'Set discount on specific category with valid upto',
            'add_discount': category_discount
        }
        all_data = {
            'name': 'All Products',
            'value': 'ALL',
            'description': 'Set discount on specific product with valid upto',
            'add_discount': all_discount
        }

        product_discount = [
            all_data,
            category_data,
            individual_data

        ]

        return {"status_code": HTTP_200_OK, "discounts": product_discount}
    except Exception as e:
        print(e)


@v4_seller_product_discount_router.get("/all/remove/list", response_model=SellerAllDiscountList, dependencies=[Depends(JWTBearer())])
async def ProductDiscountList(request: Request, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)
        # Today Date
        today = date.today()
        # YY-mm-dd
        today = today.strftime("%Y-%m-%d")
        all_list = []

        all_discounts = db.execute(
            "SELECT id, product_id, category_id, discount, valid_upto  FROM product_discount u1 WHERE u1.seller_id =:seller_id AND u1.discount != 0  AND u1.valid_upto >=:param1 AND u1.type = 'ALL' AND u1.created_at = (SELECT MAX(u2.created_at) FROM product_discount u2 WHERE u2.product_id = u1.product_id) ORDER BY `id`  DESC LIMIT 1", {
                "seller_id": userdata['id'],
                "param1": today
            }).all()
        for product in all_discounts:
            productname = db.query(ProductModel).filter(
                ProductModel.id == product.product_id).first()
            alldata = {
                'name': 'ALL',
                'description': 'Discount running on all products',
                "valid_upto": product.valid_upto.strftime("%B %d %Y"),
                "running_discount": str(round(product.discount)) + '%',
            }
            all_list.append(alldata)
        return {"status_code": HTTP_200_OK, "total_products": len(all_discounts), "all": all_list}
    except Exception as e:
        print(e)


@v4_seller_product_discount_router.get("/category/remove/list", response_model=SellerCategoryDiscountList, dependencies=[Depends(JWTBearer())])
async def ProductDiscountList(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:
        userdata = auth(request=request)

        # Today Date
        today = date.today()
        # YY-mm-dd
        today = today.strftime("%Y-%m-%d")

        category_list = []
        offset = (page - 1) * limit
        category = db.execute("SELECT id, category_id, discount, valid_upto FROM product_discount  WHERE seller_id =:seller_id AND valid_upto >=:param1 AND discount != 0 AND type=:param2 AND id IN (SELECT MAX(id) FROM product_discount as table2 WHERE product_discount.category_id = table2.category_id) GROUP BY category_id ORDER BY id limit :limitparam offset :offsetparam", {
            "seller_id": userdata['id'],
            "param1": today,
            "param2": 'CATEGORY',
            "limitparam": limit,
            "offsetparam": offset
        }).all()
        all_category = db.execute("SELECT id, category_id, discount, valid_upto FROM product_discount  WHERE seller_id =:seller_id AND valid_upto >=:param1 AND discount != 0 AND type=:param2 AND id IN (SELECT MAX(id) FROM product_discount as table2 WHERE product_discount.category_id = table2.category_id) GROUP BY category_id ORDER BY id", {
            "seller_id": userdata['id'],
            "param1": today,
            "param2": 'CATEGORY'
        }).all()
        for product in category:

            categories = db.query(CategoriesModel).filter(
                CategoriesModel.id == product.category_id).first()
            # Count total products
            lists = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).filter(
                UserModel.status == 1).filter(ProductModel.category == product.category_id).filter(ProductModel.userid == userdata['id']).filter(ProductModel.status != 98).group_by(ProductModel.id).count()

            image = ''
            if(categories.image_path is not None):
                image = categories.image_path
            productdata = {
                'id': product.category_id,
                'name': categories.name,
                'description': categories.description,
                'image': image,
                'total_items': lists,
                "valid_upto": product.valid_upto.strftime("%B %d %Y"),
                "running_discount": str(round(product.discount)) + '%',
            }
            category_list.append(productdata)

        # Pagination
        total_pages = str(round((len(all_category)/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1
        return {"status_code": HTTP_200_OK, "total_products": len(category), "categories": category_list, "current_page": page, "total_pages": total_pages}

    except Exception as e:
        print(e)


@v4_seller_product_discount_router.get("/individual/remove/list", response_model=SellerProductDiscountList, dependencies=[Depends(JWTBearer())])
async def ProductDiscountList(request: Request,  db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:
        userdata = auth(request=request)

        # Today Date
        today = date.today()
        # YY-mm-dd
        today = today.strftime("%Y-%m-%d")

        product_list = []
        offset = (page - 1) * limit
        products = db.execute("SELECT id, product_id, discount,valid_upto FROM product_discount  WHERE seller_id =:seller_id AND valid_upto >=:param1 AND discount != 0 AND type=:param2 AND id IN (SELECT MAX(id) FROM product_discount as table2 WHERE product_discount.product_id = table2.product_id AND type = 'INDIVIDUAL') GROUP BY product_id ORDER BY id limit :limitparam offset :offsetparam", {
            "seller_id": userdata['id'],
            "param1": today,
            "param2": 'INDIVIDUAL',
            "limitparam": limit,
            "offsetparam": offset
        }).all()

        all_products = db.execute("SELECT id, product_id, discount,valid_upto FROM product_discount  WHERE seller_id =:seller_id AND valid_upto >=:param1 AND discount != 0 AND type=:param2 AND id IN (SELECT MAX(id) FROM product_discount as table2 WHERE product_discount.product_id = table2.product_id AND type = 'INDIVIDUAL') GROUP BY product_id ORDER BY id", {
            "seller_id": userdata['id'],
            "param1": today,
            "param2": 'INDIVIDUAL',
        }).all()

        for product in products:

            productname = db.query(ProductModel).filter(
                ProductModel.id == product.product_id).first()
            price = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == product.product_id).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            stock = False
            total = 0.00
            moq = 0
            if(price):
                total = (price.price * price.tax) / 100 + price.price

                # Check Product for Stock or out Stock
                checkinventory = db.query(ProductPricingModel).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == product.product_id).filter(InventoryModel.out_of_stock == 0).filter(ProductPricingModel.deleted_at.is_(None)).first()

                stock = False
                if(checkinventory is not None):
                    stock = True

                moq = price.moq
            status = 'Approved'
            if(productname.status == 1):
                status = 'Inactive'
            elif(productname.status == 98):
                status = 'Rejected'
            elif(productname.status == 81):
                status = 'Hidden'
            elif(productname.status == 99):
                status = 'Rejected'

            data = {
                'id': product.product_id,
                'name': productname.title,
                'image': productname.images[0].file_path,
                'price': floatingValue(round(total, 2)),
                'moq': moq,
                'stock': stock,
                "status": status,
                "created_at": productname.created_at.strftime("%Y-%m-%d"),
                "valid_upto": product.valid_upto.strftime("%B %d %Y"),
                "running_discount": str(round(product.discount)) + '%',
            }
            product_list.append(data)
        # Pagination
        total_pages = str(round((len(all_products)/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1
        return {"status_code": HTTP_200_OK, "total_products": len(products), "products": product_list, "current_page": page, "total_pages": total_pages}

    except Exception as e:
        print(e)


@v4_seller_product_discount_router.post("/category", response_model=SellerCategoryDiscountList, dependencies=[Depends(JWTBearer())])
async def ProductDiscountCategoryList(request: Request, data: ProductDiscountSearchSchema, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)
        # Today Date
        today = date.today()
        # YY-mm-dd
        today = today.strftime("%Y-%m-%d")
        search = data.search.replace("+", "%")
        search = search.rstrip()
        search = "%{}%".format(search)
        offset = (data.page - 1) * data.limit
        if (data.search != ''):
            categories = db.execute('SELECT categories.id, categories.name, categories.image_path, categories.description FROM `categories` LEFT JOIN products ON products.category = categories.id LEFT JOIN product_discount ON product_discount.category_id = categories.id WHERE categories.name LIKE :search AND products.status = 51 AND products.userid =:userid AND (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto ) is NULL OR (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) != 0   GROUP BY categories.id ORDER BY categories.id DESC limit :limitparam offset :offsetparam', {
                "search": search,
                "userid": userdata['id'],
                'valid_upto': today,
                'limitparam': data.limit,
                'offsetparam': offset
            }).all()
            total_categories = db.execute('SELECT categories.id, categories.name, categories.image_path, categories.description FROM `categories` LEFT JOIN products ON products.category = categories.id LEFT JOIN product_discount ON product_discount.category_id = categories.id WHERE categories.name LIKE :search AND products.status = 51 AND products.userid =:userid AND (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto ) is NULL OR (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) != 0   GROUP BY categories.id ORDER BY categories.id DESC', {
                "search": search,
                "userid": userdata['id'],
                'valid_upto': today,
            }).all()
        else:
            categories = db.execute('SELECT categories.id, categories.name, categories.image_path, categories.description FROM `categories` LEFT JOIN products ON products.category = categories.id LEFT JOIN product_discount ON product_discount.category_id = categories.id WHERE products.status = 51 AND products.userid =:userid AND (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto ) is NULL OR (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) != 0   GROUP BY categories.id ORDER BY categories.id DESC limit :limitparam offset :offsetparam', {
                "userid": userdata['id'],
                'valid_upto': today,
                'limitparam': data.limit,
                'offsetparam': offset
            }).all()
            total_categories = db.execute('SELECT categories.id, categories.name, categories.image_path, categories.description FROM `categories` LEFT JOIN products ON products.category = categories.id LEFT JOIN product_discount ON product_discount.category_id = categories.id WHERE products.status = 51 AND products.userid =:userid AND (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto ) is NULL OR (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) != 0   GROUP BY categories.id ORDER BY categories.id DESC', {
                "userid": userdata['id'],
                'valid_upto': today,
            }).all()

        category_list = []
        for category in categories:
            # Count total products
            lists = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).filter(
                UserModel.status == 1).filter(ProductModel.category == category.id).filter(ProductModel.userid == userdata['id']).filter(ProductModel.status != 98).group_by(ProductModel.id).count()

            image = ''
            if(category.image_path is not None):
                image = category.image_path

            running_discount = db.query(ProductDiscountModel).filter(ProductDiscountModel.seller_id == userdata['id']).filter(ProductDiscountModel.category_id == category.id).filter(
                ProductDiscountModel.type == 'category').filter(func.date_format(ProductDiscountModel.valid_upto, '%Y-%m-%d') >= today).order_by(ProductDiscountModel.id.desc()).first()

            discount = ''
            if(running_discount):
                discount = str(round(running_discount.discount))+'%'

            prodata = {
                'id': category.id,
                'name': category.name,
                'description': category.description,
                'image': image,
                'total_items': lists,
                "running_discount": discount,
            }
            category_list.append(prodata)
            # Count Total Products
        total_records = len(total_categories)
        # Number of Total Pages
        total_pages = str(round((total_records/data.limit), 2))
        total_pages = total_pages.split('.')
        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1
        return {"status_code": HTTP_200_OK, "total_products": total_records, "categories": category_list, "current_page": data.page, "total_pages": total_pages}
    except Exception as e:
        print(e)


@v4_seller_product_discount_router.post("/individual", response_model=SellerProductDiscountList, dependencies=[Depends(JWTBearer())])
async def ProductDiscountIndividualList(request: Request, data: ProductDiscountSearchSchema, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)
        # Today Date
        today = date.today()
        # YY-mm-dd
        today = today.strftime("%Y-%m-%d")
        search = data.search.replace("+", "%")
        search = search.rstrip()
        search = "%{}%".format(search)
        offset = (data.page - 1) * data.limit
        if (data.search != ''):
            productdata = db.execute('SELECT products.id, products.title FROM products LEFT JOIN product_discount ON product_discount.product_id = products.id WHERE products.title LIKE :search AND products.status = 51 AND products.userid =:userid AND (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) is NULL OR (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) != 0 GROUP BY products.id ORDER BY products.id DESC limit :limitparam offset :offsetparam', {
                "search": search,
                "userid": userdata['id'],
                "valid_upto": today,
                "limitparam": data.limit,
                "offsetparam": offset
            }).all()
            all_productdata = db.execute('SELECT products.id, products.title FROM products LEFT JOIN product_discount ON product_discount.product_id = products.id WHERE products.title LIKE :search AND products.status = 51 AND products.userid =:userid AND (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) is NULL OR (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) != 0 GROUP BY products.id ORDER BY products.id DESC', {
                "search": search,
                "userid": userdata['id'],
                "valid_upto": today,
            }).all()
        else:
            productdata = db.execute('SELECT products.id, products.title FROM products LEFT JOIN product_discount ON product_discount.product_id = products.id WHERE products.status = 51 AND products.userid =:userid AND (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) is NULL OR (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) != 0 GROUP BY products.id ORDER BY products.id DESC limit :limitparam offset :offsetparam', {
                "userid": userdata['id'],
                "valid_upto": today,
                "limitparam": data.limit,
                "offsetparam": offset
            }).all()
            all_productdata = db.execute('SELECT products.id, products.title FROM products LEFT JOIN product_discount ON product_discount.product_id = products.id WHERE products.status = 51 AND products.userid =:userid AND (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) is NULL OR (DATE_FORMAT(product_discount.valid_upto, "%Y-%m-%d") <:valid_upto) != 0 GROUP BY products.id ORDER BY products.id DESC', {
                "userid": userdata['id'],
                "valid_upto": today
            }).all()

        productlist = []
        for product in productdata:
            product = db.query(ProductModel).filter(
                ProductModel.id == product.id).first()
            running_discount = db.query(ProductDiscountModel).filter(ProductDiscountModel.seller_id == userdata['id']).filter(ProductDiscountModel.product_id == product.id).filter(
                ProductDiscountModel.type == 'INDIVIDUAL').filter(func.date_format(ProductDiscountModel.valid_upto, '%Y-%m-%d') >= today).order_by(ProductDiscountModel.id.desc()).first()

            discount = ''
            if(running_discount):
                discount = str(round(running_discount.discount))+'%'

            price = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == product.id).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            stock = False
            total = 0.00
            moq = 0
            if(price):
                total = (price.price * price.tax) / 100 + price.price

                # Check Product for Stock or out Stock
                checkinventory = db.query(ProductPricingModel).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == product.id).filter(InventoryModel.out_of_stock == 0).filter(ProductPricingModel.deleted_at.is_(None)).first()

                stock = False
                if(checkinventory is not None):
                    stock = True

                moq = price.moq

            status = 'Approved'
            if(product.status == 1):
                status = 'Inactive'
            elif(product.status == 98):
                status = 'Rejected'
            elif(product.status == 81):
                status = 'Hidden'
            elif(product.status == 99):
                status = 'Rejected'
            prodata = {
                'id': product.id,
                'name': product.title,
                'image': product.images[0].file_path,
                'price': floatingValue(round(total, 2)),
                'moq': moq,
                'stock': stock,
                "status": status,
                "created_at": product.created_at.strftime("%B %d %Y"),
                "running_discount": discount,
            }
            productlist.append(prodata)
            # Count Total Products
        total_records = len(all_productdata)
        # Number of Total Pages
        total_pages = str(round((total_records/data.limit), 2))
        total_pages = total_pages.split('.')
        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1
        return {"status_code": HTTP_200_OK, "total_products": total_records, "products": productlist, "current_page": data.page, "total_pages": total_pages}
    except Exception as e:
        print(e)


@v4_seller_product_discount_router.post("/add", dependencies=[Depends(JWTBearer())])
async def AddProductDiscount(request: Request, data: AddProductDiscountSchema, db: Session = Depends(get_db)):
    try:

        # User
        checkuser = auth(request=request)
        if (data.type == 'category' or data.type == 'CATEGORY'):
            for category in data.category_id:

                products = db.query(ProductModel).filter(
                    ProductModel.userid == checkuser['id']).filter(ProductModel.category == category).filter(ProductModel.status == 51).filter(
                    ProductModel.status != 98).order_by(ProductModel.id.desc()).all()
                for categorydata in products:
                    # Check Discount
                    check_discount = db.query(ProductDiscountModel).filter(
                        ProductDiscountModel.seller_id == checkuser['id']).filter(ProductDiscountModel.category_id == categorydata.id).filter(ProductDiscountModel.type == 'CATEGORY').first()
                    if(check_discount is not None):
                        check_discount.valid_upto = data.valid_upto
                        check_discount.discount = data.discount
                        check_discount.created_at = datetime.now()
                        check_discount.updated_at = datetime.now()
                        db.flush()
                        db.commit()

                    else:
                        # Check product_discount on others type
                        check_product_discount = db.query(ProductDiscountModel).filter(
                            ProductDiscountModel.seller_id == checkuser['id']).filter(ProductDiscountModel.product_id == categorydata.id).first()
                        if(check_product_discount is not None):
                            check_product_discount.valid_upto = data.valid_upto
                            check_product_discount.discount = data.discount
                            check_product_discount.type = 'CATEGORY'
                            check_product_discount.category_id = category
                            check_product_discount.created_at = datetime.now()
                            check_product_discount.updated_at = datetime.now()
                            db.flush()
                            db.commit()
                        else:
                            category_discount = ProductDiscountModel(
                                product_id=categorydata.id,
                                seller_id=checkuser['id'],
                                type='CATEGORY',
                                category_id=category,
                                discount=data.discount,
                                valid_upto=data.valid_upto,
                                created_at=datetime.now(),
                                updated_at=datetime.now()
                            )
                            db.add(category_discount)
                            db.commit()

        elif (data.type == 'individual' or data.type == 'INDIVIDUAL'):
            for productid in data.product_id:
                check_discount = db.query(ProductDiscountModel).filter(
                    ProductDiscountModel.seller_id == checkuser['id']).filter(ProductDiscountModel.product_id == productid).filter(ProductDiscountModel.type == 'INDIVIDUAL').first()
                if(check_discount is not None):
                    check_discount.category_id = 0
                    check_discount.valid_upto = data.valid_upto
                    check_discount.discount = data.discount
                    check_discount.created_at = datetime.now()
                    check_discount.updated_at = datetime.now()
                    db.flush()
                    db.commit()
                else:
                    individual_discount = ProductDiscountModel(
                        product_id=productid,
                        seller_id=checkuser['id'],
                        type='INDIVIDUAL',
                        category_id=0,
                        discount=data.discount,
                        valid_upto=data.valid_upto,
                        created_at=datetime.now(),
                        updated_at=datetime.now()
                    )
                    db.add(individual_discount)
                    db.commit()
                    db.refresh(individual_discount)

        elif (data.type == 'all' or data.type == 'ALL'):
            check_discount = db.query(ProductDiscountModel).filter(
                ProductDiscountModel.seller_id == checkuser['id']).delete()
            db.commit()
            db.execute("INSERT INTO `product_discount`(product_id, seller_id, type, category_id, discount, valid_upto) SELECT products.id, :seller_id, :type, :category_id, :discount, :valid_upto FROM `products` WHERE products.userid =:seller_id AND products.status = 51", {
                "seller_id": checkuser['id'],
                "type": 'ALL',
                "category_id": 0,
                "discount": data.discount,
                "valid_upto": data.valid_upto
            })
            db.commit()

            # all_products = db.query(ProductModel).filter(ProductModel.userid == checkuser['id']).filter(
            #     ProductModel.status == 51).order_by(ProductModel.id.desc()).all()

            # for all_product in all_products:
            #     check_discount = db.query(ProductDiscountModel).filter(
            #         ProductDiscountModel.seller_id == checkuser['id']).filter(ProductDiscountModel.product_id == all_product.id).filter(ProductDiscountModel.type == 'ALL').first()
            #     if(check_discount is not None):
            #         check_discount.category_id = 0
            #         check_discount.valid_upto = data.valid_upto
            #         check_discount.discount = data.discount
            #         check_discount.created_at = datetime.now()
            #         check_discount.updated_at = datetime.now()
            #         db.flush()
            #         db.commit()
            #     else:
            #         all_discount = ProductDiscountModel(
            #             product_id=all_product.id,
            #             seller_id=checkuser['id'],
            #             type='ALL',
            #             category_id=0,
            #             discount=data.discount,
            #             valid_upto=data.valid_upto,
            #             created_at=datetime.now(),
            #             updated_at=datetime.now()
            #         )
            #         db.add(all_discount)
            #         db.commit()
            #         db.refresh(all_discount)

        else:
            return {"status_code": HTTP_404_NOT_FOUND, "message": "Error"}

        return {"status_code": HTTP_202_ACCEPTED, "message": "Added Successfully"}

    except Exception as e:
        print(e)


@v4_seller_product_discount_router.post("/remove", dependencies=[Depends(JWTBearer())])
async def UpdateDiscount(request: Request, data: RemoveProductDiscountSchema, db: Session = Depends(get_db)):
    try:

        # User
        checkuser = auth(request=request)
        # Today Date
        today = date.today()
        # YY-mm-dd
        today = today.strftime("%Y-%m-%d")

        if (data.type == 'category' or data.type == 'CATEGORY'):
            for category in data.category_id:
                db.query(ProductDiscountModel).filter(
                    ProductDiscountModel.category_id == category).filter(ProductDiscountModel.seller_id == checkuser['id']).filter(ProductDiscountModel.type == 'CATEGORY').delete()
                db.commit()

        elif (data.type == 'individual' or data.type == 'INDIVIDUAL'):
            for productid in data.product_id:
                db.query(ProductDiscountModel).filter(
                    ProductDiscountModel.product_id == productid).filter(ProductDiscountModel.seller_id == checkuser['id']).filter(ProductDiscountModel.type == 'INDIVIDUAL').delete()
                db.commit()

        elif (data.type == 'all' or data.type == 'ALL'):
            db.query(ProductDiscountModel).filter(
                ProductDiscountModel.seller_id == checkuser['id']).delete()
            db.commit()
        else:
            return {"status_code": HTTP_404_NOT_FOUND, "message": "Error"}

        return {"status_code": HTTP_202_ACCEPTED, "message": "Deleted Successfully"}

    except Exception as e:
        print(e)
