
from datetime import datetime, timedelta
from typing import List

from fastapi.param_functions import File
from starlette import status
from starlette.requests import Request
from starlette.types import Message
from app.api.util.check_user import CheckUser
from app.api.helpers.orders import OrderHelper
from app.api.helpers.orderstaticstatus import OrderStatus
from app.db.models.acconts import AccountsModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.order_status import OrderStatusModel
from app.db.models.orders import OrdersModel
from app.db.models.pickup_address import PickupLocationModel
from app.services.auth import auth
from starlette.status import HTTP_200_OK, HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED
from app.db.models.products import ProductModel
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.products import *
from app.services.auth import auth
from app.db.schemas.orders_schema import AcceptOrder, PackedOrders, SellerCancelOrderSchema, SellerOrderDetailSchema, SellerOrderSearchSchema, SellerOrdersSchema
from fastapi import APIRouter,  Depends, Form
from os import getcwd
from starlette.requests import Request
from app.api.helpers.media import MediaHelper
from fastapi import File, UploadFile
import uuid
from os import getcwd, unlink
from app.db.models.shipping import OrderShippingModel
from app.db.models.return_proof import OrderReturnProofModel
from app.api.util.message import Message
from app.db.models.wallet import WalletModel
from app.core.config import COIN_EXPIRING_DAYS
v4_seller_order_router = APIRouter()

# Today Orders


@v4_seller_order_router.get("/today/{param}", response_model=SellerOrdersSchema,  dependencies=[Depends(JWTBearer())])
async def todayOrders(request: Request, param: str, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:
        userdata = auth(request=request)

        # Today Orders
        today = date.today()
        # YY-mm-dd
        today = today.strftime("%Y-%m-%d")

        # Today Orders
        offset = (page - 1) * limit
        todayorders = db.execute("SELECT orders.id, OS.status FROM orders LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE order_fails.order_id is NULL AND P.userid =:userid AND date_format(orders.created_at, '%Y-%m-%d') =:today GROUP BY orders.id HAVING (CASE WHEN :param = 'orders' THEN MIN(OS.status) = 0 WHEN :param = 'cancelled_orders' THEN MAX(OS.status) = 980 ELSE OS.status >= 90 AND OS.status != 91 AND OS.status != 980 END) ORDER BY orders.id DESC LIMIT :limitparam OFFSET :offset", {
            "userid": userdata['id'],
            "param": param,
            "today": today,
            "limitparam": limit,
            "offset": offset
        }).all()
        data = db.execute("SELECT COUNT(*) as total_count FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE order_fails.order_id is NULL AND P.userid =:userid AND date_format(orders.created_at, '%Y-%m-%d') =:today GROUP BY orders.id HAVING (CASE WHEN :param = 'orders' THEN MIN(OS.status) = 0 WHEN :param = 'cancelled_orders' THEN MAX(OS.status) = 980 ELSE OS.status >= 90 AND OS.status != 91 AND OS.status != 980 END)) AS counts", {
            "userid": userdata['id'],
            "param": param,
            "today": today,
        }).first()
        data = data.total_count
        # if(param == 'orders'):
        #     data = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
        #         ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == userdata['id']).filter(func.date_format(OrdersModel.created_at, '%Y-%m-%d') == today).group_by(OrdersModel.id).count()

        #     todayorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
        #         ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == userdata['id']).filter(func.date_format(OrdersModel.created_at, '%Y-%m-%d') == today).group_by(OrdersModel.id).limit(
        #         limit=limit).offset((page - 1) * limit).all()
        # elif(param == 'cancelled_orders'):
        #     # Today Cancelled Orders

        #     data = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
        #         ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == userdata['id']).filter(func.date_format(OrderStatusModel.created_at, '%Y-%m-%d') == today).filter(OrderStatusModel.status == 980).group_by(OrdersModel.id).count()

        #     todayorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
        #         ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == userdata['id']).filter(func.date_format(OrderStatusModel.created_at, '%Y-%m-%d') == today).filter(OrderStatusModel.status == 980).group_by(OrdersModel.id).limit(
        #         limit=limit).offset((page - 1) * limit).all()

        # else:
        #     # Total Return Orders
        #     data = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == userdata['id']).filter(func.date_format(OrderStatusModel.created_at, '%Y-%m-%d') == today).filter(
        #         OrderStatusModel.status >= 90).filter(OrderStatusModel.status != 91).filter(OrderStatusModel.status != 980).group_by(OrderStatusModel.order_id).count()

        #     todayorders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == userdata['id']).filter(func.date_format(OrderStatusModel.created_at, '%Y-%m-%d') == today).filter(
        #         OrderStatusModel.status >= 90).filter(OrderStatusModel.status != 91).filter(OrderStatusModel.status != 980).group_by(OrderStatusModel.order_id).limit(
        #         limit=limit).offset((page - 1) * limit).all()

        # statuslist = OrderStatus.AllstatusList()
        statustitle = 'Orders List'
        # for s_title in statuslist:
        #     if(status == s_title['status_id']):
        #         statustitle = s_title['status_title']

        if(status == 999):
            statustitle = 'All Orders'

        if(len(todayorders) == 0):
            return {"status_code": HTTP_200_OK, 'title': statustitle,  "total_orders": 0, 'orders': [], 'current_page': page, 'total_pages': 0, "message": "Not found"}

         # Customize Order Data
        orders: OrdersModel = await OrderHelper.customizeOrderListData(request=request, db=db, data=todayorders)

        # Count Total Products
        total_records = data

        # Number of Total Pages
        total_pages = round((total_records/limit), 0)

        if(total_pages == 0):
            total_pages = 1

        # Check total count Similarity of records
        check = (total_pages * limit)

        if(check == total_records):
            pass
        else:
            total_pages = total_pages

        return {"status_code": HTTP_200_OK, 'title': statustitle, "total_orders": total_records, "orders": orders, "current_page": page, "total_pages": total_pages}

    except Exception as e:
        return {"status_code": HTTP_200_OK, 'title': statustitle,  "total_orders": 0, 'orders': [], 'current_page': page, 'total_pages': 0, "message": "Not found"}


# Seller Orders
@v4_seller_order_router.get("/", dependencies=[Depends(JWTBearer())])
async def getOrders(request: Request, db: Session = Depends(get_db)):
    try:

        # user
        userdata = auth(request=request)

        # All Orders
        all_orders = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails  ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL GROUP BY orders.id) as counts", {
            "userid": userdata['id']
        }).first()
        # # Pending Orders
        pending_orders = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL GROUP BY orders.id HAVING MAX(OS.status) = 0) as counts", {
            "userid": userdata['id']
        }).first()
        # Accept Orders
        accept_orders = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL GROUP BY orders.id HAVING MAX(OS.status) = 10) as counts", {
            "userid": userdata['id']
        }).first()

        # Approved Orders
        approved_orders = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL GROUP BY orders.id HAVING MAX(OS.status) = 30) as counts", {
            "userid": userdata['id']
        }).first()

        # Packed Orders
        packed_orders = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL GROUP BY orders.id HAVING MAX(OS.status) = 40) as counts", {
            "userid": userdata['id']
        }).first()
        # Shipped Orders
        shipped_orders = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL GROUP BY orders.id HAVING MAX(OS.status) = 60) as counts", {
            "userid": userdata['id']
        }).first()
        # Delivered Orders
        delivered = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL GROUP BY orders.id HAVING MAX(OS.status) = 70) as counts", {
            "userid": userdata['id']
        }).first()

        # Return Approved
        return_approved = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL AND OT.status = 90 GROUP BY orders.id) as counts", {
            "userid": userdata['id']
        }).first()
        # Return Completed
        return_completed = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails  ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL AND OT.status >= 100 AND OT.status != 980 GROUP BY orders.id) as counts", {
            "userid": userdata['id']
        }).first()
        # Reversed Orders
        reversed_orders = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails  ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL  GROUP BY orders.id HAVING MAX(OS.status) = 61) as counts", {
            "userid": userdata['id']
        }).first()
        # Cancelled Orders
        cancelled_orders = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id, OS.status FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE P.userid =:userid AND order_fails.order_id is NULL AND OT.status = 980 GROUP BY orders.id) as counts", {
            "userid": userdata['id']
        }).first()

        orders_count = [

            {
                'status': 0,
                'count': pending_orders.total_count,
                'title': 'New Orders',
                'description': 'Your all pending orders',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/pending orders.svg'
            },
            {
                'status': 10,
                'count': accept_orders.total_count,
                'title': 'Accepted Orders',
                'description': 'You have accepted orders',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/accepted orders.svg'
            },

            {
                'status': 30,
                'count': approved_orders.total_count,
                'title': 'Approved Orders',
                'description': 'System has approved your orders',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/approved orders.svg'
            },
            {
                'status': 40,
                'count': packed_orders.total_count,
                'title': 'Packed Orders',
                'description': 'Packed Orders',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/packed orders.svg'
            },
            {
                'status': 60,
                'count': shipped_orders.total_count,
                'title': 'Shipped Orders',
                'description': 'Shipped Orders',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/shipped orders.svg'
            },

            {
                'status': 70,
                'count': delivered.total_count,
                'title': 'Delivered Orders',
                'description': 'Delivered Orders',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/delivered orders.svg'
            },


            {
                'status': 90,
                'count': return_approved.total_count,
                'title': 'Return Initiated',
                'description': 'Return Initiated',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/return initiate.svg'
            },

            {
                'status': 100,
                'count': return_completed.total_count,
                'title': 'Return Completed',
                'description': 'Return Completd',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/return completed.svg'
            },

            {
                'status': 61,
                'count': reversed_orders.total_count,
                'title': 'Reversed Orders',
                'description': 'Reversed Orders',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/reversed orders.svg'
            },

            {
                'status': 980,
                'count': cancelled_orders.total_count,
                'title': 'Cancelled Orders',
                'description': 'Cancelled Orders',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/cancelled orders.svg'

            },
            {
                'status': 999,
                'count': all_orders.total_count,
                'title': 'All Orders',
                'description': 'All Orders',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/orders 2.svg'
            },
        ]

        return {"status_code": HTTP_200_OK, "order_status": orders_count}

    except Exception as e:

        return {"status_code": HTTP_200_OK, "order_status": []}

# Seller Order List Status Wise


# @v4_seller_order_router.get("/{status}", response_model=SellerOrdersSchema, dependencies=[Depends(JWTBearer())])
# async def getOrdersStatusWise(request: Request, status: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
#     try:

#         userdata = auth(request=request)

#         data: OrdersModel = await OrderHelper.getSellerOrdersStatusWise(db=db, status=status, user_id=userdata['id'])

#         orders: OrdersModel = data.limit(
#             limit=limit).offset((page - 1) * limit).all()

#         statuslist = OrderStatus.AllstatusList()
#         statustitle = ''
#         for s_title in statuslist:
#             if(status == s_title['status_id']):
#                 statustitle = s_title['status_title']

#         if(status == 999):
#             statustitle = 'All Orders'

#         if(len(orders) == 0):
#             return {"status_code": HTTP_200_OK, 'title': statustitle,  "total_orders": 0, 'orders': [], 'current_page': page, 'total_pages': 0, "message": "Not found"}

#         # Customize Order Data
#         orders: OrdersModel = await OrderHelper.customizeOrderListData(request=request, db=db, data=orders)

#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, 'title': statustitle, "total_orders": total_records, "orders": orders, "current_page": page, "total_pages": total_pages}

#     except Exception as e:
#         return {"status_code": HTTP_200_OK, 'title': statustitle,  "total_orders": 0, 'orders': [], 'current_page': page, 'total_pages': 0, "message": "Not found"}

# search seller orders


@v4_seller_order_router.post("/search", response_model=SellerOrdersSchema, dependencies=[Depends(JWTBearer())])
async def searchSellerOrders(request: Request, search: SellerOrderSearchSchema, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        if(search.search == '' and search.from_date == '' and search.to_date == ''):
            if(search.status == 0):
                offset = (search.page - 1) * search.limit
                data = db.execute("SELECT orders.id, orders.reff, orders.order_number, orders.commission_reff, orders.user_id, orders.status, orders.message, orders.round_off, orders.grand_total, orders.partial_amount, orders.cod_amount, orders.discount_rate, orders.discount, orders.delivery_charge, orders.item_count, orders.address_id, orders.meta_data, orders.pickup_address_id, orders.payment_status, orders.payment_method, orders.invoice, orders.invoice_date, orders.height, orders.width, orders.app_version, orders.free_delivery, orders.created_at, orders.updated_at FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE order_fails.order_id is NULL AND P.userid =:userid AND OT.status = 0 GROUP BY orders.id ORDER BY orders.id DESC LIMIT :ordlimit OFFSET :offset", {
                    "userid": userdata['id'],
                    "ordlimit": search.limit,
                    "offset": offset
                }).all()

                orders = data

                datacount = db.execute("SELECT ( SELECT COUNT(*)  FROM ( SELECT orders.id FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails ON order_fails.order_id = orders.id WHERE order_fails.order_id is NULL AND P.userid =:userid AND OT.status = 0 GROUP BY orders.id) as count) as total_count", {
                    "userid": userdata['id'],
                }).first()

                datacount = datacount.total_count

            else:
                data: OrdersModel = await OrderHelper.getSellerOrdersStatusWise(db=db, status=search.status, user_id=userdata['id'])
                datacount = data.count()
                orders: OrdersModel = data.limit(
                    limit=search.limit).offset((search.page - 1) * search.limit).all()

        else:
            data: OrdersModel = await OrderHelper.searchSellerOrdersStatusWise(db=db, status=search.status, user_id=userdata['id'],  search=search.search, from_date=search.from_date, to_date=search.to_date)
            datacount = data.count()
            orders: OrdersModel = data.limit(
                limit=search.limit).offset((search.page - 1) * search.limit).all()

        statuslist = OrderStatus.AllstatusList()
        statustitle = ''
        for s_title in statuslist:
            if(search.status == s_title['status_id']):
                statustitle = s_title['status_title']

        if(search.status == 999):
            statustitle = 'All Orders'

        if(len(orders) == 0):
            return {"status_code": HTTP_200_OK, 'title': statustitle,  "total_orders": 0, 'orders': [], 'current_page': search.page, 'total_pages': 0, "message": "Not found"}

        # Customize Order Data
        orders: OrdersModel = await OrderHelper.customizeOrderListData(request=request, db=db, data=orders)

        total_records = datacount

        # Number of Total Pages
        total_pages = str(round((total_records/search.limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK, 'title': statustitle, "total_orders": total_records, "orders": orders, "current_page": search.page, "total_pages": total_pages}

    except Exception as e:
        print(e)
        return {"status_code": HTTP_200_OK, 'title': statustitle,  "total_orders": 0, 'orders': [], 'current_page': search.page, 'total_pages': 0, "message": "Not found"}

# Order details


@v4_seller_order_router.get("/details/{order_id}", response_model=SellerOrderDetailSchema, dependencies=[Depends(JWTBearer())])
async def getOrderDetails(request: Request, order_id: int, db: Session = Depends(get_db)):
    try:

        # Check user and Order data
        checkuser = CheckUser.CheckSellerOrder(
            request=request, order_id=order_id, db=db)
        if(checkuser == False):
            return {"status_code": HTTP_304_NOT_MODIFIED}

        userdata = auth(request=request)

        # order details
        orderdata: OrdersModel = await OrderHelper.customizeOrderDetails(db=db, data=order_id)

        # order items
        orderitems: OrdersModel = await OrderHelper.customizeOrderItems(request=request, db=db, data=order_id)

        # order status
        orderstatus: OrdersModel = await OrderHelper.orderStatusSeller(db=db, data=order_id)

        # pickup address
        pickup_address = db.query(PickupLocationModel).filter(
            PickupLocationModel.seller_id == userdata['id']).filter(PickupLocationModel.status == 51).all()

        # Cancel Reason
        cancel_reason = [
            {
                'selected': False,
                'text': 'I want to change address for the order',
            },
            {
                'selected': False,
                'text': 'I want to buy different item',
            },
            {
                'selected': False,
                'text': 'I have changed my mind',
            },
            {
                'selected': False,
                'text': 'I want to convert my order prepaid',
            },
            {
                'selected': False,
                'text': 'Other',
            },
        ]

        addresses = []
        if(len(pickup_address) != 0):

            for address in pickup_address:
                pickup_address = {
                    'id': address.id,
                    'name': address.name
                }

                addresses.append(pickup_address)

        # Check Last Order Invoice
        # search = "%{}%".format('ASEZ20232024')
        # checkinvoice = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
        #     ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == userdata['id']).filter(OrdersModel.invoice.ilike(search)).filter(OrderStatusModel.status == 10).order_by(OrderStatusModel.updated_at.desc()).first()
        # if(checkinvoice):
        #     invoice = checkinvoice.invoice.split('2024')
        #     invoice = int(invoice[1]) + 1
        #     if(invoice <= 9):
        #         invoice = str('ASEZ202320240')+str(invoice)
        #     else:
        #         invoice = str('ASEZ20232024')+str(invoice)
        # else:
        #     invoice = 'ASEZ2023202401'
        checkinvoice = db.execute(
            "SELECT orders.id, orders.invoice  FROM orders WHERE orders.invoice REGEXP '^[0-9]+$'  and app_version = 'V4'  order by orders.invoice + 0 DESC").first()
        if(checkinvoice is not None):
            invoice = int(checkinvoice.invoice) + 1
            invoice = str(invoice)
        else:
            invoice = '1'

        today = datetime.now()
        invoice_date = today.strftime('%Y-%m-%d')

        invoice = {
            'number': invoice,
            'date': invoice_date
        }

        # Packaging
        packaging = db.query(OrderShippingModel).filter(
            OrderShippingModel.order_id == order_id).first()

        if(packaging is not None):
            packaging = {
                'lebel_pdf': packaging.slip,
                'invoice_pdf': packaging.invoice
            }
        else:
            packaging = {
                'lebel_pdf': '',
                'invoice_pdf': ''
            }
        # Item Weight Type
        weight_type = {
            'Gram',
            'Kg'
        }
        return {"status_code": HTTP_202_ACCEPTED, "invoice": invoice, "item_weight_type": weight_type, "order": orderdata, "packaging": packaging, "items": orderitems, "order_status": orderstatus, 'pickup_address': addresses, "cancel_reasons": cancel_reason}

    except Exception as e:
        print(e)


# Seller Order Accept
@v4_seller_order_router.post("/accept", dependencies=[Depends(JWTBearer())])
async def acceptOrder(request: Request, data: AcceptOrder,  db: Session = Depends(get_db)):
    try:

        # Check user
        checkuser = CheckUser.CheckSellerOrder(
            request=request, order_id=data.order_id, db=db)
        if(checkuser == False):
            return {"is_accepted": False, "message": 'Order Accepted'}

        # Order
        order = db.query(OrdersModel).filter(
            OrdersModel.id == data.order_id).first()

        for item in data.items:

            checkitem = db.query(OrderItemsModel).filter(
                OrderItemsModel.id == item.item_id).first()
            if(checkitem.status != 980):

                weight = 0
                if(item.weight_type == 'Kg'):
                    weight = item.weight * 1000
                else:
                    weight = item.weight

                checkitem.weight = weight
                checkitem.status = 10
                checkitem.updated_at = datetime.now()
                db.commit()
                db.flush()

        # Update Order
        # order.invoice = data.invoice_number
        # order.invoice_date = data.invoice_date
        order.pickup_address_id = data.address_id
        order.height = data.shipment_height
        order.width = data.shipment_width
        order.length = data.shipment_length

        db.commit()
        db.flush()

        # Update Order Status
        dborderstatus = OrderStatusModel(
            order_id=data.order_id,
            status=10,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )

        db.add(dborderstatus)
        db.commit()
        db.refresh

        return {"is_accepted": True, "message": 'Order Accepted'}
    except Exception as e:
        return {"is_accepted": False, "message": 'Order Accepted'}


# Packed Order
@v4_seller_order_router.post("/packed", dependencies=[Depends(JWTBearer())])
async def packedOrders(request: Request, data: PackedOrders, db: Session = Depends(get_db)):
    try:

        # Check user
        checkuser = CheckUser.CheckSellerOrder(
            request=request, order_id=data.order_id, db=db)
        if(checkuser == False):
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

        db_orderstatus = OrderStatusModel(
            order_id=data.order_id,
            status=40,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(db_orderstatus)
        db.commit()
        db.refresh(db_orderstatus)

        # Order Details
        order = db.query(OrdersModel).filter(
            OrdersModel.id == data.order_id).first()
        # Buyer
        buyerdetails = db.query(UserModel).filter(
            UserModel.id == order.user_id).first()
        # Check Order Items
        checkorderItems = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status != 980).all()
        product_title = db.query(ProductModel).filter(
            ProductModel.id == checkorderItems[0].product_id).first()
        notification_image = db.query(ProductMediaModel).filter(
            ProductMediaModel.model_id == product_title.id).filter(ProductMediaModel.deleted_at.is_(None)).first()

        checkorderItems = len(checkorderItems)
        if(checkorderItems > 1):
            checkorderItems = (checkorderItems - 1)

            product_title = str(
                product_title.title)

            product_title = product_title[:15]

            product_title = str(
                product_title)+' and '+str(checkorderItems)+' more'

        else:
            product_title = str(product_title.title)
            product_title = product_title[:15]
        # Send Notification to Buyer
        b_notification_body = 'Order for ' + str(product_title) + ' with Order ID ' + str(
            order.order_number) + ' has been packed.'
        b_notification_title = 'Order Packed'
        b_path = '/order-detail'
        b_notification_image = notification_image.file_path

        if(buyerdetails.fcm_token is not None):
            await Message.SendOrderNotificationtoBuyer(user_token=buyerdetails.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=order.id)

        return {"status_code": HTTP_200_OK, "message": "Order Packed"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "message": "Invalid Input"}

# Seller Order Cancel


@v4_seller_order_router.post("/cancel", dependencies=[Depends(JWTBearer())])
async def cancelOrder(request: Request, data: SellerCancelOrderSchema, db: Session = Depends(get_db)):
    try:

        # Check user
        checkuser = CheckUser.CheckSellerOrder(
            request=request, order_id=data.id, db=db)
        if(checkuser == False):
            return {"is_cancelled": False, "message": "Not Modified"}

        userdata = auth(request=request)

        # Order
        order = db.query(OrdersModel).filter(
            OrdersModel.id == data.id).first()
        order.message = data.message
        order.updated_at = datetime.now()
        db.flush()
        db.commit()

        # Update Order Status
        dborderstatus = OrderStatusModel(
            order_id=data.id,
            status=980,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(dborderstatus)
        db.commit()

        # Update Order item Status
        items = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == data.id).filter(OrderItemsModel.status != 980).all()
        item_total = 0
        for item in items:
            if(item.wallet_amount != 0):
                todaydate = date.today()
                td = timedelta(days=COIN_EXPIRING_DAYS)
                valid_upto = todaydate + td
                valid_upto = valid_upto.strftime("%Y-%m-%d")
                db_wallet = WalletModel(
                    user_id=order.user_id,
                    reff_id=order.id,
                    reff_type='ORDER_CANCELLED',
                    description='Cancelled (Order ID: ' +
                    str(order.order_number)+str(')'),
                    type='CR',
                    amount=item.wallet_amount,
                    status=1,
                    valid_upto=valid_upto,
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )
                db.add(db_wallet)
                db.commit()

            price = (item.price * item.tax) / 100 + item.price
            price = round(price) * item.quantity

            item_total += price

            item.status = 980
            item.message = data.message
            item.cancelled_by_seller = 'Yes'
            item.updated_at = datetime.now()

            db.flush()
            db.commit()

        # Maintain Seller Account
        today = datetime.now()
        today = today.strftime('%Y-%m-%d')
        dbaccount = AccountsModel(
            user_id=userdata['id'],
            txn_date=today,
            head='CANCELLED',
            txn_description='CANCELLED (' +
            str(order.order_number) + ')',
            txn_type="CR",
            txn_amount=item_total
        )
        db.add(dbaccount)
        db.commit()
        db.refresh(dbaccount)
        return {"is_cancelled": True, "message": "Order has been Cancelled"}
    except Exception as e:
        print(e)


# Seller Order Item Cancel
@v4_seller_order_router.post("/cancel/item", dependencies=[Depends(JWTBearer())])
async def cancelOrderItem(request: Request, data: SellerCancelOrderSchema,  db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        # Item Data
        item = db.query(OrderItemsModel).filter(
            OrderItemsModel.id == data.id).first()

        # Order data
        order = db.query(OrdersModel).filter(
            OrdersModel.id == item.order_id).first()

        # Check user
        checkuser = CheckUser.CheckSellerOrder(
            request=request, order_id=order.id, db=db)
        if(checkuser == False):
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

        item.status = 980
        item.message = 'Cancelled by Seller'
        item.updated_at = datetime.now()
        db.flush()
        db.commit()

        price = (item.price * item.tax) / 100 + item.price
        item_total = round(price) * item.quantity
        if(item.wallet_amount != 0):
            todaydate = date.today()
            td = timedelta(days=COIN_EXPIRING_DAYS)
            valid_upto = todaydate + td
            valid_upto = valid_upto.strftime("%Y-%m-%d")
            db_wallet = WalletModel(
                user_id=order.user_id,
                reff_id=order.id,
                reff_type='ORDER_CANCELLED',
                description='Cancelled (Order ID: ' +
                str(order.order_number)+str(')'),
                type='CR',
                amount=item.wallet_amount,
                status=1,
                valid_upto=valid_upto,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.add(db_wallet)
            db.commit()

        # Maintain Seller Account
        today = datetime.now()
        today = today.strftime('%Y-%m-%d')
        dbaccount = AccountsModel(
            user_id=userdata['id'],
            txn_date=today,
            head='CANCELLED',
            txn_description='CANCELLED (' +
            str(order.order_number) + ')',
            txn_type="CR",
            txn_amount=item_total
        )
        db.add(dbaccount)
        db.commit()
        db.refresh(dbaccount)

        return {"status_code": HTTP_202_ACCEPTED, "message": "Order has been Cancelled"}

    except Exception as e:
        print(e)


# Upload Order Packed Proof
@v4_seller_order_router.post("/upload/image/packed", dependencies=[Depends(JWTBearer())])
async def UploadImagePacked(request: Request, order_id: int = Form(...), images: List[UploadFile] = File(...), db: Session = Depends(get_db)):
    try:

        # Check user
        checkuser = CheckUser.CheckSellerOrder(
            request=request, order_id=order_id, db=db)
        if(checkuser == False):
            return {"is_packed": False, "message": "Not Modified"}

        if(len(images) > 0):
            for image in images:
                file = image.filename.split(".")
                filename = f"{uuid.uuid4()}.{file[1]}"
                # Set Path Of image
                file_name = getcwd()+"/app/static/proof/"+filename
                # Upload image in path
                with open(file_name, 'wb+') as f:
                    f.write(image.file.read())
                    f.close()
                # open the image
                uploaded_file = getcwd()+"/app/static/proof/" + str(filename)
                filename = str(filename)
                # Upload Photo
                MediaHelper.uploadImagePacked(
                    uploaded_file=uploaded_file, file_name=filename)
                # Remove Original Image from Path
                unlink(uploaded_file)
                file_path = str(
                    linode_obj_config['endpoint_url'])+'/proof/'+str(filename)
                db_orderproof = OrderReturnProofModel(
                    order_id=order_id,
                    image=file_path,
                    order_type='PACKED',
                    status=51,
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )
                db.add(db_orderproof)
                db.commit()
                db.refresh(db_orderproof)
            # Check Packed orders
            check_packed_orders = db.query(OrderStatusModel).filter(
                OrderStatusModel.order_id == order_id).order_by(OrderStatusModel.id.desc()).first()
            if(check_packed_orders.status < 40):
                db_orderstatus = OrderStatusModel(
                    order_id=order_id,
                    status=40,
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )
                db.add(db_orderstatus)
                db.commit()
                db.refresh(db_orderstatus)

                # Order Details
                order = db.query(OrdersModel).filter(
                    OrdersModel.id == order_id).first()
                # Buyer
                buyerdetails = db.query(UserModel).filter(
                    UserModel.id == order.user_id).first()
                # Check Order Items
                checkorderItems = db.query(OrderItemsModel).filter(
                    OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status != 980).all()
                product_title = db.query(ProductModel).filter(
                    ProductModel.id == checkorderItems[0].product_id).first()
                notification_image = db.query(ProductMediaModel).filter(
                    ProductMediaModel.model_id == product_title.id).filter(ProductMediaModel.deleted_at.is_(None)).first()

                checkorderItems = len(checkorderItems)
                if(checkorderItems > 1):
                    checkorderItems = (checkorderItems - 1)

                    product_title = str(
                        product_title.title)

                    product_title = product_title[:15]

                    product_title = str(
                        product_title)+' and '+str(checkorderItems)+' more'

                else:
                    product_title = str(product_title.title)
                    product_title = product_title[:15]
                # Send Notification to Buyer
                b_notification_body = 'Order for ' + str(product_title) + ' with Order ID ' + str(
                    order.order_number) + ' has been packed.'
                b_notification_title = 'Order Packed'
                b_path = '/order-detail'
                b_notification_image = notification_image.file_path

                if(buyerdetails.fcm_token is not None):
                    await Message.SendOrderNotificationtoBuyer(user_token=buyerdetails.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=order.id)

        return {"is_packed": True, "message": "Successfully Updated"}
    except Exception as e:
        return {"is_packed": False, "message": "Not modified"}
