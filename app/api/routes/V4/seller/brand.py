from os import getcwd, unlink

from fastapi import APIRouter,  Depends, Form, Request


from sqlalchemy.orm.session import Session


from app.api.helpers.media import MediaHelper
from app.api.helpers.auth import *

from app.db.config import get_db
from app.db.models.brand import BrandsModel


from app.db.schemas.auth_schema import *
from app.db.schemas.brand_schema import V4SellerBrandListSchema
from datetime import datetime
from app.services.auth import auth
from app.resources.strings import *
from app.api.helpers.users import *
from app.services.auth_bearer import JWTBearer
from starlette.status import HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED
from starlette.requests import Request
from fastapi import File, UploadFile
import uuid
v4_seller_brand_router = APIRouter()


# List of All Brands
@v4_seller_brand_router.get("/list", response_model=V4SellerBrandListSchema, dependencies=[Depends(JWTBearer())])
async def getBrands(request: Request, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        # data: BrandsModel = await BrandsHelper.getBrands(db=db, user_id=userdata['id'])
        data: BrandsModel = db.query(BrandsModel, UserModel.name.label('seller')).join(UserModel, UserModel.id == BrandsModel.user_id).filter(
            BrandsModel.user_id == userdata['id']).filter(BrandsModel.id != 1).order_by(BrandsModel.id.desc())

        if(data.count() == 0):
            return {"status_code": HTTP_202_ACCEPTED, "total_brands": 0, "brands": []}

        brands: BrandsModel = data.all()

        branddata = []

        for brand in brands:

            status = False
            if(brand.BrandsModel.status == 1):
                status = True

            b = {
                'id': brand.BrandsModel.id,
                'name': brand.BrandsModel.name,
                'logo': brand.BrandsModel.logo,
                'status': status
            }
            branddata.append(b)

        # Count Total Products
        total_records = data.count()

        return {"status_code": HTTP_202_ACCEPTED, "total_brands": total_records, "brands": branddata}

    except Exception as e:
        return {"status_code": HTTP_202_ACCEPTED, "total_brands": 0, "brands": []}


# Add New Brand
@v4_seller_brand_router.post("/add", dependencies=[Depends(JWTBearer())])
async def addBrands(request: Request, name: str = Form(...),  logo: UploadFile = File(...), db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        file = logo.filename.split(".")
        extension = file[1]
        uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
            datetime.now().strftime('%Y-%d-%M-%h-%s')

        filename = f"{uniqueID}.{file[1]}"

        # Set Path Of image
        file_name = getcwd()+"/app/static/brands/"+filename

        # Upload image in path
        with open(file_name, 'wb+') as f:
            f.write(logo.file.read())
            f.close()

        uploaded_file = getcwd()+"/app/static/brands/" + str(filename)
        filename = str(filename)
        # Upload Photo
        MediaHelper.uploadBrandPhoto(
            uploaded_file=uploaded_file, file_name=filename)

        # Remove Original Image from Path

        unlink(uploaded_file)

        file_path = str(
            linode_obj_config['endpoint_url'])+'/brands/'+str(filename)

        dbbrand = BrandsModel(
            user_id=userdata['id'],
            name=name,
            logo=file_path,
            slug=name.replace(" ", "-").lower(),
            status=0
        )
        db.add(dbbrand)
        db.commit()
        db.refresh(dbbrand)

        return {"is_added": True, "message": "Brand added successfully"}

    except Exception as e:
        return {"is_added": False, "message": "Not Modified"}


# Edit Brand Data
@v4_seller_brand_router.get("/edit/{brand_id}", dependencies=[Depends(JWTBearer())])
async def editBrands(request: Request, brand_id: int, db: Session = Depends(get_db)):
    try:

        brand = db.query(BrandsModel).filter(
            BrandsModel.id == brand_id).first()

        brand = {
            "id": brand.id,
            'name': brand.name,
            'logo': brand.logo
        }

        return {"status_code": HTTP_202_ACCEPTED, "brand": brand}
    except Exception as e:
        return {"status_code": HTTP_304_NOT_MODIFIED, "brand": {}}

# Update Brand Data


@v4_seller_brand_router.post("/update", dependencies=[Depends(JWTBearer())])
async def updateBrand(request: Request, logo: Optional[UploadFile] = File(None), id: int = Form(...), name: str = Form(...), db: Session = Depends(get_db)):
    try:

        # User
        user = auth(request=request)

        brand = db.query(BrandsModel).filter(BrandsModel.id == id).first()

        if(user['id'] != brand.user_id):
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

        if(logo != None):
            file = logo.filename.split(".")
            extension = file[1]
            uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
                datetime.now().strftime('%Y-%d-%M-%h-%s')

            filename = f"{uniqueID}.{file[1]}"

            # Set Path Of image
            file_name = getcwd()+"/app/static/brands/"+filename

            # Upload image in path
            with open(file_name, 'wb+') as f:
                f.write(logo.file.read())
                f.close()

            uploaded_file = getcwd()+"/app/static/brands/" + str(filename)
            filename = str(filename)
            # Upload Photo
            MediaHelper.uploadBrandPhoto(
                uploaded_file=uploaded_file, file_name=filename)

            # Remove Original Image from Path

            unlink(uploaded_file)

            file_path = str(
                linode_obj_config['endpoint_url'])+'/brands/'+str(filename)

        brand.name = name
        brand.slug = name.replace(" ", "-").lower(),
        if(logo != None):
            brand.logo = file_path

        db.flush()
        db.commit()

        return {"is_updated": True, "message": "Brand Updated Successfully"}

    except Exception as e:
        return {"is_updated": False, "message": "Not modified"}
