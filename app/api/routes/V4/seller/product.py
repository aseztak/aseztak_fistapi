from datetime import datetime
from starlette.requests import Request
from app.api.util.check_user import CheckUser
from app.db.models.attribute_family_values import AttriFamilyValuesModel
from app.db.models.attribute_values import AttributeValueModel
from app.db.models.attributes import AttributeModel
from app.db.models.categories import CategoriesModel
from app.db.models.category_commission import CategoryCommissionModel
from app.db.models.media import ProductMediaModel
from app.db.models.productattribute import ProductAttributeModel
from app.services.auth import auth
from typing import  List, Optional
from starlette.status import HTTP_200_OK, HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED, HTTP_404_NOT_FOUND
from app.db.models.products import InventoryModel, ProductModel, ProductPricingAttributeModel, ProductPricingModel
from app.db.models.brand import BrandsModel
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.db.schemas.product_schema import AttributeAddSchema, AddPricingSchema, DeleteImage, DeleteProducPriceSchema, DeleteProductSchema, PriceStock, ProductOverViewSchema, SellerAllProductList, SellerProductListCategoryWise, SellerProductSearchSchema, SellerSearchAllProducts, UpdatePricingSchema,  SellerAllProductListSchema,  SellerProductSearch, topsaleproductschema
from app.api.helpers.products import *
from app.services.auth import auth
from fastapi import File, UploadFile
import uuid
from PIL import Image, ImageOps
from fastapi import APIRouter,  Depends, Form
from os import getcwd, unlink
from app.api.helpers.media import MediaHelper
from app.api.util.calculation import floatingValue
from app.db.models.product_rejection import ProductRejectionModel
from sqlalchemy.orm import joinedload
import string

v4_seller_product_router = APIRouter()

# Basic Product Adding
# , dependencies = [Depends(JWTBearer())]


@v4_seller_product_router.post("/add", dependencies=[Depends(JWTBearer())])
async def addProduct(request: Request, title: str = Form(...), category: int = Form(...), description: Optional[str] = Form(None), brand: Optional[str] = Form(None), images: List[UploadFile] = File(...), db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        title = title.translate(str.maketrans('', '', string.punctuation))
        title = " ".join(title.split())
        title = title.title()
        slug = title.replace(" ", "-").lower()

        # title = title.strip()
        # title = " ".join(title.strip().split())
        # slug = title.replace(" ", "-").lower()
        description = description.strip()
        description = " ".join(description.strip().split())
        checkProduct = db.query(ProductModel).filter(
            ProductModel.slug == slug).first()
        if(checkProduct):
            title = title+str(1)
            slug = title.replace(" ", "-").lower()
            slug = title.replace(" ", "-").lower()
            checkProductwithextra = db.query(ProductModel).filter(
                ProductModel.slug == slug).first()
            if(checkProductwithextra):
                title = title+str(1)

            slug = title.replace(" ", "-").lower()
            dbproduct = ProductModel(
                userid=userdata['id'],
                title=title,
                slug=slug,
                category=category,
                brand_id=brand,
                short_description=description,
                created_at=datetime.now(),
                updated_at=datetime.now()

            )

            db.add(dbproduct)
            db.flush()

        else:
            dbproduct = ProductModel(
                userid=userdata['id'],
                title=title,
                slug=slug,
                category=category,
                brand_id=brand,
                short_description=description,
                created_at=datetime.now(),
                updated_at=datetime.now()

            )

            db.add(dbproduct)
            db.flush()

        # Store Categories
        cats = []
        categorydata = db.query(CategoriesModel).filter(
            CategoriesModel.id == category).first()
        cats.append(categorydata.id)
        if(categorydata.parent_id > 1):

            checkparent = db.query(CategoriesModel).filter(
                CategoriesModel.id == categorydata.parent_id).first()
            cats.append(checkparent.id)

        if(checkparent is not None and checkparent.parent_id > 1):

            checkparent1 = db.query(CategoriesModel).filter(
                CategoriesModel.id == checkparent.parent_id).first()
            cats.append(checkparent1.id)

            if(checkparent1 is not None and checkparent1.parent_id > 1):
                checkparent2 = db.query(CategoriesModel).filter(
                    CategoriesModel.id == checkparent1.parent_id).first()
                cats.append(checkparent2.id)

                if(checkparent2 is not None and checkparent2.parent_id > 1):
                    checkparent3 = db.query(CategoriesModel).filter(
                        CategoriesModel.id == checkparent2.parent_id).first()
                    cats.append(checkparent3.id)

        for cat in cats:
            productcategories = ProductCategories(
                category_id=cat,
                product_id=dbproduct.id,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.add(productcategories)
            db.commit()
        if(len(images) > 0):
            for image in images:
                # Rename Image
                file = image.filename.rsplit(".", 1)
                extension = file[1]
                uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
                    datetime.now().strftime('%Y-%d-%M-%h-%s')

                filename = f"{uniqueID}.{file[1]}"

                # Set Path Of image
                file_name = getcwd()+"/app/static/products/"+filename

                # Upload image in path
                with open(file_name, 'wb+') as f:
                    f.write(image.file.read())
                    f.close()
                # open the image
                picture = Image.open(file_name)

                picture = ImageOps.exif_transpose(picture)

                width, height = picture.size
                newwidth = 600
                ratio = float(newwidth) / float(width)
                newheight = int(height * ratio)

                if(width < 600):
                    picture = picture.resize(
                        (round(width), round(height)))
                else:
                    picture = picture.resize(
                        (round(newwidth), round(newheight)))

                # if(width < 600):
                #     picture = picture.resize(
                #         (round(width), round(height)), Image.ANTIALIAS)
                # else:
                #     picture = picture.resize(
                #         (round(newwidth), round(newheight)), Image.ANTIALIAS)

                if(extension.upper() == 'PNG'):
                    picture.save("app/static/products/product_"+str(filename),
                                 'PNG',
                                 optimize=True,
                                 quality=85)

                if(extension.upper() == 'JPG' or extension.upper() == 'JPEG'):
                    picture.save("app/static/products/product_"+str(filename),
                                 'JPEG',
                                 optimize=True,
                                 quality=85)

                uploaded_file = getcwd()+"/app/static/products/"+str("product_") + str(filename)
                filename = str("product_") + str(filename)

                # Upload Photo
                MediaHelper.uploadPhoto(
                    uploaded_file=uploaded_file, file_name=filename)

                # Remove Original Image from Path
                unlink(file_name)
                unlink(uploaded_file)

                file_path = str(
                    linode_obj_config['endpoint_url'])+'/products/'+str(filename)

                name = filename.split(".")
                dbmedia = ProductMediaModel(
                    model_id=dbproduct.id,
                    collection_name='product',
                    name=name[0],
                    file_name=filename,
                    file_path=file_path,
                    default_img=0,
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )

                db.add(dbmedia)
                db.commit()

        return {"status_code": HTTP_200_OK, "is_product": True, "product": dbproduct.id, "message": "Product Added Successfully"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "is_product": False, "product": 0, "message": "Not Modified..."}


# Product Edit
@v4_seller_product_router.get('/edit/{product_id}', dependencies=[Depends(JWTBearer())])
async def editProduct(request: Request, product_id: int, db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=product_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "message": "Not Modified"}

        data: ProductModel = await ProductsHelper.getProduct(db=db, id=product_id)

        if(data):

            brand = 0
            if(data.brand_id == None):
                data.brand_id = brand

            images = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product_id).filter(
                ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.id.desc()).all()

            imagedata = []
            if(len(images) > 0):
                for image in images:

                    filename = image.file_path
                    img = filename

                    img = {
                        'id': image.id,
                        'path': img,

                    }
                    imagedata.append(img)

            product = {
                'title': data.title,
                'brand': data.brand_id,
                'short_description': data.short_description,
                'images': imagedata
            }

            return {"status_code": HTTP_200_OK, "product": product}

        return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Error"}

    except Exception as e:
        print(e)

# Update Product basic


@v4_seller_product_router.put('/update/{product_id}', dependencies=[Depends(JWTBearer())])
async def updateProduct(request: Request, product_id: int, title: str = Form(...), description: Optional[str] = Form(None), brand: Optional[int] = Form(None), images: Optional[List[UploadFile]] = File([]), db: Session = Depends(get_db)):

    try:

        if(brand is None):
            brand = 1

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=product_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "is_update": False, "product": product_id, "message": "Not Modified"}

        product = db.query(ProductModel).filter(
            ProductModel.id == product_id).first()

        if(product):

            title = title.translate(str.maketrans('', '', string.punctuation))
            title = " ".join(title.split())
            title = title.title()
            slug = title.replace(" ", "-").lower()

            # title = title.strip()
            # title = " ".join(title.strip().split())

            # slug = title.replace(" ", "-").lower()

            description = description.strip()
            description = " ".join(description.strip().split())

            checkproduct = db.query(ProductModel).filter(
                ProductModel.slug == slug).first()
            if(checkproduct):
                if(checkproduct.id != product.id):
                    return {"status_code": HTTP_200_OK, "is_update": False, "product": product_id, "message": "Already Exist"}

            product.title = title
            product.slug = slug
            product.brand_id = brand
            product.short_description = description
            product.status = 1
            product.updated_at = datetime.now()

            db.flush()
            db.commit()

            if(len(images) > 0):
                for image in images:
                    # Rename Image
                    file = image.filename.rsplit(".", 1)
                    extension = file[1]
                    uniqueID = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid.uuid4()) + \
                        datetime.now().strftime('%Y-%d-%M-%h-%s')

                    filename = f"{uniqueID}.{file[1]}"

                    # Set Path Of image
                    file_name = getcwd()+"/app/static/products/"+filename

                    # Upload image in path
                    with open(file_name, 'wb+') as f:
                        f.write(image.file.read())
                        f.close()
                    # open the image
                    picture = Image.open(file_name)

                    picture = ImageOps.exif_transpose(picture)

                    width, height = picture.size
                    newwidth = 600
                    ratio = float(newwidth) / float(width)
                    newheight = int(height * ratio)

                    if(width < 600):
                        picture = picture.resize(
                            (round(width), round(height)))
                    else:
                        picture = picture.resize(
                            (round(newwidth), round(newheight)))

                    # if(width < 600):
                    #         picture = picture.resize(
                    #         (round(width), round(height)), Image.ANTIALIAS)
                    # else:
                    #     picture = picture.resize(
                    #         (round(newwidth), round(newheight)), Image.ANTIALIAS)

                    if(extension.upper() == 'PNG'):
                        picture.save("app/static/products/product_"+str(filename),
                                     'PNG',
                                     optimize=True,
                                     quality=85)

                    if(extension.upper() == 'JPG' or extension.upper() == 'JPEG'):
                        picture.save("app/static/products/product_"+str(filename),
                                     'JPEG',
                                     optimize=True,
                                     quality=85)

                    uploaded_file = getcwd()+"/app/static/products/"+str("product_") + str(filename)
                    filename = str("product_") + str(filename)

                    # Upload Photo
                    MediaHelper.uploadPhoto(
                        uploaded_file=uploaded_file, file_name=filename)

                    # Remove Original Image from Path
                    unlink(file_name)
                    unlink(uploaded_file)

                    file_path = str(
                        linode_obj_config['endpoint_url'])+'/products/'+str(filename)

                    name = filename.split(".")
                    dbmedia = ProductMediaModel(
                        model_id=product.id,
                        collection_name='product',
                        name=name[0],
                        file_name=filename,
                        file_path=file_path,
                        default_img=0,
                        created_at=datetime.now(),
                        updated_at=datetime.now()
                    )

                    db.add(dbmedia)
                    db.commit()
                    db.refresh(dbmedia)

        return {"status_code": HTTP_200_OK, "is_update": True, "product": product.id, "message": "Successfully Updated"}
    except Exception as e:
        print(e)

# Product Attributes


@v4_seller_product_router.get("/attributes/{product_id}", dependencies=[Depends(JWTBearer())])
async def GetAttributes(request: Request, product_id: int, db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=product_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK,  "attributes": []}

        products = db.query(ProductModel).filter(
            ProductModel.id == product_id).first()
        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == products.category).first()

        attributes = db.query(AttriFamilyValuesModel, AttributeModel).join(AttributeModel, AttributeModel.id == AttriFamilyValuesModel.attribute_id).options(
            joinedload(AttributeModel.product_attributes)

        ).filter(AttriFamilyValuesModel.family_id == category.family_id).group_by(AttributeModel.id).all()

        attribute_data = []

        for attribute in attributes:

            if(attribute.AttributeModel.is_required == 0):
                attribute.AttributeModel.is_required = False
            else:
                attribute.AttributeModel.is_required = True

            # Attribute Values
            attribute_values = attribute.AttributeModel.product_attributes
            attribute_values_data = []

            selected_values = []
            if(attribute.AttributeModel.frontend_type != 'text'):
                for attrvalues in attribute_values:
                    checkProductAttribute = db.query(ProductAttributeModel).filter(
                        ProductAttributeModel.product_id == product_id).filter(ProductAttributeModel.attribute_value_id == attrvalues.id).first()
                    selected = False
                    if(checkProductAttribute is not None):
                        selected = True
                        selected_values.append(str(
                            checkProductAttribute.attribute_value_id))

                    attribute_value = {
                        'selected': selected,
                        'code': attribute.AttributeModel.code,
                        'id': str(attrvalues.id),
                        'value': attrvalues.value,
                    }
                    attribute_values_data.append(attribute_value)

            else:
                checkProductTextAttribute = db.query(ProductAttributeModel).filter(
                    ProductAttributeModel.product_id == product_id).filter(ProductAttributeModel.attribute_id == attribute.AttributeModel.id).first()

                if(checkProductTextAttribute):

                    selected_values.append(str(
                        checkProductTextAttribute.attribute_value))

            # Attribute

            attrdata = {
                'is_required': attribute.AttributeModel.is_required,
                'code': attribute.AttributeModel.code,
                'name': attribute.AttributeModel.name,
                'type': attribute.AttributeModel.frontend_type,
                'selected_values': selected_values,
                'values': attribute_values_data
            }

            attribute_data.append(attrdata)

        return {"status_code": HTTP_200_OK,  "attributes": attribute_data}

    except Exception as e:
        print(e)


# Add Product Attributes
@v4_seller_product_router.post("/add/attributes", dependencies=[Depends(JWTBearer())])
async def addAttributes(request: Request, data: AttributeAddSchema, db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=data.product_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "is_update": False,  "message": "Not Modified"}

        if(len(data.attributes) != 0):
            # Re-Structure Attributes
            attrdata = []
            for attribute in data.attributes:
                attr = db.query(AttributeModel).filter(
                    AttributeModel.code == attribute.code).first()

                # Delete Old attributes
                checkattributes = db.query(ProductAttributeModel).filter(
                    ProductAttributeModel.product_id == data.product_id).filter(ProductAttributeModel.attribute_id == attr.id).first()
                if(checkattributes):
                    db.query(ProductAttributeModel).filter(
                        ProductAttributeModel.product_id == data.product_id).filter(ProductAttributeModel.attribute_id == attr.id).delete()

                if(attr.frontend_type == 'text'):
                    if(len(attribute.values) > 0):
                        attrd = {
                            'attribute_id':  attr.id,
                            'attribute_value_id': 0,
                            'attribute_value': attribute.values[0]
                        }
                        attrdata.append(attrd)

                else:

                    for value in attribute.values:

                        attrvalue = db.query(AttributeValueModel).filter(
                            AttributeValueModel.id == value).first()

                        attrd = {
                            'attribute_id':  attr.id,
                            'attribute_value_id': value,
                            'attribute_value': attrvalue.value
                        }
                        attrdata.append(attrd)

            # Add Attributes
            for attrbt in attrdata:
                dbproductattribute = ProductAttributeModel(
                    product_id=data.product_id,
                    attribute_id=attrbt['attribute_id'],
                    attribute_value_id=attrbt['attribute_value_id'],
                    attribute_value=attrbt['attribute_value'],
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )

                db.add(dbproductattribute)
                db.commit()
                db.refresh(dbproductattribute)

            # Update Pricing
            pricing = db.query(ProductPricingModel).filter(
                ProductPricingModel.product_id == data.product_id).filter(ProductPricingModel.deleted_at.is_(None)).all()

            if(len(pricing) > 0):
                attribute_values = []
                for price in pricing:
                    pricing_attributes = db.query(ProductPricingAttributeModel).filter(
                        ProductPricingAttributeModel.product_pricing_id == price.id).all()

                    for price_attribute in pricing_attributes:
                        attribute_values.append(price_attribute)

                for attr in attribute_values:

                    productattribute = db.query(ProductAttributeModel).filter(ProductAttributeModel.product_id == data.product_id).filter(
                        ProductAttributeModel.attribute_value_id == attr.attribute_id).first()
                    if(productattribute is None):
                        db.query(ProductPricingModel).filter(
                            ProductPricingModel.id == attr.product_pricing_id).update({'deleted_at': datetime.now()})
                        db.commit()

        return {"status_code": HTTP_200_OK, "is_update": True,  "message": "Attributes updated successfully"}

    except Exception as e:
        print(e)


# Prouct Pricing
@v4_seller_product_router.get('/pricing/{product_id}', dependencies=[Depends(JWTBearer())])
async def productPricing(request: Request, product_id: int, db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=product_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK,  "message": "Not Modified"}

        # Product Detail
        product = db.query(ProductModel).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images)).filter(
            ProductModel.id == product_id).first()

        # Pricing Units
        pricing_units = {
            'Pcs',
            'Box',
            'Mtr',
            'Pair',
            'kg',
            'Bnd',
            'Dzn',
            'Ltr',
            'Qts',
            'Than',
        }

        # Product Attributes
        product_attributs = db.query(ProductAttributeModel).filter(
            ProductAttributeModel.product_id == product_id).group_by(ProductAttributeModel.attribute_id).all()

        price_attributes = []
        for attributes in product_attributs:
            # Attribute
            attribute = db.query(AttributeModel).filter(
                AttributeModel.id == attributes.attribute_id).first()

            if(attribute.is_price_variable == 1):
                p_attributes = db.query(ProductAttributeModel).filter(
                    ProductAttributeModel.product_id == product_id).all()

                price_variable_attributes = []
                for pattribute in p_attributes:
                    pattr = db.query(AttributeModel).filter(
                        AttributeModel.id == pattribute.attribute_id).first()

                    if(pattr.is_price_variable == 1 and attribute.id == pattribute.attribute_id):
                        p_attributes = {
                            'id': pattribute.attribute_value_id,
                            'value': pattribute.attribute_value
                        }
                        price_variable_attributes.append(p_attributes)

                pp_attributes = {
                    'name': attribute.name,
                    'code': attribute.code,
                    'values': price_variable_attributes
                }

                price_attributes.append(pp_attributes)

        # Category Commission
        category_commision = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id == product.category).order_by(
            CategoryCommissionModel.id.desc()).first()
        category_commision = category_commision.commission

        # GST
        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == product.category).first()
        gst = category.gst
        # TCS
        tcs = 1
        # TDS
        tds = 1

        # TDS
        tcs_policy = {
            'title': TCS_POLICY_TITLE,
            'body': TCS_POLICY_BODY

        }

        # TCS
        tds_policy = {
            'title': TDS_POLICTY_TITLE,
            'body': TDS_POLICY_BODY
        }

        customer_will_see = {
            'title': CUSTOMER_WILL_SEE,
            'body': THIS_PRICE_WILL_BE_SHOWN_IN_THE_APP
        }

        service_charge_on_product = {
            'title': 'Service Charge on product value (' + str(category_commision)+'% + GST)',
            'body': SERVICE_CHARGE_ON_PRODUCT_VALUE_TEXT
        }
        price_breakup = {
            'heading': PRICE_BREAKUP,
            'customer_will_see': customer_will_see,
            'service_charge_on_product_value': service_charge_on_product,
            'tds_on_product_value': tds_policy,
            'tcs_on_product_value': tcs_policy,
            'you_will_get': YOU_WILL_GET
        }

        # Image List
        images = []
        imagedata = product.images

        file_name = ''
        if(len(imagedata) > 0):
            for image in imagedata:
                img = {
                    'id': image.id,
                    'image': image.file_path
                }
                images.append(img)

            # Product Image
            file_name = images[0]['image']

        # ProductData
        product = {
            'name': product.title,
            'description': product.short_description,
            'image': file_name
        }
        return {"product": product, "pricing_units": pricing_units, "category_commission": category_commision, "gst": gst, "tcs": tcs, "tds": tds, "price_varible_attributes": price_attributes, "images": images,  "price_breakup": price_breakup}

    except Exception as e:
        print(e)

# Add Product pricing


@v4_seller_product_router.post('/add/pricing', dependencies=[Depends(JWTBearer())])
async def addPricing(request: Request, data: AddPricingSchema, db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=data.product_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "is_update": False,  "message": "Not Modified"}

        # Minimum Order Qty
        moq = data.moq

        # No of Sets
        nos = data.items

        # No of stock
        stock = data.stock

        # Check Remainder of Moq and No of Sets

        remainder = moq % nos

        if (moq < nos or remainder != 0):
            return {"status_code": HTTP_200_OK, "is_update": False, "message": "Invalid Pack of Moq"}

        # Check Remainder of Stock and Moq
        if(data.unlimited == False):
            remainder1 = stock % nos
            if (stock < moq or remainder1 != 0):
                return {"status_code": HTTP_200_OK, "is_update": False, "message": "Invalid Stock Quantity"}

        # Check Duplicate Entry
        pricings = db.query(ProductPricingModel).filter(
            ProductPricingModel.product_id == data.product_id).filter(ProductPricingModel.deleted_at.is_(None)).all()
        if(len(pricings) > 0):
            for prices in pricings:
                priceattributes = db.query(ProductPricingAttributeModel).filter(
                    ProductPricingAttributeModel.product_pricing_id == prices.id).filter(ProductPricingAttributeModel.attribute_id.in_(data.attributes)).all()

                priceAttributes = db.query(ProductPricingAttributeModel).filter(
                    ProductPricingAttributeModel.product_pricing_id == prices.id).all()

                if(len(priceattributes) == len(priceAttributes) and len(priceAttributes) == len(data.attributes)):
                    return {"status_code": HTTP_200_OK, "is_update": False, "message": "Already Exist"}

        # Insert product pricing
        dbprice = ProductPricingModel(
            product_id=data.product_id,
            price=data.price,
            tax=data.tax,
            hsn=data.hsn,
            mrp=data.mrp,
            moq=data.moq,
            unit=data.unit,
            items=data.items,
            description=data.description,
            seller_amount=data.seller_amount,
            default_image=data.default_image,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )

        db.add(dbprice)
        db.commit()
        db.refresh(dbprice)

        # Insert Inventory
        if(data.unlimited == False):
            addunltd = 0
        else:
            addunltd = 1
        dbinventory = InventoryModel(
            pricing_id=dbprice.id,
            stock=data.stock,
            unlimited=addunltd,
        )

        db.add(dbinventory)
        db.commit()
        db.refresh(dbinventory)

        # Insert into product price attributes
        for attribute in data.attributes:
            dbpriceattributes = ProductPricingAttributeModel(
                product_pricing_id=dbprice.id,
                attribute_id=attribute
            )

            db.add(dbpriceattributes)
            db.commit()
            db.refresh(dbpriceattributes)

        return {"status_code": HTTP_200_OK, "is_update": True, "message": "Created Successfully"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "is_update": False, "message": "Invalid Input"}


# Edit Pricing
@v4_seller_product_router.get('/edit/pricing/{price_id}', dependencies=[Depends(JWTBearer())])
async def getPricingDetails(request: Request, price_id: int, db: Session = Depends(get_db)):
    try:
        # Pricing Detail
        price = db.query(ProductPricingModel).filter(
            ProductPricingModel.id == price_id).first()

        # Product
        product = db.query(ProductModel).filter(
            ProductModel.id == price.product_id).first()

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=product.id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK,  "message": "Not Modified"}

     # Pricing Units
        pricing_units = {
            'Pcs',
            'Box',
            'Mtr',
            'Pair',
            'kg',
            'Bnd',
            'Dzn',
            'Ltr',
            'Qts',
            'Than',
        }

        # Check Inventory
        inventory = db.query(InventoryModel).filter(
            InventoryModel.pricing_id == price.id).first()

        unlimited = False
        if(inventory.unlimited == 1):
            unlimited = True

        out_of_stock = False

        if(inventory.out_of_stock == 1):

            out_of_stock = True
        # price image
        price_image = {
            'id': '0',
            'url': ''
        }

        if(price.default_image != 0):
            price_image = {
                'id': '0',
                'url': ''
            }
            priceimage = db.query(ProductMediaModel).filter(
                ProductMediaModel.id == price.default_image).filter(ProductMediaModel.deleted_at.is_(None)).first()

            if(priceimage is not None):

                price_image = {
                    'id': str(price.default_image),
                    'url': priceimage.file_path
                }

        # GST
        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == product.category).first()
        gst = category.gst
        # New Price with GST (2023-06-05)
        newprice = (float(price.price) * float(price.tax)) / 100
        newprice = (float(newprice) + float(price.price))
        newprice = float(round(newprice, 2))
        pricing = {
            'id': price.id,
            'stock': inventory.stock,
            'unlimited': unlimited,
            'out_of_stock': out_of_stock,
            'price': floatingValue(newprice),
            'items': price.items,
            'tax': floatingValue(price.tax),
            'moq': price.moq,
            'mrp': floatingValue(price.mrp),
            'hsn': price.hsn,
            'unit': price.unit,
            'seller_amount': floatingValue(price.seller_amount),
            'description': price.description,
            'default_image': price_image
        }

        # Product Attributes
        product_attributs = db.query(ProductAttributeModel).filter(
            ProductAttributeModel.product_id == price.product_id).group_by(ProductAttributeModel.attribute_id).all()

        price_attributes = []
        for attributes in product_attributs:
            # Attribute
            attribute = db.query(AttributeModel).filter(
                AttributeModel.id == attributes.attribute_id).first()

            if(attribute.is_price_variable == 1):
                p_attributes = db.query(ProductAttributeModel).filter(
                    ProductAttributeModel.product_id == price.product_id).all()

                price_variable_attributes = []
                for pattribute in p_attributes:
                    pattr = db.query(AttributeModel).filter(
                        AttributeModel.id == pattribute.attribute_id).first()

                    if(pattr.is_price_variable == 1 and attribute.id == pattribute.attribute_id):

                        # Check Selected Attribute Value of pricing
                        selected = False
                        checkproattr = db.query(ProductPricingAttributeModel).filter(
                            ProductPricingAttributeModel.product_pricing_id == price.id).filter(ProductPricingAttributeModel.attribute_id == pattribute.attribute_value_id).first()

                        if(checkproattr):
                            selected = True

                        p_attributes = {
                            'selected': selected,
                            'id': pattribute.attribute_value_id,
                            'value': pattribute.attribute_value
                        }
                        price_variable_attributes.append(p_attributes)

                pp_attributes = {
                    'name': attribute.name,
                    'code': attribute.code,
                    'values': price_variable_attributes
                }

                price_attributes.append(pp_attributes)

        commission = db.query(CategoryCommissionModel).filter(
            CategoryCommissionModel.category_id == category.id).order_by(CategoryCommissionModel.id.desc()).first()

        category_commission = commission.commission

        commission_tax = commission.commission_tax

        # Image List
        images = []
        imagedata = db.query(ProductMediaModel).filter(
            ProductMediaModel.model_id == product.id).filter(ProductMediaModel.deleted_at.is_(None)).all()

        if(len(imagedata) > 0):
            for image in imagedata:
                img = {
                    'id': image.id,
                    'image': image.file_path
                }
                images.append(img)

        return {"status_code": HTTP_202_ACCEPTED, "pricing_units": pricing_units, "pricing": pricing, "price_variable_attributes": price_attributes, "images": images, "category_commission": category_commission, "gst": gst,  "commission_tax": commission_tax}

    except Exception as e:
        print(e)


# Update Product pricing
@v4_seller_product_router.put('/update/pricing/data', dependencies=[Depends(JWTBearer())])
async def updatePricing(request: Request, data: UpdatePricingSchema, db: Session = Depends(get_db)):
    try:

        pricedata = db.query(ProductPricingModel).filter(
            ProductPricingModel.id == data.id).first()

       # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=pricedata.product_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "is_update": False,  "message": "Not Modified"}

        # Minimum Order Qty
        moq = data.moq

        # No of Sets
        nos = data.items

        # No of stock
        stock = data.stock

        # Check Remainder of Moq and No of Sets

        remainder = moq % nos

        if (moq < nos or remainder != 0):
            return {"status_code": HTTP_200_OK, "is_update": False, "message": "Invalid Pack of Moq"}

        # Check Remainder of Stock and Moq
        if(data.unlimited == False):
            remainder1 = stock % nos
            if (stock < moq or remainder1 != 0):
                return {"status_code": HTTP_200_OK, "is_update": False, "message": "Invalid Stock Quantity"}

        # Check Duplicate Entry
        pricings = db.query(ProductPricingModel).filter(ProductPricingModel.id != pricedata.id).filter(
            ProductPricingModel.product_id == pricedata.product_id).filter(ProductPricingModel.deleted_at.is_(None)).all()

        if(len(pricings) > 0):
            for prices in pricings:
                priceattributes = db.query(ProductPricingAttributeModel).filter(
                    ProductPricingAttributeModel.product_pricing_id == prices.id).filter(ProductPricingAttributeModel.attribute_id.in_(data.attributes)).all()

                priceAttributes = db.query(ProductPricingAttributeModel).filter(
                    ProductPricingAttributeModel.product_pricing_id == prices.id).all()

                if(len(priceattributes) == len(priceAttributes) and len(priceAttributes) == len(data.attributes)):
                    return {"status_code": HTTP_200_OK, "is_update": False, "message": "Already Exist"}

        # Check Existing Default Image
        defaultImg = pricedata.default_image
        if(data.default_image != 0):
            defaultImg = data.default_image

        # Update product pricing
        pricedata.price = data.price,
        pricedata.hsn = data.hsn,
        pricedata.mrp = data.mrp,
        pricedata.moq = data.moq,
        pricedata.unit = data.unit,
        pricedata.items = data.items,
        pricedata.description = data.description,
        pricedata.seller_amount = data.seller_amount,
        pricedata.default_image = defaultImg,
        pricedata.updated_at = datetime.now()
        db.flush()
        db.commit()

        # Insert Inventory
        if(data.unlimited == False):
            addunltd = 0
        else:
            addunltd = 1

        dbinventory = db.query(InventoryModel).filter(
            InventoryModel.pricing_id == pricedata.id).first()

        out_of_stock = 0
        stock = data.stock
        if(data.out_of_stock == True):
            out_of_stock = 1
            stock = 0

        dbinventory.stock = stock,
        dbinventory.unlimited = addunltd,
        dbinventory.out_of_stock = out_of_stock

        db.flush()
        db.commit()

        # delete first pricing attributes

        db.query(ProductPricingAttributeModel).filter(
            ProductPricingAttributeModel.product_pricing_id == pricedata.id).delete()

        # Insert into product price attributes
        for attribute in data.attributes:
            dbpriceattributes = ProductPricingAttributeModel(
                product_pricing_id=pricedata.id,
                attribute_id=attribute
            )

            db.add(dbpriceattributes)
            db.commit()
            db.refresh(dbpriceattributes)

        return {"status_code": HTTP_200_OK, "is_update": True, "message": "Updated Successfully"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "is_update": False, "message": "Not Modified"}


# Delete Product Pricing
@v4_seller_product_router.delete("/delete/pricing", dependencies=[Depends(JWTBearer())])
async def deletePricing(request: Request, data: DeleteProducPriceSchema, db: Session = Depends(get_db)):
    try:

        # priceData
        priceData = db.query(ProductPricingModel).filter(
            ProductPricingModel.id == data.id).first()

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=priceData.product_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "is_delete": False,  "message": "Not Modified"}

        db.query(ProductPricingModel).filter(
            ProductPricingModel.id == data.id).update({'deleted_at': datetime.now()})
        db.commit()

        # Check Any Price left
        checkPriceLeft = db.query(ProductPricingModel).filter(
            ProductPricingModel.product_id == priceData.product_id).filter(ProductPricingModel.deleted_at.is_(None)).count()

        if(checkPriceLeft == 0):
            db.query(ProductModel).filter(ProductModel.id ==
                                          priceData.product_id).update({'status': 1})
            db.commit()

        return {"status_code": HTTP_200_OK, "is_delete": True, "message": "Pricing Deleted"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "is_delete": False, "message": "Not modified"}

# Update Price Stock


@v4_seller_product_router.post("/update/price/stock", dependencies=[Depends(JWTBearer())])
async def UpdatePriceStock(request: Request, data: PriceStock, db: Session = Depends(get_db)):
    try:

        # Price Data
        priceData = db.query(ProductPricingModel).filter(
            ProductPricingModel.id == data.id).first()

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=priceData.product_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "is_update": False,  "message": "Not Modified"}

        inventory = db.query(InventoryModel).filter(
            InventoryModel.pricing_id == data.id).first()
        inventory.stock = 0
        inventory.out_of_stock = 1
        db.flush()
        db.commit()

        return {"status_code": HTTP_200_OK, "is_update": True, "message": "Updated Succesfully"}

    except Exception as e:
        print(e)

# Delete Product


@v4_seller_product_router.delete("/delete", dependencies=[Depends(JWTBearer())])
async def deleteProduct(request: Request, data: DeleteProductSchema, db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=data.id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "is_delete": False,  "message": "Not Modified"}

        db.query(ProductModel).filter(ProductModel.id ==
                                      data.id).update({'status': 98})
        db.commit()

        return {"status_code": HTTP_200_OK, "is_delete": True, "message": "Product Deleted"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "is_delete": False, "message": "Invalid Input"}


# Product list category wise
@v4_seller_product_router.get("/categories/{param}", response_model=SellerProductListCategoryWise, dependencies=[Depends(JWTBearer())])
async def getProductsByCategory(request: Request, param: str, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        # user
        userdata = auth(request=request)

        data: ProductModel = await ProductsHelper.getProductCategoryWise(db=db, seller_id=userdata['id'], param=param)

        if(data.count() == 0):
            return {"status_code": HTTP_404_NOT_FOUND, "total_products": 0, "categories": [], "current_page": page, "total_pages": 0}

        total_products = db.query(ProductModel).filter(
            ProductModel.userid == userdata['id']).filter(ProductModel.status != 98).count()

        products: ProductModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        productdata = []
        for product in products:

            # Count total products
            lists = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).filter(
                UserModel.status == 1).filter(ProductModel.category == product.category).filter(ProductModel.userid == userdata['id']).filter(ProductModel.status != 98).group_by(ProductModel.id).count()

            # Category Details
            category = db.query(CategoriesModel).filter(
                CategoriesModel.id == product.category).first()

            image = ''
            if(category.image_path is not None):
                image = category.image_path
            prodata = {
                'id': category.id,
                'name': category.name,
                'description': category.description,
                'image': image,
                'total_items': lists
            }

            productdata.append(prodata)

        # Count Total Products
        total_records = data.count()

        # Number of Total Pages
        total_pages = round((total_records/limit), 0)

        # Check total count Similarity of records
        check = (total_pages * limit)

        if(check == total_records):
            pass
        else:
            total_pages = total_pages + 1

        return {"status_code": HTTP_202_ACCEPTED, "total_products": total_products, "categories": productdata, "current_page": page, "total_pages": round(total_pages)}
    except Exception as e:
        print(e)


# Product List Buttons (Status)
@v4_seller_product_router.get("/list/{category_id}", dependencies=[Depends(JWTBearer())])
async def getProductCounts(request: Request, category_id: int, db: Session = Depends(get_db)):
    try:

        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == category_id).first()

        userdata = auth(request=request)
        # All Products
        AllProducts: ProductModel = await ProductsHelper.getProductList(db=db, category_id=category_id, seller_id=userdata['id'], status='all', search='', from_date='', to_date='')

        # Pending Products
        pendingProducts: ProductModel = await ProductsHelper.getProductList(db=db, category_id=category_id, seller_id=userdata['id'], status='1', search='', from_date='', to_date='')
        # Approved Products
        approvedProducts: ProductModel = await ProductsHelper.getProductList(db=db, category_id=category_id, seller_id=userdata['id'], status='51', search='', from_date='', to_date='')
        # Out of Stock Products
        outofStockProducts: ProductModel = await ProductsHelper.getProductList(db=db, category_id=category_id, seller_id=userdata['id'], status='outstock', search='', from_date='', to_date='')

        # IN Stock Products
        inStockProducts: ProductModel = await ProductsHelper.getProductList(db=db, category_id=category_id, seller_id=userdata['id'], status='instock', search='', from_date='', to_date='')

        # Rejected Products
        rejectedProducts: ProductModel = await ProductsHelper.getProductList(db=db, category_id=category_id, seller_id=userdata['id'], status='99', search='', from_date='', to_date='')

        # Disabled Products
        disabledProducts: ProductModel = await ProductsHelper.getProductList(db=db, category_id=category_id, seller_id=userdata['id'], status='81', search='', from_date='', to_date='')

        category = {
            'category_id': category_id,
            'category_title': category.name,
        }
        product_counts = [
            {
                'status': 'all',
                'title': 'All',
                'count': AllProducts.count(),
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/product_all.svg',
            },
            {
                'status': '1',
                'title': 'Pending',
                'count': pendingProducts.count(),
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/product_pending.svg'
            },
            {
                'status': '51',
                'title': 'Live Products',
                'count': approvedProducts.count(),
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/product_live.svg'
            },
            {
                'status': 'instock',
                'title': 'In Stock',
                'count': inStockProducts.count(),
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/in stock products.svg'
            },
            {
                'status': 'outstock',
                'title': 'Out of Stock',
                'count': outofStockProducts.count(),
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/out%20of%20stock%20products.svg'
            },
            {
                'status': '99',
                'title': 'Rejected',
                'count': rejectedProducts.count(),
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/product_rejected.svg'
            },
            {
                'status': '81',
                'title': 'Hidden Products',
                'count': disabledProducts.count(),
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/product_hidden.svg'
            }
        ]
        return {"status_code": HTTP_200_OK, "category": category, "product_counts": product_counts, "message": "Success"}

    except Exception as e:
        print(e)

# ProductList


@v4_seller_product_router.post("/list/category-wise", response_model=SellerAllProductListSchema, dependencies=[Depends(JWTBearer())])
async def getProducts(request: Request, searchdata: SellerProductSearch,  db: Session = Depends(get_db)):
    try:
        page = searchdata.page
        limit = searchdata.limit

        userdata = auth(request=request)

        data: ProductModel = await ProductsHelper.getProductList(db=db, category_id=searchdata.category_id, seller_id=userdata['id'], status=searchdata.status, search=searchdata.search, from_date=searchdata.from_date, to_date=searchdata.to_date)

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "total_items": 0, "category": "", "items": [], "current_page": page, "total_pages": 0}

        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == searchdata.category_id).first()

        products: ProductModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        productdata = []
        for product in products:

            price = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == product.id).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            stock = False
            total = 0.00
            moq = 0
            unit = ''
            if(price):
                total = (price.price * price.tax) / 100 + price.price

                # Check Out of stock
                checkinventory = db.query(ProductPricingModel).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == product.id).filter(InventoryModel.out_of_stock == 0).filter(ProductPricingModel.deleted_at.is_(None)).first()

                stock = False
                if(checkinventory is not None):
                    stock = True

                moq = price.moq
                unit = price.unit

            status = 'Approved'
            if(product.status == 1):
                status = 'Inactive'
            elif(product.status == 98):
                status = 'Rejected'
            elif(product.status == 81):
                status = 'Hidden'
            elif(product.status == 99):
                status = 'Rejected'

            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                ProductMediaModel.default_img == 1).filter(ProductMediaModel.deleted_at.is_(None)).first()

            if(image == None):
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()

            product_reject = False
            if(product.status == 99):
                rejection = db.query(ProductRejectionModel).filter(
                    ProductRejectionModel.product_id == product.id).first()

                if(rejection):
                    product_reject = True

            mark_disable = False
            if(product.status == 51):
                mark_disable = True

            if(image is not None):
                file_path = image.file_path

            else:
                file_path = ''

            description = ''
            if(product.short_description is not None):
                description = product.short_description
            pro = {
                'id': product.id,
                'name': product.title,
                'description': description,
                'image': file_path,
                'price': floatingValue(round(total, 2)),
                'moq': moq,
                'unit': unit,
                'stock': stock,
                'status': status,
                'product_rejection': product_reject,
                'hide_product': mark_disable,
                'created_at': product.created_at.strftime("%B %d %Y"),


            }

            productdata.append(pro)

        # Count Total Products
        total_records = data.count()

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK,  "total_items": total_records, "category": category.name, "items": productdata, "current_page": page, "total_pages": round(total_pages)}

    except Exception as e:
        print(e)


# Get Product Rejecttion messages
@v4_seller_product_router.get("/rejection/message/{product_id}", dependencies=[Depends(JWTBearer())])
async def RejectMessages(request: Request, product_id: int, db: Session = Depends(get_db)):
    try:

        reject_messages = db.query(ProductRejectionModel).filter(
            ProductRejectionModel.product_id == product_id).all()

        messages = []
        for message in reject_messages:
            msz = {
                'type': message.type,
                'message': message.message
            }
            messages.append(msz)

        return {"status_code": HTTP_200_OK, "reject_messages": messages}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "reject_messages": []}

# Search Products


@v4_seller_product_router.post("/search", response_model=SellerAllProductListSchema, dependencies=[Depends(JWTBearer())])
async def searchProducts(request: Request, search: SellerProductSearchSchema, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        data: ProductModel = await ProductsHelper.getProductList(db=db, category_id=search.category_id, seller_id=userdata['id'], status=search.status, search=search.search, from_date=search.from_date, to_date=search.to_date)

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "total_items": 0, "category": "", "items": [], "current_page": search.page, "total_pages": 0}

        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == search.category_id).first()

        products: ProductModel = data.limit(
            limit=search.limit).offset((search.page - 1) * search.limit).all()

        productdata = []
        for product in products:

            price = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == product.id).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            stock = False
            total = 0.00
            moq = 0
            unit = ''
            if(price):
                total = (price.price * price.tax) / 100 + price.price

                # Check Product for Stock or out Stock
                checkinventory = db.query(ProductPricingModel).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == product.id).filter(InventoryModel.out_of_stock == 0).filter(ProductPricingModel.deleted_at.is_(None)).first()

                stock = False
                if(checkinventory is not None):
                    stock = True

                moq = price.moq
                unit = price.unit

            status = 'Approved'
            if(product.status == 1):
                status = 'Inactive'
            elif(product.status == 98):
                status = 'Rejected'

            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                ProductMediaModel.default_img == 1).filter(ProductMediaModel.deleted_at.is_(None)).first()

            if(image == None):
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()

            pro = {
                'id': product.id,
                'name': product.title,
                'description': product.short_description,
                'image': image.file_path,
                'price': floatingValue(round(total, 2)),
                'moq': moq,
                'unit': unit,
                'stock': stock,
                'status': status,
                'created_at': product.created_at.strftime("%B %d %Y"),


            }

            productdata.append(pro)

        total_records = data.count()
        # Number of Total Pages
        total_pages = str(round((total_records/search.limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK,  "total_items": total_records, "category": category.name, "items": productdata, "current_page": search.page, "total_pages": round(total_pages)}

    except Exception as e:
        print(e)


# delete product image


@v4_seller_product_router.delete("/image/delete", dependencies=[Depends(JWTBearer())])
async def deleteImage(request: Request, data: DeleteImage, db: Session = Depends(get_db)):
    try:

        # Media
        media = db.query(ProductMediaModel).filter(
            ProductMediaModel.id == data.id).first()

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=media.model_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "is_delete": False,  "message": "Not Modified"}

        db.query(ProductMediaModel).filter(
            ProductMediaModel.id == data.id).update({'deleted_at': datetime.now()})
        db.commit()

        # Check No Image Left This Product
        check_no_image_left = db.query(ProductMediaModel).filter(
            ProductMediaModel.model_id == media.model_id).filter(ProductMediaModel.deleted_at.is_(None)).count()

        if(check_no_image_left == 0):
            media.product.status = 1
            db.flush()
            db.commit()

        return {"status_code": HTTP_200_OK, "is_delete": True, "message": "Deleted Successfully"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "is_delete": False, "message": "Not Modified"}


@v4_seller_product_router.get("/overview/{id}", response_model=ProductOverViewSchema, dependencies=[Depends(JWTBearer())])
async def OverView(request: Request, id: int, db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK,  "message": "Not Modified"}

        product = db.query(ProductModel).filter(ProductModel.id == id).first()

        # Check Aseztak Commission
        category_commission = db.query(CategoryCommissionModel).filter(
            CategoryCommissionModel.category_id == product.category).order_by(CategoryCommissionModel.id.desc()).first()
        # Check Brand
        brand = {
            'name': '',
            'logo': ''
        }
        # Changes
        if(product.brand_id is not None and product.brand_id != 1):
            brand = db.query(BrandsModel).filter(
                BrandsModel.id == product.brand_id).first()
            brand = {
                'name': brand.name,
                'logo': brand.logo,
            }

        # images
        images = await ProductsHelper.ProductImages(
            request=request, db=db, product_id=id)

        # Specifications
        specification: ProductAttributeModel = await ProductsHelper.getAttributes(data=product, db=db)

        # Pricing List
        pricings: ProductModel = await ProductsHelper.pricingList(db=db, product_id=id)

        pricing_data = []
        if(len(pricings) > 0):
            for prices in pricings:
                # Check Inventory
                inventory = db.query(InventoryModel).filter(
                    InventoryModel.pricing_id == prices.id).first()

                # Check Stock
                stock = 0
                if(inventory is not None):
                    stock = inventory.stock

                unlimited = False
                if(inventory is not None and inventory.unlimited == 1):
                    unlimited = True

                # Product Image
                filename = ''
                if(prices.default_image != 0):

                    image = db.query(ProductMediaModel).filter(ProductMediaModel.id == prices.default_image).filter(
                        ProductMediaModel.deleted_at.is_(None)).first()

                    if(image is not None):
                        filename = image.file_path

                else:
                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == prices.product_id).filter(ProductMediaModel.default_img == 1).filter(
                        ProductMediaModel.deleted_at.is_(None)).first()

                    if(image == None):
                        image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == prices.product_id).filter(
                            ProductMediaModel.deleted_at.is_(None)).first()

                    if(image is not None):
                        filename = image.file_path

                if(filename != ''):
                    img = filename
                else:
                    img = ''
                # newprice with GST (2023-06-05)
                newprice = (float(prices.price) * float(prices.tax)) / 100
                newprice = (float(newprice) + float(prices.price))
                newprice = float(round(newprice, 2))
                pdata = {
                    'id': prices.id,
                    'image': img,
                    'stock': stock,
                    'unlimited': unlimited,
                    'price': newprice,
                    'mrp': prices.mrp,
                    'tax': prices.tax,
                    'moq': prices.moq,
                    'items': prices.items,
                    'unit': prices.unit,
                    'hsn': prices.hsn,
                    'description': prices.description,
                    'seller_amount': prices.seller_amount,
                    'price_attributes': await ProductsHelper.getPricingAttribjutes(data=prices, db=db),
                    'commission': int(category_commission.commission)
                }

                pricing_data.append(pdata)

        has_attributes = True
        if(len(specification) == 0):
            has_attributes = False

        status = ''
        if(product.status == 1):
            status = 'Pending'
        elif(product.status == 51):
            status = 'Approved'
        else:
            status = 'Rejected'

        # category
        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == product.category).first()

        # Hide Product
        hide_product = False
        if(product.status == 51):
            hide_product = True

        # Check Out of stock
        checkinventory = db.query(ProductPricingModel).join(
            InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == product.id).filter(InventoryModel.out_of_stock == 0).filter(ProductPricingModel.deleted_at.is_(None)).first()

        is_stock = False
        if(checkinventory is not None):
            is_stock = True

        product = {
            'title': product.title,
            'category': category.name,
            'description': product.short_description,
            'brand': brand,
            'images': images,
            'has_attribute': has_attributes,
            'attributes': specification,
            'pricing': pricing_data,
            'status': status,
            'hide_product': hide_product,
            'stock': is_stock,
            'created_at': product.created_at.strftime("%B %d %Y")
        }

        return {"status_code": HTTP_200_OK, "product": product}

    except Exception as e:
        print(e)

# Update stock of all pricings


@v4_seller_product_router.get("/update/stock/{id}", dependencies=[Depends(JWTBearer())])
async def updateStock(request: Request, id: int, db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "is_updated": False, "message": "Not Modified"}

        pricings = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == id).filter(
            ProductPricingModel.deleted_at.is_(None)).all()

        if(len(pricings) > 0):
            for price in pricings:
                inventory = db.query(InventoryModel).filter(
                    InventoryModel.pricing_id == price.id).first()

                if(inventory is not None):
                    inventory.stock = 0
                    inventory.unlimited = 0
                    inventory.out_of_stock = 1

                    db.flush()
                    db.commit()

            return {"status_code": HTTP_200_OK, "is_updated": True, "message": "Updated Successfully"}
        else:
            return {"status_code": HTTP_200_OK, "is_updated": False, "message": "No record found"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "is_updated": False, "message": "Invalid Input"}


# All Products
@v4_seller_product_router.get("/all", response_model=SellerAllProductList, dependencies=[Depends(JWTBearer())])
async def AllProducts(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        userdata = auth(request=request)

        data: ProductModel = await ProductsHelper.getSellerAllProducts(db=db, seller_id=userdata['id'])
        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "total_items": 0, "category": "", "items": [], "current_page": page, "total_pages": 0}

        products: ProductModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        productdata = []
        for product in products:

            price = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == product.id).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            stock = False
            total = 0.00
            moq = 0
            unit = ''
            if(price):
                total = (price.price * price.tax) / 100 + price.price

                # Check Product for Stock or out Stock
                checkinventory = db.query(ProductPricingModel).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == product.id).filter(InventoryModel.out_of_stock == 0).first()

                stock = False
                if(checkinventory is not None):
                    stock = True

                moq = price.moq
                unit = price.unit

            status = 'Approved'
            if(product.status == 1):
                status = 'Inactive'
            elif(product.status == 98):
                status = 'Rejected'

            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                ProductMediaModel.default_img == 1).filter(ProductMediaModel.deleted_at.is_(None)).first()

            if(image == None):
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()
            file_path = ''
            if(image is not None):
                file_path = image.file_path
            pro = {
                'id': product.id,
                'name': product.title,
                'description': product.short_description,
                'image': file_path,
                'price': floatingValue(round(total, 2)),
                'moq': moq,
                'unit': unit,
                'stock': stock,
                'status': status,
                'created_at': product.created_at.strftime("%B %d %Y"),


            }

            productdata.append(pro)

        total_records = data.count()
        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK,  "total_items": total_records, "items": productdata, "current_page": page, "total_pages": round(total_pages)}

    except Exception as e:
        print(e)


# Search All Products
@v4_seller_product_router.post("/search/all", response_model=SellerAllProductList, dependencies=[Depends(JWTBearer())])
async def AllProducts(request: Request, search: SellerSearchAllProducts, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        data: ProductModel = await ProductsHelper.getSellerAllProducts(db=db, seller_id=userdata['id'], search=search.search, from_date=search.from_date, to_date=search.to_date)

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "total_items": 0, "category": "", "items": [], "current_page": search.page, "total_pages": 0}

        products: ProductModel = data.limit(
            limit=search.limit).offset((search.page - 1) * search.limit).all()

        productdata = []
        for product in products:

            price = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == product.id).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            stock = False
            total = 0.00
            moq = 0
            unit = ''
            if(price):
                total = (price.price * price.tax) / 100 + price.price

                # Check Product for Stock or out Stock
                checkinventory = db.query(ProductPricingModel).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == product.id).filter(InventoryModel.out_of_stock == 0).first()

                stock = False
                if(checkinventory is not None):
                    stock = True

                moq = price.moq
                unit = price.unit

            status = 'Approved'
            if(product.status == 1):
                status = 'Inactive'
            elif(product.status == 98):
                status = 'Rejected'

            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                ProductMediaModel.default_img == 1).filter(ProductMediaModel.deleted_at.is_(None)).first()

            if(image == None):
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()
            file_path = ''
            if(image is not None):
                file_path = image.file_path
            pro = {
                'id': product.id,
                'name': product.title,
                'description': product.short_description,
                'image': file_path,
                'price': floatingValue(round(total, 2)),
                'moq': moq,
                'unit': unit,
                'stock': stock,
                'status': status,
                'created_at': product.created_at.strftime("%B %d %Y"),


            }

            productdata.append(pro)

        total_records = data.count()
        # Number of Total Pages
        total_pages = str(round((total_records/search.limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK,  "total_items": total_records, "items": productdata, "current_page": search.page, "total_pages": round(total_pages)}

    except Exception as e:
        print(e)


# Hide Products
@v4_seller_product_router.get("/hide/{pro_id}", dependencies=[Depends(JWTBearer())])
async def disabledProduct(request: Request, pro_id: int, db: Session = Depends(get_db)):
    try:
        # Check User
        checkuser = CheckUser.checkSeller(
            request=request, product_id=pro_id, db=db)

        # Check User and Product Data
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "is_updated": False, "message": "Not Modified"}

        db_product = db.query(ProductModel).filter(
            ProductModel.id == pro_id).first()

        if(db_product.status == 51):
            db_product.status = 81

            db.flush()
            db.commit()
        else:
            if(db_product.status == 81):
                db_product.status = 51

            db.flush()
            db.commit()
        return {"status_code": HTTP_200_OK, "is_updated": True, "message": "Updated Successfully"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "is_updated": False, "message": "Not modified"}

# Active Products


@v4_seller_product_router.post("/top/products", dependencies=[Depends(JWTBearer())])
async def topProducts(request: Request, data: topsaleproductschema, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)
        pastthisrtydaysdate = (datetime.now() -
                               timedelta(days=30))
        pastthisrtydaysdate = pastthisrtydaysdate.strftime("%Y-%m-%d")

        currentdate = date.today()
        currentdate = currentdate.strftime("%Y-%m-%d")
        if(data.from_date != ''):
            from_date = data.from_date
            to_date = data.to_date
        else:
            from_date = pastthisrtydaysdate
            to_date = currentdate

        products_count = db.execute("SELECT ( SELECT COUNT(*)  FROM ( SELECT P.id, P.title, (SELECT SUM(OT.quantity) FROM order_items as OT LEFT JOIN order_fails as ofail ON ofail.order_id = OT.order_id WHERE ofail.order_id is NULL AND OT.product_id=order_items.product_id AND OT.status != 980 AND date_format(OT.created_at, '%Y-%m-%d') >=:from_date AND date_format(OT.created_at, '%Y-%m-%d') <=:to_date) as sold_quantity,(SELECT SUM(OT1.quantity) FROM order_items as OT1 LEFT JOIN order_fails as ofail1 ON ofail1.order_id = OT1.order_id WHERE ofail1.order_id is NULL AND OT1.product_id=order_items.product_id AND OT1.status BETWEEN 70 AND 81 AND date_format(OT1.created_at, '%Y-%m-%d') >=:from_date AND date_format(OT1.created_at, '%Y-%m-%d') <=:to_date) as delivered_quantity,(SELECT SUM(OT2.quantity) FROM order_items as OT2 LEFT JOIN order_fails as ofail2 ON ofail2.order_id = OT2.order_id WHERE ofail2.order_id is NULL AND date_format(OT2.created_at, '%Y-%m-%d') >=:from_date AND date_format(OT2.created_at, '%Y-%m-%d') <=:to_date AND OT2.product_id=order_items.product_id AND OT2.status != 70 AND OT2.status != 80 AND OT2.status != 81 AND OT2.status != 980) as pending_quantity FROM  order_items LEFT JOIN order_fails ON order_fails.order_id = order_items.order_id LEFT JOIN products as P ON P.id=order_items.product_id WHERE order_fails.order_id is NULL AND date_format(order_items.created_at, '%Y-%m-%d') >=:from_date AND date_format(order_items.created_at, '%Y-%m-%d') <=:to_date AND order_items.status != 980  AND P.userid =:user_id GROUP BY order_items.product_id ORDER BY sold_quantity DESC) as count) as total_count", {
            "from_date": from_date,
            "to_date": to_date,
            "user_id": userdata['id']
        }).first()
        offset = (data.page - 1) * data.limit
        products = db.execute("SELECT P.id, P.title, (SELECT SUM(OT.quantity) FROM order_items as OT LEFT JOIN order_fails as ofail ON ofail.order_id = OT.order_id WHERE ofail.order_id is NULL AND OT.product_id=order_items.product_id AND OT.status != 980 AND date_format(OT.created_at, '%Y-%m-%d') >=:from_date AND date_format(OT.created_at, '%Y-%m-%d') <=:to_date) as sold_quantity,(SELECT SUM(OT1.quantity) FROM order_items as OT1 LEFT JOIN order_fails as ofail1 ON ofail1.order_id = OT1.order_id  WHERE ofail1.order_id is NULL AND OT1.product_id=order_items.product_id AND OT1.status BETWEEN 70 AND 81 AND date_format(OT1.created_at, '%Y-%m-%d') >=:from_date AND date_format(OT1.created_at, '%Y-%m-%d') <=:to_date) as delivered_quantity,(SELECT SUM(OT2.quantity) FROM order_items as OT2 LEFT JOIN order_fails as ofail2 ON ofail2.order_id = OT2.order_id WHERE ofail2.order_id is NULL AND date_format(OT2.created_at, '%Y-%m-%d') >=:from_date AND date_format(OT2.created_at, '%Y-%m-%d') <=:to_date AND OT2.product_id=order_items.product_id AND OT2.status != 70 AND OT2.status != 80 AND OT2.status != 81 AND OT2.status != 980) as pending_quantity FROM  order_items LEFT JOIN order_fails ON order_fails.order_id = order_items.order_id LEFT JOIN products as P ON P.id=order_items.product_id WHERE order_fails.order_id is NULL AND date_format(order_items.created_at, '%Y-%m-%d') >=:from_date AND date_format(order_items.created_at, '%Y-%m-%d') <=:to_date AND order_items.status != 980  AND P.userid =:user_id GROUP BY order_items.product_id ORDER BY sold_quantity DESC LIMIT :limit OFFSET :offset", {
            "from_date": from_date,
            "to_date": to_date,
            "user_id": userdata['id'],
            "limit": data.limit,
            "offset": offset
        }).all()
        products_count = products_count.total_count

        product_list = []
        if(products_count != 0):
            for product in products:
                if(product.delivered_quantity is None):
                    delivered_quantity = 0
                else:
                    delivered_quantity = product.delivered_quantity

                if(product.pending_quantity is None):
                    pending_quantity = 0
                else:
                    pending_quantity = product.pending_quantity
                # Check Image
                file = ''
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product.id).order_by(
                    ProductMediaModel.default_img.desc()).first()
                if(image is not None):
                    file = image.file_path
                # check pricing
                pricing = db.query(ProductPricingModel).filter(
                    ProductPricingModel.product_id == product.id).first()
                # Order Count
                check_order_count = db.execute("SELECT COUNT(*) as total_count  FROM ( SELECT orders.id FROM orders LEFT JOIN order_items as OT ON OT.order_id = orders.id LEFT JOIN order_status as OS ON OS.order_id = orders.id LEFT JOIN products as P ON P.id = OT.product_id LEFT JOIN order_fails  ON order_fails.order_id = orders.id WHERE date_format(OT.created_at, '%Y-%m-%d') >=:from_date AND date_format(OT.created_at, '%Y-%m-%d') <=:to_date AND OT.product_id =:product_id AND order_fails.order_id is NULL AND OT.status != 980 GROUP BY orders.id) as counts", {
                    "from_date": from_date,
                    "to_date": to_date,
                    "product_id": product.id
                }).first()
                p = {
                    'id': product.id,
                    'image': file,
                    'title': product.title,
                    'order_count': check_order_count.total_count,
                    'sold_quantity': product.sold_quantity,
                    'delivered_quantity': delivered_quantity,
                    'return_pending_quantity': pending_quantity,
                    'unit': str(pricing.unit)
                }
                product_list.append(p)

        total_records = products_count

        # Number of Total Pages
        total_pages = str(round((total_records/data.limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1
        return {"status_code": HTTP_200_OK, "products": product_list, "total_products": products_count, "current_page": data.page, "total_pages": total_pages}
    except Exception as e:
        print(e)
