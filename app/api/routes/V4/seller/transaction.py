
from starlette.requests import Request
from starlette.status import HTTP_200_OK
from app.api.util.calculation import floatingValue
from app.api.helpers.media import MediaHelper
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.users import *
from app.services.auth import auth
from app.db.models.acconts import AccountsModel
from app.api.helpers.accounts import AccountsHelper
from fastapi import APIRouter,  Depends
from app.db.schemas.transaction_schema import ExportAccountTransaction, TransactionSchema, SearchTransaction
import pandas as pd
from os import getcwd, unlink
from app.db.models.user import UserModel
v4_seller_transaction_router = APIRouter()


# Seller Transaction
@v4_seller_transaction_router.post("/search", response_model=TransactionSchema, dependencies=[Depends(JWTBearer())])
async def SearchTransactionHistory(request: Request, search: SearchTransaction, db: Session = Depends(get_db)):
    transaction_data = []
    try:

        userdata = auth(request=request)

        # Transaction
        if(search.from_date != '' and search.to_date != ''):
            data: AccountsModel = await AccountsHelper.TransactionHistory(db=db, user_id=userdata['id'], from_date=search.from_date, to_date=search.to_date)
        else:
            data: AccountsModel = await AccountsHelper.TransactionHistory(db=db, user_id=userdata['id'])
        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "closing_balance": 0.00, "transactions": [], "current_page": search.page, "total_pages": 0, "message": "No record found"}

        if(search.from_date == '' and search.to_date == ''):
            debit_balance = 0
            credit_balance = 0
            for balance in data.all():
                if(str('DR') in str(balance.txn_type)):
                    debit_balance += balance.txn_amount
                else:
                    credit_balance += balance.txn_amount

            debit_balance = round(debit_balance, 2)
            credit_balance = round(credit_balance, 2)

            openning_balance = (debit_balance - credit_balance)
            if(debit_balance > credit_balance):
                opening_blnc = {
                    'balance': str(round(openning_balance, 2)),
                    'txn_type': 'DR'
                }
            else:
                opening_blnc = {
                    'balance': str(round(openning_balance, 2)),
                    'txn_type': 'CR'
                }
        else:
            checkbalance = db.query(AccountsModel).filter(
                AccountsModel.user_id == userdata['id']).filter(AccountsModel.txn_date < search.from_date).all()

            debit_balance = 0
            credit_balance = 0
            if(len(checkbalance) > 0):
                for balance in checkbalance:
                    if(str('DR') in str(balance.txn_type)):
                        debit_balance += balance.txn_amount
                    else:
                        credit_balance += balance.txn_amount

            debit_balance = round(debit_balance, 2)
            credit_balance = round(credit_balance, 2)

            openning_balance = (debit_balance - credit_balance)
            if(debit_balance > credit_balance):
                opening_blnc = {
                    'balance': str(round(openning_balance, 2)),
                    'txn_type': 'DR'
                }
            else:
                opening_blnc = {
                    'balance': str(round(openning_balance, 2)),
                    'txn_type': 'CR'
                }

        transactions: AccountsModel = data.limit(
            limit=search.limit).offset((search.page - 1) * search.limit).all()

        for transaction in transactions:
            debit = 0
            credit = 0
            if(str('DR') in str(transaction.txn_type)):
                debit = round(transaction.txn_amount, 2)
            else:
                credit = round(transaction.txn_amount, 2)

            if(str('DR') in str(transaction.txn_type)):
                clear_balance = openning_balance + debit
                type = ' (Debit)'
            else:
                clear_balance = openning_balance - credit
                type = ' (Credit)'

            openning_balance = clear_balance

            txn = {
                'date': transaction.txn_date.strftime("%d %B %Y"),
                'head': transaction.head,
                'description': transaction.txn_description,
                'debit': floatingValue(round(debit, 2)),
                'credit': floatingValue(round(credit, 2)),
                'balance': str(round(openning_balance, 2)) + type,
                'txn_type': transaction.txn_type
            }

            transaction_data.append(txn)

        # Count Total Products
        total_records = data.count()

        # Number of Total Pages
        total_pages = str(round((total_records/search.limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0])+1

        return {"status_code": HTTP_200_OK, "opening_balance": opening_blnc, "transactions": transaction_data, "current_page": search.page, "total_pages": total_pages, "message": "Success"}

    except Exception as e:
        print(e)


@v4_seller_transaction_router.post("/export", dependencies=[Depends(JWTBearer())])
async def ExportTransaction(request: Request, search: ExportAccountTransaction, db: Session = Depends(get_db)):
    transaction_data = []
    try:
        userdata = auth(request=request)
        userdata = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        userdata = {
            'id': userdata.id,
            'name': userdata.name,
            'mobile': userdata.mobile,
            'email': userdata.email,
            'region': userdata.region,
            'city': userdata.city,
            'pincode': userdata.pincode
        }

        # Transaction
        if(search.from_date != '' and search.to_date != ''):
            data: AccountsModel = await AccountsHelper.TransactionHistory(db=db, user_id=userdata['id'], from_date=search.from_date, to_date=search.to_date)

            # rename csv file
            username = userdata['name']
            username = username.replace(" ", "-").lower()

            statement = str('AccountStatement-') + \
                str(username) + '-' + \
                str(userdata['id'])+str(search.from_date) + \
                str(search.to_date)+str('.csv')

        else:
            data: AccountsModel = await AccountsHelper.TransactionHistory(db=db, user_id=userdata['id'], from_date='', to_date='')
            # rename csv file
            username = userdata['name']
            username = username.replace(" ", "-").lower()

            statement = str('AccountStatement-') + \
                str(username) + '-'+str(userdata['id'])+str('.csv')

        if(data.count() == 0):
            return {"file": ''}

        checkbalance = db.query(AccountsModel).filter(
            AccountsModel.user_id == userdata['id']).filter(AccountsModel.txn_date < search.from_date).all()

        debit_balance = 0
        credit_balance = 0
        if(len(checkbalance) > 0):
            for balance in checkbalance:
                if(str('DR') in str(balance.txn_type)):
                    debit_balance += balance.txn_amount
                else:
                    credit_balance += balance.txn_amount

        debit_balance = round(debit_balance, 2)
        credit_balance = round(credit_balance, 2)

        openning_balance = (debit_balance - credit_balance)
        if(debit_balance > credit_balance):
            opening_blnc = str(round(openning_balance, 2)) + ' DR '
        else:
            opening_blnc = str(round(openning_balance, 2)) + ' CR '

        transactions: AccountsModel = data.all()

        for transaction in transactions:
            debit = 0
            credit = 0
            if(str('DR') in str(transaction.txn_type)):
                debit = round(transaction.txn_amount, 2)
            else:
                credit = round(transaction.txn_amount, 2)

            if(str('DR') in str(transaction.txn_type)):
                clear_balance = openning_balance + debit
                type = ' (Debit)'
            else:
                clear_balance = openning_balance - credit
                type = ' (Credit)'

            openning_balance = clear_balance
            txn_type = 'DR'
            if('CR' in str(transaction.txn_type)):
                txn_type = 'CR'
            txn = {
                'Opening Balance': '',
                opening_blnc: '',
                'Date': transaction.txn_date.strftime("%d %B %Y"),
                'Description': transaction.txn_description,
                'Debit': floatingValue(round(debit, 2)),
                'Credit': floatingValue(round(credit, 2)),
                'Balance': str(round(openning_balance, 2)) + type,
                'Txn Type': txn_type
            }

            transaction_data.append(txn)

        marks_data = pd.DataFrame(transaction_data
                                  )

        # saving the csv
        uploaded_file = getcwd()+"/app/static/transactions/"+str(statement)
        marks_data.to_csv(uploaded_file, encoding='utf-8',
                          index=False)

        # Delete File From Account Transaction
        MediaHelper.deleteFile(bucket_name='transaction', file_name=statement)

        # Upload FILE
        MediaHelper.uploadAccountTransaction(
            uploaded_file=uploaded_file, file_name=statement)

        # Remove file from Path
        unlink(uploaded_file)

        file_path = str(
            linode_obj_config['endpoint_url'])+'/transaction/'+str(statement)

        return {"file": file_path}
    except Exception as e:
        return {"file": ""}
