
from datetime import datetime
from starlette.requests import Request
from app.db.models.pickup_address import PickupLocationModel
from app.db.schemas.address_schema import AddPickupLocationSchema, PickupLocationSchema, UpdatePickupLocationSchema, DeleteAddressSchema
from app.services.auth import auth
from starlette.status import HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED, HTTP_404_NOT_FOUND
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.products import *
from app.services.auth import auth
from fastapi import APIRouter,  Depends
import requests
import json


v4_seller_address_router = APIRouter()

# Address List


@v4_seller_address_router.get("/", response_model=PickupLocationSchema, dependencies=[Depends(JWTBearer())])
async def getAddress(request: Request, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        addresses = db.query(PickupLocationModel).filter(
            PickupLocationModel.seller_id == userdata['id']).filter(PickupLocationModel.status == 51).order_by(PickupLocationModel.id.desc()).all()

        if(len(addresses) == 0):
            return {"status_code": HTTP_404_NOT_FOUND, "total_address": 0, "addresses": [], "message": "No data found"}

        address_data = []

        for address in addresses:
            if(address.email == None):
                address.email = ''

            if(address.registered_name == None):
                address.registered_name = ''

            if(address.return_state == None):
                address.return_state = ''

            add = {
                'id': address.id,
                'name': address.name,
                'email': address.email,
                'phone':  address.phone,
                'pincode': address.pin,
                'city': address.city,
                'address':  address.address,
                'country': address.country,
                'registered_name': address.registered_name,
                'return_address': address.return_address,
                'return_pin': address.return_pin,
                'return_city': address.return_city,
                'return_state': address.return_state,
                'return_country': address.return_country,
                'created_at': address.created_at.strftime('%B %d %Y'),
            }

            address_data.append(add)

        return {"status_code": HTTP_202_ACCEPTED, "total_address": len(addresses),  "addresses": address_data}

    except Exception as e:
        print(e)


# Add Pickup Location
@v4_seller_address_router.post("/add", dependencies=[Depends(JWTBearer())])
async def addAddress(request: Request, data: AddPickupLocationSchema, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        # Upload warehouse data in Delhivery Panel
        payload = {
            "phone": data.phone,
            "city": data.city,
            "name": data.name,
            "pin": data.pincode,
            "address": data.address,
            "country": data.country,
            "email": data.email,
            "registered_name": data.registered_name,
            "return_address": data.return_address,
            "return_pin": data.return_pin,
            "return_city": data.return_city,
            "return_state": data.return_state,
            "return_country": data.return_country
        }
        headers = {
            "Authorization": "Token "+str(API_TOKEN),
            "Content-Type": "application/json",
            "Accept": "application/json"
        }

        response = requests.request(
            "POST", API_WAREHOUSE_URL, json=payload, headers=headers)

        response = json.loads(response.content.decode('utf-8'))

        if(response['success'] == True):

            dbaddress = PickupLocationModel(
                seller_id=userdata['id'],
                name=data.name.strip(),
                email=data.email,
                phone=data.phone,
                city=data.city,
                country=data.country,
                pin=data.pincode,
                address=data.address,
                registered_name=data.registered_name,
                return_address=data.return_address,
                return_pin=data.return_pin,
                return_city=data.return_city,
                return_state=data.return_state,
                return_country=data.return_country,
                status=51,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )

            db.add(dbaddress)
            db.commit()
            db.refresh(dbaddress)

            return {"is_added": True, "message": "Warehouse Added Successfully"}
        else:

            if(response['error_code'][0] == 1005):
                message = response['error'][0]
            else:
                message = response['error'][0].split('name: ')[1]
            return {"is_added": False, "message": message}
    except Exception as e:
        print(e)


@v4_seller_address_router.get("/edit/{id}", dependencies=[Depends(JWTBearer())])
async def editAddress(request: Request, id: int, db: Session = Depends(get_db)):
    try:

        # user
        userdata = auth(request=request)

        address = db.query(PickupLocationModel).filter(PickupLocationModel.seller_id == userdata['id']).filter(
            PickupLocationModel.id == id).first()

        if(address is None):
            return {"status_code": HTTP_304_NOT_MODIFIED, "address": {}, 'message': 'Not Modified'}
        address = {
            'name': address.name,
            'email': address.email,
            'phone':  address.phone,
            'pincode': address.pin,
            'city': address.city,
            'address':  address.address,
            'country': address.country,
            'registered_name': address.registered_name,
            'return_address': address.return_address,
            'return_pin': address.return_pin,
            'return_city': address.return_city,
            'return_state': address.return_state,
            'return_country': address.return_country,
        }

        return {"status_code": HTTP_202_ACCEPTED, "address": address}

    except Exception as e:
        print(e)


@v4_seller_address_router.post("/update", dependencies=[Depends(JWTBearer())])
async def updateAddress(request: Request, data: UpdatePickupLocationSchema, db: Session = Depends(get_db)):

    try:
        # user
        userdata = auth(request=request)

        address = db.query(PickupLocationModel).filter(
            PickupLocationModel.id == data.id).first()

        if(userdata['id'] != address.seller_id):
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

        # # Check Addres by name
        # search = "%{}%".format(data.name)
        # checkAddress = db.query(PickupLocationModel).filter(
        #     PickupLocationModel.name == search).first()

        # if(checkAddress):
        #     return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Address already exist"}

        address.name = data.name,
        address.email = data.email,
        address.phone = data.phone,
        address.city = data.city,
        address.country = data.country,
        address.pin = data.pincode,
        address.address = data.address,
        address.registered_name = data.registered_name,
        address.return_address = data.return_address,
        address.return_pin = data.return_pin,
        address.return_city = data.return_city,
        address.return_state = data.return_state,
        address.return_country = data.return_country,
        address.updated_at = datetime.now()

        db.flush()
        db.commit()

        # Upload warehouse data in Delhivery Panel

        payload = {
            "phone": data.phone,
            "city": data.city,
            "name": data.name,
            "pin": data.pincode,
            "address": data.address,
            "country": data.country,
            "email": data.email,
            "registered_name": data.registered_name,
            "return_address": data.return_address,
            "return_pin": data.return_pin,
            "return_city": data.return_city,
            "return_state": data.return_state,
            "return_country": data.return_country
        }
        headers = {
            "Authorization": "Token "+str(API_TOKEN),
            "Content-Type": "application/json",
            "Accept": "application/json"
        }

        response = requests.request(
            "POST", API_WAREHOUSE_EDIT_URL, json=payload, headers=headers)

        response = json.loads(response.content.decode('utf-8'))

        if(response['success'] == True):
            return {"is_updated": True, "message": "Warehouse Updated Successfully"}
        else:

            if(response['error_code'][0] == 1005):
                message = response['error'][0]
            else:
                message = response['error'][0].split('name: ')[1]
            return {"is_updated": False, "message": message}

    except Exception as e:
        print(e)


@v4_seller_address_router.post("/delete", dependencies=[Depends(JWTBearer())])
async def deleteAddress(request: Request, data:  DeleteAddressSchema, db: Session = Depends(get_db)):
    try:
        address = db.query(PickupLocationModel).filter(
            PickupLocationModel.id == data.id).first()
        address.status = 98
        db.flush()
        db.commit()
        return {"is_deleted": True, "message": "Deleted..."}
    except Exception as e:
        return {"is_deleted": False, "message": "Not modified"}
