from starlette.requests import Request
from starlette.status import HTTP_202_ACCEPTED
from app.db.models.bank_accounts import BankAccountsModel
from app.db.models.billing_address import BillingAddressModel


from app.db.schemas.user_schema import  SellerProfileUpdateSchema, UpdateMobileSchema, UpdateMobileNumberSchema

from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.users import *
from app.services.auth import auth


from fastapi import APIRouter,  Depends, status

v4_seller_users_router = APIRouter()


@v4_seller_users_router.get("/me", dependencies=[Depends(JWTBearer())])
async def me(request: Request, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        # user
        user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        # profile
        profile = db.query(UserProfileModel).filter(
            UserProfileModel.user_id == user.id).first()

        profile_exist = False
        business_type = ''
        proof_type = ''
        proof = ''
        if(profile):
            profile_exist = True
            business_type = profile.business_type
            proof_type = profile.proof_type
            proof = profile.proof

        # billing
        billing_address = db.query(BillingAddressModel).filter(
            BillingAddressModel.user_id == user.id).first()
        billing_address_exist = False
        if(billing_address):
            billing_address_exist = True

        # bank account
        bank_account = db.query(BankAccountsModel).filter(
            BankAccountsModel.user_id == user.id).first()
        bank_account_exist = False
        if(bank_account):
            bank_account_exist = True

        user = {
            'name': user.name,
            'email': user.email,
            'mobile': user.mobile,
            'country': user.country,
            'region': user.region,
            'city': user.city,
            'pincode': user.pincode,
            'business_type': business_type,
            'proof_type': proof_type,
            'proof': proof,
            'status': user.status
        }

        return {"status_code": HTTP_202_ACCEPTED, "user": user, "profile_exist": profile_exist, "bank_account_exist": bank_account_exist, "billing_address_exist": billing_address_exist}
    except Exception as e:
        print(e)


@v4_seller_users_router.post("/update/profile", dependencies=[Depends(JWTBearer())])
async def update_profile(request: Request, user: SellerProfileUpdateSchema, db: Session = Depends(get_db)):

    userdata = auth(request=request)

    try:
        user = await sellerProfileUpdate(db=db, data=user, user_id=userdata['id'])

        if(user):
            return {'status_code': status.HTTP_202_ACCEPTED, 'message': 'Updated Successfully', 'user': user}
        else:
            return {'status_code': status.HTTP_400_BAD_REQUEST, 'message': 'Invalid Input', 'user': {}}
    except Exception as e:
        print(e)


# Change Mobile Number
@v4_seller_users_router.post("/change/mobile", dependencies=[Depends(JWTBearer())])
async def change_mobile(request: Request, user: UpdateMobileSchema, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)
        # New changes
        userdata = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        userdata = {
            'id': userdata.id,
            'name': userdata.name,
            'mobile': userdata.mobile,
            'region': userdata.region,
            'city': userdata.city,
            'pincode': userdata.pincode
        }

        user = await SellerChangeMobileNumber(db=db, data=user, mobile=userdata['mobile'])

        if(user['status'] == True):
            return {'status_code': status.HTTP_200_OK, 'message': 'Otp sent to your mobile number', 'user': user['user']}
        else:
            return {'status_code': status.HTTP_304_NOT_MODIFIED, 'message': 'Mobile Number Already Exist', 'user': user['user']}
    except Exception as e:
        print(e)


@v4_seller_users_router.post("/update/mobile", dependencies=[Depends(JWTBearer())])
async def update_mobile(request: Request, user: UpdateMobileNumberSchema, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)
        userdata = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        user = await SellerupdateMobileNumber(db=db, data=user, mobile=userdata.mobile)

        if(user['status'] == True):
            return {'status_code': status.HTTP_200_OK, 'message': 'Mobile Number Updated', 'user':  user['user']}
        else:
            return {'status_code': status.HTTP_304_NOT_MODIFIED, 'message': 'Invalid otp', 'user': {}}
    except Exception as e:
        print(e)

# user bank account


# @users_router.post("/send-otp", tags=["user"])
# async def sendOtp(mobile: str, db: Session = Depends(get_db)):
#     try:
#         exist = userExist(db=db, mobile=mobile)
#         if(exist):
#             return {'status': True, 'message': "OTP sent to your Mobile"}
#         else:
#             return {'status': False, 'message': "Invalid Credential "}
#     except Exception as e:
#         print(e)
