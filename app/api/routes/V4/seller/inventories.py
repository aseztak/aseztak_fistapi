from fastapi import APIRouter,  Depends, Request
from sqlalchemy.orm.session import Session


from app.api.helpers.auth import *
from app.api.helpers.products import ProductsHelper

from app.db.config import get_db

from app.db.models.media import ProductMediaModel

from app.db.models.products import InventoryModel, ProductModel, ProductPricingModel

from app.db.schemas.auth_schema import *
from app.db.schemas.product_schema import CountStockProductsSchema, StockProductsSchema


from app.services.auth import auth
from app.resources.strings import *
from app.api.helpers.users import *
from app.services.auth_bearer import JWTBearer

from starlette.status import HTTP_202_ACCEPTED, HTTP_200_OK
from starlette.requests import Request


v4_seller_inventories_router = APIRouter()


@v4_seller_inventories_router.get("/", response_model=CountStockProductsSchema, dependencies=[Depends(JWTBearer())])
async def countProducst(request: Request, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        # All Products
        all_products: ProductModel = await ProductsHelper.listOfStockProducts(db=db, user_id=userdata['id'], stock_type=999)

        out_stock_products = db.query(ProductModel.id, InventoryModel.out_of_stock).join(ProductPricingModel, ProductPricingModel.product_id ==
                                                                                         ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.deleted_at.is_(None)).filter(ProductModel.userid == userdata['id']).filter(InventoryModel.out_of_stock != 0).filter(ProductModel.status < 98).group_by(ProductModel.id).count()

        in_stock_products = db.query(ProductModel.id, InventoryModel.out_of_stock).join(ProductPricingModel, ProductPricingModel.product_id ==
                                                                                        ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.deleted_at.is_(None)).filter(ProductModel.userid == userdata['id']).filter(InventoryModel.out_of_stock != 1).filter(ProductModel.status < 98).group_by(ProductModel.id).count()

        low_stock_products = db.query(ProductModel.id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(
            InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.deleted_at.is_(None)).filter(ProductModel.userid == userdata['id']).filter(InventoryModel.out_of_stock == 0).filter(InventoryModel.stock == (ProductPricingModel.items + ProductPricingModel.items)).filter(ProductModel.status < 98).group_by(ProductModel.id).count()

        count_products = [
            {
                'type': 999,
                'title': 'All Products',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/products.svg',
                'count': (all_products.count())
            },
            {
                'type': 1,
                'title': 'In Stock Products',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/in stock products.svg',
                'count': in_stock_products
            },
            {
                'type': 0,
                'title': 'Out of Stock Products',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/out of stock products.svg',
                'count': out_stock_products
            },
            {
                'type': 998,
                'title': 'Low Stock Products',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/svg_icons/low stock products.svg',
                'count': low_stock_products
            }
        ]

        return {"status_code": HTTP_202_ACCEPTED, "count_products": count_products}
    except Exception as e:
        print(e)


# Get Pricing List
@v4_seller_inventories_router.get("/stock/list/{stock_type}", response_model=StockProductsSchema, dependencies=[Depends(JWTBearer())])
async def listOfProducts(request: Request, stock_type: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        userdata = auth(request=request)

        data: ProductModel = await ProductsHelper.listOfStockProducts(db=db, user_id=userdata['id'], stock_type=stock_type)

        productdata = []
        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "total_items": 0, "items": productdata, "current_page": page, "total_pages": 0}

        products: ProductModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        for product in products:

            # Product Detail
            product = db.query(ProductModel).filter(
                ProductModel.id == product.id).first()

            if(stock_type == 998):
                pricing = db.query(ProductPricingModel).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(
                    ProductPricingModel.product_id == product.id).filter(InventoryModel.stock == (ProductPricingModel.items + ProductPricingModel.items)).filter(ProductPricingModel.deleted_at.is_(None)).first()
            elif(stock_type == 999):
                pricing = db.query(ProductPricingModel).filter(
                    ProductPricingModel.product_id == product.id).filter(ProductPricingModel.deleted_at.is_(None)).first()
            else:
                pricing = db.query(ProductPricingModel).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(
                    ProductPricingModel.product_id == product.id).filter(InventoryModel.out_of_stock != stock_type).filter(ProductPricingModel.deleted_at.is_(None)).first()

            if(pricing is not None):
                if(pricing.default_image == 1):
                    checkMedia = db.query(ProductMediaModel).filter(ProductMediaModel.id == pricing.default_image).filter(
                        ProductMediaModel.deleted_at.is_(None)).first()

                else:
                    checkMedia = db.query(ProductMediaModel).filter(
                        ProductMediaModel.model_id == product.id).filter(ProductMediaModel.default_img == 1).first()

                    if(checkMedia == None):
                        checkMedia = db.query(ProductMediaModel).filter(
                            ProductMediaModel.model_id == product.id).filter(ProductMediaModel.deleted_at.is_(None)).first()

                file_path = ''
                if(checkMedia is not None):
                    file_path = checkMedia.file_path

                # Check Stock
                inventory = db.query(InventoryModel).filter(
                    InventoryModel.pricing_id == pricing.id).first()

                price_attributes = await ProductsHelper.getPricingAttribjutes(data=pricing, db=db)

                unlimted = True
                if(inventory.unlimited == 0):
                    unlimted = False

                pricing = {
                    'price_id': pricing.id,
                    'price': pricing.price,
                    'moq': pricing.moq,
                    'unit': pricing.unit,
                    'stock': inventory.stock,
                    'unlimted': unlimted,
                    'price_attributes': price_attributes
                }

                pro = {
                    'id': product.id,
                    'title': product.title,
                    'description': product.short_description,
                    'created_at': product.created_at.strftime("%B %d %Y"),
                    'image': file_path,
                    'pricing': pricing
                }

                productdata.append(pro)

           # Count Total Products
        total_records = data.count()

        # Number of Total Pages
        total_pages = str(round((total_records/10), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK, "total_items": total_records, "items": productdata, "current_page": page, "total_pages": total_pages}
    except Exception as e:

        return {"status_code": HTTP_200_OK, "total_items": 0, "items": [], "current_page": page, "total_pages": 0}
