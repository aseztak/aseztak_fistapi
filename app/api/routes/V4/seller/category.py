from fastapi.param_functions import Depends
from fastapi.routing import APIRouter
from starlette.requests import Request
from sqlalchemy.orm.session import Session
from app.api.helpers.categories import CategoriesHelper
from app.db.config import get_db
from app.db.models.categories import CategoriesModel
from app.db.schemas.category_schema import SelectableCategorySchema, searchselectablecategories
from starlette.status import HTTP_200_OK
from app.services.auth_bearer import JWTBearer

v4_seller_categories_route = APIRouter()


@v4_seller_categories_route.post("/selectable", response_model=SelectableCategorySchema, dependencies=[Depends(JWTBearer())])
async def getCategories(request: Request, data: searchselectablecategories, db: Session = Depends(get_db)):
    try:
        page = data.page
        limit = data.limit

        if(data.search == '' or data.search == 'search'):
            search = 'search'
        else:
            search = data.search
        data: CategoriesModel = await CategoriesHelper.selectableCategories(db=db, search=search)
        if(data.count() == 0):

            return {"status_code": HTTP_200_OK, "categories": [], "current_page": page, "total_pages": 0}
        else:

            categories_data: CategoriesModel = data.limit(
                limit=limit).offset((page - 1) * limit).all()
            categories = []
            for category in categories_data:

                if(category.image_path == None):
                    image = ''
                else:
                    image = category.image_path
                description = ''
                if(category.description is not None):
                    description = category.description.rstrip()
                cat = {
                    'id': category.id,
                    'name': category.name,
                    'description': description,
                    'image': image,
                    'status': category.status
                }
                categories.append(cat)

            # Total Records
            total_records = data.count()

            # Number of Total Pages
            total_pages = str(round((total_records/limit), 2))

            total_pages = total_pages.split('.')

            if(total_pages[1] != 0):
                total_pages = int(total_pages[0]) + 1

            return {"status_code": HTTP_200_OK, "total_count": data.count(), "current_page": page, "total_pages": total_pages, "categories": categories}

    except Exception as e:

        return {"status_code": HTTP_200_OK, "categories": [], "current_page": page, "total_pages": 0}
