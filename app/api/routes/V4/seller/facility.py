from fastapi import APIRouter

from starlette.requests import Request
from app.db.schemas.user_schema import FacilitySchema

from app.services.auth import auth
from starlette.status import HTTP_200_OK
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.products import *
from app.services.auth import auth
from fastapi import APIRouter,  Depends
from starlette.requests import Request


v4_seller_facility_router = APIRouter()


# Seller Facility
@v4_seller_facility_router.get("/list",  dependencies=[Depends(JWTBearer())])
async def SellerFacility(request: Request, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        facility = db.query(UserFacilityModel).filter(
            UserFacilityModel.user_id == userdata['id']).filter(UserFacilityModel.key_name == 'purchase_limit').first()
        if(facility is not None):

            facility = {
                'id': facility.id,
                'key_name': facility.key_name,
                'key_value': facility.key_value,
                'created_at': facility.created_at.strftime("%Y-%m-%d")
            }

        else:
            facility = {
                'id': 0,
                'key_name': '',
                'key_value': '',
                'created_at': ''
            }

        return {"status_code": HTTP_200_OK, "facility": facility}

    except Exception as e:
        print(e)


@v4_seller_facility_router.post("/add",  dependencies=[Depends(JWTBearer())])
async def AddSellerFacility(request: Request, data: FacilitySchema, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        checkfacility = db.query(UserFacilityModel).filter(
            UserFacilityModel.user_id == userdata['id']).filter(UserFacilityModel.key_name == 'purchase_limit').first()

        if(checkfacility is not None):
            checkfacility.key_value = data.key_value
            checkfacility.updated_at = datetime.now()

            db.flush()
            db.commit()

        else:

            db_facility = UserFacilityModel(
                user_id=userdata['id'],
                key_name='purchase_limit',
                key_value=data.key_value,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.add(db_facility)
            db.commit()
            db.refresh(db_facility)
        return {"status_code": HTTP_200_OK, "message": "Purchase Limit Added Successfully"}

    except Exception as e:
        print(e)


@v4_seller_facility_router.get("/delete",  dependencies=[Depends(JWTBearer())])
async def updateSellerFacility(request: Request, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        db.query(UserFacilityModel).filter(
            UserFacilityModel.user_id == userdata['id']).filter(UserFacilityModel.key_name == 'purchase_limit').delete()

        db.commit()

        return {"status_code": HTTP_200_OK, "message": "Deleted"}
    except Exception as e:
        print(e)
