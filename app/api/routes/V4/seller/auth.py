from fastapi import APIRouter,  Depends, Form, status, Request

from sqlalchemy.orm.session import Session
from app.api.helpers.media import MediaHelper
from app.api.helpers.auth import *
from app.db.config import  get_db
from app.db.schemas.auth_schema import *
from datetime import datetime
from app.services.auth_handler import signJWT
from app.resources.strings import *
from app.api.helpers.users import *
import requests
from app.db.models.billing_address import BillingAddressModel
from app.db.models.bank_accounts import BankAccountsModel
import uuid
from os import getcwd, unlink
from fastapi import File, UploadFile

#CLOSED ON 2024-06-19
#SlowAPI
# from slowapi import Limiter, _rate_limit_exceeded_handler
# from slowapi.util import get_remote_address
# from slowapi.errors import RateLimitExceeded

#CLOSED ON 2024-06-19
# limiter = Limiter(key_func=get_remote_address)


v4_seller_auth_router = APIRouter()

# Register


@v4_seller_auth_router.post("/register")
async def register(name: str = Form(...), business_type: Optional[str] = Form(...), email: Optional[str] = Form(None), mobile: str = Form(...), country: Optional[str] = Form(...), region: Optional[str] = Form(...), city: Optional[str] = Form(...), pincode: Optional[str] = Form(...), signature: str = Form(...), proof: str = Form(...), proof_file: UploadFile = File(...), db: Session = Depends(get_db)):
    # already exist
    exist = db.query(UserModel).filter(UserModel.mobile == mobile).first()
    if(exist):
        return {'status_code': HTTP_200_OK, 'message': "Already Exist", 'is_user': False}

    otp = random.randint(746008, 999999)
    try:

        if(email is None):
            email = str(mobile)+'@aseztak.com'
        else:
            email = email
        db_user = UserModel(
            name=name,
            mobile=mobile,
            email=email,
            otp=otp,
            status=0,
            country=country,
            region=region,
            city=city,
            pincode=pincode,
            app_version='V4',
            created_at=datetime.now(),
            updated_at=datetime.now()
        )

        db.add(db_user)
        db.flush()

        # create Role
        db_user_role = UserRoleModel(
            role_id=6,
            model_id=db_user.id
        )

        db.add(db_user_role)

        # Profile
        db_user_profile = UserProfileModel(
            user_id=db_user.id,
            business_type=business_type,
            proof=proof,
            proof_type='GSTIN'
        )
        db.add(db_user_profile)

        # Upload Seller Document Proof
        file = proof_file.filename.split(".")

        filename = f"{uuid.uuid4()}.{file[1]}"

        # Set Path Of image
        file_name = getcwd()+"/app/static/userdocument/"+filename

        # Upload image in path
        with open(file_name, 'wb+') as f:
            f.write(proof_file.file.read())
            f.close()

        # open the image
        uploaded_file = getcwd()+"/app/static/userdocument/" + str(filename)
        filename = str(filename)
        # Upload Photo
        MediaHelper.uploadUserDocument(
            uploaded_file=uploaded_file, file_name=filename)

        # Remove Original Image from Path

        unlink(uploaded_file)

        file_path = str(
            linode_obj_config['endpoint_url'])+'/userdocument/'+str(filename)

        # Insert Data in Documents table
        db_documents = UserDocumentModel(
            user_id=db_user.id,
            doc_type='GSTIN',
            file_path=file_path,
            filename='',
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(db_documents)
        db.commit()
        db.refresh(db_user)

        # send OTP Message
        signature = signature
        mobile = str(mobile)
        extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
            AUTHKEY+'&template_id='+TEMPLATE_ID + \
            '&mobile='+str(91)+mobile+'&otp='+str(otp)

        sendurl = OTP_SEND_URL+extra_param
        requests.get(sendurl)

        data = {
            'status_code': HTTP_200_OK,
            'is_user': True,
            'message': "Registered Successfully",
            'data': db_user,
            'signature': signature,
            'otp_sent_at': datetime.now(),
            'otp_expires': 60
        }

        return data

    except Exception as e:
        print(e)

# Mobile Login - Send OTP


@v4_seller_auth_router.post("/send_otp")
# @limiter.limit("5/minute")
async def send_otp(request: Request, data: SendOtpSchema, db: Session = Depends(get_db)):

    mobile = data.mobile

    # Check Seller
    checkSeller = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id ==
                                           UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.mobile == mobile).filter(UserModel.deleted_at.is_(None)).first()

    # Check User
    checkUser = db.query(UserModel).filter(UserModel.mobile == mobile).filter(
        UserModel.deleted_at.is_(None)).first()

    if(checkUser is None and checkSeller is None):
        return {"status_code": status.HTTP_200_OK, "user_exist": False, "is_seller": False, "status": True, "message": "No record found."}

    if(checkUser is not None and checkSeller is None):
        return {"status_code": status.HTTP_200_OK, "user_exist": True, "is_seller": False, "status": False, "message": "Sorry! Your account has been registered as Buyer"}

    if(checkUser is not None and checkUser.status == 0):
        return {"status_code": status.HTTP_200_OK, "user_exist": True, "is_seller": False, "status": False, "message": "Sorry! Your account has not been activated yet!"}

    if(checkUser is not None and checkUser.status == 90):
        return {"status_code": status.HTTP_200_OK, "user_exist": True, "is_seller": False, "status": False, "message": "Sorry! Your account has been suspended!"}

    if(checkUser is not None and checkUser.status == 98):
        return {"status_code": status.HTTP_200_OK, "user_exist": True, "is_seller": False, "status": False, "message": "Sorry! Your account has been deleted!"}

    otp = random.randint(746008, 999999)

    signature = data.signature

    extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
        AUTHKEY+'&template_id='+TEMPLATE_ID + \
        '&mobile='+str(91)+mobile+'&otp='+str(otp)

    sendurl = OTP_SEND_URL+extra_param

    try:

        if(mobile != '9797979797'):
            # Update Otp User
            await SellerOtpUpdate(db=db, mobile=mobile, otp=otp)
            requests.get(sendurl)

        return {'status_code': HTTP_200_OK, "user_exist": True, "is_seller": True, "status": True, "message": "Otp Sent", 'signature': signature, 'otp_sent_at': datetime.now(), 'otp_expires': 60}

    except Exception as e:
        print(e)


# Mobile Login - with Mobile no & OTP


@v4_seller_auth_router.post("/login", )
def user_login(data: VerifyOtpSellerV4, db: Session = Depends(get_db)):

    try:

        # user = check_seller_user(data=data, db=db)
        user = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.mobile ==
                                                                                                                                         data.mobile).filter(UserModel.otp == data.otp).filter(UserModel.deleted_at.is_(None)).first()

        if user:
            # Store IP Address
            user.last_login_at = datetime.now()
            user.last_login_ip = data.ip
            # user.platform = data.platform
            user.platform = 'android-655'
            user.fcm_token = data.fcm_token
            db.flush()
            db.commit()

            is_profile = user.profile

            token = signJWT(user)

            if(is_profile is not None):

                if(is_profile.business_type is None):
                    business_type = ''
                else:
                    business_type = is_profile.business_type

                userdata = {
                    "id": user.id,
                    "name": user.name,
                    "email": user.email,
                    "mobile": user.mobile,
                    "country": user.country,
                    "region": user.region,
                    "pincode": user.pincode,
                    "city": user.city,
                    "business_type": business_type,
                    "proof_type": 'GSTIN',
                    "proof": is_profile.proof,
                    "status": user.status

                }
            else:

                userdata = {
                    "id": user.id,
                    "name": user.name,
                    "email": user.email,
                    "mobile": user.mobile,
                    "country": user.country,
                    "region": user.region,
                    "pincode": user.pincode,
                    "city": user.city,
                    'business_type': '',
                    "proof_type": 'GSTIN',
                    "proof": '',
                    "status": user.status

                }

             # billing
            billing_address = db.query(BillingAddressModel).filter(
                BillingAddressModel.user_id == user.id).first()

            billing_address_exist = False
            if(billing_address):
                billing_address_exist = True

            # bank account
            bank_account = db.query(BankAccountsModel).filter(
                BankAccountsModel.user_id == user.id).first()
            bank_account_exist = False

            if(bank_account):
                bank_account_exist = True

            # Update Otp as Null
            if(user.mobile != '9797979797'):
                user.otp = None
                db.flush()
                db.commit()

            if(user.status == 0):

                return {'status_code': status.HTTP_200_OK, 'error': False, 'status': False, 'message': 'Sorry! Your account has not been activated yet!', 'token': token['access_token'], 'profile_exist': is_profile, 'billing_address_exist': billing_address_exist, 'bank_account_exist': bank_account_exist, 'user': userdata}

            elif(user.status == 90):

                return {'status_code': status.HTTP_200_OK, 'error': False, 'status': False, 'message': 'Sorry! Your account has been suspended!', 'token': token['access_token'], 'profile_exist': is_profile, 'billing_address_exist': billing_address_exist, 'bank_account_exist': bank_account_exist, 'user': userdata}

            else:
                return {'status_code': status.HTTP_200_OK, 'error': False, 'status': True, 'message': 'Success', 'token': token['access_token'], 'profile_exist': is_profile, 'billing_address_exist': billing_address_exist, 'bank_account_exist': bank_account_exist, 'user': userdata}

            # return token['access_token']
            # return {'status_code': status.HTTP_202_ACCEPTED, 'error': False, 'token': token['access_token'], 'profile_exist': is_profile, 'user': user}

        else:
            return {'status_code': status.HTTP_200_OK, 'error':  True, 'message': 'Invalid Otp'}

    except Exception as e:
        print(e)
