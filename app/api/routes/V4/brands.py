

from datetime import date, datetime
from typing import Optional
from app.api.helpers.brands import BrandsHelper
from app.api.util.calculation import floatingValue, productPricecalculation, calculatediscountprice



from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from fastapi import APIRouter,  Depends, Request
from app.db.models.brand import BrandsModel
from starlette.status import HTTP_200_OK
from sqlalchemy import func

from app.db.models.products import ProductModel, ProductPricingModel
from app.db.schemas.brand_schema import TopBrands
from app.db.models.productattribute import ProductAttributeModel
from app.db.models.attributes import AttributeModel
from starlette.status import HTTP_202_ACCEPTED, HTTP_404_NOT_FOUND
from app.db.schemas.product_schema import FilterDataSchema, ProductData
from app.db.models.user import UserModel
from app.api.util.service import Services
from datetime import date, timedelta
from app.db.models.tag import TagModel
from app.db.models.product_tags import ProductTagModel
from app.api.helpers.services import AsezServices
from app.api.helpers.products import ProductsHelper
from app.services.auth import auth
from app.db.models.product_discount import ProductDiscountModel
from app.api.helpers.discount import Discount
v4_brands = APIRouter()

# Get Latest Brands


@v4_brands.get("/list", response_model=TopBrands, dependencies=[Depends(JWTBearer())])
async def getBrands(search: Optional[str] = 'search', db: Session = Depends(get_db)):
    try:

        if(search == 'search'):
            # brands list
            brands_list = db.query(BrandsModel).join(ProductModel, ProductModel.brand_id == BrandsModel.id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(
                BrandsModel.status == 1).group_by(BrandsModel.id).having(func.count(func.distinct(ProductModel.id)) >= 10).order_by(BrandsModel.id.desc()).limit(10).all()
        else:

            search = search.replace("+", "%")
            search = search.rstrip()
            search = "%{}%".format(search)

            brands_list = db.query(BrandsModel).join(ProductModel, ProductModel.brand_id == BrandsModel.id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(BrandsModel.name.like(search)).filter(
                BrandsModel.status == 1).group_by(BrandsModel.id).order_by(BrandsModel.id.desc()).limit(10).all()

        brands = []
        if(len(brands_list) > 0):
            for brand in brands_list:
                b = {
                    'id': brand.id,
                    'title': brand.name,
                    'logo': brand.logo
                }
                brands.append(b)

        return {"status_code": HTTP_200_OK, "brands": brands}
    except Exception as e:
        print(e)


@v4_brands.get("/filter_attributes/{brand_id}", dependencies=[Depends(JWTBearer())])
async def BrandFilterAttributes(brand_id: int, db: Session = Depends(get_db)):
    try:
        brands = db.query(BrandsModel).filter(
            BrandsModel.id == brand_id).first()
        products = db.query(ProductModel, ProductAttributeModel).join(ProductAttributeModel, ProductAttributeModel.product_id == ProductModel.id).join(UserModel, UserModel.id == ProductModel.userid).filter(
            UserModel.status == 1).filter(ProductModel.brand_id == brands.id).group_by(ProductAttributeModel.attribute_id).filter(ProductModel.status == 51).all()
        all_attr = []
        for product in products:
            all_attr.append(product.ProductAttributeModel.attribute_id)
        attributes_val = db.query(ProductModel, ProductAttributeModel).join(ProductAttributeModel, ProductAttributeModel.product_id == ProductModel.id).filter(ProductModel.brand_id == brands.id).group_by(
            ProductAttributeModel.attribute_id, ProductAttributeModel.attribute_value_id, ProductAttributeModel.attribute_value).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).all()
        if(len(all_attr) == 0 or len(attributes_val) == 0):
            return False
        today = datetime.now()
        aseztak_service = Services.aseztak_services(
            today, db=db)
        min_max_product = db.query(ProductPricingModel, func.max((ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)*ProductPricingModel.tax/100+(ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)).label('max_price'), func.min((ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate /
                                   100)+100)/100)*ProductPricingModel.tax/100+(ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)).label('min_price')).join(ProductModel, ProductModel.id == ProductPricingModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.brand_id == brands.id).filter(ProductModel.status == 51).first()
        # pricelist = []
        # for price in min_max_product:

        # Customization min price
        customMinPrice = str(min_max_product.min_price)
        customMinPrice = customMinPrice.split(".")
        customMinPrice = int(customMinPrice[0])

        pricing = [{
            'name': 'Pricing',
            'values': [
                {
                    'min_price': str(round(customMinPrice)),
                    'max_price': str(round(min_max_product.max_price))
                },
            ]
        }]
        value_list = []
        ideal_for = []
        color = []
        clothing_size = []
        for attribute in all_attr:
            attributes = db.query(AttributeModel).filter(
                AttributeModel.id == attribute).filter(AttributeModel.is_filterable == 1).first()
            if(attributes):
                # filter_attributes = []
                attributes_values = []
                for values in attributes_val:
                    if(attribute == values.ProductAttributeModel.attribute_id):
                        value = {
                            'id': values.ProductAttributeModel.attribute_value_id,
                            'value': values.ProductAttributeModel.attribute_value
                        }
                        attributes_values.append(value)
                if(attributes.code == 'ideal_for'):
                    attrs = {
                        'name': attributes.name,
                        'values': attributes_values
                    }
                    ideal_for.append(attrs)
                elif(attributes.code == 'color_for_all_product'):
                    attrs = {
                        'name': attributes.name,
                        'values': attributes_values
                    }
                    color.append(attrs)
                elif(attributes.code == 'clothing_size_for_all_types_of_garments_except_mens_inner_wear_products'):
                    attrs = {
                        'name': attributes.name,
                        'values': attributes_values
                    }
                    clothing_size.append(attrs)
                else:
                    attrs = {
                        'name': attributes.name,
                        'values': attributes_values
                    }
                    value_list.append(attrs)
                # attrs = {
                #     'name': attributes.name,
                #     'values': attributes_values
                # }
                # value_list.append(attrs)
        # pricelist.append(pricing)
        filter_attributes = pricing + ideal_for + color + clothing_size + value_list
        if(filter_attributes == False):
            return {"status_code": HTTP_404_NOT_FOUND}
        return {"status_code": HTTP_202_ACCEPTED, "filter_attributes": filter_attributes}
    except Exception as e:
        print(e)


@v4_brands.post("/product/list", response_model=ProductData, dependencies=[Depends(JWTBearer())])
async def BrandProductList(request: Request, filterdata: FilterDataSchema, db: Session = Depends(get_db)):
    try:

        # Check User
        userdata = auth(request=request)
        check_user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        page = filterdata.page
        limit = filterdata.limit

        brands = db.query(BrandsModel).filter(
            BrandsModel.id == filterdata.id).first()

        # brand name
        brand = brands.name

        today = datetime.now()
        aseztak_service = Services.aseztak_services(
            today, db=db)
        if(len(filterdata.attributes) == 0 and filterdata.min_price == '0' and filterdata.max_price == '0' and filterdata.sort == 'all'):
            data: ProductModel = await BrandsHelper.getProductsbybrands(db=db, brand_id=brands.id)
            # products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).filter(ProductModel.brand_id == data.id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductPricingModel.deleted_at.is_(None)).order_by(desc(ProductModel.id))
        else:
            if(filterdata.min_price == '0' and filterdata.max_price == '0'):
                # Get Min and Max Price
                # productPrice: ProductModel = await BrandsHelper.productMinMaxPircebyBrand(db=db, brand_id=filterdata.id)

                # min_max_product = db.query(ProductModel, func.max((ProductPricingModel.price * ProductPricingModel.tax)/100 + ProductPricingModel.price).label('max_price'), func.min((ProductPricingModel.price * ProductPricingModel.tax)/100 + ProductPricingModel.price).label('min_price')).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).filter(ProductModel.brand_id == brands.id).filter(ProductModel.status == 51).first()
                # filterdata.min_price = round(productPrice.min_price)
                # filterdata.max_price = round(productPrice.max_price)
                filterdata.min_price = '0'
                filterdata.max_price = '0'

            data: ProductModel = await BrandsHelper.filerSearchProductsbyBrands(db=db, brand_id=filterdata.id, sort=filterdata.sort, min_price=filterdata.min_price, max_price=filterdata.max_price, attributes=filterdata.attributes)

        product_count = data.count()

        if(product_count == 0):
            return {"status_code": HTTP_200_OK, "is_registered": True, "brand": brand, "products": [], "total_products": 0, "current_page": 1, "total_pages": 0}

        else:

            productdata: ProductModel = data.limit(
                limit=filterdata.limit).offset((filterdata.page - 1) * filterdata.limit).all()

            # Aseztak Service
            today = date.today()
            # aseztak_service = Services.aseztak_services(
            #     today, db=db)
            today_date = today.strftime(
                '%Y-%m-%d')
            aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
            # calculating Price
            for product in productdata:
                # CHECK PRODUCT DISCOUNT
                check_product_discount = product.product_discount.filter(
                    func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
                product_discount = {
                    'rate': '0.00',
                    'product_sale_price': '0.00',
                    'discount_amount': '0.00',
                }

                # Check Pricing
                pricing = product.product_pricing[0]
                if(pricing):
                    # product_price = (pricing.price * pricing.tax) / 100 + pricing.price
                    product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=aseztak_service, app_version='V4')

                    # Get Product Sale Price
                    product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
                                                                 gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                    # Check ONLINE Discounted Price
                    discount_price = float(0.00)
                    checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                    if(checkdiscount != 0):
                        discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                    # Check Product Discount
                    if(check_product_discount is not None):
                        product_discount = {
                            'rate': floatingValue(check_product_discount.discount),
                            'product_sale_price': floatingValue(product_sale_price),
                            'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                        }
                    # Restructure Price data
                    price_data = {
                        'id': pricing.id,
                        'price': floatingValue(product_price),
                        'product_discount': product_discount,
                        'discount': str(checkdiscount),
                        'discount_price': floatingValue(discount_price),
                        'moq': pricing.moq,
                        'unit': pricing.unit
                    }
                    product.pricing = price_data
                 # New Changes (TINA)
                start_time = date.today() - timedelta(days=14)
                start_date = start_time.strftime("%Y-%m-%d")
                product_date = product.created_at.strftime("%Y-%m-%d")
                is_new_arrival = False
                if (product_date >= start_date):
                    is_new_arrival = True

                product.is_new_arrival = is_new_arrival

                # Check Best Selling and Treding Product
                check_best_selling = db.query(TagModel).join(ProductTagModel, ProductTagModel.tag_id == TagModel.id).filter(
                    ProductTagModel.product_id == product.id).filter(TagModel.name == 'Best Selling').first()

                if (check_best_selling is not None):
                    product.is_best_selling = True

                check_trending = db.query(TagModel).join(ProductTagModel, ProductTagModel.tag_id == TagModel.id).filter(
                    ProductTagModel.product_id == product.id).filter(TagModel.name == 'Trending').first()

                if (check_trending is not None):
                    product.is_trending = True
                # NEW Change (TINA)
                # Favourite
                product.favourite = False

                # Product Image
                productimage = product.images[0]

                product.image = ''
                if(productimage):
                    product.image = productimage.file_path

            # Total Records
            total_records = product_count

            total_pages = str(round((total_records/limit), 2))

            total_pages = total_pages.split('.')

            if(total_pages[1] != "0"):
                total_pages = int(total_pages[0]) + 1

            else:
                total_pages = int(total_pages[0])
        if(check_user.status == 1):
            is_registered = True
        else:
            is_registered = False
        return {"status_code": HTTP_200_OK, "is_registered": is_registered, "title": brand, "products": productdata, "total_products": total_records, "current_page": filterdata.page, "total_pages": round(total_pages, 0)}

    except Exception as e:
        print(e)
