
from starlette.status import  HTTP_200_OK
from app.api.helpers.calculation import *
from datetime import datetime
from fastapi import APIRouter,  Depends
from datetime import date
from app.services.auth import auth
from app.db.config import get_db
from app.services.auth_bearer import JWTBearer
from app.db.models.wallet import WalletModel
from app.db.schemas.wallet_schema import  AddWallete
from app.api.util.calculation import floatingValue
from app.db.models.orders import OrdersModel
from app.db.models.order_status import OrderStatusModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.products import ProductModel, ProductPricingModel
from app.api.helpers.products import ProductsHelper
from app.core.config import REWARDS_PERCENTAGE
from sqlalchemy.sql.functions import func

v4_wallet_router = APIRouter()


# @v4_wallet_router.get("/list", response_model=WalletListSchema, dependencies=[Depends(JWTBearer())])
# async def index(request: Request, db: Session = Depends(get_db)):
#     try:

#         userdata = auth(request=request)

#         wallets = db.query(WalletModel).filter(
#             WalletModel.user_id == userdata['id']).order_by(WalletModel.id.desc()).all()

#         wallet_list = []
#         for wallet in wallets:

#             if('DR' in str(wallet.type)):
#                 txn_type = 'DR'
#             else:
#                 txn_type = 'CR'

#             wallet_type = ''
#             if('GENERAL' in str(wallet.wallet_type)):
#                 wallet_type = 'GENERAL'

#             w = {
#                 'id': wallet.id,
#                 'wallet_type': wallet_type,
#                 'description': wallet.description,
#                 'type': txn_type,
#                 'amount': floatingValue(roundOf(wallet.amount)),
#                 'created_at': wallet.created_at.strftime("%Y-%m-%d")
#             }
#             wallet_list.append(w)

#         return {"status_code": HTTP_200_OK, "wallets": wallet_list}
#     except Exception as e:
#         print(e)


@v4_wallet_router.get("/list", dependencies=[Depends(JWTBearer())])
async def transaction(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 15):
    try:
        userdata = auth(request=request)

        all_transactions = db.query(WalletModel).filter(
            WalletModel.user_id == userdata['id']).order_by(WalletModel.id.desc())
        total_transactions = all_transactions.count()
        transactions = all_transactions.limit(
            limit=limit).offset((page - 1) * limit).all()
        # Number of Total Pages
        total_pages = str(round((total_transactions/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        transaction_data = []
        debit_balance = db.execute(
            "SELECT SUM(wallet.amount) as debit_balance FROM wallet WHERE wallet.user_id =:user_id AND wallet.status = 1 AND wallet.type = 'DR'", {
                'user_id': userdata['id']
            }).first()
        credit_balance = db.execute(
            "SELECT SUM(wallet.amount) as credit_balance FROM wallet WHERE wallet.user_id =:user_id AND wallet.status = 1 AND wallet.type = 'CR' AND wallet.valid_upto is NOT Null", {
                'user_id': userdata['id']
            }).first()
        if(debit_balance['debit_balance'] is None):
            debit_balance = 0
        else:
            debit_balance = debit_balance['debit_balance']
        if(credit_balance['credit_balance'] is None):
            credit_balance = 0
        else:
            credit_balance = credit_balance['credit_balance']
        closing_balance = (
            float(credit_balance) - float(debit_balance))

        for transaction in transactions:
            debit = 0
            credit = 0

            # Check rewards
            if(transaction.status == 0):
                check_orders = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
                    OrdersModel.reff == transaction.reff_id).having(func.max(OrderStatusModel.status) <= 81).group_by(OrderStatusModel.order_id)
                total_rewards_points = 0
                calculatewallet_amount = 0
                if(check_orders.count() > 0):
                    for ord in check_orders:
                        # Aseztak Service
                        today_date = ord.created_at.strftime('%Y-%m-%d')

                        aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

                        items = ord.order_items.filter(OrderItemsModel.status.between(
                            0, 81)).filter(OrderItemsModel.status != 80).all()
                        total_amount = 0
                        for item in items:
                            # Pricing Object
                            pricingdata = item.uuid.split('-')
                            pricingdata = db.query(ProductPricingModel).filter(
                                ProductPricingModel.id == pricingdata[1]).first()
                            product = db.query(ProductModel).filter(
                                ProductModel.id == item.product_id).first()
                            # Get Product Price
                            product_price: ProductModel = await ProductsHelper.getPrice(db,
                                                                                        product, pricingdata, asez_service=aseztak_service, app_version=ord.app_version, order_item_id=item.id)

                            total_amount += product_price * item.quantity
                            totalamount = product_price * item.quantity
                            calculateforwalletamount = (
                                float(totalamount) * float(REWARDS_PERCENTAGE) / 100)
                            calculateforwalletamount = int(
                                calculateforwalletamount)
                            calculatewallet_amount += calculateforwalletamount

                #         total_rewards_points += total_amount
                # rewards_points = (float(total_rewards_points) *
                #                   float(REWARDS_PERCENTAGE)) / 100
                rewards_points = int(calculatewallet_amount)
                if('DR' in str(transaction.type)):
                    debit = round(transaction.amount, 2)
                else:
                    credit = round(rewards_points, 2)
            else:

                if('DR' in str(transaction.type)):
                    debit = round(transaction.amount, 2)
                else:
                    credit = round(transaction.amount, 2)
            if(round(debit, 2) != 0 or round(credit, 2) != 0):
                valid_upto = ''
                expired = False
                if(transaction.valid_upto is not None):
                    valid_upto = transaction.valid_upto
                    today = date.today()
                    # today = today.strftime("%Y-%m-%d")
                    if(transaction.valid_upto < today):
                        expired = True

                txn = {
                    'date': transaction.created_at.strftime("%Y-%m-%d"),
                    'wallet_type': str(transaction.reff_type),
                    'description': transaction.description,
                    'debit': floatingValue(round(debit, 2)),
                    'credit': floatingValue(round(credit, 2)),
                    'type': transaction.type,
                    'status': transaction.status,
                    'valid_upto': valid_upto,
                    'expired': expired
                }

                transaction_data.append(txn)

        closing_balance = floatingValue(round(closing_balance, 2))

        return {"status_code": HTTP_200_OK, "wallet_list_data": transaction_data, "closing_balance": closing_balance, "message": "Success", "total_items": total_transactions, "current_page": page, "total_page": total_pages}

    except Exception as e:
        print(e)


@v4_wallet_router.post("/add", dependencies=[Depends(JWTBearer())])
async def AddWallet(request: Request, data: AddWallete, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        wallet = WalletModel(
            user_id=userdata['id'],
            reff_id=userdata['id'],
            reff_type=data.wallete_type,
            description=data.description,
            type=data.type,
            amount=data.amount,
            status=1,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(wallet)
        db.commit()
        db.refresh(wallet)

        return {"status_code": HTTP_200_OK, "message": "Addedd Succesfully"}

    except Exception as e:
        print(e)
