

from typing import Optional


from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from fastapi import APIRouter,  Depends
from starlette.status import HTTP_200_OK
from sqlalchemy import func

from app.db.models.products import ProductModel
from app.db.models.user import UserModel, UserRoleModel
from app.db.schemas.user_schema import TopSellerListSchema


v4_seller = APIRouter()


@v4_seller.get("/list", response_model=TopSellerListSchema,  dependencies=[Depends(JWTBearer())])
async def SellerList(search: Optional[str] = 'search', db: Session = Depends(get_db)):
    try:

        if(search == 'search'):
            # Seller List
            seller_list = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).join(ProductModel, ProductModel.userid == UserModel.id).filter(
                UserRoleModel.role_id == 6).filter(UserModel.status == 1).filter(ProductModel.status == 51).group_by(UserModel.id).having(func.count(func.distinct(ProductModel.id)) >= 50).order_by(UserModel.id.desc()).limit(10).all()

        else:
            search = search.replace("+", "%")
            search = search.rstrip()
            search = "%{}%".format(search)

            # Seller List
            seller_list = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).join(ProductModel, ProductModel.userid == UserModel.id).filter(
                UserRoleModel.role_id == 6).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(UserModel.name.like(search)).group_by(UserModel.id).order_by(UserModel.id.desc()).limit(10).all()

        # Sellers
        sellers = []
        if(len(seller_list) > 0):
            for seller in seller_list:
                if(seller.city is None):
                    city = ''
                else:
                    city = seller.city

                if(seller.region is None):
                    state = ''
                else:
                    state = seller.region

                s = {
                    'id': seller.id,
                    'name': seller.name,
                    'address': str(city)+', '+str(state)
                }
                sellers.append(s)

        return {"status_code": HTTP_200_OK, "sellers":  sellers}
    except Exception as E:
        print(E)
