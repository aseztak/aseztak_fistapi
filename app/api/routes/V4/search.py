
from starlette.requests import Request
from app.api.util.calculation import floatingValue, productPricecalculation


from app.api.helpers.tags import get_tag


from app.db.models.tag import TagModel
from app.db.schemas.product_schema import FiltertagDataSchema, ProductData

from app.services.auth_bearer import JWTBearer

from app.db.config import  get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.users import *
from starlette.status import HTTP_202_ACCEPTED, HTTP_404_NOT_FOUND
from fastapi import APIRouter,  Depends
from app.db.models.products import *
from app.api.helpers.products import *
from app.services.auth import auth
from datetime import date, timedelta
from app.db.models.tag import TagModel
from app.db.models.product_tags import ProductTagModel
from app.api.helpers.services import AsezServices
from app.api.helpers.products import ProductsHelper
from app.api.helpers.discount import Discount

v4_search_router = APIRouter()


# Search Products
@v4_search_router.get("-products/{param}", response_model=ProductData, dependencies=[Depends(JWTBearer())])
async def searchProducts(request: Request, param: str, db: Session = Depends(get_db), page: int = 1, limit: int = 10):

    try:
        userdata = auth(request=request)

        search = param.replace("+", "%")
        search = search.rstrip()
        search = "%{}%".format(search)

        title = 'Product Search'
        data = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).filter(
            ProductModel.status == 51).filter(ProductPricingModel.deleted_at.is_(None)).filter(ProductModel.title.ilike(search)).group_by(ProductModel.id)

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "title": title, "products": [], "total_products": 0, "current_page": page, "total_pages": 0}
        else:

            productdata: ProductModel = data.limit(
                limit=limit).offset((page - 1) * limit).all()

            # calculating Price
            # calculating Price
            for product in productdata:

                # Check Pricing
                pricing = product.pricing[0]
                if(pricing):
                    # Aseztak Service
                    # aseztak_service = Services.aseztak_services(
                    #     pricing.updated_at, db=db)
                    today_date = pricing.updated_at.strftime(
                        '%Y-%m-%d')
                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                    product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=aseztak_service, app_version='V4')
                    # product_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
                    #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                    # Restructure Price data
                    product.pricing.price = floatingValue(product_price)
                    product.pricing.id = pricing.id
                    product.pricing.moq = pricing.moq

                # Favourite
                favourite = product.wishlist.filter(
                    FavouriteModel.user_id == userdata['id']).first()

                product.favourite = False
                if(favourite):
                    product.favourite = True

                # Product Image
                productimage = product.images[0]

                product.image = ''
                if(productimage):
                    product.image = productimage.file_path

            # Total Records
            total_records = data.count()

            # Number of Total Pages
            total_pages = str(round((total_records/limit), 2))

            total_pages = total_pages.split('.')

            if(total_pages[1] != "0"):
                total_pages = int(total_pages[0]) + 1
            else:
                total_pages = int(total_pages[0])

            return {"status_code": HTTP_202_ACCEPTED, "title": title, "products": productdata, "total_products": total_records, "current_page": page, "total_pages": round(total_pages, 0)}

    except Exception as e:
        print(e)


# Filter Attributes
@v4_search_router.get("/filter-attributes/{tag_id}", dependencies=[Depends(JWTBearer())])
async def filterAttributes(tag_id: int, db: Session = Depends(get_db)):
    try:

        # productPrice: ProductModel = await ProductsHelper.productMinMaxPircebyTag(db=db, tag_id=tag_id)

        # pricing = {
        #     'min_price': round(productPrice.min_price),
        #     'max_price': round(productPrice.max_price)
        # }

        data: ProductModel = await ProductsHelper.filterAttributesbyTags(db=db, tag_id=tag_id)

        if(data == False):
            return {"status_code": HTTP_404_NOT_FOUND}

        return {"status_code": HTTP_202_ACCEPTED, "filter_attributes": data}

    except Exception as e:
        print(e)


@v4_search_router.post("/products-tag-wise", response_model=ProductData, dependencies=[Depends(JWTBearer())])
async def filterSearchProducts(request: Request, filterdata: FiltertagDataSchema, db: Session = Depends(get_db)):
    try:

        page = filterdata.page
        limit = filterdata.limit

        tag: TagModel = await get_tag(db=db, tag_id=filterdata.id)

        # Title
        title = tag.name

        userdata = auth(request=request)
        # Check User
        check_user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        if(len(filterdata.attributes) == 0 and filterdata.min_price == '0' and filterdata.max_price == '0' and filterdata.sort == 'all'):
            data: ProductModel = await ProductsHelper.getProductsbytags(db=db, tag_id=tag.id)

        else:

            if(filterdata.min_price == '0' and filterdata.max_price == '0'):
                # # Get Min and Max Price
                # productPrice: ProductModel = await ProductsHelper.productMinMaxPircebyTag(db=db, tag_id=filterdata.id)

                # # Customization min price
                # customMinPrice = str(productPrice.min_price)
                # customMinPrice = customMinPrice.split(".")
                # customMinPrice = int(customMinPrice[0])

                # filterdata.min_price = round(customMinPrice)
                # filterdata.max_price = round(productPrice.max_price)
                filterdata.min_price = '0'
                filterdata.max_price = '0'

            data: ProductModel = await ProductsHelper.filerSearchProductsbyTags(db=db, tag_id=filterdata.id, sort=filterdata.sort, min_price=filterdata.min_price, max_price=filterdata.max_price, attributes=filterdata.attributes)

        product_count = data.count()

        if(product_count == 0):
            return {"status_code": HTTP_200_OK, "is_registered": True, "title": title, "products": [], "total_products": 0, "current_page": filterdata.page, "total_pages": 0}

        else:

            productdata: ProductModel = data.limit(
                limit=filterdata.limit).offset((filterdata.page - 1) * filterdata.limit).all()

            # Aseztak Service
            today = date.today()
            # aseztak_service = Services.aseztak_services(
            #     today, db=db)
            today_date = today.strftime(
                '%Y-%m-%d')
            aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
            # calculating Price
            for product in productdata:
                # CHECK PRODUCT DISCOUNT
                check_product_discount = product.product_discount.filter(
                    func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
                product_discount = {
                    'rate': '0.00',
                    'product_sale_price': '0.00',
                    'discount_amount': '0.00',
                }

                # Check Pricing
                pricing = product.product_pricing[0]
                if(pricing):
                    product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=aseztak_service, app_version='V4')
                    # Get Product Sale Price
                    product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
                                                                 gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                    # Check ONLINE Discounted Price
                    discount_price = float(0.00)
                    checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                    if(checkdiscount != 0):
                        discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                    # Check Product Discount
                    if(check_product_discount is not None):
                        product_discount = {
                            'rate': floatingValue(check_product_discount.discount),
                            'product_sale_price': floatingValue(product_sale_price),
                            'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                        }

                   # Restructure Price data
                    price_data = {
                        'id': pricing.id,
                        'price': floatingValue(product_price),
                        'product_discount': product_discount,
                        'discount': str(checkdiscount),
                        'discount_price': floatingValue(discount_price),
                        'moq': pricing.moq,
                        'unit': pricing.unit
                    }
                    product.pricing = price_data

                 # New Changes (TINA)
                start_time = date.today() - timedelta(days=14)
                start_date = start_time.strftime("%Y-%m-%d")
                product_date = product.created_at.strftime("%Y-%m-%d")
                is_new_arrival = False
                if (product_date >= start_date):
                    is_new_arrival = True

                product.is_new_arrival = is_new_arrival

                # Check Best Selling and Treding Product
                check_best_selling = db.query(TagModel).join(ProductTagModel, ProductTagModel.tag_id == TagModel.id).filter(
                    ProductTagModel.product_id == product.id).filter(TagModel.name == 'Best Selling').first()

                if (check_best_selling is not None):
                    product.is_best_selling = True

                check_trending = db.query(TagModel).join(ProductTagModel, ProductTagModel.tag_id == TagModel.id).filter(
                    ProductTagModel.product_id == product.id).filter(TagModel.name == 'Trending').first()

                if (check_trending is not None):
                    product.is_trending = True
                # NEW Change (TINA)
                # Favourite
                product.favourite = False

                # Product Image
                productimage = product.images[0]

                product.image = ''
                if(productimage):
                    product.image = productimage.file_path

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=product.userid, today_date=today_date)
                product.seller_discount = check_seller_discount['rate']

            # Total Records
            total_records = product_count

            total_pages = str(round((total_records/limit), 2))

            total_pages = total_pages.split('.')

            if(total_pages[1] != "0"):
                total_pages = int(total_pages[0]) + 1

            else:
                total_pages = int(total_pages[0])
            if(check_user.status == 1):
                is_registered = True
            else:
                is_registered = False
            return {"status_code": HTTP_200_OK, "is_registered": is_registered, "title": title, "products": productdata, "total_products": total_records, "current_page": filterdata.page, "total_pages": round(total_pages, 0)}

    except Exception as e:
        print(e)
