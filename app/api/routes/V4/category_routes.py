
from fastapi.param_functions import Depends
from fastapi.routing import APIRouter
from app.api.helpers.categories import CategoriesHelper
from app.db.config import get_db
from app.db.schemas.category_schema import CategoriesSearch, CategoryData
from starlette.requests import Request
from sqlalchemy.orm.session import Session

from app.services.auth_bearer import JWTBearer
from app.db.models.categories import CategoriesModel

v4_categories_route = APIRouter()


@v4_categories_route.post("/list", response_model=CategoryData, dependencies=[Depends(JWTBearer())])
async def get_categories(request: Request, data: CategoriesSearch, db: Session = Depends(get_db)):

    try:

        # Category Title
        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == data.parent_id).first()

        data = CategoriesHelper.v4_get_categories(
            db=db, parent_id=data.parent_id, search=data.search)

        categories = await CategoriesHelper.hasChildCategories(request=request, db=db, data=data)

        return {'title': category.name, 'categories': categories}

    except Exception as e:
        print(e)
