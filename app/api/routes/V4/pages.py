
from app.api.helpers.calculation import *
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from fastapi import APIRouter,  Depends
from starlette.status import HTTP_200_OK
from app.db.models.page import PageModel
from app.db.schemas.page_schema import PageData, PagesDataSchema

v4_pages_router = APIRouter()


@v4_pages_router.get("/", response_model=PagesDataSchema)
async def get_page(db: Session = Depends(get_db)):
    try:

        page_data = db.query(PageModel).filter(
            (PageModel.type == 'Buyer') | (PageModel.type == 'Both')).order_by(PageModel.id.asc()).all()

        return {"status_code": HTTP_200_OK, 'pages': page_data}

    except Exception as e:
        print(e)


@v4_pages_router.get("/detail/{slug}", response_model=PageData)
async def get_page(slug: str, db: Session = Depends(get_db)):
    try:

        page_data = db.query(PageModel).filter(PageModel.slug == slug).first()

        return {"status_code": HTTP_200_OK, "page": page_data}

    except Exception as e:
        print(e)


@v4_pages_router.get("/list/{param}")
async def get_page(param: str, db: Session = Depends(get_db)):
    try:
        param = param.lower()
        if(param == 'seller'):
            page_type = 'Seller'
        if(param == 'buyer'):
            page_type = 'Buyer'

        page_data = db.query(PageModel).filter(
            (PageModel.type == page_type) | (PageModel.type == 'Both')).order_by(PageModel.id.asc()).all()
        pages = []
        for page in page_data:
            p = {
                'id': page.id,
                'name': page.name,
                'slug': page.slug
            }
            pages.append(p)

        return {"status_code": HTTP_200_OK, 'pages': pages}

    except Exception as e:
        print(e)


@v4_pages_router.get("/get/details/{id}", response_model=PageData)
async def get_page(id: int, db: Session = Depends(get_db)):

    try:
        data = db.query(PageModel).filter(PageModel.id == id).first()

        return {"status_code": HTTP_200_OK, "page": data}

    except Exception as e:
        print(e)
