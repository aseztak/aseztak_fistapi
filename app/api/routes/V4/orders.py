from datetime import date, datetime, timedelta
from typing import List, Optional

from starlette.requests import Request
from starlette.status import HTTP_200_OK, HTTP_304_NOT_MODIFIED
from app.api.helpers.media import MediaHelper
from app.api.util.check_user import CheckUser
from app.api.helpers.calculation import *
from app.db.models.orders import OrdersModel
from app.db.models.return_proof import OrderReturnProofModel
from app.db.models.shipping import OrderShippingModel
from app.services.auth_bearer import JWTBearer
from app.db.config import  get_db
from sqlalchemy.orm.session import Session
from fastapi import APIRouter,  Depends, Form
from app.api.helpers.orders import OrderHelper
from app.db.schemas.orders_schema import OrderDetailSchema, OrderListSchema, cancelOrderscheam,  SearchOrderSchema
from app.api.helpers.users import *
from app.api.helpers.orderstaticstatus import OrderStatus
from fastapi import File, UploadFile
import uuid
from os import getcwd, unlink
from app.api.util.calculation import orderDeliveryCalculation, floatingValue, orderDiscountCalculation, roundOf
from app.core import config
from app.services.auth import auth
from app.api.helpers.products import ProductsHelper
from app.api.helpers.services import AsezServices
from app.db.models.order_status import OrderStatusModel
from app.db.models.shipping_address import ShippingAddressModel
from app.db.models.acconts import AccountsModel
from app.api.util.message import Message
from app.db.models.wallet import WalletModel
from app.db.models.transaction import TransactionsModel
from app.core.config import REWARDS_PERCENTAGE, COIN_EXPIRING_DAYS
v4_order_router = APIRouter()

# All Orders RAHUl


@v4_order_router.get("/all", dependencies=[Depends(JWTBearer())])
async def getAllOrders(request: Request, db: Session = Depends(get_db)):
    userdata = auth(request=request)
    try:

        all_orders: OrdersModel = await OrderHelper.getOrders(db=db, user_id=userdata['id'])

        # Pending Orders
        pending_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='0')

        # Approved Orders
        approved_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='30')

        # Packed Orders
        packed_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='40')

        # Shipped Orders
        shipped_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='60')

        # Delivered Orders
        delivered_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='70')

        # Reversed Orders
        reversed_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='61')

        # Return Initiated Orders
        return_initiated_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='80')

        # Return Approved Orders
        return_approved_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='90')

        # Return Declined Orders
        return_declined_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='81')

        # Return Completed Orders
        return_completed_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='100')

        # Refunded Orders
        return_refunded_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='110')

        # Cancelled Orders
        cancelled_orders: OrdersModel = await OrderHelper.countOrdersStatuswise(db=db, user_id=userdata['id'], status='980')

        orders_count = [
            {
                'status': 'all',
                'count': all_orders.count(),
                'title': 'All Orders',
                'description': 'All Orders',
                'icon': ''
            },
            {
                'status': '0',
                'count': pending_orders.count(),
                'title': 'Ordered',
                'description': 'Your all pending orders',
                'icon': ''
            },

            {
                'status': '30',
                'count': approved_orders.count(),
                'title': 'Processing',
                'description': 'System has approved your orders',
                'icon': ''
            },
            {
                'status': '40',
                'count': packed_orders.count(),
                'title': 'Packed',
                'description': 'Packed Orders',
                'icon': ''
            },
            {
                'status': '60',
                'count': shipped_orders.count(),
                'title': 'Shipped',
                'description': 'Shipped Orders',
                'icon': ""
            },

            {
                'status': '70',
                'count': delivered_orders.count(),
                'title': 'Delivered',
                'description': 'Delivered Orders',
                'icon': ''
            },

            {
                'status': '61',
                'count': reversed_orders.count(),
                'title': 'Reverse',
                'description': 'Reversed Orders',
                'icon': ''
            },
            {
                'status': '80',
                'count': return_initiated_orders.count(),
                'title': 'Return Initiated',
                'description': 'Return Initiated Orders',
                'icon': ''
            },
            {
                'status': '90',
                'count': return_approved_orders.count(),
                'title': 'Return Approved',
                'description': 'Return Approved Orders',
                'icon': ''
            },
            {
                'status': '81',
                'count': return_declined_orders.count(),
                'title': 'Return Declined',
                'description': 'Return Declined Orders',
                'icon': ''
            },
            {
                'status': '100',
                'count': return_completed_orders.count(),
                'title': 'Return Completed',
                'description': 'Return Completed Orders',
                'icon': ''
            },
            {
                'status': '110',
                'count': return_refunded_orders.count(),
                'title': 'Refunded',
                'description': 'Refunded Orders',
                'icon': ''
            },
            {
                'status': '980',
                'count': cancelled_orders.count(),
                'title': 'Cancelled',
                'description': 'Cancelled Orders',
                'icon': ""

            }
        ]
        return {"status_code": HTTP_200_OK, "orders": orders_count}
    except Exception as e:
        return {"status_code": HTTP_200_OK, "orders": []}


# All orders status wise
@v4_order_router.post("/all/status-wise", response_model=OrderListSchema, dependencies=[Depends(JWTBearer())])
async def getAllOrdersStatusWise(request: Request, searchorder: SearchOrderSchema, db: Session = Depends(get_db)):
    userdata = auth(request=request)
    title = ''
    try:
        page = searchorder.page
        limit = searchorder.limit

        if(searchorder.status == 'all'):

            title = 'All Orders'

            data: OrdersModel = await OrderHelper.searchOrders(db=db, user_id=userdata['id'], search=searchorder.search, from_date=searchorder.from_date, to_date=searchorder.to_date)

            orderdata: OrdersModel = data.limit(
                limit=limit).offset((page - 1) * limit).all()

        else:

            all_static_status = OrderStatus.statusList()
            for st in all_static_status:
                if(str(searchorder.status) == str(st['status_id'])):
                    title = st['status_title']

            data: OrdersModel = await OrderHelper.searchOrdersStatusWise(db=db, user_id=userdata['id'], status=searchorder.status, search=searchorder.search, from_date=searchorder.from_date, to_date=searchorder.to_date)

            orderdata: OrdersModel = data.limit(
                limit=limit).offset((page - 1) * limit).all()

        if(data == False):
            return {"status_code": HTTP_200_OK, "order_items": [], "total_amount": 0.00, "total_items": 0, "current_page": 0, "total_page": 0}

        message = ''
        invoice = ''

        for order in orderdata:
            # Get Order All Items
            products = []

            items = order.order_items.all()

            total_amount = 0
            for item in items:
                # Get Product Detail
                productdata = db.query(ProductModel).where(
                    ProductModel.id == item.product_id).first()

                # Product Pricing Images
                product_price_id = item.uuid.split('-')
                product_price = db.query(ProductPricingModel).where(
                    ProductPricingModel.id == product_price_id[1]).first()

                filename = ''
                if(product_price is not None):

                    if(item.images != None):
                        image = db.query(ProductMediaModel).filter(
                            ProductMediaModel.id == item.images.strip("[]")).first()

                        if(image is not None):
                            filename = image.file_path

                        else:
                            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == item.product_id).filter(
                                ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                            filename = image.file_path
                    else:
                        if(product_price.default_image != 0):
                            image = db.query(ProductMediaModel).filter(
                                ProductMediaModel.id == product_price.default_image).first()
                            if(image is not None):
                                filename = image.file_path
                        else:
                            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == item.product_id).filter(
                                ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                            if(image is not None):
                                filename = image.file_path

                    img = filename

                products.append(img)

                # Calculation Total Amount of items
                # Aseztak Service
                # aseztak_service = Services.aseztak_services(
                #     item.created_at, db=db)
                today_date = item.created_at.strftime(
                    '%Y-%m-%d')
                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                if(aseztak_service is None):

                    # Calculate Product Price
                    product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
                                                            gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)

                    total_amount += product_price * item.quantity
                else:
                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, product_price, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item.id)
                    # Calculate Product Price
                    # product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
                    #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order.app_version)

                    total_amount += product_price * item.quantity

            discount_amount = 0
            order_date = order.created_at.strftime("%Y-%m-%d")
            if(order.discount != 0):
                # CHECK ORDER DISCOUNT
                order_discount = db.query(OrderDiscountModel).filter(
                    OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

                discount_rate = order_discount.discount
                if(order.discount_rate != 0):
                    discount_rate = order.discount_rate

                discount_amount = orderDiscountCalculation(app_version=order.app_version,
                                                           order_amount=total_amount, discount_amount=order.discount, discount_rate=discount_rate)

            # Check Shipping Charge
            # delivery_charge = 0
            # if(order.delivery_charge != 0): CHANGES BY RAHUL
            shipping_charge = db.query(ShippingChargeModel).filter(
                ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

            delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order.free_delivery, user_id=order.user_id, order_date=order.created_at,
                                                       app_version=order.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

            # Customize Order Reff
            reff_id = 'ASEZ'+str(order.reff)+"U"+str(order.user_id)
            if(order.message == None):
                order.message = message

            if(order.invoice == None):
                order.invoice = invoice
            else:
                checkinvoice = db.execute("SELECT orders.id, orders.invoice FROM orders WHERE orders.id =:param AND orders.invoice REGEXP '^[0-9]+$'  and app_version = 'V4'", {
                    "param": order.id
                }).first()
                if(checkinvoice is not None):
                    if(int(checkinvoice.invoice) <= 9):
                        order.invoice = str('ASEZ202320240') + \
                            str(checkinvoice.invoice)
                    else:
                        order.invoice = str('ASEZ20232024') + \
                            str(checkinvoice.invoice)

                else:
                    order.invoice = order.invoice
            order.reff = reff_id
            order.products = products

            # Item Total Amount
            order.item_total_amount = floatingValue(total_amount)
            order.delivery_charge = floatingValue(delivery_charge)
            order.discount = floatingValue(discount_amount)
            order.total_items = len(items)

            if(order.app_version == 'V4'):
                grand_total = roundOf(order.grand_total)
            else:
                grand_total = (
                    total_amount + delivery_charge) - discount_amount

                grand_total = roundOf(grand_total)

            order.created_at = order.created_at.strftime("%d %b %Y")
            order.grand_total = floatingValue(grand_total)

        # Order Status
        orderdata: OrdersModel = await OrderHelper.orderCurrentStatus(db=db, data=orderdata)

        # Total Records
        total_records = data.count()

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != 0):
            total_pages = int(total_pages[0]) + 1

        return {"status_code": HTTP_200_OK, "title": title, "order_items": orderdata, "total_amount": 0.00, "total_items": total_records, "current_page": page, "total_page": total_pages}

    except Exception as e:
        print(e)
        return {"status_code": HTTP_200_OK, "title": title, "order_items": [], "total_amount": 0.00, "total_items": 0, "current_page": page, "total_page": 0}


# Order Details
@v4_order_router.get("/details/{order_id}", response_model=OrderDetailSchema, dependencies=[Depends(JWTBearer())])
async def getOrderDetail(request: Request, order_id: int, db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.CheckBuyerOrder(
            request=request, order_id=order_id, db=db)
        if(checkuser == False):
            return {"status_code": HTTP_200_OK, "order": {}, "shipping_address": {}, "action_button": {}, "order_tracking": {}, "cancel_reason": {}, "return_reason": {}, "order_status": {}}

        data: OrdersModel = await OrderHelper.getOrderDetail(db=db, order_id=order_id)

        if(data == False):
            return {"status_code": HTTP_200_OK, "order": {}, "shipping_address": {}, "action_button": {}, "order_tracking": {}, "cancel_reason": {}, "return_reason": {}, "order_status": {}}
        else:
            # Order Reff ID
            order_reff_id = data.reff
            # Customize Order Reff
            reff_id = 'ASEZ'+str(data.reff)+"U"+str(data.user_id)
            message = ''
            invoice = ''
            if(data.message == None):
                data.message = message

            if(data.invoice == None):
                data.invoice = invoice
            else:
                checkinvoice = db.execute("SELECT orders.id, orders.invoice FROM orders WHERE orders.id =:param AND orders.invoice REGEXP '^[0-9]+$'  and app_version = 'V4'", {
                    "param": data.id
                }).first()
                if(checkinvoice is not None):
                    if(int(checkinvoice.invoice) <= 9):
                        data.invoice = str('ASEZ202320240') + \
                            str(checkinvoice.invoice)
                    else:
                        data.invoice = str('ASEZ20232024') + \
                            str(checkinvoice.invoice)

                else:
                    data.invoice = data.invoice
            data.reff = reff_id

            # Action Button
            action_button: OrdersModel = await OrderHelper.orderActionButton(db=db, data=data)

            # Current Status
            orderdata: OrdersModel = await OrderHelper.orderCurrentStatus(db=db, data=data)

            # Download Invoice
            download_invoice = ''
            if(orderdata.current_status['status_id'] >= 70 and orderdata.current_status['status_id'] < 980):
                download_invoice = db.query(OrderShippingModel).filter(
                    OrderShippingModel.order_id == data.id).first()
                if(download_invoice is not None):
                    if(download_invoice.invoice is None):
                        download_invoice = ''
                    else:
                        download_invoice = download_invoice.invoice

           # Calculation Total Amount of items
           # Get Order All Items
            orderitems = data.order_items.all()

            # Custom Order Items
            order_items: OrdersModel = await OrderHelper.customOrderItems(request=request, db=db, data=orderitems)

            total_amount = 0
            cancel_total_amount = 0
            return_total_amount = 0
            discount_amount = 0
            cancel_total_discount_amount = 0
            return_total_discount_amount = 0
            item_wallet_amount = 0
            use_wallet_amount = 0
            for orderitem in orderitems:
                total_amount += float(orderitem.total)
                use_wallet_amount += float(orderitem.wallet_amount)
                if(orderitem.status <= 81):
                    item_wallet_amount += float(orderitem.wallet_amount)

                # Check Cancle Items Amount
                if(orderitem.status == 980):
                    cancel_total_amount += float(orderitem.total)

                # # Check Return Items Amount
                if(orderitem.status >= 90 and orderitem.status != 91 and orderitem.status != 980):
                    return_total_amount += float(orderitem.total)

            if(orderdata.current_status['status_id'] >= 90 and orderdata.current_status['status_id'] != 91):
                item_wallet_amount = 0
                cancel_total_amount = 0
                return_total_amount = 0

            # Calculate Total Active Amount
            rest_total_amount = (
                total_amount - cancel_total_amount - return_total_amount)

            # CHECK ORDER DISCOUNT for V3
            order_date = data.created_at.strftime("%Y-%m-%d")
            if(data.discount != 0):
                order_discount = db.query(OrderDiscountModel).filter(
                    OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

                discount_rate = order_discount.discount
                if(data.discount_rate != 0):
                    discount_rate = data.discount_rate

                discount_amount = orderDiscountCalculation(app_version=data.app_version,
                                                           order_amount=rest_total_amount, discount_amount=data.discount, discount_rate=discount_rate)

            # Check Shipping Charge
            # delivery_charge = 0
            # if(data.delivery_charge != 0):CHANGES RAHUL
            shipping_charge = db.query(ShippingChargeModel).filter(
                ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

            delivery_charge = orderDeliveryCalculation(db=db, free_delivery=data.free_delivery, user_id=data.user_id, order_date=data.created_at,
                                                       app_version=data.app_version, order_amount=rest_total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=data.payment_method)

            # Order Summary
            cancel_visible = True
            if(cancel_total_amount == 0):
                cancel_visible = False

            return_visible = True
            if(return_total_amount == 0):
                return_visible = False

            discount_visible = True
            if(discount_amount == 0):
                discount_visible = False

            # Grand Total
            if(data.app_version == 'V4'):
                if(cancel_total_amount != 0 or return_total_amount != 0):

                    grand_total = (
                        rest_total_amount + delivery_charge) - discount_amount

                    round_off = roundOf(grand_total) - grand_total

                    grand_total = roundOf(grand_total + round_off)
                else:
                    round_off = (float(total_amount) +
                                 float(data.delivery_charge) - float(discount_amount))
                    round_off = (float(data.grand_total) - float(round_off))
                    # round_off = data.round_off

                    grand_total = roundOf(
                        data.grand_total)

            else:

                round_off = 0.00
                grand_total = (
                    rest_total_amount + delivery_charge) - discount_amount

                grand_total = grand_total
            if(data.partial_amount != 0):
                advance_payment = data.partial_amount
                cod_amount = (data.cod_amount)

                # amount_round_off = float(
                #     cod_amount) + float(advance_payment)
                # round_off_grand_total = (
                #     float(round(amount_round_off, 0)) - float(amount_round_off))
                # round_off = float(round_off_grand_total)
                partial_visible = True

                partial_payment = {
                    "advance_payment": floatingValue(advance_payment),
                    "cod": floatingValue(cod_amount)
                }

            else:
                advance_payment = 0
                cod_amount = 0
                partial_visible = False

                partial_payment = {
                    "advance_payment": "",
                    "cod": ""
                }

            order_summary = [
                {
                    'name': 'Payment Method',
                    'value': str(data.payment_method),
                    'currency': False,
                    'visible': True
                },
                {
                    'name': 'No of Items',
                    'value': str(len(orderitems)),
                    'currency': False,
                    'visible': True
                },
                {
                    'name': 'Item Total Amount',
                    'value': floatingValue(total_amount),
                    'currency':  True,
                    'visible': True
                },
                {
                    'name': 'Cancel Items (-)',
                    'value':  floatingValue(cancel_total_amount),
                    'currency': True,
                    'visible': cancel_visible
                },
                {
                    'name': 'Return Items (-)',
                    'value': floatingValue(return_total_amount),
                    'currency': True,
                    'visible': return_visible
                },
                {
                    'name': 'Discount (-)',
                    'value': floatingValue(discount_amount),
                    'currency': True,
                    'visible': discount_visible
                },
                {
                    'name': 'Shipping Charge',
                    'value': floatingValue(delivery_charge),
                    'currency': True,
                    'visible': True
                },

                {
                    'name': 'Advance Paid',
                    'value': floatingValue(advance_payment),
                    'currency': True,
                    'visible': False
                },
                {
                    'name': 'Cod Amount',
                    'value': floatingValue(cod_amount),
                    'currency': True,
                    'visible': False
                },
                {
                    'name': 'Round Off',
                    'value': floatingValue(round_off),
                    'currency': True,
                    'visible': True
                }
            ]

            grand_total = {
                'name': 'Grand Total',
                'value': floatingValue(grand_total),
                'currency': True,
                'visible': True
            }

            # Check Rewards on this order
            check_rewards_points = ''
            if(orderdata.current_status['status_id'] < 90):
                check_rewards = db.query(WalletModel).filter(
                    WalletModel.reff_id == order_reff_id).filter(WalletModel.reff_type == 'ORDER_REWARDS').first()
                if(check_rewards is not None):
                    check_rewards_points = (
                        float(grand_total['value']) + float(round_off) - float(delivery_charge))
                    check_rewards_points = (
                        float(check_rewards_points) * float(REWARDS_PERCENTAGE) / 100)
                    check_rewards_points = int(check_rewards_points)
                    if(check_rewards.status == 0):
                        check_rewards_points = str(
                            'You will get ')+str(check_rewards_points)+str(' Aseztak coin on this order')
                    else:
                        check_rewards_points = str(
                            check_rewards_points)+str(' Aseztak coin credited to your wallet')

            data.created_at = data.created_at.strftime("%d %b %Y")

            # Shipping Address
            shipping_address: OrdersModel = await OrderHelper.orderShippingAddress(db=db, data=orderdata)

            # Check Order Refund Message
            refund_message: OrdersModel = await OrderHelper.orderRefundMessage(db=db, data=data)

            if(refund_message != ''):
                orderdata.order_refund_message = refund_message

            # Order Return Period
            if(action_button['return_period'] != ''):
                orderdata.return_period = action_button['return_period']

            # Check Order Return Proof
            return_proof = {
                'rejected': False,
                'text': ''
            }
            checkreturnProof = db.query(OrderReturnProofModel).filter(
                OrderReturnProofModel.order_id == order_id).order_by(OrderReturnProofModel.updated_at.desc()).first()
            if(checkreturnProof):
                if(checkreturnProof.status == 1):
                    return_proof = {
                        'rejected': True,
                        'text': 'Your return image proof has been rejected'
                    }
            else:
                return_proof = {
                    'rejected': False,
                    'text': ''
                }

            orderdata.proof = return_proof

            # Order Tracking
            ordertracking: OrdersModel = await OrderHelper.ordertracking(db=db, data=orderdata)

            # Cancel Reason
            cancel_reason = [
                {
                    'selected': False,
                    'text': 'I want to change address for the order',
                },
                {
                    'selected': False,
                    'text': 'I want to buy different item',
                },
                {
                    'selected': False,
                    'text': 'I have changed my mind',
                },
                {
                    'selected': False,
                    'text': 'I want to convert my order prepaid',
                },
                {
                    'selected': False,
                    'text': 'Other',
                },
            ]

            # Return Reason
            return_reason = [
                {
                    'selected': False,
                    'text': 'Wrong items recieved',
                },
                {
                    'selected': False,
                    'text': 'Recieved broken/damage product',
                },
                {
                    'selected': False,
                    'text': 'Product is missing in the packages',
                },
                {
                    'selected': False,
                    'text': 'Recieved different items from product description',
                },
                {
                    'selected': False,
                    'text': 'Other',
                },
            ]

            # Order Status
            order_status: OrdersModel = await OrderHelper.orderStatus(db=db, data=orderdata)

            # Return Proof
            return_proof_images = []
            return_images = db.query(OrderReturnProofModel).filter(
                OrderReturnProofModel.order_id == order_id).filter(OrderReturnProofModel.order_type == 'RETURN').all()

            if(len(return_images) > 0):
                for img in return_images:
                    imgstatus = ''
                    if(img.status == 1):
                        imgstatus = 'REJECTED'

                    rimg = {
                        'id': img.id,
                        'image': img.image,
                        'status': imgstatus
                    }
                    return_proof_images.append(rimg)

            wallet_amount = float(item_wallet_amount)
            if(item_wallet_amount == 0):
                wallet_amount = float(use_wallet_amount)

            # Check Refunded Transaction
            check_refund_transactions = db.query(TransactionsModel).filter(
                TransactionsModel.order_ref_id == order_reff_id).filter(TransactionsModel.refund_txn_id.isnot(None)).all()
            refund_transactions = []
            if(len(check_refund_transactions) > 0):
                for transaction in check_refund_transactions:
                    t = {
                        'title': 'Refunded',
                        'transaction_id': transaction.refund_txn_id,
                        'amount': transaction.amount
                    }
                    refund_transactions.append(t)

            return {"status_code": HTTP_200_OK, "rewards_text": check_rewards_points, "use_wallet_amount": wallet_amount, "download_invoice": download_invoice, "order": orderdata, "order_summary": order_summary, "grand_total": grand_total, "order_items": order_items, "shipping_address": shipping_address, "action_button": action_button['action'], "order_tracking": ordertracking, "cancel_reason": cancel_reason, "return_reason": return_reason, "order_status": order_status, "return_proof_images": return_proof_images, "partial_payment": partial_payment, "refund_transactions": refund_transactions}

    except Exception as e:
        print(e)


# Cancel Ordder
@v4_order_router.post("/cancel", dependencies=[Depends(JWTBearer())])
async def cancelOrder(request: Request, data: cancelOrderscheam, db: Session = Depends(get_db)):
    try:
        # Check User
        checkuser = CheckUser.CheckBuyerOrder(
            request=request, order_id=data.order_id, db=db)
        if(checkuser == False):
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

        # Get Order
        order = db.query(OrdersModel).filter(
            OrdersModel.id == data.order_id).first()

        # buyer Shipping
        buyerShipping = db.query(ShippingAddressModel).filter(
            ShippingAddressModel.id == order.address_id).first()

        # Latest Order Status
        order_status = db.query(OrderStatusModel).filter(
            OrderStatusModel.order_id == data.order_id).order_by(OrderStatusModel.id.desc()).first()

        # Check Shipping
        shipping = db.query(OrderShippingModel).filter(
            OrderShippingModel.order_id == data.order_id).first()

        if(shipping and shipping.wbns):

            if(config.APP_MODE == 'production'):
                token = API_TOKEN
                url = API_CANCEL_URL
            else:
                token = API_TEST_TOKEN
                url = API_TEST_CANCEL_URL

            authorizationtoken = "Token "+str(token)

            payload = {
                "waybill": str(shipping.wbns),
                "cancellation": "true"
            }
            headers = {
                "Accept": "application/json",
                "Authorization": authorizationtoken,
                "Content-Type": "application/json"
            }

            response = requests.request(
                "POST", url, json=payload, headers=headers)

            response = json.loads(response.content.decode('utf-8'))

            if(response['status'] == True):
                shipping.status = response['status']
                shipping.remark = response['remark']
                shipping.updated_at = datetime.now()
                db.flush()
                db.commit()

        # Check Order Items Selected
        if(len(data.items) > 0):

            for item in data.items:
                itemdata = db.query(OrderItemsModel).filter(
                    OrderItemsModel.id == item).first()
                itemdata.status = 980
                itemdata.message = data.message
                itemdata.updated_at = datetime.now()
                db.flush()
                db.commit()

                # New Changes Wallet Amount
                if(itemdata.wallet_amount != 0):
                    todaydate = date.today()
                    td = timedelta(days=COIN_EXPIRING_DAYS)
                    valid_upto = todaydate + td
                    valid_upto = valid_upto.strftime("%Y-%m-%d")
                    db_wallet = WalletModel(
                        user_id=order.user_id,
                        reff_id=order.id,
                        reff_type='ORDER_CANCELLED',
                        description='Cancelled (Order ID: ' +
                        str(order.order_number)+str(')'),
                        type='CR',
                        amount=itemdata.wallet_amount,
                        status=1,
                        valid_upto=valid_upto,
                        created_at=datetime.now(),
                        updated_at=datetime.now()
                    )
                    db.add(db_wallet)
                    db.commit()

                # Product Data
                product = db.query(ProductModel).filter(
                    ProductModel.id == itemdata.product_id).first()

                # Check if order status less then 50
                if(order_status.status < 50):

                    item_price_id = itemdata.uuid.split('-')
                    item_price = db.query(ProductPricingModel).filter(
                        ProductPricingModel.id == item_price_id[1]).first()

                    if(item_price):

                        # Check Inventory of price
                        inventory = db.query(InventoryModel).filter(
                            InventoryModel.pricing_id == item_price_id[1]).first()

                        if(inventory.unlimited == 0):
                            inventory.stock = int(
                                inventory.stock) + int(itemdata.quantity)
                            inventory.out_of_stock = 0
                            db.flush()
                            db.commit()

                    # Item Total Amount
                    item_total_amount = (
                        itemdata.price * itemdata.tax / 100) + itemdata.price
                    item_total_amount = item_total_amount * itemdata.quantity

                    # Seller Account Maintainence
                    # Today Date
                    today = date.today()
                    # YY-mm-dd
                    today = today.strftime("%Y-%m-%d")
                    dbaccount = AccountsModel(
                        user_id=product.userid,
                        txn_date=today,
                        head='CANCELLED',
                        txn_description='CANCELLED (' +
                        str(order.order_number) + ')',
                        txn_type='CR',
                        txn_amount=round(item_total_amount, 2),
                        created_at=datetime.now(),
                        updated_at=datetime.now()
                    )
                    db.add(dbaccount)
                    db.commit()
                    db.refresh(dbaccount)

        # Check Order Items
        checkitem = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == data.order_id).filter(OrderItemsModel.status != 980).first()
        productTitle = ''

        if(checkitem == None):
            # Update Order Status
            dborderstatus = OrderStatusModel(
                order_id=data.order_id,
                status=980,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.add(dborderstatus)
            db.commit()
            db.refresh(dborderstatus)

            # Update Order Message
            order.message = data.message
            order.updated_at = datetime.now()
            db.flush()
            db.commit()

            # Update all order items status
            oitems = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == data.order_id).all()
            for oitem in oitems:
                # New Changes Wallet Amount
                # Product Title
                checkProduct = db.query(ProductModel).filter(
                    ProductModel.id == oitem.product_id).first()
                productTitle = checkProduct.title

                oitem.status = 980,
                oitem.message = data.message,
                oitem.updated_at = datetime.now()
                db.flush()
                db.commit()

        # Check if items count is Zero
        if(len(data.items) == 0):

            if(order_status.status < 50):
                items_total_amount = 0

                # Order items
                orderitems = db.query(OrderItemsModel).filter(
                    OrderItemsModel.order_id == data.order_id).filter(OrderItemsModel.status != 980).all()

                for orderitem in orderitems:
                    # product data
                    productdata = db.query(ProductModel).filter(
                        ProductModel.id == orderitem.product_id).first()

                    items_price_id = orderitem.uuid.split('-')

                    items_price = db.query(ProductPricingModel).filter(
                        ProductPricingModel.id == items_price_id[1]).first()

                    if(items_price):

                        # Check Inventory of price
                        inventories = db.query(InventoryModel).filter(
                            InventoryModel.pricing_id == items_price_id[1]).first()

                        if(inventories.unlimited == 0):
                            inventories.stock = int(
                                inventories.stock) + int(orderitem.quantity)
                            inventories.out_of_stock = 0
                            db.flush()
                            db.commit()

                    # Item Total Amount
                    items_total = (
                        orderitem.price * orderitem.tax / 100) + orderitem.price
                    items_total_amount += items_total * orderitem.quantity

                # Seller Account Maintainence
                # Today Date
                today = date.today()
                # YY-mm-dd
                today = today.strftime("%Y-%m-%d")
                dbaccounts = AccountsModel(
                    user_id=productdata.userid,
                    txn_date=today,
                    head='CANCELLED',
                    txn_description='CANCELLED (' +
                    str(order.order_number) + ')',
                    txn_type='CR',
                    txn_amount=round(items_total_amount, 2),
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )
                db.add(dbaccounts)
                db.commit()
                db.refresh(dbaccounts)

            # Update Order Status

            dborderstatusa = OrderStatusModel(
                order_id=data.order_id,
                status=980,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.add(dborderstatusa)
            db.commit()

            # Update Order Message
            order.message = data.message
            order.updated_at = datetime.now()
            db.flush()
            db.commit()

            # Update all order items status
            oitemms = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == data.order_id).all()
            for oitems in oitemms:
                # New Changes Wallet Amount
                if(oitems.wallet_amount != 0):
                    todaydate = date.today()
                    td = timedelta(days=COIN_EXPIRING_DAYS)
                    valid_upto = todaydate + td
                    valid_upto = valid_upto.strftime("%Y-%m-%d")
                    db_wallet = WalletModel(
                        user_id=order.user_id,
                        reff_id=data.order_id,
                        reff_type='ORDER_CANCELLED',
                        description='Cancelled (Order ID: ' +
                        str(order.order_number)+str(')'),
                        type='CR',
                        amount=oitems.wallet_amount,
                        status=1,
                        valid_upto=valid_upto,
                        created_at=datetime.now(),
                        updated_at=datetime.now()
                    )
                    db.add(db_wallet)
                    db.commit()

                # product data
                productdata = db.query(ProductModel).filter(
                    ProductModel.id == oitems.product_id).first()
                # Product Title
                productTitle = productdata.title

                oitems.status = 980,
                oitems.message = data.message,
                oitems.updated_at = datetime.now()
                db.flush()
                db.commit()

        # Seller Data
        itemData = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == order.id).first()
        productData = db.query(ProductModel).filter(
            ProductModel.id == itemData.product_id).first()
        seller = db.query(UserModel).filter(
            UserModel.id == productData.userid).first()

        # Send Cancelled Order Message to Buyer and Seller
        if(len(data.items) == 0):

            product_title = str(productTitle)
            product_title = product_title[:15]
        else:

            c_total_items = len(data.items)

            checkItemdata = db.query(OrderItemsModel).filter(
                OrderItemsModel.id == data.items[0]).first()
            checkItemProduct = db.query(ProductModel).filter(
                ProductModel.id == checkItemdata.product_id).first()

            if(c_total_items > 1):
                c_total_items = (c_total_items - 1)

                product_title = str(
                    checkItemProduct.title)

                product_title = product_title[:15]

                product_title = str(
                    product_title)+' and '+str(c_total_items)+' more'
            else:
                product_title = str(checkItemProduct.title)
                product_title = product_title[:15]

        # CHECK final current status
        check_latest_status = db.query(OrderStatusModel).filter(
            OrderStatusModel.order_id == order.id).order_by(OrderStatusModel.id.desc()).first()

        if(check_latest_status.status == 980):

            # Send Message to Buyer
            await Message.SendOrderCancelMessagetoBuyer(
                mobile=buyerShipping.phone, message=product_title, order_number=order.order_number, buyer='Buyer')

            # Send Message to Seller
            await Message.SendOrderCancelMessagetoBuyer(
                mobile=seller.mobile, message=product_title, order_number=order.order_number, buyer='Buyer')

        else:

            # Send Message to Buyer
            await Message.SendOrderItemCancelMessagetoBuyer(
                mobile=buyerShipping.phone, message=product_title, order_number=order.order_number, buyer='Buyer')

            # Send Message to Seller
            await Message.SendOrderItemCancelMessagetoBuyer(
                mobile=seller.mobile, message=product_title, order_number=order.order_number, buyer='Buyer')

        return {"status_code": HTTP_200_OK, "message": "Order has been Cancelled"}

    except Exception as e:
        print(e)
        return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Invalid Input!"}


@v4_order_router.post("/return", dependencies=[Depends(JWTBearer())])
async def returnOrder(request: Request, order_id: int = Form(...), message: str = Form(...), items: Optional[List] = Form(...), account_type: str = Form(...), account_holder_name: str = Form(...), acc_no: str = Form(...), bank: str = Form(...), ifsc_code: str = Form(...), branch: str = Form(...), images: List[UploadFile] = File(...), db: Session = Depends(get_db)):
    try:

        # Check User
        checkuser = CheckUser.CheckBuyerOrder(
            request=request, order_id=order_id, db=db)
        if(checkuser == False):
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}

        # get order
        order = db.query(OrdersModel).filter(
            OrdersModel.id == order_id).first()
        # buyer Shipping
        buyerShipping = db.query(ShippingAddressModel).filter(
            ShippingAddressModel.id == order.address_id).first()

        if(account_type == ''):
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Please add bank account details"}

        # Update profile info (Bank Details)
        profile = db.query(BankAccountsModel).filter(
            BankAccountsModel.user_id == order.user_id).first()

        account_type = account_type.lower()

        if(profile is not None):
            profile.account_type = account_type
            profile.account_holder_name = account_holder_name
            profile.acc_no = acc_no
            profile.bank = bank
            profile.ifsc_code = ifsc_code
            profile.branch = branch
            db.flush()
            db.commit()
        else:
            db_bank_details = BankAccountsModel(
                user_id=order.user_id,
                account_type=account_type,
                account_holder_name=account_holder_name,
                acc_no=acc_no,
                bank=bank,
                ifsc_code=ifsc_code,
                branch=branch,
            )
            db.add(db_bank_details)
            db.commit()

        # Update status of order items
        productTitle = ''
        for item in items:

            itemdata = db.query(OrderItemsModel).filter(
                OrderItemsModel.id == item).first()
            # # New Changes Wallet Amount Credit
            # if(itemdata.wallet_amount != 0):
            #     todaydate = date.today()
            #     td = timedelta(days=COIN_EXPIRING_DAYS)
            #     valid_upto = todaydate + td
            #     valid_upto = valid_upto.strftime("%Y-%m-%d")
            #     db_wallet = WalletModel(
            #         user_id=order.user_id,
            #         reff_id=order.id,
            #         reff_type='ORDER_RETURNED',
            #         description='Returned (Order ID:' +
            #         str(order.order_number)+str(')'),
            #         type='CR',
            #         amount=itemdata.wallet_amount,
            #         status=1,
            #         valid_upto=valid_upto,
            #         created_at=datetime.now(),
            #         updated_at=datetime.now()
            #     )
            #     db.add(db_wallet)
            #     db.commit()
            # Product data
            product = db.query(ProductModel).filter(
                ProductModel.id == itemdata.product_id).first()
            productTitle = product.title

            itemdata.status = 80
            itemdata.message = message
            itemdata.updated_at = datetime.now()

            db.flush()
            db.commit()

        # Check Items
        checkitems = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order_id).filter(
            OrderItemsModel.status <= 70).filter(OrderItemsModel.status != 980).first()

        if(checkitems == None):
            # Update Order Status
            dborderstatus = OrderStatusModel(
                order_id=order_id,
                status=80,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.add(dborderstatus)
            db.commit()
            db.refresh(dborderstatus)

            # Update Order
            order.message = message
            order.updated_at = datetime.now()
            db.flush()
            db.commit()

        # Check selected items
        if(len(items) == 0):
            orderitems = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == order_id).filter(OrderItemsModel.status != 980).all()

            for orderitem in orderitems:
                # # New Changes Wallet Amount Credit
                # if(orderitem.wallet_amount != 0):
                #     todaydate = date.today()
                #     td = timedelta(days=COIN_EXPIRING_DAYS)
                #     valid_upto = todaydate + td
                #     valid_upto = valid_upto.strftime("%Y-%m-%d")
                #     db_wallet = WalletModel(
                #         user_id=order.user_id,
                #         reff_id=order.id,
                #         reff_type='ORDER_RETURNED',
                #         description='Returned (Order ID: ' +
                #         str(order.order_number)+str(')'),
                #         type='CR',
                #         amount=orderitem.wallet_amount,
                #         status=1,
                #         valid_upto=valid_upto,
                #         created_at=datetime.now(),
                #         updated_at=datetime.now()
                #     )
                #     db.add(db_wallet)
                #     db.commit()

                # Product data
                product = db.query(ProductModel).filter(
                    ProductModel.id == orderitem.product_id).first()
                productTitle = product.title

                orderitem.status = 80
                orderitem.message = message
                orderitem.updated_at = datetime.now()
                db.flush()
                db.commit()

            # Update Order Status
            dborderstatuss = OrderStatusModel(
                order_id=order_id,
                status=80,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.add(dborderstatuss)
            db.commit()
            db.refresh(dborderstatuss)

            # Update Order
            order.message = message
            order.updated_at = datetime.now()
            db.flush()
            db.commit()

        if(len(images) > 0):
            for image in images:
                file = image.filename.split(".")
                filename = f"{uuid.uuid4()}.{file[1]}"
                # Set Path Of image
                file_name = getcwd()+"/app/static/proof/"+filename

                # Upload image in path
                with open(file_name, 'wb+') as f:
                    f.write(image.file.read())
                    f.close()

                # open the image
                uploaded_file = getcwd()+"/app/static/proof/" + str(filename)
                filename = str(filename)
                # Upload Photo
                MediaHelper.uploadOrderReturnProof(
                    uploaded_file=uploaded_file, file_name=filename)

                # Remove Original Image from Path

                unlink(uploaded_file)

                file_path = str(
                    linode_obj_config['endpoint_url'])+'/proof/'+str(filename)

                db_orderproof = OrderReturnProofModel(
                    order_id=order_id,
                    image=file_path,
                    status=51,
                    order_type='RETURN',
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )
                db.add(db_orderproof)
                db.commit()
                db.refresh(db_orderproof)

        # Send Return Initiated Notification to buyer
        if(len(items) == 0):

            product_title = str(productTitle)
            product_title = product_title[:15]
        else:

            c_total_items = len(items)

            checkItemdata = db.query(OrderItemsModel).filter(
                OrderItemsModel.id == items[0]).first()
            checkItemProduct = db.query(ProductModel).filter(
                ProductModel.id == checkItemdata.product_id).first()

            if(c_total_items > 1):
                c_total_items = (c_total_items - 1)

                product_title = str(
                    checkItemProduct.title)

                product_title = product_title[:15]

                product_title = str(
                    product_title)+' and '+str(c_total_items)+' more'
            else:
                product_title = str(checkItemProduct.title)
                product_title = product_title[:15]

        itemData = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 80).first()
        # Buyer
        buyerdetails = db.query(UserModel).filter(
            UserModel.id == order.user_id).first()

        # Check Order Items
        notification_image = db.query(ProductMediaModel).filter(
            ProductMediaModel.model_id == itemData.product_id).filter(ProductMediaModel.deleted_at.is_(None)).first()

        # Send Notification to Buyer
        b_notification_body = 'Return request for ' + str(product_title) + ' with Order ID ' + str(
            order.order_number) + ' has been Initiated.'
        b_notification_title = 'Return Initiated'
        b_path = '/order-detail'
        b_notification_image = notification_image.file_path

        # Send Buyer Notification
        if(buyerdetails.fcm_token is not None):
            await Message.SendOrderNotificationtoBuyer(user_token=buyerdetails.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=order.id)

        return {"status_code": HTTP_200_OK, "message": "Return request submitted successfully"}

    except Exception as e:
        print(e)
