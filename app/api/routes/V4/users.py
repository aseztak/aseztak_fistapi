from os import getcwd, unlink
from typing import Optional
from starlette.requests import Request

from app.api.helpers.media import MediaHelper
from app.db.schemas.user_schema import UserBankDetails, UserBankDetailsSchema,  UpdateProfile, UpdateMobileSchema, UpdateMobileNumberSchema

from app.services.auth_bearer import JWTBearer
import uuid
from app.db.config import  get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.users import *
from app.services.auth import auth
from app.db.models.billing_address import BillingAddressModel
from fastapi import APIRouter,  Depends, File, Form, UploadFile, status
from starlette.status import HTTP_200_OK
from app.resources.strings import *
v4_users_router = APIRouter()


@v4_users_router.get("/me", dependencies=[Depends(JWTBearer())])
async def me(request: Request, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        # user
        user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        # profile
        profile = db.query(UserProfileModel).filter(
            UserProfileModel.user_id == user.id).first()

        profile_exist = False
        business_type = ''
        proof_type = ''
        proof = ''
        if(profile):
            profile_exist = True
            business_type = profile.business_type
            proof_type = profile.proof_type
            proof = profile.proof

        # billing
        billing_address = db.query(BillingAddressModel).filter(
            BillingAddressModel.user_id == user.id).first()
        billing_address_exist = False
        if(billing_address):
            billing_address_exist = True

        # bank account
        bank_account = db.query(BankAccountsModel).filter(
            BankAccountsModel.user_id == user.id).first()
        bank_account_exist = False
        if(bank_account):
            bank_account_exist = True

        # Buyer Documents
        buyer_document = db.query(UserDocumentModel).filter(
            UserDocumentModel.user_id == user.id).first()
        document_proof = ''
        if(buyer_document is not None):
            document_proof = buyer_document.file_path

        user = {
            'name': user.name,
            'email': user.email,
            'mobile': user.mobile,
            'country': user.country,
            'region': user.region,
            'city': user.city,
            'pincode': user.pincode,
            'business_type': business_type,
            'proof_type': proof_type,
            'proof': proof,
            'document_proof': document_proof,
            'status': user.status
        }

        # Document Type
        document_type = DOCUMENT_TYPE

        return {"status_code": HTTP_200_OK, "user": user, "profile_exist": profile_exist, "bank_account_exist": bank_account_exist, "billing_address_exist": billing_address_exist, "document_type": document_type}
    except Exception as e:
        print(e)

#CLOSED 2024-01-10
# @v4_users_router.get("/status/{user_id}")
# async def UserStatus(user_id: int, db: Session = Depends(get_db)):
#     try:

#         user = db.query(UserModel).filter(
#             UserModel.id == user_id).first()

#         if(user is not None):
#             return {"status": user.status}
#         else:
#             return {"status": 0}

#     except Exception as e:
#         return 401


@v4_users_router.post("/update/profile", dependencies=[Depends(JWTBearer())])
async def update_profile(request: Request, user: UpdateProfile, db: Session = Depends(get_db)):

    userdata = auth(request=request)

    document_type = DOCUMENT_TYPE
    try:
        user = await updateProfile(db=db, data=user, user_id=userdata['id'])

        if(user):
            return {'status_code': status.HTTP_202_ACCEPTED, 'message': 'Updated Successfully', 'user': user, 'document_type': document_type}
        else:
            return {'status_code': status.HTTP_400_BAD_REQUEST, 'message': 'Invalid Input', 'user': {}, 'document_type': document_type}
    except Exception as e:
        print(e)


@v4_users_router.post("/update/buyer/profile", dependencies=[Depends(JWTBearer())])
async def update_buyer_profile(request: Request, name: str = Form(...), email: Optional[str] = Form(None), proof: str = Form(...), proof_type: str = Form(...),  proof_file: Optional[UploadFile] = File(None),  country: str = Form(...), region: str = Form(...), pincode: str = Form(...), city: str = Form(...), db: Session = Depends(get_db)):
    document_type = DOCUMENT_TYPE
    try:
        # User
        userdata = auth(request=request)
        user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        if(email == None):
            email = str(user.mobile)+'@aseztak.com'
        else:
            email = email

        if(user):
            user.name = name
            user.email = email
            user.country = country
            user.region = region
            user.city = city
            user.pincode = pincode
            user.status = 1
            db.flush()
            db.commit()

        # User Profile
        profile = user.profile
        if(profile is None):
            profile = UserProfileModel(
                user_id=user.id,
                proof=proof,
                proof_type=proof_type
            )
            db.add(profile)
            db.commit()
        else:
            profile.proof = proof
            profile.proof_type = proof_type
            db.flush()
            db.commit()

        # Upload Buyer Documents
        if(proof_file != None):
            file = proof_file.filename.split(".")

            filename = f"{uuid.uuid4()}.{file[1]}"

            # Set Path Of image
            file_name = getcwd()+"/app/static/userdocument/"+filename

            # Upload image in path
            with open(file_name, 'wb+') as f:
                f.write(proof_file.file.read())
                f.close()

            # open the image
            uploaded_file = getcwd()+"/app/static/userdocument/" + str(filename)
            filename = str(filename)
            # Upload Photo
            MediaHelper.uploadUserDocument(
                uploaded_file=uploaded_file, file_name=filename)

            # Remove Original Image from Path

            unlink(uploaded_file)

            file_path = str(
                linode_obj_config['endpoint_url'])+'/userdocument/'+str(filename)

            # Insert Data in Documents table
            db_documents = UserDocumentModel(
                user_id=user.id,
                doc_type=proof_type,
                file_path=file_path,
                filename='',
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.add(db_documents)
            db.commit()

        userdata = {
            'name': user.name,
            'email': user.email,
            'mobile': user.mobile,
            'country': user.country,
            'region': user.region,
            'city': user.city,
            'business_type': '',
            'proof_type': profile.proof_type,
            'proof': profile.proof,
            'status': user.status
        }
        return {'status_code': status.HTTP_200_OK, 'message': 'Updated Successfully', 'user': user, 'document_type': document_type}

    except Exception as e:
        print(e)
        return {'status_code': status.HTTP_304_NOT_MODIFIED, 'message': 'Invalid Input', 'user': {}, 'document_type': document_type}

# Change Mobile Number


@v4_users_router.post("/change/mobile", dependencies=[Depends(JWTBearer())])
async def change_mobile(request: Request, user: UpdateMobileSchema, db: Session = Depends(get_db)):
    try:

        document_type = DOCUMENT_TYPE

        userdata = auth(request=request)

        # New Changes
        userdata = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        userdata = {
            'id': userdata.id,
            'email': userdata.email,
            'mobile': userdata.mobile,
            'region': userdata.region,
            'city': userdata.city,
            'pincode': userdata.pincode
        }

        user = await BuyerChangeMobileNumber(db=db, data=user, mobile=userdata['mobile'])

        if(user['status'] == True):

            return {'status_code': status.HTTP_200_OK, 'message': 'Otp sent to your mobile number', 'user': user['user'], 'document_type': document_type}
        else:
            return {'status_code': status.HTTP_304_NOT_MODIFIED, 'message': 'Mobile Number Already Exist', 'user': user['user'], 'document_type': document_type}
    except Exception as e:
        print(e)


@v4_users_router.post("/update/mobile", dependencies=[Depends(JWTBearer())])
async def update_mobile(request: Request, user: UpdateMobileNumberSchema, db: Session = Depends(get_db)):
    try:

        document_type = DOCUMENT_TYPE

        userdata = auth(request=request)
        # New Changes
        userdata = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        userdata = {
            'id': userdata.id,
            'email': userdata.email,
            'mobile': userdata.mobile,
            'region': userdata.region,
            'city': userdata.city,
            'pincode': userdata.pincode
        }
        user = await BuyerupdateMobileNumber(db=db, data=user, mobile=userdata['mobile'])

        if(user['status'] == True):
            return {'status_code': status.HTTP_200_OK, 'message': 'Mobile Number Updated', 'user':  user['user'], 'document_type': document_type}
        else:
            return {'status_code': status.HTTP_304_NOT_MODIFIED, 'message': 'Invalid otp', 'user': {}, 'document_type': document_type}
    except Exception as e:
        print(e)


# add Bank details
@v4_users_router.post("/update/bank/details", dependencies=[Depends(JWTBearer())])
async def update_mobile(request: Request, data: UserBankDetails,  db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        checkBankDetails = db.query(BankAccountsModel).filter(
            BankAccountsModel.user_id == userdata['id']).first()

        if(checkBankDetails is not None):
            checkBankDetails.account_holder_name = data.account_holder_name
            checkBankDetails.account_type = data.account_type
            checkBankDetails.bank = data.bank
            checkBankDetails.branch = data.branch
            checkBankDetails.acc_no = data.acc_no
            checkBankDetails.ifsc_code = data.ifsc_code
            db.flush()
            db.commit()
        else:
            dbBankdetails = BankAccountsModel(
                user_id=userdata['id'],
                account_holder_name=data.account_holder_name,
                account_type=data.account_type,
                bank=data.bank,
                branch=data.branch,
                acc_no=data.acc_no,
                ifsc_code=data.ifsc_code
            )
            db.add(dbBankdetails)
            db.commit()
            db.refresh(dbBankdetails)

        return {'status_code': status.HTTP_200_OK, 'message': 'Bank Details Added', }
    except Exception as e:
        print(e)


@v4_users_router.get("/bank/details", response_model=UserBankDetailsSchema, dependencies=[Depends(JWTBearer())])
async def update_mobile(request: Request,   db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        bank_details = db.query(BankAccountsModel).filter(
            BankAccountsModel.user_id == userdata['id']).first()

        if(bank_details is not None):

            if('savings' in str(bank_details.account_type)):
                acc_type = 'Savings'
            else:
                acc_type = 'Current'

            account = {
                'account_holder_name': bank_details.account_holder_name,
                'account_type': acc_type,
                'bank': bank_details.bank,
                'branch': bank_details.branch,
                'acc_no': bank_details.acc_no,
                'ifsc_code': bank_details.ifsc_code
            }
        else:
            account = {
                'account_holder_name': '',
                'account_type': '',
                'bank': '',
                'branch': '',
                'acc_no': '',
                'ifsc_code': ''
            }

        return {"status_code": HTTP_200_OK, "account": account}
    except Exception as e:
        print(e)


# @users_router.post("/send-otp", tags=["user"])
# async def sendOtp(mobile: str, db: Session = Depends(get_db)):
#     try:
#         exist = userExist(db=db, mobile=mobile)
#         if(exist):
#             return {'status': True, 'message': "OTP sent to your Mobile"}
#         else:
#             return {'status': False, 'message': "Invalid Credential "}
#     except Exception as e:
#         print(e)
