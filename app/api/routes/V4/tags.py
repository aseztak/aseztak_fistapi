
from app.api.helpers.tags import search_tags_products
from app.db.schemas.tags_schema import TagsDataSchema
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from app.api.helpers.users import *
from fastapi import APIRouter,  Depends


v4_tags_router = APIRouter()


# Tag /Products search result
@v4_tags_router.get("/search", response_model=TagsDataSchema, dependencies=[Depends(JWTBearer())])
async def search(nameLike: str, db: Session = Depends(get_db)):
    try:
        results = await search_tags_products(nameLike=nameLike,  db=db)
        return {"tags": results}

    except Exception as e:
        print(e)
