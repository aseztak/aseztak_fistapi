

from datetime import date, timedelta, datetime

from sqlalchemy import func
from starlette.status import HTTP_202_ACCEPTED, HTTP_404_NOT_FOUND, HTTP_200_OK
from app.db.models.categories import CategoriesModel, ProductCategories
from app.db.models.media import ProductMediaModel
from app.db.models.products import FavouriteModel, InventoryModel, ProductModel, ProductPricingModel
from app.api.helpers.products import ProductModel, ProductsHelper

from app.db.schemas.product_schema import ProductDetailNew, FilterDataSchema, ProductDataTabWise, ProductData, ProductDataSellerWiseSchema, RecentlyViewedProductSchema, filterAttributesV4Schema, ProductSearchList, ProductSearchResult, SellerWiseProductData, mostsellingdiscountedproducts

from app.services.auth_bearer import JWTBearer
from app.db.config import  get_db
from sqlalchemy.orm.session import Session

from starlette.requests import Request
from app.services.auth import auth
from fastapi import APIRouter,  Depends


from app.api.util.calculation import productPricecalculation, floatingValue, calculatediscountprice

from app.db.models.user import UserModel
from app.db.models.brand import BrandsModel

from app.db.models.attribute_family_values import AttriFamilyValuesModel

from app.db.models.attributes import AttributeModel

from sqlalchemy.orm import joinedload

from app.db.models.productattribute import ProductAttributeModel

from app.db.models.tag import TagModel
from app.db.models.product_tags import ProductTagModel
from app.api.helpers.discount import Discount
from app.api.helpers.services import AsezServices
from app.db.models.product_discount import ProductDiscountModel

from app.core import config
from app.api.helpers.orders import OrderHelper
from app.api.helpers.discount import Discount

v4_product_router = APIRouter()


@v4_product_router.get("/detail/{product_id}", response_model=ProductDetailNew,  dependencies=[Depends(JWTBearer())])
async def get_product_detail(request: Request, product_id: int, db: Session = Depends(get_db)):

    userdata = auth(request=request)
    # Check User
    check_user = db.query(UserModel).filter(
        UserModel.id == userdata['id']).first()
    try:
        data = await ProductsHelper.get_product(
            db=db, product_id=product_id)

        if(data == False):

            return {"status_code": HTTP_404_NOT_FOUND}

        else:
            today = date.today()
            today_date = today.strftime('%Y-%m-%d')
            # Check Delivery Charger for new user
            new_free_delivery = False
            if(config.FREE_DELIVERY == "True"):
                check_order_exist = await OrderHelper.check_order_exist(db=db, user_id=userdata['id'])
                new_free_delivery = check_order_exist
            # if(new_free_delivery == True):
            #     purchase_limit_text = ' No Limit'
            # else:
            # Check Seller Purchase Limit
            check_seller_purchase_limit = await ProductsHelper.check_seller_facility(db=db, user_id=data.seller.id, type='purchase_limit')
            purchase_limit_text = ' No Limit'
            if(check_seller_purchase_limit is not None):
                purchase_limit_text = '₹' + \
                    str(check_seller_purchase_limit.key_value)

            # Aseztak Service
            check_asez_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

            # Customize Product Pricing Data
            # Check Pricing
            pricing = data.product_pricing[0]
            # Check Product Discount
            check_product_discount = data.product_discount.filter(
                func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
            product_discount = {
                'rate': '0.00',
                'product_sale_price': '0.00',
                'discount_amount': '0.00',

            }
            pricingdata = {
                'id': 0,
                'mrp': '0.00',
                'product_discount': product_discount,
                'price': '0.00',
                'discount': str(0.0),
                'discount_price': '0.00',
                'moq': 0,
                'unit': ''
            }

            stock = False
            if(pricing):
                # Get Product Price
                product_price: ProductModel = await ProductsHelper.getPrice(db, data, pricing, asez_service=check_asez_service, app_version='V4')
                # Get Product Sale Price
                product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=check_asez_service.rate,
                                                             gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                # Check Discounted Price
                discount_price = float(0.00)
                checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                if(checkdiscount != 0):
                    discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))

                if(check_product_discount is not None):
                    product_discount = {
                        'rate': floatingValue(check_product_discount.discount),
                        'product_sale_price': floatingValue(product_sale_price),
                        'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),


                    }

                # Restructure Price data
                pricingdata = {
                    'id': pricing.id,
                    'mrp': floatingValue(pricing.mrp),
                    'product_discount': product_discount,
                    'price': floatingValue(product_price),
                    'discount': str(checkdiscount),
                    'discount_price': floatingValue(discount_price),
                    'moq': pricing.moq,
                    'unit': pricing.unit
                }

                # Check Product Stock
                # Check Stock
                checkStock = db.query(ProductPricingModel.id).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.product_id == data.id).filter(InventoryModel.out_of_stock == 0).filter(ProductPricingModel.deleted_at.is_(None)).first()

                stock = True
                if(checkStock is not None):
                    stock = True
                else:
                    stock = False

            # Product Images
            images = data.images

            imagedata = []
            if(len(images) > 0):

                for image in images:
                    img = {
                        'id': image.id,
                        'image': image.file_path
                    }
                    imagedata.append(img)

            # Product Favourite
            favourite = data.wishlist.filter(
                FavouriteModel.user_id == userdata['id']).first()
            if(favourite is not None):
                favourite = True
            else:
                favourite = False

            # Custome Static Text
            productstatic_text: ProductModel = await ProductsHelper.ProductstaticText(request=request, db=db, free_delivery=new_free_delivery, data=data)

            # Seller Information
            seller = db.query(UserModel).filter(
                UserModel.id == data.userid).first()
            # Check Seller Discount
            seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=seller.id, today_date=today_date)
            seller = {
                'id': seller.id,
                'name': seller.name,
                'city': seller.city,
                'region': seller.region,
                'discount': str(seller_discount['rate']),
                'amount': str(seller_discount['amount'])
            }

            # Check Total Wishlist
            check_total_wishlist = db.query(FavouriteModel).filter(
                FavouriteModel.product_id == data.id).count()
            total_wishlist = check_total_wishlist + 15

            # Customize Product Data
            product = {
                'id': data.id,
                'title': data.title,
                'images': imagedata,
                'slug': data.slug,
                'short_description': data.short_description,
                'category': data.category,
                'pricing': pricingdata,
                'created_at': data.created_at.strftime("%Y-%m-%d"),
                'stock': stock,
                'favourite': favourite,
                'assured': True,
                'status': data.status,
                'delivery': productstatic_text['data_delivery'],
                'payment': productstatic_text['data_payment'],
                'return_days': productstatic_text['data_return_days'],
                'discount': productstatic_text['data_discount'],
                'seller': seller,
                'total_wishlist': str(total_wishlist)

            }

            # Specifications
            specification: ProductAttributeModel = await ProductsHelper.getProductAttributes(product_id=data.id, db=db)

            specification_data = []
            for spec in specification:
                if(spec['values'] != ''):
                    specification_data.append(spec)

            # Seller Products
            sellerproductsdata = await ProductsHelper.getSellerWiseProducts(db=db, product_id=data.id, user_id=data.userid)
            sellerProducts = []
            for sellerproductdata in sellerproductsdata:
                # Check Product Discount
                check_product_discount = sellerproductdata.product_discount.filter(
                    func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
                product_discount = {
                    'rate': '0.00',
                    'product_sale_price': '0.00',
                    'discount_amount': '0.00',
                }

                # Check Pricing
                sellerpricing = sellerproductdata.product_pricing[0]
                sellerpricingdata = {
                    'id': 0,
                    'price': '0.00',
                    'product_discount': product_discount,
                    'discount': '0.00',
                    'discount_price': '0.00',
                    'moq': 0,
                    'unit': ''
                }

                if(sellerpricing):
                    pp: ProductModel = await ProductsHelper.getPrice(db, sellerproductdata, sellerpricing, asez_service=check_asez_service, app_version='V4')
                    # Get Product Sale Price
                    product_sale_price = productPricecalculation(price=sellerpricing.price, tax=sellerpricing.tax, commission=check_asez_service.rate,
                                                                 gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                    # Check Discounted Price
                    discount_price = float(0.00)
                    checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                    if(checkdiscount != 0):
                        discount_price = await calculatediscountprice(price=float(pp), discount=float(checkdiscount))

                    if(check_product_discount is not None):
                        product_discount = {
                            'rate': floatingValue(check_product_discount.discount),
                            'product_sale_price': floatingValue(product_sale_price),
                            'discount_amount': floatingValue(float(product_sale_price) - float(pp)),

                        }

                    sellerpricingdata = {
                        'id': sellerpricing.id,
                        'price': floatingValue(pp),
                        'product_discount': product_discount,
                        'discount': str(checkdiscount),
                        'discount_price': floatingValue(discount_price),
                        'moq': sellerpricing.moq,
                        'unit': sellerpricing.unit
                    }

                # Product Image
                simg = ''
                simage = sellerproductdata.images[0]
                if(simage is not None):
                    simg = simage.file_path

                # New Changes (TINA)
                sproductis_new_arrival = await ProductsHelper.newarrival(sellerproductdata)

                # Check Best Selling and Treding Product # NEW Change (TINA)
                sproductis_best_selling = await ProductsHelper.bestsellingproducts(db, sellerproductdata)

                sproductis_trending = await ProductsHelper.trendingproducts(db, sellerproductdata)

                # Product Favourite
                sfavourite = True

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=sellerproductdata.userid, today_date=today_date)

                sellerpr = {
                    'id': sellerproductdata.id,
                    'title': sellerproductdata.title,
                    'image': simg,
                    'short_description': sellerproductdata.short_description,
                    'category': sellerproductdata.category,
                    'pricing': sellerpricingdata,
                    'favourite': sfavourite,
                    'is_new_arrival': sproductis_new_arrival,
                    'is_best_selling': sproductis_best_selling,
                    'is_trending': sproductis_trending,
                    'created_at': sellerproductdata.created_at.strftime("%Y-%m-%d"),
                    'status': sellerproductdata.status,
                    'seller_discount': check_seller_discount['rate']
                }
                sellerProducts.append(sellerpr)

            # Similar Products
            similardata = await ProductsHelper.getSimilarProducts(product_id=data.id, category_id=data.category, db=db)

            similarproducts = similardata.limit(
                limit=10).all()

            similarproductdata = []
            for similarproduct in similarproducts:
                # Check Product Discount
                check_product_discount = similarproduct.product_discount.filter(
                    func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
                product_discount = {
                    'rate': '0.00',
                    'product_sale_price': '0.00',
                    'discount_amount': '0.00',
                }

                # Check Pricing
                similarpricing = similarproduct.product_pricing[0]

                similarpricingdata = {
                    'id': 0,
                    'price': '0.00',
                    'product_discount': product_discount,
                    'discount': '0.00',
                    'discount_price': '0.00',
                    'moq': 0,
                    'unit': ''
                }

                if(similarpricing):
                    spp: ProductModel = await ProductsHelper.getPrice(db, similarproduct, similarpricing, asez_service=check_asez_service, app_version='V4')
                    # Get Product Sale Price
                    product_sale_price = productPricecalculation(price=similarpricing.price, tax=similarpricing.tax, commission=check_asez_service.rate,
                                                                 gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                    # Check Discounted Price
                    discount_price = float(0.00)
                    checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                    if(checkdiscount != 0):
                        discount_price = await calculatediscountprice(price=float(spp), discount=float(checkdiscount))

                    if(check_product_discount is not None):
                        product_discount = {
                            'rate': floatingValue(check_product_discount.discount),
                            'product_sale_price': floatingValue(product_sale_price),
                            'discount_amount': floatingValue(float(product_sale_price) - float(spp)),

                        }
                    similarpricingdata = {
                        'id': similarpricing.id,
                        'price': floatingValue(spp),
                        'product_discount': product_discount,
                        'discount': str(checkdiscount),
                        'discount_price': floatingValue(discount_price),
                        'moq': similarpricing.moq,
                        'unit': similarpricing.unit
                    }

                # Product Image
                similarimg = ''
                similarimage = similarproduct.images[0]
                if(similarimage is not None):
                    similarimg = similarimage.file_path

                # New Changes (TINA)
                ssproductis_new_arrival = await ProductsHelper.newarrival(similarproduct)

                # Check Best Selling and Treding Product # NEW Change (TINA)
                ssproductis_best_selling = await ProductsHelper.bestsellingproducts(db, similarproduct)

                ssproductis_trending = await ProductsHelper.trendingproducts(db, similarproduct)

                # Product Favourite
                similarfavourite = False
                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=similarproduct.userid, today_date=today_date)

                similarpr = {
                    'id': similarproduct.id,
                    'title': similarproduct.title,
                    'image': similarimg,
                    'short_description': similarproduct.short_description,
                    'category': similarproduct.category,
                    'pricing': similarpricingdata,
                    'favourite': similarfavourite,
                    'is_new_arrival': ssproductis_new_arrival,
                    'is_best_selling': ssproductis_best_selling,
                    'is_trending': ssproductis_trending,
                    'created_at': similarproduct.created_at.strftime("%Y-%m-%d"),
                    'status': similarproduct.status,
                    'seller_discount': check_seller_discount['rate']
                }
                similarproductdata.append(similarpr)
            if(check_user.status == 1):
                is_registered = True
            else:
                is_registered = False
            return {"status_code": HTTP_202_ACCEPTED, "is_registered": is_registered, "product": product, "purchase_limit_text": purchase_limit_text, "specification": specification_data, "seller_products": sellerProducts, "similarproducts": similarproductdata}
    except Exception as e:
        print(e)

# response_model=ProductDetail,


@v4_product_router.get("/similar/listing/{product_id}", response_model=ProductData,  dependencies=[Depends(JWTBearer())])
async def get_product_detail(request: Request, product_id: int, db: Session = Depends(get_db), page: int = 1, limit: int = 15):

    userdata = auth(request=request)
    # Check User
    check_user = db.query(UserModel).filter(
        UserModel.id == userdata['id']).first()
    try:

        data: ProductModel = await ProductsHelper.get_product(
            db=db, product_id=product_id)

        similardata: ProductModel = await ProductsHelper.getSimilarProducts(product_id=data.id, category_id=data.category, db=db)
        product_count = similardata.count()
        if(product_count == 0):
            return {"status_code": HTTP_404_NOT_FOUND}
        else:

            similarproducts: ProductModel = similardata.limit(
                limit=limit).offset((page - 1) * limit).all()

            # Aseztak Service
            today = date.today()
            today_date = today.strftime('%Y-%m-%d')
            check_asez_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

            for product in similarproducts:
                # CHECK PRODUCT DISCOUNT
                check_product_discount = product.product_discount.filter(
                    func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
                product_discount = {
                    'rate': '0.00',
                    'product_sale_price': '0.00',
                    'discount_amount': '0.00',
                }

                # Check Pricing
                pricing = product.product_pricing[0]

                if(pricing):
                    product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=check_asez_service, app_version='V4')

                    # Get Product Sale Price
                    product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=check_asez_service.rate,
                                                                 gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                    # Check ONLINE Discounted Price
                    discount_price = float(0.00)
                    checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                    if(checkdiscount != 0):
                        discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                    # Check Product Discount
                    if(check_product_discount is not None):
                        product_discount = {
                            'rate': floatingValue(check_product_discount.discount),
                            'product_sale_price': floatingValue(product_sale_price),
                            'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                        }
                        # Restructure Price data
                    price_data = {
                        'id': pricing.id,
                        'price': floatingValue(product_price),
                        'product_discount': product_discount,
                        'discount': str(checkdiscount),
                        'discount_price': floatingValue(discount_price),
                        'moq': pricing.moq,
                        'unit': pricing.unit
                    }
                    product.pricing = price_data

                # New Changes (TINA)
                start_time = date.today() - timedelta(days=14)
                start_date = start_time.strftime("%Y-%m-%d")
                product_date = product.created_at.strftime("%Y-%m-%d")
                is_new_arrival = False
                if (product_date >= start_date):
                    is_new_arrival = True

                product.is_new_arrival = is_new_arrival

                # Check Best Selling and Treding Product
                check_best_selling = db.query(TagModel).join(ProductTagModel, ProductTagModel.tag_id == TagModel.id).filter(
                    ProductTagModel.product_id == product.id).filter(TagModel.name == 'Best Selling').first()

                if (check_best_selling is not None):
                    product.is_best_selling = True

                check_trending = db.query(TagModel).join(ProductTagModel, ProductTagModel.tag_id == TagModel.id).filter(
                    ProductTagModel.product_id == product.id).filter(TagModel.name == 'Trending').first()

                if (check_trending is not None):
                    product.is_trending = True
                # New changes (TINA)
                 # Favourite
                product.favourite = False

                # Product Image
                productimage = product.images[0]
                product.image = ''
                if(productimage):
                    product.image = productimage.file_path

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=product.userid, today_date=today_date)
                product.seller_discount = check_seller_discount['rate']

            total_records = product_count

            # Number of Total Pages
            total_pages = str(round((total_records/limit), 2))

            total_pages = total_pages.split('.')

            if(total_pages[1] != "0"):
                total_pages = int(total_pages[0]) + 1
            else:
                total_pages = int(total_pages[0])

            if(check_user.status == 1):
                is_registered = True
            else:
                is_registered = False
            return {"status_code": HTTP_200_OK, "is_registered": is_registered, "title": 'Similar Products',  "products": similarproducts, "total_products": total_records, "current_page": page, "total_pages": total_pages}
    except Exception as e:
        print(e)


@v4_product_router.get("/seller-wise-products/{seller_id}", response_model=ProductDataSellerWiseSchema, dependencies=[Depends(JWTBearer())])
async def sellerWiseProducts(request: Request, seller_id: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:
        userdata = auth(request=request)
        # Check User
        check_user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        data: ProductModel = await ProductsHelper.sellerWiseProducts(db=db, seller_id=seller_id)
        # Total Records
        total_records = data.count()

        # Unique Products
        unique_products = db.query(ProductModel).filter(
            ProductModel.userid == seller_id).filter(ProductModel.status == 51).count()

        # Seller
        db_seller = db.query(UserModel).filter(
            UserModel.id == seller_id).first()

        seller_created_at = db_seller.created_at.strftime("%Y")

        # Today Date
        today = date.today()
        today_date = today.strftime('%Y-%m-%d')

        # Check Brands products
        checkBrandsproductsexist = db.execute("SELECT COUNT(*) as total_products FROM products LEFT JOIN brands ON brands.id = products.brand_id WHERE brands.user_id =:user_id AND products.status = 51",{
            "user_id": seller_id
        }).first()
        brands = []
        if(checkBrandsproductsexist.total_products != 0):
            checkBrands = db.query(BrandsModel).filter(
                BrandsModel.user_id == seller_id).filter(BrandsModel.status == 1).all()
            if(len(checkBrands) > 0):
                for brand in checkBrands:
                    b = {
                        'id': brand.id,
                        'name': brand.name,
                        'logo': brand.logo
                    }
                    brands.append(b)
        # Check Seller Purchase Limit
        check_seller_purchase_limit = await ProductsHelper.check_seller_facility(db=db, user_id=seller_id, type='purchase_limit')
        purchase_limit_text = ''
        if(check_seller_purchase_limit is not None):
            purchase_limit_text = 'Min. purchase limit ₹' + \
                str(check_seller_purchase_limit.key_value)
         # Check Seller Discount
        check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=db_seller.id, today_date=today_date)

        seller_data = {
            'id': db_seller.id,
            'name': db_seller.name,
            'address': str(db_seller.city)+','+' '+str(db_seller.region),
            'brands': brands,
            'level': 'Gold',
            'rating': '4.5',
            'since_year': 'Selling since '+str(seller_created_at),
            'rated_by_customer': 'Rated by 506 Customer',
            'no_of_categories': str(total_records)+' Various Categories',
            'no_of_products': str(unique_products)+' Unique Products',
            "purchase_limit_text": purchase_limit_text,
            'discount_rate': check_seller_discount['rate'],
            'discount_amount': check_seller_discount['amount']

        }

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != "0"):
            total_pages = int(total_pages[0]) + 1
        else:
            total_pages = int(total_pages[0])

        if(total_records == 0):
            return {"status_code": HTTP_200_OK, "is_registered": True, "products": [], "current_page": page, "total_pages": 0}

        productdata: ProductModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        # Aseztak Service
        check_asez_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

        productsdata = []
        for product in productdata:
            products = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(ProductModel.status == 51).filter(
                ProductModel.userid == seller_id).filter(ProductModel.category == product.category).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(ProductModel.updated_at.desc())
            category = db.query(CategoriesModel).filter(
                CategoriesModel.id == product.category).first()
            if(category.image_path is not None):
                img = category.image_path
            else:
                img = ''
            category = {
                'id': category.id,
                'title': category.name,
                'image': img
            }
            prodata = []
            for pro in products.limit(4).all():
                filename = pro.images[0]
                img = filename.file_path

                # Check Product Discount
                check_product_discount = pro.product_discount.filter(
                    func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
                product_discount = {
                    'rate': '0.00',
                    'product_sale_price': '0.00',
                    'discount_amount': '0.00',
                }
                # Pricing
                pricing = pro.product_pricing[0]

                product_price: ProductModel = await ProductsHelper.getPrice(db, pro, pricing, asez_service=check_asez_service, app_version='V4')

                # Get Product Sale Price
                product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=check_asez_service.rate,
                                                             gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                # Check Discounted Price
                discount_price = float(0.00)
                checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                if(checkdiscount != 0):
                    discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))

                if(check_product_discount is not None):
                    product_discount = {
                        'rate': floatingValue(check_product_discount.discount),
                        'product_sale_price': floatingValue(product_sale_price),
                        'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),

                    }
                if(pricing):
                    pricing = {
                        'id': pricing.id,
                        'price': floatingValue(product_price),
                        'product_discount': product_discount,
                        'discount': str(checkdiscount),
                        'discount_price': floatingValue(discount_price),
                        'unit':  pricing.unit,
                        'moq': pricing.moq
                    }
                p = {
                    'id':  pro.id,
                    'title': pro.title,
                    'image': img,
                    'pricing': pricing
                }
                prodata.append(p)
            pdata = {
                'total_products': len(products.all()),
                'category': category,
                'products': prodata
            }
            productsdata.append(pdata)
        if(check_user.status == 1):
            is_registered = True
        else:
            is_registered = False
        return {"status_code": HTTP_200_OK, "is_registered": is_registered, "seller": seller_data, "items": productsdata, "current_page": page, "total_pages": round(total_pages, 0)}
    except Exception as e:
        print(e)
        return {"status_code": HTTP_200_OK, "products": [], "current_page": page, "total_pages": 0}


@v4_product_router.get("/seller-wise/view-all/{category_id}/{seller_id}", response_model=ProductData, dependencies=[Depends(JWTBearer())])
async def sellerWiseProductsViewAll(request: Request, category_id: int, seller_id: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        userdata = auth(request=request)
        # Check User
        check_user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).join(ProductCategories, ProductCategories.product_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(UserModel.status == 1).filter(ProductPricingModel.deleted_at.is_(None)).filter(
            ProductCategories.category_id == category_id).filter(ProductModel.userid == seller_id).filter(ProductModel.status == 51).group_by(ProductModel.id).order_by(ProductModel.updated_at.desc())

        # Category
        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == category_id).first()

        if(products.count() == 0):
            return {"status_code": HTTP_200_OK, "is_registered": True, "products": [], "total_products": 0, "current_page": page, "total_pages": 0}

        total_records = products.count()

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != "0"):
            total_pages = int(total_pages[0]) + 1
        else:
            total_pages = int(total_pages[0])

        productdata: ProductModel = products.limit(
            limit=limit).offset((page - 1) * limit).all()

        # calculating Price
        # Aseztak Service
        today = date.today()
        today_date = today.strftime('%Y-%m-%d')
        check_asez_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

        for product in productdata:
            # CHECK PRODUCT DISCOUNT
            check_product_discount = product.product_discount.filter(
                func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
            product_discount = {
                'rate': '0.00',
                'product_sale_price': '0.00',
                'discount_amount': '0.00',
            }

            # Check Pricing
            pricing = product.product_pricing[0]
            if(pricing):
                product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=check_asez_service, app_version='V4')
                # Get Product Sale Price
                product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=check_asez_service.rate,
                                                             gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                # Check ONLINE Discounted Price
                discount_price = float(0.00)
                checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                if(checkdiscount != 0):
                    discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                # Check Product Discount
                if(check_product_discount is not None):
                    product_discount = {
                        'rate': floatingValue(check_product_discount.discount),
                        'product_sale_price': floatingValue(product_sale_price),
                        'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                    }
                # Restructure Price data
                price_data = {
                    'id': pricing.id,
                    'price': floatingValue(product_price),
                    'product_discount': product_discount,
                    'discount': str(checkdiscount),
                    'discount_price': floatingValue(discount_price),
                    'moq': pricing.moq,
                    'unit': pricing.unit
                }
                product.pricing = price_data

             # Favourite
            product.favourite = False

            # Product Image
            productimage = product.images[0]

            product.image = ''
            if(productimage):
                product.image = productimage.file_path

            # Check Seller Discount
            check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=product.userid, today_date=today_date)
            product.seller_discount = check_seller_discount['rate']

        if(check_user.status == 1):
            is_registered = True
        else:
            is_registered = False
        return {"status_code": HTTP_200_OK, "is_registered": is_registered, "title": category.name, "products": productdata, "total_products": total_records, "current_page": page, "total_pages": total_pages}

    except Exception as e:
        print(e)

# Updated rahul


@v4_product_router.get("/list-data/{section}/{param}/{sorting}", response_model=ProductDataTabWise, dependencies=[Depends(JWTBearer())])
async def getAllProducts(request: Request, section: str, param: str, sorting: str, db: Session = Depends(get_db), page: int = 1, limit: int = 15):
    userdata = auth(request=request)
    # Check User
    check_user = db.query(UserModel).filter(
        UserModel.id == userdata['id']).first()

    # Title
    if(param.lower() == 'all'):
        title = 'All'
    else:
        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == param).first()
        title = category.name

    try:

        if (section.lower() == 'footwear' and param.lower() == 'all'):

            category_data = db.query(CategoriesModel).filter(
                CategoriesModel.name == 'Footwear').first()
            # Get Min and Max Price
            # productPrice: ProductModel = await ProductsHelper.productMinMaxPirce(db=db, category_id=category_data.id)

            # # Customization min price
            # customMinPrice = str(productPrice.min_price)
            # customMinPrice = customMinPrice.split(".")
            # customMinPrice = int(customMinPrice[0])

            # min_price = round(customMinPrice)
            # max_price = round(productPrice.max_price)
            min_price = '0'
            max_price = '0'

            data: ProductModel = await ProductsHelper.get_products_category_wise_tab_V4(db=db, categoryId=category_data.id, sort=sorting.lower(), min_price=min_price, max_price=max_price)

        elif (section.lower() == 'garments' and param.lower() == 'all'):
            category_data = db.query(CategoriesModel).filter(
                CategoriesModel.name == 'Garments').first()
            # Get Min and Max Price
            # productPrice: ProductModel = await ProductsHelper.productMinMaxPirceGarments(db=db, category_id=category_data.id)

            # # Customization min price
            # customMinPrice = str(productPrice.min_price)
            # customMinPrice = customMinPrice.split(".")
            # customMinPrice = int(customMinPrice[0])

            # min_price = round(customMinPrice)
            # max_price = round(productPrice.max_price)

            min_price = '0'
            max_price = '0'

            data: ProductModel = await ProductsHelper.get_products_category_wise_tab_V4_garments(db=db, categoryId=category_data.id, sort=sorting.lower(), min_price=min_price, max_price=max_price)

        elif (section.lower() == 'all' and param.lower() == 'all'):

            # # Get Min and Max Price
            # productPrice: ProductModel = await ProductsHelper.productMinMaxPirceAll(db=db)

            # # Customization min price
            # customMinPrice = str(productPrice.min_price)
            # customMinPrice = customMinPrice.split(".")
            # customMinPrice = int(customMinPrice[0])

            # min_price = round(customMinPrice)
            # max_price = round(productPrice.max_price)
            min_price = '0'
            max_price = '0'

            data: ProductModel = ProductsHelper.get_products_all_tab_v4(
                db=db, sort=sorting.lower(), min_price=min_price, max_price=max_price)

        else:

            # # Get Min and Max Price
            # productPrice: ProductModel = await ProductsHelper.productMinMaxPirce(db=db, category_id=param.lower())

            # # Customization min price
            # customMinPrice = str(productPrice.min_price)
            # customMinPrice = customMinPrice.split(".")
            # customMinPrice = int(customMinPrice[0])

            # min_price = round(customMinPrice)
            # max_price = round(productPrice.max_price)
            min_price = '0'
            max_price = '0'

            data: ProductModel = await ProductsHelper.get_products_category_wise_tab_V4(db=db, categoryId=param.lower(), sort=sorting, min_price=min_price, max_price=max_price)

        product_count = data.count()
        if(product_count == 0):
            return {"status_code": HTTP_200_OK, "is_registered": True, "type": param, "products": [], "total_products": total_records, "current_page": page, "total_pages": 0}
        else:
            limit = 20
            productdata: ProductModel = data.filter(ProductModel.status == 51).limit(
                limit=limit).offset((page - 1) * limit).all()

            # calculating Price
            # Aseztak Service
            today = date.today()
            today_date = today.strftime('%Y-%m-%d')
            # Aseztak Service
            check_asez_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

            for product in productdata:
                # CHECK PRODUCT DISCOUNT
                check_product_discount = product.product_discount.filter(
                    func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
                product_discount = {
                    'rate': '0.00',
                    'product_sale_price': '0.00',
                    'discount_amount': '0.00',
                }

                # Check Pricing
                pricing = product.product_pricing[0]
                if(pricing):

                    product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=check_asez_service, app_version='V4')

                    # Get Product Sale Price
                    product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=check_asez_service.rate,
                                                                 gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                    # Check ONLINE Discounted Price
                    discount_price = float(0.00)
                    checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                    if(checkdiscount != 0):
                        discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                    # Check Product Discount
                    if(check_product_discount is not None):
                        product_discount = {
                            'rate': floatingValue(check_product_discount.discount),
                            'product_sale_price': floatingValue(product_sale_price),
                            'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                        }
                    # Restructure Price data
                    price_data = {
                        'id': pricing.id,
                        'price': floatingValue(product_price),
                        'product_discount': product_discount,
                        'discount': str(checkdiscount),
                        'discount_price': floatingValue(discount_price),
                        'moq': pricing.moq,
                        'unit': pricing.unit
                    }
                    product.pricing = price_data

                 # New Changes (TINA)
                product.is_new_arrival = await ProductsHelper.newarrival(product)

                # Check Best Selling and Treding Product
                product.is_best_selling = await ProductsHelper.bestsellingproducts(db, product)

                product.is_trending = await ProductsHelper.trendingproducts(db, product)

                # Favourite
                product.favourite = False

                # Product Image
                productimage = product.images[0]

                product.image = ''
                if(productimage):
                    product.image = productimage.file_path

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=product.userid, today_date=today_date)
                product.seller_discount = check_seller_discount['rate']

            # Total Records
            total_records = product_count

            # Number of Total Pages
            total_pages = str(round((total_records/limit), 2))

            total_pages = total_pages.split('.')

            if(total_pages[1] != "0"):
                total_pages = int(total_pages[0]) + 1
            else:
                total_pages = int(total_pages[0])
            if(check_user.status == 1):
                is_registered = True
            else:
                is_registered = False
            return {"status_code": HTTP_200_OK, "is_registered": is_registered, "type": param, "title": title, "products": productdata, "total_products": total_records, "current_page": page, "total_pages": round(total_pages, 0)}
    except Exception as e:
        print(e)


# Filter Sorting
@v4_product_router.get("/filter-sorting", dependencies=[Depends(JWTBearer())])
async def filterAttributes():
    try:
        sorting = [{
            'name': 'Popularity',
            'value': 'all'
        },
            {
            'name': 'Price -- Low to High',
            'value': 'asc'
        },
            {
            'name': 'Price -- High to Low',
            'value': 'desc'
        }]
        return {"status_code": HTTP_200_OK, "sorting_data": sorting}

    except Exception as e:
        print(e)


# Filter Attributes
@v4_product_router.get("/filter-attributes/{category_id}", response_model=filterAttributesV4Schema, dependencies=[Depends(JWTBearer())])
async def filterAttributes(category_id: int, db: Session = Depends(get_db)):
    try:

        data: ProductModel = await ProductsHelper.filterAttributes(db=db, category_id=category_id)

        if(data == False):
            return {"status_code": HTTP_404_NOT_FOUND}

        return {"status_code": HTTP_202_ACCEPTED, "filter_attributes": data}

    except Exception as e:
        print(e)


@v4_product_router.post("/v4/recently-viewed/products", response_model=ProductData, dependencies=[Depends(JWTBearer())])
async def RecentlyViewedProducts(request: Request, data: RecentlyViewedProductSchema, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    userdata = auth(request=request)
    try:
        productdata = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).filter(
            ProductModel.id.in_(data.id)).filter(ProductModel.status == 51).all()

        # Aseztak Service
        today = date.today()
        today_date = today.strftime('%Y-%m-%d')
        # Aseztak Service
        check_asez_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

        for product in productdata:
            # CHECK PRODUCT DISCOUNT
            check_product_discount = product.product_discount.filter(
                func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
            product_discount = {
                'rate': '0.00',
                'product_sale_price': '0.00',
                'discount_amount': '0.00',
            }
            # Check Pricing
            pricing = product.product_pricing[0]

            if(pricing):
                product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=check_asez_service, app_version='V4')

                # Get Product Sale Price
                product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=check_asez_service.rate,
                                                             gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                # Check ONLINE Discounted Price
                discount_price = float(0.00)
                checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                if(checkdiscount != 0):
                    discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                # Check Product Discount
                if(check_product_discount is not None):
                    product_discount = {
                        'rate': floatingValue(check_product_discount.discount),
                        'product_sale_price': floatingValue(product_sale_price),
                        'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                    }
                # Restructure Price data
                price_data = {
                    'id': pricing.id,
                    'price': floatingValue(product_price),
                    'product_discount': product_discount,
                    'discount': str(checkdiscount),
                    'discount_price': floatingValue(discount_price),
                    'moq': pricing.moq,
                    'unit': pricing.unit
                }

                product.pricing = price_data

             # New Changes (TINA)
            product.is_new_arrival = await ProductsHelper.newarrival(product)

            # Check Best Selling and Treding Product
            product.is_best_selling = await ProductsHelper.bestsellingproducts(db, product)

            product.is_trending = await ProductsHelper.trendingproducts(db, product)

            # Check product favourite
            product.favourite = False

            # Product Image
            productimage = product.images[0]

            product.image = ''
            if(productimage):
                product.image = productimage.file_path

        return {"status_code": HTTP_202_ACCEPTED, "title": "Recently Viewed Products", "products": productdata, "total_products": len(productdata), "current_page": page, "total_pages": 0}
    except Exception as e:
        print(e)


@v4_product_router.post('/list/category-wise', response_model=ProductData, dependencies=[Depends(JWTBearer())])
async def get_products_category_wise(request: Request, filterdata: FilterDataSchema, db: Session = Depends(get_db)):
    userdata = auth(request=request)
    # Check User
    check_user = db.query(UserModel).filter(
        UserModel.id == userdata['id']).first()
    try:

        page = filterdata.page
        limit = filterdata.limit
        # Category
        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == filterdata.id).first()

        if(len(filterdata.attributes) == 0 and filterdata.min_price == '0' and filterdata.max_price == '0' and filterdata.sort == 'all'):
            data: ProductModel = ProductsHelper.get_products_category_wise(
                db=db, categoryId=category.id)

        else:
            if(filterdata.min_price == '0' and filterdata.max_price == '0'):

                # # Get Min and Max Price
                # productPrice: ProductModel = await ProductsHelper.productMinMaxPirce(db=db, category_id=filterdata.id)

                # # Customization min price
                # customMinPrice = str(productPrice.min_price)
                # customMinPrice = customMinPrice.split(".")
                # customMinPrice = int(customMinPrice[0])

                # filterdata.min_price = round(customMinPrice)
                # filterdata.max_price = round(productPrice.max_price)

                filterdata.min_price = '0'
                filterdata.max_price = '0'

            data: ProductModel = await ProductsHelper.filerSearchProducts(db=db, category_id=filterdata.id, sort=filterdata.sort, min_price=filterdata.min_price, max_price=filterdata.max_price, attributes=filterdata.attributes)

        product_count = data.count()

        if(product_count == 0):
            return {"status_code": HTTP_404_NOT_FOUND}

        else:

            productdata: ProductModel = data.limit(
                limit=limit).offset((page - 1) * limit).all()

            # calculating Price
            today = date.today()
            today_date = today.strftime('%Y-%m-%d')
            # Aseztak Service
            check_asez_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

            for product in productdata:
                # CHECK PRODUCT DISCOUNT
                check_product_discount = product.product_discount.filter(
                    func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
                product_discount = {
                    'rate': '0.00',
                    'product_sale_price': '0.00',
                    'discount_amount': '0.00',
                }

                # Check Pricing
                pricing = product.product_pricing[0]
                if(pricing):

                    product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=check_asez_service, app_version='V4')

                    # Get Product Sale Price
                    product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=check_asez_service.rate,
                                                                 gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                    # Check ONLINE Discounted Price
                    discount_price = float(0.00)
                    checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                    if(checkdiscount != 0):
                        discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                    # Check Product Discount
                    if(check_product_discount is not None):
                        product_discount = {
                            'rate': floatingValue(check_product_discount.discount),
                            'product_sale_price': floatingValue(product_sale_price),
                            'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                        }
                    # Restructure Price data
                    price_data = {
                        'id': pricing.id,
                        'price': floatingValue(product_price),
                        'product_discount': product_discount,
                        'discount': str(checkdiscount),
                        'discount_price': floatingValue(discount_price),
                        'moq': pricing.moq,
                        'unit': pricing.unit
                    }
                    product.pricing = price_data

                 # New Changes (TINA)
                product.is_new_arrival = await ProductsHelper.newarrival(product)

                # Check Best Selling and Treding Product
                product.is_best_selling = await ProductsHelper.bestsellingproducts(db, product)

                product.is_trending = await ProductsHelper.trendingproducts(db, product)
                # Favourite
                product.favourite = False

                # Product Image
                productimage = product.images[0]

                product.image = ''
                if(productimage):
                    product.image = productimage.file_path

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=product.userid, today_date=today_date)
                product.seller_discount = check_seller_discount['rate']

            # Total Records
            total_records = product_count

            total_pages = str(round((total_records/limit), 2))

            total_pages = total_pages.split('.')

            if(total_pages[1] != "0"):
                total_pages = int(total_pages[0]) + 1
            else:
                total_pages = int(total_pages[0])
            if(check_user.status == 1):
                is_registered = True
            else:
                is_registered = False
            return {"status_code": HTTP_202_ACCEPTED, "is_registered": is_registered, "title": category.name, "products": productdata, "total_products": total_records, "current_page": page, "total_pages": round(total_pages, 0)}
    except Exception as e:
        print(e)


@v4_product_router.get("/get/attributes/{product_id}", dependencies=[Depends(JWTBearer())])
async def GetAttributes(product_id: int, db: Session = Depends(get_db)):
    try:

        products = db.query(ProductModel).filter(
            ProductModel.id == product_id).first()
        category = db.query(CategoriesModel).filter(
            CategoriesModel.id == products.category).first()

        attributes = db.query(AttriFamilyValuesModel, AttributeModel).join(AttributeModel, AttributeModel.id == AttriFamilyValuesModel.attribute_id).options(
            joinedload(AttributeModel.product_attributes)

        ).filter(AttriFamilyValuesModel.family_id == category.family_id).group_by(AttributeModel.id).all()

        attribute_data = []

        for attribute in attributes:

            if(attribute.AttributeModel.is_required == 0):
                attribute.AttributeModel.is_required = False
            else:
                attribute.AttributeModel.is_required = True

            # Attribute Values
            attribute_values = attribute.AttributeModel.product_attributes
            attribute_values_data = []

            selected_values = []

            if(attribute.AttributeModel.frontend_type != 'text'):
                for attrvalues in attribute_values:
                    checkProductAttribute = db.query(ProductAttributeModel).filter(
                        ProductAttributeModel.product_id == product_id).filter(ProductAttributeModel.attribute_value_id == attrvalues.id).first()
                    selected = False
                    if(checkProductAttribute is not None):
                        selected = True
                        selected_values.append(str(
                            checkProductAttribute.attribute_value_id))

                    attribute_value = {
                        'selected': selected,
                        'code': attribute.AttributeModel.code,
                        'id': str(attrvalues.id),
                        'value': attrvalues.value,
                    }
                    attribute_values_data.append(attribute_value)

            else:
                checkProductTextAttribute = db.query(ProductAttributeModel).filter(
                    ProductAttributeModel.product_id == product_id).filter(ProductAttributeModel.attribute_id == attribute.AttributeModel.id).first()

                if(checkProductTextAttribute):

                    selected_values.append(str(
                        checkProductTextAttribute.attribute_value))
            # Attribute

            attrdata = {
                'is_required': attribute.AttributeModel.is_required,
                'code': attribute.AttributeModel.code,
                'name': attribute.AttributeModel.name,
                'type': attribute.AttributeModel.frontend_type,
                'selected_values': selected_values,
                'values': attribute_values_data
            }

            attribute_data.append(attrdata)

        return {"status_code": HTTP_200_OK,  "attributes": attribute_data}

    except Exception as e:
        print(e)


# Product Search New Changes (TINA)
@v4_product_router.get("/search", response_model=ProductSearchList, dependencies=[Depends(JWTBearer())])
async def ProductSearch(search: str, db: Session = Depends(get_db)):
    try:

        if (search == 'search'):
            data = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).order_by(
                ProductModel.id.desc()).limit(10).all()
        else:
            search = search.replace("+", "%")
            search = search.rstrip()
            search = "%{}%".format(search)
            data = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.title.ilike(
                search)).filter(ProductModel.status == 51).limit(15).all()
            product_list = []
        if (len(data) == 0):
            return {"products": []}
        else:
            product_list = []
            for product in data:
                product_data = {
                    'id': product.id,
                    'name': product.title,
                    'slug': product.slug,
                    'status': product.status
                }
                product_list.append(product_data)
        return {"products": data}
    except Exception as e:
        print(e)


@v4_product_router.post("/search-result", response_model=ProductData, dependencies=[Depends(JWTBearer())])
async def ProductsSearch(request: Request, search: ProductSearchResult, db: Session = Depends(get_db)):
    page = search.page
    limit = search.limit
    userdata = auth(request=request)
    try:
        search = search.title.rstrip()
        search = "%{}%".format(search)
        data = db.query(ProductModel).filter(ProductModel.title.like(search))
        product_count = data.count()
        if (product_count == 0):
            return {"status_code": HTTP_404_NOT_FOUND}
        else:
            productdata = data.limit(
                limit=limit).offset((page - 1) * limit).all()
            # calculating Price
            # Aseztak Service
            today = date.today()
            today_date = today.strftime('%Y-%m-%d')
            # Aseztak Service
            check_asez_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

            for product in productdata:
                # CHECK PRODUCT DISCOUNT
                check_product_discount = product.product_discount.filter(
                    func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
                product_discount = {
                    'rate': '0.00',
                    'product_sale_price': '0.00',
                    'discount_amount': '0.00',
                }
                # Check Pricing
                pricing = product.product_pricing[0]
                if (pricing):
                    product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=check_asez_service, app_version='V4')

                    # Get Product Sale Price
                    product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=check_asez_service.rate,
                                                                 gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                    # Check ONLINE Discounted Price
                    discount_price = float(0.00)
                    checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                    if(checkdiscount != 0):
                        discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                    # Check Product Discount
                    if(check_product_discount is not None):
                        product_discount = {
                            'rate': floatingValue(check_product_discount.discount),
                            'product_sale_price': floatingValue(product_sale_price),
                            'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                        }

                    # Restructure Price data
                    price_data = {
                        'id': pricing.id,
                        'price': floatingValue(product_price),
                        'product_discount': product_discount,
                        'discount': str(checkdiscount),
                        'discount_price': floatingValue(discount_price),
                        'moq': pricing.moq,
                        'unit': pricing.unit
                    }
                    product.pricing = price_data

                 # New Changes (TINA)
                product.is_new_arrival = await ProductsHelper.newarrival(product)

                # Check Best Selling and Treding Product
                product.is_best_selling = await ProductsHelper.bestsellingproducts(db, product)
                product.is_trending = await ProductsHelper.trendingproducts(db, product)

                # Favourite
                product.favourite = False
                # Product Image
                productimage = product.images[0]
                product.image = ''
                if (productimage):
                    product.image = productimage.file_path

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=product.userid, today_date=today_date)
                product.seller_discount = check_seller_discount['rate']

            # Total Records
            total_records = product_count
            total_pages = str(round((total_records/limit), 2))
            total_pages = total_pages.split('.')
            if (total_pages[1] != "0"):
                total_pages = int(total_pages[0]) + 1
            else:
                total_pages = int(total_pages[0])
        return {"status_code": HTTP_202_ACCEPTED, "title": product.title, "products": productdata, "total_products": total_records, "current_page": page, "total_pages": round(total_pages, 0)}
    except Exception as e:
        print(e)


# New Seller All Products
@v4_product_router.get("/seller/all/products/{seller_id}", response_model=SellerWiseProductData, dependencies=[Depends(JWTBearer())])
async def sellerWiseProductsViewAll(request: Request, seller_id: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:

        userdata = auth(request=request)
        # Check User
        check_user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(
            ProductModel.product_pricing)).filter(UserModel.status == 1).filter(ProductPricingModel.deleted_at.is_(None)).filter(ProductModel.userid == seller_id).filter(ProductModel.status == 51).group_by(ProductModel.id).order_by(ProductModel.updated_at.desc())

        # Category
        seller = products[0].seller

        if(products.count() == 0):
            return {"status_code": HTTP_200_OK, "is_registered": True, "products": [], "total_products": 0, "current_page": page, "total_pages": 0}

        total_records = products.count()

        # Unique Products
        unique_products = db.query(ProductModel).filter(
            ProductModel.userid == seller_id).filter(ProductModel.status == 51).count()

        # Seller
        db_seller = db.query(UserModel).filter(
            UserModel.id == seller_id).first()

        seller_created_at = db_seller.created_at.strftime("%Y")

        # Today Date
        today = date.today()
        today_date = today.strftime('%Y-%m-%d')

        # Check Brands products
        checkBrandsproductsexist = db.execute("SELECT COUNT(*) as total_products FROM products LEFT JOIN brands ON brands.id = products.brand_id WHERE brands.user_id =:user_id AND products.status = 51",{
            "user_id": seller_id
        }).first()
        brands = []
        if(checkBrandsproductsexist.total_products != 0):
            checkBrands = db.query(BrandsModel).filter(
                BrandsModel.user_id == seller_id).filter(BrandsModel.status == 1).all()

            if(len(checkBrands) > 0):
                for brand in checkBrands:
                    b = {
                        'id': brand.id,
                        'name': brand.name,
                        'logo': brand.logo
                    }
                    brands.append(b)
        # Check Seller Purchase Limit
        check_seller_purchase_limit = await ProductsHelper.check_seller_facility(db=db, user_id=seller_id, type='purchase_limit')
        purchase_limit_text = ''
        if(check_seller_purchase_limit is not None):
            purchase_limit_text = 'Min. purchase limit ₹' + \
                str(check_seller_purchase_limit.key_value)

        # Check Seller Discount
        check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=db_seller.id, today_date=today_date)
        seller_data = {
            'id': db_seller.id,
            'name': db_seller.name,
            'address': str(db_seller.city)+','+' '+str(db_seller.region),
            'brands': brands,
            'level': 'Gold',
            'rating': '4.5',
            'since_year': 'Selling since '+str(seller_created_at),
            'rated_by_customer': 'Rated by 506 Customer',
            'no_of_categories': str(total_records)+' Various Categories',
            'no_of_products': str(unique_products)+' Unique Products',
            "purchase_limit_text": purchase_limit_text,
            "discount_rate": check_seller_discount['rate'],
            'discount_amount': check_seller_discount['amount']

        }

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != "0"):
            total_pages = int(total_pages[0]) + 1
        else:
            total_pages = int(total_pages[0])

        productdata: ProductModel = products.limit(
            limit=limit).offset((page - 1) * limit).all()

        # calculating Price
        # Aseztak Service
        check_asez_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

        for product in productdata:
            # CHECK PRODUCT DISCOUNT
            check_product_discount = product.product_discount.filter(
                func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
            product_discount = {
                'rate': '0.00',
                'product_sale_price': '0.00',
                'discount_amount': '0.00',
            }

            # Check Pricing
            pricing = product.product_pricing[0]
            if(pricing):
                product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=check_asez_service, app_version='V4')
                # Get Product Sale Price
                product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=check_asez_service.rate,
                                                             gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                # Check ONLINE Discounted Price
                discount_price = float(0.00)
                checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                if(checkdiscount != 0):
                    discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                # Check Product Discount
                if(check_product_discount is not None):
                    product_discount = {
                        'rate': floatingValue(check_product_discount.discount),
                        'product_sale_price': floatingValue(product_sale_price),
                        'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                    }
                # Restructure Price data
                price_data = {
                    'id': pricing.id,
                    'price': floatingValue(product_price),
                    'product_discount': product_discount,
                    'discount': str(checkdiscount),
                    'discount_price': floatingValue(discount_price),
                    'moq': pricing.moq,
                    'unit': pricing.unit
                }
                product.pricing = price_data

             # Favourite
            product.favourite = False

            # Product Image
            productimage = product.images[0]

            product.image = ''
            if(productimage):
                product.image = productimage.file_path

            # Check Seller Discount
            product.seller_discount = check_seller_discount['rate']

        if(check_user.status == 1):
            is_registered = True
        else:
            is_registered = False
        return {"status_code": HTTP_200_OK, "is_registered": is_registered, "title": seller.name, "seller": seller_data, "products": productdata, "total_products": total_records, "current_page": page, "total_pages": total_pages}

    except Exception as e:
        print(e)


# Seller Category List
@v4_product_router.get("/seller/category/list/{seller_id}", dependencies=[Depends(JWTBearer())])
async def sellerCategoryList(seller_id: int, db: Session = Depends(get_db), page: int = 1, limit: int = 10):
    try:
        # Check Product Categories
        product_categories = db.query(ProductModel.category).filter(
            ProductModel.userid == seller_id).filter(ProductModel.status == 51).group_by(ProductModel.category)

        total_records = product_categories.count()
        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != "0"):
            total_pages = int(total_pages[0]) + 1
        else:
            total_pages = int(total_pages[0])

        productdata: ProductModel = product_categories.limit(
            limit=limit).offset((page - 1) * limit).all()
        unique_categories = []
        for category in productdata:
            cat = db.query(CategoriesModel).filter(
                CategoriesModel.id == category.category).first()
            c = {
                'id': cat.id,
                'name': cat.name,
                'image': cat.image_path,
            }
            unique_categories.append(c)

        return {"status_code": HTTP_200_OK, "categories": unique_categories, "total_categories": int(total_records), "current_page": int(page), "total_pages": int(total_pages)}
    except Exception as e:
        return {"status_code": HTTP_200_OK, "categories": [], "total_products": 0, "current_page": page, "total_pages": 0}

# Most Selling products


@v4_product_router.post("/most-selling-discounted-products", response_model=ProductData, dependencies=[Depends(JWTBearer())])
async def getAllProducts(request: Request, data: mostsellingdiscountedproducts, db: Session = Depends(get_db)):
    userdata = auth(request=request)
    # Check User
    check_user = db.query(UserModel).filter(
        UserModel.id == userdata['id']).first()

    # Aseztak Service
    today = datetime.now()

    # Title
    if(data.type == 'discounted'):
        title = 'Discounted products'
    if(data.type == 'selling'):
        title = 'Most selling products'
    else:
        title = 'All Products'
    try:
        pdata = await ProductsHelper.mostsellingdiscountedallproducts(db=db, type=data.type, category_id=data.category_id, sort=data.sorting)
        product_count = pdata.count()
        if(product_count == 0):
            return {"status_code": HTTP_202_ACCEPTED, "is_registered": is_registered, "title": title, "products": [], "total_products": 0, "current_page": data.page, "total_pages": 0}
        else:
            limit = 10
            productdata: ProductModel = pdata.filter(ProductModel.status == 51).limit(
                limit=limit).offset((data.page - 1) * limit).all()

            # calculating Price
            # Aseztak Service
            today = date.today()
            today_date = today.strftime('%Y-%m-%d')
            # Aseztak Service
            check_asez_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

            for product in productdata:
                # CHECK PRODUCT DISCOUNT
                check_product_discount = product.product_discount.filter(
                    func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today_date).order_by(ProductDiscountModel.id.desc()).first()
                product_discount = {
                    'rate': '0.00',
                    'product_sale_price': '0.00',
                    'discount_amount': '0.00',
                }

                # Check Pricing
                pricing = product.product_pricing[0]
                if(pricing):

                    product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricing, asez_service=check_asez_service, app_version='V4')

                    # Get Product Sale Price
                    product_sale_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=check_asez_service.rate,
                                                                 gst_on_commission=check_asez_service.gst_on_rate, tds=check_asez_service.tds_rate, tcs=check_asez_service.tcs_rate, round_off=check_asez_service.round_off)

                    # Check ONLINE Discounted Price
                    discount_price = float(0.00)
                    checkdiscount = await Discount.checkorderdiscount(disocunt_date=today_date, db=db)
                    if(checkdiscount != 0):
                        discount_price = await calculatediscountprice(price=float(product_price), discount=float(checkdiscount))
                    # Check Product Discount
                    if(check_product_discount is not None):
                        product_discount = {
                            'rate': floatingValue(check_product_discount.discount),
                            'product_sale_price': floatingValue(product_sale_price),
                            'discount_amount': floatingValue(float(product_sale_price) - float(product_price)),
                        }
                    # Restructure Price data
                    price_data = {
                        'id': pricing.id,
                        'price': floatingValue(product_price),
                        'product_discount': product_discount,
                        'discount': str(checkdiscount),
                        'discount_price': floatingValue(discount_price),
                        'moq': pricing.moq,
                        'unit': pricing.unit
                    }
                    product.pricing = price_data

                 # New Changes (TINA)
                product.is_new_arrival = await ProductsHelper.newarrival(product)

                # Check Best Selling and Treding Product
                product.is_best_selling = await ProductsHelper.bestsellingproducts(db, product)

                product.is_trending = await ProductsHelper.trendingproducts(db, product)

                # Favourite
                product.favourite = False

                # Product Image
                productimage = product.images[0]

                product.image = ''
                if(productimage):
                    product.image = productimage.file_path

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscountProductThumb(db=db, seller_id=product.userid, today_date=today_date)
                product.seller_discount = check_seller_discount['rate']

            # Total Records
            total_records = product_count

            # Number of Total Pages
            total_pages = str(round((total_records/limit), 2))

            total_pages = total_pages.split('.')

            if(total_pages[1] != "0"):
                total_pages = int(total_pages[0]) + 1
            else:
                total_pages = int(total_pages[0])
            if(check_user.status == 1):
                is_registered = True
            else:
                is_registered = False
            return {"status_code": HTTP_202_ACCEPTED, "is_registered": is_registered, "title": title, "products": productdata, "total_products": total_records, "current_page": data.page, "total_pages": round(total_pages, 0)}
    except Exception as e:
        print(e)
