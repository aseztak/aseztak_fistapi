
from datetime import date, datetime
from app.api.helpers.discount import Discount
from app.api.helpers.widgets import WidgetsHelper
from app.api.util.calculation import floatingValue, calculatediscountprice
from app.db.models.banners import BannersModel
from app.db.models.products import  InventoryModel, ProductModel, ProductPricingModel
from app.db.models.user import UserModel
from app.api.helpers.categories import CategoriesHelper
from app.api.helpers.products import ProductModel, ProductsHelper


from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session

from fastapi import APIRouter,  Depends
from starlette.status import HTTP_200_OK
from starlette.requests import Request
from app.services.auth import auth
from app.db.schemas.home_schema import  V4HomeSchema
from sqlalchemy.orm import joinedload
from sqlalchemy import text

from app.db.models.carts import CartModel
from app.db.models.media import ProductMediaModel
from app.api.helpers.services import AsezServices
from app.api.helpers.products import ProductsHelper
from app.db.models.user_facility import UserFacilityModel
from app.db.models.products import ProductModel
from app.db.schemas.product_schema import SellerDiscountList
v4_home_router = APIRouter()

# @v4_home_router.get("/update-footwear-seller-wise")
# async def updateFootWearProducts(db: Session = Depends(get_db)):
#     try:

#         products = db.query(ProductModel).join(
#             ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).filter(ProductModel.userid == 20405).filter(ProductModel.status != 98).filter(ProductModel.status != 99).all()

#         if(len(products) > 0):
#             for product in products:
#                 pricings = db.query(ProductPricingModel).filter(ProductPricingModel.deleted_at.is_(
#                     None)).filter(ProductPricingModel.product_id == product.id).all()

#                 for pricing in pricings:
#                     pricing.price = (pricing.price - 20)
#                     db.flush()
#                     db.commit()

#             return "SUCCESS"
#     except Exception as e:
#         print(e)

# @v4_home_router.get("/update-footwear-product-price")
# async def updateFootWearProducts(db: Session = Depends(get_db)):
#     try:

#         products = db.query(ProductModel).join(ProductCategories, ProductCategories.product_id ==
#                                                ProductModel.id).filter(ProductCategories.category_id == 1437).filter(ProductModel.status != 98).filter(ProductModel.status != 99).group_by(ProductModel.id).all()

#         try:
#             for product in products:
#                 pricings = db.query(ProductPricingModel).filter(
#                     ProductPricingModel.product_id == product.id).all()
#                 if(len(pricings) > 0):
#                     for pricing in pricings:
#                         commission = 3
#                         commission_amount = (pricing.price * commission) / 100
#                         price = (pricing.price - commission_amount)

#                         pricing.price = price
#                         db.flush()
#                         db.commit()

#             return "SUCCESS"
#         except Exception as e:
#             print(e)

#     except Exception as e:
#         print(e)
# # New (Rahul Singh)
# @v4_home_router.get("", response_model=HomeSchema, dependencies=[Depends(JWTBearer())])
# async def Home(request: Request, db: Session = Depends(get_db)):
#     try:

#         userdata = auth(request=request)
#         # Check user App Version
#         dbuser = db.query(UserModel).filter(
#             UserModel.id == userdata['id']).first()
#         if(dbuser.app_version is None):
#             dbuser.app_version = 'V4'
#             db.flush()
#             db.commit()

#         if(dbuser.app_version == 'V3'):
#             dbuser.app_version = 'V4'
#             db.flush()
#             db.commit()

#         # Category
#         categories = db.execute(text(
#             "CALL Home_Categories()")).all()

#         # Banners
#         banners = db.query(BannersModel).filter(
#             BannersModel.type == 'INNER').filter(BannersModel.status == 1).order_by(BannersModel.id.desc()).all()

#         banner_data = []
#         if(len(banners) > 0):
#             for banner in banners:
#                 bn = {
#                     'id': banner.id,
#                     'banner': banner.banner
#                 }
#                 banner_data.append(bn)

#         # Widgets
#         widgets = await WidgetsHelper.get_home_widgets(db=db)
#         widget_data = []
#         if(len(widgets) > 0):
#             for widget in widgets:

#                 images = widget.widget_images

#                 image_data = []
#                 for image in images:
#                     img = {
#                         'image': image.image,
#                         'go_to': image.go_to,
#                         'id': image.go_to_id
#                     }

#                     image_data.append(img)

#                 wd = {
#                     'id': widget.id,
#                     'widget_name': widget.widget_name,
#                     'title': widget.title,
#                     'description': widget.description,
#                     'no_of_images': widget.no_of_images,
#                     'items': image_data
#                 }

#                 widget_data.append(wd)

#         # Featured Products
#         featured_products = {
#             'title': 'Featured Products',
#             'products': []
#         }

#         return {"status_code": HTTP_200_OK, "categories": categories, "banners": banner_data, "widgets": widget_data, "featured_products": featured_products}
#     except Exception as e:
#         print(e)


# New HOME API (Rahul Singh)
@v4_home_router.get("", response_model=V4HomeSchema, dependencies=[Depends(JWTBearer())])
async def Home(request: Request, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)
        # Check user App Version
        dbuser = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()
        if(dbuser.app_version is None):
            dbuser.app_version = 'V4'
            db.flush()
            db.commit()

        if(dbuser.app_version == 'V3'):
            dbuser.app_version = 'V4'
            db.flush()
            db.commit()

        # Category
        categories = db.execute(text(
            "CALL Home_Categories()")).all()

        # Banners
        banner_data = []
        banners = db.execute(text("CALL Home_Banners()")).all()
        for banner in banners:
            if(banner.link is None):
                link = ''
            else:
                link = banner.link

            b = {
                'id': banner.id,
                'banner': banner.banner,
                'link_id': banner.link_id,
                'link': link
            }
            banner_data.append(b)

        # Widgets
        widgets = await WidgetsHelper.get_home_widgets(db=db)
        widget_data = []
        if(len(widgets) > 0):
            for widget in widgets:

                images = widget.widget_images
                image_data = []
                for image in images:
                    img = {
                        'image': image.image,
                        'go_to': image.go_to,
                        'id': image.go_to_id
                    }

                    image_data.append(img)

                wd = {
                    'id': widget.id,
                    'widget_name': widget.widget_name,
                    'title': widget.title,
                    'description': widget.description,
                    'no_of_images': widget.no_of_images,
                    'items': image_data
                }

                widget_data.append(wd)

        # Cart Data
        cart_data = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).options(joinedload(CartModel.product)).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(
            CartModel.user_id == userdata['id']).filter(CartModel.status == 1).first()

        if(cart_data is not None):
            # Get Product Detail
            productdata = db.query(ProductModel).where(
                ProductModel.id == cart_data.product_id).first()

            pricing = cart_data.uuid.split('-')
            # ProductImage
            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == cart_data.product.id).filter(
                ProductMediaModel.deleted_at.is_(None)).first()

            pricing = db.query(ProductPricingModel).filter(ProductPricingModel.id == pricing[1]).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            if(pricing):
                cartcheckinventory = db.query(InventoryModel).where(
                    InventoryModel.pricing_id == pricing.id).first()

                if(cartcheckinventory and cartcheckinventory.out_of_stock == 0):
                    stock = True
                else:
                    stock = False

                # Aseztak Service
                # aseztak_service = Services.aseztak_services(
                #     pricing.updated_at, db=db)
                today_date = pricing.updated_at.strftime(
                    '%Y-%m-%d')
                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricing, asez_service=aseztak_service, app_version='V4')
                # product_price = productPricecalculation(price=cart_data.price, tax=cart_data.tax, commission=aseztak_service.rate,
                #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                cart_data = {
                    'id': cart_data.id,
                    'product_title': cart_data.product.title,
                    'product_description': cart_data.product.short_description,
                    'image': image.file_path,
                    'price': floatingValue(product_price),
                    'quantity': cart_data.quantity,
                    'unit': pricing.unit,
                    'total': floatingValue((product_price * cart_data.quantity)),
                    'stock': stock

                }
        else:
            cart_data = {
                'id': 0,
                'product_title': '',
                'product_description': '',
                'image': '',
                'price': '0.00',
                'quantity': 0,
                'unit': '',
                'total': '0.0',
                'stock': False

            }

        return {"status_code": HTTP_200_OK, "categories": categories, "banners": banner_data, "widgets": widget_data, "cart_data": cart_data}
    except Exception as e:
        print(e)


@v4_home_router.get("/category")
async def homeMenu():
    try:
        categories = [
            {
                'name': 'GARMENTS',
                'description': 'All types of readymade garments like T-shirt, jeans, pants, kurties etc',
                'value': 'garments',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/garments.png'
            },
            # {
            #     'name': 'FOOTWEAR',
            #     'description': 'Different types of unisex shoes and footwear options for women, men, kids',
            #     'value': 'footwear',
            #     'icon': 'https://aseztak.ap-south-1.linodeobjects.com/shoes.png'
            # },
            {
                'name': 'ALL',
                'description': 'All types products like T-shirt, jeans, pants, kurties etc',
                'value': 'all',
                'icon': 'https://aseztak.ap-south-1.linodeobjects.com/all_icon.png'
            }
        ]

        return {"status_code": HTTP_200_OK, "menus": categories}
    except Exception as e:
        print(e)


@v4_home_router.get("/menu", dependencies=[Depends(JWTBearer())])
async def homeMenu(request: Request, db: Session = Depends(get_db)):
    try:
        data = CategoriesHelper.get_categories(db=db, parent_id=1)
        categories = await CategoriesHelper.hasChildCategories(request=request, db=db, data=data)

        all = [{
            'id': 'all',
            'name': 'All',
            'icon': ''
        }]

        cat_data = []
        for category in categories:
            c = {
                'id': str(category.id),
                'name': category.name,
                'icon': category.image_path
            }
            cat_data.append(c)

        final_array = all + cat_data

        return {"status_code": HTTP_200_OK, "menus": final_array}
    except Exception as e:
        print(e)


@v4_home_router.get("/{param}", response_model=V4HomeSchema,  dependencies=[Depends(JWTBearer())])
async def Home(request: Request, param: str, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)
        # Check Free Delivery for every buyer
        check_free_delivery = db.query(UserFacilityModel).filter(UserFacilityModel.user_id == userdata['id']).filter(
            UserFacilityModel.key_name == 'free_delivery').order_by(UserFacilityModel.id.desc()).first()
        buyer_free_delivery = ''
        if(check_free_delivery is not None):
            buyer_free_delivery = float(check_free_delivery.key_value)
        # Check user App Version
        dbuser = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()
        if (dbuser.app_version is None):
            dbuser.app_version = 'V4'
            db.flush()
            db.commit()

        if (dbuser.app_version == 'V3'):
            dbuser.app_version = 'V4'
            db.flush()
            db.commit()

        if (param.lower() == 'all'):
            # Category
            categories = db.execute(text(
                "CALL Home_Categories()")).all()
        else:

            parent_categories = CategoriesHelper.get_parent_categories(
                db=db, param=param)

            categories = []
            for category in parent_categories:
                # Check Has Child Category
                has_child = CategoriesHelper.get_parent_has_child(
                    db=db, parent_id=category.id)
                c = {
                    'id': category.id,
                    'name': category.name,
                    'slug': category.slug,
                    'description': category.description,
                    'image': category.image_path,
                    'status': category.status,
                    'haschild': has_child
                }

                categories.append(c)

        # Banners and Widgets
        if (param.lower() == 'footwear'):
            # Banners
            banners = db.query(BannersModel).filter(
                BannersModel.type == 'FOOTWEAR').filter(BannersModel.status == 1).order_by(BannersModel.id.desc()).all()

            # Widgets
            widgets = await WidgetsHelper.get_home_new_widgets(db=db, param='FOOTWEAR')

        elif (param.lower() == 'garments'):
            # Banners
            banners = db.query(BannersModel).filter(
                BannersModel.type == 'GARMENTS').filter(BannersModel.status == 1).order_by(BannersModel.id.desc()).all()

            # Widgets
            widgets = await WidgetsHelper.get_home_new_widgets(db=db, param='GARMENTS')

        else:
            banners = db.execute(text("CALL Home_Banners()")).all()
            widgets = await WidgetsHelper.get_home_new_widgets(db=db, param='HOME')

        # Banners
        banner_data = []
        for banner in banners:
            link = ''
            if(banner.link is not None):
                link = banner.link
            b = {
                'id': banner.id,
                'banner': banner.banner,
                'link_id': banner.link_id,
                'link': link

            }
            banner_data.append(b)

        # Widgets
        widget_data = []
        if (len(widgets) > 0):
            for widget in widgets:

                images = widget.widget_images
                image_data = []
                for image in images:
                    img = {
                        'image': image.image,
                        'go_to': image.go_to,
                        'id': image.go_to_id
                    }

                    image_data.append(img)

                if(widget.start_date is None):
                    start_date = 0
                else:
                    start_date = widget.start_date.strftime(
                        "%Y-%m-%d %H:%M:%S")
                    dt_obj = datetime.strptime(start_date,
                                               '%Y-%m-%d %H:%M:%S')
                    start_date = dt_obj.timestamp() * 1000
                end_date = 0
                if(widget.end_date is not None):
                    enddate = widget.end_date.strftime(
                        "%Y-%m-%d %H:%M:%S")
                    dt_obj = datetime.strptime(enddate,
                                               '%Y-%m-%d %H:%M:%S')
                    end_date = dt_obj.timestamp() * 1000
                    current_date = datetime.now()
                    if(current_date > widget.end_date):
                        start_date = 0
                        end_date = 0
                wd = {
                    'id': widget.id,
                    'start_date': start_date,
                    'end_date': end_date,
                    'widget_name': widget.widget_name,
                    'title': widget.title,
                    'description': widget.description,
                    'no_of_images': widget.no_of_images,
                    'items': image_data
                }

                widget_data.append(wd)

        # Featured Products
        featured_products = {
            'title': 'Featured Products',
            'products': []
        }

        # Cart Data
        cart_data = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).options(joinedload(CartModel.product)).filter(
            CartModel.user_id == userdata['id']).filter(CartModel.status == 1).filter(UserModel.status == 1).filter(ProductModel.status == 51).first()

        if (cart_data is not None):
            # Get Product Detail
            productdata = db.query(ProductModel).where(
                ProductModel.id == cart_data.product_id).first()

            pricing = cart_data.uuid.split('-')
            # ProductImage
            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == cart_data.product.id).filter(
                ProductMediaModel.deleted_at.is_(None)).first()

            pricing = db.query(ProductPricingModel).filter(ProductPricingModel.id == pricing[1]).filter(
                ProductPricingModel.deleted_at.is_(None)).first()

            if (pricing):
                cartcheckinventory = db.query(InventoryModel).where(
                    InventoryModel.pricing_id == pricing.id).first()

                if (cartcheckinventory and cartcheckinventory.out_of_stock == 0):
                    stock = True
                else:
                    stock = False

                # Aseztak Service
                # aseztak_service = Services.aseztak_services(
                #     pricing.updated_at, db=db)
                today_date = pricing.updated_at.strftime(
                    '%Y-%m-%d')
                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricing, asez_service=aseztak_service, app_version='V4')
                # product_price = productPricecalculation(price=cart_data.price, tax=cart_data.tax, commission=aseztak_service.rate,
                #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                cart_data = {
                    'id': cart_data.id,
                    'product_title': cart_data.product.title,
                    'product_description': cart_data.product.short_description,
                    'image': image.file_path,
                    'price': floatingValue(product_price),
                    'quantity': cart_data.quantity,
                    'unit': pricing.unit,
                    'total': floatingValue((product_price * cart_data.quantity)),
                    'stock': stock

                }
        else:
            cart_data = {
                'id': 0,
                'product_title': '',
                'product_description': '',
                'image': '',
                'price': '0.00',
                'quantity': 0,
                'unit': '',
                'total': '0.0',
                'stock': False

            }
        # Check Discounted Product Users
        todaydate = date.today()
        todaydate = todaydate.strftime("%Y-%m-%d")
        # user_list = db.query(UserModel).join(ProductDiscountModel, ProductDiscountModel.seller_id == UserModel.id).filter(
        #     func.date_format(ProductDiscountModel.valid_upto, "%Y-%m-%d") >= todaydate).group_by(ProductDiscountModel.seller_id).order_by(UserModel.id.desc())
        # user_list = db.query(SellerDiscountModel).filter(func.date_format(
        #     SellerDiscountModel.valid_upto, '%Y-%m-%d') >= todaydate).group_by(SellerDiscountModel.seller_id).order_by(SellerDiscountModel.id.desc())
        sellers = []
        # if(user_list.count() > 0):
        #     for user in user_list:
        #         userdata = db.query(UserModel).filter(
        #             UserModel.id == user.seller_id).first()
        #         # Check Product Discount
        #         # check_product_discount = db.query(ProductDiscountModel).filter(func.date_format(ProductDiscountModel.valid_upto, '%Y-%m-%d') >= todaydate).filter(
        #         #     ProductDiscountModel.seller_id == user.id).order_by(ProductDiscountModel.discount.desc()).first()

        #         u = {
        #             'id': userdata.id,
        #             'name': userdata.name,
        #             'city': userdata.city,
        #             'state': userdata.region,
        #             'title': 'Massive Discounts',
        #             'subtitle': 'Grab Mind Blowing Discounts From Top Sellers',
        #             'discount': float(user.rate),
        #             'amount': float(user.limit_amount),
        #             'valid_upto': str(user.valid_upto)
        #         }
        #         sellers.append(u)
        return {"status_code": HTTP_200_OK, "buyer_free_delivery": buyer_free_delivery, "categories": categories, "banners": banner_data, "widgets": widget_data, "cart_data": cart_data, "sellers": sellers}
    except Exception as e:
        print(e)


@v4_home_router.get("/discounted/sellers", response_model=SellerDiscountList, dependencies=[Depends(JWTBearer())])
async def discountedSellers(request: Request, db: Session = Depends(get_db)):
    try:
        # Check Discounted Product Users
        todaydate = date.today()
        todaydate = todaydate.strftime("%Y-%m-%d")
        # user_list = db.query(SellerDiscountModel).filter(func.date_format(
        #     SellerDiscountModel.valid_upto, '%Y-%m-%d') >= todaydate).group_by(SellerDiscountModel.seller_id).order_by(SellerDiscountModel.id.desc())
        user_list = db.execute(
            "select a.* from seller_discount a inner join (select seller_id ,limit_amount, max(id) as maxid from seller_discount WHERE date_format(seller_discount.valid_upto, '%Y-%m-%d') >=:todaydate group by seller_id) as b on a.id = b.maxid ORDER BY a.id DESC", {
                "todaydate": todaydate
            }).all()
        sellers = []
        # Aseztak Service
        check_asez_service = await AsezServices.aseztak_services(commission_date=todaydate, db=db)

        if(len(user_list) > 0):
            for user in user_list:
                userdata = db.query(UserModel).filter(
                    UserModel.id == user.seller_id).first()

                # Products
                products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(
                    ProductModel.userid == userdata.id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(ProductModel.updated_at.desc()).limit(5)
                sellerProducts = []
                if(products.count() > 0):
                    for product in products.all():
                        product_discount = {
                            'rate': '0.00',
                            'product_sale_price': '0.00',
                            'discount_amount': '0.00',
                        }

                        # Check Pricing
                        sellerpricing = product.product_pricing[0]
                        sellerpricingdata = {
                            'id': 0,
                            'price': '0.00',
                            'product_discount': product_discount,
                            'discount': '0.00',
                            'discount_price': '0.00',
                            'moq': 0,
                            'unit': ''
                        }

                        if(sellerpricing):
                            pp: ProductModel = await ProductsHelper.getPrice(db, product, sellerpricing, asez_service=check_asez_service, app_version='V4')
                            # Check Discounted Price
                            discount_price = float(0.00)
                            checkdiscount = await Discount.checkorderdiscount(disocunt_date=todaydate, db=db)
                            if(checkdiscount != 0):
                                discount_price = await calculatediscountprice(price=float(pp), discount=float(checkdiscount))

                            sellerpricingdata = {
                                'id': sellerpricing.id,
                                'price': floatingValue(pp),
                                'product_discount': product_discount,
                                'discount': str(checkdiscount),
                                'discount_price': floatingValue(discount_price),
                                'moq': sellerpricing.moq,
                                'unit': sellerpricing.unit
                            }

                        # Product Image
                        simg = ''
                        simage = product.images[0]
                        if(simage is not None):
                            simg = simage.file_path

                        # New Changes (TINA)
                        sproductis_new_arrival = await ProductsHelper.newarrival(product)

                        # Check Best Selling and Treding Product # NEW Change (TINA)
                        sproductis_best_selling = await ProductsHelper.bestsellingproducts(db, product)

                        sproductis_trending = await ProductsHelper.trendingproducts(db, product)

                        # Product Favourite
                        sfavourite = True

                        sellerpr = {
                            'id': product.id,
                            'title': product.title,
                            'image': simg,
                            'short_description': product.short_description,
                            'category': product.category,
                            'pricing': sellerpricingdata,
                            'favourite': sfavourite,
                            'is_new_arrival': sproductis_new_arrival,
                            'is_best_selling': sproductis_best_selling,
                            'is_trending': sproductis_trending,
                            'created_at': product.created_at.strftime("%Y-%m-%d"),
                            'status': product.status,
                            'seller_discount': user.rate
                        }
                        sellerProducts.append(sellerpr)

                    u = {
                        'id': userdata.id,
                        'name': userdata.name,
                        'city': userdata.city,
                        'state': userdata.region,
                        'title': 'Massive Discounts',
                        'subtitle': 'Grab Mind Blowing Discounts From Top Sellers',
                        'discount': str(float(user.rate)),
                        'amount': str(float(user.limit_amount)),
                        'valid_upto': str(user.valid_upto),
                        'products': sellerProducts
                    }
                    sellers.append(u)
        return {"status_code": HTTP_200_OK, "sellers": sellers}
    except Exception as e:
        print(e)
        return {"status_code": HTTP_200_OK, "sellers": []}

# @v4_home_router.get("/seller/accounts/update/{seller_id}")
# async def sellerAccountsUpdate(seller_id: int, db: Session = Depends(get_db)):
#     try:

#         orders = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
#             ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(
#             ProductModel.userid == seller_id).filter(func.date_format(OrdersModel.created_at,  "%Y-%m-%d") >= '2022-01-01').group_by(OrdersModel.id).filter(OrderItemsModel.status.between(0, 81)).filter(OrderItemsModel.status != 61).filter(OrderItemsModel.status != 80).filter(OrdersModel.commission_reff.is_(None)).all()


#         if(len(orders) > 0):

#             accounts_id = []

#             not_found = []
#             for order in orders:
#                 order_number = "%{}%".format(order.order_number)
#                 # Check Accounts
#                 check_accounts = db.query(AccountsModel).filter(
#                     AccountsModel.txn_description.ilike(order_number)).first()

#                 if(check_accounts is not None):
#                     accounts_id.append(check_accounts.id)
#                 else:
#                     not_found.append(order.order_number)

#             # Accounts
#             db.query(AccountsModel).filter(AccountsModel.user_id == seller_id).filter(
#                 AccountsModel.id.not_in(accounts_id)).delete()
#             db.commit()

#             return "SUCCESS"

#             return len(orders)
#     except Exception as e:
#         print(e)


# Updte Product categoies FOOTWEAR
# @v4_home_router.get("/update/categories")
# async def updateCategories(db: Session = Depends(get_db)):
#     try:
#         product_categories = db.query(ProductCategories).filter(
#             ProductCategories.category_id == 1437).all()
#         if (len(product_categories) > 0):
#             for product_category in product_categories:
#                 product_cat = db.query(ProductCategories).filter(
#                     ProductCategories.product_id == product_category.product_id).all()

#                 for product in product_cat:
#                     if (product.category_id == 7):
#                         product.category_id = 2
#                         db.flush()
#                         db.commit()

#         return "SUCCESS"
#     except Exception as e:
#         return "FAILED"
