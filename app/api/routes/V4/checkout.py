from operator import or_
from typing import  Optional
from datetime import date, datetime

from starlette.requests import Request
from starlette.status import  HTTP_200_OK, HTTP_400_BAD_REQUEST

from app.api.helpers.checkout import CheckOutHelper
from app.api.helpers.cart import CartHelper, OrderCalculation
from app.api.helpers.calculation import *
from app.db.models.carts import CartModel
from app.db.models.orders import OrdersModel
from app.db.models.user import UserModel
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session

from fastapi import APIRouter,  Depends
from app.db.schemas.orders_schema import  checkOutSchema
from app.api.helpers.users import *
import requests
import json
from app.services.auth import auth
from paytmchecksum import PaytmChecksum
from app.core import config
from app.db.models.transaction import TransactionsModel
from app.api.util.calculation import productPricecalculation, checkOrderPlaceDeliveryCalculation, floatingValue, roundOf
from app.db.models.user_facility import UserFacilityModel
from app.db.models.wallet import WalletModel
from app.api.helpers.products import ProductsHelper
from app.api.helpers.services import AsezServices
from app.api.helpers.orders import OrderHelper
from app.api.helpers.facility import Facility
from app.api.helpers.address import AddressHelper
from app.api.helpers.products import ProductsHelper
from app.api.helpers.wallet_helper import WalletHelper
from app.db.models.order_fails import OrderFailsModel
from app.db.models.payment_failed import PaymentFailedModel
from app.core.config import REWARDS_PERCENTAGE
from app.db.schemas.orders_schema import paymentoption
from app.db.models.payment_options import PaymentOptionModel
from app.api.util.message import Message

v4_checkout_router = APIRouter()


@v4_checkout_router.get("/new/summary/{walletbalance}", dependencies=[Depends(JWTBearer())])
async def cartSummary(request: Request, walletbalance: Optional[str] = "0.00", db: Session = Depends(get_db)):

    userdata = auth(request=request)
    finalcartitems = []
    total_amount = 0
    # Total Amount without discount
    total_amount_without_discoun = 0
    total_delivery = 0
    offerTxt = ''
    checkTotalAmountforSellerCodLimit = ''
    # Total Savings Amount
    total_savings_amount = ''

    # Check Delivery Charger for new user
    new_free_delivery = False
    if(config.FREE_DELIVERY == "True"):
        check_order_exist = await OrderHelper.check_order_exist(db=db, user_id=userdata['id'])
        new_free_delivery = check_order_exist

    # Check User facility
    buyer_facility = await Facility.buyer_facility(db=db, user_id=userdata['id'])

    # Check Discount Facility
    buyer_orderdiscount_facility = await Facility.buyer_discount_facility(db=db, user_id=userdata['id'])
    # New Changes
    place_order_btn = False
    try:
        data = await CartHelper.cart_data_on_summary_page(db=db, user_id=userdata['id'])

        finalcartdata = []
        for cartitems in data:

            in_stock_items = await CartHelper.in_stock_cart_items_on_summary_page(db=db, seller_id=cartitems.seller_id, user_id=userdata['id'])

            cartdata = []
            for cartitem in in_stock_items:
                cartdata.append(cartitem)

            finalcartdata.append(cartdata)
        # Calculate Wallet Amount
        calculatewallet_amount = 0
        for cartitemdata in finalcartdata:

            sub_total = 0
            # Sub total with discount
            sub_total_with_discount = 0
            # Sub total without discount
            sub_total_without_discount = 0
            product_data = []

            for item in cartitemdata:

                # Fetch Single Cart Data
                singlecartdata = await CartHelper.single_cart_data(db=db, user_id=item.user_id, product_id=item.product_id)

                # Get Seller Data
                seller = await fetch_seller_data(db=db, seller_id=item.seller_id)

                # Get Product Data
                product = await ProductsHelper.fetch_product_data(db=db, product_id=item.product_id)

                # Get cart items by product id
                itemsdata = await CartHelper.fetch_cart_item_by_product(db=db, user_id=userdata['id'], seller_id=item.seller_id, product_id=item.product_id)

                # Get Pricing ID
                itm_pricing_id = singlecartdata.uuid.split('-')

                # Get Item Pricing
                itm_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=itm_pricing_id[1])

                # Check Pricing Exist
                if(itm_pricing):

                    total_items = []
                    order_total_amount = 0

                    # Load Items
                    img = ''
                    for items in itemsdata:
                        # Get Product Detail
                        productdata = await ProductsHelper.fetch_product_data(db=db, product_id=items.product_id)
                        # productdata = db.query(ProductModel).where(
                        #     ProductModel.id == items.product_id).first()

                        # Get Pricing ID
                        item_pricing_id = items.uuid.split('-')

                        # Get Pricing
                        item_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=item_pricing_id[1])

                        # Check Pricing
                        if(item_pricing):

                            # Get Pricing Stock
                            checkiteminventory = await ProductsHelper.fetch_pricing_inventory(db=db, pricing_id=item_pricing.id)

                            # Check Pricing Stock
                            if(checkiteminventory.out_of_stock == 0):

                                # Aseztak Service
                                # today_date = item_pricing.updated_at.strftime(
                                #     '%Y-%m-%d')
                                todaydate = date.today()
                                today_date = todaydate.strftime('%Y-%m-%d')
                                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                                # Calculate Product Pricing
                                product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                # Product Price With Discount
                                product_price_with_discount: ProductModel = await ProductsHelper.chekcOutPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                # Product Price without discount
                                product_price_without_discount = productPricecalculation(price=item_pricing.price, tax=item_pricing.tax, commission=aseztak_service.rate,
                                                                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                                # Calculate Product toal Price with Quantity
                                totalprice = round(
                                    product_price, 2) * items.quantity
                                calculateforwalletamount = (
                                    float(totalprice) * float(REWARDS_PERCENTAGE) / 100)
                                calculateforwalletamount = int(
                                    calculateforwalletamount)
                                calculatewallet_amount += calculateforwalletamount
                                # Total Price with discount
                                totalprice_with_discount = round(
                                    product_price_with_discount, 2) * items.quantity
                                # Total Price without discount
                                totalprice_without_discount = round(
                                    product_price_without_discount, 2) * items.quantity
                                sub_total += totalprice
                                # Sub Total with discount
                                sub_total_with_discount += totalprice_with_discount

                                # Sub Total without discount
                                sub_total_without_discount += totalprice_without_discount

                                order_total_amount += totalprice
                                total_items.append(items)

                                # Set Product Image
                                if(item_pricing.default_image != 0):
                                    image = await ProductsHelper.fetch_product_media_data(db=db, default_img_id=item_pricing.default_image, product_id=0)
                                else:
                                    image = await ProductsHelper.fetch_product_media_data(db=db, default_img_id=1, product_id=items.product_id)

                                if(image == None):
                                    image = await ProductsHelper.fetch_product_media_data(db=db, default_img_id=0, product_id=items.product_id)

                            img = image.file_path

                    # Customize Product Data group By Seller
                    p_data = {
                        'image': img,
                        'id': product.id,
                        'title': product.title,
                        'slug': product.slug,
                        'items': len(total_items),
                        'amount': floatingValue(round(order_total_amount, 2))
                    }

                    product_data.append(p_data)

                # Customize Seller Details
                seller = {
                    'id': seller.id,
                    'name': seller.name,
                    'address': seller.city+', '+seller.region
                }

            if(len(product_data) > 0):
                # Store Data For delivery charge Purpose
                delivery_charge = 0
                min_order_value = 0
                order_checkout_text = PRODUCT_FREE_DELIVERY_TEXT
                free_delivery = True

                # Today date
                today = date.today()
                # YY-mm-dd
                today = today.strftime("%Y-%m-%d")

                # Check Shipping Charge
                shipping_charge = await OrderHelper.check_order_shippinhg_charge(db=db, date=today)

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscount(db=db, seller_id=seller['id'], today_date=today, order_amount=float(round(sub_total, 2)))
                if(check_seller_discount != 0):
                    seller_discount_amount = (
                        float(round(sub_total, 2)) - float(round(sub_total_with_discount)))
                    sub_total = (float(round(sub_total_with_discount, 2)))
                    order_checkout_text = str(
                        'You saved ₹')+str(round(seller_discount_amount, 2))

                # If order limit not equal to '0'
                if(new_free_delivery == False):

                    # Check Buyer Facility
                    if(buyer_facility):
                        freedeliveryamount = int(buyer_facility.key_value)

                        if(round(sub_total, 2) < round(freedeliveryamount, 2)):

                            # Shipping rate
                            delivery_charge = shipping_charge.rate
                            # minimum order check text
                            if(shipping_charge.rate != 0):
                                order_checkout_text = 'Get Free Delivery on your every order above Rs/- ' + \
                                    str(freedeliveryamount)
                                free_delivery = False
                            else:
                                order_checkout_text = PRODUCT_FREE_DELIVERY_TEXT
                                # Check free delivery
                                free_delivery = True

                    else:

                        if (shipping_charge.order_limit != 0):
                            if (round(sub_total, 2) < round(shipping_charge.order_limit, 2)):
                                # Shipping rate
                                delivery_charge = shipping_charge.rate
                                # Minimum order amount
                                min_order_value = shipping_charge.order_limit
                                # minimum order check text
                                if(shipping_charge.shipping_text is not None):
                                    order_checkout_text = shipping_charge.shipping_text
                                else:
                                    order_checkout_text = ''

                                # Check free delivery
                                free_delivery = False

                        else:
                            # Shipping rate
                            delivery_charge = shipping_charge.rate
                            # minimum order check text
                            if(shipping_charge.rate == 0):
                                order_checkout_text = PRODUCT_FREE_DELIVERY_TEXT
                                free_delivery = True
                            else:
                                order_checkout_text = ''
                                # Check free delivery
                                free_delivery = False

                # if(new_free_delivery == False):
                # Check Seller Purchase Limit
                check_seller_facility = await ProductsHelper.check_seller_facility(db=db, user_id=cartitemdata[0].seller_id, type='purchase_limit')
                # Showing Seller Products #New Updated
                seller_purchase_limit = {
                    'visible': True,
                    'order_amount': float(0)
                }
                # New Product Total Amount
                if(check_seller_facility is not None):
                    # Showing Seller Products #New Updated
                    seller_purchase_limit = {
                        'visible': True,
                        'order_amount': float(check_seller_facility.key_value)
                    }
                    if(float(check_seller_facility.key_value) <= float(round(sub_total, 2))):
                        # Product Total Amount
                        total_amount += round(sub_total, 2)
                        # Total Amount without discount
                        total_amount_without_discoun += round(
                            sub_total_without_discount, 2)
                        # Product Total Delivery Charge
                        total_delivery += delivery_charge
                        place_order_btn = True
                    else:
                        seller_purchase_limit = {
                            'visible': False,
                            'order_amount': float(check_seller_facility.key_value)
                        }
                else:
                    place_order_btn = True
                    # Product Total Amount
                    total_amount += round(sub_total, 2)
                    # Total Amount without discount
                    total_amount_without_discoun += round(
                        sub_total_without_discount, 2)
                    # Product Total Delivery Charge
                    total_delivery += delivery_charge
                # Cart Items
                # Check COD limit partial order avobe 10000/-
                checkTotalAmountforCodLimit = ''
                if(round(sub_total, 2) > 30000):
                    checkTotalAmountforCod = 'You can buy only ₹30000 at a time in COD'
                    checkTotalAmountforCodLimit = checkTotalAmountforCod
                    # checkTotalAmountforSellerCodLimit += checkTotalAmountforCodLimit
                    checkTotalAmountforSellerCodLimit = checkTotalAmountforCodLimit

                finalcartitems.append(
                    {'seller': seller, 'seller_purchase_limit': seller_purchase_limit, 'product_data': product_data, 'total_amount': floatingValue(round(sub_total, 2)), 'delivery_charge': floatingValue(delivery_charge), 'min_order_value': floatingValue(min_order_value), 'order_checkout_text': order_checkout_text, 'free_delivery': free_delivery, "checkTotalAmountforCodLimit": checkTotalAmountforCodLimit})

        # Total Cart Items #New Updated
        final_count_total_items = 0
        if(len(finalcartitems) > 0):
            for total_cart_item in finalcartitems:
                if(total_cart_item['seller_purchase_limit']['visible'] == True):
                    for fnal_product in total_cart_item['product_data']:
                        final_count_total_items += fnal_product['items']
        # Grand Total
        grandtotal = total_amount + float(total_delivery)  # New Updated

        # Check total product savings amount
        check_total_savings_amount = (
            float(total_amount_without_discoun) - float(total_amount))
        saving_amount_visible = False

        totalAmountObj = {
            "name": "Total Amount",
            "value": floatingValue(round(total_amount, 2)),
            "currency": True,
            "visible": True,
            "offer_text": ""
        }

        if(check_total_savings_amount > 0):
            totalAmountObj = {
                "name": "Total Amount",
                "value": floatingValue(round(total_amount_without_discoun, 2)),
                "currency": True,
                "visible": True,
                "offer_text": ""
            }
            saving_amount_visible = True
            total_savings_amount = '- ₹' + \
                str(round(check_total_savings_amount, 2))
        # Order Summary Text
        # New Changes Wallet
        walletvisible = False
        wallet_closing_balance = await WalletHelper.check_wallet_balnce(db=db, user_id=userdata['id'])
        checked_wallet = False
        if(walletbalance != "0.00"):
            checked_wallet = True
        # New Changes Wallet
        wallet_balance = {
            'visible': False,
            'usable': False,
            'balance': floatingValue(wallet_closing_balance),
            'use_balance': floatingValue(0),
            'checked_wallet': checked_wallet
        }
        # New Changes Wallet
        if(wallet_closing_balance > 0):
            if(walletbalance != "0.00"):
                walletvisible = True
            # use_balance = await WalletHelper.check_usable_balnce(db=db, item_total=float(round(total_amount, 2)))
            if(float(wallet_closing_balance) > float(calculatewallet_amount)):
                if(calculatewallet_amount != 0):

                    wallet_balance = {
                        'visible': True,
                        'usable': True,
                        'balance': floatingValue(wallet_closing_balance),
                        'use_balance': floatingValue(calculatewallet_amount),
                        'checked_wallet': checked_wallet
                    }
        # Check Cod discount
        current_datetime = datetime.now()
        current_datetime = current_datetime.strftime("%Y-%m-%d %H:%M:%S")
        db_check_payment_options = db.execute(
            "SELECT * FROM `payment_options` WHERE payment_options.prefix = 'COD' AND (CASE WHEN payment_options.start_date is not NULL THEN payment_options.end_date >=:current_date AND payment_options.start_date <=:current_date END) ORDER BY id DESC LIMIT 1  ", {
                'current_date': current_datetime
            }).first()
        if(db_check_payment_options is None):
            db_check_payment_options = db.execute(
                "SELECT * FROM payment_options WHERE payment_options.prefix = 'COD' AND payment_options.start_date is NULL").first()
        discountamount = 0
        discountrate = 0
        if(db_check_payment_options.discount != 0):
            if(db_check_payment_options.limit_amount > 0):
                if(float(totalAmountObj['value']) > float(db_check_payment_options.limit_amount)):
                    discountamount = (
                        float(round(total_amount, 2)) - float(walletbalance))
                    discountamount = (float(discountamount) *
                                    float(db_check_payment_options.discount)) / 100
                    discountamount = round(discountamount, 2)
                    discountrate = db_check_payment_options.discount
            else:
                discountamount = (
                    float(round(total_amount, 2)) - float(walletbalance))
                discountamount = (float(discountamount) *
                    float(db_check_payment_options.discount)) / 100
                discountamount = round(discountamount, 2)
                discountrate = db_check_payment_options.discount


        discountvisible = False
        if(discountamount != 0):
            discountvisible = True

        order_summary = [
            {
                "name": "Items",
                "code": "items",
                "value": str(final_count_total_items),  # New Updated
                "currency": False,
                "visible": True,
                "offer_text": ""
            },
            totalAmountObj,
            {
                "name": "You Saved",
                "code": "extra_discount",
                "value": total_savings_amount,
                "currency": True,
                "visible": saving_amount_visible,
                "offer_text": ""
            },
            {
                "name": "Wallet Amount",
                "code": "wallet_amount",
                "value": str('-') + walletbalance,
                "currency": True,
                "visible": walletvisible,
                "offer_text": ""
            },
            {
                "name": "Discount (-)",
                "code": "discount",
                "value": floatingValue(discountamount),
                "currency": True,
                "visible": discountvisible,
                "offer_text": ""
            },
            {
                "name": "Shipping Charge ",
                "code": "shipping_charge",
                "value": floatingValue(total_delivery),
                "currency": True,
                "visible": True,
                "offer_text": ""
            }


        ]
        # New Changes
        itmtotalamount = float(totalAmountObj['value'])
        itmdiscountamount = float(round(check_total_savings_amount, 2))
        itmtotalamount = (float(itmtotalamount) - float(itmdiscountamount))

        # Grand Total
        rounded_value = round(grandtotal, 0) - grandtotal

        if(walletbalance != "0.00"):
            grandtotal = (round(grandtotal, 2) - float(walletbalance))

        # Check Partial Payment (New)
        partial_amount = (round(grandtotal, 2) * 20 / 100)
        partial_amount = round(partial_amount, 2)

        if(discountamount != 0):
            grandtotal = (float(round(grandtotal, 2)) - float(discountamount))
        grand_total = {
            "title": "Grand Total",
            "payment_method": 'COD',
            "discount_rate": floatingValue(discountrate),
            "discount_amount": floatingValue(discountamount),
            "round_off": floatingValue(round(rounded_value, 2)),

            "amount": floatingValue(round(grandtotal)),
            "partial": floatingValue(partial_amount),
        }

        # Shipping Address
        shipping_addresses = await AddressHelper.fetch_shipping_address_data(db=db, user_id=item.user_id)

        for address in shipping_addresses:
            alter_phone = ''
            if(address.alt_phone == None):
                address.alt_phone = alter_phone

            default_address = False
            if(address.default_address == 1):
                default_address = True

            address.default_address = default_address

        # Payment Method
        payment_method = []
        db_payment_options = db.query(PaymentOptionModel).group_by(
            PaymentOptionModel.prefix).order_by(PaymentOptionModel.id.asc()).all()
        # for paymentoptions in db_payment_options:
        #     body = ''
        #     title = ''
        #     if('ONLINE' in str(paymentoptions.prefix)):
        #         body = 'Use Netbanking, Debit Card or Credit Card'
        #         title = 'Pay Online'
        #     if('COD' in str(paymentoptions.prefix)):
        #         body = 'Pay by Cash when your product arrived'
        #         title = 'Cash on Delivery'
        #     if('PARTIAL' in str(paymentoptions.prefix)):
        #         body = 'Pay 20% Advance & 80% Cash on Delivery'
        #         title = 'Advance Payment'
        #     p = {
        #         'value': paymentoptions.prefix,
        #         'title': title,
        #         'body': body
        #     }
        #     payment_method.append(p)
        payment_method = [
            {
                'value': 'ONLINE',
                'title': 'Pay Online',
                'body': 'Use Netbanking, Debit Card or Credit Card'
            },
            {
                'value': 'PARTIAL',  # Partial Payment (New)
                'title': 'Advance Payment',
                'body': 'Pay 20% Advance & 80% Cash on Delivery'
            },
            {
                'value': 'COD',
                'title': 'Cash on Delivery',
                'body': 'Pay by Cash when your product arrives'
            }

        ]

        payment_methods = []
        for payment in payment_method:
            title = payment['title']
            body = payment['body']

            # CHECK ORDER DISCOUNT
            # checkoffer = db.execute("SELECT id, discount, start_date, discount_text, payment_method FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
            #                         {"param": today}).first()
            checkoffer = db.execute(
                "SELECT * FROM `payment_options` WHERE payment_options.prefix ='ONLINE' AND payment_type = 'UPI' AND (CASE WHEN payment_options.start_date is not NULL THEN payment_options.end_date >=:current_date AND payment_options.start_date <=:current_date ELSE payment_options.start_date is NULL AND payment_options.payment_type != (SELECT PO.payment_type FROM payment_options as PO WHERE PO.prefix = 'ONLINE' AND PO.end_date >=current_date AND PO.start_date <=current_date) END)", {
                    'current_date': current_datetime
                }).first()
            if(checkoffer is None):
                checkoffer = db.execute(
                    "SELECT * FROM payment_options WHERE payment_options.prefix ='ONLINE' AND payment_options.payment_type = 'UPI' AND payment_options.start_date is NULL").first()
            offer = ''
            discount_text = ''
            discount = 0
            # if(checkoffer.discount != 0 and checkoffer.prefix == payment['value']):
            #     offer = ""
            #     fractional, whole = math.modf(checkoffer.discount)
            #     if(fractional == 0):
            #         discount = round(checkoffer.discount)
            #     else:
            #         discount = round(checkoffer.discount, 2)

            # if(checkoffer.discount_text is not None):
            #     # discount_text = checkoffer.discount_text
            #     offerTxt = checkoffer.discount_text
            #     discount_text = ''
            # else:

            #     discount_text = ''

            # Footer Text Changes Rahul

            # Changes rahul (Will be change in future)
            # Check Shipping Charge
            check_shipping_charge = db.execute("SELECT id, rate, payment_mode, shipping_text, start_date, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                               {"param": today}).first()
            if(check_shipping_charge.shipping_text is not None and check_shipping_charge.rate == 0):
                offerTxt = check_shipping_charge.shipping_text
            if(float(check_shipping_charge.rate) != 0 and float(check_shipping_charge.order_limit) != 0 and float(checkoffer.discount) != 0):
                offerTxt = '₹0 shipping on ₹' + \
                    str(check_shipping_charge.order_limit) + ' & ' + \
                    str(round(checkoffer.discount)) + '% off on Online Payment'
                if(checkoffer.limit_amount > 0):
                    offerTxt = 'Get ' +str(round(checkoffer.discount)) + '% off on Online Payment. Min Order: ₹'+str(int(checkoffer.limit_amount))
            # Changes Rahul (Will be changed after 19th April)
            # if(payment['value'] == 'ONLINE' and checkoffer.discount == 0 and check_shipping_charge.payment_mode == 'COD'):
            #     if(checkoffer.discount_text is not None):
            #         # offer = checkoffer.discount_text
            #         offerTxt = checkoffer.discount_text
            #         offer = ''
            #     else:
            #         offer = ''
            #         offerTxt = ''

            visible = True
            disabled = False

            if(payment['value'] == 'COD'):

                discount = 0
                facility = await Facility.check_buyer_cod_limit(db=db, user_id=item.user_id)

                if(facility):
                    facility_date = facility.updated_at.strftime(
                        "%Y-%m-%d")

                    orders = await OrderHelper.check_buyer_cod_orders(db=db, user_id=userdata['id'], date=facility_date)

                    restAmount = 0
                    if(len(orders) > 0):
                        previews_total_amount = 0
                        for order in orders:
                            previews_total_amount += await OrderCalculation.calculateTotalAmount(db=db,
                                                                                                 orderID=order.id)

                        previews_total_amount = roundOf(
                            previews_total_amount)

                        if(int(facility.key_value) > previews_total_amount):

                            restAmount = (int(facility.key_value)
                                          ) - previews_total_amount

                        else:

                            restAmount = 0

                        restAmount = roundOf(restAmount)

                        if(restAmount != 0):
                            if (round(grandtotal) > restAmount):
                                disabled = True
                                # title = "Remaining COD limit"
                                title = "Cash on Delivery"
                                body = 'Remaining limit ₹' + str(restAmount) + \
                                    " out of ₹" + \
                                    str(facility.key_value)
                            else:
                                disabled = False
                                # title = "Remaining COD limit"
                                title = "Cash on Delivery"
                                body = 'Remaining limit ₹' + str(restAmount) + \
                                    " out of ₹" + \
                                    str(facility.key_value)
                        else:
                            disabled = True
                            # title = "Remaining COD limit"
                            title = "Cash on Delivery"
                            body = 'Remaining limit ₹' + str(restAmount) + \
                                " out of ₹" + \
                                str(facility.key_value)

                    else:
                        if(round(grandtotal) > int(facility.key_value)):
                            disabled = True
                            # title = "Remaining COD limit"
                            title = "Cash on Delivery"
                            body = 'Remaining limit ₹' + str(restAmount) + \
                                " out of ₹" + \
                                str(facility.key_value)
                        else:
                            disabled = False
                            # title = "Remaining COD limit"
                            title = "Cash on Delivery"
                            body = 'Remaining limit ₹' + str(restAmount) + \
                                " out of ₹" + \
                                str(facility.key_value)

                    if(facility.key_value == '0'):
                        disabled = True
                        # title = "Remaining COD limit"
                        title = "Cash on Delivery"
                        body = 'Remaining limit ₹' + str(restAmount) + \
                            " out of ₹" + \
                            str(facility.key_value)

                if(checkTotalAmountforSellerCodLimit != ''):
                    disabled = True
                    # title = 'COD Limit'
                    title = 'Cash on Delivery'
                    body = checkTotalAmountforSellerCodLimit
            # Partial Paymeny Option (New)
            if(payment['value'] == 'PARTIAL'):
                visible = True
                disabled = False

            pmethods = {
                'value': payment['value'],
                'title': title,
                'description': body,
                'visible': visible,
                'disabled': disabled,
                'discount': discount,
                'discount_text': discount_text,
                'offer': offer

            }

            payment_methods.append(pmethods)

        # User Document Type
        user_document_type = []
        documents = [{
            'name': 'GSTIN',
                    'value': '',
                    'selected': False
        },
            {
            'name': 'AADHAAR',
                    'value': '',
                    'selected': False
        },
            {
            'name': 'PAN',
                    'value': '',
                    'selected': False
        }]

        for document in documents:

            checkdocument = await fetch_user_profile_data(db=db, user_id=item.user_id)

            selected = False
            value = ''
            if(checkdocument and checkdocument.proof_type == document['name']):
                selected = True
                value = checkdocument.proof

            doc = {
                'name': document['name'],
                'value': value,
                'selected': selected
            }

            user_document_type.append(doc)

        # Check buyer facility
        if(buyer_facility and buyer_orderdiscount_facility):
            freedeliveryamount = int(buyer_facility.key_value)
            discountfacilty = int(buyer_orderdiscount_facility.key_value)

            if(freedeliveryamount != 0):

                offerTxt = '₹0 shipping on ₹' + \
                    str(freedeliveryamount) + ' & ' + \
                    str(discountfacilty) + '% off on Online Payment'
            else:
                offerTxt = 'Free Shipping' + ' & ' + \
                    str(discountfacilty) + '% off on Online Payment'

        # Check Discount
        elif(buyer_orderdiscount_facility):
            discountfacilty = int(buyer_orderdiscount_facility.key_value)
            if (shipping_charge.order_limit != 0):
                if (round(total_amount, 2) > round(shipping_charge.order_limit, 2)):
                    # Shipping rate
                    delivery_charge = shipping_charge.rate
                    # Minimum order amount
                    min_order_value = shipping_charge.order_limit
                    # minimum order check text
                    if(shipping_charge.shipping_text is not None):
                        offerTxt = '₹0 shipping on ₹' + \
                            str(min_order_value) + ' & ' + \
                            str(discountfacilty) + '% off on Online Payment'
                    else:
                        offerTxt = ''
            else:
                delivery_charge = shipping_charge.rate
                # minimum order check text
                if(shipping_charge.rate == 0):
                    offerTxt = 'Free Shipping' + ' & ' + \
                        str(discountfacilty) + '% off on Online Payment'
                else:
                    offerTxt = ''

        elif(buyer_facility):
            freedeliveryamount = int(buyer_facility.key_value)
            fractional, whole = math.modf(checkoffer.discount)
            if(fractional == 0):
                discount = round(checkoffer.discount)
            else:
                discount = round(checkoffer.discount, 2)

            if(freedeliveryamount != 0):
                offerTxt = '₹0 shipping on ₹' + \
                    str(freedeliveryamount) + ' & ' + \
                    str(discount) + '% off on Online Payment'
            else:
                offerTxt = 'Free Delivery' + ' & ' + \
                    str(discount) + '% off on Online Payment'
        # get_rewards = (float(round(itmtotalamount, 2))
        #                * float(REWARDS_PERCENTAGE)) / 100
        get_rewards = calculatewallet_amount
        return {'status_code': HTTP_200_OK, "wallet_balance": wallet_balance, 'offer_text': offerTxt, 'get_rewards': str(get_rewards), 'item_total_amount': str(calculatewallet_amount), "total_savings_amount": total_savings_amount, 'shipping_address': shipping_addresses, 'user_documents': user_document_type,  'payment_method': payment_methods, 'order_summary': order_summary, "grand_total": grand_total, 'item_data': finalcartitems, "place_order_btn": place_order_btn}

    except Exception as e:
        print(e)
        return {'status_code': HTTP_400_BAD_REQUEST}


@v4_checkout_router.post("/payment/option", dependencies=[Depends(JWTBearer())])
async def onlinepaymentoption(request: Request, paydata: paymentoption, db: Session = Depends(get_db)):
    try:
        finalcartitems = []  # New Updated
        userdata = auth(request=request)

        # Check Delivery Charger for new user
        new_free_delivery = False
        if(config.FREE_DELIVERY == "True"):
            check_order_exist = await OrderHelper.check_order_exist(db=db, user_id=userdata['id'])
            new_free_delivery = check_order_exist

        offer_Txt = ''

        # Check User facility
        buyer_facility = await Facility.buyer_facility(db=db, user_id=userdata['id'])
        # Check Discount Facility
        buyer_orderdiscount_facility = await Facility.buyer_discount_facility(db=db, user_id=userdata['id'])

        # TEMPORARY CHANGES
        current_datetime = datetime.now()
        current_datetime = current_datetime.strftime("%Y-%m-%d %H:%M:%S")
        db_check_payment_options = db.execute(
            "SELECT * FROM `payment_options` WHERE payment_options.prefix =:prefix AND (CASE WHEN payment_options.start_date is not NULL THEN payment_options.end_date >=:current_date AND payment_options.start_date <=:current_date ELSE payment_options.start_date is NULL AND payment_options.payment_type != (SELECT PO.payment_type FROM payment_options as PO WHERE PO.prefix =:prefix AND PO.end_date >=:current_date AND PO.start_date <=:current_date) END)", {
                "prefix": paydata.payment_type,
                'current_date': current_datetime
            }).all()
        if(len(db_check_payment_options) == 0):
            db_check_payment_options = db.execute("SELECT * FROM payment_options WHERE payment_options.prefix =:prefix AND payment_options.start_date is NULL", {
                'prefix': paydata.payment_type
            }).all()
        ChoosePaymentOption = []
        # for payment in db_check_payment_options:
        #     offet_text = ''
        #     discount = round(payment.discount)
        #     if(buyer_orderdiscount_facility):
        #         discount = int(buyer_orderdiscount_facility.key_value)

        #     if(payment.discount != 0):
        #         offet_text = 'Get ' + \
        #             str(round(discount))+'% Instant Discount'
        #     p = {
        #         'value': discount,
        #         'title': payment.title,
        #         'payment_type': payment.payment_type,
        #         'desciption': payment.description,
        #         'offer_text': offet_text
        #     }
        #     ChoosePaymentOption.append(p)

        # discoun_rate = 0
        # for paymentoption in ChoosePaymentOption:
        #     if(paymentoption['payment_type'].lower() == paydata.payment_key.lower()):
        #         discoun_rate = paymentoption['value']
        total_amount = 0
        # Total Amount Without Discount
        total_amount_without_discount = 0
        total_delivery = 0
        grand_total = 0.00
        if(paydata.payment_type.lower() == 'online'):
            title = 'Online Payment'
        else:
            title = 'Partial Payment'
        cartitems = []

        data = await CartHelper.cart_data_on_summary_page(db=db, user_id=userdata['id'])

        if(len(data) == 0):
            return {'status_code': HTTP_200_OK, "message": "No data found", "title": title, 'order_summary_text': [], "grand_total": {}}

        # New changes for partial payment
        if(paydata.payment_type.lower() != 'cod' and paydata.payment_type.lower() != 'partial'):

            finalcartdata = []
            sellers_detail = []
            for cartitems in data:
                in_stock_items = await CartHelper.in_stock_cart_items_on_summary_page(db=db, seller_id=cartitems.seller_id, user_id=userdata['id'])

                cartdata = []
                if(len(in_stock_items) > 0):
                    for cartitem in in_stock_items:
                        cartdata.append(cartitem)

                    finalcartdata.append(cartdata)

            for cartitemdata in finalcartdata:
                sub_total = 0
                # Sub Total With Discount
                sub_total_with_discount = 0
                # Sub Total Without Discount
                sub_total_without_discount = 0
                product_data = []
                # Get Seller Data
                seller = await fetch_seller_data(db=db, seller_id=cartitemdata[0].seller_id)

                for item in cartitemdata:

                    # Get Single Cart Data
                    singlecartdata = await CartHelper.single_cart_data(db=db, user_id=item.user_id, product_id=item.product_id)

                    # Get cart items by product id
                    itemsdata = db.query(CartModel).join(
                        ProductModel, ProductModel.id == CartModel.product_id).filter(CartModel.user_id == item.user_id).filter(CartModel.product_id == item.product_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).all()

                    # Get Pricing ID
                    itm_pricing_id = singlecartdata.uuid.split('-')
                    # Get Pricing
                    itm_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=itm_pricing_id[1])

                    if(itm_pricing):
                        total_items = []  # New Updated
                        for items in itemsdata:
                            # Get Product Detail
                            productdata = await ProductsHelper.fetch_product_data(db=db, product_id=items.product_id)

                            # Pricing ID
                            item_pricing_id = items.uuid.split('-')

                            # Get Pricing
                            item_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=item_pricing_id[1])

                            # Check Pricing Ecist
                            if(item_pricing):
                                # Inventory
                                checkiteminventory = await ProductsHelper.fetch_pricing_inventory(db=db, pricing_id=item_pricing.id)

                                # Check Product Stock
                                if(checkiteminventory.out_of_stock == 0):
                                    # Aseztak Service
                                    # aseztak_service = Services.aseztak_services(
                                    #     item_pricing.updated_at, db=db)
                                    # today_date = item_pricing.updated_at.strftime(
                                    #     '%Y-%m-%d')
                                    todaydate = date.today()
                                    today_date = todaydate.strftime('%Y-%m-%d')
                                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                                    # Calculate Product Pricing
                                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                    # Product Price with discount
                                    product_price_with_discount: ProductModel = await ProductsHelper.chekcOutPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                    # Product Price without discount
                                    product_price_without_discount = productPricecalculation(price=item_pricing.price, tax=item_pricing.tax, commission=aseztak_service.rate,
                                                                                             gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                                    # Sub Total
                                    totalprice = round(
                                        product_price, 2) * items.quantity
                                    # Total Price with Discount
                                    totalprice_with_discount = round(
                                        product_price_with_discount, 2) * items.quantity
                                    # Total Price without Discount
                                    totalprice_without_discount = round(
                                        product_price_without_discount, 2) * items.quantity
                                    sub_total += totalprice
                                    # Sub Total without discount
                                    sub_total_with_discount += totalprice_with_discount
                                    # Sub Total without discount
                                    sub_total_without_discount += totalprice_without_discount
                                    total_items.append(items)

                        # New Updated
                        p_data = {
                            'items': len(total_items),
                        }
                        product_data.append(p_data)

                # Today Date
                today = date.today()
                # YY-mm-dd
                today = today.strftime("%Y-%m-%d")

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscount(db=db, seller_id=seller.id, today_date=today, order_amount=float(round(sub_total, 2)))
                if(check_seller_discount != 0):
                    sub_total = (float(round(sub_total_with_discount, 2)))
                # Check Shipping Charge
                delivery_charge = 0
                if(sub_total != 0 and new_free_delivery == False):
                    shipping_charge = db.execute("SELECT id, rate, start_date, payment_mode, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                                 {"param": today}).first()

                    if(buyer_facility):
                        freedeliveryamont = int(buyer_facility.key_value)
                        if(round(sub_total, 2) < round(freedeliveryamont, 2)):
                            if(int(buyer_facility.key_value) != 0):
                                delivery_charge = shipping_charge.rate
                    else:
                        delivery_charge = checkOrderPlaceDeliveryCalculation(
                            order_amount=sub_total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode='ONLINE')

                # if(new_free_delivery == False):
                # Check Seller Purchase Limit New Updated
                showing_seller_product = True
                check_seller_facility = await ProductsHelper.check_seller_facility(db=db, user_id=cartitemdata[0].seller_id, type='purchase_limit')
                if(check_seller_facility is not None):
                    if(float(check_seller_facility.key_value) <= float(round(sub_total, 2))):
                        total_amount += round(sub_total, 2)
                        total_amount_without_discount += round(
                            sub_total_without_discount, 2)
                        total_delivery += delivery_charge
                    else:
                        showing_seller_product = False
                else:
                    total_amount += round(sub_total, 2)
                    # Total Amount without discount
                    total_amount_without_discount += round(
                        sub_total_without_discount, 2)

                    total_delivery += delivery_charge
                # else:
                #     showing_seller_product = True
                #     total_amount += round(sub_total, 2)
                #     # Total Amount without discount
                #     total_amount_without_discount += round(
                #         sub_total_without_discount, 2)

                #     total_delivery += delivery_charge
                # New Updated
                seller = {
                    'id': seller.id,
                    'name': seller.name,
                    'visible': showing_seller_product
                }
                sellers_detail.append(seller)
                finalcartitems.append(
                    {'showing_product': showing_seller_product, 'product_data': product_data})

            discountamount = 0.00
            discountrate = 0.00
            offerTxt = ""
            # Check savings amount
            check_savings_amount = (
                float(total_amount_without_discount) - float(total_amount))
            if(check_savings_amount > 0):
                savings_amount = '- ₹'+str(round(check_savings_amount, 2))
                savings_visible = True
                totalAmountObj = {
                    "name": "Total Amount",
                    "value": floatingValue(round(total_amount_without_discount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }
            else:
                totalAmountObj = {
                    "name": "Total Amount",
                    "value": floatingValue(round(total_amount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }
                savings_amount = floatingValue(0)
                savings_visible = False
            # New Changes Wallet
            if(paydata.wallet_amount != "0.00"):
                wallet_amount = (float(totalAmountObj['value']) - float(
                    round(check_savings_amount, 2))) - float(paydata.wallet_amount)
            else:
                wallet_amount = total_amount

            #New Update calculate item total amount with discount limit amount (2024)
            for payment in db_check_payment_options:
                offet_text = ''
                # discount = round(payment.discount)
                if(payment.limit_amount > 0 and float(totalAmountObj['value']) > float(payment.limit_amount)):
                    discount = round(payment.discount)
                if(payment.limit_amount == 0):
                    discount = round(payment.discount)
                if(buyer_orderdiscount_facility):
                    discount = int(buyer_orderdiscount_facility.key_value)

                if(discount != 0):
                    offet_text = 'Get ' + \
                        str(round(discount))+'% Instant Discount'
                p = {
                    'value': discount,
                    'title': payment.title,
                    'payment_type': payment.payment_type,
                    'desciption': payment.description,
                    'offer_text': offet_text
                }
                ChoosePaymentOption.append(p)

            discoun_rate = 0
            for paymentoption in ChoosePaymentOption:
                if(paymentoption['payment_type'].lower() == paydata.payment_key.lower()):
                    discoun_rate = paymentoption['value']    

            if(discoun_rate != 0):
                dis = (float(round(wallet_amount, 2)) *
                       float(discoun_rate) / 100)
                discountamount += dis

                discountrate = discoun_rate

                offerTxt = 'Get ₹' + \
                    str(round(discountamount, 2))+' instant discount'

            if(paydata.payment_key.lower() == 'online'):
                grand_totla = round(wallet_amount, 2)
            else:
                grand_totla = round(wallet_amount, 2) - \
                    round(discountamount, 2)

            walletvisible = False
            if(paydata.wallet_amount != "0.00"):
                walletvisible = True

            visible = True
            if(paydata.payment_key.lower() == 'online'):
                visible = False
            # NEW Total Cart Items
            final_count_total_items = 0
            if(len(finalcartitems) > 0):
                for total_cart_item in finalcartitems:
                    if(total_cart_item['showing_product'] == True):
                        for fnal_product in total_cart_item['product_data']:
                            final_count_total_items += fnal_product['items']

            if(paydata.payment_type.lower() != 'cod'):

                order_summary = [
                    {
                        "name": "Items",
                        "code": "items",
                        "value": str(final_count_total_items),  # New Updated
                        "currency": False,
                        "visible": True,
                        "offer_text": ""
                    },
                    totalAmountObj,
                    {
                        "name": "Extra Discount",
                        "code": "extra_discount",
                        "value": savings_amount,
                        "currency": True,
                        "visible": savings_visible,
                        "offer_text": ""
                    },
                    {
                        "name": "Wallet Amount ",
                        "code": "wallet_amount",
                        "value": str('-') + paydata.wallet_amount,
                        "currency": True,
                        "visible": walletvisible,
                        "offer_text": ""
                    },
                    {
                        "name": "Discount (-)",
                        "code": "discount",
                        "value": floatingValue(round(discountamount, 2)),
                        "currency": True,
                        "visible": visible,
                        "offer_text": offerTxt
                    },
                    {
                        "name": "Shipping Charge ",
                        "code": "shipping_charge",
                        "value": floatingValue(total_delivery),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    }




                ]
            else:
                order_summary = [
                    {
                        "name": "Items",
                        "code": "items",
                        # New Updated
                        "value": floatingValue(final_count_total_items),
                        "currency": False,
                        "visible": True,
                        "offer_text": ""
                    },
                    totalAmountObj,
                    {
                        "name": "Shipping Charge ",
                        "code": "shipping_charge",
                        "value": floatingValue(total_delivery),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    },
                    {
                        "name": "Discount",
                        "code": "discount",
                        "value": floatingValue(round(discountamount)),
                        "currency": True,
                        "visible": False,
                        "offer_text": offerTxt
                    },

                    {
                        "name": "Extra Discount",
                        "code": "extra_discount",
                        "value": savings_amount,
                        "currency": True,
                        "visible": savings_visible,
                        "offer_text": ""
                    },
                    {
                        "name": "Wallet Amount ",
                        "code": "wallet_amount",
                        "value": str('-') + paydata.wallet_amount,
                        "currency": True,
                        "visible": walletvisible,
                        "offer_text": ""
                    },
                    {
                        "name": "Payable Amount",
                        "code": "payable_amount",
                        "value": floatingValue(round(total_amount, 0)),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    }

                ]

            # Grand Total
            grand_total = float(round(grand_totla, 2)) + \
                float(round(total_delivery, 0))

            rounded_value = round(grand_total, 0) - grand_total

            # if(walletbalance != "0.00"):
            #     grand_total = (round(grand_total, 2) - float(walletbalance))

            grand_total = {
                "title": "Grand Total",
                "payment_method": paydata.payment_type.upper(),
                "discount_rate": floatingValue(discountrate),
                "discount_amount": floatingValue(discountamount),
                "round_off": floatingValue(round(rounded_value, 2)),
                "amount": floatingValue(round(grand_total)),
                "partial": floatingValue(0.00),

            }

            return {'status_code': HTTP_200_OK, "message": "Success", "title": title, 'online_payment_options': ChoosePaymentOption, 'order_summary': order_summary, "grand_total": grand_total, "sellers_detail": sellers_detail}
        elif(paydata.payment_type.lower() == 'partial'):  # New changes for partial payment
            finalcartdata = []
            sellers_detail = []
            for cartitems in data:
                in_stock_items = await CartHelper.in_stock_cart_items_on_summary_page(db=db, seller_id=cartitems.seller_id, user_id=userdata['id'])

                cartdata = []
                if(len(in_stock_items) > 0):
                    for cartitem in in_stock_items:
                        cartdata.append(cartitem)

                    finalcartdata.append(cartdata)

            for cartitemdata in finalcartdata:
                sub_total = 0
                # Sub Total With Discount
                sub_total_with_discount = 0
                # Sub Total Without Discount
                sub_total_without_discount = 0
                product_data = []
                # Get Seller Data
                seller = await fetch_seller_data(db=db, seller_id=cartitemdata[0].seller_id)

                for item in cartitemdata:

                    # Get Single Cart Data
                    singlecartdata = await CartHelper.single_cart_data(db=db, user_id=item.user_id, product_id=item.product_id)

                    # Get cart items by product id
                    itemsdata = db.query(CartModel).join(
                        ProductModel, ProductModel.id == CartModel.product_id).filter(CartModel.user_id == item.user_id).filter(CartModel.product_id == item.product_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).all()

                    # Get Pricing ID
                    itm_pricing_id = singlecartdata.uuid.split('-')
                    # Get Pricing
                    itm_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=itm_pricing_id[1])

                    if(itm_pricing):
                        total_items = []  # New Updated
                        for items in itemsdata:
                            # Get Product Detail
                            productdata = await ProductsHelper.fetch_product_data(db=db, product_id=items.product_id)

                            # Pricing ID
                            item_pricing_id = items.uuid.split('-')

                            # Get Pricing
                            item_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=item_pricing_id[1])

                            # Check Pricing Ecist
                            if(item_pricing):
                                # Inventory
                                checkiteminventory = await ProductsHelper.fetch_pricing_inventory(db=db, pricing_id=item_pricing.id)

                                # Check Product Stock
                                if(checkiteminventory.out_of_stock == 0):
                                    # Aseztak Service
                                    # aseztak_service = Services.aseztak_services(
                                    #     item_pricing.updated_at, db=db)
                                    # today_date = item_pricing.updated_at.strftime(
                                    #     '%Y-%m-%d')
                                    todaydate = date.today()
                                    today_date = todaydate.strftime('%Y-%m-%d')
                                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                                    # Calculate Product Pricing
                                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                    # Product Price with discount
                                    product_price_with_discount: ProductModel = await ProductsHelper.chekcOutPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')

                                    # Product Price without discount
                                    product_price_without_discount = productPricecalculation(price=item_pricing.price, tax=item_pricing.tax, commission=aseztak_service.rate,
                                                                                             gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                                    # Sub Total
                                    totalprice = round(
                                        product_price, 2) * items.quantity
                                    # Total Price with Discount
                                    totalprice_with_discount = round(
                                        product_price_with_discount, 2) * items.quantity
                                    # Total Price without Discount
                                    totalprice_without_discount = round(
                                        product_price_without_discount, 2) * items.quantity
                                    sub_total += totalprice
                                    # Sub Total with discount
                                    sub_total_with_discount += totalprice_with_discount
                                    # Sub Total without discount
                                    sub_total_without_discount += totalprice_without_discount
                                    total_items.append(items)

                        # New Updated
                        p_data = {
                            'items': len(total_items),
                        }
                        product_data.append(p_data)

                # Today Date
                today = date.today()
                # YY-mm-dd
                today = today.strftime("%Y-%m-%d")

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscount(db=db, seller_id=seller.id, today_date=today, order_amount=float(round(sub_total, 2)))
                if(check_seller_discount != 0):
                    sub_total = (float(round(sub_total_with_discount, 2)))

                # Check Shipping Charge
                delivery_charge = 0
                if(sub_total != 0 and new_free_delivery == False):
                    shipping_charge = db.execute("SELECT id, rate, start_date, payment_mode, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                                 {"param": today}).first()

                    if(buyer_facility):
                        freedeliveryamont = int(buyer_facility.key_value)
                        if(round(sub_total, 2) < round(freedeliveryamont, 2)):
                            if(int(buyer_facility.key_value) != 0):
                                delivery_charge = shipping_charge.rate
                    else:
                        delivery_charge = checkOrderPlaceDeliveryCalculation(
                            order_amount=sub_total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode='ONLINE')
                # if(new_free_delivery == False):
                # Check Seller Purchase Limit New Updated
                showing_seller_product = True
                check_seller_facility = await ProductsHelper.check_seller_facility(db=db, user_id=cartitemdata[0].seller_id, type='purchase_limit')
                if(check_seller_facility is not None):
                    if(float(check_seller_facility.key_value) <= float(round(sub_total, 2))):
                        total_amount += round(sub_total, 2)
                        total_amount_without_discount += round(
                            sub_total_without_discount, 2)
                        total_delivery += delivery_charge
                    else:
                        showing_seller_product = False
                else:
                    total_amount += round(sub_total, 2)
                    # Total Amount without discount
                    total_amount_without_discount += round(
                        sub_total_without_discount, 2)

                    total_delivery += delivery_charge
                # else:
                #     showing_seller_product = True
                #     total_amount += round(sub_total, 2)
                #     # Total Amount without discount
                #     total_amount_without_discount += round(
                #         sub_total_without_discount, 2)

                #     total_delivery += delivery_charge
                # New Updated
                seller = {
                    'id': seller.id,
                    'name': seller.name,
                    'visible': showing_seller_product
                }
                sellers_detail.append(seller)
                finalcartitems.append(
                    {'showing_product': showing_seller_product, 'product_data': product_data})

            discountamount = 0.00
            discountrate = 0.00
            offerTxt = ""
            # Check savings amount
            check_savings_amount = (
                float(total_amount_without_discount) - float(total_amount))
            if(check_savings_amount > 0):
                savings_amount = '- ₹'+str(round(check_savings_amount, 2))
                savings_visible = True
                totalAmountObj = {
                    "name": "Total Amount",
                    "value": floatingValue(round(total_amount_without_discount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }
            else:
                totalAmountObj = {
                    "name": "Total Amount",
                    "value": floatingValue(round(total_amount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }
                savings_amount = floatingValue(0)
                savings_visible = False
            # New Changes Wallet
            if(paydata.wallet_amount != "0.00"):
                wallet_amount = (float(totalAmountObj['value']) - float(
                    round(check_savings_amount, 2))) - float(paydata.wallet_amount)
            else:
                wallet_amount = total_amount

            #New Update calculate item total amount with discount limit amount (2024)
            for payment in db_check_payment_options:
                offet_text = ''
                # discount = round(payment.discount)
                if(payment.limit_amount > 0 and float(totalAmountObj['value']) > float(payment.limit_amount)):
                    discount = round(payment.discount)
                if(payment.limit_amount == 0):
                    discount = round(payment.discount)
                if(buyer_orderdiscount_facility):
                    discount = int(buyer_orderdiscount_facility.key_value)

                if(discount != 0):
                    offet_text = 'Get ' + \
                        str(round(discount))+'% Instant Discount'
                p = {
                    'value': discount,
                    'title': payment.title,
                    'payment_type': payment.payment_type,
                    'desciption': payment.description,
                    'offer_text': offet_text
                }
                ChoosePaymentOption.append(p)

            discoun_rate = 0
            for paymentoption in ChoosePaymentOption:
                if(paymentoption['payment_type'].lower() == paydata.payment_key.lower()):
                    discoun_rate = paymentoption['value']  


            walletvisible = False
            if(paydata.wallet_amount != "0.00"):
                walletvisible = True

            if(discoun_rate != 0):
                dis = (float(round(wallet_amount, 2)) *
                       float(discoun_rate) / 100)
                discountamount += dis

                discountrate = discoun_rate

                offerTxt = 'Get ₹' + \
                    str(round(discountamount, 2))+' instant discount'

            if(paydata.payment_key.lower() == 'partial'):
                grand_totla = round(wallet_amount, 2)
            else:
                grand_totla = round(wallet_amount, 2) - \
                    round(discountamount, 2)

            walletvisible = False
            if(paydata.wallet_amount != "0.00"):
                walletvisible = True

            visible = True
            if(paydata.payment_key.lower() == 'partial'):
                visible = False
            # NEW Total Cart Items
            final_count_total_items = 0
            if(len(finalcartitems) > 0):
                for total_cart_item in finalcartitems:
                    if(total_cart_item['showing_product'] == True):
                        for fnal_product in total_cart_item['product_data']:
                            final_count_total_items += fnal_product['items']

            # Grand Total
            grand_total = float(round(grand_totla, 2)) + \
                float(round(total_delivery, 0))
            # if(walletbalance != "0.00"):
            #     grand_total = (round(grand_total, 2) - float(walletbalance))

            # new changes for partial payment
            partial_amount = (round(grand_total, 2) * 20 / 100)
            partial_amount = round(partial_amount, 2)
            cod_amount = (round(grand_total, 2) - partial_amount)

            visible = False
            if(discountamount != 0):
                visible = True

            order_summary = [
                {
                    "name": "Items",
                    "code": "items",
                    # New Updated
                    "value": floatingValue(final_count_total_items),
                    "currency": False,
                    "visible": True,
                    "offer_text": ""
                },
                totalAmountObj,
                {
                    "name": "Extra Discount",
                    "code": "extra_discount",
                    "value": savings_amount,
                    "currency": True,
                    "visible": savings_visible,
                    "offer_text": ""
                },
                {
                    "name": "Wallet Amount",
                    "code": "wallet_amount",
                    "value": str('-') + paydata.wallet_amount,
                    "currency": True,
                    "visible": walletvisible,
                    "offer_text": ""
                },
                {
                    "name": "Discount (-)",
                    "code": "discount",
                    "value": floatingValue(round(discountamount, 2)),
                    "currency": True,
                    "visible": visible,
                    "offer_text": offerTxt
                },
                {
                    "name": "Shipping Charge ",
                    "code": "shipping_charge",
                    "value": floatingValue(total_delivery),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                },
                {
                    "name": "COD Amount",
                    "code": "cod_amount",
                    "value": floatingValue(round(cod_amount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                },

                {
                    "name": "Advance Payment",
                    "code": "payable_amount",
                    "value": floatingValue(round(partial_amount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }

            ]

            rounded_value = round(grand_total, 0) - grand_total

            grand_total = {
                "title": "Grand Total",
                "payment_method": paydata.payment_type.upper(),
                "discount_rate": floatingValue(discountrate),
                "discount_amount": floatingValue(discountamount),
                "round_off": floatingValue(round(rounded_value, 2)),
                "amount": floatingValue(round(grand_total)),
                "partial": floatingValue(round(partial_amount, 2)),

            }

            return {'status_code': HTTP_200_OK, "message": "Success", "title": title, 'online_payment_options': ChoosePaymentOption, 'order_summary': order_summary, "grand_total": grand_total, "sellers_detail": sellers_detail}

        # cartitems: CartModel = await CartHelper.orderSummary(request=request, db=db, data=data, payment_method=payment_method)
        # return cartitems
        else:
            return {'status_code': HTTP_200_OK, "message": "No data found", "payment_method": paydata.payment_type, 'order_summary_text': [], "grand_total": {}}

    except Exception as e:
        print(e)


@v4_checkout_router.get("/new/online/payment/option/{payment_method}/{walletbalance}", dependencies=[Depends(JWTBearer())])
async def checkOut(request: Request, payment_method: str, walletbalance: Optional[str] = '0.00', db: Session = Depends(get_db)):
    try:
        finalcartitems = []  # New Updated
        userdata = auth(request=request)

        # Check Delivery Charger for new user
        new_free_delivery = False
        if(config.FREE_DELIVERY == "True"):
            check_order_exist = await OrderHelper.check_order_exist(db=db, user_id=userdata['id'])
            new_free_delivery = check_order_exist

        offer_Txt = ''

        # Check User facility
        buyer_facility = await Facility.buyer_facility(db=db, user_id=userdata['id'])
        # Check Discount Facility
        buyer_orderdiscount_facility = await Facility.buyer_discount_facility(db=db, user_id=userdata['id'])

        # TEMPORARY CHANGES
        todaydate = datetime.now()
        today = todaydate.strftime("%Y-%m-%d")
        # Offer 08th to 14th Ocotber\
        if(buyer_orderdiscount_facility):
            offervalue = int(buyer_orderdiscount_facility.key_value)

        else:
            offervalue = 2
        if(payment_method.lower() == 'partial'):  # New Changes for Partial payment
            ChoosePaymentOption = [
                {

                    'value': 0,
                    'title': 'Debit Card',
                    'payment_type': 'DEBIT_CARD',
                    'description': 'Master Card, Rupay, Visa, American Express',
                    'offer_text': '',

                },
                {

                    'value': 0,
                    'title': 'Credit Card',
                    'payment_type': 'CREDIT_CARD',
                    'description': 'Master Card, Rupay, Visa, American Express',
                    'offer_text': '',

                },
                {

                    'value': 0,
                    'title': 'UPI',
                    'payment_type': 'UPI',
                    'description': 'GPay, PhonePe, Paytm etc.',
                    'offer_text': ''
                },
                {

                    'value': 0,
                    'title': 'Paytm Wallet',
                    'payment_type': 'BALANCE',
                    'description': 'Pay using paytm wallet',
                    'offer_text': ''
                },
                {

                    'value': 0,
                    'title': 'Net Banking',
                    'payment_type': 'NET_BANKING',
                    'description': 'HDFC Bank, ICICI Bank, State Bank of India, Axis',
                    'offer_text': ''
                }
            ]

        else:
            if(today > '2023-04-06' and today < '2023-04-13'):  # new Offer

                ChoosePaymentOption = [
                    {

                        'value': 2,
                        'title': 'Debit Card',
                        'payment_type': 'DEBIT_CARD',
                        'description': 'Master Card, Rupay, Visa, American Express',
                        'offer_text': 'Get 2% Instant Discount',

                    },
                    {

                        'value': 0,
                        'title': 'Credit Card',
                        'payment_type': 'CREDIT_CARD',
                        'description': 'Master Card, Rupay, Visa, American Express',
                        'offer_text': '',

                    },
                    {

                        'value': 5,
                        'title': 'UPI',
                        'payment_type': 'UPI',
                        'description': 'GPay, PhonePe, Paytm etc.',
                        'offer_text': 'Get 5% Instant Discount'
                    },
                    {

                        'value': 2,
                        'title': 'Paytm Wallet',
                        'payment_type': 'BALANCE',
                        'description': 'Pay using paytm wallet',
                        'offer_text': 'Get 2% Instant Discount'
                    },
                    {

                        'value': 2,
                        'title': 'Net Banking',
                        'payment_type': 'NET_BANKING',
                        'description': 'HDFC Bank, ICICI Bank, State Bank of India, Axis',
                        'offer_text': 'Get 2% Instant Discount'
                    }
                ]
            else:
                ChoosePaymentOption = [
                    {

                        'value': offervalue,
                        'title': 'Debit Card',
                        'payment_type': 'DEBIT_CARD',
                        'description': 'Master Card, Rupay, Visa, American Express',
                        'offer_text': 'Get ' + str(offervalue)+'% Instant Discount',

                    },
                    {

                        'value': 0,
                        'title': 'Credit Card',
                        'payment_type': 'CREDIT_CARD',
                        'description': 'Master Card, Rupay, Visa, American Express',
                        'offer_text': '',

                    },
                    {

                        'value': offervalue,
                        'title': 'UPI',
                        'payment_type': 'UPI',
                        'description': 'GPay, PhonePe, Paytm etc.',
                        'offer_text': 'Get ' + str(offervalue)+'% Instant Discount',
                    },
                    {

                        'value': offervalue,
                        'title': 'Paytm Wallet',
                        'payment_type': 'BALANCE',
                        'description': 'Pay using paytm wallet',
                        'offer_text': 'Get ' + str(offervalue)+'% Instant Discount',
                    },
                    {

                        'value': offervalue,
                        'title': 'Net Banking',
                        'payment_type': 'NET_BANKING',
                        'description': 'HDFC Bank, ICICI Bank, State Bank of India, Axis',
                        'offer_text': 'Get ' + str(offervalue)+'% Instant Discount',
                    }
                ]

        # Check Payment Option
        discoun_rate = 0
        for paymentoption in ChoosePaymentOption:
            if(paymentoption['payment_type'].lower() == payment_method.lower()):
                discoun_rate = paymentoption['value']
        total_amount = 0
        # Total Amount Without Discount
        total_amount_without_discount = 0
        total_delivery = 0
        grand_total = 0.00
        if(payment_method.lower() == 'online'):
            title = 'Online Payment'
        else:
            title = 'Partial Payment'
        cartitems = []

        data = await CartHelper.cart_data_on_summary_page(db=db, user_id=userdata['id'])

        if(len(data) == 0):
            return {'status_code': HTTP_200_OK, "message": "No data found", "title": title, 'order_summary_text': [], "grand_total": {}}

        # New changes for partial payment
        if(payment_method.lower() != 'cod' and payment_method.lower() != 'partial'):

            finalcartdata = []
            sellers_detail = []
            for cartitems in data:
                in_stock_items = await CartHelper.in_stock_cart_items_on_summary_page(db=db, seller_id=cartitems.seller_id, user_id=userdata['id'])

                cartdata = []
                if(len(in_stock_items) > 0):
                    for cartitem in in_stock_items:
                        cartdata.append(cartitem)

                    finalcartdata.append(cartdata)

            for cartitemdata in finalcartdata:
                sub_total = 0
                # Sub Total With Discount
                sub_total_with_discount = 0
                # Sub Total Without Discount
                sub_total_without_discount = 0
                product_data = []
                # Get Seller Data
                seller = await fetch_seller_data(db=db, seller_id=cartitemdata[0].seller_id)

                for item in cartitemdata:

                    # Get Single Cart Data
                    singlecartdata = await CartHelper.single_cart_data(db=db, user_id=item.user_id, product_id=item.product_id)

                    # Get cart items by product id
                    itemsdata = db.query(CartModel).join(
                        ProductModel, ProductModel.id == CartModel.product_id).filter(CartModel.user_id == item.user_id).filter(CartModel.product_id == item.product_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).all()

                    # Get Pricing ID
                    itm_pricing_id = singlecartdata.uuid.split('-')
                    # Get Pricing
                    itm_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=itm_pricing_id[1])

                    if(itm_pricing):
                        total_items = []  # New Updated
                        for items in itemsdata:
                            # Get Product Detail
                            productdata = await ProductsHelper.fetch_product_data(db=db, product_id=items.product_id)

                            # Pricing ID
                            item_pricing_id = items.uuid.split('-')

                            # Get Pricing
                            item_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=item_pricing_id[1])

                            # Check Pricing Ecist
                            if(item_pricing):
                                # Inventory
                                checkiteminventory = await ProductsHelper.fetch_pricing_inventory(db=db, pricing_id=item_pricing.id)

                                # Check Product Stock
                                if(checkiteminventory.out_of_stock == 0):
                                    # Aseztak Service
                                    # aseztak_service = Services.aseztak_services(
                                    #     item_pricing.updated_at, db=db)
                                    # today_date = item_pricing.updated_at.strftime(
                                    #     '%Y-%m-%d')
                                    todaydate = date.today()
                                    today_date = todaydate.strftime('%Y-%m-%d')
                                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                                    # Calculate Product Pricing
                                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                    # Product Price with discount
                                    product_price_with_discount: ProductModel = await ProductsHelper.chekcOutPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                    # Product Price without discount
                                    product_price_without_discount = productPricecalculation(price=item_pricing.price, tax=item_pricing.tax, commission=aseztak_service.rate,
                                                                                             gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                                    # Sub Total
                                    totalprice = round(
                                        product_price, 2) * items.quantity
                                    # Total Price with Discount
                                    totalprice_with_discount = round(
                                        product_price_with_discount, 2) * items.quantity
                                    # Total Price without Discount
                                    totalprice_without_discount = round(
                                        product_price_without_discount, 2) * items.quantity
                                    sub_total += totalprice
                                    # Sub Total without discount
                                    sub_total_with_discount += totalprice_with_discount
                                    # Sub Total without discount
                                    sub_total_without_discount += totalprice_without_discount
                                    total_items.append(items)

                        # New Updated
                        p_data = {
                            'items': len(total_items),
                        }
                        product_data.append(p_data)

                # Today Date
                today = date.today()
                # YY-mm-dd
                today = today.strftime("%Y-%m-%d")

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscount(db=db, seller_id=seller.id, today_date=today, order_amount=float(round(sub_total, 2)))
                if(check_seller_discount != 0):
                    sub_total = (float(round(sub_total_with_discount, 2)))
                # Check Shipping Charge
                delivery_charge = 0
                if(sub_total != 0 and new_free_delivery == False):
                    shipping_charge = db.execute("SELECT id, rate, start_date, payment_mode, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                                 {"param": today}).first()

                    if(buyer_facility):
                        freedeliveryamont = int(buyer_facility.key_value)
                        if(round(sub_total, 2) < round(freedeliveryamont, 2)):
                            if(int(buyer_facility.key_value) != 0):
                                delivery_charge = shipping_charge.rate
                    else:
                        delivery_charge = checkOrderPlaceDeliveryCalculation(
                            order_amount=sub_total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode='ONLINE')

                # if(new_free_delivery == False):
                # Check Seller Purchase Limit New Updated
                showing_seller_product = True
                check_seller_facility = await ProductsHelper.check_seller_facility(db=db, user_id=cartitemdata[0].seller_id, type='purchase_limit')
                if(check_seller_facility is not None):
                    if(float(check_seller_facility.key_value) <= float(round(sub_total, 2))):
                        total_amount += round(sub_total, 2)
                        total_amount_without_discount += round(
                            sub_total_without_discount, 2)
                        total_delivery += delivery_charge
                    else:
                        showing_seller_product = False
                else:
                    total_amount += round(sub_total, 2)
                    # Total Amount without discount
                    total_amount_without_discount += round(
                        sub_total_without_discount, 2)

                    total_delivery += delivery_charge
                # else:
                #     showing_seller_product = True
                #     total_amount += round(sub_total, 2)
                #     # Total Amount without discount
                #     total_amount_without_discount += round(
                #         sub_total_without_discount, 2)

                #     total_delivery += delivery_charge
                # New Updated
                seller = {
                    'id': seller.id,
                    'name': seller.name,
                    'visible': showing_seller_product
                }
                sellers_detail.append(seller)
                finalcartitems.append(
                    {'showing_product': showing_seller_product, 'product_data': product_data})

                # CHECK ORDER DISCOUNT
            checkdiscountoffer = db.execute("SELECT id, discount, start_date, payment_method FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
                                            {"param": today}).first()

            discountamount = 0.00
            discountrate = 0.00
            offerTxt = ""
            # Check savings amount
            check_savings_amount = (
                float(total_amount_without_discount) - float(total_amount))
            if(check_savings_amount > 0):
                savings_amount = '- ₹'+str(round(check_savings_amount, 2))
                savings_visible = True
                totalAmountObj = {
                    "name": "Total Amount",
                    "value": floatingValue(round(total_amount_without_discount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }
            else:
                totalAmountObj = {
                    "name": "Total Amount",
                    "value": floatingValue(round(total_amount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }
                savings_amount = floatingValue(0)
                savings_visible = False
            # New Changes Wallet
            if(walletbalance != "0.00"):
                wallet_amount = (float(totalAmountObj['value']) - float(
                    round(check_savings_amount, 2))) - float(walletbalance)
            else:
                wallet_amount = total_amount

            if(discoun_rate != 0):
                dis = (float(round(wallet_amount, 2)) *
                       float(discoun_rate) / 100)
                discountamount += dis

                discountrate = discoun_rate

                offerTxt = 'Get ₹' + \
                    str(round(discountamount, 2))+' instant discount'
            else:
                if(checkdiscountoffer and checkdiscountoffer.discount != 0 and payment_method.lower() != 'credit_card'):
                    dis = (float(round(wallet_amount, 2)) *
                           float(checkdiscountoffer.discount) / 100)
                    discountamount += dis

                    discountrate = checkdiscountoffer.discount

                    offerTxt = 'Get ₹' + \
                        str(round(discountamount, 2))+' instant discount'

            if(payment_method.lower() == 'online'):
                grand_totla = round(wallet_amount, 2)
            else:
                grand_totla = round(wallet_amount, 2) - \
                    round(discountamount, 2)

            walletvisible = False
            if(walletbalance != "0.00"):
                walletvisible = True

            visible = True
            if(payment_method.lower() == 'online'):
                visible = False
            # NEW Total Cart Items
            final_count_total_items = 0
            if(len(finalcartitems) > 0):
                for total_cart_item in finalcartitems:
                    if(total_cart_item['showing_product'] == True):
                        for fnal_product in total_cart_item['product_data']:
                            final_count_total_items += fnal_product['items']

            if(payment_method.lower() != 'cod'):

                order_summary = [
                    {
                        "name": "Items",
                        "code": "items",
                        "value": str(final_count_total_items),  # New Updated
                        "currency": False,
                        "visible": True,
                        "offer_text": ""
                    },
                    totalAmountObj,
                    {
                        "name": "Extra Discount",
                        "code": "extra_discount",
                        "value": savings_amount,
                        "currency": True,
                        "visible": savings_visible,
                        "offer_text": ""
                    },
                    {
                        "name": "Wallet Amount ",
                        "code": "wallet_amount",
                        "value": str('-') + walletbalance,
                        "currency": True,
                        "visible": walletvisible,
                        "offer_text": ""
                    },
                    {
                        "name": "Discount (-)",
                        "code": "discount",
                        "value": floatingValue(round(discountamount, 2)),
                        "currency": True,
                        "visible": visible,
                        "offer_text": offerTxt
                    },
                    {
                        "name": "Shipping Charge ",
                        "code": "shipping_charge",
                        "value": floatingValue(total_delivery),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    }




                ]
            else:
                order_summary = [
                    {
                        "name": "Items",
                        "code": "items",
                        # New Updated
                        "value": floatingValue(final_count_total_items),
                        "currency": False,
                        "visible": True,
                        "offer_text": ""
                    },
                    totalAmountObj,
                    {
                        "name": "Shipping Charge ",
                        "code": "shipping_charge",
                        "value": floatingValue(total_delivery),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    },
                    {
                        "name": "Discount",
                        "code": "discount",
                        "value": floatingValue(round(discountamount)),
                        "currency": True,
                        "visible": False,
                        "offer_text": offerTxt
                    },

                    {
                        "name": "Extra Discount",
                        "code": "extra_discount",
                        "value": savings_amount,
                        "currency": True,
                        "visible": savings_visible,
                        "offer_text": ""
                    },
                    {
                        "name": "Wallet Amount ",
                        "code": "wallet_amount",
                        "value": str('-') + walletbalance,
                        "currency": True,
                        "visible": walletvisible,
                        "offer_text": ""
                    },
                    {
                        "name": "Payable Amount",
                        "code": "payable_amount",
                        "value": floatingValue(round(total_amount, 0)),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    }

                ]

            # Grand Total
            grand_total = float(round(grand_totla, 2)) + \
                float(round(total_delivery, 0))

            rounded_value = round(grand_total, 0) - grand_total

            # if(walletbalance != "0.00"):
            #     grand_total = (round(grand_total, 2) - float(walletbalance))

            grand_total = {
                "title": "Grand Total",
                "payment_method": payment_method.upper(),
                "discount_rate": floatingValue(discountrate),
                "discount_amount": floatingValue(discountamount),
                "round_off": floatingValue(round(rounded_value, 2)),
                "amount": floatingValue(round(grand_total)),
                "partial": floatingValue(0.00),

            }

            return {'status_code': HTTP_200_OK, "message": "Success", "title": title, 'online_payment_options': ChoosePaymentOption, 'order_summary': order_summary, "grand_total": grand_total, "sellers_detail": sellers_detail}
        elif(payment_method.lower() == 'partial'):  # New changes for partial payment

            finalcartdata = []
            sellers_detail = []
            for cartitems in data:
                in_stock_items = await CartHelper.in_stock_cart_items_on_summary_page(db=db, seller_id=cartitems.seller_id, user_id=userdata['id'])

                cartdata = []
                if(len(in_stock_items) > 0):
                    for cartitem in in_stock_items:
                        cartdata.append(cartitem)

                    finalcartdata.append(cartdata)

            for cartitemdata in finalcartdata:
                sub_total = 0
                # Sub Total With Discount
                sub_total_with_discount = 0
                # Sub Total Without Discount
                sub_total_without_discount = 0
                product_data = []
                # Get Seller Data
                seller = await fetch_seller_data(db=db, seller_id=cartitemdata[0].seller_id)

                for item in cartitemdata:

                    # Get Single Cart Data
                    singlecartdata = await CartHelper.single_cart_data(db=db, user_id=item.user_id, product_id=item.product_id)

                    # Get cart items by product id
                    itemsdata = db.query(CartModel).join(
                        ProductModel, ProductModel.id == CartModel.product_id).filter(CartModel.user_id == item.user_id).filter(CartModel.product_id == item.product_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).all()

                    # Get Pricing ID
                    itm_pricing_id = singlecartdata.uuid.split('-')
                    # Get Pricing
                    itm_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=itm_pricing_id[1])

                    if(itm_pricing):
                        total_items = []  # New Updated
                        for items in itemsdata:
                            # Get Product Detail
                            productdata = await ProductsHelper.fetch_product_data(db=db, product_id=items.product_id)

                            # Pricing ID
                            item_pricing_id = items.uuid.split('-')

                            # Get Pricing
                            item_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=item_pricing_id[1])

                            # Check Pricing Ecist
                            if(item_pricing):
                                # Inventory
                                checkiteminventory = await ProductsHelper.fetch_pricing_inventory(db=db, pricing_id=item_pricing.id)

                                # Check Product Stock
                                if(checkiteminventory.out_of_stock == 0):
                                    # Aseztak Service
                                    # aseztak_service = Services.aseztak_services(
                                    #     item_pricing.updated_at, db=db)
                                    # today_date = item_pricing.updated_at.strftime(
                                    #     '%Y-%m-%d')
                                    todaydate = date.today()
                                    today_date = todaydate.strftime('%Y-%m-%d')
                                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                                    # Calculate Product Pricing
                                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                    # Product Price with discount
                                    product_price_with_discount: ProductModel = await ProductsHelper.chekcOutPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')

                                    # Product Price without discount
                                    product_price_without_discount = productPricecalculation(price=item_pricing.price, tax=item_pricing.tax, commission=aseztak_service.rate,
                                                                                             gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                                    # Sub Total
                                    totalprice = round(
                                        product_price, 2) * items.quantity
                                    # Total Price with Discount
                                    totalprice_with_discount = round(
                                        product_price_with_discount, 2) * items.quantity
                                    # Total Price without Discount
                                    totalprice_without_discount = round(
                                        product_price_without_discount, 2) * items.quantity
                                    sub_total += totalprice
                                    # Sub Total with discount
                                    sub_total_with_discount += totalprice_with_discount
                                    # Sub Total without discount
                                    sub_total_without_discount += totalprice_without_discount
                                    total_items.append(items)

                        # New Updated
                        p_data = {
                            'items': len(total_items),
                        }
                        product_data.append(p_data)

                # Today Date
                today = date.today()
                # YY-mm-dd
                today = today.strftime("%Y-%m-%d")

                # Check Seller Discount
                check_seller_discount = await Discount.checkSellerDiscount(db=db, seller_id=seller.id, today_date=today, order_amount=float(round(sub_total, 2)))
                if(check_seller_discount != 0):
                    sub_total = (float(round(sub_total_with_discount, 2)))

                # Check Shipping Charge
                delivery_charge = 0
                if(sub_total != 0 and new_free_delivery == False):
                    shipping_charge = db.execute("SELECT id, rate, start_date, payment_mode, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                                 {"param": today}).first()

                    if(buyer_facility):
                        freedeliveryamont = int(buyer_facility.key_value)
                        if(round(sub_total, 2) < round(freedeliveryamont, 2)):
                            if(int(buyer_facility.key_value) != 0):
                                delivery_charge = shipping_charge.rate
                    else:
                        delivery_charge = checkOrderPlaceDeliveryCalculation(
                            order_amount=sub_total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode='ONLINE')
                # if(new_free_delivery == False):
                # Check Seller Purchase Limit New Updated
                showing_seller_product = True
                check_seller_facility = await ProductsHelper.check_seller_facility(db=db, user_id=cartitemdata[0].seller_id, type='purchase_limit')
                if(check_seller_facility is not None):
                    if(float(check_seller_facility.key_value) <= float(round(sub_total, 2))):
                        total_amount += round(sub_total, 2)
                        total_amount_without_discount += round(
                            sub_total_without_discount, 2)
                        total_delivery += delivery_charge
                    else:
                        showing_seller_product = False
                else:
                    total_amount += round(sub_total, 2)
                    # Total Amount without discount
                    total_amount_without_discount += round(
                        sub_total_without_discount, 2)

                    total_delivery += delivery_charge
                # else:
                #     showing_seller_product = True
                #     total_amount += round(sub_total, 2)
                #     # Total Amount without discount
                #     total_amount_without_discount += round(
                #         sub_total_without_discount, 2)

                #     total_delivery += delivery_charge
                # New Updated
                seller = {
                    'id': seller.id,
                    'name': seller.name,
                    'visible': showing_seller_product
                }
                sellers_detail.append(seller)
                finalcartitems.append(
                    {'showing_product': showing_seller_product, 'product_data': product_data})

            discountamount = 0.00
            discountrate = 0.00
            offerTxt = ""
            # Check savings amount
            check_savings_amount = (
                float(total_amount_without_discount) - float(total_amount))
            if(check_savings_amount > 0):
                savings_amount = '- ₹'+str(round(check_savings_amount, 2))
                savings_visible = True
                totalAmountObj = {
                    "name": "Total Amount",
                    "value": floatingValue(round(total_amount_without_discount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }
            else:
                totalAmountObj = {
                    "name": "Total Amount",
                    "value": floatingValue(round(total_amount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }
                savings_amount = floatingValue(0)
                savings_visible = False
            # New Changes Wallet
            if(walletbalance != "0.00"):
                wallet_amount = (float(totalAmountObj['value']) - float(
                    round(check_savings_amount, 2))) - float(walletbalance)
            else:
                wallet_amount = total_amount
            walletvisible = False
            if(walletbalance != "0.00"):
                walletvisible = True

            discountamount += 0

            discountrate = 0

            offerTxt = ''

            if(payment_method.lower() == 'online'):
                grand_totla = round(wallet_amount, 2)
            else:
                grand_totla = round(wallet_amount, 2) - \
                    round(discountamount, 2)

            walletvisible = False
            if(walletbalance != "0.00"):
                walletvisible = True

            visible = True
            if(payment_method.lower() == 'online'):
                visible = False
            # NEW Total Cart Items
            final_count_total_items = 0
            if(len(finalcartitems) > 0):
                for total_cart_item in finalcartitems:
                    if(total_cart_item['showing_product'] == True):
                        for fnal_product in total_cart_item['product_data']:
                            final_count_total_items += fnal_product['items']

            # Grand Total
            grand_total = float(round(grand_totla, 2)) + \
                float(round(total_delivery, 0))
            # if(walletbalance != "0.00"):
            #     grand_total = (round(grand_total, 2) - float(walletbalance))

            # new changes for partial payment
            partial_amount = (round(grand_total, 2) * 20 / 100)
            partial_amount = round(partial_amount, 2)
            cod_amount = (round(grand_total, 2) - partial_amount)

            order_summary = [
                {
                    "name": "Items",
                    "code": "items",
                    # New Updated
                    "value": floatingValue(final_count_total_items),
                    "currency": False,
                    "visible": True,
                    "offer_text": ""
                },
                totalAmountObj,
                {
                    "name": "Extra Discount",
                    "code": "extra_discount",
                    "value": savings_amount,
                    "currency": True,
                    "visible": savings_visible,
                    "offer_text": ""
                },
                {
                    "name": "Wallet Amount",
                    "code": "wallet_amount",
                    "value": str('-') + walletbalance,
                    "currency": True,
                    "visible": walletvisible,
                    "offer_text": ""
                },
                {
                    "name": "Shipping Charge ",
                    "code": "shipping_charge",
                    "value": floatingValue(total_delivery),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                },
                {
                    "name": "COD Amount",
                    "code": "cod_amount",
                    "value": floatingValue(round(cod_amount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                },

                {
                    "name": "Advance Payment",
                    "code": "payable_amount",
                    "value": floatingValue(round(partial_amount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }

            ]

            rounded_value = round(grand_total, 0) - grand_total

            grand_total = {
                "title": "Grand Total",
                "payment_method": payment_method.upper(),
                "discount_rate": floatingValue(discountrate),
                "discount_amount": floatingValue(discountamount),
                "round_off": floatingValue(round(rounded_value, 2)),
                "amount": floatingValue(round(grand_total)),
                "partial": floatingValue(round(partial_amount, 2)),

            }

            return {'status_code': HTTP_200_OK, "message": "Success", "title": title, 'online_payment_options': ChoosePaymentOption, 'order_summary': order_summary, "grand_total": grand_total, "sellers_detail": sellers_detail}

        # cartitems: CartModel = await CartHelper.orderSummary(request=request, db=db, data=data, payment_method=payment_method)
        # return cartitems
        else:
            return {'status_code': HTTP_200_OK, "message": "No data found", "payment_method": payment_method, 'order_summary_text': [], "grand_total": {}}

    except Exception as e:
        print(e)


@v4_checkout_router.get("/summary/{walletbalance}", dependencies=[Depends(JWTBearer())])
async def cartSummary(request: Request, walletbalance: Optional[str] = "0.00", db: Session = Depends(get_db)):

    userdata = auth(request=request)
    finalcartitems = []
    total_amount = 0
    # Total Amount without discount
    total_amount_without_discoun = 0
    total_delivery = 0
    offerTxt = ''
    checkTotalAmountforSellerCodLimit = ''
    # Total Savings Amount
    total_savings_amount = ''

    # Check Delivery Charger for new user
    new_free_delivery = False
    if(config.FREE_DELIVERY == "True"):
        check_order_exist = await OrderHelper.check_order_exist(db=db, user_id=userdata['id'])
        new_free_delivery = check_order_exist

    # Check User facility
    buyer_facility = await Facility.buyer_facility(db=db, user_id=userdata['id'])

    # Check Discount Facility
    buyer_orderdiscount_facility = await Facility.buyer_discount_facility(db=db, user_id=userdata['id'])

    try:
        data = await CartHelper.cart_data_on_summary_page(db=db, user_id=userdata['id'])

        finalcartdata = []
        for cartitems in data:

            in_stock_items = await CartHelper.in_stock_cart_items_on_summary_page(db=db, seller_id=cartitems.seller_id, user_id=userdata['id'])

            cartdata = []
            for cartitem in in_stock_items:
                cartdata.append(cartitem)

            finalcartdata.append(cartdata)

        for cartitemdata in finalcartdata:

            sub_total = 0
            # Sub total without discount
            sub_total_without_discount = 0
            product_data = []

            for item in cartitemdata:

                # Fetch Single Cart Data
                singlecartdata = await CartHelper.single_cart_data(db=db, user_id=item.user_id, product_id=item.product_id)

                # Get Seller Data
                seller = await fetch_seller_data(db=db, seller_id=item.seller_id)

                # Get Product Data
                product = await ProductsHelper.fetch_product_data(db=db, product_id=item.product_id)

                # Get cart items by product id
                itemsdata = await CartHelper.fetch_cart_item_by_product(db=db, user_id=userdata['id'], seller_id=item.seller_id, product_id=item.product_id)

                # Get Pricing ID
                itm_pricing_id = singlecartdata.uuid.split('-')

                # Get Item Pricing
                itm_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=itm_pricing_id[1])

                # Check Pricing Exist
                if(itm_pricing):

                    total_items = []
                    order_total_amount = 0

                    # Load Items
                    img = ''
                    for items in itemsdata:
                        # Get Product Detail
                        productdata = await ProductsHelper.fetch_product_data(db=db, product_id=items.product_id)
                        # productdata = db.query(ProductModel).where(
                        #     ProductModel.id == items.product_id).first()

                        # Get Pricing ID
                        item_pricing_id = items.uuid.split('-')

                        # Get Pricing
                        item_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=item_pricing_id[1])

                        # Check Pricing
                        if(item_pricing):

                            # Get Pricing Stock
                            checkiteminventory = await ProductsHelper.fetch_pricing_inventory(db=db, pricing_id=item_pricing.id)

                            # Check Pricing Stock
                            if(checkiteminventory.out_of_stock == 0):

                                # Aseztak Service
                                # today_date = item_pricing.updated_at.strftime(
                                #     '%Y-%m-%d')
                                todaydate = date.today()
                                today_date = todaydate.strftime('%Y-%m-%d')
                                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                                # Calculate Product Pricing
                                product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                # Product Price without discount
                                product_price_without_discount = productPricecalculation(price=item_pricing.price, tax=item_pricing.tax, commission=aseztak_service.rate,
                                                                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                                # Calculate Product toal Price with Quantity
                                totalprice = round(
                                    product_price, 2) * items.quantity
                                # Total Price without discount
                                totalprice_without_discount = round(
                                    product_price_without_discount, 2) * items.quantity
                                sub_total += totalprice
                                # Sub Total without discount
                                sub_total_without_discount += totalprice_without_discount

                                order_total_amount += totalprice
                                total_items.append(items)

                                # Set Product Image
                                if(item_pricing.default_image != 0):
                                    image = await ProductsHelper.fetch_product_media_data(db=db, default_img_id=item_pricing.default_image, product_id=0)
                                else:
                                    image = await ProductsHelper.fetch_product_media_data(db=db, default_img_id=1, product_id=items.product_id)

                                if(image == None):
                                    image = await ProductsHelper.fetch_product_media_data(db=db, default_img_id=0, product_id=items.product_id)

                            img = image.file_path

                    # Customize Product Data group By Seller
                    p_data = {
                        'image': img,
                        'id': product.id,
                        'title': product.title,
                        'slug': product.slug,
                        'items': len(total_items),
                        'amount': floatingValue(round(order_total_amount, 2))
                    }

                    product_data.append(p_data)

                # Customize Seller Details
                seller = {
                    'id': seller.id,
                    'name': seller.name,
                    'address': seller.city+', '+seller.region
                }

            if(len(product_data) > 0):
                # Store Data For delivery charge Purpose
                delivery_charge = 0
                min_order_value = 0
                order_checkout_text = PRODUCT_FREE_DELIVERY_TEXT
                free_delivery = True

                # Today date
                today = date.today()
                # YY-mm-dd
                today = today.strftime("%Y-%m-%d")

                # Check Shipping Charge
                shipping_charge = await OrderHelper.check_order_shippinhg_charge(db=db, date=today)

                # If order limit not equal to '0'
                if(new_free_delivery == False):

                    # Check Buyer Facility
                    if(buyer_facility):
                        freedeliveryamount = int(buyer_facility.key_value)

                        if(round(sub_total, 2) < round(freedeliveryamount, 2)):

                            # Shipping rate
                            delivery_charge = shipping_charge.rate
                            # minimum order check text
                            if(shipping_charge.rate != 0):
                                order_checkout_text = 'Get Free Delivery on your every order above Rs/- ' + \
                                    str(freedeliveryamount)
                                free_delivery = False
                            else:
                                order_checkout_text = PRODUCT_FREE_DELIVERY_TEXT
                                # Check free delivery
                                free_delivery = True

                    else:

                        if (shipping_charge.order_limit != 0):
                            if (round(sub_total, 2) < round(shipping_charge.order_limit, 2)):
                                # Shipping rate
                                delivery_charge = shipping_charge.rate
                                # Minimum order amount
                                min_order_value = shipping_charge.order_limit
                                # minimum order check text
                                if(shipping_charge.shipping_text is not None):
                                    order_checkout_text = shipping_charge.shipping_text
                                else:
                                    order_checkout_text = ''

                                # Check free delivery
                                free_delivery = False

                        else:
                            # Shipping rate
                            delivery_charge = shipping_charge.rate
                            # minimum order check text
                            if(shipping_charge.rate == 0):
                                order_checkout_text = PRODUCT_FREE_DELIVERY_TEXT
                                free_delivery = True
                            else:
                                order_checkout_text = ''
                                # Check free delivery
                                free_delivery = False
                # Product Total Amount
                total_amount += round(sub_total, 2)
                # Total Amount without discount
                total_amount_without_discoun += round(
                    sub_total_without_discount, 2)
                # Product Total Delivery Charge
                total_delivery += delivery_charge

                # # Grand Total
                # grandtotal = total_amount + float(total_delivery)

                # Cart Items
                # Check COD limit partial order avobe 10000/-
                checkTotalAmountforCodLimit = ''
                if(round(sub_total, 2) > 30000):
                    checkTotalAmountforCod = 'You can buy only ₹30000 at a time in COD'
                    checkTotalAmountforCodLimit = checkTotalAmountforCod
                    # checkTotalAmountforSellerCodLimit += checkTotalAmountforCodLimit
                    checkTotalAmountforSellerCodLimit = checkTotalAmountforCodLimit

                finalcartitems.append(
                    {'seller': seller, 'product_data': product_data, 'total_amount': floatingValue(round(sub_total, 2)), 'delivery_charge': floatingValue(delivery_charge), 'min_order_value': floatingValue(min_order_value), 'order_checkout_text': order_checkout_text, 'free_delivery': free_delivery, "checkTotalAmountforCodLimit": checkTotalAmountforCodLimit})

                # Append total cart items
                # # Total Cart Items
                # total_cart_items = await CartHelper.fetch_total_cart_items_data(db=db, user_id=item.user_id)

                # final_count_total_items = []
                # if(len(total_cart_items) > 0):
                #     for total_cart_item in total_cart_items:
                #         final_count_total_items.append(total_cart_item)
        # Total Cart Items #New Updated
        final_count_total_items = 0
        if(len(finalcartitems) > 0):
            for total_cart_item in finalcartitems:
                for fnal_product in total_cart_item['product_data']:
                    final_count_total_items += fnal_product['items']
        # Grand Total
        grandtotal = total_amount + float(total_delivery)  # New Updated

        # Check total product savings amount
        check_total_savings_amount = (
            float(total_amount_without_discoun) - float(total_amount))
        saving_amount_visible = False

        totalAmountObj = {
            "name": "Total Amount",
            "value": floatingValue(round(total_amount, 2)),
            "currency": True,
            "visible": True,
            "offer_text": ""
        }

        if(check_total_savings_amount > 0):
            totalAmountObj = {
                "name": "Total Amount",
                "value": floatingValue(round(total_amount_without_discoun, 2)),
                "currency": True,
                "visible": True,
                "offer_text": ""
            }
            saving_amount_visible = True
            total_savings_amount = '- ₹' + \
                str(round(check_total_savings_amount))
        # Order Summary Text
        walletvisible = False
        if(walletbalance != "0.00"):
            walletvisible = True

        order_summary = [
            {
                "name": "Items",
                "code": "items",
                "value": str(final_count_total_items),  # New Updated
                "currency": False,
                "visible": True,
                "offer_text": ""
            },
            totalAmountObj,
            {
                "name": "Shipping Charge ",
                "code": "shipping_charge",
                "value": floatingValue(total_delivery),
                "currency": True,
                "visible": True,
                "offer_text": ""
            },
            {
                "name": "You Saved",
                "code": "extra_discount",
                "value": total_savings_amount,
                "currency": True,
                "visible": saving_amount_visible,
                "offer_text": ""
            },
            {
                "name": "Wallet Amount",
                "code": "wallet_amount",
                "value": str('-') + walletbalance,
                "currency": True,
                "visible": walletvisible,
                "offer_text": ""
            }

        ]

        # Grand Total
        rounded_value = round(grandtotal, 0) - grandtotal

        if(walletbalance != "0.00"):
            grandtotal = (round(grandtotal) - float(walletbalance))

        grand_total = {
            "title": "Grand Total",
            "payment_method": 'COD',
            "discount_rate": floatingValue(0.00),
            "discount_amount": floatingValue(0.00),
            "round_off": floatingValue(round(rounded_value, 2)),

            "amount": floatingValue(round(grandtotal)),
            "partial": floatingValue(0.00),
        }

        # Shipping Address
        shipping_addresses = await AddressHelper.fetch_shipping_address_data(db=db, user_id=item.user_id)

        for address in shipping_addresses:
            alter_phone = ''
            if(address.alt_phone == None):
                address.alt_phone = alter_phone

            default_address = False
            if(address.default_address == 1):
                default_address = True

            address.default_address = default_address

        # Payment Method
        payment_method = [
            {
                'value': 'ONLINE',
                'title': 'Pay Online',
                'body': 'Use Netbanking, Debit Card or Credit Card'
            },
            {
                'value': 'COD',
                'title': 'Cash on Delivery',
                'body': 'Pay by Cash when your product arrives'
            }

        ]

        payment_methods = []
        for payment in payment_method:
            title = payment['title']
            body = payment['body']

            # CHECK ORDER DISCOUNT
            checkoffer = db.execute("SELECT id, discount, start_date, discount_text, payment_method FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
                                    {"param": today}).first()

            offer = ''
            discount_text = ''
            discount = 0
            if(checkoffer.discount != 0 and checkoffer.payment_method == payment['value']):
                offer = ""
                fractional, whole = math.modf(checkoffer.discount)
                if(fractional == 0):
                    discount = round(checkoffer.discount)
                else:
                    discount = round(checkoffer.discount, 2)

                if(checkoffer.discount_text is not None):
                    # discount_text = checkoffer.discount_text
                    offerTxt = checkoffer.discount_text
                    discount_text = ''
                else:

                    discount_text = ''

                # Footer Text Changes Rahul

            # Changes rahul (Will be change in future)
            # Check Shipping Charge
            check_shipping_charge = db.execute("SELECT id, rate, payment_mode, shipping_text, start_date, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                               {"param": today}).first()

            # Changes Rahul (Will be changed after 19th April)
            if(payment['value'] == 'ONLINE' and checkoffer.discount == 0 and check_shipping_charge.payment_mode == 'COD'):
                if(checkoffer.discount_text is not None):
                    # offer = checkoffer.discount_text
                    offerTxt = checkoffer.discount_text
                    offer = ''
                else:
                    offer = ''
                    offerTxt = ''

            # NEW CHANGES RAHUL (WILL BE DISAPPEAR after 20th July)
            if(payment['value'] == 'COD' and checkoffer.discount == 0 and check_shipping_charge.rate == 0 and check_shipping_charge.payment_mode == 'BOTH'):
                # offer = check_shipping_charge.shipping_text
                offer = ''

            visible = True
            disabled = False

            if(payment['value'] == 'COD'):

                discount = 0
                facility = await Facility.check_buyer_cod_limit(db=db, user_id=item.user_id)

                if(facility):
                    facility_date = facility.updated_at.strftime(
                        "%Y-%m-%d")

                    orders = await OrderHelper.check_buyer_cod_orders(db=db, user_id=userdata['id'], date=facility_date)

                    restAmount = 0
                    if(len(orders) > 0):
                        previews_total_amount = 0
                        for order in orders:
                            previews_total_amount += await OrderCalculation.calculateTotalAmount(db=db,
                                                                                                 orderID=order.id)

                        previews_total_amount = roundOf(
                            previews_total_amount)

                        if(int(facility.key_value) > previews_total_amount):

                            restAmount = (int(facility.key_value)
                                          ) - previews_total_amount

                        else:

                            restAmount = 0

                        restAmount = roundOf(restAmount)

                        if(restAmount != 0):
                            if (round(grandtotal) > restAmount):
                                disabled = True
                                # title = "Remaining COD limit"
                                title = "Cash on Delivery"
                                body = 'Remaining limit ₹' + str(restAmount) + \
                                    " out of ₹" + \
                                    str(facility.key_value)
                            else:
                                disabled = False
                                # title = "Remaining COD limit"
                                title = "Cash on Delivery"
                                body = 'Remaining limit ₹' + str(restAmount) + \
                                    " out of ₹" + \
                                    str(facility.key_value)
                        else:
                            disabled = True
                            # title = "Remaining COD limit"
                            title = "Cash on Delivery"
                            body = 'Remaining limit ₹' + str(restAmount) + \
                                " out of ₹" + \
                                str(facility.key_value)

                    else:
                        if(round(grandtotal) > int(facility.key_value)):
                            disabled = True
                            # title = "Remaining COD limit"
                            title = "Cash on Delivery"
                            body = 'Remaining limit ₹' + str(restAmount) + \
                                " out of ₹" + \
                                str(facility.key_value)
                        else:
                            disabled = False
                            # title = "Remaining COD limit"
                            title = "Cash on Delivery"
                            body = 'Remaining limit ₹' + str(restAmount) + \
                                " out of ₹" + \
                                str(facility.key_value)

                    if(facility.key_value == '0'):
                        disabled = True
                        # title = "Remaining COD limit"
                        title = "Cash on Delivery"
                        body = 'Remaining limit ₹' + str(restAmount) + \
                            " out of ₹" + \
                            str(facility.key_value)

                if(checkTotalAmountforSellerCodLimit != ''):
                    disabled = True
                    # title = 'COD Limit'
                    title = 'Cash on Delivery'
                    body = checkTotalAmountforSellerCodLimit

            pmethods = {
                'value': payment['value'],
                'title': title,
                'description': body,
                'visible': visible,
                'disabled': disabled,
                'discount': discount,
                'discount_text': discount_text,
                'offer': offer

            }

            payment_methods.append(pmethods)

        # User Document Type
        user_document_type = []
        documents = [{
            'name': 'GSTIN',
                    'value': '',
                    'selected': False
        },
            {
            'name': 'AADHAAR',
                    'value': '',
                    'selected': False
        },
            {
            'name': 'PAN',
                    'value': '',
                    'selected': False
        }]

        for document in documents:

            checkdocument = await fetch_user_profile_data(db=db, user_id=item.user_id)

            selected = False
            value = ''
            if(checkdocument and checkdocument.proof_type == document['name']):
                selected = True
                value = checkdocument.proof

            doc = {
                'name': document['name'],
                'value': value,
                'selected': selected
            }

            user_document_type.append(doc)

        # Check Wallet Balance (new)
        wallet_transaction = db.query(WalletModel).filter(
            WalletModel.user_id == userdata['id']).order_by(WalletModel.id.asc())

        wallet_debit_balance = 0
        wallet_credit_balance = 0
        wallet_closing_balance = 0

        wallet_balance = {
            'visible': False,
            'usable': False,
            'balance': floatingValue(0),
            'use_balance': floatingValue(0)
        }
        # if(wallet_transaction.count() > 0):
        #     if(walletbalance != "0.00"):
        #         usable = False
        #     else:
        #         usable = True

        #     for transaction in wallet_transaction.all():

        #         if('DR' in str(transaction.type)):
        #             wallet_debit_balance += round(transaction.amount, 2)
        #         else:
        #             wallet_credit_balance += round(transaction.amount, 2)

        #     wallet_closing_balance = (round(wallet_credit_balance,
        #                                     2) - round(wallet_debit_balance, 2))

        #     wallet_closing_balance = float(wallet_closing_balance) - float(
        #         walletbalance)

        #     wallet_closing_balance = floatingValue(
        #         round(wallet_closing_balance, 2))

        #     if(round(grandtotal) > 9000 and round(grandtotal) < 15000):

        #         if(float(wallet_closing_balance) >= 5000):

        #             use_balance = floatingValue(5000)
        #         else:
        #             use_balance = wallet_closing_balance

        #         wallet_balance = {
        #             'visible': True,
        #             'usable': usable,
        #             'balance': wallet_closing_balance,
        #             'use_balance': use_balance
        #         }

        #     elif(round(grandtotal) > 15000 and round(grandtotal) < 21000):

        #         if(float(wallet_closing_balance) >= 7000):

        #             use_balance = floatingValue(7000)
        #         else:
        #             use_balance = wallet_closing_balance

        #         wallet_balance = {
        #             'visible': True,
        #             'usable': usable,
        #             'balance': wallet_closing_balance,
        #             'use_balance': use_balance
        #         }

        #     elif(round(grandtotal) > 21000):
        #         if(float(wallet_closing_balance) >= floatingValue(10000)):
        #             use_balance = floatingValue(10000)
        #         else:
        #             use_balance = wallet_closing_balance

        #         wallet_balance = {
        #             'visible': True,
        #             'usable': usable,
        #             'balance': wallet_closing_balance,
        #             'use_balance': use_balance
        #         }
        #     else:
        #         wallet_balance = {
        #             'visible': True,
        #             'usable': usable,
        #             'balance': wallet_closing_balance,
        #             'use_balance': floatingValue(0)
        #         }

        # Check buyer facility
        if(buyer_facility and buyer_orderdiscount_facility):
            freedeliveryamount = int(buyer_facility.key_value)
            discountfacilty = int(buyer_orderdiscount_facility.key_value)

            if(freedeliveryamount != 0):

                offerTxt = '₹0 shipping on ₹' + \
                    str(freedeliveryamount) + ' & ' + \
                    str(discountfacilty) + '% off on Online Payment'
            else:
                offerTxt = 'Free Shipping' + ' & ' + \
                    str(discountfacilty) + '% off on Online Payment'

        # Check Discount
        elif(buyer_orderdiscount_facility):
            discountfacilty = int(buyer_orderdiscount_facility.key_value)
            if (shipping_charge.order_limit != 0):
                if (round(sub_total, 2) > round(shipping_charge.order_limit, 2)):
                    # Shipping rate
                    delivery_charge = shipping_charge.rate
                    # Minimum order amount
                    min_order_value = shipping_charge.order_limit
                    # minimum order check text
                    if(shipping_charge.shipping_text is not None):
                        offerTxt = '₹0 shipping on ₹' + \
                            str(min_order_value) + ' & ' + \
                            str(discountfacilty) + '% off on Online Payment'
                    else:
                        offerTxt = ''
            else:
                delivery_charge = shipping_charge.rate
                # minimum order check text
                if(shipping_charge.rate == 0):
                    offerTxt = 'Free Shipping' + ' & ' + \
                        str(discountfacilty) + '% off on Online Payment'
                else:
                    offerTxt = ''

        elif(buyer_facility):
            freedeliveryamount = int(buyer_facility.key_value)
            fractional, whole = math.modf(checkoffer.discount)
            if(fractional == 0):
                discount = round(checkoffer.discount)
            else:
                discount = round(checkoffer.discount, 2)

            if(freedeliveryamount != 0):
                offerTxt = '₹0 shipping on ₹' + \
                    str(freedeliveryamount) + ' & ' + \
                    str(discount) + '% off on Online Payment'
            else:
                offerTxt = 'Free Delivery' + ' & ' + \
                    str(discount) + '% off on Online Payment'

        return {'status_code': HTTP_200_OK, "wallet_balance": wallet_balance, 'offer_text': offerTxt, "total_savings_amount": total_savings_amount, 'shipping_address': shipping_addresses, 'user_documents': user_document_type,  'payment_method': payment_methods, 'order_summary': order_summary, "grand_total": grand_total, 'item_data': finalcartitems}

    except Exception as e:
        print(e)
        return {'status_code': HTTP_400_BAD_REQUEST}


@v4_checkout_router.get("/new/payment/option/{payment_method}/{walletbalance}", dependencies=[Depends(JWTBearer())])
async def checkOut(request: Request, payment_method: str, walletbalance: Optional[str] = '0.00', db: Session = Depends(get_db)):
    try:
        finalcartitems = []  # New Updated
        userdata = auth(request=request)

        # Check Delivery Charger for new user
        new_free_delivery = False
        if(config.FREE_DELIVERY == "True"):
            check_order_exist = await OrderHelper.check_order_exist(db=db, user_id=userdata['id'])
            new_free_delivery = check_order_exist

        offer_Txt = ''

        # Check User facility
        buyer_facility = await Facility.buyer_facility(db=db, user_id=userdata['id'])
        # Check Discount Facility
        buyer_orderdiscount_facility = await Facility.buyer_discount_facility(db=db, user_id=userdata['id'])

        # TEMPORARY CHANGES
        todaydate = datetime.now()
        today = todaydate.strftime("%Y-%m-%d")
        # Offer 08th to 14th Ocotber\
        if(buyer_orderdiscount_facility):
            offervalue = int(buyer_orderdiscount_facility.key_value)

        else:
            offervalue = 2
        if(today > '2023-04-06' and today < '2023-04-13'):  # new offer

            ChoosePaymentOption = [
                {

                    'value': 2,
                    'title': 'Debit Card',
                    'payment_type': 'DEBIT_CARD',
                    'description': 'Master Card, Rupay, Visa, American Express',
                    'offer_text': 'Get 2% Instant Discount',

                },
                {

                    'value': 0,
                    'title': 'Credit Card',
                    'payment_type': 'CREDIT_CARD',
                    'description': 'Master Card, Rupay, Visa, American Express',
                    'offer_text': '',

                },
                {

                    'value': 5,
                    'title': 'UPI',
                    'payment_type': 'UPI',
                    'description': 'GPay, PhonePe, Paytm etc.',
                    'offer_text': 'Get 5% Instant Discount'
                },
                {

                    'value': 2,
                    'title': 'Paytm Wallet',
                    'payment_type': 'BALANCE',
                    'description': 'Pay using paytm wallet',
                    'offer_text': 'Get 2% Instant Discount'
                },
                {

                    'value': 2,
                    'title': 'Net Banking',
                    'payment_type': 'NET_BANKING',
                    'description': 'HDFC Bank, ICICI Bank, State Bank of India, Axis',
                    'offer_text': 'Get 2% Instant Discount'
                }
            ]
        else:
            ChoosePaymentOption = [
                {

                    'value': offervalue,
                    'title': 'Debit Card',
                    'payment_type': 'DEBIT_CARD',
                    'description': 'Master Card, Rupay, Visa, American Express',
                    'offer_text': 'Get ' + str(offervalue)+'% Instant Discount',

                },
                {

                    'value': 0,
                    'title': 'Credit Card',
                    'payment_type': 'CREDIT_CARD',
                    'description': 'Master Card, Rupay, Visa, American Express',
                    'offer_text': '',

                },
                {

                    'value': offervalue,
                    'title': 'UPI',
                    'payment_type': 'UPI',
                    'description': 'GPay, PhonePe, Paytm etc.',
                    'offer_text': 'Get ' + str(offervalue)+'% Instant Discount',
                },
                {

                    'value': offervalue,
                    'title': 'Paytm Wallet',
                    'payment_type': 'BALANCE',
                    'description': 'Pay using paytm wallet',
                    'offer_text': 'Get ' + str(offervalue)+'% Instant Discount',
                },
                {

                    'value': offervalue,
                    'title': 'Net Banking',
                    'payment_type': 'NET_BANKING',
                    'description': 'HDFC Bank, ICICI Bank, State Bank of India, Axis',
                    'offer_text': 'Get ' + str(offervalue)+'% Instant Discount',
                }
            ]

        # Check Payment Option
        discoun_rate = 0
        for paymentoption in ChoosePaymentOption:
            if(paymentoption['payment_type'].lower() == payment_method.lower()):
                discoun_rate = paymentoption['value']

        total_amount = 0
        # Total Amount Without Discount
        total_amount_without_discount = 0
        total_delivery = 0
        grand_total = 0.00
        if(payment_method.lower() == 'online'):
            title = 'Online Payment'
        else:
            title = 'Partial Payment'
        cartitems = []

        data = await CartHelper.cart_data_on_summary_page(db=db, user_id=userdata['id'])

        if(len(data) == 0):
            return {'status_code': HTTP_200_OK, "message": "No data found", "title": title, 'order_summary_text': [], "grand_total": {}}

        if(payment_method.lower() != 'cod'):

            finalcartdata = []
            for cartitems in data:
                in_stock_items = await CartHelper.in_stock_cart_items_on_summary_page(db=db, seller_id=cartitems.seller_id, user_id=userdata['id'])

                cartdata = []
                for cartitem in in_stock_items:
                    cartdata.append(cartitem)

                finalcartdata.append(cartdata)

            for cartitemdata in finalcartdata:
                sub_total = 0
                # Sub Total Without Discount
                sub_total_without_discount = 0
                product_data = []
                for item in cartitemdata:

                    # Get Single Cart Data
                    singlecartdata = await CartHelper.single_cart_data(db=db, user_id=item.user_id, product_id=item.product_id)

                    # Get cart items by product id
                    itemsdata = db.query(CartModel).join(
                        ProductModel, ProductModel.id == CartModel.product_id).filter(CartModel.user_id == item.user_id).filter(CartModel.product_id == item.product_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).all()

                    # Get Pricing ID
                    itm_pricing_id = singlecartdata.uuid.split('-')
                    # Get Pricing
                    itm_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=itm_pricing_id[1])

                    if(itm_pricing):
                        total_items = []  # New Updated
                        for items in itemsdata:
                            # Get Product Detail
                            productdata = await ProductsHelper.fetch_product_data(db=db, product_id=items.product_id)

                            # Pricing ID
                            item_pricing_id = items.uuid.split('-')

                            # Get Pricing
                            item_pricing = await ProductsHelper.fetch_cart_item_pricing_data(db=db, pricing_id=item_pricing_id[1])

                            # Check Pricing Ecist
                            if(item_pricing):
                                # Inventory
                                checkiteminventory = await ProductsHelper.fetch_pricing_inventory(db=db, pricing_id=item_pricing.id)

                                # Check Product Stock
                                if(checkiteminventory.out_of_stock == 0):
                                    # Aseztak Service
                                    # aseztak_service = Services.aseztak_services(
                                    #     item_pricing.updated_at, db=db)
                                    # today_date = item_pricing.updated_at.strftime(
                                    #     '%Y-%m-%d')
                                    todaydate = date.today()
                                    today_date = todaydate.strftime('%Y-%m-%d')
                                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                                    # Calculate Product Pricing
                                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                    # Product Price without discount
                                    product_price_without_discount = productPricecalculation(price=item_pricing.price, tax=item_pricing.tax, commission=aseztak_service.rate,
                                                                                             gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                                    # Sub Total
                                    totalprice = round(
                                        product_price, 2) * items.quantity
                                    # Total Price without Discount
                                    totalprice_without_discount = round(
                                        product_price_without_discount, 2) * items.quantity
                                    sub_total += totalprice
                                    # Sub Total without discount
                                    sub_total_without_discount += totalprice_without_discount
                                    total_items.append(items)

                        # New Updated
                        p_data = {
                            'items': len(total_items),
                        }
                        product_data.append(p_data)

                # Today Date
                today = date.today()
                # YY-mm-dd
                today = today.strftime("%Y-%m-%d")

                # Check Shipping Charge
                delivery_charge = 0
                if(sub_total != 0 and new_free_delivery == False):
                    shipping_charge = db.execute("SELECT id, rate, start_date, payment_mode, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                                 {"param": today}).first()

                    if(buyer_facility):
                        freedeliveryamont = int(buyer_facility.key_value)
                        if(round(sub_total, 2) < round(freedeliveryamont, 2)):
                            if(int(buyer_facility.key_value) != 0):
                                delivery_charge = shipping_charge.rate
                    else:
                        delivery_charge = checkOrderPlaceDeliveryCalculation(
                            order_amount=sub_total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode='ONLINE')

                total_amount += round(sub_total, 2)
                # Total Amount without discount
                total_amount_without_discount += round(
                    sub_total_without_discount, 2)

                total_delivery += delivery_charge
                # New Updated
                finalcartitems.append(
                    {'product_data': product_data})

                # CHECK ORDER DISCOUNT
            checkdiscountoffer = db.execute("SELECT id, discount, start_date, payment_method FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
                                            {"param": today}).first()

            discountamount = 0.00
            discountrate = 0.00
            offerTxt = ""
            # Check savings amount
            check_savings_amount = (
                float(total_amount_without_discount) - float(total_amount))
            if(check_savings_amount > 0):
                savings_amount = '- ₹'+str(round(check_savings_amount))
                savings_visible = True
                totalAmountObj = {
                    "name": "Total Amount",
                    "value": floatingValue(round(total_amount_without_discount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }
            else:
                totalAmountObj = {
                    "name": "Total Amount",
                    "value": floatingValue(round(total_amount, 2)),
                    "currency": True,
                    "visible": True,
                    "offer_text": ""
                }
                savings_amount = floatingValue(0)
                savings_visible = False

            if(discoun_rate != 0):
                dis = (float(round(total_amount, 2)) *
                       float(discoun_rate) / 100)
                discountamount += dis

                discountrate = discoun_rate

                offerTxt = 'Get ₹' + \
                    str(round(discountamount, 2))+' instant discount'
            else:
                if(checkdiscountoffer and checkdiscountoffer.discount != 0 and payment_method.lower() != 'credit_card'):
                    dis = (float(round(total_amount, 2)) *
                           float(checkdiscountoffer.discount) / 100)
                    discountamount += dis

                    discountrate = checkdiscountoffer.discount

                    offerTxt = 'Get ₹' + \
                        str(round(discountamount, 2))+' instant discount'

            if(payment_method.lower() == 'online'):
                grand_totla = round(total_amount, 2)
            else:
                grand_totla = round(total_amount, 2) - \
                    round(discountamount, 2)

            walletvisible = False
            if(walletbalance != "0.00"):
                walletvisible = True

            visible = True
            if(payment_method.lower() == 'online'):
                visible = False
            # NEW Total Cart Items
            final_count_total_items = 0
            if(len(finalcartitems) > 0):
                for total_cart_item in finalcartitems:
                    for fnal_product in total_cart_item['product_data']:
                        final_count_total_items += fnal_product['items']

            if(payment_method.lower() != 'cod'):

                order_summary = [
                    {
                        "name": "Items",
                        "code": "items",
                        "value": str(final_count_total_items),  # New Updated
                        "currency": False,
                        "visible": True,
                        "offer_text": ""
                    },
                    totalAmountObj,
                    {
                        "name": "Shipping Charge ",
                        "code": "shipping_charge",
                        "value": floatingValue(total_delivery),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    },
                    {
                        "name": "Discount",
                        "code": "discount",
                        "value": floatingValue(round(discountamount, 2)),
                        "currency": True,
                        "visible": visible,
                        "offer_text": offerTxt
                    },

                    {
                        "name": "Extra Discount",
                        "code": "extra_discount",
                        "value": savings_amount,
                        "currency": True,
                        "visible": savings_visible,
                        "offer_text": ""
                    },
                    {
                        "name": "Wallet Amount ",
                        "code": "wallet_amount",
                        "value": str('-') + walletbalance,
                        "currency": True,
                        "visible": walletvisible,
                        "offer_text": ""
                    }

                ]
            else:
                order_summary = [
                    {
                        "name": "Items",
                        "code": "items",
                        # New Updated
                        "value": floatingValue(final_count_total_items),
                        "currency": False,
                        "visible": True,
                        "offer_text": ""
                    },
                    totalAmountObj,
                    {
                        "name": "Shipping Charge ",
                        "code": "shipping_charge",
                        "value": floatingValue(total_delivery),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    },
                    {
                        "name": "Discount",
                        "code": "discount",
                        "value": floatingValue(round(discountamount)),
                        "currency": True,
                        "visible": False,
                        "offer_text": offerTxt
                    },

                    {
                        "name": "Extra Discount",
                        "code": "extra_discount",
                        "value": savings_amount,
                        "currency": True,
                        "visible": savings_visible,
                        "offer_text": ""
                    },
                    {
                        "name": "Wallet Amount ",
                        "code": "wallet_amount",
                        "value": str('-') + walletbalance,
                        "currency": True,
                        "visible": walletvisible,
                        "offer_text": ""
                    },
                    {
                        "name": "Payable Amount",
                        "code": "payable_amount",
                        "value": floatingValue(round(total_amount, 0)),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    }

                ]

            # Grand Total
            grand_total = float(round(grand_totla, 2)) + \
                float(round(total_delivery, 0))

            rounded_value = round(grand_total, 0) - grand_total

            if(walletbalance != "0.00"):
                grand_total = (round(grand_total) - float(walletbalance))

            grand_total = {
                "title": "Grand Total",
                "payment_method": payment_method.upper(),
                "discount_rate": floatingValue(discountrate),
                "discount_amount": floatingValue(discountamount),
                "round_off": floatingValue(round(rounded_value, 2)),
                "amount": floatingValue(round(grand_total)),
                "partial": floatingValue(0.00),

            }

            # if(wallet_transaction.count() > 0):

            #     if(walletbalance != "0.00"):
            #         usable = False
            #     else:
            #         usable = True

            #     for transaction in wallet_transaction.all():

            #         if('DR' in str(transaction.type)):
            #             wallet_debit_balance += round(transaction.amount, 2)
            #         else:
            #             wallet_credit_balance += round(transaction.amount, 2)

            #     wallet_closing_balance = (round(wallet_credit_balance,
            #                                     2) - round(wallet_debit_balance, 2))
            #     wallet_closing_balance = floatingValue(
            #         round(wallet_closing_balance, 2))

            #     if(round(grandd_total) > 9000 and round(grandd_total) < 15000):
            #         if(wallet_closing_balance >= floatingValue(5000)):
            #             wallet_balance = {
            #                 'usable': usable,
            #                 'balance': wallet_closing_balance,
            #                 'use_balance': floatingValue(5000)
            #             }

            #     elif(round(grandd_total) > 15000 and round(grandd_total) < 21000):
            #         if(wallet_closing_balance >= floatingValue(7000)):
            #             wallet_balance = {
            #                 'usable': usable,
            #                 'balance': wallet_closing_balance,
            #                 'use_balance': floatingValue(7000)
            #             }
            #     elif(round(grandd_total) > 21000):
            #         if(wallet_closing_balance >= floatingValue(10000)):
            #             wallet_balance = {
            #                 'usable': usable,
            #                 'balance': wallet_closing_balance,
            #                 'use_balance': floatingValue(10000)
            #             }
            #     else:
            #         wallet_balance = {
            #             'usable': usable,
            #             'balance': wallet_closing_balance,
            #             'use_balance': floatingValue(0)
            #         }

            return {'status_code': HTTP_200_OK, "message": "Success", "title": title, 'online_payment_options': ChoosePaymentOption, 'order_summary': order_summary, "grand_total": grand_total}

        # cartitems: CartModel = await CartHelper.orderSummary(request=request, db=db, data=data, payment_method=payment_method)
        # return cartitems
        else:
            return {'status_code': HTTP_200_OK, "message": "No data found", "payment_method": payment_method, 'order_summary_text': [], "grand_total": {}}

    except Exception as e:
        print(e)


@v4_checkout_router.post("/order", dependencies=[Depends(JWTBearer())])
async def checkout(request: Request, data: checkOutSchema, db: Session = Depends(get_db)):
    userdata = auth(request=request)
    # New changes
    userdata = db.query(UserModel).filter(
        UserModel.id == userdata['id']).first()

    userdata = {
        'id': userdata.id,
        'name': userdata.name,
        'email': userdata.email,
        'mobile': userdata.mobile,
        'region': userdata.region,
        'city': userdata.city,
        'pincode': userdata.pincode,
    }

    try:

        # Update User Proof
        user: UserModel = await updateProof(db=db, user_id=userdata['id'], data=data)

        if(user == True):

            if(data.payment_type == 'ONLINE'):
                paytmParams = dict()
                paytmParams["body"] = {
                    "mid": config.PAYTM_MID_KEY,
                    "orderId": data.txn_order_id,
                }
                checksum = PaytmChecksum.generateSignature(
                    json.dumps(paytmParams["body"]), config.PAYTM_SECRET_KEY)
                paytmParams["head"] = {
                    "signature": checksum
                }

                post_data = json.dumps(paytmParams)

                # for Staging
                # url = "https://securegw-stage.paytm.in/v3/order/status"

                # for Production
                url = "https://securegw.paytm.in/v3/order/status"

                response = requests.post(url, data=post_data, headers={
                    "Content-type": "application/json"}).json()

                check_txn_status = response['body']['resultInfo']['resultStatus']
                # if(check_txn_status == 'TXN_SUCCESS' and response['body']['resultInfo']['resultCode'] == '01' and float(response['body']['txnAmount']) != float(data.total_amount)):
                #     return {"status_code": HTTP_200_OK, "status": "failed", "message": "Transaction Failed! Order not placed"}

                # Order Checkout #RAHUL
                if(data.txn_order_id == ''):  # new Changes
                    data.partial_amount = 0
                cartSummaryData: OrdersModel = await CheckOutHelper.checkOut(db=db, data=data, user_id=userdata['id'], txn_status=check_txn_status)
                # if(cartSummaryData['txn_status'] == 'TXN_FAILURE'):
                #     return {"status_code": HTTP_200_OK, "status": "failed", "message": "Order Failed "}

                # Order Refference ID
                order_ref_id = "ASEZ" + \
                    str(cartSummaryData['max_reff'])+"U"+str(userdata['id'])

                if(order_ref_id == False):
                    return {"status_code": HTTP_200_OK, "status": "failed", "message": "Order not placed "}

                txn_total_amount = float(
                    data.total_amount)
                if(check_txn_status == 'TXN_SUCCESS'):
                    dbtransaction = TransactionsModel(
                        txn_payment_id=response['body']['txnId'],
                        txn_order_id=response['body']['orderId'],
                        payment_method=response['body']['paymentMode'],
                        order_ref_id=cartSummaryData['max_reff'],
                        amount=txn_total_amount,
                        status=response['body']['resultInfo']['resultStatus'],
                        email=userdata['email'],
                        contact=userdata['mobile'],
                        created_at=datetime.now(),
                        updated_at=datetime.now()
                    )

                    db.add(dbtransaction)
                    db.commit()
                    # db.refresh(dbtransaction)
                # New Update (2023-07-28)
                if(check_txn_status == 'TXN_SUCCESS' and response['body']['resultInfo']['resultCode'] == '01' and float(response['body']['txnAmount']) != float(data.total_amount)):
                    # Check Orders
                    ordersdata = db.query(OrdersModel).filter(
                        OrdersModel.reff == cartSummaryData['max_reff']).all()
                    # Insert Order Failed Data
                    for orderdata in ordersdata:
                        db_orderfailed = OrderFailsModel(
                            order_id=orderdata.id,
                            payment_type=orderdata.payment_method,
                            created_at=orderdata.created_at,
                            updated_at=orderdata.created_at
                        )
                        db.add(db_orderfailed)
                        db.commit()
                        # Insert Payment failed Data
                        db_paymentfailed = PaymentFailedModel(
                            order_fail_id=db_orderfailed.id,
                            txn_payment_id=response['body']['txnId'],
                            txn_order_id=response['body']['orderId'],
                            txn_status='TXN_FAILURE',
                            reason='Fraud',
                            created_at=orderdata.created_at,
                            updated_at=orderdata.created_at
                        )
                        db.add(db_paymentfailed)
                        db.commit()
                    await Message.sendorderfailedmsz(name=userdata['name'],  mobile=userdata['mobile'])    
                    return {"status_code": HTTP_200_OK, "status": "failed", "message": "Transaction Failed! Order not placed"}
                # New Update (2023-07-28)
                if(cartSummaryData['txn_status'] == 'TXN_FAILURE'):
                    # Check Orders
                    ordersdata = db.query(OrdersModel).filter(
                        OrdersModel.reff == cartSummaryData['max_reff']).all()
                    # Insert Order Failed Data
                    for orderdata in ordersdata:
                        db_orderfailed = OrderFailsModel(
                            order_id=orderdata.id,
                            payment_type=orderdata.payment_method,
                            created_at=orderdata.created_at,
                            updated_at=orderdata.created_at
                        )
                        db.add(db_orderfailed)
                        db.commit()
                        # Insert Payment failed Data
                        db_paymentfailed = PaymentFailedModel(
                            order_fail_id=db_orderfailed.id,
                            txn_payment_id=response['body']['txnId'],
                            txn_order_id=response['body']['orderId'],
                            txn_status=check_txn_status,
                            reason=response['body']['resultInfo']['resultMsg'],
                            created_at=orderdata.created_at,
                            updated_at=orderdata.created_at
                        )
                        db.add(db_paymentfailed)
                        db.commit()
                    await Message.sendorderfailedmsz(name=userdata['name'],  mobile=userdata['mobile'])    
                    return {"status_code": HTTP_200_OK, "status": "failed", "message": "Order Failed "}
                # New Update Today (2023-08-05)
                if(check_txn_status == 'PENDING'):
                    # Check Orders
                    ordersdata = db.query(OrdersModel).filter(
                        OrdersModel.reff == cartSummaryData['max_reff']).all()
                    # Insert Order Failed Data
                    for orderdata in ordersdata:
                        db_orderfailed = OrderFailsModel(
                            order_id=orderdata.id,
                            payment_type=orderdata.payment_method,
                            created_at=orderdata.created_at,
                            updated_at=orderdata.created_at
                        )
                        db.add(db_orderfailed)
                        db.commit()
                        # Insert Payment failed Data
                        db_paymentfailed = PaymentFailedModel(
                            order_fail_id=db_orderfailed.id,
                            txn_payment_id=response['body']['txnId'],
                            txn_order_id=response['body']['orderId'],
                            txn_status=check_txn_status,
                            reason=response['body']['resultInfo']['resultMsg'],
                            created_at=orderdata.created_at,
                            updated_at=orderdata.created_at
                        )
                        db.add(db_paymentfailed)
                        db.commit()
                    await Message.sendorderfailedmsz(name=userdata['name'],  mobile=userdata['mobile'])        
                    return {"status_code": HTTP_200_OK, "status": "failed", "message": "Order Failed "}
                # Update Wallet Balance
                if(data.wallet_amount != '0.00'):
                    # Debit Wallet Amount
                    dbusedwallet = WalletModel(
                        user_id=userdata['id'],
                        reff_id=cartSummaryData['max_reff'],
                        reff_type='USED',
                        description='Ordered (Reff ID: ' +
                        "ASEZ" +
                        str(cartSummaryData['max_reff'])+"U" +
                        str(userdata['id'])+str(')'),
                        type='DR',
                        amount=data.wallet_amount,
                        status=1,
                        created_at=datetime.now(),
                        updated_at=datetime.now()
                    )
                    db.add(dbusedwallet)
                    db.commit()
                # Credit Wallet Amount #New Changes
                if(data.itemtotalamount != '0.00'):
                    # credit_rewards = (
                    #     float(data.itemtotalamount) * float(REWARDS_PERCENTAGE)) / 100
                    # credit_rewards = int(credit_rewards)
                    dbrewardswallet = WalletModel(
                        user_id=userdata['id'],
                        reff_id=cartSummaryData['max_reff'],
                        reff_type='ORDER_REWARDS',
                        description='Ordered (Reff ID: ' +
                        "ASEZ" +
                        str(cartSummaryData['max_reff'])+"U" +
                        str(userdata['id'])+str(')'),
                        type='CR',
                        amount=int(data.itemtotalamount),
                        status=0,
                        created_at=datetime.now(),
                        updated_at=datetime.now()
                    )
                    db.add(dbrewardswallet)
                    db.commit()

                # Update Wallet in Orders table
                if(data.wallet_amount != '0.00' and order_ref_id != False):
                    db.query(OrdersModel).filter(OrdersModel.reff == cartSummaryData['max_reff']).update(
                        {'use_wallet': 'Yes', 'wallet_reff_id': dbusedwallet.id})
                    db.commit()

                return {"status_code": HTTP_200_OK, "status": "success", "order_reff_id": order_ref_id, "message": 'Thank you for ordering'}

            else:
                check_txn_status = 'TXN_SUCCESS'
                # Check Partial Payment (New)
                txn_partial_amount = 0  # new update
                if(data.partial_amount != 0 and data.txn_order_id != ''):
                    # if(data.payment_type == 'PARTIAL'):  # New Changes
                    txn_partial_amount = data.partial_amount
                    paytmParams = dict()
                    paytmParams["body"] = {
                        "mid": config.PAYTM_MID_KEY,
                        "orderId": data.txn_order_id,
                    }
                    checksum = PaytmChecksum.generateSignature(
                        json.dumps(paytmParams["body"]), config.PAYTM_SECRET_KEY)
                    paytmParams["head"] = {
                        "signature": checksum
                    }

                    post_data = json.dumps(paytmParams)

                    url = "https://securegw.paytm.in/v3/order/status"

                    response = requests.post(url, data=post_data, headers={
                        "Content-type": "application/json"}).json()

                    check_txn_status = response['body']['resultInfo']['resultStatus']
                    # if(check_txn_status == 'TXN_SUCCESS' and response['body']['resultInfo']['resultCode'] == '01' and float(response['body']['txnAmount']) != float(txn_partial_amount)):
                    #     return {"status_code": HTTP_200_OK, "status": "failed", "message": "Transaction Failed! Order not placed"}

                # Order Checkout #RAHUL
                # if(data.txn_order_id == ''):  # new Changes
                if(data.payment_type == 'COD' and data.txn_order_id == ''):
                    data.partial_amount = 0
                cartSummaryData: OrdersModel = await CheckOutHelper.checkOut(db=db, data=data, user_id=userdata['id'], txn_status=check_txn_status)
                # if(cartSummaryData['txn_status'] == 'TXN_FAILURE'):
                #     return {"status_code": HTTP_200_OK, "status": "failed", "message": "Order Failed "}

                # Insert Partial Payment Transaction (New)
                if(txn_partial_amount != 0 and data.txn_order_id != ''):
                    # if(data.payment_type == 'PARTIAL'):  # New Changes
                    if(check_txn_status == 'TXN_SUCCESS'):
                        dbtransaction = TransactionsModel(
                            txn_payment_id=response['body']['txnId'],
                            txn_order_id=response['body']['orderId'],
                            payment_method=response['body']['paymentMode'],
                            order_ref_id=cartSummaryData['max_reff'],
                            amount=txn_partial_amount,
                            status=response['body']['resultInfo']['resultStatus'],
                            email=userdata['email'],
                            contact=userdata['mobile'],
                            created_at=datetime.now(),
                            updated_at=datetime.now()
                        )

                        db.add(dbtransaction)
                        db.commit()
                        # db.refresh(dbtransaction)
                # New Update (2023-07-28)
                if(data.partial_amount != 0 and data.txn_order_id != ''):
                    if(check_txn_status == 'TXN_SUCCESS' and response['body']['resultInfo']['resultCode'] == '01' and float(response['body']['txnAmount']) != float(txn_partial_amount)):
                        # Check Orders
                        ordersdata = db.query(OrdersModel).filter(
                            OrdersModel.reff == cartSummaryData['max_reff']).all()
                        # Insert Order Failed Data
                        for orderdata in ordersdata:
                            db_orderfailed = OrderFailsModel(
                                order_id=orderdata.id,
                                payment_type=orderdata.payment_method,
                                created_at=orderdata.created_at,
                                updated_at=orderdata.created_at
                            )
                            db.add(db_orderfailed)
                            db.commit()
                            # Insert Payment failed Data
                            db_paymentfailed = PaymentFailedModel(
                                order_fail_id=db_orderfailed.id,
                                txn_payment_id=response['body']['txnId'],
                                txn_order_id=response['body']['orderId'],
                                txn_status=check_txn_status,
                                reason='Fraud',
                                created_at=orderdata.created_at,
                                updated_at=orderdata.created_at
                            )
                            db.add(db_paymentfailed)
                            db.commit()
                        await Message.sendorderfailedmsz(name=userdata['name'],  mobile=userdata['mobile'])        
                        return {"status_code": HTTP_200_OK, "status": "failed", "message": "Transaction Failed! Order not placed"}
                # New Update (2023-07-28)
                if(data.partial_amount != 0 and data.txn_order_id != ''):
                    if(cartSummaryData['txn_status'] == 'TXN_FAILURE'):
                        # Check Orders
                        ordersdata = db.query(OrdersModel).filter(
                            OrdersModel.reff == cartSummaryData['max_reff']).all()
                        # Insert Order Failed Data
                        for orderdata in ordersdata:
                            db_orderfailed = OrderFailsModel(
                                order_id=orderdata.id,
                                payment_type=orderdata.payment_method,
                                created_at=orderdata.created_at,
                                updated_at=orderdata.created_at
                            )
                            db.add(db_orderfailed)
                            db.commit()
                            # Insert Payment failed Data
                            db_paymentfailed = PaymentFailedModel(
                                order_fail_id=db_orderfailed.id,
                                txn_payment_id=response['body']['txnId'],
                                txn_order_id=response['body']['orderId'],
                                txn_status=check_txn_status,
                                reason=response['body']['resultInfo']['resultMsg'],
                                created_at=orderdata.created_at,
                                updated_at=orderdata.created_at
                            )
                            db.add(db_paymentfailed)
                            db.commit()
                        await Message.sendorderfailedmsz(name=userdata['name'],  mobile=userdata['mobile'])        
                        return {"status_code": HTTP_200_OK, "status": "failed", "message": "Order Failed "}
                # new Update today (2023-08-08)
                if(data.partial_amount != 0 and data.txn_order_id != ''):
                    if(check_txn_status == 'PENDING'):
                        # Check Orders
                        ordersdata = db.query(OrdersModel).filter(
                            OrdersModel.reff == cartSummaryData['max_reff']).all()
                        # Insert Order Failed Data
                        for orderdata in ordersdata:
                            db_orderfailed = OrderFailsModel(
                                order_id=orderdata.id,
                                payment_type=orderdata.payment_method,
                                created_at=orderdata.created_at,
                                updated_at=orderdata.created_at
                            )
                            db.add(db_orderfailed)
                            db.commit()
                            # Insert Payment failed Data
                            db_paymentfailed = PaymentFailedModel(
                                order_fail_id=db_orderfailed.id,
                                txn_payment_id=response['body']['txnId'],
                                txn_order_id=response['body']['orderId'],
                                txn_status=check_txn_status,
                                reason=response['body']['resultInfo']['resultMsg'],
                                created_at=orderdata.created_at,
                                updated_at=orderdata.created_at
                            )
                            db.add(db_paymentfailed)
                            db.commit()
                        await Message.sendorderfailedmsz(name=userdata['name'],  mobile=userdata['mobile'])        
                        return {"status_code": HTTP_200_OK, "status": "failed", "message": "Order Failed "}
                # Insert Transaction
                if(txn_partial_amount != 0 and data.txn_order_id != ''):
                    data.payment_type = 'PARTIAL'
                # if(data.payment_type == 'PARTIAL'):  # New Changes
                    txn_total_amount = (float(
                        data.total_amount) - float(txn_partial_amount))  # New Changes

                if(data.payment_type == 'COD' and data.txn_order_id == ''):  # New Changes
                    txn_total_amount = float(
                        data.total_amount) + float(data.wallet_amount)  # New Changes

                dbtransaction = TransactionsModel(
                    payment_method=data.payment_type,  # New Changes
                    order_ref_id=cartSummaryData['max_reff'],
                    amount=txn_total_amount,
                    status="TXN_SUCCESS",
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )

                db.add(dbtransaction)
                db.commit()
                # db.refresh(dbtransaction)

                # Update Wallet Balance
                if(data.wallet_amount != '0.00'):
                    # Debit Wallet Amount
                    dbusedwallet = WalletModel(
                        user_id=userdata['id'],
                        reff_id=cartSummaryData['max_reff'],
                        reff_type='USED',
                        description='Ordered (Reff ID: ' +
                        "ASEZ" +
                        str(cartSummaryData['max_reff']) +
                        "U"+str(userdata['id'])+str(')'),
                        type='DR',
                        amount=data.wallet_amount,
                        status=1,
                        created_at=datetime.now(),
                        updated_at=datetime.now()
                    )
                    db.add(dbusedwallet)
                    db.commit()
                # Credit Wallet Amount #New Changes
                if(data.itemtotalamount != '0.00'):
                    # credit_rewards = (
                    #     float(data.itemtotalamount) * float(REWARDS_PERCENTAGE)) / 100
                    # credit_rewards = int(credit_rewards)
                    dbrewardswallet = WalletModel(
                        user_id=userdata['id'],
                        reff_id=cartSummaryData['max_reff'],
                        reff_type='ORDER_REWARDS',
                        description='Ordered (Reff ID: ' +
                        "ASEZ" +
                        str(cartSummaryData['max_reff']) +
                        "U"+str(userdata['id'])+str(')'),
                        type='CR',
                        amount=int(data.itemtotalamount),
                        status=0,
                        created_at=datetime.now(),
                        updated_at=datetime.now()
                    )
                    db.add(dbrewardswallet)
                    db.commit()

                # Order Refference ID
                order_ref_id = "ASEZ" + \
                    str(cartSummaryData['max_reff'])+"U"+str(userdata['id'])

                if(order_ref_id == False):
                    return {"status_code": HTTP_200_OK, "status": "failed", "message": "Order not placed "}
                # Update Wallet in Orders table
                if(data.wallet_amount != '0.00' and order_ref_id != False):
                    db.query(OrdersModel).filter(OrdersModel.reff == cartSummaryData['max_reff']).update(
                        {'use_wallet': 'Yes', 'wallet_reff_id': dbusedwallet.id})
                    db.commit()

                return {"status_code": HTTP_200_OK, "status": "success", "order_reff_id": order_ref_id, "message": 'Thank you for ordering'}

        else:
            return {"status_code": HTTP_200_OK, "status": "failed", "message": "Order not placed"}

    except Exception as e:
        print(e)


@v4_checkout_router.get("/payment/option/{payment_method}", dependencies=[Depends(JWTBearer())])
async def checkOut(request: Request, payment_method: str, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        # Check Delivery Charger for new user
        new_free_delivery = False
        if(config.FREE_DELIVERY == "True"):
            checkUserExistOrder = db.query(OrdersModel).filter(
                OrdersModel.user_id == userdata['id']).count()
            if(checkUserExistOrder == 0):
                new_free_delivery = True

        offer_Txt = ''

        # Check User facility
        buyer_facility = db.query(UserFacilityModel).filter(UserFacilityModel.user_id == userdata['id']).filter(
            UserFacilityModel.key_name == 'free_delivery').order_by(UserFacilityModel.id.desc()).first()

        # TEMPORARY CHANGES
        todaydate = datetime.now()
        today = todaydate.strftime("%Y-%m-%d")
        # Offer 08th to 14th Ocotber
        if(today > '2022-12-16' and today < '2022-12-23'):

            ChoosePaymentOption = [
                {

                    'value': 2,
                    'title': 'Debit Card',
                    'payment_type': 'DEBIT_CARD',
                    'description': 'Master Card, Rupay, Visa, American Express',
                    'offer_text': 'Get 2% Instant Discount',

                },
                {

                    'value': 1,
                    'title': 'Credit Card',
                    'payment_type': 'CREDIT_CARD',
                    'description': 'Master Card, Rupay, Visa, American Express',
                    'offer_text': 'Get 1% Instant Discount',

                },
                {

                    'value': 5,
                    'title': 'UPI',
                    'payment_type': 'UPI',
                    'description': 'GPay, PhonePe, Paytm etc.',
                    'offer_text': 'Get 5% Instant Discount'
                },
                {

                    'value': 2,
                    'title': 'Paytm Wallet',
                    'payment_type': 'BALANCE',
                    'description': 'Pay using paytm wallet',
                    'offer_text': 'Get 2% Instant Discount'
                },
                {

                    'value': 2,
                    'title': 'Net Banking',
                    'payment_type': 'NET_BANKING',
                    'description': 'HDFC Bank, ICICI Bank, State Bank of India, Axis',
                    'offer_text': 'Get 2% Instant Discount'
                }
            ]
        # Sale start from 15th Oct to 20th Oct
        # elif(today > '2022-10-14' and today < '2022-10-21'):

        #     ChoosePaymentOption = [
        #         {

        #             'value': 2,
        #             'title': 'Debit Card',
        #             'payment_type': 'DEBIT_CARD',
        #             'description': 'Master Card, Rupay, Visa, American Express',
        #             'offer_text': 'Get 2% Instant Discount',

        #         },
        #         {

        #             'value': 1,
        #             'title': 'Credit Card',
        #             'payment_type': 'CREDIT_CARD',
        #             'description': 'Master Card, Rupay, Visa, American Express',
        #             'offer_text': 'Get 1% Instant Discount',

        #         },
        #         {

        #             'value': 5,
        #             'title': 'UPI',
        #             'payment_type': 'UPI',
        #             'description': 'GPay, PhonePe, Paytm etc.',
        #             'offer_text': 'Get 5% Instant Discount'
        #         },
        #         {

        #             'value': 2,
        #             'title': 'Wallets',
        #             'payment_type': 'BALANCE',
        #             'description': 'Mobowiki, GPay, PhonePe, Paytm',
        #             'offer_text': 'Get 2% Instant Discount'
        #         },
        #         {

        #             'value': 2,
        #             'title': 'Net Banking',
        #             'payment_type': 'NET_BANKING',
        #             'description': 'HDFC Bank, ICICI Bank, State Bank of India, Axis',
        #             'offer_text': 'Get 2% Instant Discount'
        #         }
        #     ]

        else:
            ChoosePaymentOption = [
                {

                    'value': 2,
                    'title': 'Debit Card',
                    'payment_type': 'DEBIT_CARD',
                    'description': 'Master Card, Rupay, Visa, American Express',
                    'offer_text': 'Get 2% Instant Discount',

                },
                {

                    'value': 1,
                    'title': 'Credit Card',
                    'payment_type': 'CREDIT_CARD',
                    'description': 'Master Card, Rupay, Visa, American Express',
                    'offer_text': 'Get 1% Instant Discount',

                },
                {

                    'value': 2,
                    'title': 'UPI',
                    'payment_type': 'UPI',
                    'description': 'GPay, PhonePe, Paytm etc.',
                    'offer_text': 'Get 2% Instant Discount'
                },
                {

                    'value': 2,
                    'title': 'Paytm Wallet',
                    'payment_type': 'BALANCE',
                    'description': 'Pay using paytm wallet',
                    'offer_text': 'Get 2% Instant Discount'
                },
                {

                    'value': 2,
                    'title': 'Net Banking',
                    'payment_type': 'NET_BANKING',
                    'description': 'HDFC Bank, ICICI Bank, State Bank of India, Axis',
                    'offer_text': 'Get 2% Instant Discount'
                }
            ]

        # ChoosePaymentOption = [
        #     {

        #         'value': 2,
        #         'title': 'Debit Card',
        #         'payment_type': 'DEBIT_CARD',
        #         'description': 'Master Card, Rupay, Visa, Amaerican Express',
        #         'offer_text': 'Get 2% Instant Discount',

        #     },
        #     {

        #         'value': 1,
        #         'title': 'Credit Card',
        #         'payment_type': 'CREDIT_CARD',
        #         'description': 'Master Card, Rupay, Visa, Amaerican Express',
        #         'offer_text': 'Get 1% Instant Discount',

        #     },
        #     {

        #         'value': 2,
        #         'title': 'UPI',
        #         'payment_type': 'UPI',
        #         'description': 'GPay, PhonePe, Paytm etc.',
        #         'offer_text': 'Get 2% Instant Discount'
        #     },
        #     {

        #         'value': 2,
        #         'title': 'Wallets',
        #         'payment_type': 'BALANCE',
        #         'description': 'Mobowiki, GPay, PhonePe, Paytm',
        #         'offer_text': 'Get 2% Instant Discount'
        #     },
        #     {

        #         'value': 2,
        #         'title': 'Net Banking',
        #         'payment_type': 'NET_BANKING',
        #         'description': 'HDFC Bank, ICICI Bank, State Bank of India, Axis',
        #         'offer_text': 'Get 2% Instant Discount'
        #     }
        # ]

        # Check Payment Option
        discoun_rate = 0
        for paymentoption in ChoosePaymentOption:
            if(paymentoption['payment_type'].lower() == payment_method.lower()):
                discoun_rate = paymentoption['value']

        total_amount = 0
        total_delivery = 0
        grand_total = 0.00
        if(payment_method.lower() == 'online'):
            title = 'Online Payment'
        else:
            title = 'Partial Payment'
        cartitems = []
        total_items = []

        data = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).filter(
            CartModel.user_id == userdata['id']).filter(ProductModel.status == 51).filter(CartModel.status == 1).group_by(CartModel.seller_id).all()

        if(len(data) == 0):
            return {'status_code': HTTP_200_OK, "message": "No data found", "title": title, 'order_summary_text': [], "grand_total": {}}

        if(payment_method.lower() != 'cod'):

            finalcartdata = []
            for cartitems in data:

                in_stock_items = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(
                    CartModel.user_id == userdata['id']).filter(CartModel.seller_id == cartitems.seller_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).filter(CartModel.uuid.op('regexp')(ProductPricingModel.id)).filter(ProductPricingModel.deleted_at.is_(None)).filter(InventoryModel.out_of_stock == 0).filter(or_(CartModel.quantity <= InventoryModel.stock, InventoryModel.unlimited == 1)).group_by(CartModel.product_id).all()

                cartdata = []
                for cartitem in in_stock_items:
                    cartdata.append(cartitem)

                finalcartdata.append(cartdata)

                # CLOSED BY RAHUL (START)
                # checkcartitems = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).filter(
                #     CartModel.user_id == userdata['id']).filter(CartModel.seller_id == cartitems.seller_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).group_by(CartModel.product_id).all()

                # # Check Out Of stock Products
                # in_stock_items = []
                # for cartitem in checkcartitems:
                #     # Get Pricing ID
                #     pricing = cartitem.uuid.split('-')

                #     # Get Pricing
                #     pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                #         ProductPricingModel.deleted_at.is_(None)).first()

                #     # Check Pricing Exist
                #     if(pricing):

                #         # Inventory
                #         inventory = db.query(InventoryModel).filter(
                #             InventoryModel.pricing_id == pricing.id).first()

                #         # Check Pricing Stock
                #         if(inventory.out_of_stock == 0):
                #             in_stock_items.append(cartitem)

                # # Check Low Stock Products
                # cartdata = []
                # if(len(in_stock_items) > 0):
                #     for in_stock_item in in_stock_items:
                #         # Get Pricing ID
                #         pricing = in_stock_item.uuid.split('-')

                #         # Get Pricing
                #         pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                #             ProductPricingModel.deleted_at.is_(None)).first()

                #         # Check Pricing Exist
                #         if(pricing):

                #             # Pricing Inventory
                #             cartcheckinventory = db.query(InventoryModel).filter(
                #                 InventoryModel.pricing_id == pricing.id).first()

                #             # Check Cart Item Quantity with inventory Stock
                #             if(in_stock_item.quantity > cartcheckinventory.stock and cartcheckinventory.unlimited == 0):
                #                 pass
                #             else:
                #                 cartdata.append(in_stock_item)

                # finalcartdata.append(cartdata)
                # CLOSED BY RAHUL (END)

            for cartitemdata in finalcartdata:
                sub_total = 0
                for item in cartitemdata:

                    # Get Single Cart Data
                    singlecartdata = db.query(CartModel).where(CartModel.product_id == item.product_id).where(
                        CartModel.user_id == item.user_id).first()

                    # Get cart items by product id
                    itemsdata = db.query(CartModel).join(
                        ProductModel, ProductModel.id == CartModel.product_id).filter(CartModel.user_id == item.user_id).filter(CartModel.product_id == item.product_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).all()

                    # Get Pricing ID
                    itm_pricing_id = singlecartdata.uuid.split('-')
                    # Get Pricing
                    itm_pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == itm_pricing_id[1]).where(
                        ProductPricingModel.deleted_at.is_(None)).first()

                    if(itm_pricing):
                        # Inventory
                        checkinventory = db.query(InventoryModel).where(
                            InventoryModel.pricing_id == itm_pricing.id).first()

                        # Check Product Stock
                        # CLOSED BY RAHUL (START)
                        # if(checkinventory.out_of_stock == 0):
                        # CLOSED BY RAHUL (END)

                        for items in itemsdata:
                            # Get Product Detail
                            productdata = db.query(ProductModel).where(
                                ProductModel.id == items.product_id).first()

                            # Pricing ID
                            item_pricing_id = items.uuid.split('-')

                            # Get Pricing
                            item_pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == item_pricing_id[1]).where(
                                ProductPricingModel.deleted_at.is_(None)).first()

                            # Check Pricing Ecist
                            if(item_pricing):

                                # Inventory
                                checkiteminventory = db.query(InventoryModel).where(
                                    InventoryModel.pricing_id == item_pricing.id).first()

                                # Check Product Stock
                                if(checkiteminventory.out_of_stock == 0):
                                    # Aseztak Service
                                    # aseztak_service = Services.aseztak_services(
                                    #     item_pricing.updated_at, db=db)
                                    # today_date = item_pricing.updated_at.strftime(
                                    #     '%Y-%m-%d')
                                    todaydate = date.today()
                                    today_date = todaydate.strftime('%Y-%m-%d')
                                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                                    # Calculate Product Pricing
                                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, item_pricing, asez_service=aseztak_service, app_version='V4')
                                    # product_price = productPricecalculation(price=item_pricing.price, tax=item_pricing.tax, commission=aseztak_service.rate,
                                    #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                                    # Sub Total
                                    totalprice = round(
                                        product_price, 2) * items.quantity

                                    sub_total += totalprice
                                    total_items.append(items)

                # Today Date
                today = date.today()
                # YY-mm-dd
                today = today.strftime("%Y-%m-%d")

                # Check Shipping Charge
                delivery_charge = 0
                if(sub_total != 0 and new_free_delivery == False):
                    shipping_charge = db.execute("SELECT id, rate, start_date, payment_mode, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                                 {"param": today}).first()

                    if(buyer_facility):
                        freedeliveryamont = int(buyer_facility.key_value)
                        if(round(sub_total, 2) < round(freedeliveryamont, 2)):
                            if(int(buyer_facility.key_value) != 0):
                                delivery_charge = shipping_charge.rate
                    else:
                        delivery_charge = checkOrderPlaceDeliveryCalculation(
                            order_amount=sub_total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode='ONLINE')

                total_amount += round(sub_total, 2)

                total_delivery += delivery_charge

                # CHECK ORDER DISCOUNT
            checkdiscountoffer = db.execute("SELECT id, discount, start_date, payment_method FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
                                            {"param": today}).first()

            discountamount = 0.00
            discountrate = 0.00
            offerTxt = ""

            if(discoun_rate != 0):
                dis = (float(round(total_amount, 2)) *
                       float(discoun_rate) / 100)
                discountamount += dis

                discountrate = discoun_rate

                offerTxt = 'Get ₹' + \
                    str(round(discountamount, 2))+' instant discount'
            else:
                if(checkdiscountoffer and checkdiscountoffer.discount != 0):
                    dis = (float(round(total_amount, 2)) *
                           float(checkdiscountoffer.discount) / 100)
                    discountamount += dis

                    discountrate = checkdiscountoffer.discount

                    offerTxt = 'Get ₹' + \
                        str(round(discountamount, 2))+' instant discount'

            grand_totla = round(total_amount, 2) - \
                round(discountamount, 2)

            if(payment_method.lower() != 'cod'):

                order_summary = [
                    {
                        "name": "Items",
                        "value": str(len(total_items)),
                        "currency": False,
                        "visible": True,
                        "offer_text": ""
                    },
                    {
                        "name": "Total Amount",
                        "value": floatingValue(round(total_amount, 2)),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    },
                    {
                        "name": "Discount",
                        "value": floatingValue(round(discountamount, 2)),
                        "currency": True,
                        "visible": True,
                        "offer_text": offerTxt
                    },
                    {
                        "name": "Shipping Charge ",
                        "value": floatingValue(total_delivery),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    }
                ]
            else:
                order_summary = [
                    {
                        "name": "Items",
                        "value": floatingValue(len(cartitems)),
                        "currency": False,
                        "visible": True,
                        "offer_text": ""
                    },
                    {
                        "name": "Total Amount",
                        "value": floatingValue(round(total_amount, 0)),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    },
                    {
                        "name": "Discount",
                        "value": floatingValue(round(discountamount)),
                        "currency": True,
                        "visible": False,
                        "offer_text": offerTxt
                    },
                    {
                        "name": "Shipping Charge ",
                        "value": floatingValue(total_delivery),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    },
                    {
                        "name": "Payable Amount",
                        "value": floatingValue(round(total_amount, 0)),
                        "currency": True,
                        "visible": True,
                        "offer_text": ""
                    }

                ]

            # Grand Total
            grand_total = float(round(grand_totla, 2)) + \
                float(round(total_delivery, 0))
            rounded_value = round(grand_total, 0) - grand_total

            grand_total = {
                "title": "Grand Total",
                "payment_method": payment_method.upper(),
                "discount_rate": floatingValue(discountrate),
                "discount_amount": floatingValue(discountamount),
                "round_off": floatingValue(round(rounded_value, 2)),
                "amount": floatingValue(round(grand_total)),
                "partial": floatingValue(0.00),

            }

            return {'status_code': HTTP_200_OK, "message": "Success", "title": title, 'online_payment_options': ChoosePaymentOption, 'order_summary': order_summary, "grand_total": grand_total}

        # cartitems: CartModel = await CartHelper.orderSummary(request=request, db=db, data=data, payment_method=payment_method)
        # return cartitems
        else:
            return {'status_code': HTTP_200_OK, "message": "No data found", "payment_method": payment_method, 'order_summary_text': [], "grand_total": {}}

    except Exception as e:
        print(e)


# @v4_checkout_router.post("/order", dependencies=[Depends(JWTBearer())])
# async def checkout(request: Request, data: checkOutSchema, db: Session = Depends(get_db)):
#     userdata = auth(request=request)

#     try:

#         # Update User Proof
#         user: UserModel = await updateProof(db=db, user_id=userdata['id'], data=data)

#         if(user == True):

#             if(data.payment_type == 'ONLINE'):
#                 paytmParams = dict()
#                 paytmParams["body"] = {
#                     "mid": config.PAYTM_MID_KEY,
#                     "orderId": data.txn_order_id,
#                 }
#                 checksum = PaytmChecksum.generateSignature(
#                     json.dumps(paytmParams["body"]), config.PAYTM_SECRET_KEY)
#                 paytmParams["head"] = {
#                     "signature": checksum
#                 }

#                 post_data = json.dumps(paytmParams)

#                 # for Staging
#                 # url = "https://securegw-stage.paytm.in/v3/order/status"

#                 # for Production
#                 url = "https://securegw.paytm.in/v3/order/status"

#                 response = requests.post(url, data=post_data, headers={
#                     "Content-type": "application/json"}).json()

#                 if(response['body']['resultInfo']['resultStatus'] == 'TXN_FAILURE'):
#                     return {"status_code": HTTP_200_OK, "message": "Transaction Failed! Order not placed"}
#                 else:

#                     # Order Checkout #RAHUL
#                     cartSummaryData: OrdersModel = await CheckOutHelper.checkOut(db=db, data=data, user_id=userdata['id'])

#                     # Order Refference ID
#                     order_ref_id = "ASEZ" + \
#                         str(cartSummaryData)+"U"+str(userdata['id'])

#                     if(order_ref_id == False):
#                         return {"status_code": HTTP_200_OK, "message": "Order not placed "}

#                     dbtransaction = TransactionsModel(
#                         txn_payment_id=response['body']['txnId'],
#                         txn_order_id=response['body']['orderId'],
#                         payment_method=response['body']['paymentMode'],
#                         order_ref_id=cartSummaryData,
#                         amount=data.total_amount,
#                         status=response['body']['resultInfo']['resultStatus'],
#                         email=userdata['email'],
#                         contact=userdata['mobile'],
#                         created_at=datetime.now(),
#                         updated_at=datetime.now()
#                     )

#                     db.add(dbtransaction)
#                     db.commit()
#                     db.refresh(dbtransaction)

#                     return {"status_code": HTTP_200_OK, "order_reff_id": order_ref_id, "message": 'Thank you for ordering'}

#             else:

#                 # Order Checkout #RAHUL
#                 cartSummaryData: OrdersModel = await CheckOutHelper.checkOut(db=db, data=data, user_id=userdata['id'])

#                 # Order Refference ID
#                 order_ref_id = "ASEZ" + \
#                     str(cartSummaryData)+"U"+str(userdata['id'])

#                 if(order_ref_id == False):
#                     return {"status_code": HTTP_200_OK, "message": "Order not placed "}

#                 return {"status_code": HTTP_200_OK, "order_reff_id": order_ref_id, "message": 'Thank you for ordering'}

#         else:
#             return {"status_code": HTTP_200_OK, "message": "Order not placed"}

#     except Exception as e:
#         print(e)
