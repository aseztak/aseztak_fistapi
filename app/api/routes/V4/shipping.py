from datetime import datetime

from starlette.requests import Request
from starlette.status import HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED, HTTP_404_NOT_FOUND, HTTP_200_OK
from app.db.models.shipping_address import ShippingAddressModel

from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from fastapi import APIRouter,  Depends
from app.api.helpers.address import AddressHelper
from app.db.schemas.address_schema import AddressListSchema, AddAddressSchema, DefaultAddressSchema, EditShippingAddressSchema, UpdateAddressSchema, DeleteAddressSchema
from app.resources.strings import *
import requests
from requests.structures import CaseInsensitiveDict
import json
from app.services.auth import auth

v4_address_router = APIRouter()

# Get all Shipping Address


@v4_address_router.get("/address/list", response_model=AddressListSchema, dependencies=[Depends(JWTBearer())])
async def getAllShippingAddress(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 15):

    userdata = auth(request=request)
    try:

        data: ShippingAddressModel = await AddressHelper.getShippingAddress(db=db, user_id=userdata['id'])

        if(data.count() == 0):
            return {"status_code": HTTP_404_NOT_FOUND}
        else:

            addressdata: ShippingAddressModel = data.limit(
                limit=limit).offset((page - 1) * limit).all()

            # Customize Aternative Phone
            addresses: ShippingAddressModel = await AddressHelper.customizeAddress(addressdata)

            # Total Records
            total_records = data.count()

            # Number of Total Pages
            total_pages = str(round((total_records/limit), 2))

            total_pages = total_pages.split('.')

            if(total_pages[1] != "0"):
                total_pages = int(total_pages[0]) + 1
            else:
                total_pages = int(total_pages[0])
            return {"status_code": HTTP_202_ACCEPTED, "addresses": addresses, "total_address": total_records, "current_page": page, "total_pages": round(total_pages, 0)}

    except Exception as e:
        print(e)

# Add Shipping Address


@v4_address_router.post("/address/add", dependencies=[Depends(JWTBearer())])
async def addShippingAddress(request: Request, data: AddAddressSchema, db: Session = Depends(get_db)):
    userdata = auth(request=request)
    try:

        address = await AddressHelper.addAddress(db=db, user_id=userdata['id'], data=data)
        if(address):
            return {"status_code": HTTP_200_OK, "message": "Successfully Added"}
        # else:
        #     return {"Status_code:": HTTP_304_NOT_MODIFIED, "message": "Invalid Input"}

    except Exception as e:
        print(e)


# Edit Shipping Address
@v4_address_router.get("/address/edit/{id}", response_model=EditShippingAddressSchema, dependencies=[Depends(JWTBearer())])
async def editShippingAddress(request: Request, id: int, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        data: ShippingAddressModel = await AddressHelper.editShippingAddress(db=db, id=id, user_id=userdata['id'])

        # Customize Aternative Phone
        address: ShippingAddressModel = await AddressHelper.customizeAddress(data)

        if(data):
            return {"status_code": HTTP_202_ACCEPTED, "address": address, "message": "Success"}
        else:
            return {"status_code": HTTP_404_NOT_FOUND, "address": {}, "message": 'Not Found'}

    except Exception as e:
        print(e)

# Update Shipping Address


@v4_address_router.post("/address/update/{id}", dependencies=[Depends(JWTBearer())])
async def updateShippingAddress(request: Request, data: UpdateAddressSchema, id: int, db: Session = Depends(get_db)):

    try:
        userdata = auth(request=request)

        address = await AddressHelper.updateAddress(db=db, id=id, user_id=userdata['id'], data=data)
        if(address):
            return {"status_code": HTTP_200_OK, "message": "Successfully Updated"}
        else:
            return {"Status_code:": HTTP_304_NOT_MODIFIED, "message": "Invalid Input"}

    except Exception as e:
        print(e)

# Delete Shipping Address


@v4_address_router.delete("/address/delete", dependencies=[Depends(JWTBearer())])
async def addressDelete(request: Request, data: DeleteAddressSchema, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)
        address = await AddressHelper.deleteAddressData(
            db=db, user_id=userdata['id'], data=data)

        if(address):
            return {"status_code": HTTP_202_ACCEPTED, "message": "Deleted"}
        else:
            return {"status_code": HTTP_304_NOT_MODIFIED, "message": "Not Modified"}
        return
    except Exception as e:
        print(e)


# Check Shipping
@v4_address_router.get("/check/pincode/{pincode}", dependencies=[Depends(JWTBearer())])
async def checkPincode(pincode: str):
    try:

        pincode = pincode
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/json"

        get_pincode_url = f"{API_URL}c/api/pin-codes/json/?token={API_TOKEN}&filter_codes={pincode}"
        get_data = requests.get(
            get_pincode_url, headers=headers)
        if get_data.status_code == 200:
            json_data = json.loads(get_data.content.decode('utf-8'))

            if(len(json_data['delivery_codes']) == 0):
                return {"status_code": HTTP_404_NOT_FOUND, "message": "Currently no delivery in this area"}
            else:
                return {"status_code": HTTP_202_ACCEPTED, "message": "Success"}
        else:
            return {"status_code": HTTP_404_NOT_FOUND, "message": "Something went wrong"}

        # test_get_response = requests.get(get_test_url)
        # # # inp_post_response = requests.post(get_inp_url , json=request_example)
        # # if inp_post_response .status_code == 200:
        # #     print(json.loads(test_get_response.content.decode('utf-8')))
        return

    except Exception as e:
        print(e)


@v4_address_router.post("/set-default-address", dependencies=[Depends(JWTBearer())])
async def setDefaultAddress(data: DefaultAddressSchema, db: Session = Depends(get_db)):
    try:
        address = db.query(ShippingAddressModel).filter(
            ShippingAddressModel.id == data.id).first()

        # Check Default Address
        check_default_address = db.query(ShippingAddressModel).filter(
            ShippingAddressModel.user_id == address.user_id).filter(ShippingAddressModel.default_address == 1).first()

        if(check_default_address is not None):
            check_default_address.default_address = 0
            db.flush()
            db.commit()

        address.default_address = 1
        address.updated_at = datetime.now()
        db.flush()
        db.commit()
        return {"status_code": HTTP_200_OK, "message": "Successfully Update"}
    except Exception as e:
        print(e)
