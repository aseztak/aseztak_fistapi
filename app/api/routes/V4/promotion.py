from starlette.status import  HTTP_200_OK

from app.db.config import get_db
from sqlalchemy.orm.session import Session
from starlette.requests import Request
from fastapi import APIRouter,  Depends
from app.services.auth_bearer import JWTBearer
from app.db.models.promotion import PromotionModel

v4_promotion_router = APIRouter()


@v4_promotion_router.get("/list",  dependencies=[Depends(JWTBearer())])
async def addProducts(request: Request, db: Session = Depends(get_db)):
    try:
        promotions = db.query(PromotionModel).order_by(
            PromotionModel.id.desc()).all()
        promotion_list = []
        for promotion in promotions:
            data = {
                'id': promotion.id,
                'image-link': promotion.link,
                'valid_upto': promotion.valid_upto.strftime("%Y-%m-%d")
            }
            promotion_list.append(data)
        return {"statuc_code": HTTP_200_OK, "promotions": promotion_list}
    except Exception as e:
        print(e)
