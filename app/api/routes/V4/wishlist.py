
from starlette.status import  HTTP_200_OK
from app.api.util.calculation import floatingValue, productPricecalculation
from app.db.models.media import ProductMediaModel
from app.db.models.products import FavouriteModel, ProductModel
from app.api.helpers.products import ProductModel
from app.db.models.user import UserModel
from app.api.helpers.calculation import *
from app.services.auth_bearer import JWTBearer
from app.db.config import  get_db
from sqlalchemy.orm.session import Session
from starlette.requests import Request

from fastapi import APIRouter,  Depends

from app.services.auth import auth
from app.db.schemas.wishlist_schema import DeleteWishList, WishlistSchema, AddWishList
from sqlalchemy.orm import joinedload


v4_wishlist_router = APIRouter()


@v4_wishlist_router.get("/", response_model=WishlistSchema, dependencies=[Depends(JWTBearer())])
async def showwishlist(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 15):
    try:

        userdata = auth(request=request)

        data = db.query(FavouriteModel).join(ProductModel, ProductModel.id == FavouriteModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(
            FavouriteModel.user_id == userdata['id']).group_by(FavouriteModel.product_id).order_by(FavouriteModel.id.desc())

        total_records = data.count()

        if(total_records == 0):
            return {"status_code": HTTP_200_OK, "wishlists": [], "current_page": page, "total_items": total_records, "total_pages": 0}

        wishdata: FavouriteModel = data.limit(
            limit=limit).offset((page - 1) * limit).all()

        # return data
        wishlist = []
        for wish in wishdata:

            # Product
            products = db.query(ProductModel).filter(
                ProductModel.id == wish.product_id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).first()

            # image
            image = products.images[0]

            if(image is not None):
                if(image.file_path is not None):
                    img = image.file_path
                else:
                    img = ''
            else:
                img = ''

            # Product Pricing
            product_price = products.product_pricing[0]

            if(product_price is not None):

                # Aseztak Service
                # aseztak_service = Services.aseztak_services(
                #     product_price.updated_at, db=db)
                today_date = product_price.updated_at.strftime(
                    '%Y-%m-%d')
                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                # Calculate Product Price
                if(aseztak_service):
                    price: ProductModel = await ProductsHelper.getPrice(db, products, product_price, asez_service=aseztak_service, app_version='V4')
                    # price = productPricecalculation(price=product_price.price, tax=product_price.tax, commission=aseztak_service.rate,
                    #                                 gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version='V4')

                else:

                    price = productPricecalculation(price=product_price.price, tax=product_price.tax, commission=0,
                                                    gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version='V4')

                price = {
                    'price': floatingValue(price),
                    'moq': product_price.moq,
                    'unit': product_price.unit
                }
                # New Changes (Rahul)
                product_deleted = False
                if(products.status != 51):
                    product_deleted = True
                product = {
                    'id': products.id,
                    'title': products.title,
                    'description': products.short_description,
                    'deleted': product_deleted,
                    'image': img,
                    'pricing': price
                }

                w_data = {
                    'id': wish.id,
                    'product': product,
                }
                wishlist.append(w_data)

        # Number of Total Pages
        total_pages = str(round((total_records/limit), 2))

        total_pages = total_pages.split('.')

        if(total_pages[1] != "0"):
            total_pages = int(total_pages[0]) + 1
        else:
            total_pages = int(total_pages[0])
        return {"status_code": HTTP_200_OK, "wishlists": wishlist, "current_page": page, "total_items": total_records, "total_pages": total_pages}
    except Exception as e:
        return {"status_code": HTTP_200_OK, "wishlists": wishlist, "current_page": page, "total_items": total_records, "total_pages": total_pages}


@v4_wishlist_router.post("/add", dependencies=[Depends(JWTBearer())])
async def create_wishlist(request: Request, data: AddWishList, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)
        checkWishlist = db.query(FavouriteModel).filter(FavouriteModel.user_id == userdata['id']).filter(
            FavouriteModel.product_id == data.product_id).first()

        if(checkWishlist is None):

            dbwishlist = FavouriteModel(
                user_id=userdata['id'], product_id=data.product_id)

            db.add(dbwishlist)
            db.commit()
            db.refresh(dbwishlist)

            wish = True

        else:
            dbwishlist = db.query(FavouriteModel).filter(FavouriteModel.user_id == userdata['id']).filter(
                FavouriteModel.product_id == data.product_id).delete()
            db.commit()

            wish = False

        return {"status_code": HTTP_200_OK, "message": "Success", "wish": wish}
    except Exception as e:
        return {"status_code": HTTP_200_OK, "message": "Invalid Input"}


# Delete Wish List
@v4_wishlist_router.delete("/delete", dependencies=[Depends(JWTBearer())])
async def deleteWishlist(request: Request, data: DeleteWishList, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        # check data
        checkdata = db.query(FavouriteModel).filter(
            FavouriteModel.user_id == userdata['id']).filter(FavouriteModel.id == data.id).first()

        if(checkdata is not None):

            db.query(FavouriteModel).filter(FavouriteModel.user_id ==
                                            userdata['id']).filter(FavouriteModel.id == data.id).delete()
            db.commit()

            return {"status_code": HTTP_200_OK, "message": "Success"}
        else:
            return {"status_code": HTTP_200_OK, "message": "Invalid Input"}

    except Exception as e:

        return {"status_code": HTTP_200_OK, "message": "Invalid Input"}
