

from starlette.requests import Request
from starlette.status import HTTP_200_OK
from app.api.helpers.calculation import *
from app.db.models.carts import CartModel
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from fastapi import APIRouter,  Depends
from app.db.schemas.cart_schema import CartData, SaveCartData, UpdateCartSchema, DeleteCartSchema, AddCartSchema
from app.api.helpers.cart import CartHelper
from app.services.auth import auth
from app.api.util.calculation import floatingValue
from datetime import date

cart_router = APIRouter()


# Add to Cart
@cart_router.post("/add", dependencies=[Depends(JWTBearer())])
async def addToCart(request: Request, data: AddCartSchema, db: Session = Depends(get_db)):

    userdata = auth(request=request)

    try:

        cart = await CartHelper.addTocart(
            db=db, data=data, user_id=userdata['id'])

        if(cart):
            return {"status_code": HTTP_200_OK, "message": "Added Successfully"}
        else:
            return {"status_code": HTTP_200_OK, "message": "Not Modified"}

    except Exception as e:
        print(e)


@cart_router.get('/all',  response_model=CartData, dependencies=[Depends(JWTBearer())])
# @cart_router.get('/all/{user_id}', response_model=CartData, dependencies=[Depends(JWTBearer())])
async def getCartAll(request: Request, db: Session = Depends(get_db)):

    userdata = auth(request=request)

    # Calculation Total Cart Price
    total_amount = 0

    try:
        data: CartModel = CartHelper.getAllCarts(
            db=db, user_id=userdata['id'])

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "place_order_button": False, "cart_items": [], "total_amount": 0.00, "total_items": 0, "message": ''}

        else:
            cartdata: CartModel = data.all()

            # Calculatin Cart Product Price
            for cart in cartdata:

                pricing = cart.uuid.split('-')

                # Check Deleted Pricing
                checkPricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                    ProductPricingModel.deleted_at.isnot(None)).first()
                if(checkPricing is not None):
                    db.query(CartModel).filter(
                        CartModel.id == cart.id).delete()
                    db.commit()

                # Check Stock
                pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                    ProductPricingModel.deleted_at.is_(None)).first()
                # Get Product Detail
                productdata = db.query(ProductModel).where(
                    ProductModel.id == cart.product_id).first()

                if(pricing):
                    # Aseztak Service
                    # aseztak_service = Services.aseztak_services(
                    #     pricing.updated_at, db=db)
                    # today_date = pricing.updated_at.strftime(
                    #     '%Y-%m-%d')
                    todaydate = date.today()
                    today_date = todaydate.strftime('%Y-%m-%d')
                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricing, asez_service=aseztak_service, app_version='V4')
                    # product_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
                    #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                    productprice = round(product_price, 2)
                    cart.price = productprice

                    # Calculate Product Total Price with Quantity
                    total = (productprice * cart.quantity)
                    cart.total = floatingValue(total)

                    # Check Inventory

                    cartcheckinventory = db.query(InventoryModel).where(
                        InventoryModel.pricing_id == pricing.id).first()

                    cart.moq = pricing.moq

                    cart.unit = pricing.unit

                    # Check Stock
                    cart.total_stock = cartcheckinventory.stock
                    stock = True
                    if(cartcheckinventory and cartcheckinventory.out_of_stock == 0):
                        stock = True
                    else:
                        stock = False

                    cart.stock = stock

                    # Check Unlimited Stock
                    unlimited = True
                    if(cartcheckinventory and cartcheckinventory.unlimited == 1):
                        unlimited = True
                    else:
                        unlimited = False

                    cart.unlimited = unlimited

                    # Check Out Of Stock Quantity
                    out_of_quantity = False
                    message = ''
                    if(cart.quantity > cartcheckinventory.stock and cartcheckinventory.unlimited == 0):
                        out_of_quantity = True
                        message = 'Only '+str(cartcheckinventory.stock) + \
                            ' '+str(pricing.unit)+' left'

                    cart.out_of_quantity = out_of_quantity
                    cart.message = message

                else:
                    # Calculate Product Total Price with Quantity
                    total = (cart.price * cart.quantity)
                    cart.total = floatingValue(total)

            # Product Data
            cartdata: CartModel = await CartHelper.productTitle(request=request, db=db, data=cartdata)

            # Check Out Of stock Products
            in_stock_items = []
            for cartitem in data.all():
                # Check Stock
                pricing = cartitem.uuid.split('-')
                pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                    ProductPricingModel.deleted_at.is_(None)).first()
                # Get Product Detail
                productdata = db.query(ProductModel).where(
                    ProductModel.id == cartitem.product_id).first()

                if(pricing):
                    inventory = db.query(InventoryModel).filter(
                        InventoryModel.pricing_id == pricing.id).first()
                    if(inventory.out_of_stock == 0):
                        in_stock_items.append(cartitem)

                        # Aseztak Service
                        # aseztak_service = Services.aseztak_services(
                        #     pricing.updated_at, db=db)
                        # today_date = pricing.updated_at.strftime(
                        #     '%Y-%m-%d')
                        todaydate = date.today()
                        today_date = todaydate.strftime('%Y-%m-%d')
                        aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                        product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricing, asez_service=aseztak_service, app_version='V4')
                        # product_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
                        #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                        productprice = round(product_price, 2)
                        # Calculate Product Total Price with Quantity
                        total = (productprice * cartitem.quantity)
                        total_amount += total

            # Check Low Stock Carts
            low_stock_products = []
            if(len(in_stock_items) > 0):
                for in_stock_item in in_stock_items:
                    # Check Stock
                    pricing = in_stock_item.uuid.split('-')
                    pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                        ProductPricingModel.deleted_at.is_(None)).first()

                    if(pricing):
                        cartcheckinventory = db.query(InventoryModel).filter(
                            InventoryModel.pricing_id == pricing.id).first()
                        if(in_stock_item.quantity > cartcheckinventory.stock and cartcheckinventory.unlimited == 0):
                            low_stock_products.append(in_stock_item)

            # Check Place Order
            # check_cart_items = (len(in_stock_items) - len(low_stock_products))

            if(len(low_stock_products) == 0):
                place_order_btn = True
                message = 'Go to checkout page'
            else:
                place_order_btn = False
                message = 'Before Checkout please check your cart items'

            return {"status_code": HTTP_200_OK, "place_order_button": place_order_btn, "cart_items": cartdata, "total_amount":  floatingValue(total_amount), "total_items": data.count(), "message": message}

    except Exception as e:
        print(e)


# Cart Update
@cart_router.post("/cart/update", dependencies=[Depends(JWTBearer())])
async def cartUpdate(request: Request, data: UpdateCartSchema, db: Session = Depends(get_db)):
    try:

        # User
        userdata = auth(request=request)

        # CheckCart Data
        checkdata = db.query(CartModel).filter(CartModel.id == data.id).filter(
            CartModel.user_id == userdata['id']).first()
        if(checkdata is None):
            return {"status_code": HTTP_200_OK, "message": "Not Modified"}

        cart = await CartHelper.updateCart(
            db=db, data=data)

        if(cart):
            return {"status_code": HTTP_200_OK, "message": "Updated"}
        else:
            return {"status_code": HTTP_200_OK, "message": "Not Modified"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "message": "Not Modified"}


# Cart Delete
@cart_router.delete("/cart/delete", dependencies=[Depends(JWTBearer())])
async def cartDelete(request: Request, data: DeleteCartSchema, db: Session = Depends(get_db)):
    try:
        # User
        userdata = auth(request=request)

        # Checkdata
        checkdata = db.query(CartModel).filter(CartModel.id == data.id).filter(
            CartModel.user_id == userdata['id']).first()
        if(checkdata is None):
            return {"status_code": HTTP_200_OK, "message": "Not Modified"}

        cart = await CartHelper.deleteCartData(
            db=db, data=data)

        if(cart):
            return {"status_code": HTTP_200_OK, "message": "Deleted"}
        else:
            return {"status_code": HTTP_200_OK, "message": "Not Modified"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "message": "Not Modified"}

# Cart Delete from checkout page


@cart_router.delete("/cart/delete/{product_id}", dependencies=[Depends(JWTBearer())])
async def cartDelete(request: Request, product_id: int, db: Session = Depends(get_db)):

    userdata = auth(request=request)
    try:
        cart = await CartHelper.deleteCartfromCheckout(
            db=db, user_id=userdata['id'], product_id=product_id)

        if(cart):
            return {"status_code": HTTP_200_OK, "message": "Deleted"}
        else:
            return {"status_code": HTTP_200_OK, "message": "Not Modified"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "message": "Not Modified"}

# Delete All Carts Data


@cart_router.delete("/delete/all", dependencies=[Depends(JWTBearer())])
async def deleteAll(request: Request, db: Session = Depends(get_db)):
    try:
        userdata = auth(request=request)

        db.query(CartModel).filter(
            CartModel.user_id == userdata['id']).delete()
        db.commit()

        return {"status_code": HTTP_200_OK, "message": "Deleted Successfully"}
    except Exception as e:
        return {"status_code": HTTP_200_OK, "message": "not Modified"}

# Cart Summary

# save for Later all


@cart_router.get('/save-for-later/all',  response_model=SaveCartData, dependencies=[Depends(JWTBearer())])
async def getCartAll(request: Request, db: Session = Depends(get_db), page: int = 1, limit: int = 10):

    userdata = auth(request=request)

    # Calculation Total Cart Price
    total_amount = 0

    try:
        data: CartModel = CartHelper.getAllSaveCarts(
            db=db, user_id=userdata['id'])

        if(data.count() == 0):
            return {"status_code": HTTP_200_OK, "place_order_button": False, "cart_items": [], "total_amount": 0.00, "total_items": 0, 'current_page': page, 'total_page': 0}

        else:
            cartdata: CartModel = data.limit(
                limit=limit).offset((page - 1) * limit).all()

            # Calculatin Cart Product Price
            for cart in cartdata:

                pricing = cart.uuid.split('-')

                # Check Deleted Pricing
                checkPricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                    ProductPricingModel.deleted_at.isnot(None)).first()
                if(checkPricing is not None):
                    db.query(CartModel).filter(
                        CartModel.id == cart.id).delete()
                    db.commit()

                # Check Stock
                pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                    ProductPricingModel.deleted_at.is_(None)).first()
                # Get Product Detail
                productdata = db.query(ProductModel).where(
                    ProductModel.id == cart.product_id).first()

                if(pricing):
                    # Aseztak Service
                    # aseztak_service = Services.aseztak_services(
                    #     pricing.updated_at, db=db)
                    # today_date = pricing.updated_at.strftime(
                    #     '%Y-%m-%d')
                    todaydate = date.today()
                    today_date = todaydate.strftime('%Y-%m-%d')
                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricing, asez_service=aseztak_service, app_version='V4')
                    # product_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
                    #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                    productprice = round(product_price, 2)
                    cart.price = productprice

                    # Calculate Product Total Price with Quantity
                    total = (productprice * cart.quantity)
                    cart.total = floatingValue(total)

                    # Check Inventory

                    cartcheckinventory = db.query(InventoryModel).where(
                        InventoryModel.pricing_id == pricing.id).first()

                    cart.moq = pricing.moq

                    cart.unit = pricing.unit

                    # Check Stock
                    cart.total_stock = cartcheckinventory.stock
                    stock = True
                    if(cartcheckinventory and cartcheckinventory.out_of_stock == 0):
                        stock = True
                    else:
                        stock = False

                    cart.stock = stock

                    # Check Unlimited Stock
                    unlimited = True
                    if(cartcheckinventory and cartcheckinventory.unlimited == 1):
                        unlimited = True
                    else:
                        unlimited = False

                    cart.unlimited = unlimited

                    # Check Out Of Stock Quantity
                    out_of_quantity = False
                    message = ''
                    if(cart.quantity > cartcheckinventory.stock and cartcheckinventory.unlimited == 0):
                        out_of_quantity = True
                        message = 'Only '+str(cartcheckinventory.stock) + \
                            ' '+str(pricing.unit)+' left'

                    cart.out_of_quantity = out_of_quantity
                    cart.message = message

                else:
                    # Calculate Product Total Price with Quantity
                    total = (cart.price * cart.quantity)
                    cart.total = floatingValue(total)

            # Product Data
            cartdata: CartModel = await CartHelper.productTitle(request=request, db=db, data=cartdata)

            # Check Out Of stock Products
            in_stock_items = []
            for cartitem in data.all():
                # Check Stock
                pricing = cartitem.uuid.split('-')
                pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                    ProductPricingModel.deleted_at.is_(None)).first()
                # Get Product Detail
                productdata = db.query(ProductModel).where(
                    ProductModel.id == cartitem.product_id).first()

                if(pricing):
                    inventory = db.query(InventoryModel).filter(
                        InventoryModel.pricing_id == pricing.id).first()
                    if(inventory.out_of_stock == 0):
                        in_stock_items.append(cartitem)

                        # Aseztak Service
                        # aseztak_service = Services.aseztak_services(
                        #     pricing.updated_at, db=db)
                        # today_date = pricing.updated_at.strftime(
                        #     '%Y-%m-%d')
                        todaydate = date.today()
                        today_date = todaydate.strftime('%Y-%m-%d')
                        aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                        product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricing, asez_service=aseztak_service, app_version='V4')
                        # product_price = productPricecalculation(price=pricing.price, tax=pricing.tax, commission=aseztak_service.rate,
                        #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                        productprice = round(product_price, 2)
                        # Calculate Product Total Price with Quantity
                        total = (productprice * cartitem.quantity)
                        total_amount += total

            # Check Low Stock Carts
            low_stock_products = []
            if(len(in_stock_items) > 0):
                for in_stock_item in in_stock_items:
                    # Check Stock
                    pricing = in_stock_item.uuid.split('-')
                    pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                        ProductPricingModel.deleted_at.is_(None)).first()

                    if(pricing):
                        cartcheckinventory = db.query(InventoryModel).filter(
                            InventoryModel.pricing_id == pricing.id).first()
                        if(in_stock_item.quantity > cartcheckinventory.stock and cartcheckinventory.unlimited == 0):
                            low_stock_products.append(in_stock_item)

            # Total Records
            total_records = data.count()

            # Number of Total Pages
            total_pages = str(round((total_records/limit), 2))

            total_pages = total_pages.split('.')

            if(total_pages[1] != '0'):
                total_pages = int(total_pages[0]) + 1
            else:
                total_pages = int(total_pages[0])

            return {"status_code": HTTP_200_OK, "cart_items": cartdata, "total_amount":  floatingValue(total_amount), "total_items": data.count(), "current_page": page, "total_page": total_pages}

    except Exception as e:
        print(e)


# Save for later
@cart_router.get("/save-for-later/{cart_id}", dependencies=[Depends(JWTBearer())])
async def saveForlater(request: Request, cart_id: int, db: Session = Depends(get_db)):
    try:
        # Update cart data
        cart = db.query(CartModel).filter(CartModel.id == cart_id).first()

        if(cart.status == 0):
            cart.status = 1
        else:
            cart.status = 0

        db.flush()
        db.commit()

        if(cart.status == 1):
            return {"status_code": HTTP_200_OK, "message": "Moved to cart"}
        else:
            return {"status_code": HTTP_200_OK, "message": "Saved for Later"}

    except Exception as e:
        return {"status_code": HTTP_200_OK, "message": "Invalid"}
