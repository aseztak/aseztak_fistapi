from fastapi import APIRouter

# from app.api.routes.users import users_router
# from app.api.routes.auth import auth_router

# from app.api.routes.home import home_router
from app.api.routes.settings import settings_router
from app.api.routes.test import test_router
from app.api.routes.users import users_router
from app.api.routes.product import product_router
from app.api.routes.wishlist import wishlist_router
# from app.api.routes.category_routes import categories_route
from app.api.routes.carts import cart_router
from app.api.routes.performance import performance_router
from app.api.routes.orders import order_router
from app.api.routes.shipping import address_router
from app.api.routes.page import page_router
from app.api.routes.tags import tags_router
from app.api.routes.pincodes import pincodes_router
# from app.api.routes.search import search_router
from app.api.routes.seller.test import seller_router
from app.api.routes.seller.product import seller_product_router
# from app.api.routes.seller.category import seller_categories_route
# from app.api.routes.seller.auth import seller_auth_router
# from app.api.routes.seller.users import seller_users_router
# from app.api.routes.seller.settings import seller_settings_router
from app.api.routes.seller.dashboard import seller_dashboard_router
from app.api.routes.seller.orders import seller_order_router
# from app.api.routes.seller.address import seller_address_router
# from app.api.routes.seller.fulfillment import seller_fulfillment_router
from app.api.routes.seller.invoicelist import seller_invoice_router
from app.api.routes.seller.inventories import seller_inventories_router
# from app.api.routes.seller.brand import seller_brand_router
# from app.api.routes.seller.account import seller_account_router
from app.api.routes.seller.page import seller_page_router
from app.api.routes.seller.helper import seller_helper_router
from app.api.routes.seller.test import seller_router
from app.api.routes.widgets import widgets_router
from app.api.routes.seller.transaction import seller_transaction_router
from app.api.routes.checkout import checkout_router
from app.api.routes.seller.payments import seller_payments_router

from app.api.routes.faqs import buyer_faq_router

# Admin APP
from app.api.routes.admin.orders import admin_app_order_router
from app.api.routes.admin.users import admin_app_users_router
from app.api.routes.admin.fulfillment_orders import admin_app_fulfillment_orders_router
from app.api.routes.admin.auth import admin_app_auth_router
from app.api.routes.admin.dashboard import admin_app_dashboard_router
from app.api.routes.admin.faqs import admin_app_faqs_router
from app.api.routes.admin.pages import admin_app_page_router
from app.api.routes.admin.products import admin_app_products_router
from app.api.routes.admin.notification_image import admin_app_notification_image_router
from app.api.routes.admin.banner import admin_app_banners_router
from app.api.routes.admin.commission import admin_app_commission_router
from app.api.routes.admin.widgets import admin_app_widgets_router
from app.api.routes.admin.new_auth import new_admin_app_auth_router
from app.api.routes.admin.new_dashboard import new_admin_app_dashboard_router
from app.api.routes.admin.new_orders import new_admin_app_order_router

# V4 APIS
from app.api.routes.V4.home import v4_home_router
from app.api.routes.V4.product import v4_product_router
from app.api.routes.V4.auth import v4_auth_router
from app.api.routes.V4.users import v4_users_router
from app.api.routes.V4.tags import v4_tags_router
from app.api.routes.V4.search import v4_search_router
from app.api.routes.V4.shipping import v4_address_router
from app.api.routes.V4.pages import v4_pages_router
from app.api.routes.V4.checkout import v4_checkout_router
from app.api.routes.V4.wishlist import v4_wishlist_router
from app.api.routes.V4.orders import v4_order_router
from app.api.routes.V4.brands import v4_brands
from app.api.routes.V4.sellers import v4_seller
from app.api.routes.V4.wallet import v4_wallet_router
from app.api.routes.V4.category_routes import v4_categories_route
from app.api.routes.V4.promotion import v4_promotion_router
from app.api.routes.V4.seller.inventories import v4_seller_inventories_router
from app.api.routes.V4.seller.product import v4_seller_product_router
from app.api.routes.V4.seller.orders import v4_seller_order_router
from app.api.routes.V4.seller.product_discount import v4_seller_product_discount_router
from app.api.routes.V4.seller.auth import v4_seller_auth_router
from app.api.routes.V4.seller.dashboard import v4_seller_dashboard_router
from app.api.routes.V4.seller.facility import v4_seller_facility_router
from app.api.routes.V4.seller.category import v4_seller_categories_route
from app.api.routes.V4.seller.address import v4_seller_address_router
from app.api.routes.V4.seller.account import v4_seller_account_router
from app.api.routes.V4.seller.brand import v4_seller_brand_router
from app.api.routes.V4.seller.fulfillment import v4_seller_fulfillment_router
from app.api.routes.V4.seller.transaction import v4_seller_transaction_router

# Marketer API
from app.api.routes.marketer.auth import marketer_auth_router
from app.api.routes.marketer.marketing import marketer_marketing_router

from app.api.routes.V4.seller.users import v4_seller_users_router
router = APIRouter()

route_path = ''

# router.include_router(auth_router, prefix=f"{route_path}/auth", tags=["auth"])
router.include_router(users_router, prefix=f"{route_path}/user", tags=["user"])
router.include_router(test_router, prefix=f"{route_path}/test", tags=["test"])
router.include_router(
    settings_router, prefix=f"{route_path}/settings", tags=["settings"])
# router.include_router(home_router, prefix=f"{route_path}/home", tags=["home"])

router.include_router(
    product_router, prefix=f"{route_path}/product", tags=["product"])

router.include_router(
    wishlist_router, prefix=f"{route_path}/wishlist", tags=["wishlist"])

# router.include_router(
#     categories_route, prefix=f"{route_path}/category", tags=["category"])

router.include_router(cart_router, prefix=f"{route_path}/cart", tags=["cart"])

router.include_router(performance_router,
                      prefix=f"{route_path}/buyer", tags=["buyer-performance"])

router.include_router(
    order_router, prefix=f"{route_path}/orders", tags=["orders"])

router.include_router(
    address_router, prefix=f"{route_path}/shipping", tags=["shipping"])
router.include_router(page_router, prefix=f"{route_path}/page", tags=["pages"])
router.include_router(tags_router, prefix=f"{route_path}/tags", tags=["tags"])
# router.include_router(
#     search_router, prefix=f"{route_path}/search", tags=["search"])

router.include_router(
    widgets_router, prefix=f"{route_path}/widgets", tags=["widgets"])

router.include_router(
    checkout_router, prefix=f"{route_path}/checkout", tags=["checkout"])


router.include_router(
    pincodes_router, prefix=f"{route_path}/pincodes", tags=["pincodes"])

router.include_router(
    buyer_faq_router, prefix=f"{route_path}/faqs", tags=["buyer faqs"])


# SELLER
route_path = "/seller"

router.include_router(
    seller_router, prefix=f"{route_path}", tags=["seller"])

router.include_router(seller_dashboard_router,
                      prefix=f"{route_path}/dashboard", tags=["dashboard"])

# router.include_router(seller_auth_router,
#                       prefix=f"{route_path}/auth", tags=["seller-auth"])

# router.include_router(seller_users_router,
#                       prefix=f"{route_path}/user", tags=["seller-user"])

# router.include_router(seller_settings_router,
#                       prefix=f"{route_path}/settings", tags=["seller-settings"])

# router.include_router(seller_account_router,
#                       prefix=f"{route_path}/account", tags=["seller-account"])

# router.include_router(seller_categories_route,
#                       prefix=f"{route_path}/categories", tags=["seller-category"])

router.include_router(seller_product_router,
                      prefix=f"{route_path}/product", tags=["seller-product"])

router.include_router(seller_order_router,
                      prefix=f"{route_path}/orders", tags=["seller-orders"])

# router.include_router(seller_address_router,
#                       prefix=f"{route_path}/warehouse", tags=["seller-warehouse"])


# router.include_router(seller_fulfillment_router,
#                       prefix=f"{route_path}/fulfillment", tags=["seller-fulfillment"])


router.include_router(seller_invoice_router,
                      prefix=f"{route_path}/invoice", tags=["seller-invoice"])


router.include_router(seller_inventories_router,
                      prefix=f"{route_path}/inventories", tags=["seller-inventories"])


# router.include_router(seller_brand_router,
#                       prefix=f"{route_path}/brand", tags=["seller-brand"])


router.include_router(seller_page_router,
                      prefix=f"{route_path}/page", tags=["seller-page"])


router.include_router(seller_helper_router,
                      prefix=f"{route_path}/helper", tags=["seller-helper"])

router.include_router(seller_router,
                      prefix=f"{route_path}/test", tags=["seller-test"])


router.include_router(seller_transaction_router,
                      prefix=f"{route_path}/transaction", tags=["seller-transaction"])

router.include_router(seller_payments_router,
                      prefix=f"{route_path}/payments", tags=["seller-payments"])
# ADMIN
route_path = "/admin"

router.include_router(
    admin_app_auth_router, prefix=f"{route_path}/auth", tags=["admin-auth"])

router.include_router(admin_app_order_router,
                      prefix=f"{route_path}/orders", tags=["admin-orders"])

router.include_router(admin_app_dashboard_router,
                      prefix=f"{route_path}/dashboard", tags=["admin-dashboard"])

router.include_router(admin_app_users_router,
                      prefix=f"{route_path}/users", tags=["admin-users"])

router.include_router(admin_app_fulfillment_orders_router,
                      prefix=f"{route_path}/fulfillment_orders", tags=["admin-fulfillment_orders"])


router.include_router(admin_app_products_router,
                      prefix=f"{route_path}/products", tags=["admin-products"])


router.include_router(admin_app_banners_router,
                      prefix=f"{route_path}/banners", tags=["admin-banners"])

router.include_router(admin_app_notification_image_router,
                      prefix=f"{route_path}/notification_image", tags=["admin-notification_image"])

router.include_router(admin_app_page_router,
                      prefix=f"{route_path}/page", tags=["admin-page"])


router.include_router(admin_app_faqs_router,
                      prefix=f"{route_path}/faqs", tags=["admin-faqs"])

router.include_router(admin_app_commission_router,
                      prefix=f"{route_path}/seller-commission", tags=["Seller Commission Bills"])

router.include_router(admin_app_widgets_router,
                      prefix=f"{route_path}/widgets", tags=["Admin App Widgets"])


# NEW ADMIN
route_path = "/admin-app"
router.include_router(
    new_admin_app_auth_router, prefix=f"{route_path}/auth", tags=["new-admin-auth"])
router.include_router(
    new_admin_app_dashboard_router, prefix=f"{route_path}/dashboard", tags=["new-admin-dashboard"])
router.include_router(
    new_admin_app_order_router, prefix=f"{route_path}/order", tags=["new-admin-order"])

# V4 Routes
route_path = "/v4"

router.include_router(v4_home_router,
                      prefix=f"{route_path}/home", tags=["V4 home"])


router.include_router(v4_auth_router,
                      prefix=f"{route_path}/auth", tags=["V4 Auth Routes"])

router.include_router(v4_users_router,
                      prefix=f"{route_path}/user", tags=["V4 User Routes"])


router.include_router(v4_product_router,
                      prefix=f"{route_path}/products", tags=["V4 Products Routes"])

router.include_router(v4_search_router,
                      prefix=f"{route_path}/search", tags=["V4 Search Results Tags and Products"])

router.include_router(v4_wishlist_router,
                      prefix=f"{route_path}/wishlist", tags=["V4 Wishlist"])


router.include_router(
    v4_address_router, prefix=f"{route_path}/shipping", tags=["V4 Shipping Address"])


router.include_router(
    v4_pages_router, prefix=f"{route_path}/pages", tags=["V4 Pages"])

router.include_router(v4_checkout_router,
                      prefix=f"{route_path}/checkout", tags=["V4 Checkout"])


# Buyer Order V4
router.include_router(v4_order_router,
                      prefix=f"{route_path}/orders", tags=["V4 Orders"])

# Brands
router.include_router(
    v4_brands, prefix=f"{route_path}/brands", tags=["V4 Brands"])


# Sellers
router.include_router(
    v4_seller, prefix=f"{route_path}/sellers", tags=["V4 Seller"])


# Buyer Waller
router.include_router(
    v4_wallet_router, prefix=f"{route_path}/wallet", tags=["V4 Wallet"])


# Buyer Category Search
router.include_router(
    v4_categories_route, prefix=f"{route_path}/category", tags=["V4 Category"])

# Promotion
router.include_router(
    v4_promotion_router, prefix=f"{route_path}/promotion", tags=["V4 Promotion"])


# Seller V4 routes
router.include_router(
    v4_seller_auth_router, prefix=f"{route_path}/seller/auth", tags=["V4 Seller Auth"]
)

router.include_router(
    v4_seller_users_router, prefix=f"{route_path}/seller/users", tags=["V4 Seller users"]
)


router.include_router(
    v4_seller_inventories_router, prefix=f"{route_path}/seller/inventories", tags=["V4 Seller Inventories"]
)

router.include_router(v4_seller_product_router,
                      prefix=f"{route_path}/seller/product", tags=["V4 Seller Products"])

router.include_router(v4_seller_order_router,
                      prefix=f"{route_path}/seller/orders", tags=["V4 Seller Orders"])

router.include_router(v4_seller_product_discount_router,
                      prefix=f"{route_path}/seller/product-discount", tags=["V4 Seller Product Discount"])

router.include_router(v4_seller_dashboard_router,
                      prefix=f"{route_path}/seller/dashboard", tags=["V4 Seller Dashboard"])

router.include_router(v4_seller_facility_router,
                      prefix=f"{route_path}/seller/facility", tags=["V4 Seller Facility"])

router.include_router(v4_seller_categories_route,
                      prefix=f"{route_path}/categories", tags=["V4 Seller Selectable Categories"])

router.include_router(v4_seller_address_router,
                      prefix=f"{route_path}/seller/warehouse", tags=["V4 Seller Warehouse"])

router.include_router(v4_seller_account_router,
                      prefix=f"{route_path}/seller/account", tags=["V4 Seller Account"])

router.include_router(v4_seller_brand_router,
                      prefix=f"{route_path}/seller/brand", tags=["V4 Seller Brand"])

router.include_router(v4_seller_fulfillment_router,
                      prefix=f"{route_path}/seller/fulfillment", tags=["V4 Seller Fullfillment"])

router.include_router(v4_seller_transaction_router,
                      prefix=f"{route_path}/seller/transaction", tags=["V4 Seller Transaction"])

# marketer api router
router.include_router(marketer_auth_router,
                      prefix=f"/marketer/auth", tags=["marketer auth"])

router.include_router(marketer_marketing_router,
                      prefix=f"/marketer", tags=["marketer marketing"])
