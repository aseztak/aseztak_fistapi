from app.db.models.widget_images import WidgetImagesModel
from app.db.models.widgets import WidgetsModel


from app.db.schemas.widget_schema import WidgetListSchema
from app.db.config import get_db
from sqlalchemy.orm.session import Session

from fastapi import APIRouter,  Depends
from starlette.status import HTTP_200_OK

widgets_router = APIRouter()


@widgets_router.get("/widgets/{param}", response_model=WidgetListSchema)
async def widgets(param: str = 'home', db: Session = Depends(get_db)):
    try:

        widgets = db.query(WidgetsModel).filter(
            WidgetsModel.widget_type == param).filter(WidgetsModel.slno != 0).filter(WidgetsModel.enabled == True).order_by(WidgetsModel.slno.asc()).all()

        widget_data = []

        if(len(widgets) > 0):
            for widget in widgets:

                images = db.query(WidgetImagesModel).filter(
                    WidgetImagesModel.widget_id == widget.id).all()

                image_data = []
                for image in images:
                    img = {
                        'image': image.image,
                        'got_to': image.go_to,
                        'id': image.go_to_id
                    }

                    image_data.append(img)

                wd = {
                    'id': widget.id,
                    'widget_name': widget.widget_name,
                    'title': widget.title,
                    'description': widget.description,
                    'no_of_images': widget.no_of_images,
                    'items': image_data
                }

                widget_data.append(wd)

        return {"status_code": HTTP_200_OK, "widgets": widget_data}

    except Exception as e:
        print(e)
