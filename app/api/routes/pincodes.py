import json
from starlette.requests import Request

from fastapi import APIRouter




pincodes_router= APIRouter()


@pincodes_router.get("/")
async def Pin(request: Request):
    with open('app/static/pincodes.json', 'r') as myfile:
        data=myfile.read()

    obj = json.loads(data)

    # json_formatted_str = json.dumps(obj, indent=2)

    # print(json_formatted_str)
    pins = []
    for pincodes in obj:
        pincode = {
            'officename': pincodes['officeName'],
            'pincode': pincodes['pincode'],
            'taluk': pincodes['taluk'],
            'districtName': pincodes['districtName'],
            'stateName': pincodes['stateName']
        }
        pins.append(pincode)
        return pins

       