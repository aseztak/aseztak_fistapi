

from app.db.config import get_db
from sqlalchemy.orm.session import Session
from sqlalchemy import func
from fastapi import APIRouter, Request,  Depends

from starlette.status import  HTTP_200_OK
from app.services.auth import auth
from app.services.auth_bearer import JWTBearer

from app.db.models.orders import OrdersModel

from app.db.models.order_status import OrderStatusModel

from app.db.models.user import UserModel


performance_router = APIRouter()


@performance_router.get("/statistic", dependencies=[Depends(JWTBearer())])
async def getDiscount(request: Request, db: Session = Depends(get_db)):
    try:

        userdata = auth(request=request)

        user = db.query(UserModel).filter(
            UserModel.id == userdata['id']).first()

        all = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.delivery_charge, OrdersModel.discount,
                       OrdersModel.discount_rate, OrdersModel.grand_total, OrdersModel.app_version).filter(OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()

        pending = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrderStatusModel.order_id, OrderStatusModel.status).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(
            OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 0).filter(
            OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()

        # accepted
        accepted = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrderStatusModel.order_id, OrderStatusModel.status).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(
            OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 10).filter(
            OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()
        # approved
        approved = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrderStatusModel.order_id, OrderStatusModel.status).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(
            OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 30).filter(
            OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()
        # packed
        packed = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrderStatusModel.order_id, OrderStatusModel.status).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(
            OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 40).filter(
            OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()
        # Shipped
        shipped = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrderStatusModel.order_id, OrderStatusModel.status).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 60).filter(
            OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()

        # Delivered
        delivered = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrderStatusModel.order_id, OrderStatusModel.status).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).filter(
            OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()

        # Return Declined
        return_declined = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrderStatusModel.order_id, OrderStatusModel.status).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 81).filter(
            OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()

        cancelled = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrderStatusModel.order_id, OrderStatusModel.status).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 980).filter(
            OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()

        return_orders = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrderStatusModel.order_id, OrderStatusModel.status).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) >= 90).filter(OrderStatusModel.status != 980).filter(
            OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()

        reversed_orders = db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrderStatusModel.order_id, OrderStatusModel.status).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 61).filter(
            OrdersModel.user_id == user.id).order_by(OrdersModel.id.desc()).count()

        if(all > 0):
            under_process = round(
                ((pending + accepted + approved + packed + shipped) / all) * 100)

            delivered_orders = round(
                ((delivered + return_declined) / all) * 100)

            returned_orders = round(
                ((reversed_orders + return_orders) / all) * 100)

            statistic = [{
                'title': 'Under Process',
                'value': under_process
            },

                {
                'title': 'Delivered',
                'value': delivered_orders
            },

                {
                'title': 'Returned',
                'value': returned_orders
            },

                {
                'title': 'Cancelled',
                'value': round(((cancelled / all) * 100))
            }]
        else:
            statistic = [{
                'title': 'Under Process',
                'value': 0
            },

                {
                'title': 'Delivered',
                'value': 0
            },

                {
                'title': 'Returned',
                'value': 0
            },

                {
                'title': 'Cancelled',
                'value': 0
            }]

        # User Data
        city = ''
        region = ''
        if(user.name is not None):
            if(user.city is not None):
                city = user.city
            if(user.region is not None):
                region = user.region

            user_data = {
                'name': user.name,
                'city': city,
                'region': region
            }
        else:
            user_data = {
                'name': '',
                'city': city,
                'region': region
            }
        return {"status_code": HTTP_200_OK, "user_data": user_data, "total_orders": all, "statistic": statistic}

        return order_count
    except Exception as e:
        print(e)
