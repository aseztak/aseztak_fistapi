from starlette.requests import Request

from app.api.helpers.calculation import *
from app.db.models.user import UserModel
from app.services.auth_bearer import JWTBearer
from app.db.config import get_db
from sqlalchemy.orm.session import Session
from fastapi import APIRouter,  Depends
from app.db.schemas.orders_schema import PaymentTokenSchema
from app.api.helpers.users import *
import requests
import json
from app.services.auth import auth
from paytmchecksum import PaytmChecksum
import uuid
from app.core import config
checkout_router = APIRouter()


@checkout_router.post("/onlinepayment/generate-token", dependencies=[Depends(JWTBearer())])
async def tokenGenerate(request: Request, tokenData: PaymentTokenSchema, db: Session = Depends(get_db)):
    userdata = auth(request=request)
    # New Changes
    userdata = db.query(UserModel).filter(
        UserModel.id == userdata['id']).first()
    try:
        paytmParams = dict()
        order_id = uuid.uuid4().int & (1 << 64)-1
        order_id = str(order_id)

        paytmParams["body"] = {
            "requestType": "Payment",
            "mid": config.PAYTM_MID_KEY,
            "websiteName": "YOUR_WEBSITE_NAME",
            "orderId": order_id,
            "callbackUrl": "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+order_id,
            "txnAmount": {
                "value": str(tokenData.amount),
                "currency": "INR",
            },
            "userInfo": {
                "custId": "CUST_"+str(userdata.id),  # New Changes
                "mobile": str(userdata.mobile),  # New Changes
                "email": str(userdata.email),  # New Changes
                "firstName": str(userdata.name),  # New Changes

            },
            "enablePaymentMode": [{"mode": tokenData.payment_mode}]

        }

        # Generate checksum by parameters we have in body
        # Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
        checksum = PaytmChecksum.generateSignature(
            json.dumps(paytmParams["body"]), config.PAYTM_SECRET_KEY)

        paytmParams["head"] = {
            "signature": checksum
        }

        post_data = json.dumps(paytmParams)

        # for Staging
        # url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid=ZDyxrW03476660872979&orderId="+order_id

        # for Production
        url = "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=" + \
            str(config.PAYTM_MID_KEY)+"&orderId="+order_id
        response = requests.post(url, data=post_data, headers={
            "Content-type": "application/json"}).json()

        if(response['body']['resultInfo']['resultStatus'] == 'S'):
            resp = {
                'message': 'Success',
                'order_id': order_id,
                'txnToken': response['body']['txnToken']
            }
        else:
            resp = {
                'message': 'Failed',
                'order_id': order_id,
                'txnToken': ''
            }
        return resp
        # base64_dict = base64.b64encode(data.data)

        # return base64_dict
        # return base64_dict
    except Exception as e:
        print(e)
