# from datetime import datetime, timedelta
# from starlette.requests import Request
# from app.api.helpers.admin_app_calculation import Calculations
# from app.api.helpers.admin_app_orderstaticstatus import OrderStatus
# from app.api.helpers.admin_app_orders import OrderHelper
# from app.db.models.order_items import OrderItemsModel
# from app.db.models.order_status import OrderStatusModel
# from app.db.models.orders import OrdersModel
# from app.api.util.admin_app_calculation import floatingValue, orderDeliveryCalculation, orderDiscountCalculation, productPricecalculation, roundOf
# from app.db.models.shippingcharge import ShippingChargeModel
# from app.services.auth import auth
# from starlette.status import HTTP_200_OK, HTTP_202_ACCEPTED, HTTP_304_NOT_MODIFIED, HTTP_404_NOT_FOUND, HTTP_401_UNAUTHORIZED
# from app.db.models.products import InventoryModel, ProductModel, ProductPricingAttributeModel, ProductPricingModel
# from app.services.auth_bearer import JWTBearer
# from app.db.config import get_db
# from sqlalchemy.orm.session import Session
# from app.api.helpers.admin_app_products import *
# from app.services.auth import auth
from fastapi import APIRouter
# from starlette.requests import Request
# from app.db.models.orderdiscount import OrderDiscountModel
# from app.db.models.shippingcharge import ShippingChargeModel
# from app.api.helpers.services import AsezServices
# from app.api.helpers.products import ProductsHelper
# from app.db.models.marketing_users import MarketerUserModel
# from app.db.models.marketing_users_client import MarketerUsersClientModel
# from app.db.schemas.user_schema import AddUsers, MarketingSchema
# from app.db.models.shipping_address import ShippingAddressModel
# from app.db.schemas.admin_orders_schema import AdminMarketerBuyerList, AdminMarketerOrderDetailsSchema, AdminMarketerorderDetails, AdminMarketerorderList, AdminOrderListSchema
marketer_marketing_router = APIRouter()

#CLOSED 2024-01-08
# @marketer_marketing_router.post('/marketing', dependencies=[Depends(JWTBearer())])
# async def getTodaysOrderList(request: Request, data: MarketingSchema, db: Session = Depends(get_db)):
#     try:
#         userdata = auth(request=request)

#         # today
#         today = datetime.now()
#         today = today.strftime("%Y-%m-%d")
#         # Statistic Orders
#         start_date = data.from_date
#         end_date = data.to_date
#         count = end_date - start_date

#         order_stat = []

#         for o in range((count.days) + 1):
#             newdate = (start_date + timedelta(days=o)).strftime('%Y-%m-%d')

#             day = (start_date + timedelta(days=o)).strftime('%d')

#             month = (start_date + timedelta(days=o)).strftime('%b')

#             week = (start_date + timedelta(days=o)).strftime('%A')

#             grand = 0

#             marketers = db.query(MarketerUsersClientModel).filter(
#                 MarketerUsersClientModel.marketer_id == userdata['id']).order_by(MarketerUsersClientModel.id.desc()).all()

#             todays_order = 0
#             marketer_id = 0
#             if(len(marketers) > 0):
#                 for marketer in marketers:

#                     order = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(UserModel, UserModel.id == OrdersModel.user_id).filter(UserModel.id == marketer.user_id).filter(func.date_format(OrdersModel.created_at, '%Y-%m-%d') >= newdate).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= newdate).filter(func.date_format(
#                             OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).group_by(OrdersModel.id)
#                     todays_order += order.count()
#                     if((order.count()) > 0):
#                         for order_value in order.all():

#                             # items = db.query(OrderItemsModel).filter(
#                             #     OrderItemsModel.order_id == order_value.id)
#                             items = order_value.order_items
#                             item = items.first()

#                             if(item is not None):

#                                 # Latest Status
#                                 current_status = order_value.order_status.order_by(
#                                     OrderStatusModel.id.desc()).first()

#                                 statuslist = OrderStatus.AllstatusList()
#                                 statustitle = ''
#                                 for s_title in statuslist:
#                                     if(current_status.status == s_title['status_id']):
#                                         statustitle = s_title['status_title']

#                                 # Calculate Items
#                                 total = 0
#                                 for item_data in items.all():
#                                     pricingdata = item_data.uuid.split('-')
#                                     pricingdata = db.query(ProductPricingModel).where(ProductPricingModel.id == pricingdata[1]).where(
#                                         ProductPricingModel.deleted_at.is_(None)).first()
#                                     # Get Product Detail
#                                     productdata = db.query(ProductModel).where(
#                                         ProductModel.id == item_data.product_id).first()

#                                     # Aseztak Service
#                                     # aseztak_service = Services.aseztak_services(
#                                     #     item.created_at, db=db)
#                                     today_date = item_data.created_at.strftime(
#                                         '%Y-%m-%d')
#                                     aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#                                     if(aseztak_service is None):

#                                         # Calculate Product Price
#                                         product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=0,
#                                                                                 gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order_value.app_version)

#                                         total += product_price * item_data.quantity
#                                     else:
#                                         product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=order_value.app_version, order_item_id=item_data.id)
#                                         # Calculate Product Price
#                                         # product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=aseztak_service.rate,
#                                         #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order_value.app_version)

#                                         total += product_price * item_data.quantity

#                                 # Check Order Discount
#                             discount_amount = 0
#                             order_date = order_value.created_at.strftime(
#                                 "%Y-%m-%d")
#                             if(order_value.discount != 0):
#                                 # CHECK ORDER DISCOUNT
#                                 order_discount = db.query(OrderDiscountModel).filter(
#                                     OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                                 discount_rate = order_discount.discount
#                                 if(order_value.discount_rate != 0):
#                                     discount_rate = order_value.discount_rate

#                                 discount_amount = orderDiscountCalculation(app_version=order_value.app_version,
#                                                                            order_amount=total, discount_amount=order_value.discount, discount_rate=discount_rate)

#                             # Check Shipping Charge
#                             # delivery_charge = 0
#                             # if(order_value.delivery_charge != 0): CHANGES RAHUL
#                             shipping_charge = db.query(ShippingChargeModel).filter(
#                                 ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#                             delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order_value.free_delivery, user_id=order_value.user_id, order_date=order_value.created_at,
#                                                                        app_version=order_value.app_version, order_amount=total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order_value.payment_method)

#                             # Grand Total
#                             if(order_value.app_version == 'V4'):
#                                 grand_total = order_value.grand_total
#                             else:
#                                 grand_total = (
#                                     total + delivery_charge) - discount_amount

#                             grand_total = round(grand_total)

#                             grand += grand_total

#                     buyer_count = db.query(MarketerUsersClientModel).filter(MarketerUsersClientModel.user_id == marketer.user_id).filter(func.date_format(MarketerUsersClientModel.created_at, '%Y-%m-%d') >= newdate).filter(func.date_format(
#                         MarketerUsersClientModel.created_at, '%Y-%m-%d') <= newdate).group_by(MarketerUsersClientModel.id).count()
#                     marketer_id += buyer_count

#                 ordr = {
#                     'date': day,
#                     'month': month,
#                     'day': week,
#                     'sale': grand,
#                     'order_count': todays_order,
#                     'buyer_count': marketer_id,
#                     'created_at': marketer.created_at

#                 }
#                 order_stat.append(ordr)

#         marketers = db.query(MarketerUsersClientModel).filter(
#             MarketerUsersClientModel.marketer_id == userdata['id']).order_by(MarketerUsersClientModel.id.desc()).all()
#         todays_order = []
#         if(len(marketers) > 0):
#             for marketer in marketers:
#                 orders = db.query(OrdersModel).join(UserModel, UserModel.id == OrdersModel.user_id).filter(UserModel.id == marketer.user_id).filter(func.date_format(
#                     OrdersModel.created_at,  "%Y-%m-%d") == today).order_by(OrdersModel.id.desc())
#                 order_total_amount = 0
#                 discount_amount = 0
#                 for orderdata in orders.all():
#                     user = db.query(UserModel).filter(
#                         UserModel.id == orderdata.user_id).first()

#                     # Check Items Calculate All Items
#                     items = db.query(OrderItemsModel).filter(
#                         OrderItemsModel.order_id == orderdata.id).all()
#                     if(len(items) > 0):
#                         total_amount = 0
#                         # Aseztak Service
#                         today_date = orderdata.created_at.strftime('%Y-%m-%d')
#                         aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#                         for item in items:

#                             # Pricing Object
#                             pricingdata = item.uuid.split('-')
#                             pricingdata = db.query(ProductPricingModel).filter(
#                                 ProductPricingModel.id == pricingdata[1]).first()
#                             # Product Object
#                             productdata = db.query(ProductModel).filter(
#                                 ProductModel.id == item.product_id).first()
#                             if(aseztak_service is None):

#                                 # Calculate Product Price
#                                 product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
#                                                                         gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=orderdata.app_version)

#                                 total_amount += product_price * item.quantity
#                             else:
#                                 # Get Product Price
#                                 product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=orderdata.app_version, order_item_id=item.id)
#                                 # Calculate Product Price

#                                 total_amount += product_price * item.quantity

#                         # Check Order Discount
#                         discount_amount = 0
#                         order_date = orderdata.created_at.strftime("%Y-%m-%d")
#                         if(orderdata.discount != 0):
#                             # CHECK ORDER DISCOUNT
#                             order_discount = db.query(OrderDiscountModel).filter(
#                                 OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                             discount_rate = order_discount.discount
#                             if(orderdata.discount_rate != 0):
#                                 discount_rate = orderdata.discount_rate

#                             discount_amount = orderDiscountCalculation(app_version=orderdata.app_version,
#                                                                        order_amount=total_amount, discount_amount=orderdata.discount, discount_rate=discount_rate)

#                         # Check Shipping Charge
#                         delivery_charge = 0
#                         # if(orderdata.delivery_charge != 0): CHANGES RAHUL
#                         shipping_charge = db.query(ShippingChargeModel).filter(
#                             ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#                         delivery_charge = orderDeliveryCalculation(db=db, free_delivery=orderdata.free_delivery, user_id=orderdata.user_id, order_date=orderdata.created_at,
#                                                                    app_version=orderdata.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=orderdata.payment_method)

#                         # Grand Total
#                         grand_total = (
#                             total_amount + delivery_charge) - discount_amount

#                         grand_total = round(grand_total)

#                         order_total_amount = grand_total

#                     buyer_address = db.query(ShippingAddressModel).filter(
#                         ShippingAddressModel.user_id == orderdata.user_id).first()

#                     marketerdata = {
#                         'order_id': orderdata.id,
#                         'order_no': orderdata.order_number,
#                         'order_amount': order_total_amount,
#                         'buyer_name': user.name,
#                         'buyer_address': str(buyer_address.locality) + ',' + str(buyer_address.city) + ',' + str(buyer_address.state),
#                         'created_at': orderdata.created_at
#                     }
#                     todays_order.append(marketerdata)

#         # buyers = db.query(UserModel).filter(func.date_format(
#         #     UserModel.created_at,  "%Y-%m-%d") == today).order_by(UserModel.id.desc()).all()
#         buyers = db.query(MarketerUsersClientModel).filter(MarketerUsersClientModel.marketer_id == userdata['id']).filter(
#             func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d") == today).order_by(MarketerUsersClientModel.id.desc()).all()
#         todays_buyer = []
#         if(len(buyers) > 0):
#             for buyer in buyers:

#                 buyerdata = {
#                     'id': buyer.user.id,
#                     'buyer_name': buyer.user.name,
#                     'address': str(buyer.user.city)+', '+str(buyer.user.region)+', '+str(buyer.user.country)+' - '+str(buyer.user.pincode),
#                     'created_at': buyer.user.created_at
#                 }
#                 todays_buyer.append(buyerdata)

#         marketer_agent = db.query(MarketerUserModel).filter(
#             MarketerUserModel.id == userdata['id']).first()
#         # Current Month Order Amount
#         month = datetime.now()
#         month = month.strftime("%m %Y")
#         # created_at = marketer_agent.created_at.strftime('%Y-%m-%d')
#         monthly_order_count = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrdersModel.id == OrderItemsModel.order_id).filter(OrderItemsModel.status.between(0, 81)).filter(OrderItemsModel.status != 80).filter(MarketerUsersClientModel.marketer_id == userdata['id']).filter(func.date_format(
#             OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, '%m %Y') == month).group_by(
#             OrderItemsModel.order_id).order_by(OrderItemsModel.id.desc()).all()
#         final_amount = 0
#         for order in monthly_order_count:
#             # Order Items
#             items = order.order_items.filter(OrderItemsModel.status.between(0, 81)).filter(
#                 OrderItemsModel.status != 80).filter(OrderItemsModel.status != 61).all()
#             total_amount = 0
#             today_date = order.created_at.strftime('%Y-%m-%d')
#             aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#             for item_data in items:
#                 # Pricing Object
#                 pricingdata = item_data.uuid.split('-')
#                 pricingdata = db.query(ProductPricingModel).filter(
#                     ProductPricingModel.id == pricingdata[1]).first()
#                 # Product Object
#                 productdata = db.query(ProductModel).filter(
#                     ProductModel.id == item_data.product_id).first()
#                 if(aseztak_service is None):
#                     # Calculate Product Price
#                     product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=0,
#                                                             gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)
#                     total_amount += product_price * item_data.quantity
#                 else:
#                     # Get Product Price
#                     product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item_data.id)
#                     total_amount += product_price * item_data.quantity

#             # Check Order Discount
#             discount_amount = 0
#             order_date = order.created_at.strftime("%Y-%m-%d")
#             if(order.discount != 0):
#                 # CHECK ORDER DISCOUNT
#                 order_discount = db.query(OrderDiscountModel).filter(
#                     OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()
#                 discount_rate = order_discount.discount
#                 if(order.discount_rate != 0):
#                     discount_rate = order.discount_rate
#                 discount_amount = orderDiscountCalculation(app_version=order.app_version,
#                                                            order_amount=total_amount, discount_amount=order.discount, discount_rate=discount_rate)
#             # Check Shipping Charge
#             shipping_charge = db.query(ShippingChargeModel).filter(
#                 ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#             delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order.free_delivery, user_id=order.user_id, order_date=order.created_at,
#                                                        app_version=order.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

#             grand_total = (
#                 total_amount + delivery_charge) - discount_amount
#             grand_total = round(grand_total)
#             final_amount += grand_total

#         return {"todays_order": todays_order, "order_statistics": order_stat, "todays_buyer": todays_buyer, "monthly_order_amount": final_amount}

#     except Exception as e:
#         print(e)

# Add User


# @marketer_marketing_router.post("/add/user", dependencies=[Depends(JWTBearer())])
# async def addUser(request: Request, data: AddUsers, db: Session = Depends(get_db)):
#     try:
#         userdata = auth(request=request)
#         # Check user
#         check_user = db.query(UserModel).filter(
#             UserModel.mobile == data.mobile).first()
#         if(check_user):

#             # Check already exist
#             check_marketer_user = db.query(MarketerUsersClientModel).filter(MarketerUsersClientModel.user_id == check_user.id).filter(
#                 MarketerUsersClientModel.marketer_id == userdata['id']).first()
#             if(check_marketer_user is None):

#                 marketer_data = MarketerUsersClientModel(
#                     marketer_id=userdata['id'],
#                     user_id=check_user.id,
#                     user_mobile=check_user.mobile,
#                     created_at=datetime.now(),
#                     updated_at=datetime.now()

#                 )
#                 db.add(marketer_data)
#                 db.commit()
#                 db.refresh(marketer_data)
#                 return {"status_code": HTTP_200_OK, "user_data": check_user.id, "message": "Added"}
#             else:
#                 return {"status_code": HTTP_200_OK, "user_data": 0, "message": "Already Exist"}
#     except Exception as e:
#         print(e)
#         return {"status_code": HTTP_200_OK, "user_data": 0, "message": "Not Modified"}

# # User list of marketer


# @marketer_marketing_router.get('/buyer-order/status/{id}', dependencies=[Depends(JWTBearer())])
# async def getBuyerOrderStatus(request: Request, id: int, db: Session = Depends(get_db)):
#     try:
#         userdata = auth(request=request)
#         # user details
#         user_details = db.query(UserModel).filter(UserModel.id == id).first()
#         user_details = {
#             'user_id': id,
#             'name': user_details.name,
#             'mobile': user_details.mobile,
#             'address': str(user_details.city)+str(', ')+str(user_details.region)+str(', ')+str(user_details.country)+str(' - ')+str(user_details.pincode),
#             'created_at': user_details.created_at

#         }

#         # Check marketing user client
#         check_marketer_user_client = db.query(MarketerUsersClientModel).filter(
#             MarketerUsersClientModel.user_id == id).filter(MarketerUsersClientModel.marketer_id == userdata['id']).first()
#         client_join_date = check_marketer_user_client.created_at.strftime(
#             "%Y-%m-%d")

#         delivered_order: OrdersModel = await OrderHelper.getBuyersOrder(
#             db=db, user_id=id, client_join_date=client_join_date, status='70', page=1, limit=1)
#         active_order: OrdersModel = await OrderHelper.getBuyersOrder(
#             db=db, user_id=id, client_join_date=client_join_date, status='60', page=1, limit=1)
#         cancelled_order: OrdersModel = await OrderHelper.getBuyersOrder(
#             db=db, user_id=id, client_join_date=client_join_date, status='980', page=1, limit=1)
#         returned_order: OrdersModel = await OrderHelper.getBuyersOrder(
#             db=db, user_id=id, client_join_date=client_join_date, status='80', page=1, limit=1)
#         # all_order = db.query(OrdersModel).join(UserModel, UserModel.id == OrdersModel.user_id).filter(UserModel.mobile == mobile_no).order_by(OrdersModel.id.desc())

#         all_order = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(UserModel, UserModel.id == OrdersModel.user_id).filter(
#             OrdersModel.user_id == id).filter(OrdersModel.user_id == id).filter(func.date_format(
#                 OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#         order_total_amount = 0
#         discount_amount = 0
#         delivered_order_total_amount = 0
#         active_order_total_amount = 0
#         cancel_order_total_amount = 0
#         return_order_total_amount = 0

#         for orderdata in all_order.all():

#             # Check Current Status
#             current_status = db.query(OrderStatusModel).filter(
#                 OrderStatusModel.order_id == orderdata.id).order_by(OrderStatusModel.id.desc()).first()

#             # Check Items Calculate All Items
#             items = db.query(OrderItemsModel).filter(
#                 OrderItemsModel.order_id == orderdata.id).all()
#             if(len(items) > 0):
#                 total_amount = 0
#                 cancel_total_amount = 0
#                 active_total_amount = 0
#                 return_total_amount = 0
#                 delivered_total_amount = 0
#                 # Aseztak Service
#                 today_date = orderdata.created_at.strftime('%Y-%m-%d')
#                 aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#                 for item in items:
#                     # Aseztak Service
#                     # aseztak_service = Services.aseztak_services(
#                     #     item.created_at, db=db)
#                     # Pricing Object
#                     pricingdata = item.uuid.split('-')
#                     pricingdata = db.query(ProductPricingModel).filter(
#                         ProductPricingModel.id == pricingdata[1]).first()
#                     # Product Object
#                     productdata = db.query(ProductModel).filter(
#                         ProductModel.id == item.product_id).first()
#                     if(aseztak_service is None):

#                         # Calculate Product Price
#                         product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
#                                                                 gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=orderdata.app_version)

#                         total_amount += product_price * item.quantity
#                     else:
#                         # Get Product Price
#                         product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=orderdata.app_version, order_item_id=item.id)

#                         total_amount += product_price * item.quantity

#                     # Check Delivered Items
#                     if(item.status == 70 or item.status == 81):
#                         delivered_total_amount += product_price * item.quantity

#                     # Check Cancelled Item Amount
#                     if(item.status == 980):
#                         cancel_total_amount += product_price * item.quantity

#                     # Check Active Item Amount
#                     if(current_status.status <= 60):
#                         active_total_amount += product_price * item.quantity

#                     # # Check Return Items Amount
#                     if(item.status >= 80 and item.status != 81 and item.status != 91 and item.status != 980):
#                         return_total_amount += product_price * item.quantity

#                 # Check Rest Amount Cancel Order
#                 rest_item_amount_for_cancel_order = (
#                     total_amount - cancel_total_amount)
#                 # Check Rest Amount Active Order
#                 rest_item_amount_for_active_order = (
#                     total_amount - active_total_amount)

#                 # Check Rest Amount Return Order
#                 rest_item_amount_for_return_order = (
#                     total_amount - return_total_amount)

#                 # Check Order Discount
#                 discount_amount = 0
#                 del_discont_amount = 0
#                 cancel_discount_amount = 0
#                 active_discount_amount = 0
#                 return_discount_amount = 0
#                 rest_discount_amount_active = 0
#                 rest_discount_amount = 0
#                 rest_discount_amount_two = 0
#                 order_date = orderdata.created_at.strftime("%Y-%m-%d")
#                 if(orderdata.discount != 0):
#                     # CHECK ORDER DISCOUNT
#                     order_discount = db.query(OrderDiscountModel).filter(
#                         OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                     discount_rate = order_discount.discount
#                     if(orderdata.discount_rate != 0):
#                         discount_rate = orderdata.discount_rate

#                     discount_amount = orderDiscountCalculation(app_version=orderdata.app_version,
#                                                                order_amount=total_amount, discount_amount=orderdata.discount, discount_rate=discount_rate)
#                     if(delivered_total_amount != 0):
#                         # Del Discount Amount
#                         del_discont_amount = orderDiscountCalculation(app_version=orderdata.app_version,
#                                                                       order_amount=delivered_total_amount, discount_amount=orderdata.discount, discount_rate=discount_rate)
#                     if(active_total_amount != 0):
#                         # Cancel Discount Amount
#                         active_discount_amount = orderDiscountCalculation(app_version=orderdata.app_version,
#                                                                           order_amount=active_total_amount, discount_amount=orderdata.discount, discount_rate=discount_rate)

#                         # # Rest Discount Amount
#                         rest_discount_amount_active = orderDiscountCalculation(app_version=orderdata.app_version,
#                                                                                order_amount=rest_item_amount_for_active_order, discount_amount=orderdata.discount, discount_rate=discount_rate)

#                         rest_item_amount_for_active_order = (
#                             rest_item_amount_for_active_order - rest_discount_amount_active)
#                     if(cancel_total_amount != 0):
#                         # Cancel Discount Amount
#                         cancel_discount_amount = orderDiscountCalculation(app_version=orderdata.app_version,
#                                                                           order_amount=cancel_total_amount, discount_amount=orderdata.discount, discount_rate=discount_rate)

#                         # Rest Discount Amount
#                         rest_discount_amount = orderDiscountCalculation(app_version=orderdata.app_version,
#                                                                         order_amount=rest_item_amount_for_cancel_order, discount_amount=orderdata.discount, discount_rate=discount_rate)

#                         rest_item_amount_for_cancel_order = (
#                             rest_item_amount_for_cancel_order - rest_discount_amount)

#                     if(return_total_amount != 0):
#                         # Return Discount Amount
#                         return_discount_amount = orderDiscountCalculation(app_version=orderdata.app_version,
#                                                                           order_amount=return_total_amount, discount_amount=orderdata.discount, discount_rate=discount_rate)

#                         # Rest Discount Amount
#                         rest_discount_amount_two = orderDiscountCalculation(app_version=orderdata.app_version,
#                                                                             order_amount=rest_item_amount_for_return_order, discount_amount=orderdata.discount, discount_rate=discount_rate)

#                         rest_item_amount_for_return_order = (
#                             rest_item_amount_for_return_order - rest_discount_amount_two)

#                 # Check Shipping Charge
#                 delivery_charge = 0
#                 active_delivery_charge = 0
#                 del_delivery_charge = 0
#                 cancel_delivery_charge = 0
#                 return_delivery_charge = 0
#                 # if(orderdata.delivery_charge != 0): CHANGES RAHUL
#                 shipping_charge = db.query(ShippingChargeModel).filter(
#                     ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#                 delivery_charge = orderDeliveryCalculation(db=db, free_delivery=orderdata.free_delivery, user_id=orderdata.user_id, order_date=orderdata.created_at,
#                                                            app_version=orderdata.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=orderdata.payment_method)

#                 if(delivered_total_amount != 0):
#                     # Del Delivery Charge Amount
#                     del_delivery_charge = orderDeliveryCalculation(db=db, free_delivery=orderdata.free_delivery, user_id=orderdata.user_id, order_date=orderdata.created_at,
#                                                                    app_version=orderdata.app_version, order_amount=delivered_total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=orderdata.payment_method)
#                 if(active_total_amount != 0):
#                     # Del Delivery Charge Amount
#                     active_delivery_charge = orderDeliveryCalculation(db=db, free_delivery=orderdata.free_delivery, user_id=orderdata.user_id, order_date=orderdata.created_at,
#                                                                       app_version=orderdata.app_version, order_amount=rest_item_amount_for_active_order, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=orderdata.payment_method)

#                 if(cancel_total_amount != 0):
#                     # Cancel Delivery Charge Amount
#                     cancel_delivery_charge = orderDeliveryCalculation(db=db, free_delivery=orderdata.free_delivery, user_id=orderdata.user_id, order_date=orderdata.created_at,
#                                                                       app_version=orderdata.app_version, order_amount=rest_item_amount_for_cancel_order, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=orderdata.payment_method)

#                 if(return_total_amount != 0):
#                     # Return Delivery Charge Amount
#                     return_delivery_charge = orderDeliveryCalculation(db=db, free_delivery=orderdata.free_delivery, user_id=orderdata.user_id, order_date=orderdata.created_at,
#                                                                       app_version=orderdata.app_version, order_amount=rest_item_amount_for_return_order, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=orderdata.payment_method)

#                 # Grand Total
#                 grand_total = (
#                     total_amount + delivery_charge) - discount_amount

#                 grand_total = round(grand_total)

#                 order_total_amount += grand_total

#                 # Delivered Total Amount
#                 delgrand_total = (
#                     delivered_total_amount + del_delivery_charge) - del_discont_amount

#                 delgrand_total = round(delgrand_total)

#                 delivered_order_total_amount += delgrand_total

#                 # Cancel Total Amount
#                 if(current_status.status == 980):
#                     cancelgrand_total = (
#                         cancel_total_amount + cancel_delivery_charge) - cancel_discount_amount
#                 else:
#                     cancelgrand_total = (
#                         cancel_total_amount) - cancel_discount_amount

#                 cancelgrand_total = round(cancelgrand_total)

#                 cancel_order_total_amount += cancelgrand_total

#                 # Active Total Amount
#                 if(current_status.status <= 60):
#                     activegrand_total = (
#                         active_total_amount + active_delivery_charge) - active_discount_amount
#                 else:
#                     activegrand_total = (
#                         active_total_amount) - active_discount_amount

#                 activegrand_total = round(activegrand_total)

#                 active_order_total_amount += activegrand_total

#                 # Return Total Amount
#                 if(current_status.status == 80 or current_status.status == 90 or current_status.status == 100 or current_status.status == 110):
#                     returngrand_total = (
#                         return_total_amount + return_delivery_charge) - return_discount_amount
#                 else:
#                     returngrand_total = (
#                         return_total_amount) - return_discount_amount

#                 returngrand_total = round(returngrand_total)

#                 return_order_total_amount += returngrand_total

#         count_orders = [
#             {
#                 'order_Count': all_order.count(),
#                 'amount': order_total_amount,
#                 'Key': 'all_order',
#                 'name': 'All Orders',
#             },
#             {
#                 'order_Count': active_order.count(),
#                 'amount': active_order_total_amount,
#                 'Key': 'active_order',
#                 'name': 'Active',
#             },
#             {
#                 'order_Count': delivered_order.count(),
#                 'amount': delivered_order_total_amount,
#                 'Key': 'delivered_order',
#                 'name': 'Delivered',
#             },

#             {
#                 'order_Count': cancelled_order.count(),
#                 'amount': cancel_order_total_amount,
#                 'Key': 'cancelled_order',
#                 'name': 'Cancelled',
#             },
#             {
#                 'order_Count': returned_order.count(),
#                 'amount': return_order_total_amount,
#                 'Key': 'returned_order',
#                 'name': 'Returned',
#             },

#         ]

#         return {"status_code": HTTP_200_OK, "user_data": user_details, "static_count": count_orders}

#     except Exception as e:
#         print(e)


# @marketer_marketing_router.post('/marketer/order-list/', response_model=AdminOrderListSchema, dependencies=[Depends(JWTBearer())])
# async def getBuyerOrderStatus(request: Request, param: AdminMarketerorderList, db: Session = Depends(get_db)):
#     try:
#         userdata = auth(request=request)
#         # Check marketing user client
#         check_marketer_user_client = db.query(MarketerUserModel).filter(
#             MarketerUserModel.id == userdata['id']).first()
#         client_join_date = check_marketer_user_client.created_at.strftime(
#             "%Y-%m-%d")
#         if(param.search != ''):
#             search = param.search.replace("+", "%")
#             search = search.rstrip()
#             search = "%{}%".format(search)

#             if(param.key == 'all_order' and param.from_date != '' and param.to_date != '' and search != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(
#                                 OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).order_by(OrdersModel.id.desc())

#             elif(param.key == 'all_order' and param.from_date == '' and param.to_date == '' and search != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).filter(OrdersModel.user_id == param.user_id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#             elif(param.key == 'delivered_order' and param.from_date != '' and param.to_date != '' and search != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(
#                                 OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 70).order_by(OrdersModel.id.desc())

#             elif(param.key == 'delivered_order' and param.from_date == '' and param.to_date == '' and search != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 70).order_by(OrdersModel.id.desc())

#             elif(param.key == 'active_order' and param.from_date != '' and param.to_date != '' and search != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(
#                                 OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) <= 60).order_by(OrdersModel.id.desc())

#             elif(param.key == 'active_order' and param.from_date == '' and param.to_date == '' and search != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) <= 60).order_by(OrdersModel.id.desc())

#             elif(param.key == 'cancelled_order' and param.from_date != '' and param.to_date != '' and search != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(
#                                 OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.id.desc())

#             elif(param.key == 'cancelled_order' and param.from_date == '' and param.to_date == '' and search != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.id.desc())

#             elif(param.key == 'returned_order' and param.from_date != '' and param.to_date != '' and search != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(
#                                 OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status.between(80, 110)).filter(OrderItemsModel.status != 81).filter(OrderItemsModel.status != 91).filter(OrderItemsModel.status == 110).order_by(OrdersModel.id.desc())

#             elif(param.key == 'returned_order' and param.from_date == '' and param.to_date == '' and search != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                         OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status.between(80, 110)).filter(OrderItemsModel.status != 81).filter(OrderItemsModel.status != 91).filter(OrderItemsModel.status == 110).order_by(OrdersModel.id.desc())

#         else:
#             if(param.key == 'all_order' and param.from_date != '' and param.to_date != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(
#                             OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).order_by(OrdersModel.id.desc())

#             elif(param.key == 'all_order' and param.from_date == '' and param.to_date == ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).filter(func.date_format(
#                     OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

#             elif(param.key == 'delivered_order' and param.from_date != '' and param.to_date != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(
#                             OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 70).order_by(OrdersModel.id.desc())

#             elif(param.key == 'delivered_order' and param.from_date == '' and param.to_date == ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 70).order_by(OrdersModel.id.desc())

#             elif(param.key == 'active_order' and param.from_date != '' and param.to_date != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(
#                             OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) <= 60).order_by(OrdersModel.id.desc())

#             elif(param.key == 'active_order' and param.from_date == '' and param.to_date == ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) <= 60).order_by(OrdersModel.id.desc())

#             elif(param.key == 'cancelled_order' and param.from_date != '' and param.to_date != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(
#                             OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.id.desc())

#             elif(param.key == 'cancelled_order' and param.from_date == '' and param.to_date == ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.id.desc())

#             elif(param.key == 'returned_order' and param.from_date != '' and param.to_date != ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(
#                             OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status.between(80, 110)).filter(OrderItemsModel.status != 81).filter(OrderItemsModel.status != 91).filter(OrderItemsModel.status == 110).order_by(OrdersModel.id.desc())

#             elif(param.key == 'returned_order' and param.from_date == '' and param.to_date == ''):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(func.date_format(
#                     OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(check_marketer_user_client.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).filter(OrdersModel.user_id == param.user_id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status.between(80, 110)).filter(OrderItemsModel.status != 81).filter(OrderItemsModel.status != 91).filter(OrderItemsModel.status == 110).order_by(OrdersModel.id.desc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK, "total_records": 0, "orders": [], "current_page": param.page, "total_page": 0}

#         orders: OrdersModel = data.limit(
#             limit=param.limit).offset((param.page - 1) * param.limit).all()

#         # Calculation Total Amount of items
#         orderdata: OrdersModel = await Calculations.CalculateTotalAmountAllOrders(db=db, data=orders)

#         # Order Status
#         orderdata: OrdersModel = await OrderHelper.customizeOrderData(db=db, data=orderdata)

#         # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/param.limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "orders": orderdata, "total_records": total_records,  "current_page": param.page, "total_page": total_pages}

#     except Exception as e:
#         print(e)


# @marketer_marketing_router.get('/marketer/order-details/{order_id}', response_model=AdminMarketerOrderDetailsSchema, dependencies=[Depends(JWTBearer())])
# async def getBuyerOrderStatus(request: Request, order_id: int, db: Session = Depends(get_db)):
#     try:
#         # Order Data
#         data = db.query(OrdersModel, ShippingAddressModel).join(ShippingAddressModel,
#                                                                 ShippingAddressModel.id == OrdersModel.address_id).where(OrdersModel.id == order_id).first()

#         # Shipping Address
#         shipping_address = data.ShippingAddressModel

#         # Buyer
#         buyer = db.query(UserModel).filter(
#             UserModel.id == shipping_address.user_id).first()

#         # Shipping Address
#         shipped_address = {
#             'ship_to': shipping_address.ship_to,
#             'email': buyer.email,
#             'address': shipping_address.address,
#             'city': shipping_address.city,
#             'pincode': shipping_address.pincode,
#             'state': shipping_address.state,
#             'locality': shipping_address.locality,
#             'country': shipping_address.country,
#             'phone': shipping_address.phone,
#         }

#         # Order
#         order = data.OrdersModel

#         # Pickup Address
#         sellersaddress = db.query(UserModel).join(ProductModel, ProductModel.userid == UserModel.id).join(
#             OrderItemsModel, OrderItemsModel.product_id == ProductModel.id).filter(OrderItemsModel.order_id == order.id).first()

#         seller_address = {


#             'seller': sellersaddress.name,
#             'city': sellersaddress.city,
#             'region': sellersaddress.region,
#             'country': sellersaddress.country,
#             'pincode': sellersaddress.pincode,
#         }

#         # Order Current Status
#         current_status = order.order_status.order_by(
#             OrderStatusModel.id.desc()).first()
#         status = ''
#         for st in OrderStatus.AllstatusList():
#             if(current_status.status == st['status_id']):
#                 status = st['status_title']

#         order_status = status

#         # shipping details
#         shipping = ""
#         if(order_status >= 'Picked up Competed' and order_status <= 'Shipped'):
#             shipping = order.order_shipping.first()

#             if(shipping is not None):
#                 if(shipping.courier_partner == 'DELHIVERY'):
#                     tracking_link = 'https://www.delhivery.com/track/package/' + \
#                         str(shipping.wbns)

#                 elif(shipping.courier_partner == 'ECOMEXPRESS'):
#                     tracking_link = 'https://shiprocket.co/tracking/' + \
#                         str(shipping.wbns)

#                 elif(shipping.courier_partner == 'XPRESSBEES'):
#                     tracking_link = 'https://ship.xpressbees.com/shipping/tracking/' + \
#                         str(shipping.wbns)

#                 elif(shipping.courier_partner == 'UDAAN EXPRESS'):
#                     tracking_link = 'https://udaanexpress.com/track/' + \
#                         str(shipping.wbns)

#                 elif(shipping.courier_partner == 'DTDC'):
#                     tracking_link = 'https://www.trackingmore.com/track/en/' + \
#                         str(shipping.wbns)+str('?express=dtdc')

#                 shipping = {
#                     'wbns': shipping.wbns,
#                     'courier_partner': shipping.courier_partner,
#                     'tracking_link': tracking_link
#                 }

#         # Order Items
#         order_items = []
#         items = order.order_items.all()
#         total_amount = 0
#         cancel_total_amount = 0
#         return_total_amount = 0
#         discount_amount = 0
#         cancel_total_discount_amount = 0
#         return_total_discount_amount = 0
#         for item_data in items:

#             status = ''
#             for st in OrderStatus.AllstatusList():
#                 if(item_data.status == st['status_id']):
#                     status = st['status_title']
#             pricingdata = item_data.uuid.split('-')
#             pricingdata = db.query(ProductPricingModel).filter(
#                 ProductPricingModel.id == pricingdata[1]).first()
#             # product details
#             product = db.query(ProductModel).where(
#                 ProductModel.id == item_data.product_id).first()

#             image = db.query(ProductMediaModel).filter(ProductMediaModel.deleted_at.is_(
#                 None)).filter(ProductMediaModel.model_id == product.id).first()

#             # item price
#             item_total_amount = 0
#             # Aseztak Service
#             # aseztak_service = Services.aseztak_services(
#             #     item_data.created_at, db=db)
#             today_date = item_data.created_at.strftime(
#                 '%Y-%m-%d')
#             aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
#             if(aseztak_service is None):
#                 # Calculate Product Price
#                 product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=0,
#                                                         gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)

#                 item_total_amount = product_price * item_data.quantity

#                 total_amount += product_price * item_data.quantity
#             else:
#                 product_price: ProductModel = await ProductsHelper.getPrice(db, product, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item_data.id)
#                 # Calculate Product Price
#                 # product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=aseztak_service.rate,
#                 #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order.app_version)

#                 item_total_amount = product_price * item_data.quantity

#                 total_amount += product_price * item_data.quantity

#             # Check Order Item Discount for V4
#             if(item_data.discount_amount != 0):
#                 discount_amount += item_data.discount_amount

#             # Check Cancle Items Amount
#             if(item_data.status == 980):
#                 if(item_data.discount_amount != 0):
#                     cancel_total_discount_amount += item_data.discount_amount

#                 cancel_total_amount += product_price * item_data.quantity

#                 # # Check Return Items Amount
#             if(item_data.status >= 90 and item_data.status != 91 and item_data.status != 980):
#                 if(item_data.discount_amount != 0):
#                     return_total_discount_amount += item_data.discount_amount

#                 return_total_amount += product_price * item_data.quantity

#             message = ""
#             if(item_data.message is None):

#                 item_data.message = message

#             else:

#                 item_data.message = item_data.message
#             # Item Details
#             item = {
#                 'product_id': product.id,
#                 'product': product.title,
#                 'quantity': item_data.quantity,
#                 'attributes': item_data.attributes,
#                 'image': image.file_path,
#                 'price': product_price,
#                 'total_amount': item_total_amount,
#                 'status': status,
#                 'message': item_data.message

#             }

#             order_items.append(item)

#         if(current_status.status >= 90 and current_status.status != 91):
#             cancel_total_amount = 0
#             return_total_amount = 0
#             cancel_total_discount_amount = 0
#             return_total_discount_amount = 0

#         # Calculate Total Active Amount
#         rest_total_amount = (
#             total_amount - cancel_total_amount - return_total_amount)

#         # Discount Amount
#         discount_rate = order.discount_rate
#         discount_amount = (
#             discount_amount - cancel_total_discount_amount - return_total_discount_amount)

#         # Check Order Discount for V3
#         order_date = order.created_at.strftime("%Y-%m-%d")

#         if(order.app_version == 'V3' and order.discount != 0):
#             order_discount = db.query(OrderDiscountModel).filter(
#                 OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#             discount_rate = order_discount.discount
#             if(order.discount_rate != 0):
#                 discount_rate = order.discount_rate

#             discount_amount = orderDiscountCalculation(app_version=order.app_version,
#                                                        order_amount=rest_total_amount, discount_amount=order.discount, discount_rate=discount_rate)

#         # Check Shipping Charge
#         # delivery_charge = 0
#         # if(order.delivery_charge != 0): CHANGES RAHUL
#         shipping_charge = db.query(ShippingChargeModel).filter(
#             ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#         delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order.free_delivery, user_id=order.user_id, order_date=order.created_at,
#                                                    app_version=order.app_version, order_amount=rest_total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

#         # Grand Total
#         if(order.app_version == 'V4'):
#             if(cancel_total_amount != 0 or return_total_amount != 0):
#                 grand_total = (
#                     rest_total_amount + delivery_charge) - discount_amount
#             else:
#                 grand_total = order.grand_total
#         else:
#             grand_total = (
#                 rest_total_amount + delivery_charge) - discount_amount

#             grand_total = round(grand_total)

#         order = {
#             'id': order.id,
#             'order_number': order.order_number,
#             'payment_method': order.payment_method,
#             'cancel_total_amount': cancel_total_amount,
#             'return_total_amount': return_total_amount,
#             'order_total_amount': round(total_amount, 2),
#             'delivery_charge': delivery_charge,
#             'discount': round(discount_amount, 2),
#             'discount_rate': discount_rate,
#             'grand_total': round(grand_total, 2),
#             'created_at': order.created_at.strftime("%B %d %Y"),

#         }

#         allorderstatus = []

#         statusvalue = db.query(OrderStatusModel).filter(
#             OrderStatusModel.order_id == order['id']).all()
#         for orderstatus in statusvalue:
#             stats = ''

#             for sts in OrderStatus.AllstatusList():
#                 if(orderstatus.status == sts['status_id']):
#                     stats = sts['status_title']

#             if(stats == 'Cancelled' or stats == 'Return Initiated'):
#                 message = sts['message']
#             stts = {
#                 'created_at': orderstatus.created_at.strftime(" %d %B %Y"),
#                 'status': stats,
#                 'message': message
#             }
#             allorderstatus.append(stts)

#         return {"status_code": HTTP_200_OK, "shipping": shipping, "shipping_address": shipped_address, "seller_address": seller_address,  "order_summery": order, "item_detail": order_items, "order_status": allorderstatus}

#     except Exception as e:
#         print(e)


# @marketer_marketing_router.post('/order-list', response_model=AdminOrderListSchema, dependencies=[Depends(JWTBearer())])
# async def getOrderDetails(request: Request, param: AdminMarketerorderDetails, db: Session = Depends(get_db)):
#     try:
#         userdata = auth(request=request)

#         # Check marketing user client
#         marketer_user = db.query(MarketerUserModel).filter(
#             MarketerUserModel.id == userdata['id']).first()

#         search = param.search.replace("+", "%")
#         search = search.rstrip()
#         search = "%{}%".format(search)
#         if(param.status != '' and param.search == "" and param.from_date == "" and param.to_date == ""):

#             if(param.status == 'all'):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).filter(func.date_format(
#                     OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#             else:
#                 if(int(param.status) <= 70):
#                     data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == param.status).filter(func.date_format(
#                         OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())
#                 else:
#                     data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status == param.status).filter(func.date_format(
#                         OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#         elif(param.status != '' and param.search != "" and param.from_date == "" and param.to_date == ""):

#             if(param.status == 'all'):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#             else:
#                 if(int(param.status) <= 70):
#                     data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == param.status).filter(
#                         OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#                 else:
#                     data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status == param.status).filter(
#                         OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#         elif(param.status != '' and param.search == "" and param.from_date != "" and param.to_date != ""):
#             if(param.status == 'all'):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).filter(func.date_format(
#                     OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#             else:
#                 if(int(param.status) <= 70):
#                     data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == param.status).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#                 else:
#                     data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status == param.status).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#         else:
#             if(param.status == 'all'):
#                 data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).filter(
#                     OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).filter(func.date_format(
#                         OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())
#             else:
#                 if(int(param.status) <= 70):
#                     data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == param.status).filter(
#                         OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#                 else:
#                     data = db.query(OrdersModel).join(MarketerUsersClientModel, MarketerUsersClientModel.user_id == OrdersModel.user_id).join(ShippingAddressModel, ShippingAddressModel.id == OrdersModel.address_id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status == param.status).filter(
#                         OrdersModel.order_number.ilike(search) | ShippingAddressModel.ship_to.ilike(search) | ShippingAddressModel.phone.ilike(search)).filter(func.date_format(
#                             OrdersModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                                 OrdersModel.created_at, '%Y-%m-%d') <= param.to_date).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(marketer_user.created_at, "%Y-%m-%d")).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= func.date_format(MarketerUsersClientModel.created_at, "%Y-%m-%d")).order_by(OrdersModel.id.desc())

#         if(data.count() == 0):
#             return {"status_code": HTTP_200_OK, "total_records": 0, "orders": [], "current_page": param.page, "total_page": 0}

#         orders: OrdersModel = data.limit(
#             limit=param.limit).offset((param.page - 1) * param.limit).all()
#         orderdata = []
#         for order in orders:
#             items = db.query(OrderItemsModel, ProductModel).join(ProductModel, ProductModel.id ==
#                                                                  OrderItemsModel.product_id).filter(OrderItemsModel.order_id == order.id).all()
#             total_amount = 0
#             # Latest Status
#             current_status = db.query(OrderStatusModel).filter(
#                 OrderStatusModel.order_id == order.id).order_by(OrderStatusModel.id.desc()).first()

#             statuslist = OrderStatus.AllstatusList()
#             statustitle = ''
#             for s_title in statuslist:
#                 if(current_status.status == s_title['status_id']):
#                     statustitle = s_title['status_title']

#             if(order.message is not None):
#                 message = order.message
#             else:
#                 message = ''
#             currentstatus = {
#                 'status': statustitle,
#                 'created_at': current_status.created_at.strftime("%B %d %Y"),
#                 'message': message
#             }

#             # Calculate Items
#             # Aseztak Service

#             today_date = order.created_at.strftime('%Y-%m-%d')
#             aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

#             for item_data in items:
#                 item_data = item_data.OrderItemsModel
#                 # Pricing Object
#                 pricingdata = item_data.uuid.split('-')
#                 pricingdata = db.query(ProductPricingModel).filter(
#                     ProductPricingModel.id == pricingdata[1]).first()
#                 # Product Object
#                 productdata = db.query(ProductModel).filter(
#                     ProductModel.id == item_data.product_id).first()

#                 if(aseztak_service is None):

#                     # Calculate Product Price
#                     product_price = productPricecalculation(price=item_data.price, tax=item_data.tax, commission=0,
#                                                             gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)

#                     total_amount += product_price * item_data.quantity
#                 else:
#                     # Get Product Price
#                     product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item_data.id)

#                     total_amount += product_price * item_data.quantity

#             # Check Order Discount
#             discount_amount = 0
#             order_date = order.created_at.strftime("%Y-%m-%d")
#             if(order.discount != 0):
#                 # CHECK ORDER DISCOUNT
#                 order_discount = db.query(OrderDiscountModel).filter(
#                     OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

#                 discount_rate = order_discount.discount
#                 if(order.discount_rate != 0):
#                     discount_rate = order.discount_rate

#                 discount_amount = orderDiscountCalculation(app_version=order.app_version,
#                                                            order_amount=total_amount, discount_amount=order.discount, discount_rate=discount_rate)

#             # Check Shipping Charge
#             # delivery_charge = 0
#             # if(order.delivery_charge != 0): CHANGES RAHUL
#             shipping_charge = db.query(ShippingChargeModel).filter(
#                 ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

#             delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order.free_delivery, user_id=order.user_id, order_date=order.created_at,
#                                                        app_version=order.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

#             # Grand Total
#             if(order.app_version == 'V4'):
#                 grand_total = order.grand_total
#             else:
#                 grand_total = (
#                     total_amount + delivery_charge) - discount_amount

#             grand_total = round(grand_total)

#             created_at = order.created_at.strftime("%B %d %Y")

#             if(order.status == 0):
#                 confirm = 'Not Confirmed'
#             else:
#                 confirm = 'Confirmed'
#             buyer_name = db.query(ShippingAddressModel).filter(
#                 ShippingAddressModel.id == order.address_id).first()
#             # Seller
#             seller = db.query(OrderItemsModel, ProductModel).join(ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(
#                 OrderItemsModel.order_id == order.id).first()
#             if(seller is not None):

#                 seller = db.query(UserModel).filter(
#                     UserModel.id == seller.ProductModel.userid).first()
#                 seller = seller.name
#             else:
#                 seller = ''
#             buyer = {
#                 'name': buyer_name.ship_to,
#                 'confirm': confirm
#             }

#             product_image = db.query(ProductModel).join(OrderItemsModel, OrderItemsModel.product_id == ProductModel.id).filter(
#                 OrderItemsModel.order_id == order.id).all()

#             images = []
#             for product_images in product_image:

#                 image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product_images.id).filter(
#                     ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.id.desc()).first()

#                 if(image == None):
#                     image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product_images.id).filter(
#                         ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.id.desc()).first()

#                 img = ''
#                 if(image is not None):
#                     if(image.file_path is not None):
#                         filename = image.file_path

#                 images.append(filename)

#             order_data = {
#                 'id': order.id,
#                 'order_number': order.order_number,
#                 'total_items': len(items),
#                 'item_total_amount': floatingValue(total_amount),
#                 'grand_total': floatingValue(grand_total),
#                 'created_at': created_at,
#                 'payment_method': order.payment_method,
#                 'current_status': currentstatus,
#                 'buyer': buyer,
#                 'seller': seller,
#                 'images': images

#             }
#             orderdata.append(order_data)
#         # Count Total Products
#         total_records = data.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/param.limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "orders": orderdata, "total_records": total_records,  "current_page": param.page, "total_page": total_pages}
#     except Exception as e:
#         print(e)


# @marketer_marketing_router.post('/buyer-list', dependencies=[Depends(JWTBearer())])
# async def GetBuyerList(request: Request, param: AdminMarketerBuyerList, db: Session = Depends(get_db)):
#     try:
#         userdata = auth(request=request)

#         # Check marketing user client
#         marketer_user = db.query(MarketerUserModel).filter(
#             MarketerUserModel.id == userdata['id']).first()
#         if(param.search != ''):
#             search = param.search.replace("+", "%")
#             search = search.rstrip()
#             search = "%{}%".format(search)

#             buyers = db.query(MarketerUsersClientModel).join(UserModel, UserModel.id == MarketerUsersClientModel.user_id).filter(
#                 UserModel.name.ilike(search) | UserModel.mobile.ilike(search)).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).order_by(MarketerUsersClientModel.id.desc())
#             if(search != '' and param.from_date != '' and param.to_date != ''):
#                 buyers = db.query(MarketerUsersClientModel).join(UserModel, UserModel.id == MarketerUsersClientModel.user_id).filter(
#                     UserModel.name.ilike(search) | UserModel.mobile.ilike(search)).filter(func.date_format(
#                         MarketerUsersClientModel.created_at, '%Y-%m-%d') >= param.from_date).filter(func.date_format(
#                             MarketerUsersClientModel.created_at, '%Y-%m-%d') <= param.to_date).filter(MarketerUsersClientModel.marketer_id == marketer_user.id).order_by(MarketerUsersClientModel.id.desc())

#         else:
#             buyers = db.query(MarketerUsersClientModel).filter(
#                 MarketerUsersClientModel.marketer_id == marketer_user.id).order_by(MarketerUsersClientModel.id.desc())

#         if(buyers.count() == 0):
#             return {"status_code": HTTP_200_OK, "total_records": 0, "buyers": [], "current_page": param.page, "total_page": 0}

#         allbuyer: MarketerUsersClientModel = buyers.limit(
#             limit=param.limit).offset((param.page - 1) * param.limit).all()

#         buyerdata = []
#         for buyer in allbuyer:
#             buyer_details = db.query(UserModel).filter(
#                 UserModel.id == buyer.user_id).first()
#             orders = db.query(OrdersModel).filter(OrdersModel.user_id == buyer.user_id).group_by(
#                 OrdersModel.id).order_by(OrdersModel.id.desc()).count()

#             if(buyer_details.status == 1):
#                 buyerstatus = "Active"
#             else:
#                 buyerstatus = "Inactive"
#             data = {
#                 'id': buyer.user_id,
#                 'name': buyer_details.name,
#                 'marketer_id': buyer.marketer_id,
#                 'created_at': buyer.created_at.strftime("%B %d %Y"),
#                 'mobile_no': buyer_details.mobile,
#                 'email_id': buyer_details.email,
#                 'address': str(buyer_details.city)+', '+str(buyer_details.region)+', '+str(buyer_details.country)+' - '+str(buyer_details.pincode),
#                 'status': buyerstatus,
#                 'order_count': orders

#             }
#             buyerdata.append(data)

#         # Count Total Products
#         total_records = buyers.count()

#         # Number of Total Pages
#         total_pages = str(round((total_records/param.limit), 2))

#         total_pages = total_pages.split('.')

#         if(total_pages[1] != 0):
#             total_pages = int(total_pages[0]) + 1

#         return {"status_code": HTTP_200_OK, "buyers": buyerdata, "total_records": total_records,  "current_page": param.page, "total_page": total_pages}

#     except Exception as e:
#         print(e)
