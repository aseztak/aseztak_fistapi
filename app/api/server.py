
from starlette.responses import PlainTextResponse

from app.core.events import create_start_app_handler, create_stop_app_handler
import time
from fastapi import FastAPI, Request, Security, status, HTTPException, Depends
from fastapi.exceptions import RequestValidationError
from starlette.middleware.cors import CORSMiddleware
# from starlette.exceptions import HTTPException
from app.api.errors.http_error import http_error_handler
from app.api.errors.validation_error import http422_error_handler
from app.api.routes import router as api_router
from app.core import config

from starlette.staticfiles import StaticFiles
# from debug_toolbar.middleware import DebugToolbarMiddleware
from fastapi.security import APIKeyHeader

#CLOSED ON 2024-06-19 - 2024-06-20
#SlowAPI
# from slowapi import Limiter, _rate_limit_exceeded_handler
# from slowapi.errors import RateLimitExceeded
# from starlette.responses import JSONResponse
# from sqlalchemy.orm.session import Session
# from app.db.config import  SessionLocal
# from datetime import datetime

#CLOSED ON 2024-06-19
# def customheader(request: Request):
#     return request.headers.get('X-Identification-Id')
# limiter = Limiter(key_func=customheader)


# inclue router
api_key_header = APIKeyHeader(name="X-API-Key")
api_keys = [config.API_SECRET_KEY]


# def get_custom_header(X_Identification_Id: str = Header(...)):
#     if not X_Identification_Id:
#         raise HTTPException(status_code=400, detail="Custom-Header not found")
#     return X_Identification_Id

def init() -> FastAPI:
    if(config.APP_MODE == str('local')):
        application = FastAPI(
            debug=config.DEBUG, title=config.PROJECT_NAME, version=config.VERSION)
    else:
        application = FastAPI(docs_url=None,
            debug=config.DEBUG, title=config.PROJECT_NAME, version=config.VERSION)

    application.mount("/static", StaticFiles(directory="app/static"))

    application.add_middleware(
        CORSMiddleware,
        allow_origins=["https://api.aseztak.in/"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    # Process time middleware
    @application.middleware("http")
    async def add_process_time_header(request: Request, call_next):
        start_time = time.time()
        response = await call_next(request)
        process_time = time.time() - start_time

        response.headers["X-Process-Time"] = str(
            round(process_time * 1000, 3)) + " ms"

        return response

    # Events
    application.add_event_handler(
        "startup", create_start_app_handler(application))
    application.add_event_handler(
        "shutdown", create_stop_app_handler(application))

    # Exceptions
    @application.exception_handler(RequestValidationError)
    async def handle_error(request: Request, exc: RequestValidationError) -> PlainTextResponse:
        return PlainTextResponse(str(exc.errors()), status_code=400)
    application.add_exception_handler(HTTPException, http_error_handler)
    application.add_exception_handler(
        RequestValidationError, http422_error_handler)

    #New Changes SlowAPI
    # @application.middleware("http")
    # async def check_ip_middleware(request: Request, call_next):
    #     ip_address = request.headers.get('X-Identification-Id')
    #     db: Session = SessionLocal()
    #     currentdate = datetime.today()
    #     currentdate = currentdate.strftime('%Y-%m-%d')
    #     try:
    #         ip_entry = db.execute("SELECT * FROM blocked_ip_address WHERE ip=:ip AND date_format(created_at, '%Y-%m-%d') =:currentdate",{
    #             "ip": ip_address,
    #             "currentdate": currentdate
    #         }).first()
    #         if ip_entry:
    #             return JSONResponse(
    #                 status_code=429,
    #                 content={"message": "You have reached maximum limit of request"}
    #             )
    #     finally:
    #         db.close()
    #     response = await call_next(request)
    #     return response


    # @application.exception_handler(RateLimitExceeded)
    # async def rate_limit_exceeded_handler(request: Request, exc: RateLimitExceeded):
    #     ip_address = request.headers.get('X-Identification-Id')
    #     if ip_address:
    #         db = SessionLocal()
    #         try:
    #             db.execute("INSERT INTO blocked_ip_address (ip, created_at) VALUES(:ip, :created_at)",{
    #                 "ip": ip_address,
    #                 "created_at": datetime.now()
    #             })
    #             db.commit()
    #         finally:
    #             db.close()
    #     return JSONResponse(
    #         status_code=429,
    #         content={"message": "You have reached maximum limit of request"}
    #     )
    # async def custom_rate_limit_exceeded_handler(request: Request, exc: RateLimitExceeded):
    #     return JSONResponse(
    #         status_code=429,
    #         content={"message": "You have reached maximum limit of request"}
    #     )
    # application.state.limiter = limiter
    # application.add_exception_handler(RateLimitExceeded, rate_limit_exceeded_handler)
    #New Changes SlowAPI

    def get_api_key(api_key: str = Security(api_key_header)) -> str:
        if api_key and api_key in api_keys:
            return api_key
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid or missing API Key",
        )

    # application.include_router(api_router, prefix=config.API_PREFIX)
    application.include_router(api_router, prefix=config.API_PREFIX, dependencies=[Depends(get_api_key)])

    return application


app = init()


