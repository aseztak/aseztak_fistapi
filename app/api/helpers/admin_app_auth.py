
from datetime import datetime
from app.db.models.user import *
from app.db.schemas.admin_auth_schema import RegisterSchema
from sqlalchemy.orm import Session
import random
import math
from app.resources.strings import *
import requests
from starlette.status import HTTP_200_OK

from app.db.models.user_facility import UserFacilityModel

# Create new user


def create_user(db: Session, user: RegisterSchema):

    # already exist
    exist = db.query(UserModel).filter(UserModel.mobile == user.mobile).first()
    if(exist):
        return {'message': "Already Exist", 'exist': True}

    otp = _generate_otp()
    try:

        if(user.email == ''):
            email = str(user.mobile)+'@aseztak.com'
        else:
            email = user.email

        db_user = UserModel(
            name=user.name,
            mobile=user.mobile,
            password=user.mobile,
            email=email,
            otp=otp,
            status=1,
            country=user.country,
            region=user.region,
            city=user.city,
            pincode=user.pincode,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )

        db.add(db_user)
        db.commit()

        # create Role
        db_user_role = UserRoleModel(
            role_id=5,
            model_id=db_user.id
        )

        db.add(db_user_role)
        db.commit()
        db.refresh(db_user)

        # send OTP Message

        data = {
            'exist': False,
            'message': "Register Successfull",
            'data': db_user
        }

        return data

    except:
        db.rollback

# Create Seller


def create_seller(db: Session, user: RegisterSchema):

    # already exist
    exist = db.query(UserModel).filter(UserModel.mobile == user.mobile).first()
    if(exist):
        return {'status_code': HTTP_200_OK, 'message': "Already Exist", 'is_user': False}

    otp = random.randint(746008, 999999)
    try:

        if(user.email == ''):
            email = str(user.mobile)+'@aseztak.com'
        else:
            email = user.email

        db_user = UserModel(
            name=user.name,
            mobile=user.mobile,
            password=user.mobile,
            email=email,
            otp=otp,
            status=1,
            country=user.country,
            region=user.region,
            city=user.city,
            pincode=user.pincode,
            app_version='V4',

            created_at=datetime.now(),
            updated_at=datetime.now()
        )

        db.add(db_user)
        db.commit()

        # create Role
        db_user_role = UserRoleModel(
            role_id=6,
            model_id=db_user.id
        )

        db.add(db_user_role)
        db.commit()

        # Profile
        db_user_profile = UserProfileModel(
            user_id=db_user.id,
            proof=user.proof,
            proof_type=user.proof_type
        )
        db.add(db_user_profile)
        db.commit()

        db.refresh(db_user)

        # send OTP Message
        signature = user.signature
        mobile = str(user.mobile)
        extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
            AUTHKEY+'&template_id='+TEMPLATE_ID + \
            '&mobile='+str(91)+mobile+'&otp='+str(otp)

        sendurl = OTP_SEND_URL+extra_param
        requests.get(sendurl)

        data = {
            'status_code': HTTP_200_OK,
            'is_user': True,
            'message': "Registered Successfully",
            'data': db_user,
            'signature': signature,
            'otp_sent_at': datetime.now(),
            'otp_expires': 60
        }

        return data

    except Exception as e:
        print(e)


# Update Otp User
async def userOtpUpdate(db: Session, mobile: str, otp: str):
    try:
        user = db.query(UserModel).where(UserModel.mobile == mobile).first()
        if(user):

            if(user.app_version == 'V3'):
                app_version = 'V4'
            elif(user.app_version is None):
                app_version = 'V4'
            else:
                app_version = 'V4'

            user.otp = otp
            user.app_version = app_version
            db.flush()
            db.commit()
        else:

            user = UserModel(
                mobile=mobile, otp=otp, status=1, app_version='V4', created_at=datetime.now(), updated_at=datetime.now())

            db.add(user)
            db.commit()

            # User Limit
            user_limit = UserFacilityModel(
                user_id=user.id,
                key_name='cod',
                key_value=10000,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.add(user_limit)
            db.commit()

            # Update Role
            db_user_role = UserRoleModel(model_id=user.id, role_id=5,)

            db.add(db_user_role)
            db.commit()
            db.refresh(db_user_role)

        return user
    except:
        print('Error in otp updates')

# Update Otp Seller User


async def SellerOtpUpdate(db: Session, mobile: str, otp: str):
    try:
        user = db.query(UserModel).where(UserModel.mobile == mobile).first()
        if(user):
            user.otp = otp
            db.flush()
            db.commit()
        else:

            user = UserModel(
                mobile=mobile, otp=otp, status=1, app_version='V4', created_at=datetime.now(), updated_at=datetime.now())

            db.add(user)
            db.commit()

            # Update Role
            db_user_role = UserRoleModel(model_id=user.id, role_id=6,)

            db.add(db_user_role)
            db.commit()
            db.refresh(db_user_role)
        return user
    except:
        print('Error in otp updates')


# User login
def check_user(db: Session, data):

    user = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).where(UserRoleModel.role_id == 5).where(UserModel.mobile ==
                                                                                                                                   data.mobile).where(UserModel.otp == data.otp).first()

    if(user):
        profile = db.query(UserProfileModel).where(
            UserProfileModel.user_id == user.id).first()
        if(profile):
            return {'is_profile': True,  'user': user, 'profile': profile}
        else:
            return {'is_profile': False, 'user': user, 'profile': {}}

    return False


# User login
def check_seller_user(db: Session, data):

    user = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).where(UserRoleModel.role_id == 6).where(UserModel.mobile ==
                                                                                                                                   data.mobile).where(UserModel.otp == data.otp).first()

    if(user):
        profile = db.query(UserProfileModel).where(
            UserProfileModel.user_id == user.id).first()
        if(profile):
            return {'is_profile': True,  'user': user, 'profile': profile}
        else:
            return {'is_profile': False, 'user': user, 'profile': {}}

    return False


# Generate OTP(6 digit)


def _generate_otp():
    # storing strings in a list
    digits = [i for i in range(0, 10)]

    # initializing a string
    random_str = ""

    # we can generate any lenght of string we want
    for i in range(6):
        # generating a random index
        # if we multiply with 10 it will generate a number between 0 and 10 not including 10
        # multiply the random.random() with length of your base list or str
        index = math.floor(random.random() * 10)

        random_str += str(digits[index])

    return random_str
