from sqlalchemy.orm import Session
from app.db.models.acconts import AccountsModel


class AccountsHelper:
    async def TransactionHistory(db: Session, user_id: int, from_date: str = '', to_date: str = ''):

        if(from_date != ''):

            return db.query(AccountsModel).filter(AccountsModel.user_id == user_id).filter(AccountsModel.txn_date.between(from_date, to_date))
        else:
            return db.query(AccountsModel).filter(AccountsModel.user_id == user_id)
