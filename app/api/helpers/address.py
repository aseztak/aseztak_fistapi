from datetime import datetime

from sqlalchemy.sql.expression import desc



from sqlalchemy.orm import Session
from app.db.models.shipping_address import ShippingAddressModel


from app.resources.strings import *


class AddressHelper:
    # Fetch Buyer SHipping Address new (Rahul)
    async def fetch_shipping_address_data(db: Session, user_id: int):
        try:
            shipping_addresses = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.user_id == user_id).filter(ShippingAddressModel.status == 51).order_by(ShippingAddressModel.default_address.desc()).all()

            return shipping_addresses
        except Exception as e:
            print(e)

    async def getShippingAddress(db: Session, user_id: int):

        address = db.query(ShippingAddressModel).filter(
            ShippingAddressModel.user_id == user_id).filter(ShippingAddressModel.status == 51).order_by(desc(ShippingAddressModel.default_address))
        return address

    async def customizeAddress(data: ShippingAddressModel) -> ShippingAddressModel:

        if(type(data) in (tuple, list)):
            for address in data:
                alter_phone = ''
                if(address.alt_phone == None):
                    address.alt_phone = alter_phone

                default_address = False

                if(address.default_address == 1):
                    default_address = True

                address.default_address = default_address

            return data
        else:
            alter_phone = ''
            if(data.alt_phone == None):
                data.alt_phone = alter_phone

            return data
    # Add Shipping Address

    async def addAddress(db: Session, user_id: int, data):

        try:

            # check if default addres is already there
            checkDefault = db.query(ShippingAddressModel).filter(ShippingAddressModel.user_id == user_id).filter(
                ShippingAddressModel.default_address == 1).filter(ShippingAddressModel.status == 51).first()
            if(checkDefault):
                checkDefault.default_address = 0
                db.flush()
                db.commit()

            db_address = ShippingAddressModel(
                ship_to=data.ship_to,
                user_id=user_id,
                locality=data.locality,
                address=data.address,
                city=data.city,
                state=data.state,
                country='India',
                pincode=data.pincode,
                phone=data.phone,
                alt_phone=data.alt_phone,
                default_address=1,
                status=51,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )

            db.add(db_address)
            db.commit()
            db.refresh(db_address)

            return True

        except Exception as e:
            print(e)

    # Update Shipping Address
    async def updateAddress(db: Session, id: int, user_id: int, data):

        try:

            address = db.query(ShippingAddressModel).filter(ShippingAddressModel.user_id == user_id).filter(
                ShippingAddressModel.id == id).filter(ShippingAddressModel.status == 51).first()

            # Check If Default Address Exist
            if(data.default_address == 1):
                checkDefault = db.query(ShippingAddressModel).filter(ShippingAddressModel.user_id == address.user_id).filter(
                    ShippingAddressModel.default_address == 1).filter(ShippingAddressModel.status == 51).first()
                if(checkDefault):
                    checkDefault.default_address = 0
                    db.flush()
                    db.commit()

            # Update Address
            address.ship_to = data.ship_to
            address.locality = data.locality
            address.address = data.address
            address.city = data.city
            address.state = data.state
            address.pincode = data.pincode
            address.phone = data.phone
            address.alt_phone = data.alt_phone
            address.default_address = data.default_address
            address.updated_at = datetime.now()

            db.flush()
            db.commit()

            return True

        except Exception as e:
            print(e)

    # Edit Shipping Address

    async def editShippingAddress(db: Session, id: int, user_id: int):
        try:

            address = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.id == id).filter(ShippingAddressModel.user_id == user_id).filter(ShippingAddressModel.status == 51).first()
            if(address):
                return address
            else:
                return False

        except Exception as e:
            print(e)

    # Delete Shipping Address
    async def deleteAddressData(db: Session, user_id: int,  data):
        address = db.query(ShippingAddressModel).filter(ShippingAddressModel.user_id == user_id).filter(
            ShippingAddressModel.id == data.id).first()
        if(address):
            address.status = 98
            db.flush()
            db.commit()
            return True
        else:
            return False
