from datetime import date, datetime, timedelta
from sqlalchemy.sql.expression import desc
from sqlalchemy import case


from sqlalchemy.sql.functions import func
from app.api.util.calculation import floatingValue, orderDeliveryCalculation, orderDiscountCalculation, productPricecalculation, roundOf
from app.api.helpers.orderstaticstatus import OrderStatus
from app.db.models.acconts import AccountsModel
from app.db.models.carts import CartModel
from app.db.models.categories import *

from sqlalchemy.orm import Session
from app.db.models.media import ProductMediaModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.order_status import OrderStatusModel
from app.db.models.orderdiscount import OrderDiscountModel
from app.db.models.orders import OrdersModel
from app.db.models.products import InventoryModel, ProductModel, ProductPricingModel
from app.db.models.returndays import CategoryReturnDaysModel
from app.db.models.reverse import OrderRevreseModel
from app.db.models.shipping import OrderShippingModel
from app.db.models.shipping_address import ShippingAddressModel
from app.db.models.shippingcharge import ShippingChargeModel
from app.db.models.transaction import TransactionsModel
from app.db.models.user import UserModel
import uuid
import math
from starlette.requests import Request
import os
from app.resources.strings import *
import json
from app.api.helpers.discount import Discount
from app.api.helpers.services import AsezServices
from app.db.models.seller_payment import SellerPaymentModel
from app.api.helpers.products import ProductsHelper
from app.db.models.order_status import OrderStatusModel
from app.resources.strings import ORDER_TRACK_API_LIVE, API_TOKEN
from app.db.models.reverse import OrderRevreseModel
from app.db.models.order_fails import OrderFailsModel
from app.db.models.payment_failed import PaymentFailedModel
import requests


class OrderHelper:
    # Check Order Exist (Buyer)
    async def check_order_exist(db: Session, user_id: int):
        try:
            check_order_exist = db.query(OrdersModel).filter(
                OrdersModel.user_id == user_id).count()
            if(check_order_exist > 0):
                # Check Cancel Order
                check_cancel_order = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
                    OrdersModel.user_id == user_id).having(func.max(OrderStatusModel.status) == 980).group_by(OrderStatusModel.order_id).count()
                if(check_order_exist == 1 and check_cancel_order == 1):
                    return True
                else:
                    return False

            return True
        except Exception as e:
            print(e)
    # Check Shipping Carge

    async def check_order_shippinhg_charge(db: Session, date: str):
        try:
            shipping_charge = db.execute("SELECT id, rate, start_date, shipping_text, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                         {"param": date}).first()

            return shipping_charge
        except Exception as e:
            print(e)

    # Check Orders Of buyers
    async def check_buyer_cod_orders(db: Session, user_id: int, date: str):
        try:
            orders = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
                OrdersModel.user_id == user_id).filter(func.date_format(OrdersModel.created_at, "%Y-%m-%d") >= date).filter(OrdersModel.payment_method == 'COD').having(func.max(OrderStatusModel.status) <= 60).group_by(OrderStatusModel.order_id).all()

            return orders
        except Exception as e:
            print(e)
    # New Rahul

    async def searchOrders(db: Session, user_id: int, search: str, from_date: str, to_date: str):

        if(search != 'search'):
            search = search.replace("+", "%")
            search = search.rstrip()
            search = "%{}%".format(search)

            if(search != 'search' and from_date == 'from_date'):
                orders = db.query(OrdersModel).filter(
                    OrdersModel.user_id == user_id).filter(OrdersModel.order_number.like(search)).order_by(desc(OrdersModel.id))

            elif(search != 'search' and from_date != 'from_date' or to_date != 'to_date'):

                orders = db.query(OrdersModel).filter(
                    OrdersModel.user_id == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(
                        OrdersModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(
                            OrdersModel.created_at, '%Y-%m-%d') <= to_date).group_by(OrdersModel.id).order_by(desc(OrdersModel.id))
        else:
            if(search == 'search' and from_date != 'from_date' and to_date != 'to_date'):
                orders = db.query(OrdersModel).filter(
                    OrdersModel.user_id == user_id).filter(func.date_format(
                        OrdersModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(
                            OrdersModel.created_at, '%Y-%m-%d') <= to_date).group_by(OrdersModel.id).order_by(desc(OrdersModel.id))
            else:

                orders = db.query(OrdersModel).filter(
                    OrdersModel.user_id == user_id).order_by(desc(OrdersModel.id))

        if(orders):
            return orders
        else:
            return False

    async def getOrders(db: Session, user_id: int):
        orders = db.query(OrdersModel).filter(
            OrdersModel.user_id == user_id).order_by(desc(OrdersModel.id))
        if(orders):
            return orders
        else:
            return False

    # Get Order Items
    async def getOrderItems(order_id: int, db: Session):
        return db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order_id).all()

    async def getOrderTotalItems(order_id: int, db: Session):
        try:
            # get all items
            items = db.query(OrderItemsModel).where(
                OrderItemsModel.order_id == order_id).where(OrderItemsModel.status == 0).all()
            if(len(items) == 0):
                items = db.query(OrderItemsModel).where(
                    OrderItemsModel.order_id == order_id).where(OrderItemsModel.status <= 81).all()

            if(len(items) == 0):
                items = db.query(OrderItemsModel).where(
                    OrderItemsModel.order_id == order_id).where(OrderItemsModel.status == 81).all()

            if(len(items) == 0):
                items = db.query(OrderItemsModel).where(
                    OrderItemsModel.order_id == order_id).where(OrderItemsModel.status > 10).where(OrderItemsModel.status < 980).all()

            if(len(items) == 0):
                items = db.query(OrderItemsModel).where(
                    OrderItemsModel.order_id == order_id).where(OrderItemsModel.status == 980).all()

            return items
        except Exception as e:
            print(e)
    # get orders status wise

    async def getOrdersStatuswise(db: Session, user_id: int, status: str, page: int, limit: int):

        offset = (page - 1) * limit
        orders = db.execute('SELECT order_status.order_id, MAX(order_status.status) as status FROM order_status LEFT JOIN orders ON orders.id=order_status.order_id WHERE orders.user_id=:param GROUP BY order_status.order_id HAVING MAX(order_status.status)=:param1 limit :param2 offset :param3', {
            "param": user_id, "param1": status, "param2": limit, "param3": offset
        }).all()

        orders_count = db.execute('SELECT  order_status.order_id, MAX(order_status.status) as status FROM order_status LEFT JOIN orders ON orders.id=order_status.order_id WHERE orders.user_id=:param GROUP BY order_status.order_id HAVING MAX(order_status.status)=:param1', {
            "param": user_id, "param1": status
        })

        if(len(orders) == 0):
            return False

        else:

            ordersdata = []
            for order in orders:
                checkorder = db.query(OrdersModel).where(
                    OrdersModel.user_id == user_id).where(OrdersModel.id == order.order_id).first()

                ordersdata.append(checkorder)

            return {"ordersdata": ordersdata, "order_count": len(orders_count.all())}

    # Count Buyer Orders
    # New rahul
    async def searchOrdersStatusWise(db: Session, user_id: int, status: str, search: str, from_date: str, to_date: str):

        if(search != 'search'):
            search = search.replace("+", "%")
            search = search.rstrip()
            search = "%{}%".format(search)

            if(search != 'search' and from_date == 'from_date'):
                orders = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrdersModel.user_id == user_id).filter(
                    OrdersModel.order_number.like(search)).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == status).order_by(OrdersModel.id.desc())

            elif(search != 'search' and from_date != 'from_date' or to_date != 'to_date'):
                orders = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrdersModel.user_id == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(
                    OrdersModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(
                        OrdersModel.created_at, '%Y-%m-%d') <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == status).order_by(OrdersModel.id.desc())

        else:
            if(search == 'search' and from_date != 'from_date' or to_date != 'to_date'):
                orders = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrdersModel.user_id == user_id).filter(func.date_format(
                    OrdersModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(
                        OrdersModel.created_at, '%Y-%m-%d') <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == status).order_by(OrdersModel.id.desc())

            else:
                orders = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrdersModel.user_id == user_id).group_by(
                    OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == status).order_by(OrdersModel.id.desc())

        if(orders):
            return orders
        else:
            return False

    async def countOrdersStatuswise(db: Session, user_id: int, status: str):

        return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrdersModel.user_id == user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == status).order_by(OrdersModel.id.desc())

    async def getOrderDetail(db: Session, order_id: int):
        order = db.query(OrdersModel).where(OrdersModel.id == order_id).first()

        if(order):
            return order
        else:
            return False

    # Get Order Items
    async def orderItems(db: Session, order_id: int):

        try:

            items = db.query(OrderItemsModel).where(
                OrderItemsModel.order_id == order_id).all()

            if(len(items) > 0):
                return items
            else:
                return False
        except Exception as e:
            return False

    # Custom Order Items
    async def customOrderItems(request: Request, db: Session, data: OrdersModel) -> OrdersModel:
        try:

            # Get Order
            order = db.query(OrdersModel.id, OrdersModel.reff, OrdersModel.partial_amount, OrdersModel.payment_method, OrdersModel.payment_status, OrdersModel.created_at, OrdersModel.app_version).filter(
                OrdersModel.id == data[0].order_id).first()
            item_status_title = ''
            # Check Order Failed
            checkorderfailed = await OrderHelper.checkorderfailed(db=db, order_id=order.id)
            if(checkorderfailed['status'] != 0):

                item_status_title = 'ORDER FAILED'

            # CheckReturnPackage
            # check_reverse_order = order.order_reverse.join(
            #     OrderItemsModel, OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 90).first()
            check_reverse_order = db.query(OrderRevreseModel).join(
                OrderItemsModel, OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 90).first()
            package_details = ''
            if(check_reverse_order):
                url = str(ORDER_TRACK_API_LIVE)+str("?token=")+str(API_TOKEN) + \
                    str("&waybill=")+str(check_reverse_order.wbns)
                headers = {"accept": "application/json"}
                response = requests.get(url, headers=headers)
                response = response.json()
                if('ShipmentData' in response):
                    package_details = response['ShipmentData'][0]['Shipment']['Status']
                else:
                    package_details = ''
            # Order Created Date
            order_date = order.created_at.strftime("%Y-%m-%d")

            # Order Current Status
            current_status = db.query(OrderStatusModel).where(
                OrderStatusModel.order_id == data[0].order_id).order_by(desc(OrderStatusModel.id)).first()

            for item in data:

                # Product Pricing #CHANGES RAHUL
                product_price_id = item.uuid.split('-')
                product_price = db.query(ProductPricingModel).where(
                    ProductPricingModel.id == product_price_id[1]).first()

                # Customize Product Image
                filename = ''
                if(item.images != None):

                    image = db.query(ProductMediaModel).filter(
                        ProductMediaModel.id == item.images.strip("[]")).first()

                    if(image is not None):
                        filename = image.file_path
                    else:
                        image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == item.product_id).filter(
                            ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()
                        filename = image.file_path

                else:
                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == item.product_id).filter(
                        ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                    if(image is not None):
                        filename = image.file_path

                img = filename
                if(img is None):
                    img = ''
                # Order Item Status
                status_title = ''
                staticstatus = OrderStatus.statusList()
                for status in staticstatus:

                    check_delivered_status = db.query(OrderStatusModel).filter(
                        OrderStatusModel.order_id == data[0].order_id).filter(OrderStatusModel.status == 70).first()

                    if(check_delivered_status is not None):
                        if(item.status == 91):
                            status_title = 'Return Completed'
                        else:
                            if(status['status_id'] == item.status):
                                if(check_reverse_order is not None and package_details != '' and package_details['StatusType'] == 'PU'):
                                    status_title = 'Pickup Completed'
                                else:
                                    status_title = status['status_title']
                    else:
                        if(status['status_id'] == item.status):
                            if(check_reverse_order is not None and package_details != '' and package_details['StatusType'] == 'PU'):
                                status_title = 'Pickup Completed'
                            else:
                                status_title = status['status_title']

                item.status_title = status_title
                # New Update (2023-07-28)
                if('ONLINE' in str(order.payment_method) and order.payment_status == 1):
                    item.status_title = 'ORDER FAILED'
                if(item_status_title != ''):
                    item.status_title = 'ORDER FAILED'
                # Item Message
                message = ''
                if(item.message == None):
                    item.message = message

                # Get Product Detail
                product = db.query(ProductModel).where(
                    ProductModel.id == item.product_id).first()

                # Check Order Return Period
                checkbox = False
                return_period = ''
                if(current_status.status == 70 and current_status.status < 80):

                    category_return_days = db.query(CategoryReturnDaysModel).where(
                        CategoryReturnDaysModel.category_id == product.category).where(CategoryReturnDaysModel.start_date <= order_date).order_by(desc(CategoryReturnDaysModel.start_date)).first()

                    if (category_return_days.return_days != 0 and item.status != 980):
                        checkbox = True

                        return_days = category_return_days.return_days

                        days = return_days - 1

                        td = timedelta(days=days)
                        return_period = current_status.created_at + td
                        return_date = return_period.strftime("%d %b %Y")
                        return_period = return_period.strftime("%Y-%m-%d")

                        today = date.today()
                        today = today.strftime("%Y-%m-%d")

                        if(today > return_period):
                            return_period = 'Return policy ended on ' + \
                                str(return_date)

                        else:
                            return_period = 'Return policy valid till ' + \
                                str(return_date)

                    else:
                        return_period = 'No Return Applicable ?'

                if(item.status == 0):
                    checkbox = True
                # Check Partial Amount (New)
                if(order.partial_amount != 0.00):
                    checkbox = False
                if(order.partial_amount != 0.00 and current_status.status == 70 and item.return_expiry != 0):
                    checkbox = True

                if(product.status == 51):
                    product_deleted = False
                else:
                    product_deleted = True
                item.return_period = return_period
                item.checkbox = checkbox
                item.product_id = product.id
                item.product_title = product.title
                item.product_description = product.short_description
                item.product_image = img
                item.product_deleted = product_deleted

                # Aseztak Service
                # aseztak_service = Services.aseztak_services(
                #     item.created_at, db=db)
                today_date = item.created_at.strftime(
                    '%Y-%m-%d')
                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                if(aseztak_service is None):

                    # Calculate Product Price
                    price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
                                                    gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)
                else:
                    price: ProductModel = await ProductsHelper.getPrice(db, product, product_price, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item.id)
                    # Calculate Product Price
                    # price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
                    #                                 gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order.app_version)

                item.price = floatingValue(price)

                item.total = floatingValue(price * item.quantity)

                item.item_quantity = str(item.quantity) + ' ' + \
                    str(product_price.unit)

            return data

        except Exception as e:
            print(e)

    async def orderCurrentStatus(db: Session, data: OrdersModel) -> OrdersModel:

        current_status = {}
        if(type(data) in (tuple, list)):

            for order in data:
                # ORDER FAILED STATUS
                reff_id = order.reff.split('ASEZ')
                reff_id = reff_id[1].split('U')

                checkorderfailed = await OrderHelper.checkorderfailed(db=db, order_id=order.id)
                if(checkorderfailed['status'] != 0):
                    current_status = {
                        'status_id': checkorderfailed['status'],
                        'status_title': 'ORDER FAILED',
                        'message': 'Order Failed',
                        'visible': True,
                        'created_at': order.created_at
                    }
                    order.current_status = current_status
                else:
                    # Expected delivery
                    status = db.query(OrderStatusModel).where(
                        OrderStatusModel.order_id == order.id).where(OrderStatusModel.status == 60).first()
                    delivery_expexted = ''
                    if(status):
                        date = status.created_at

                        td = timedelta(days=5)
                        # your calculated date
                        delivery_expexted = date + td
                        delivery_expexted = delivery_expexted.strftime(
                            "%d %b %Y")
                        order.delivery_expected = 'Expected delivery ' + \
                            str(delivery_expexted)

                    # Current Status
                    all_static_status = OrderStatus.statusList()

                    latestStatus = db.query(OrderStatusModel).where(
                        OrderStatusModel.order_id == order.id).order_by(OrderStatusModel.status.desc()).first()

                    if(latestStatus.status == 10):
                        current_status = {
                            'status_id': 0,
                            'status_title': 'Ordered',
                            'message': 'Order has been placed',
                            'visible': True,
                            'created_at': latestStatus.created_at.strftime("%B %d %Y")
                        }
                    for st in all_static_status:
                        created_at = latestStatus.created_at.strftime(
                            "%B %d %Y")
                        if(latestStatus.status == st['status_id']):
                            current_status = {
                                'status_id': st['status_id'],
                                'status_title': st['status_title'],
                                'message': st['message'],
                                'visible': True,
                                'created_at': created_at
                            }

                        order.current_status = current_status

        else:
            # ORDER FAILED STATUS
            reff_id = data.reff.split('ASEZ')
            reff_id = reff_id[1].split('U')

            checkorderfailed = await OrderHelper.checkorderfailed(db=db, order_id=data.id)
            if(checkorderfailed['status'] != 0):
                current_status = {
                    'status_id': checkorderfailed['status'],
                    'status_title': 'ORDER FAILED',
                    'message': 'Order Failed',
                    'visible': True,
                    'created_at': ''
                }
                data.current_status = current_status
            else:
                # Expected delivery
                status = db.query(OrderStatusModel).where(
                    OrderStatusModel.order_id == data.id).where(OrderStatusModel.status == 60).first()

                delivery_expexted = ''
                if(status):
                    date = status.created_at

                    td = timedelta(days=5)
                    # your calculated date
                    delivery_expexted = date + td
                    delivery_expexted = delivery_expexted.strftime("%d %b %Y")
                    data.delivery_expected = 'Expected delivery ' + \
                        str(delivery_expexted)

                # Current Status
                all_static_status = OrderStatus.statusList()

                latestStatus = db.query(OrderStatusModel).where(OrderStatusModel.order_id == data.id).order_by(
                    desc(OrderStatusModel.id)).first()
                if(latestStatus.status == 10):
                    current_status = {
                        'status_id': 0,
                        'status_title': 'Ordered',
                        'message': 'Order has been placed',
                        'visible': True,
                        'created_at': latestStatus.created_at.strftime("%B %d %Y")
                    }
                for st in all_static_status:

                    if(latestStatus.status == st['status_id']):

                        current_status = {
                            'status_id': st['status_id'],
                            'status_title': st['status_title'],
                            'message': st['message'],
                            'visible': True,
                            'created_at': latestStatus.created_at.strftime("%B %d %Y")
                        }

                    data.current_status = current_status

        return data

    async def orderStatus(db: Session, data: OrdersModel) -> OrdersModel:
        # Current Status
        current_status = db.query(OrderStatusModel).where(
            OrderStatusModel.order_id == data.id).order_by(desc(OrderStatusModel.id)).first()
        # CheckReturnPackage
        check_reverse_order = data.order_reverse.first()
        package_details = ''
        if(current_status.status == 90 and check_reverse_order):
            url = str(ORDER_TRACK_API_LIVE)+str("?token=")+str(API_TOKEN) + \
                str("&waybill=")+str(check_reverse_order.wbns)
            headers = {"accept": "application/json"}
            response = requests.get(url, headers=headers)
            response = response.json()
            if('ShipmentData' in response):
                package_details = response['ShipmentData'][0]['Shipment']['Status']
            else:
                package_details = ''
        staticstatuslist = []
        staticlist = OrderStatus.statusList()
        reff_id = data.reff.split('ASEZ')
        reff_id = reff_id[1].split('U')

        # Check Failed Order
        checkfailedorder = await OrderHelper.checkorderfailed(db=db, order_id=data.id)
        if(checkfailedorder['status'] != 0):
            arr = {
                'status_id': checkfailedorder['status'],
                'status_title': 'ORDER FAILED',
                'message': 'Order Failed',
                'visible': True,
                'created_at': data.created_at
            }
            staticstatuslist.append(arr)

        else:
            # Pending Status
            pending_status = db.query(OrderStatusModel).filter(
                OrderStatusModel.order_id == data.id).filter(OrderStatusModel.status == 0).first()

            # Reverse Status
            if(current_status.status == 61):

                arr = [{
                    'status_id': 0,
                    'status_title': 'Ordered',
                    'message': 'Order has been placed',
                    'visible': True,
                    'created_at': pending_status.created_at.strftime("%B %d %Y")
                },
                    {
                    'status_id': 61,
                    'status_title': 'Reverse',
                    'message': 'Order has been Reversed',
                    'visible': True,
                    'created_at': current_status.created_at.strftime("%B %d %Y")
                },
                ]
                staticstatuslist = arr
                # Reverse Status

            # Ordered To Delivered
            if(current_status.status <= 80 and current_status.status != 61 and current_status.status != 980):

                for status in staticlist:
                    if(current_status.status == 80):
                        if(status['status_id'] <= 80 and status['status_id'] != 61):
                            statuslist = db.query(OrderStatusModel).where(
                                OrderStatusModel.order_id == data.id).where(OrderStatusModel.status == status['status_id']).first()

                            visible = False
                            created_at = ''
                            if(statuslist):
                                visible = True
                                created_at = statuslist.created_at.strftime(
                                    "%B %d %Y")

                            arr = {
                                'status_id': status['status_id'],
                                'status_title': status['status_title'],
                                'message': status['message'],
                                'visible': visible,
                                'created_at': created_at
                            }
                            staticstatuslist.append(arr)

                    if(current_status.status < 80):
                        if(status['status_id'] < 80 and status['status_id'] != 61):
                            statuslist = db.query(OrderStatusModel).where(
                                OrderStatusModel.order_id == data.id).where(OrderStatusModel.status == status['status_id']).first()

                            visible = False
                            created_at = ''
                            if(statuslist):
                                visible = True
                                created_at = statuslist.created_at.strftime(
                                    "%B %d %Y")

                            arr = {
                                'status_id': status['status_id'],
                                'status_title': status['status_title'],
                                'message': status['message'],
                                'visible': visible,
                                'created_at': created_at
                            }
                            staticstatuslist.append(arr)

            # Cancelled Status
            if(current_status.status == 980):
                arr = [{
                    'status_id': 0,
                    'status_title': 'Ordered',
                    'message': 'Order has been placed',
                    'visible': True,
                    'created_at': pending_status.created_at.strftime("%B %d %Y")

                },
                    {
                    'status_id': 980,
                    'status_title': 'Cancelled',
                    'message': 'Order has been Cancelled',
                    'visible': True,
                    'created_at': current_status.created_at.strftime("%B %d %Y")

                },
                ]
                staticstatuslist = arr

            if(current_status.status == 81):
                # Order to Rteurn Declined
                staticstatuslist = [{
                    'status_id': 0,
                    'status_title': 'Ordered',
                    'message': 'Order has been placed',
                    'visible': True,
                    'created_at': pending_status.created_at.strftime("%B %d %Y")

                }]
                for status in staticlist:
                    if(status['status_id'] >= 70 and status['status_id'] <= 81):
                        statuslist = db.query(OrderStatusModel).where(
                            OrderStatusModel.order_id == data.id).where(OrderStatusModel.status == status['status_id']).first()

                        visible = False
                        created_at = ''
                        if(statuslist):
                            visible = True
                            created_at = statuslist.created_at.strftime(
                                "%B %d %Y")

                        arr = {
                            'status_id': status['status_id'],
                            'status_title': status['status_title'],
                            'message': status['message'],
                            'visible': visible,
                            'created_at': created_at

                        }
                        staticstatuslist.append(arr)

            # Return Approved
            if(current_status.status > 81 and current_status.status != 91 and current_status.status < 980):
                # Order to Rteurn Declined
                staticstatuslist = [{
                    'status_id': 0,
                    'status_title': 'Ordered',
                    'message': 'Order has been placed',
                    'visible': True,
                    'created_at': pending_status.created_at.strftime("%B %d %Y")

                }]
                for status in staticlist:
                    if(status['status_id'] >= 70 and status['status_id'] != 81 and status['status_id'] != 91 and status['status_id'] < 980):
                        statuslist = db.query(OrderStatusModel).where(
                            OrderStatusModel.order_id == data.id).where(OrderStatusModel.status == status['status_id']).first()
                        created_at = ''
                        visible = False
                        if(statuslist):
                            visible = True
                            created_at = statuslist.created_at.strftime(
                                "%B %d %Y")

                        statustitle = status['status_title']
                        statusmessage = status['message']
                        if(status['status_id'] == 90 and package_details != '' and package_details['StatusType'] == 'PU'):
                            statustitle = 'Pickup Completed'
                            statusmessage = 'Pickup Completed'
                            created_at = package_details['StatusDateTime']
                        arr = {
                            'status_id': status['status_id'],
                            'status_title': statustitle,
                            'message': statusmessage,
                            'visible': visible,
                            'created_at': created_at
                        }
                        staticstatuslist.append(arr)

            if(current_status.status == 91):
                # Order to Rteurn Declined
                staticstatuslist = [{
                    'status_id': 0,
                    'status_title': 'Ordered',
                    'message': 'Order has been placed',
                    'visible': True,
                    'created_at': pending_status.created_at.strftime("%B %d %Y")

                }]

                check_delivered_status = db.query(OrderStatusModel).filter(
                    OrderStatusModel.order_id == data.id).filter(OrderStatusModel.status == 70).first()

                if(check_delivered_status is not None):

                    for status in staticlist:
                        if(status['status_id'] >= 70 and status['status_id'] != 81 and status['status_id'] != 91 and status['status_id'] < 980):
                            statuslist = db.query(OrderStatusModel).where(
                                OrderStatusModel.order_id == data.id).where(OrderStatusModel.status == status['status_id']).first()

                            created_at = current_status.created_at.strftime(
                                "%B %d %Y")
                            visible = True
                            if(statuslist):
                                visible = True
                                created_at = statuslist.created_at.strftime(
                                    "%B %d %Y")
                            arr = {
                                'status_id': status['status_id'],
                                'status_title': status['status_title'],
                                'message': status['message'],
                                'visible': visible,
                                'created_at': created_at
                            }
                            staticstatuslist.append(arr)

                else:

                    for status in staticlist:
                        if(status['status_id'] >= 10 and status['status_id'] != 70 and status['status_id'] != 80 and status['status_id'] != 81 and status['status_id'] != 90 and status['status_id'] < 100):
                            statuslist = db.query(OrderStatusModel).where(
                                OrderStatusModel.order_id == data.id).where(OrderStatusModel.status == status['status_id']).first()

                            created_at = current_status.created_at.strftime(
                                "%B %d %Y")
                            visible = True
                            if(statuslist):
                                visible = True
                                created_at = statuslist.created_at.strftime(
                                    "%B %d %Y")
                            arr = {
                                'status_id': status['status_id'],
                                'status_title': status['status_title'],
                                'message': status['message'],
                                'visible': visible,
                                'created_at': created_at
                            }
                            staticstatuslist.append(arr)

        return staticstatuslist

    async def orderShippingAddress(db: Session, data: OrdersModel) -> OrdersModel:

        try:
            address = db.query(ShippingAddressModel).where(
                ShippingAddressModel.id == data.address_id).first()

            if(address):
                altphone = ''
                if(address.alt_phone == None):
                    address.alt_phone = altphone
                shipping = {
                    'ship_to': address.ship_to,
                    'address': str(address.address)+', '+str(address.locality),
                    'city': address.city,
                    'state': address.state,
                    'pincode': address.pincode,
                    'phone': address.phone,
                    'alt_phone': address.alt_phone

                }
            else:
                if(data.meta_data is not None):
                    meta_data = json.loads(data.meta_data)
                    if(meta_data['address']['alternative_phone'] == None):
                        meta_data['address']['alternative_phone'] = ''
                    shipping = {
                        'ship_to': meta_data['address']['ship_to'],
                        'address': str(meta_data['address']['address']) + ", " + str(meta_data['address']['locality']),
                        'city': meta_data['address']['city'],
                        'state': meta_data['address']['state'],
                        'pincode': meta_data['address']['pincode'],
                        'phone': meta_data['address']['phone'],
                        'alt_phone': meta_data['address']['alternative_phone'],
                        'country': meta_data['address']['country'],

                    }
            return shipping

        except Exception as e:
            print(e)

    async def orderActionButton(db: Session, data) -> OrdersModel:
        try:
            order = db.query(OrdersModel).filter(
                OrdersModel.id == data.id).first()
            if(order.payment_method == 'ONLINE' and order.payment_status == 1):
                action = {
                    'code': 'NONE',
                    'visible': False,
                    'button_text': ''
                }
                return_period = ''
            else:
                if(order):
                    action = {
                        'code': 'NONE',
                        'visible': False,
                        'button_text': ''
                    }

                # Cancel Button
                checkStatus = db.query(OrderStatusModel).where(
                    OrderStatusModel.order_id == data.id).order_by(desc(OrderStatusModel.id)).first()
                if(checkStatus.status != 980):
                    if(checkStatus.status < 60):
                        action = {
                            'code': 'CANCEL',
                            'visible': True,
                            'button_text': 'Cancel Order'
                        }
                    if(checkStatus.status == 80):
                        action = {
                            'code': 'RETURN CANCEL',
                            'visible': True,
                            'button_text': 'Cancel Return'
                        }

                # Return Button
                # Check Current Status
                return_period = ''
                if(checkStatus.status == 70 and checkStatus.status < 80):

                    order_date = order.created_at.strftime("%Y-%m-%d")
                    return_days = 0
                    # Check Order Items
                    items = db.query(OrderItemsModel).where(
                        OrderItemsModel.order_id == data.id).where(OrderItemsModel.status != 980).all()
                    for item in items:
                        # get Product
                        product = db.query(ProductModel).where(
                            ProductModel.id == item.product_id).first()

                        # category Return days
                        category_return_days = db.query(CategoryReturnDaysModel).where(
                            CategoryReturnDaysModel.category_id == product.category).where(CategoryReturnDaysModel.start_date <= order_date).order_by(desc(CategoryReturnDaysModel.start_date)).first()

                        if(category_return_days.return_days != 0):
                            return_days = category_return_days.return_days

                    if(return_days != 0):
                        days = return_days - 1

                        td = timedelta(days=days)
                        return_period = checkStatus.created_at + td
                        return_date = return_period.strftime("%d %b %Y")
                        return_period = return_period.strftime("%Y-%m-%d")

                        today = date.today()
                        today = today.strftime("%Y-%m-%d")

                        if(today > return_period):
                            return_period = 'Return policy ended on ' + \
                                str(return_date)
                            action = {
                                'code': 'NONE',
                                'visible': False,
                                'button_text': ''
                            }

                        else:

                            return_period = 'Return policy valid till ' + \
                                str(return_date)
                            action = {
                                'code': 'RETURN',
                                'visible': True,
                                'button_text': 'Return Order'
                            }

                            check_items = db.query(OrderItemsModel).where(OrderItemsModel.order_id == order.id).where(
                                OrderItemsModel.status > 70).where(OrderItemsModel.status != 980).first()

                            if(check_items):
                                action = {
                                    'code': 'NONE',
                                    'visible': False,
                                    'button_text': ''
                                }
                                return_period = ''

                            checkItemsReturn = db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.id).filter(
                                OrderItemsModel.status == 80).first()
                            if(checkItemsReturn):
                                action = {
                                    'code': 'RETURN CANCEL',
                                    'visible': True,
                                    'button_text': 'Return Cancel'
                                }

            return {"action": action, "return_period": return_period}
        except Exception as e:
            print(e)

    async def ordertracking(db: Session, data: OrdersModel) -> OrdersModel:
        try:
            track = {
                'visible': False,
                'track_id': '',
                'delivery_partner_name': '',
                'link': ''
            }

            # Track Going Shipment
            if(data.current_status['status_id'] == 60):

                tracking = db.query(OrderShippingModel).where(
                    OrderShippingModel.order_id == data.id).first()

                if(tracking.courier_partner == 'DELHIVERY'):
                    tracking_link = "https://www.delhivery.com/track/package/" + \
                        str(tracking.wbns)

                if(tracking.courier_partner == 'UDAAN_EXPRESS'):
                    tracking_link = "https://udaanexpress.com/track/" + \
                        str(tracking.wbns)

                if(tracking.courier_partner == 'XPRESSBEES'):
                    tracking_link = "https://ship.xpressbees.com/shipping/tracking/" + \
                        str(tracking.wbns)

                if(tracking.courier_partner == 'ECOMEXPRESS'):
                    tracking_link = "https://shiprocket.co/tracking/" + \
                        str(tracking.wbns)

                if(tracking.courier_partner == 'DTDC'):
                    tracking_link = "https://www.trackingmore.com/track/en/" + \
                        str(tracking.wbns)+str('?express=dtdc')
                track = {
                    'visible': True,
                    'track_id': tracking.wbns,
                    'delivery_partner_name': tracking.courier_partner,
                    'link': tracking_link
                }

            # Track Reverse Shipment
            if(data.current_status['status_id'] == 90):
                tracking = db.query(OrderRevreseModel).where(
                    OrderRevreseModel.order_id == data.id).first()
                if(tracking.courier_partner == 'DELHIVERY'):
                    tracking_link = "https://www.delhivery.com/track/package/" + \
                        str(tracking.wbns)

                if(tracking.courier_partner == 'UDAAN_EXPRESS'):
                    tracking_link = "https://udaanexpress.com/track/" + \
                        str(tracking.wbns)

                if(tracking.courier_partner == 'XPRESSBEES'):
                    tracking_link = "https://ship.xpressbees.com/shipping/tracking/" + \
                        str(tracking.wbns)

                if(tracking.courier_partner == 'ECOMEXPRESS'):
                    tracking_link = "https://shiprocket.co/tracking/" + \
                        str(tracking.wbns)

                if(tracking.courier_partner == 'DTDC'):
                    tracking_link = "https://www.trackingmore.com/track/en/" + \
                        str(tracking.wbns)+str('?express=dtdc')

                track = {
                    'visible': True,
                    'track_id': tracking.wbns,
                    'delivery_partner_name': tracking.courier_partner,
                    'link': tracking_link
                }

            return track

        except Exception as e:
            print(e)

    # Check Order Refund Message
    async def orderRefundMessage(db: Session, data: OrdersModel) -> OrdersModel:
        try:

            refund_message = ''
            # Current Status
            current_status = db.query(OrderStatusModel).where(
                OrderStatusModel.order_id == data.id).order_by(desc(OrderStatusModel.id)).first()

            if(current_status.status == 61):
                if(data.payment_method == 'ONLINE'):
                    refund_message = 'Your order has been cancelled, you will get your refund within 5 - 7 working days.'

            # Check Cancel Order with Online Payment Mode
            wallet_amount = 0
            if(data.payment_method == 'ONLINE'):
                check_status = db.query(OrderStatusModel).filter(
                    OrderStatusModel.status == 980).filter(OrderStatusModel.order_id == data.id).first()
                if(check_status):
                    # Order Items
                    orderitems = db.query(OrderItemsModel).filter(
                        OrderItemsModel.order_id == data.id).filter(OrderItemsModel.status == 980).all()
                    item_total_amount = 0
                    item_total_return_amount = 0

                    for orderitem in orderitems:
                        if(orderitem.refund == 0):
                            wallet_amount += float(orderitem.wallet_amount)
                            item_total_return_amount += float(
                                orderitem.total)
                        else:
                            item_total_amount += float(orderitem.total)

                    order_date = orderitems[0].created_at.strftime("%Y-%m-%d")

                    # rest Total Amount NEW CHANGES
                    rest_total_amount = (
                        item_total_return_amount + item_total_amount)
                    shipping_charge = db.query(ShippingChargeModel).filter(
                        ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

                    delivery_charge = orderDeliveryCalculation(db=db, free_delivery=data.free_delivery, user_id=data.user_id, order_date=orderitems[0].created_at,
                                                               app_version=data.app_version, order_amount=rest_total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=data.payment_method)

                    if(data.discount != 0):
                        order_discount = db.query(OrderDiscountModel).filter(
                            OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()
                        discount_rate = order_discount.discount
                        if(data.discount_rate != 0):
                            discount_rate = data.discount_rate
                        if(item_total_amount != 0):
                            discount_amount = orderDiscountCalculation(app_version=data.app_version,
                                                                       order_amount=item_total_amount, discount_amount=data.discount, discount_rate=discount_rate)
                            item_total_amount = (
                                item_total_amount - discount_amount)
                        if(item_total_return_amount != 0):
                            discount_amount = orderDiscountCalculation(app_version=data.app_version,
                                                                       order_amount=item_total_return_amount, discount_amount=data.discount, discount_rate=discount_rate)
                            item_total_return_amount = (
                                item_total_return_amount - discount_amount)

                    # rest Total Amount
                    rest_total_amount = (
                        item_total_return_amount + item_total_amount)
                    # Check Shipping Charge
                    # delivery_charge = 0
                    # if(data.delivery_charge != 0): CHANGES RAHUL
                    # shipping_charge = db.query(ShippingChargeModel).filter(
                    #     ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

                    # delivery_charge = orderDeliveryCalculation(db=db, free_delivery=data.free_delivery, user_id=data.user_id, order_date=orderitems[0].created_at,
                    #                                            app_version=data.app_version, order_amount=rest_total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=data.payment_method)

                    if(item_total_return_amount > 0):
                        item_total_return_amount = (
                            item_total_return_amount + delivery_charge)
                    else:
                        item_total_return_amount = (
                            rest_total_amount + delivery_charge)    
                    item_total_return_amount = (
                        float(item_total_return_amount) - float(wallet_amount))
                    refund_message = 'We have accepted your cancel request for refund Rs/- ' + \
                        str(roundOf(item_total_return_amount)) + \
                        ' as refund will be credited to your Original mode of Payment within 5 - 7 days.'

                    return refund_message

                check_status = db.query(OrderStatusModel).having(func.max(
                    OrderStatusModel.status) < 90).filter(OrderStatusModel.order_id == data.id).first()

                # CHANGES NEW (rahul)
                check_items = db.query(OrderItemsModel).filter(
                    OrderItemsModel.order_id == data.id).filter(OrderItemsModel.status == 980).count()

                if(check_status and check_items != 0):
                    # Order Items
                    orderitems = db.query(OrderItemsModel).filter(
                        OrderItemsModel.order_id == data.id).all()
                    item_total_amount = 0
                    item_total_return_amount = 0

                    for orderitem in orderitems:
                        if(orderitem.status == 980 and orderitem.refund == 0):
                            wallet_amount += float(orderitem.wallet_amount)
                            item_total_return_amount += float(
                                orderitem.total)
                        else:

                            item_total_amount += float(orderitem.total)

                    order_date = orderitems[0].created_at.strftime("%Y-%m-%d")
                    if(data.discount != 0):
                        order_discount = db.query(OrderDiscountModel).filter(
                            OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()
                        discount_rate = order_discount.discount
                        if(data.discount_rate != 0):
                            discount_rate = data.discount_rate
                        if(item_total_amount != 0):
                            discount_amount = orderDiscountCalculation(app_version=data.app_version,
                                                                       order_amount=item_total_amount, discount_amount=data.discount, discount_rate=discount_rate)
                            item_total_amount = (
                                item_total_amount - discount_amount)
                        if(item_total_return_amount != 0):
                            discount_amount = orderDiscountCalculation(app_version=data.app_version,
                                                                       order_amount=item_total_return_amount, discount_amount=data.discount, discount_rate=discount_rate)
                            item_total_return_amount = (
                                item_total_return_amount - discount_amount)

                    # rest Total Amount
                    rest_total_amount = (
                        item_total_return_amount + item_total_amount)
                    # Check Shipping Charge
                    # delivery_charge = 0
                    # if(data.delivery_charge != 0): CHANGES RAHUL
                    shipping_charge = db.query(ShippingChargeModel).filter(
                        ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

                    delivery_charge = orderDeliveryCalculation(db=db, free_delivery=data.free_delivery, user_id=data.user_id, order_date=orderitems[0].created_at,
                                                               app_version=data.app_version, order_amount=rest_total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=data.payment_method)

                    check_item_total_return_amount = item_total_return_amount
                    if(data.delivery_charge == 0):
                        item_total_return_amount = (
                            item_total_return_amount - delivery_charge)
                    else:
                        item_total_return_amount = item_total_return_amount

                    item_total_return_amount = (
                        float(item_total_return_amount) - float(wallet_amount))
                    if(check_item_total_return_amount > 0):
                        refund_message = 'We have accepted your cancel request for refund Rs/- ' + \
                            str(roundOf(item_total_return_amount)) + \
                            ' as refund will be credited to your Original mode of Payment within 5 - 7 days.'

                        return refund_message

            # Check Order return complete Status (If order has been returned by Buyer)
            check_status = db.query(OrderStatusModel).where(
                OrderStatusModel.order_id == data.id).filter(OrderStatusModel.status >= 100).filter(OrderStatusModel.status != 980).first()

            # Order Items
            orderitems = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == data.id).all()

            # Order Date

            order_date = orderitems[0].created_at.strftime("%Y-%m-%d")

            if(check_status):

                item_total_amount = 0
                item_total_return_amount = 0

                delivery_charge = 0
                for orderitem in orderitems:
                    if(orderitem.status >= 90 and orderitem.status != 91 and orderitem.status != 980):
                        wallet_amount += float(orderitem.wallet_amount)
                        item_total_amount += float(
                            orderitem.price) * orderitem.quantity

                # Check Order Delivery Charge and Discount
                if(item_total_amount != 0):
                    if(data.discount != 0):
                        order_discount = db.query(OrderDiscountModel).filter(
                            OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()
                        discount_rate = order_discount.discount

                        if(data.discount_rate != 0):
                            discount_rate = data.discount_rate
                        if(item_total_amount != 0):
                            discount_amount = orderDiscountCalculation(app_version=data.app_version,
                                                                       order_amount=item_total_amount, discount_amount=data.discount, discount_rate=discount_rate)
                        item_total_amount = (
                            item_total_amount - discount_amount)

                # Check Delivery
                shipping_charge = db.query(ShippingChargeModel).filter(
                    ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

                delivery_charge = orderDeliveryCalculation(db=db, free_delivery=data.free_delivery, user_id=data.user_id, order_date=orderitems[0].created_at,
                                                           app_version=data.app_version, order_amount=item_total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=data.payment_method)

                item_total_amount = (
                    item_total_amount + delivery_charge)

                item_total_amount = (
                    float(item_total_amount) - float(wallet_amount))
                if(data.payment_method == 'COD'):
                    refund_message = 'We have completed your request for refund Rs/- ' + \
                        str(roundOf(item_total_amount)) + \
                        ' as refund will be credited to your bank account within 5 - 7 days.'
                else:
                    refund_message = 'We have accepted your return request for refund Rs/- ' + \
                        str(roundOf(item_total_amount)) + \
                        ' as refund will be credited to your Original mode of Payment within 5 - 7 days.'

                return refund_message
            check_status = db.query(OrderStatusModel).filter(
                OrderStatusModel.order_id == data.id).having(func.max(OrderStatusModel.status) == 70).group_by(OrderStatusModel.order_id).first()

            if(check_status):
                item_total_amounts = 0
                item_total_return_amount = 0
                for orderitem in orderitems:

                    if(orderitem.status < 90):
                        item_total_amounts += float(
                            orderitem.price) * orderitem.quantity

                    if(orderitem.status >= 90 and orderitem.status != 91 and orderitem.status != 980):
                        wallet_amount += float(orderitem.wallet_amount)
                        item_total_return_amount += float(
                            orderitem.price) * orderitem.quantity

                # Check Order Delivery Charge
                if(item_total_return_amount != 0):

                    order_date = orderitems[0].created_at.strftime("%Y-%m-%d")
                    # Check Order Delivery Charge and Discount
                    if(data.discount != 0):
                        order_discount = db.query(OrderDiscountModel).filter(
                            OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()
                        discount_rate = order_discount.discount

                        if(data.discount_rate != 0):
                            discount_rate = data.discount_rate
                        if(item_total_amounts != 0):
                            discount_amount = orderDiscountCalculation(app_version=data.app_version,
                                                                       order_amount=item_total_return_amount, discount_amount=data.discount, discount_rate=discount_rate)
                        item_total_return_amount = (
                            item_total_return_amount - discount_amount)

                    delivery_charge = 0
                    if(data.delivery_charge != 0):
                        item_total_return_amount = item_total_return_amount
                        item_total_return_amount = (
                            float(item_total_return_amount) - float(wallet_amount))
                        refund_message = 'We have completed your request for refund Rs/- ' + \
                            str(roundOf(item_total_return_amount)) + \
                            ' as refund will be credited to your bank account within 5 - 7 days.'
                    else:
                        shipping_charge = db.query(ShippingChargeModel).filter(
                            ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

                        delivery_charge = orderDeliveryCalculation(db=db, free_delivery=data.free_delivery, user_id=data.user_id, order_date=orderitems[0].created_at,
                                                                   app_version=data.app_version, order_amount=item_total_amounts, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=data.payment_method)

                        item_total_return_amount = (
                            item_total_return_amount - delivery_charge)
                        item_total_return_amount = (
                            float(item_total_return_amount) - float(wallet_amount))
                        if(delivery_charge != 0):
                            refund_message = 'We have completed your return request. As per our free delivery schedule you will get Rs/- ' + \
                                str(roundOf(item_total_return_amount)) + \
                                '  within 5 - 7 days, because your order amount is not fulfilling the regulation.'
                        else:
                            refund_message = 'We have completed your request for refund Rs/- ' + \
                                str(roundOf(item_total_return_amount)) + \
                                ' as refund will be credit to you bank account within 5 - 7 days.'

                        return refund_message

            return refund_message
        except Exception as e:
            print(e)

    # Order Checkout
    async def checkOut(db: Session, data: OrdersModel, user_id: int) -> OrdersModel:
        try:

            # get cart items group by seller
            items = db.query(func.count(CartModel.seller_id), CartModel.seller_id, CartModel.user_id).join(ProductModel, ProductModel.id ==
                                                                                                           CartModel.product_id).where(ProductModel.status == 51).where(CartModel.user_id == user_id).group_by(CartModel.seller_id).all()

            # Maximum Ref ID
            maxref = db.query(func.max(OrdersModel.reff),
                              ).one()
            maxref = maxref[0] + 1

            # Address Meta
            address = db.query(ShippingAddressModel).where(
                ShippingAddressModel.id == data.address_id).first()
            address = {
                'address': {
                    'ship_to': address.ship_to,
                    'address': address.address,
                    'city': address.city,
                    'phone': address.phone,
                    'alternative_phone': address.alt_phone,
                    'state': address.state,
                    'locality': address.locality,
                    'pincode': address.pincode,
                    'country': 'India'
                }
            }

            if(len(items) > 0):
                cart_items = []
                item_counts = []
                total_order_amount = 0
                for item in items:
                    # Get Cart items
                    c_items = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).where(
                        CartModel.seller_id == item.seller_id).where(ProductModel.status == 51).where(CartModel.user_id == user_id).all()
                    cart_items.append(c_items)

                if(len(cart_items) > 0):
                    for total_items in cart_items:
                        sub_total = 0
                        orders_items = []
                        tax = 0
                        seller_products = []

                        for oitem in total_items:

                            # Get Product
                            product = db.query(ProductModel).where(
                                ProductModel.id == oitem.product_id).where(ProductModel.status == 51).first()

                            # Get pricing Detail
                            price_id = oitem.uuid.split('-')
                            price = db.query(ProductPricingModel).where(ProductPricingModel.id == price_id[1]).where(
                                ProductPricingModel.deleted_at.is_(None)).first()
                            if(price):
                                # Checking Pricing Stock
                                check_stock = db.query(InventoryModel).where(
                                    InventoryModel.pricing_id == price.id).first()

                                if(check_stock.out_of_stock == 0):

                                    # Append Product data
                                    seller_products.append(product)

                                    # Item total price
                                    total_price = (
                                        oitem.price * oitem.tax) / 100 + oitem.price

                                    if (float(total_price) % 1) >= 0.5:
                                        total_price = math.ceil(total_price)
                                    else:
                                        total_price = round(total_price)

                                    # Sub Total
                                    sub_total += (total_price * oitem.quantity)

                                    # Order Total Amount
                                    total_order_amount += (total_price *
                                                           oitem.quantity)

                                    # Count Total Items
                                    orders_items.append(oitem)
                                    item_counts.append(oitem)

                                    # Total tax calculate
                                    pp = float(total_price) - \
                                        float(oitem.price)

                                    if (float(pp) % 1) >= 0.5:
                                        pp = math.ceil(pp)
                                    else:
                                        pp = round(pp)

                                    tax += (pp * oitem.quantity)

                        # Today Date
                        today = date.today()
                        # YY-mm-dd
                        today = today.strftime("%Y-%m-%d")
                        # Get Shipping Charge

                        shipping_charge = db.query(ShippingChargeModel).filter(
                            ShippingChargeModel.start_date <= today).order_by(ShippingChargeModel.id.desc()).first()

                        # shipping_charge = db.execute("SELECT id, rate, start_date, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                        #                              {"param": today}).first()

                        delivery = 0
                        if(shipping_charge.order_limit != 0):
                            if(round(sub_total, 0) < shipping_charge.order_limit):
                                delivery += shipping_charge.rate

                        else:
                            delivery = shipping_charge.rate

                        # Check Discount
                        discount = 0
                        payment_method = data.payment_type
                        if(data.discount != 0):

                            check_discount = db.query(OrderDiscountModel).filter(
                                OrderDiscountModel.start_date <= today).filter(OrderDiscountModel.payment_method == payment_method).order_by(OrderDiscountModel.id.desc()).first()

                            # check_discount = db.execute("SELECT id, discount, start_date, payment_method FROM order_discount WHERE payment_method=:method and start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
                            #                             {"method": payment_method, "param": today}).first()

                            # Check discount for both payment type
                            if(check_discount is None):
                                check_discount = db.query(OrderDiscountModel).filter(
                                    OrderDiscountModel.start_date <= today).order_by(OrderDiscountModel.id.desc()).first()

                                # check_discount = db.execute("SELECT id, discount, start_date, payment_method FROM order_discount WHERE payment_method='BOTH' and start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
                                #                             {"param": today}).first()

                            if(check_discount):
                                c_discount = (
                                    float(sub_total) * float(check_discount.discount)) / 100
                                discount += c_discount

                        # Insert Order
                        paymentstatus = 1
                        if(data.payment_type == 'ONLINE'):
                            paymentstatus = 51

                            # Random Generate Order Number
                        order_number = uuid.uuid4().hex.upper()[0:13]
                        if(sub_total > 0):
                            dborder = OrdersModel(
                                reff=maxref,
                                order_number="ORD-"+str(order_number),
                                user_id=user_id,
                                grand_total=round(sub_total, 0),
                                total_tax=round(tax, 0),
                                discount=round(discount, 0),
                                delivery_charge=delivery,
                                item_count=len(total_items),
                                payment_status=paymentstatus,
                                payment_method=data.payment_type,
                                meta_data=json.dumps(address),
                                address_id=data.address_id,
                                app_version='V3',
                                created_at=datetime.now(),
                                updated_at=datetime.now()
                            )
                            db.add(dborder)
                            db.commit()
                            db.refresh(dborder)

                            # Insert Order Status
                            dborderstatus = OrderStatusModel(
                                order_id=dborder.id,
                                status=0,
                                created_at=datetime.now(),
                                updated_at=datetime.now()
                            )
                            db.add(dborderstatus)
                            db.commit()
                            db.refresh(dborderstatus)

                            # Insert order Items
                            for orderitem in orders_items:
                                # Get pricing Detail
                                product_price_id = orderitem.uuid.split('-')
                                product_price = db.query(ProductPricingModel).where(ProductPricingModel.id == product_price_id[1]).where(
                                    ProductPricingModel.deleted_at.is_(None)).first()

                                imagedata = 0
                                if(product_price):
                                    # Checking Inventory for product Stock
                                    inventory = db.query(InventoryModel).where(
                                        InventoryModel.pricing_id == product_price.id).first()

                                    if(inventory.unlimited == 0 and inventory.out_of_stock == 0):
                                        total_stock = (
                                            int(inventory.stock) - int(orderitem.quantity))

                                        inventory.stock = total_stock
                                        db.flush()
                                        db.commit()

                                    if(inventory.stock == 0 and inventory.unlimited == 0):
                                        inventory.out_of_stock = 1
                                        db.flush()
                                        db.commit()

                                # Store Images
                                images = 0

                                prpdo = db.query(ProductMediaModel).where(
                                    ProductMediaModel.model_id == orderitem.product_id).first()
                                images = prpdo.id

                                price_images = 0
                                if (product_price.default_image != 0):
                                    checkImg = db.query(ProductMediaModel).where(
                                        ProductMediaModel.id == product_price.default_image).first()

                                    if(checkImg):
                                        price_images = product_price.default_image

                                if(price_images == 0):
                                    imagedata = images
                                else:
                                    imagedata = price_images

                                dborderitem = OrderItemsModel(
                                    order_id=dborder.id,
                                    product_id=orderitem.product_id,
                                    quantity=orderitem.quantity,
                                    price=product_price.price,
                                    tax=product_price.tax,
                                    attributes=orderitem.attributes,
                                    uuid=orderitem.uuid,
                                    hsn_code=orderitem.hsn_code,
                                    images="["+str(imagedata)+"]",
                                    status=0,
                                    created_at=datetime.now(),
                                    updated_at=datetime.now()
                                )
                                db.add(dborderitem)
                                db.commit()
                                db.refresh(dborderitem)

                            # Seller Account Maintain
                            seller = db.query(UserModel).where(
                                UserModel.id == seller_products[0].userid).first()

                            dbaccount = AccountsModel(
                                user_id=seller.id,
                                txn_date=today,
                                txn_description='ORDERED (' +
                                str(order_number) + ')',
                                txn_type="DR",
                                txn_amount=sub_total
                            )
                            db.add(dbaccount)
                            db.commit()
                            db.refresh(dbaccount)

                            # Insert Transaction for every Order
                            if(data.payment_type == 'COD'):
                                dbtransaction = TransactionsModel(
                                    payment_method='cod',
                                    order_ref_id=maxref,
                                    amount=total_order_amount
                                )

                            db.add(dbtransaction)
                            db.commit()
                            db.refresh(dbtransaction)

                        # Sending Message and Notification (HOLD FOR NOW)

                return maxref

            else:
                return False

        except Exception as e:
            return False

    # Seller Orders

    # GET ORDERS STATUS WISE (FOR SELLER)
    async def getSellerOrdersStatusWise(db: Session, status: int, user_id: int):
        try:

            # All Orders
            if(status == 999):
                return db.query(OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == user_id).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            # Pending Orders
            if(status == 0):
                return db.query(OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrderItemsModel.status == 0).filter(ProductModel.userid == user_id).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            # Accepted Orders
            if(status == 10):

                return db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 10).order_by(OrdersModel.id.desc())

            # Approved Orders
            if(status == 30):
                return db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 30).order_by(OrdersModel.id.desc())

            # Packed Orders
            if(status == 40):
                return db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 40).order_by(OrdersModel.id.desc())

            # Shipped Orders
            if(status == 60):
                return db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 60).order_by(OrdersModel.id.desc())

            # Delivered Orders
            if(status == 70):
                return db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).group_by(OrderStatusModel.order_id).filter(OrderStatusModel.status.between(70, 81)).order_by(OrdersModel.id.desc())

             # Return Approved Orders
            if(status == 90):
                return db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 90).order_by(OrdersModel.id.desc())

             # Reversed Orders
            if(status == 61):
                return db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 61).order_by(OrdersModel.id.desc())

             # Return Complered Orders
            if(status == 100):
                return db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).group_by(OrderStatusModel.order_id).filter(OrderStatusModel.status.between(100, 110)).order_by(OrdersModel.id.desc())

            # Cancelled Orders
            if(status == 980):
                return db.query(OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.updated_at.desc())

        except Exception as e:
            print(e)

    # Search Orders Seller (Status Wise)
    async def searchSellerOrdersStatusWise(db: Session, status: int, user_id: int, search: str = '', from_date: str = '', to_date: str = ''):
        try:
            # Search Key
            if(search != ''):
                search = "%{}%".format(search)

            # All Orders
            if(status == 999):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())
                else:
                    return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            # Pending Orders
            if(status == 0):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrderItemsModel.status == 0).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrderItemsModel.status == 0).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())
                else:
                    return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrderItemsModel.status == 0).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())
            # Accepted Orders
            if(status == 10):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 10).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 10).order_by(OrdersModel.id.desc())

                else:
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 10).order_by(OrdersModel.id.desc())
            # Approved Orders
            if(status == 30):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 30).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 30).order_by(OrdersModel.id.desc())

                else:
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 30).order_by(OrdersModel.id.desc())
            # Packed Orders
            if(status == 40):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 40).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 40).order_by(OrdersModel.id.desc())
                else:
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 40).order_by(OrdersModel.id.desc())
            # Shipped Orders
            if(status == 60):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 60).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 60).order_by(OrdersModel.id.desc())
                else:
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 60).order_by(OrdersModel.id.desc())

            # Delivered Orders
            if(status == 70):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).filter(OrderStatusModel.status.between(70, 81)).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrderStatusModel.order_id).filter(OrderStatusModel.status.between(70, 81)).order_by(OrdersModel.id.desc())
                else:
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).filter(OrderStatusModel.status.between(70, 81)).order_by(OrdersModel.id.desc())

             # Return Approved Orders
            if(status == 90):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 90).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 90).order_by(OrdersModel.id.desc())
                else:
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 90).order_by(OrdersModel.id.desc())

             # Reversed Orders
            if(status == 61):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 61).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 61).order_by(OrdersModel.id.desc())
                else:
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 61).order_by(OrdersModel.id.desc())
             # Return Complered Orders
            if(status == 100):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).filter(OrderStatusModel.status.between(100, 110)).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrderStatusModel.order_id).filter(OrderStatusModel.status.between(100, 110)).order_by(OrdersModel.id.desc())

                else:
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderStatusModel.order_id).filter(OrderStatusModel.status.between(100, 110)).order_by(OrdersModel.id.desc())

            # Cancelled Orders
            if(status == 980):
                if(search != '' and from_date != ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.id.desc())
                elif(search != '' and from_date == ''):
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrdersModel.order_number.like(search)).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.id.desc())
                else:
                    return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                        ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") >= from_date).filter(func.date_format(OrderStatusModel.created_at,  "%Y-%m-%d") <= to_date).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.id.desc())

        except Exception as e:
            print(e)

    # Get all orders (admin)

    async def getAllOrdersAdmin(db: Session, status: str, page: int, limit: int):
        try:

            if(status == '999'):
                return db.query(OrdersModel).order_by(OrdersModel.id.desc())

            if(status == '0'):
                # return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 0).order_by(OrdersModel.id.desc())
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 0).order_by(OrdersModel.id.desc())
            if(status == '10'):
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 10).order_by(OrdersModel.id.desc())

            if(status == '30'):
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 30).order_by(OrdersModel.id.desc())

            if(status == '40'):
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 40).order_by(OrdersModel.id.desc())

            if(status == '60'):
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 60).order_by(OrdersModel.id.desc())

            if(status == '70'):
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 70).order_by(OrdersModel.id.desc())

            if(status == '80'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 80).order_by(OrdersModel.id.desc())

            if(status == '81'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 81).order_by(OrdersModel.id.desc())

            if(status == '90'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 90).order_by(OrdersModel.id.desc())

            if(status == '61'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 61).order_by(OrdersModel.id.desc())

            if(status == '100'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status == 100).order_by(OrdersModel.id.desc())

            if(status == '110'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 110).order_by(OrdersModel.id.desc())

            if(status == '980'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.id.desc())

        except Exception as e:
            print(e)

    # Get Cancelled Orders
    async def getCancelledOrdersforAdmin(db: Session, status: str, type: str):
        try:
            if(status == '980' and type == 'Yes'):
                return db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrdersModel.round_off, OrderItemsModel.order_id, OrderItemsModel.status).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.cancelled_by_seller == type).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.updated_at.desc())
            else:
                return db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrdersModel.round_off, OrderItemsModel.order_id, OrderItemsModel.status).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.cancelled_by_seller == type).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.updated_at.desc())

        except Exception as e:
            print(e)

    # Customize Order List Data for Seller

    async def customizeOrderListData(request: Request, db: Session, data: OrdersModel) -> OrdersModel:
        try:

            invoice = ''

            orderdata = []

            for order in data:
                order = db.query(OrdersModel).filter(
                    OrdersModel.id == order.id).first()

                # Product Images
                products = []

                # Customize Order Reff
                reff_id = 'ASEZ'+str(order.reff)+"U"+str(order.user_id)

                # Customie Order Invoice
                if(order.invoice == None):
                    order.invoice = invoice
                else:
                    checkinvoice = db.execute("SELECT orders.id, orders.invoice FROM orders WHERE orders.id =:param AND orders.invoice REGEXP '^[0-9]+$'  and app_version = 'V4'", {
                        "param": order.id
                    }).first()
                    if(checkinvoice is not None):
                        if(int(checkinvoice.invoice) <= 9):
                            order.invoice = str('ASEZ202320240') + \
                                str(checkinvoice.invoice)
                        else:
                            order.invoice = str('ASEZ20232024') + \
                                str(checkinvoice.invoice)

                    else:
                        order.invoice = order.invoice

                # Calculate Total Amount and Items
                items = order.order_items.filter(
                    OrderItemsModel.status == 0).all()
                if(len(items) == 0):
                    items = order.order_items.filter(
                        OrderItemsModel.status <= 81).all()

                if(len(items) == 0):
                    items = order.order_items.filter(
                        OrderItemsModel.status == 81).all()

                if(len(items) == 0):
                    items = order.order_items.filter(OrderItemsModel.status > 10).filter(
                        OrderItemsModel.status < 980).all()

                if(len(items) == 0):
                    items = order.order_items.filter(
                        OrderItemsModel.status == 980).all()

                total_amount = 0
                proImgObj = db.query(ProductMediaModel)
                for item in items:

                    # Product Images
                    product_price_id = item.uuid.split('-')
                    product_price = db.query(ProductPricingModel).where(
                        ProductPricingModel.id == product_price_id[1]).first()
                    # Get Product Detail
                    product = db.query(ProductModel).where(
                        ProductModel.id == item.product_id).first()

                    filename = ''
                    if(item.images != None):
                        image = proImgObj.filter(
                            ProductMediaModel.id == item.images.strip("[]")).first()

                        if(image is not None):
                            filename = image.file_path

                        else:
                            image = proImgObj.filter(ProductMediaModel.model_id == item.product_id).filter(ProductMediaModel.deleted_at.is_(None)).order_by(
                                ProductMediaModel.default_img.desc()).first()

                            filename = image.file_path

                    else:

                        if(product_price and product_price.default_image != 0):
                            image = proImgObj.filter(
                                ProductMediaModel.id == product_price.default_image).first()

                            if(image is not None):
                                filename = image.file_path
                        else:

                            image = proImgObj.filter(ProductMediaModel.model_id == item.product_id).filter(ProductMediaModel.deleted_at.is_(None)).order_by(
                                ProductMediaModel.default_img.desc()).first()

                            if(image is not None):
                                filename = image.file_path

                    img = filename

                    products.append(img)

                    # Aseztak Service
                    # aseztak_service = Services.aseztak_services(
                    #     item.created_at, db=db)
                    today_date = item.created_at.strftime(
                        '%Y-%m-%d')
                    aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                    if(aseztak_service is None):

                        # Calculate Product Price
                        product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
                                                                gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)

                        total_amount += product_price * item.quantity
                    else:
                        product_price: ProductModel = await ProductsHelper.getPrice(db, product, product_price, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item.id)
                        # Calculate Product Price
                        # product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
                        #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order.app_version)

                        total_amount += product_price * item.quantity

                # OrderDate
                orderdate = order.created_at.strftime(
                    "%B %d %Y")

                # Current Status
                all_static_status = OrderStatus.AllstatusList()

                latestStatus = order.order_status.order_by(
                    OrderStatusModel.id.desc()).first()

                for st in all_static_status:
                    if(latestStatus.status == st['status_id']):
                        current_status = {
                            'status_id': latestStatus.status,
                            'status_title': st['status_title'],
                            'message': st['message'],
                            'created_at': latestStatus.created_at.strftime(
                                "%B %d %Y")
                        }
                # Order failed status
                checkorder_failed = await OrderHelper.checkorderfailed(db=db, order_id=order.id)
                if(checkorder_failed['status'] != 0):
                    current_status = {
                        'status_id': checkorder_failed['status'],
                        'status_title': 'ORDER FAILED',
                        'message': 'Order failed',
                        'created_at': order.created_at.strftime(
                            "%B %d %Y")
                    }
                # Check Order Confirm
                # buyer = db.query(UserModel).filter(
                #     UserModel.id == order.user_id).first()

                if(order.status == 0):

                    order_confirmed = {
                        'value': 'No',
                        'text': 'Not Confirmed'
                    }
                else:
                    order_confirmed = {
                        'value': 'Yes',
                        'text': 'Confirmed'
                    }

                ord = {
                    'id': order.id,
                    'reff': reff_id,
                    'order_number': order.order_number,
                    'order_confirmed': order_confirmed,
                    'message': '',
                    'images': products,
                    'total_items': len(items),
                    'total_amount': floatingValue(total_amount),
                    'invoice': order.invoice,
                    'payment_method': order.payment_method,
                    'created_at': orderdate,
                    'status': current_status
                }

                orderdata.append(ord)

            return orderdata

        except Exception as e:
            print(e)

    # Customize Order Detail Data for Seller
    async def customizeOrderDetails(db: Session, data: OrdersModel) -> OrdersModel:
        try:

            order = db.query(OrdersModel).filter(
                OrdersModel.id == data).first()

            # Calculate Total Amount and Items
            # get all items
            items = order.order_items.filter(OrderItemsModel.status == 0).all()
            if(len(items) == 0):
                items = order.order_items.filter(
                    OrderItemsModel.status <= 81).all()

            if(len(items) == 0):
                items = order.order_items.filter(
                    OrderItemsModel.status == 81).all()

            if(len(items) == 0):
                items = order.order_items.filter(OrderItemsModel.status > 10).filter(
                    OrderItemsModel.status < 980).all()

            if(len(items) == 0):
                items = order.order_items.filter(
                    OrderItemsModel.status == 980).all()

            total_amount = 0
            for item in items:
                pricingdata = item.uuid.split('-')
                pricingdata = db.query(ProductPricingModel).filter(
                    ProductPricingModel.id == pricingdata[1]).first()
                # Product Object
                productdata = db.query(ProductModel).filter(
                    ProductModel.id == item.product_id).first()
                # Aseztak Service
                # aseztak_service = Services.aseztak_services(
                #     item.created_at, db=db)
                today_date = item.created_at.strftime(
                    '%Y-%m-%d')
                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
                if(aseztak_service is None):

                    # Calculate Product Price
                    product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
                                                            gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)

                    total_amount += product_price * item.quantity
                else:
                    product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item.id)
                    # Calculate Product Price
                    # product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
                    #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order.app_version)

                    total_amount += product_price * item.quantity

            # OrderDate
            orderdate = order.created_at.strftime(
                "%B %d %Y")

            # Current Status
            all_static_status = OrderStatus.AllstatusList()

            latestStatus = order.order_status.order_by(
                OrderStatusModel.id.desc()).first()

            for st in all_static_status:

                if(latestStatus.status == 30):
                    current_status = {
                        'status_id': 30,
                        'status_title': 'Approved',
                        'message': 'Approved by System',
                        'created_at': latestStatus.created_at.strftime(
                            "%B %d %Y")
                    }

                if(latestStatus.status == 80 or latestStatus.status == 81):
                    current_status = {
                        'status_id': 70,
                        'status_title': 'Delivered',
                        'message': 'Order has been delivered',
                        'created_at': latestStatus.created_at.strftime(
                            "%B %d %Y")
                    }
                else:
                    if(latestStatus.status == st['status_id']):
                        current_status = {
                            'status_id': latestStatus.status,
                            'status_title': st['status_title'],
                            'message': st['message'],
                            'created_at': latestStatus.created_at.strftime(
                                "%B %d %Y")
                        }
            # ORDER FAILED STATUS
            checkorderfailed = await OrderHelper.checkorderfailed(db=db, order_id=order.id)
            if(checkorderfailed['status'] != 0):
                current_status = {
                    'status_id': checkorderfailed['status'],
                    'status_title': 'ORDER FAILED',
                    'message': 'Order Failed',
                    'created_at': order.created_at.strftime(
                        "%B %d %Y")
                }

            # Customize Order Message
            if(order.message == None):
                order.message = ''

            # Customize Order Invoice
            if(order.invoice == None):
                order.invoice = ''

            # Courier
            courier = {
                'courier_partner': '',
                'tracking_id': '',
                'tracking_link': ''
            }

            # Shipping Tracking
            if(latestStatus.status == 60):
                shipping = order.order_shipping.first()
                tracking_id = shipping.wbns

                if(shipping.courier_partner == 'DELHIVERY'):
                    tracking_link = "https://www.delhivery.com/track/package/" + \
                        str(shipping.wbns)

                if(shipping.courier_partner == 'UDAAN_EXPRESS'):
                    tracking_link = "https://udaanexpress.com/track/" + \
                        str(shipping.wbns)

                if(shipping.courier_partner == 'XPRESSBEES'):
                    tracking_link = "https://ship.xpressbees.com/shipping/tracking/" + \
                        str(shipping.wbns)

                if(shipping.courier_partner == 'ECOMEXPRESS'):
                    tracking_link = "https://shiprocket.co/tracking/" + \
                        str(shipping.wbns)

                if(shipping.courier_partner == 'E-kart'):
                    tracking_link = "https://ekartlogistics.com/shipmenttrack/" + \
                        str(shipping.wbns)

                if(shipping.courier_partner == 'DELHIVERY1'):
                    tracking_link = "https://www.delhivery.com/track/package/" + \
                        str(shipping.wbns)

                if(shipping.courier_partner == 'AMAZONSHIP'):
                    tracking_link = "https://shiprocket.co/tracking/" + \
                        str(shipping.wbns)

                if(shipping.courier_partner == 'DTDC'):
                    tracking_link = "https://www.trackingmore.com/track/en/" + \
                        str(shipping.wbns)+str('?express=dtdc')

                courier = {
                    'courier_partner': shipping.courier_partner,
                    'tracking_id': tracking_id,
                    'tracking_link': tracking_link,
                }

            # Reverse Tracking

            checkItemstatus = order.order_items.filter(
                OrderItemsModel.status == 90).first()

            if(checkItemstatus is not None and checkItemstatus.status == 90):

                reverse = order.order_reverse.first()

                tracking_id = reverse.wbns

                if(reverse.courier_partner == 'DELHIVERY'):
                    tracking_link = "https://www.delhivery.com/track/package/" + \
                        str(reverse.wbns)

                if(reverse.courier_partner == 'UDAAN_EXPRESS'):
                    tracking_link = "https://udaanexpress.com/track/" + \
                        str(reverse.wbns)

                if(reverse.courier_partner == 'XPRESSBEES'):
                    tracking_link = "https://ship.xpressbees.com/shipping/tracking/" + \
                        str(reverse.wbns)

                if(reverse.courier_partner == 'ECOMEXPRESS'):
                    tracking_link = "https://shiprocket.co/tracking/" + \
                        str(reverse.wbns)

                if(reverse.courier_partner == 'E-kart'):
                    tracking_link = "https://ekartlogistics.com/shipmenttrack/" + \
                        str(reverse.wbns)

                if(reverse.courier_partner == 'DELHIVERY1'):
                    tracking_link = "https://www.delhivery.com/track/package/" + \
                        str(reverse.wbns)

                if(reverse.courier_partner == 'AMAZONSHIP'):
                    tracking_link = "https://shiprocket.co/tracking/" + \
                        str(reverse.wbns)

                if(reverse.courier_partner == 'DTDC'):
                    tracking_link = "https://www.trackingmore.com/track/en/" + \
                        str(reverse.wbns)+str('?express=dtdc')

                courier = {
                    'courier_partner': reverse.courier_partner,
                    'tracking_id': tracking_id,
                    'tracking_link': tracking_link,
                }

            # Check Order Confirm
            buyer = db.query(UserModel).filter(
                UserModel.id == order.user_id).first()

            if(order.status == 0):

                order_confirmed = {
                    'value': 'No',
                    'text': 'Not Confirmed'
                }
            else:
                order_confirmed = {
                    'value': 'Yes',
                    'text': 'Confirmed'
                }

            order_data = {
                'id': order.id,
                'reff': 'ASEZ'+str(order.reff)+"U"+str(order.user_id),
                'order_number': order.order_number,
                'order_confirmed': order_confirmed,
                'message': order.message,
                'total_items': len(items),
                'total_amount': floatingValue(total_amount),
                'invoice': order.invoice,
                'payment_method': order.payment_method,
                'current_status': current_status,
                'courier': courier,
                'created_at': orderdate
            }

            return order_data
        except Exception as e:
            return False

    # Customize Order Items for Seller
    async def customizeOrderItems(request: Request, db: Session, data: OrdersModel) -> OrdersModel:
        try:

            # Order Details
            orderdata = db.query(OrdersModel).filter(
                OrdersModel.id == data).first()

            items = orderdata.order_items.filter(
                OrderItemsModel.status != 980).all()

            # All Status
            all_static_status = OrderStatus.AllstatusList()

            item_data = []
            for item in items:
                # Customize Item Status
                for st in all_static_status:
                    if(item.status == st['status_id']):
                        status = {
                            'status_id': item.status,
                            'status_title': st['status_title'],
                        }

                # Customize Item Message
                if(item.message == None):
                    item.message = ''

                # Customize Product Image
                filename = ''
                if(item.images != None):
                    image = db.query(ProductMediaModel).filter(
                        ProductMediaModel.id == item.images.strip("[]")).first()

                    if(image is not None):
                        filename = image.file_path

                    else:
                        image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == item.product_id).filter(
                            ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                        filename = image.file_path
                else:
                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == item.product_id).filter(
                        ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()
                    if(image is not None):
                        filename = image.file_path

                img = filename

                # Product Details
                product = db.query(ProductModel).filter(
                    ProductModel.id == item.product_id).first()

                product = {
                    'id': product.id,
                    'title': product.title,
                    'description': product.short_description,
                    'image': img,
                    'status': product.status
                }

                # Item Price
                # Aseztak Service
                pricingdata = item.uuid.split('-')
                pricingdata = db.query(ProductPricingModel).filter(
                    ProductPricingModel.id == pricingdata[1]).first()
                # Product Object
                productdata = db.query(ProductModel).filter(
                    ProductModel.id == item.product_id).first()
                # Aseztak Service
                # aseztak_service = Services.aseztak_services(
                #     item.created_at, db=db)
                today_date = item.created_at.strftime(
                    '%Y-%m-%d')
                aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

                if(aseztak_service is None):

                    # Calculate Product Price
                    price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
                                                    gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=orderdata.app_version)

                    total_amount = price * item.quantity
                else:
                    price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=orderdata.app_version, order_item_id=item.id)
                    # Calculate Product Price
                    # price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
                    #                                 gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=orderdata.app_version)

                    total_amount = price * item.quantity
                # Check Weight Box

                weight_box = True
                if(item.status != 0):
                    weight_box = False

                itm = {
                    'id': item.id,
                    'status': status,
                    'allow_process': weight_box,
                    'message': item.message,
                    'product': product,
                    'price': floatingValue(price),
                    'unit': str(pricingdata.unit),
                    'quantity': item.quantity,
                    'total': floatingValue(total_amount),
                    'attributes': item.attributes
                }

                item_data.append(itm)

            return item_data

        except Exception as e:
            print(e)

    # Customize Order Status
    async def orderStatusSeller(db: Session, data: OrdersModel) -> OrdersModel:

        staticstatuslist = []
        staticlist = OrderStatus.AllstatusList()

        order = db.query(OrdersModel).filter(OrdersModel.id == data).first()
        # Check failed Orders
        checkfailedorders = await OrderHelper.checkorderfailed(db=db, order_id=order.id)
        if(checkfailedorders['status'] != 0):
            staticstatuslist = [{
                'status_id': checkfailedorders['status'],
                'status_title': 'ORDER FAILED',
                'message': 'Order Failed',
                'visible': True,
                'created_at': order.created_at.strftime("%B %d %Y")
            }]
        else:

            current_status = db.query(OrderStatusModel).where(
                OrderStatusModel.order_id == data).order_by(desc(OrderStatusModel.id)).first()

            # Ordered To Delivered
            if(current_status.status < 80 and current_status.status != 61 and current_status.status != 980):

                for status in staticlist:
                    if(status['status_id'] < 80 and status['status_id'] != 61 and status['status_id'] != 30 and status['status_id'] != 50):
                        statuslist = db.query(OrderStatusModel).filter(
                            OrderStatusModel.order_id == data).filter(OrderStatusModel.status == status['status_id']).first()

                        visible = False
                        created_at = ''
                        if(statuslist):
                            visible = True
                            created_at = statuslist.created_at.strftime(
                                "%B %d %Y")
                        arr = {
                            'status_id': status['status_id'],
                            'status_title': status['status_title'],
                            'message': status['message'],
                            'visible': visible,
                            'created_at': created_at
                        }
                        staticstatuslist.append(arr)

            # Cancelled Status
            if(current_status.status == 980):
                arr = [{
                    'status_id': 0,
                    'status_title': 'Ordered',
                    'message': 'Order has been placed',
                    'visible': True,
                    'created_at': current_status.created_at.strftime("%B %d %Y")
                },
                    {
                    'status_id': 980,
                    'status_title': 'Cancelled',
                    'message': 'Order has been Cancelled',
                    'visible': True,
                    'created_at': current_status.created_at.strftime("%B %d %Y")
                },
                ]
                staticstatuslist = arr

            if(current_status.status == 80 or current_status.status == 81):
                # Order to Delivered if status is Return Initiated
                staticstatuslist = [{
                    'status_id': 0,
                    'status_title': 'Ordered',
                    'message': 'Order has been placed',
                    'visible': True,
                    'created_at': current_status.created_at.strftime("%B %d %Y")
                }]
                for status in staticlist:
                    if(status['status_id'] <= 70 and status['status_id'] != 30 and status['status_id'] != 50 and status['status_id'] != 61):
                        statuslist = db.query(OrderStatusModel).where(
                            OrderStatusModel.order_id == data).where(OrderStatusModel.status == status['status_id']).first()

                        visible = False
                        created_at = ''
                        if(statuslist):
                            visible = True
                            created_at = statuslist.created_at.strftime(
                                "%B %d %Y")
                        arr = {
                            'status_id': status['status_id'],
                            'status_title': status['status_title'],
                            'message': status['message'],
                            'visible': visible,
                            'created_at': created_at
                        }
                        staticstatuslist.append(arr)

            # Return Approved
            if(current_status.status > 81 and current_status.status < 980):
                # Order to Rteurn Declined
                staticstatuslist = [{
                    'status_id': 0,
                    'status_title': 'Ordered',
                    'message': 'Order has been placed',
                    'visible': True,
                    'created_at': current_status.created_at.strftime("%B %d %Y")
                }]

                for status in staticlist:
                    if(status['status_id'] >= 70 and status['status_id'] != 80 and status['status_id'] != 81 and status['status_id'] < 980):
                        statuslist = db.query(OrderStatusModel).where(
                            OrderStatusModel.order_id == data).where(OrderStatusModel.status == status['status_id']).first()

                        visible = False
                        created_at = ''
                        if(statuslist):
                            visible = True
                            created_at = statuslist.created_at.strftime(
                                "%B %d %Y")

                        status_id = status['status_id']
                        status_title = status['status_title']
                        message = status['message']
                        if(status['status_id'] == 90):
                            status_title = 'Return Initiated'
                            message = 'Return Initiated'
                        arr = {
                            'status_id': status_id,
                            'status_title': status_title,
                            'message': message,
                            'visible': visible,
                            'created_at': created_at
                        }
                        staticstatuslist.append(arr)
        return staticstatuslist

    # Invoice List

    async def InvoiceList(db: Session, user_id: int, from_date: str = '', to_date: str = ''):
        try:
            if(from_date != ''):
                orders = db.query(OrderItemsModel).join(ProductModel, ProductModel.id ==
                                                        OrderItemsModel.product_id).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).filter(ProductModel.userid == user_id).filter(OrderShippingModel.order_id == OrdersModel.id).filter(OrderItemsModel.weight != 0.00).filter(OrderStatusModel.status >= 10).filter(OrdersModel.invoice_date.between(from_date, to_date)).group_by(OrderItemsModel.order_id).order_by(OrderStatusModel.id.desc())
            else:
                orders = db.query(OrderItemsModel).join(ProductModel, ProductModel.id ==
                                                        OrderItemsModel.product_id).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).filter(ProductModel.userid == user_id).filter(OrderShippingModel.order_id == OrdersModel.id).filter(OrderItemsModel.weight != 0.00).filter(OrderStatusModel.status >= 10).group_by(OrderItemsModel.order_id).order_by(OrderStatusModel.id.desc())

            return orders

        except Exception as e:
            print(e)

    # Customize Order Details for Admin

    async def customizeOrderData(db: Session, data):
        try:
            for order in data:
                created_at = order.created_at.strftime("%B %d %Y")
                buyer = db.query(UserModel).filter(
                    UserModel.id == order.user_id).first()

                if(buyer.confirm == 'No'):
                    confirm = 'Not Confirmed'
                else:
                    confirm = 'Confirmed'

                # Seller
                seller = db.query(OrderItemsModel, ProductModel).filter(
                    OrderItemsModel.order_id == order.id).first()
                if(seller is not None):

                    seller = db.query(UserModel).filter(
                        UserModel.id == seller.ProductModel.userid).first()
                    seller = seller.name
                else:
                    seller = ''

                # Current Status
                all_static_status = OrderStatus.AllstatusList()

                latestStatus = db.query(OrderStatusModel).where(OrderStatusModel.order_id == order.id).order_by(
                    desc(OrderStatusModel.id)).first()

                statustitle = ''
                for s_title in all_static_status:
                    if(latestStatus.status == s_title['status_id']):
                        statustitle = s_title['status_title']

                order.current_status = {
                    'status': statustitle,
                    'created_at': latestStatus.created_at.strftime("%B %d %Y")
                }
                order.buyer = {
                    'name': buyer.name,
                    'confirm': confirm
                }
                order.seller = seller
                order.created_at = created_at

            return data

        except Exception as e:
            print(e)

    # Get Ordder Commission List Seller Wise

    async def getOrderCommissionList(user_id: int, db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            items = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
                ProductModel, ProductModel.id == OrderItemsModel.product_id).join(CategoryReturnDaysModel, CategoryReturnDaysModel.category_id == ProductModel.category).join(OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff.is_(None)).filter(ProductModel.userid == user_id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1), (OrderItemsModel.return_expiry == 0, OrderItemsModel.return_expiry + 2)])) < today).order_by(OrderItemsModel.id.desc()).all()

            return items

        except Exception as e:
            print(e)

    async def getOrderCommissionListRajdhaniGarments(user_id: int, db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            items = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
                ProductModel, ProductModel.id == OrderItemsModel.product_id).join(CategoryReturnDaysModel, CategoryReturnDaysModel.category_id == ProductModel.category).join(OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff.is_(None)).filter(ProductModel.userid == user_id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(func.date_format(OrdersModel.created_at,  "%Y-%m-%d") >= '2022-01-01').filter(func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1), (OrderItemsModel.return_expiry == 0, OrderItemsModel.return_expiry + 2)])) < today).order_by(OrderItemsModel.id.desc()).all()

            return items

        except Exception as e:
            print(e)

    async def getOrderCommissionListRajdhani(user_id: int, db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            items = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
                ProductModel, ProductModel.id == OrderItemsModel.product_id).join(CategoryReturnDaysModel, CategoryReturnDaysModel.category_id == ProductModel.category).join(OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff.is_(None)).filter(ProductModel.userid == user_id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1), (OrderItemsModel.return_expiry == 0, OrderItemsModel.return_expiry + 2)])) < today).filter(func.date_format(OrdersModel.created_at,  "%Y-%m-%d") >= '2022-01-01').order_by(OrderItemsModel.id.desc()).all()

            return items

        except Exception as e:
            print(e)

    # Get Generated Order Commission List Seller Wise
    async def getGeneratedOrderCommissionList(user_id: int, db: Session):
        try:

            payments = db.query(SellerPaymentModel).filter(SellerPaymentModel.seller_id == user_id).filter(
                SellerPaymentModel.txn_id.is_(None)).order_by(SellerPaymentModel.id.desc())

            return payments
        except Exception as e:
            print(e)

     # Get Generated Order Commission List Seller Wise
    async def getGeneratedOrderCommissionPaidList(user_id: int, db: Session):
        try:

            payments = db.query(SellerPaymentModel).filter(SellerPaymentModel.seller_id == user_id).filter(
                SellerPaymentModel.txn_id.isnot(None)).order_by(SellerPaymentModel.id.desc())

            return payments
        except Exception as e:
            print(e)
    # Get Generated Order Item List of Commission Seller Wise

    async def getGeneratedOrderItemsList(reff_no: str, db: Session):
        try:
            orderitems = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(
                OrdersModel.commission_reff == reff_no).order_by(OrderItemsModel.id.desc())
            return orderitems

        except Exception as e:
            print(e)

    # Get Commission Seller List

    async def getCommissionSellerList(db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            sellers = db.query(OrderItemsModel, ProductModel.userid).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
                ProductModel, ProductModel.id == OrderItemsModel.product_id).join(CategoryReturnDaysModel, CategoryReturnDaysModel.category_id == ProductModel.category).join(OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff.is_(None)).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1), (OrderItemsModel.return_expiry == 0, OrderItemsModel.return_expiry + 2)])) < today).filter(ProductModel.userid != 65).order_by(OrderItemsModel.id.desc())

            return sellers
        except Exception as e:
            print(e)

    # Get Commission List Rajdhani Seller
    async def getCommissionSellerListRajdhani(db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            sellers = db.query(OrderItemsModel, ProductModel.userid).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
                ProductModel, ProductModel.id == OrderItemsModel.product_id).join(CategoryReturnDaysModel, CategoryReturnDaysModel.category_id == ProductModel.category).join(OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff.is_(None)).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1), (OrderItemsModel.return_expiry == 0, OrderItemsModel.return_expiry + 2)])) < today).filter(func.date_format(OrdersModel.created_at,  "%Y-%m-%d") >= '2022-01-01').filter(ProductModel.userid == 65).order_by(OrderItemsModel.id.desc())

            return sellers
        except Exception as e:
            print(e)

    # New by Rahul
    async def getCommissionSellerListwithReturnDays(db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            sellers = db.query(OrderItemsModel.order_id).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
                ProductModel, ProductModel.id == OrderItemsModel.product_id).join(CategoryReturnDaysModel, CategoryReturnDaysModel.category_id == ProductModel.category).join(OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff.is_(None)).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1)])) < today).filter(ProductModel.userid != 65).group_by(OrderItemsModel.order_id).order_by(OrderItemsModel.id.desc())

            return sellers
        except Exception as e:
            print(e)

    # New by Rahul
    async def getCommissionSellerListwithZeroReturn(db: Session, seller_id: int):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            sellers = db.execute("SELECT order_items.order_id FROM `order_items` LEFT JOIN orders ON orders.id = order_items.order_id LEFT JOIN products ON products.id = order_items.product_id LEFT JOIN order_status ON order_status.order_id = order_items.order_id WHERE products.userid =:param AND orders.commission_reff is NULL AND order_items.status >= 70 AND order_items.status != 80 AND order_items.status != 90 AND order_items.status != 100 AND order_items.status != 110 AND order_items.status != 980 AND order_status.status >= 70 AND ADDDATE(date(order_status.created_at), INTERVAL 2 DAY) <= :param1 GROUP BY order_items.order_id HAVING max(order_items.return_expiry) = 0", {
                "param": seller_id, "param1": today
            }).all()

            return sellers
        except Exception as e:
            print(e)

    # New by Rahul
    async def getCommissionSellerListwithReturnDaysRajdhani(db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            sellers = db.query(OrderItemsModel.order_id).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
                ProductModel, ProductModel.id == OrderItemsModel.product_id).join(CategoryReturnDaysModel, CategoryReturnDaysModel.category_id == ProductModel.category).join(OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff.is_(None)).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1)])) < today).filter(func.date_format(OrdersModel.created_at,  "%Y-%m-%d") >= '2022-01-01').filter(ProductModel.userid == 65).group_by(OrderItemsModel.order_id).order_by(OrderItemsModel.id.desc())

            return sellers
        except Exception as e:
            print(e)

    # New by Rahul
    async def getCommissionSellerListwithZeroReturnRajdhani(db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            sellers = db.execute("SELECT order_items.order_id FROM `order_items` LEFT JOIN orders ON orders.id = order_items.order_id LEFT JOIN products ON products.id = order_items.product_id LEFT JOIN order_status ON order_status.order_id = order_items.order_id WHERE products.userid =:param AND orders.commission_reff is NULL AND order_items.status >= 70 AND order_items.status != 80 AND order_items.status != 90 AND order_items.status != 100 AND order_items.status != 110 AND order_items.status != 980 AND order_status.status >= 70 AND ADDDATE(date(order_status.created_at), INTERVAL 2 DAY) <= :param1 AND date(orders.created_at) >= '2022-01-01' GROUP BY order_items.order_id HAVING max(order_items.return_expiry) = 0", {
                "param": 65, "param1": today
            }).all()

            return sellers
        except Exception as e:
            print(e)

    # Test

    async def getOrderCommissionListTest(user_id: int, db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            items = db.query(OrderItemsModel).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
                ProductModel, ProductModel.id == OrderItemsModel.product_id).join(CategoryReturnDaysModel, CategoryReturnDaysModel.category_id == ProductModel.category).join(OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff.is_(None)).filter(ProductModel.userid == user_id).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1), (OrderItemsModel.return_expiry == 0, OrderItemsModel.return_expiry + 2)])) < today).group_by(OrderItemsModel.order_id).order_by(OrderItemsModel.id.desc()).all()

            return items

        except Exception as e:
            print(e)

    async def getCommissionSellerListTest(db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            sellers = db.query(OrderItemsModel, ProductModel.userid).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
                ProductModel, ProductModel.id == OrderItemsModel.product_id).join(CategoryReturnDaysModel, CategoryReturnDaysModel.category_id == ProductModel.category).join(OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff.is_(None)).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1), (OrderItemsModel.return_expiry == 0, OrderItemsModel.return_expiry + 2)])) < today).filter(ProductModel.userid != 65).order_by(OrderItemsModel.id.desc())

            return sellers
        except Exception as e:
            print(e)

    # Get Commission RAJDHANI Seller List

    async def getCommissionSellerRajdhaniList(db: Session):
        try:
            today = datetime.now()
            today = today.strftime("%Y-%m-%d")

            sellers = db.query(OrderItemsModel, ProductModel.userid).join(OrdersModel, OrdersModel.id == OrderItemsModel.order_id).join(
                ProductModel, ProductModel.id == OrderItemsModel.product_id).join(CategoryReturnDaysModel, CategoryReturnDaysModel.category_id == ProductModel.category).join(OrderStatusModel, OrderStatusModel.order_id == OrderItemsModel.order_id).filter(OrdersModel.commission_reff.is_(None)).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderStatusModel.status >= 70).filter(func.ADDDATE(func.date_format(OrderStatusModel.created_at, "%Y-%m-%d"),  case([(OrderItemsModel.return_expiry != 0, OrderItemsModel.return_expiry - 1), (OrderItemsModel.return_expiry == 0, OrderItemsModel.return_expiry + 2)])) < today).filter(func.date_format(OrdersModel.created_at,  "%Y-%m-%d") >= '2022-01-01').filter(ProductModel.userid == 65).order_by(OrderItemsModel.id.desc())

            return sellers
        except Exception as e:
            print(e)

    # All Generated Order Commission List
    async def getAllGeneratedOrderCommissionList(db: Session):
        try:
            payments = db.query(SellerPaymentModel).filter(
                SellerPaymentModel.txn_id.is_(None)).order_by(SellerPaymentModel.id.desc())

            return payments
        except Exception as e:
            print(e)

    # Commission Invoice List
    async def OrderCommissionInvoiceList(db: Session):
        try:
            payments = db.query(SellerPaymentModel).filter(
                SellerPaymentModel.txn_id.is_not(None)).order_by(SellerPaymentModel.id.desc())

            return payments
        except Exception as e:
            print(e)

    # Search COMMISSION Invoice List

    async def SearchOrderCommissionInvoiceList(search: str, from_date: str, to_date: str, db: Session):
        try:
            if(search != 'search' and from_date != 'from_date' and to_date != 'to_date'):

                payments = db.query(SellerPaymentModel).filter(
                    SellerPaymentModel.txn_id.is_not(None)).filter(SellerPaymentModel.reff_no.ilike(search)).filter(func.date_format(
                        SellerPaymentModel.commission_date, '%Y-%m-%d') >= from_date).filter(func.date_format(
                            SellerPaymentModel.commission_date, '%Y-%m-%d') <= to_date).order_by(SellerPaymentModel.id.desc())

                if(payments.count() == 0):
                    payments = db.query(SellerPaymentModel).filter(
                        SellerPaymentModel.txn_id.is_not(None)).filter(SellerPaymentModel.txn_id.ilike(search)).filter(func.date_format(
                            SellerPaymentModel.commission_date, '%Y-%m-%d') >= from_date).filter(func.date_format(
                                SellerPaymentModel.commission_date, '%Y-%m-%d') <= to_date).order_by(SellerPaymentModel.id.desc())

                if(payments.count() == 0):
                    payments = db.query(SellerPaymentModel).join(UserModel, UserModel.id == SellerPaymentModel.seller_id).filter(
                        SellerPaymentModel.txn_id.is_not(None)).filter(UserModel.name.ilike(search)).filter(func.date_format(
                            SellerPaymentModel.commission_date, '%Y-%m-%d') >= from_date).filter(func.date_format(
                                SellerPaymentModel.commission_date, '%Y-%m-%d') <= to_date).order_by(SellerPaymentModel.id.desc())

                if(payments.count() == 0):
                    payments = db.query(SellerPaymentModel).join(UserModel, UserModel.id == SellerPaymentModel.seller_id).filter(
                        SellerPaymentModel.txn_id.is_not(None)).filter(UserModel.mobile.ilike(search)).filter(func.date_format(
                            SellerPaymentModel.commission_date, '%Y-%m-%d') >= from_date).filter(func.date_format(
                                SellerPaymentModel.commission_date, '%Y-%m-%d') <= to_date).order_by(SellerPaymentModel.id.desc())

                if(payments.count() == 0):
                    payments = db.query(SellerPaymentModel).join(UserModel, UserModel.id == SellerPaymentModel.seller_id).filter(
                        SellerPaymentModel.txn_id.is_not(None)).filter(UserModel.name.ilike(search)).filter(func.date_format(
                            SellerPaymentModel.commission_date, '%Y-%m-%d') >= from_date).filter(func.date_format(
                                SellerPaymentModel.commission_date, '%Y-%m-%d') <= to_date).order_by(SellerPaymentModel.id.desc())

            elif(search != 'search' and from_date == 'from_date' and to_date == 'to_date'):
                payments = db.query(SellerPaymentModel).filter(
                    SellerPaymentModel.txn_id.is_not(None)).filter(SellerPaymentModel.reff_no.ilike(search)).order_by(SellerPaymentModel.id.desc())

                if(payments.count() == 0):
                    payments = db.query(SellerPaymentModel).filter(
                        SellerPaymentModel.txn_id.is_not(None)).filter(SellerPaymentModel.txn_id.ilike(search)).order_by(SellerPaymentModel.id.desc())

                if(payments.count() == 0):
                    payments = db.query(SellerPaymentModel).join(UserModel, UserModel.id == SellerPaymentModel.seller_id).filter(
                        SellerPaymentModel.txn_id.is_not(None)).filter(UserModel.name.ilike(search)).order_by(SellerPaymentModel.id.desc())

                if(payments.count() == 0):
                    payments = db.query(SellerPaymentModel).join(UserModel, UserModel.id == SellerPaymentModel.seller_id).filter(
                        SellerPaymentModel.txn_id.is_not(None)).filter(UserModel.mobile.ilike(search)).order_by(SellerPaymentModel.id.desc())

                if(payments.count() == 0):
                    payments = db.query(SellerPaymentModel).join(UserModel, UserModel.id == SellerPaymentModel.seller_id).filter(
                        SellerPaymentModel.txn_id.is_not(None)).filter(UserModel.name.ilike(search)).order_by(SellerPaymentModel.id.desc())

            else:
                payments = db.query(SellerPaymentModel).filter(
                    SellerPaymentModel.txn_id.is_not(None)).filter(func.date_format(
                        SellerPaymentModel.commission_date, '%Y-%m-%d') >= from_date).filter(func.date_format(
                            SellerPaymentModel.commission_date, '%Y-%m-%d') <= to_date).order_by(SellerPaymentModel.id.desc())

            return payments
        except Exception as e:
            print(e)

    # Get all seller list
    async def getAllSellerCommission(db: Session):
        try:

            return db.query(SellerPaymentModel)
        except Exception as e:
            print(e)

        # All Generated Order Commission List

    async def getAllGeneratedOrderCommissionListSellerWise(db: Session, seller_id: int):
        try:
            payments = db.query(SellerPaymentModel).filter(
                SellerPaymentModel.seller_id == seller_id).order_by(SellerPaymentModel.id.desc())

            return payments
        except Exception as e:
            print(e)

    # Commission Invoice List
    async def OrderCommissionInvoiceListSellerWise(db: Session, seller_id: int):
        try:
            payments = db.query(SellerPaymentModel).filter(SellerPaymentModel.seller_id == seller_id).filter(
                SellerPaymentModel.txn_id.is_not(None)).order_by(SellerPaymentModel.id.desc())

            return payments
        except Exception as e:
            print(e)

    async def getAllBuyersOrdersAdmin(db: Session, user_id: int, status: str, page: int, limit: int):
        try:

            if(status == 'all'):
                return db.query(OrdersModel).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

            if(status == '0'):
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(OrdersModel.user_id == user_id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 0).order_by(OrdersModel.id.desc())

            if(status == '10'):
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 10).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

            if(status == '30'):
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 30).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

            if(status == '40'):
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 40).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

            if(status == '60'):
                return db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).group_by(OrderStatusModel.order_id).having(func.max(OrderStatusModel.status) == 60).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

            if(status == '70'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status.between('70', '81')).filter(OrderItemsModel.status != 80).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

            if(status == '80'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 80).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

            if(status == '61'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 61).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

            if(status == '100'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status == 100).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

            if(status == '110'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status == 110).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

            if(status == '980'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).group_by(OrderItemsModel.order_id).filter(OrderItemsModel.status == 980).filter(OrdersModel.user_id == user_id).order_by(OrdersModel.id.desc())

        except Exception as e:
            print(e)

    async def getAllSellersOrdersAdmin(db: Session, user_id: int, status: str, page: int, limit: int):
        try:

            if(status == 'all'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '0'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).having(func.max(OrderStatusModel.status) == 0).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '10'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).having(func.max(OrderStatusModel.status) == 10).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '30'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).having(func.max(OrderStatusModel.status) == 30).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '40'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).having(func.max(OrderStatusModel.status) == 40).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '60'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).having(func.max(OrderStatusModel.status) == 60).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '70'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).having(func.max(OrderStatusModel.status) == 70).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '80'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).having(func.max(OrderItemsModel.status) == 80).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '81'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).having(func.max(OrderItemsModel.status) == 81).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '61'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).having(func.max(OrderStatusModel.status) == 61).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '90'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrderItemsModel.status == 90).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '100'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrderItemsModel.status == 100).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '110'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrderItemsModel.status == 110).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

            if(status == '980'):
                return db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(
                    ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(ProductModel.userid == user_id).filter(OrderItemsModel.status == 980).group_by(OrdersModel.id).order_by(OrdersModel.id.desc())

        except Exception as e:
            print(e)


# COUNT Orders Product Wise(Tina 2022-04-27)


    async def countOrdersProductWiseAdmin(db: Session, status: str, product_id: int):
        try:
            if(status == 'all'):
                return db.query(OrderItemsModel).filter(
                    OrderItemsModel.product_id == product_id).group_by(OrderItemsModel.order_id)
            if(status == '70'):
                return db.query(OrderItemsModel).filter(OrderItemsModel.status >= 70).filter(OrderItemsModel.status != 80).filter(OrderItemsModel.status != 90).filter(OrderItemsModel.status != 91).filter(
                    OrderItemsModel.status != 100).filter(OrderItemsModel.status != 110).filter(OrderItemsModel.status != 980).filter(OrderItemsModel.product_id == product_id).group_by(OrderItemsModel.order_id)
            if(status == '90'):
                return db.query(OrderItemsModel).filter(OrderItemsModel.product_id == product_id).filter(
                    OrderItemsModel.status >= 90).filter(OrderItemsModel.status != 91).filter(OrderItemsModel.status != 980).group_by(OrderItemsModel.order_id)
            if(status == '980'):
                return db.query(OrderItemsModel).filter(OrderItemsModel.product_id == product_id).filter(
                    OrderItemsModel.status == 980).group_by(OrderItemsModel.order_id)
        except Exception as e:
            print(e)

    # Get Cancelled Orders
    async def getAllCancelledOrdersforAdmin(db: Session, status: str, type: str):
        try:
            if(status == '980' and type == 'Yes'):
                return db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrdersModel.round_off, OrderItemsModel.order_id, OrderItemsModel.status, OrderItemsModel.refund).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.cancelled_by_seller == type).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.updated_at.desc())
            elif(status == '980' and type == 'No'):
                return db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrdersModel.round_off, OrderItemsModel.order_id, OrderItemsModel.status, OrderItemsModel.refund).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrderItemsModel.cancelled_by_seller == type).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.updated_at.desc())
            elif(status == '980' and type == 'online'):
                return db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrdersModel.round_off, OrderItemsModel.order_id, OrderItemsModel.status, OrderItemsModel.refund).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrdersModel.payment_method == 'ONLINE').filter(OrderItemsModel.refund == 0).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.updated_at.desc())
            elif(status == '980' and type == 'partial'):
                return db.query(OrdersModel.id, OrdersModel.user_id, OrdersModel.order_number, OrdersModel.delivery_charge, OrdersModel.discount, OrdersModel.discount_rate, OrdersModel.created_at, OrdersModel.payment_method, OrdersModel.app_version, OrdersModel.grand_total, OrdersModel.round_off, OrderItemsModel.order_id, OrderItemsModel.status, OrderItemsModel.refund).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).filter(OrdersModel.payment_method == 'PARTIAL').filter(OrderItemsModel.refund == 0).group_by(OrderItemsModel.order_id).having(func.max(OrderItemsModel.status) == 980).order_by(OrdersModel.updated_at.desc())
        except Exception as e:
            print(e)


# Check Order Failed

    async def checkorderfailed(db: Session, order_id: int):
        try:
            # order = db.query(OrdersModel).filter(
            #     OrdersModel.id == order_id).first()
            # checkorder = order.order_failed.first()
            checkorder = db.query(OrderFailsModel).filter(
                OrderFailsModel.order_id == order_id).first()
            if(checkorder is not None):
                # check_payment = checkorder.payment_failed.first()
                check_payment = db.query(PaymentFailedModel).filter(
                    PaymentFailedModel.order_fail_id == checkorder.id).first()
                if(check_payment is not None):
                    order_failed = {
                        'status': 980,
                        'txn_id': check_payment.txn_order_id,
                        'reason': check_payment.reason
                    }
                else:
                    order_failed = {
                        'status': 980,
                        'txn_id': '',
                        'reason': ''
                    }
            else:
                order_failed = {
                    'status': 0,
                    'txn_id': '',
                    'reason': ''
                }
            return order_failed
        except Exception as e:
            print(e)
            return {'status': 0, 'txn_id': '', 'reason': ''}
