
from datetime import datetime, date, timedelta
from app.db.models.user import *
from app.db.schemas.auth_schema import RegisterSchema
from sqlalchemy.orm import Session
import random
import math
from app.resources.strings import *
import requests
from starlette.status import HTTP_200_OK

from app.db.models.user_facility import UserFacilityModel
from app.db.models.marketing_users import MarketerUserModel
from app.db.models.wallet import WalletModel
from app.core.config import COIN_EXPIRING_DAYS

#Send OTP WhatsApp
async def sendotpwhatsapp(mobile: str, otp: str):
    try:
        payload = {
            "apiKey": WP_API_KEY,
            "campaignName": "SEND_ONETIME_MSZ",
            "destination": str('+91')+str(mobile),
            "source": "",
            "media": {
                "url": "",
                "filename": ""
            },
            "templateParams": [
               str(otp)
                ],
                "tags": [

                ],
                "attributes": {
                    "attribute_name": ""
                }
            }
        headers = {
                "Content-Type": "application/json",
                "Accept": "application/json"
        }

        response = requests.request(
            "POST", 'https://backend.aisensy.com/campaign/t1/api', json=payload, headers=headers)
        return response.text
    except Exception as e:
        print(e)


#Send OTP
def sendotp(otp: str, mobile: str, signature: str):
    try:
        signature = signature
        mobile = str(mobile)
        extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
            AUTHKEY+'&template_id='+TEMPLATE_ID + \
                '&mobile='+str(91)+mobile+'&otp='+str(otp)

        sendurl = OTP_SEND_URL+extra_param
        requests.get(sendurl)
    except Exception as e:
        print(e)

#Create New User
async def create_user(data: RegisterSchema, otp: str, db: Session):
    try:
        #insert user
        if(data.email == ''):
            email = str(data.mobile)+'@aseztak.com'
        else:
            email = data.email
        db_user = UserModel(
            name=data.name,
            mobile=data.mobile,
            otp=otp,
            country=data.country,
            region=data.region,
            city=data.city,
            pincode=data.pincode,
            email=email,
            status=1,
            app_version='V4',
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(db_user)
        db.flush()

        #Insert user role
        db_role = UserRoleModel(
            model_id=db_user.id,
            role_id=5
        )
        db.add(db_role)


        #Insert Proof in Profile table
        db_profile = UserProfileModel(
            user_id=db_user.id,
            proof=data.proof,
            proof_type=data.proof_type
        )
        db.add(db_profile)

        #Insert user facility
        db_facility = UserFacilityModel(
            user_id=db_user.id,
            key_name='cod',
            key_value=10000,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(db_facility)

        # Insert Wallet Balance
        todaydate = date.today()
        td = timedelta(days=COIN_EXPIRING_DAYS)
        valid_upto = todaydate + td
        valid_upto = valid_upto.strftime("%Y-%m-%d")
        user_wallet = WalletModel(
            user_id=db_user.id,
            reff_id=db_user.id,
            reff_type='REGISTRATION_REWARDS',
            description='On new registration',
            type='CR',
            amount=500,
            status=1,
            valid_upto=valid_upto,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(user_wallet)
        db.commit()
        db.refresh(db_user)
        return
    except Exception as e:
        print(e)

#Impliment Rate Limiter (Subha)
# 30 calls per minute
# CALLS = 1
# RATE_LIMIT = 10        

# @sleep_and_retry
# @limits(calls=CALLS, period=RATE_LIMIT)

async def insert_bot_data(db: Session, user_agent: str, mobile: str):
    try:
        db.execute("INSERT INTO test (name, mobile) VALUES(:user_agent, :mobilenumber)",{
                "user_agent": user_agent,
                "mobilenumber": mobile
            })
        db.commit()
    except Exception as e:
        print(e)    

def create_new_user(db: Session, user: RegisterSchema):
    # already exist
    exist = db.query(UserModel).filter(UserModel.mobile == user.mobile).first()
    otp = random.randint(746008, 999999)
    if(exist):
        if(exist.role.role_id == 6):
            data = {
                'status_code': HTTP_200_OK,
                'is_user': False,
                'is_register': True,
                'message': 'Sorry! your account has been registered as a seller',
                'signature': '',
                'otp_sent_at': datetime.now(),
                'otp_expires': 60
            }

            return data
        if(exist.status == 90):
            data = {
                'status_code': HTTP_200_OK,
                'is_user': False,
                'is_register': True,
                'message': 'Your account has been suspended',
                'signature': '',
                'otp_sent_at': datetime.now(),
                'otp_expires': 60
            }

            return data
        else:
            signature = user.signature
            mobile = str(user.mobile)
            if(mobile != '9898989898'):
                exist.otp = otp
                db.flush()
                db.commit()
                # send OTP Message
                extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
                    AUTHKEY+'&template_id='+TEMPLATE_ID + \
                    '&mobile='+str(91)+mobile+'&otp='+str(otp)
                sendurl = OTP_SEND_URL+extra_param
                requests.get(sendurl)

            data = {
                'status_code': HTTP_200_OK,
                'is_user': True,
                'is_register': True,
                'message': 'Otp sent to your mobile',
                'signature': signature,
                'otp_sent_at': datetime.now(),
                'otp_expires': 60
            }

            return data

    try:

        db_user = UserModel(
            mobile=user.mobile,
            status=0,
            otp=otp,
            app_version='V4',
            created_at=datetime.now(),
            updated_at=datetime.now()
        )


        db.add(db_user)
        db.commit()

        # User Limit
        user_limit = UserFacilityModel(
            user_id=db_user.id,
            key_name='cod',
            key_value=10000,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(user_limit)
        db.commit()

        # Insert Wallet Balance
        todaydate = date.today()
        td = timedelta(days=COIN_EXPIRING_DAYS)
        valid_upto = todaydate + td
        valid_upto = valid_upto.strftime("%Y-%m-%d")
        user_wallet = WalletModel(
            user_id=db_user.id,
            reff_id=db_user.id,
            reff_type='REGISTRATION_REWARDS',
            description='On new registration',
            type='CR',
            amount=500,
            status=1,
            valid_upto=valid_upto,
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.add(user_wallet)
        db.commit()
        # create Role
        db_user_role = UserRoleModel(
            role_id=5,
            model_id=db_user.id
        )

        db.add(db_user_role)
        db.commit()

        db.refresh(db_user)

        # send OTP Message
        signature = user.signature
        mobile = str(user.mobile)
        extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
            AUTHKEY+'&template_id='+TEMPLATE_ID + \
            '&mobile='+str(91)+mobile+'&otp='+str(otp)

        sendurl = OTP_SEND_URL+extra_param
        requests.get(sendurl)

        data = {
            'status_code': HTTP_200_OK,
            'is_user': True,
            'message': 'Otp sent to your mobile',
            'signature': signature,
            'otp_sent_at': datetime.now(),
            'otp_expires': 60
        }

        return data

    except Exception as e:
        return {
            'status_code': HTTP_200_OK,
            'is_user': False,
            'message': 'Sorry! your account has been registered as a seller',
            'signature': '',
            'otp_sent_at': datetime.now(),
            'otp_expires': 60
        }


# Create Seller


def create_seller(db: Session, user: RegisterSchema):

    # already exist
    exist = db.query(UserModel).filter(UserModel.mobile == user.mobile).first()
    if(exist):
        return {'status_code': HTTP_200_OK, 'message': "Already Exist", 'is_user': False}

    otp = random.randint(746008, 999999)
    try:

        if(user.email == ''):
            email = str(user.mobile)+'@aseztak.com'
        else:
            email = user.email

        db_user = UserModel(
            name=user.name,
            mobile=user.mobile,
            email=email,
            otp=otp,
            status=0,
            country=user.country,
            region=user.region,
            city=user.city,
            pincode=user.pincode,
            app_version='V4',
            created_at=datetime.now(),
            updated_at=datetime.now()
        )

        db.add(db_user)
        db.commit()

        # create Role
        db_user_role = UserRoleModel(
            role_id=6,
            model_id=db_user.id
        )

        db.add(db_user_role)
        db.commit()

        # Profile
        db_user_profile = UserProfileModel(
            user_id=db_user.id,
            proof=user.proof,
            proof_type=user.proof_type
        )
        db.add(db_user_profile)
        db.commit()

        db.refresh(db_user)

        # send OTP Message
        signature = user.signature
        mobile = str(user.mobile)
        extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
            AUTHKEY+'&template_id='+TEMPLATE_ID + \
            '&mobile='+str(91)+mobile+'&otp='+str(otp)

        sendurl = OTP_SEND_URL+extra_param
        requests.get(sendurl)

        data = {
            'status_code': HTTP_200_OK,
            'is_user': True,
            'message': "Registered Successfully",
            'data': db_user,
            'signature': signature,
            'otp_sent_at': datetime.now(),
            'otp_expires': 60
        }

        return data

    except Exception as e:
        print(e)


# Update Otp User
async def userOtpUpdate(db: Session, mobile: str, otp: str):
    try:
        user = db.query(UserModel).where(UserModel.mobile == mobile).first()
        if(user):

            if(user.app_version == 'V3'):
                app_version = 'V4'
            elif(user.app_version is None):
                app_version = 'V4'
            else:
                app_version = 'V4'

            user.otp = otp
            user.app_version = app_version
            db.flush()
            db.commit()
        else:

            user = UserModel(
                mobile=mobile, otp=otp, status=1, app_version='V4', created_at=datetime.now(), updated_at=datetime.now())

            db.add(user)
            db.commit()

            # User Limit
            user_limit = UserFacilityModel(
                user_id=user.id,
                key_name='cod',
                key_value=10000,
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.add(user_limit)
            db.commit()

            # Update Role
            db_user_role = UserRoleModel(model_id=user.id, role_id=5,)

            db.add(db_user_role)
            db.commit()
            db.refresh(db_user_role)

        return user
    except:
        print('Error in otp updates')

# Update Otp Seller User


async def SellerOtpUpdate(db: Session, mobile: str, otp: str):
    try:
        user = db.query(UserModel).where(UserModel.mobile == mobile).first()
        if(user):
            user.otp = otp
            db.flush()
            db.commit()
        else:

            user = UserModel(
                mobile=mobile, otp=otp, status=1, app_version='V4', created_at=datetime.now(), updated_at=datetime.now())

            db.add(user)
            db.commit()

            # Update Role
            db_user_role = UserRoleModel(model_id=user.id, role_id=6,)

            db.add(db_user_role)
            db.commit()
            db.refresh(db_user_role)
        return user
    except:
        print('Error in otp updates')

# Update marketer otp


async def MarketerOtpUpdate(db: Session, mobile: str, otp: str):
    try:
        user = db.query(MarketerUserModel).filter(
            MarketerUserModel.mobile == mobile).first()
        if(user):
            user.otp = otp
            db.flush()
            db.commit()

            return user
    except:
        print('Error in otp updates')


# User login
def check_user(db: Session, data):

    user = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).filter(UserModel.mobile ==
                                                                                                                                     data.mobile).filter(UserModel.otp == data.otp).filter(UserModel.deleted_at.is_(None)).first()

    if(user):
        profile = db.query(UserProfileModel).where(
            UserProfileModel.user_id == user.id).first()
        if(profile):
            return {'is_profile': True,  'user': user, 'profile': profile}
        else:
            return {'is_profile': False, 'user': user, 'profile': {}}

    return False


# User login
def check_seller_user(db: Session, data):

    user = db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.mobile ==
                                                                                                                                     data.mobile).filter(UserModel.otp == data.otp).filter(UserModel.deleted_at.is_(None)).first()

    if(user):
        profile = db.query(UserProfileModel).where(
            UserProfileModel.user_id == user.id).first()
        if(profile):
            return {'is_profile': True,  'user': user, 'profile': profile}
        else:
            return {'is_profile': False, 'user': user, 'profile': {}}

    return False


# Generate OTP(6 digit)


def _generate_otp():
    # storing strings in a list
    digits = [i for i in range(0, 10)]

    # initializing a string
    random_str = ""

    # we can generate any lenght of string we want
    for i in range(6):
        # generating a random index
        # if we multiply with 10 it will generate a number between 0 and 10 not including 10
        # multiply the random.random() with length of your base list or str
        index = math.floor(random.random() * 10)

        random_str += str(digits[index])

    return random_str
