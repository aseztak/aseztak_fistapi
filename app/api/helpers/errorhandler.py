from starlette.requests import Request
import socket
from app.db.config import SessionLocal
from datetime import datetime
import json

class ErrorHandlerException:
    async def ErrorHandling(request: Request, exception):
        try:
            error = str(exception)
            error = json.dumps(error)
            hostname = socket.gethostname()
            ip_address = socket.gethostbyname(hostname)
            base_path = request.base_url
            base_path = str(base_path)
            base_path=base_path[:-1]
            route_path = request.url.path
            full_url = str(base_path)+str(route_path)
            # db = SessionLocal()
            # db.execute("INSERT INTO error_log_message(ip, error_message, route, created_at, updated_at) VALUES(:ip, :error_message, :route, :created_at, :updated_at)",{
            #     "ip": ip_address,
            #     "error_message": str(error),
            #     "route": full_url,
            #     "created_at": datetime.now(),
            #     "updated_at": datetime.now()
            # })
            # db.commit()
            return
        except Exception as e:
            print(e)
