class OrderStatus:

    def statusList():
        order_status = [

            {
                'status_id': 0,
                'status_title': 'Ordered',
                'message': 'Order has been placed',
                'visible': False,
            },

            {
                'status_id': 30,
                'status_title': 'Processing',
                'message': 'Preparing your Order.',
                'visible': False,

            },

            {
                'status_id': 40,
                'status_title': 'Packed',
                'message': 'Your item has been packed.',
                'visible': False,
            },

            {
                'status_id': 60,
                'status_title': 'Shipped',
                'message': 'Order has been shipped.',
                'visible': False,
            },

            {
                'status_id': 61,
                'status_title': 'Reverse',
                'message': 'Order has been reverse.',
                'visible': False,
            },

            {
                'status_id': 70,
                'status_title': 'Delivered',
                'message': 'Order has been delivered',
                'visible': False,
            },

            {
                'status_id': 80,
                'status_title': 'Return Initiated',
                'message': 'We will notify you shortly',
                'visible': False,
            },

            {
                'status_id': 81,
                'status_title': 'Return Declined',
                'message': 'Need Help ?',
                'visible': False,
            },

            {
                'status_id': 90,
                'status_title': 'Return Approved',
                'message': 'Return Processed',
                'visible': False,
            },

            {
                'status_id': 100,
                'status_title': 'Return Completed',
                'message': 'Return completed successfully.',
                'visible': False,
            },

            {
                'status_id': 110,
                'status_title': 'Refunded',
                'message': 'Refunded',
                'visible': False,
            },

            {
                'status_id': 980,
                'status_title': 'Cancelled',
                'message': 'Order has been Cancelled',
                'visible': False,
            }

        ]

        return order_status

    def AllstatusList():
        order_status = [

            {
                'status_id': 0,
                'status_title': 'Ordered',
                'message': 'Order has been placed',
                'visible': False,
            },

            {
                'status_id': 10,
                'status_title': 'Accepted',
                'message': 'Accepted By Seller',
                'visible': False,
            },

            {
                'status_id': 20,
                'status_title': 'Approved by System',
                'message': 'Order has been placed',
                'visible': False,
            },

            {
                'status_id': 30,
                'status_title': 'Processing',
                'message': 'Preparing your Order.',
                'visible': False,

            },

            {
                'status_id': 40,
                'status_title': 'Packed',
                'message': 'Your item has been packed.',
                'visible': False,
            },

            {
                'status_id': 50,
                'status_title': 'Picked up Competed',
                'message': 'Your item has been packed.',
                'visible': False,
            },

            {
                'status_id': 60,
                'status_title': 'Shipped',
                'message': 'Order has been shipped.',
                'visible': False,
            },

            {
                'status_id': 61,
                'status_title': 'Reverse',
                'message': 'Order has been reverse.',
                'visible': False,
            },

            {
                'status_id': 70,
                'status_title': 'Delivered',
                'message': 'Order has been delivered',
                'visible': False,
            },

            {
                'status_id': 80,
                'status_title': 'Return Initiated',
                'message': 'We will notify you shortly',
                'visible': False,
            },

            {
                'status_id': 81,
                'status_title': 'Return Declined',
                'message': 'Need Help ?',
                'visible': False,
            },

            {
                'status_id': 90,
                'status_title': 'Return Approved',
                'message': 'Return Processed',
                'visible': False,
            },

            {
                'status_id': 100,
                'status_title': 'Return Completed',
                'message': 'Return completed successfully.',
                'visible': False,
            },

            {
                'status_id': 110,
                'status_title': 'Refunded',
                'message': 'Refunded',
                'visible': False,
            },

            {
                'status_id': 980,
                'status_title': 'Cancelled',
                'message': 'Order has been Cancelled',
                'visible': False,
            }

        ]

        return order_status
