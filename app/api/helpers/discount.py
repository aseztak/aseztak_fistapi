from sqlalchemy.orm.session import Session
from app.db.models.orderdiscount import OrderDiscountModel
from app.db.models.seller_discount import SellerDiscountModel
from sqlalchemy import func


class Discount:

    async def checkorderdiscount(disocunt_date: str, db: Session):
        try:
            checkDiscount = db.query(OrderDiscountModel).filter(
                OrderDiscountModel.start_date <= disocunt_date).order_by(OrderDiscountModel.id.desc()).first()

            discount = 0
            if(checkDiscount is not None):
                discount = checkDiscount.discount

            return discount
        except Exception as e:
            print(e)

    # Check Seller Discount
    async def checkSellerDiscount(db: Session, seller_id: int, today_date: str, order_amount: float):
        try:
            check_discount = db.query(SellerDiscountModel).filter(SellerDiscountModel.seller_id == seller_id).filter(
                func.date_format(SellerDiscountModel.valid_upto, '%Y-%m-%d') >= today_date).order_by(SellerDiscountModel.id.desc()).first()
            if(check_discount is not None):
                if(float(order_amount) >= float(check_discount.limit_amount)):
                    seller_discount = check_discount.rate
                else:
                    seller_discount = 0
            else:
                seller_discount = 0
            return seller_discount
        except Exception as e:
            seller_discount = 0
            return seller_discount

    # Check Seller Discount for Product Thumbs

    async def checkSellerDiscountProductThumb(db: Session, seller_id: int, today_date: str):
        try:
            check_discount = db.query(SellerDiscountModel).filter(SellerDiscountModel.seller_id == seller_id).filter(
                func.date_format(SellerDiscountModel.valid_upto, '%Y-%m-%d') >= today_date).order_by(SellerDiscountModel.id.desc()).first()

            seller_discount = {
                'rate': '0',
                'amount': '0'
            }
            if(check_discount is not None):
                seller_discount = {
                    'rate': check_discount.rate,
                    'amount': check_discount.limit_amount
                }

            return seller_discount
        except Exception as e:
            seller_discount = {
                'rate': '0',
                'amount': '0'
            }
            return seller_discount
