from app.db.models.categories import *

from sqlalchemy.orm import Session
from app.db.models.products import FavouriteModel
from app.resources.strings import *

class WishlistHelper:
    async def getwishlist(db: Session, user_id: int):
        wishlist = db.query(FavouriteModel).filter(FavouriteModel.user_id == user_id).order_by(FavouriteModel.id.asc()).all()
        return wishlist
