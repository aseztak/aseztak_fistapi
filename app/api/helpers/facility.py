from sqlalchemy.orm.session import Session
from app.db.models.user import UserModel
from app.db.models.user_facility import UserFacilityModel


class Facility:

    async def buyer_facility(db: Session, user_id: int):
        try:
            check_buyer_facility = db.query(UserFacilityModel).filter(UserFacilityModel.user_id == user_id).filter(
                UserFacilityModel.key_name == 'free_delivery').order_by(UserFacilityModel.id.desc()).first()

            return check_buyer_facility

        except Exception as e:
            print(e)

    async def buyer_discount_facility(db: Session, user_id: int):
        try:
            check_buyer_discount_facility = db.query(UserFacilityModel).filter(UserFacilityModel.user_id == user_id).filter(
                UserFacilityModel.key_name == 'online_discount').order_by(UserFacilityModel.id.desc()).first()
            return check_buyer_discount_facility

        except Exception as e:
            print(e)

    async def check_buyer_cod_limit(db: Session, user_id: int):
        try:
            facility = db.query(UserFacilityModel).filter(
                UserFacilityModel.key_name == 'cod').filter(UserFacilityModel.user_id == user_id).first()

            return facility
        except Exception as e:
            print(e)
