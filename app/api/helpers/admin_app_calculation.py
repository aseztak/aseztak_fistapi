
from sqlalchemy.orm.session import Session
from app.api.util.admin_app_calculation import orderDeliveryCalculation, productPricecalculation, floatingValue, orderDiscountCalculation, roundOf
from app.api.util.admin_app_service import Services
from sqlalchemy.orm import Session
from starlette.requests import Request
from app.db.models.carts import CartModel
from pydantic.types import Json
from app.db.models.media import ProductMediaModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.orderdiscount import OrderDiscountModel
from app.db.models.orders import OrdersModel

from app.db.models.products import InventoryModel, ProductModel, ProductPricingModel
from app.api.helpers.admin_app_products import ProductsHelper
import json

from app.db.models.shippingcharge import ShippingChargeModel
from app.resources.strings import NEW_COMMISSION_DATE, NEW_COMMISSION_PERCENT, NEW_TCS_PERCENT, NEW_TDS_PERCENT, TDS_TCS_PERCENT_DATE
# Product Price Calculation
from app.api.helpers.services import AsezServices
from app.api.helpers.products import ProductsHelper


class Calculations:
 # Calculating Product Price with Tax
    async def productPrice(db: Session, data: ProductModel) -> ProductModel:

        if(type(data) in (tuple, list)):

            for i in data:

                # Check Pricing
                pricing = db.query(ProductPricingModel).where(ProductPricingModel.product_id == i.id).where(
                    ProductPricingModel.deleted_at.is_(None)).first()
                if(pricing):
                    # Calculate price with
                    total = (pricing.price *
                             pricing.tax / 100) + pricing.price
                # Restructure Price data
                i.pricing.price = round(total, 2)
                i.pricing.id = pricing.id
                i.pricing.moq = pricing.moq

            return data

        else:
           # Check Pricing
            pricing = db.query(ProductPricingModel).where(ProductPricingModel.product_id == data.id).where(
                ProductPricingModel.deleted_at.is_(None)).first()
            if(pricing):
                # Calculate price with
                total = (pricing.price *
                         pricing.tax / 100) + pricing.price
                # Restructure Price data
                data.pricing.price = round(total, 0)
                data.pricing.id = pricing.id
                data.pricing.moq = pricing.moq

            return data

# Calculating Product Price with Tax
    async def productPricingList(request: Request, db: Session, data: ProductModel, user_id: int) -> ProductModel:

        # Check Pricing
        pricing = db.query(ProductPricingModel).where(ProductPricingModel.product_id == data.id).where(
            ProductPricingModel.deleted_at.is_(None)).all()

        price = []
        for p in pricing:

            # unique ID
            uuid = "sku-" + str(p.id) + '-variable-' + str(p.items)

            if(pricing):
                checkinventory = db.query(InventoryModel).where(
                    InventoryModel.pricing_id == p.id).first()

            # Check Stock
            stock = True
            if(checkinventory and checkinventory.out_of_stock == 0):
                stock = True
            else:
                stock = False

            # Check Unlimited Stock
            unlimited = True
            if(checkinventory and checkinventory.unlimited == 1):
                unlimited = True
            else:
                unlimited = False

            # Check Total Stock
            total_stock = 0
            if(checkinventory and checkinventory.stock != 0):
                total_stock = checkinventory.stock

            # Check Out Of Stock Quantity with Cart data
            cart = db.query(CartModel).where(CartModel.product_id == data.id).where(
                CartModel.uuid == uuid).where(CartModel.user_id == user_id).first()

            out_of_quantity = False

            message = ''

            if(cart and cart.quantity > checkinventory.stock and checkinventory.unlimited == 0):
                out_of_quantity = True
                message = 'Only '+str(checkinventory.stock) + \
                    ' '+str(p.unit)+' left'

            total = (p.price *
                     p.tax / 100) + p.price

            item_total = 0
            quantity = 0
            if(cart):
                item_total = (cart.quantity * round(total, 0))
                quantity = cart.quantity

            # Product Image
            if(p.default_image != 0):
                image = db.query(ProductMediaModel).filter(ProductMediaModel.id == p.default_image).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()
                if(image is not None):
                    filename = image.file_path
            else:
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == p.product_id).filter(ProductMediaModel.default_img == 1).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()

                if(image == None):
                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == p.product_id).filter(
                        ProductMediaModel.deleted_at.is_(None)).first()

                filename = image.file_path

            img = filename

            pdata = {
                'id': p.id,
                'product_id': p.product_id,
                'image': img,
                'price': round(total, 0),
                'moq': p.moq,
                'tax': p.tax,
                'quantity': quantity,
                'sale_price': p.price,
                'description': p.description,
                'stock': stock,
                'unlimited':  unlimited,
                'out_of_quantity': out_of_quantity,
                'total_stock': total_stock,
                'message': message,
                'unit': p.unit,
                'uuid': uuid,
                'hsn_code': p.hsn,
                'item_total': round(item_total, 0),
                'attributes': await ProductsHelper.getPricingAttribjutes(data=p, db=db)

            }

            price.append(pdata)
        return price

 # Calculating Product Price with tax (Cart)

    async def cartProductPrice(db: Session, data: CartModel) -> CartModel:
        for i in data:
            # Check Stock
            pricing = i.uuid.split('-')
            pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == pricing[1]).where(
                ProductPricingModel.deleted_at.is_(None)).first()
            # Calculate Product Price with tax
            newdate = pricing.updated_at.strftime("%Y-%m-%d")
            if(newdate >= NEW_COMMISSION_DATE):
                # Commission Calculation
                newpricecommission = NEW_COMMISSION_PERCENT
                newpricecommission = (
                    NEW_COMMISSION_PERCENT * 18) / 100 + newpricecommission
                newtcs = NEW_TCS_PERCENT
                newtds = NEW_TDS_PERCENT

                new_totalCommission = (
                    newpricecommission + newtcs + newtds) + 100
                newproductprice = (i.price * new_totalCommission) / 100

                newproductprice = round(newproductprice, 0)

                p_price = newproductprice * i.tax / 100 + newproductprice
            else:
                p_price = (i.price * i.tax / 100) + i.price

            productprice = p_price
            productprice = round(productprice, 0)
            i.price = productprice

            # Calculate Product Total Price with Quantity
            i.total = (productprice * i.quantity)

            i.moq = pricing.moq

            i.unit = pricing.unit

            if(pricing):
                checkinventory = db.query(InventoryModel).where(
                    InventoryModel.pricing_id == pricing.id).first()

            # Check Stock
            i.total_stock = checkinventory.stock
            stock = True
            if(checkinventory and checkinventory.out_of_stock == 0):
                stock = True
            else:
                stock = False

            i.stock = stock

            # Check Unlimited Stock
            unlimited = True
            if(checkinventory and checkinventory.unlimited == 1):
                unlimited = True
            else:
                unlimited = False

            i.unlimited = unlimited

            # Check Out Of Stock Quantity
            out_of_quantity = False
            message = ''
            if(i.quantity > checkinventory.stock and checkinventory.unlimited == 0):
                out_of_quantity = True
                message = 'Only '+str(checkinventory.stock) + \
                    ' '+str(pricing.unit)+' left'

            i.out_of_quantity = out_of_quantity
            i.message = message

        return data

 # Calculation Order Amount
    async def orderPrice(db: Session, data: OrdersModel) -> OrdersModel:

        if(type(data) in (tuple, list)):

            for order in data:

                # get all items
                items = db.query(OrderItemsModel).where(
                    OrderItemsModel.order_id == order.id).where(OrderItemsModel.status == 0).all()
                if(len(items) == 0):
                    items = db.query(OrderItemsModel).where(
                        OrderItemsModel.order_id == order.id).where(OrderItemsModel.status <= 81).all()

                    if(len(items) == 0):
                        items = db.query(OrderItemsModel).where(
                            OrderItemsModel.order_id == order.id).where(OrderItemsModel.status == 81).all()

                        if(len(items) == 0):
                            items = db.query(OrderItemsModel).where(
                                OrderItemsModel.order_id == order.id).where(OrderItemsModel.status > 10).where(OrderItemsModel.status < 980).all()

                            if(len(items) == 0):

                                items = db.query(OrderItemsModel).where(
                                    OrderItemsModel.order_id == order.id).where(OrderItemsModel.status == 980).all()
                total_amount = 0
                for item in items:
                    # Zero Percent Commission
                    newdate = order.created_at.strftime("%Y-%m-%d")
                    if(newdate >= NEW_COMMISSION_DATE):
                        # Commission Calculation
                        newpricecommission = NEW_COMMISSION_PERCENT
                        newpricecommission = (
                            NEW_COMMISSION_PERCENT * 18) / 100 + newpricecommission

                        if(newdate <= TDS_TCS_PERCENT_DATE):
                            newtcs = 1
                            newtds = 1
                        else:
                            newtcs = NEW_TCS_PERCENT
                            newtds = NEW_TDS_PERCENT

                        new_totalCommission = (
                            newpricecommission + newtcs + newtds) + 100
                        newproductprice = (
                            item.price * new_totalCommission) / 100
                        newproductprice = round(newproductprice)
                        price = newproductprice + \
                            (newproductprice * item.tax / 100)
                        total_amount += round(price, 0) * item.quantity
                    else:
                        price = (item.price * item.tax) / 100 + item.price
                        total_amount += round(price, 0) * item.quantity

                total_amount = round(total_amount, 0)
                discount_amount = 0
                order_date = order.created_at.strftime("%Y-%m-%d")
                if(order.discount != 0):

                    # CHECK ORDER DISCOUNT
                    order_discount = db.query(OrderDiscountModel).filter(
                        OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()
                    if(order_discount and order_discount.discount != 0):
                        discount_amount = (
                            round(total_amount, 0) * float(order_discount.discount)) / 100
                        discount_amount = round(discount_amount, 0)

                # Check Shipping Charge
                shipping_charge = db.query(ShippingChargeModel).filter(
                    ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()
                if(shipping_charge.order_limit != 0):
                    if(round(total_amount, 0) >= shipping_charge.order_limit):
                        delivery_charge = 0
                    else:
                        delivery_charge = shipping_charge.rate
                else:
                    delivery_charge = shipping_charge.rate

                if('Yes' in str(order.free_delivery)):
                    delivery_charge = 0
                # Item Total Amount
                order.item_total_amount = total_amount
                order.delivery_charge = delivery_charge
                order.discount = discount_amount
                order.total_items = len(items)
                grand_total = (
                    total_amount + float(delivery_charge)) - discount_amount
                order.created_at = order.created_at.strftime("%d %b %Y")
                order.grand_total = round(grand_total, 0)

            return data
        else:

            # get all items
            items = db.query(OrderItemsModel).where(
                OrderItemsModel.order_id == data.id).where(OrderItemsModel.status == 0).all()

            if(len(items) == 0):
                items = db.query(OrderItemsModel).where(
                    OrderItemsModel.order_id == data.id).where(OrderItemsModel.status <= 81).all()

                if(len(items) == 0):
                    items = db.query(OrderItemsModel).where(
                        OrderItemsModel.order_id == data.id).where(OrderItemsModel.status == 81).all()

                    if(len(items) == 0):
                        items = db.query(OrderItemsModel).where(
                            OrderItemsModel.order_id == data.id).where(OrderItemsModel.status > 10).where(OrderItemsModel.status < 980).all()

                        if(len(items) == 0):

                            items = db.query(OrderItemsModel).where(
                                OrderItemsModel.order_id == data.id).where(OrderItemsModel.status == 980).all()
            total_amount = 0

            for item in items:
                # Zero Percent Commission
                newdate = data.created_at.strftime("%Y-%m-%d")
                if(newdate >= NEW_COMMISSION_DATE):
                    # Commission Calculation
                    newpricecommission = NEW_COMMISSION_PERCENT
                    newpricecommission = (
                        NEW_COMMISSION_PERCENT * 18) / 100 + newpricecommission

                    if(newdate <= TDS_TCS_PERCENT_DATE):
                        newtcs = 1
                        newtds = 1
                    else:
                        newtcs = NEW_TCS_PERCENT
                        newtds = NEW_TDS_PERCENT

                    new_totalCommission = (
                        newpricecommission + newtcs + newtds) + 100
                    newproductprice = (
                        item.price * new_totalCommission) / 100
                    newproductprice = round(newproductprice)
                    price = newproductprice + \
                        (newproductprice * item.tax / 100)
                    total_amount += round(price, 0) * item.quantity
                else:
                    price = (item.price * item.tax) / 100 + item.price
                    total_amount += round(price, 0) * item.quantity

            total_amount = round(total_amount, 0)
            discount_amount = 0
            order_date = data.created_at.strftime("%Y-%m-%d")
            if(data.discount != 0):

                # CHECK ORDER DISCOUNT
                order_discount = db.query(OrderDiscountModel).filter(
                    OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()
                if(order_discount and order_discount.discount != 0):
                    discount_amount = (
                        round(total_amount, 0) * float(order_discount.discount)) / 100
                    discount_amount = round(discount_amount, 0)

            # Check Shipping Charge
            shipping_charge = db.query(ShippingChargeModel).filter(
                ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()
            if(shipping_charge.order_limit != 0):
                if(round(total_amount, 0) >= shipping_charge.order_limit):
                    deliverycharge = 0
                else:
                    deliverycharge = shipping_charge.rate
            else:
                deliverycharge = shipping_charge.rate

         # Item Total Amount
            if('Yes' in str(data.free_delivery)):
                deliverycharge = 0

            data.item_total_amount = total_amount
            data.delivery_charge = deliverycharge
            data.discount = discount_amount
            data.total_items = len(items)
            grand_total = (
                total_amount + float(deliverycharge)) - discount_amount
            data.created_at = data.created_at.strftime("%d %b %Y")
            data.grand_total = round(grand_total, 0)

            return data

    async def calculateTotalAmount(db: Session, order):
        try:

            items = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 0).all()

            if(len(items) == 0):

                items = db.query(OrderItemsModel).filter(
                    OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status <= 81).all()

                if(len(items) == 0):
                    items = db.query(OrderItemsModel).filter(
                        OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 81).all()

                    if(len(items) == 0):
                        items = db.query(OrderItemsModel).filter(
                            OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status > 10).filter(OrderItemsModel.status < 980).all()

                        if(len(items) == 0):
                            items = db.query(OrderItemsModel).filter(
                                OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 980).all()

            total_amount = 0
            for item in items:
                # Zero Percent Commission
                newdate = order.created_at.strftime("%Y-%m-%d")
                if(newdate >= NEW_COMMISSION_DATE):
                    # Commission Calculation
                    newpricecommission = NEW_COMMISSION_PERCENT
                    newpricecommission = (
                        NEW_COMMISSION_PERCENT * 18) / 100 + newpricecommission

                    if(newdate <= TDS_TCS_PERCENT_DATE):
                        newtcs = 1
                        newtds = 1
                    else:
                        newtcs = NEW_TCS_PERCENT
                        newtds = NEW_TDS_PERCENT

                    new_totalCommission = (
                        newpricecommission + newtcs + newtds) + 100
                    newproductprice = (
                        item.price * new_totalCommission) / 100
                    newproductprice = round(newproductprice)
                    price = newproductprice + \
                        (newproductprice * item.tax / 100)
                    total_amount += round(price, 0) * item.quantity
                else:
                    price = (item.price * item.tax) / 100 + item.price
                    total_amount += round(price) * item.quantity

            return {"total_amount": total_amount, "items_count": len(items)}

        except Exception as e:
            return {"total_amount": 0, "items_count": 0}

    async def totalAmount(db: Session, data):

        # get all items
        items = db.query(OrderItemsModel).where(
            OrderItemsModel.order_id == data.id).where(OrderItemsModel.status == 0).all()

        if(len(items) == 0):
            items = db.query(OrderItemsModel).where(
                OrderItemsModel.order_id == data.id).where(OrderItemsModel.status <= 81).all()

            if(len(items) == 0):
                items = db.query(OrderItemsModel).where(
                    OrderItemsModel.order_id == data.id).where(OrderItemsModel.status == 81).all()

                if(len(items) == 0):
                    items = db.query(OrderItemsModel).where(
                        OrderItemsModel.order_id == data.id).where(OrderItemsModel.status > 10).where(OrderItemsModel.status < 980).all()

                    if(len(items) == 0):

                        items = db.query(OrderItemsModel).where(
                            OrderItemsModel.order_id == data.id).where(OrderItemsModel.status == 980).all()
        total_amount = 0

        for item in items:
            price = (item.price * item.tax) / 100 + item.price
            total_amount += round(price, 0) * item.quantity

        total_amount = round(total_amount, 0)
        discount_amount = 0
        order_date = data.created_at.strftime("%Y-%m-%d")
        if(data.discount != 0):

            # CHECK ORDER DISCOUNT
            order_discount = db.query(OrderDiscountModel).filter(
                OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

            # order_discount = db.execute("SELECT id, discount, start_date, payment_method FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
            #                             {"param": order_date}).first()

            if(order_discount and order_discount.discount != 0):

                discount_amount = (
                    round(total_amount, 0) * float(order_discount.discount)) / 100

                discount_amount = round(discount_amount, 0)

        # Check Shipping Charge
        shipping_charge = db.query(ShippingChargeModel).filter(
            ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

        # shipping_charge = db.execute("SELECT id, rate, start_date, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
        #                              {"param": order_date}).first()

        if(shipping_charge.order_limit != 0):
            if(round(total_amount, 0) >= shipping_charge.order_limit):
                deliverycharge = 0
            else:
                deliverycharge = shipping_charge.rate
        else:
            deliverycharge = shipping_charge.rate

        if('Yes' in str(data.free_delivery)):
            deliverycharge = 0

        grand_total = (
            total_amount + float(deliverycharge)) - discount_amount
        # Item Total Amount
        return {"total_amount": total_amount, "discount_amount": discount_amount, "delivery_charge":  deliverycharge, "total_items": len(items), 'grand_total': round(grand_total, 0)}

    async def itemTotalPrice(db: Session, data):
        price = 0
        for item in data:
            itemprice = (item.price * item.tax) / 100 + item.price
            itemprice = round(itemprice, 0) * item.quantity
            price += itemprice

        return price

    async def calculateTatalAmountforfacility(db: Session, data):
        items = db.query(OrderItemsModel).filter(
            OrderItemsModel.order_id == data.id).filter(OrderItemsModel.status != 980).all()

        total_amount = 0

        for item in items:
            price = (item.price * item.tax) / 100 + item.price
            price = round(price) * item.quantity

            total_amount += round(price)

        discount_amount = 0

        order_created = data.created_at.strftime("%Y-%m-%d")

        if(data.discount != 0):
            # CHECK ORDER DISCOUNT
            order_discount = db.query(OrderDiscountModel).filter(
                OrderDiscountModel.start_date <= order_created).order_by(OrderDiscountModel.id.desc()).first()

            # order_discount = db.execute("SELECT id, discount, start_date, payment_method FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
            #                             {"param": order_created}).first()
            if(order_discount.discount != 0):

                discount_amount = (
                    total_amount * order_discount.discount) / 100
                discount_amount = round(discount_amount)

         # Check Shipping Charge
        shipping_charge = db.query(ShippingChargeModel).filter(
            ShippingChargeModel.start_date <= order_created).order_by(ShippingChargeModel.id.desc()).first()

        # shipping_charge = db.execute("SELECT id, rate, start_date, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
        #                              {"param": order_created}).first()

        if(shipping_charge.order_limit != 0):
            if(round(total_amount, 0) >= shipping_charge.order_limit):
                deliverycharge = 0
            else:
                deliverycharge = shipping_charge.rate
        else:
            deliverycharge = shipping_charge.rate

        if('Yes' in str(data.free_delivery)):
            deliverycharge = 0

        grand_total = (
            total_amount + float(deliverycharge)) - discount_amount

        return grand_total

    # Calculate Total Order Amount for Admin
    async def CalculateTotalAmountAllOrders(db: Session, data: OrdersModel) -> OrdersModel:
        try:

            total_amount = 0
            for order in data:

                # get all items
                items = order.order_items.filter(
                    OrderItemsModel.status == 0).all()
                if(len(items) == 0):
                    items = order.order_items.filter(
                        OrderItemsModel.status <= 81).all()

                    if(len(items) == 0):
                        items = order.order_items.filter(
                            OrderItemsModel.status == 81).all()

                        if(len(items) == 0):
                            items = order.order_items.filter(OrderItemsModel.status > 10).filter(
                                OrderItemsModel.status < 980).all()

                            if(len(items) == 0):

                                items = order.order_items.filter(
                                    OrderItemsModel.order_id == order.id).filter(OrderItemsModel.status == 980).all()

                total_amount = 0
                if(items is not None):
                    for item in items:
                        # Pricing Object
                        pricingdata = item.uuid.split('-')
                        pricingdata = db.query(ProductPricingModel).filter(
                            ProductPricingModel.id == pricingdata[1]).first()
                        # Product Object
                        productdata = db.query(ProductModel).filter(
                            ProductModel.id == item.product_id).first()

                        # Aseztak Service
                        # aseztak_service = Services.aseztak_services(
                        #     item.created_at, db=db)
                        today_date = item.created_at.strftime('%Y-%m-%d')
                        aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)

                        if(aseztak_service is None):

                            # Calculate Product Price
                            product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
                                                                    gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)

                            total_amount += product_price * item.quantity
                        else:
                            product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item.id)
                            # Calculate Product Price
                            # product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
                            #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order.app_version)

                            total_amount += product_price * item.quantity

                    discount_amount = 0
                    order_date = order.created_at.strftime("%Y-%m-%d")
                    if(order.discount != 0):
                        # CHECK ORDER DISCOUNT
                        order_discount = db.query(OrderDiscountModel).filter(
                            OrderDiscountModel.start_date <= order_date).order_by(OrderDiscountModel.id.desc()).first()

                        discount_rate = order_discount.discount
                        if(order.discount_rate != 0):
                            discount_rate = order.discount_rate

                        discount_amount = orderDiscountCalculation(app_version=order.app_version,
                                                                   order_amount=total_amount, discount_amount=order.discount, discount_rate=discount_rate)

                    # Check Shipping Charge
                    # delivery_charge = 0
                    # if(order.delivery_charge != 0): CHANGES RAHUL
                    shipping_charge = db.query(ShippingChargeModel).filter(
                        ShippingChargeModel.start_date <= order_date).order_by(ShippingChargeModel.id.desc()).first()

                    delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order.free_delivery, user_id=order.user_id, order_date=order.created_at,
                                                               app_version=order.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

                    # Item Total Amount
                    order.item_total_amount = floatingValue(total_amount)

                    order.total_items = len(items)

                    if(order.app_version == 'V4'):

                        grand_total = roundOf(order.grand_total)
                    else:
                        grand_total = (
                            total_amount + delivery_charge) - discount_amount

                        grand_total = roundOf(grand_total)

                    order.grand_total = floatingValue(grand_total)

            return data

        except Exception as e:
            print(e)
