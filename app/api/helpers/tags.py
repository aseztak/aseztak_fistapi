

from sqlalchemy.orm import Session
from app.db.models.tag import TagModel
from app.resources.strings import *
from app.api.helpers.auth import *
# @staticmethod


async def get_tag(db: Session, tag_id: int):

    return db.query(TagModel).filter(TagModel.id == tag_id).first()

# list of default tags


async def get_tags(db: Session):

    return db.query(TagModel).where(TagModel.status == 1).order_by(TagModel.counter.desc()).limit(15).all()


async def search_tags(nameLike: str, db: Session):
    # return db.query(TagModel).where(
    #     and_(
    #         TagModel.status == 1,
    #         TagModel.status.name == True
    #     )
    # ).limit(15).all()
    #
    search = "%{}%".format(nameLike)
    print("==========================" + search)

    # return db.query(TagModel).filter(TagModel.name.like('%' + nameLike + '%')).limit(15).all()
    return db.query(TagModel).filter(TagModel.name.ilike(search)).limit(15).all()


# Search Tags and Products
async def search_tags_products(nameLike: str, db: Session):

    search = "%{}%".format(nameLike)
    results = db.query(TagModel).filter(TagModel.name.ilike(search)).limit(15)
    r = []
    if(results.count() == 0):
        results = db.query(ProductModel).filter(
            ProductModel.status == 51).filter(ProductModel.title.like(search)).group_by(ProductModel.id).limit(15)

        if(results.count() > 0):
            r = []
            for result in results:
                rr = {
                    'id': result.id,
                    'name': result.title,
                    'slug': result.slug,
                    'status': result.status
                }
                r.append(rr)

    else:
        r = []
        for result in results:
            rr = {
                'id': result.id,
                'name': result.name,
                'slug': result.slug,
                'status': result.status
            }
            r.append(rr)

    return r
