from sqlalchemy.orm import Session
from app.db.models.fulfillment_items import FulFillmentItemsModel
from app.db.models.fulfillment_orders import FulFillmentOrdersModel


from app.resources.strings import *


class FulfillmentHelper:

    async def getFulfillItems(db: Session, page: int, limit: int):
        return db.query(FulFillmentItemsModel).filter(FulFillmentItemsModel.status == 1).order_by(FulFillmentItemsModel.id.desc())

    # Orders List
    async def getOrders(db: Session, user_id: int):

        orders = db.query(FulFillmentOrdersModel).filter(
            FulFillmentOrdersModel.seller_id == user_id).order_by(FulFillmentOrdersModel.id.desc())
        return orders
