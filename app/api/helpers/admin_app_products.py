from datetime import date

from sqlalchemy.sql.expression import desc, asc

from sqlalchemy.sql.functions import func
from starlette.requests import Request
# from app.db.config import SessionLocal
from app.db.models.attribute_values import AttributeValueModel
from app.db.models.attributes import AttributeModel
from app.db.models.media import ProductMediaModel
from app.db.models.product_tags import ProductTagModel
from app.db.models.productattribute import ProductAttributeModel
from app.db.models.products import FavouriteModel, InventoryModel, ProductModel, ProductPricingAttributeModel, ProductPricingModel
from app.db.models.categories import *

from sqlalchemy.orm import Session
from app.db.models.returndays import CategoryReturnDaysModel
from app.db.models.tag import TagModel
from app.db.models.user import UserModel
from sqlalchemy.orm import joinedload
from app.resources.strings import *
import itertools


class ProductsHelper:
    # Admin
    async def AllProducts(db: Session):
        return db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).order_by(ProductModel.id.desc())

    async def AllProductsStatusWise(db: Session, status: str):
        return db.query(ProductModel).filter(ProductModel.status == status).order_by(ProductModel.id.desc())

    #END#
    async def AllProductsAdmin(db: Session, status: str):
        if(status == 'all'):
            return db.query(ProductModel).order_by(ProductModel.id.desc())

        if(status == '1'):
            return db.query(ProductModel).filter(ProductModel.status == 1).order_by(ProductModel.id.desc())

        if(status == '51'):
            return db.query(ProductModel).filter(ProductModel.status == 51).order_by(ProductModel.id.desc())

        if(status == '98'):
            return db.query(ProductModel).filter(ProductModel.status == 98).order_by(ProductModel.id.desc())

        if(status == '99'):
            return db.query(ProductModel).filter(ProductModel.status == 99).order_by(ProductModel.id.desc())

    async def AllProductsSearchAdmin(db: Session, search: str, status: str):

        search = "%{}%".format(search.replace("+", " ").strip())
        if(status == 'all'):
            return db.query(ProductModel).filter(ProductModel.title.like(search)).order_by(ProductModel.id.desc())

        if(status == '1'):
            return db.query(ProductModel).filter(ProductModel.title.like(search)).filter(ProductModel.status == 1).order_by(ProductModel.id.desc())

        if(status == '51'):
            return db.query(ProductModel).filter(ProductModel.title.like(search)).filter(ProductModel.status == 51).order_by(ProductModel.id.desc())

        if(status == '98'):
            return db.query(ProductModel).filter(ProductModel.title.like(search)).filter(ProductModel.status == 98).order_by(ProductModel.id.desc())

        if(status == '99'):
            return db.query(ProductModel).filter(ProductModel.title.like(search)).filter(ProductModel.status == 99).order_by(ProductModel.id.desc())

    def get_products(db: Session):
        return db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).where(ProductModel.status == 51).order_by(desc(ProductModel.id))

    # Get Product
    async def getProduct(db: Session, id: int):
        return db.query(ProductModel).filter(ProductModel.id == id).first()

    # Product List category Wise
    def get_products_category_wise(db: Session, categoryId: int):
        return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductCategories, ProductModel.id == ProductCategories.product_id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).where(ProductCategories.category_id == categoryId).where(ProductModel.status == 51).where(UserModel.status == 1).group_by(ProductModel.id).order_by(desc(ProductModel.id))

    async def get_product(db: Session, product_id: int):

        product = db.query(ProductModel).where(
            ProductModel.id == product_id).where(ProductModel.status == 51).first()

        if(product):
            return product
        else:
            return False

    # Get Product Buy
    async def get_product_buy(db: Session, p_id: int):
        return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).where(ProductModel.status == 51).where(ProductPricingModel.deleted_at.is_(None)).where(ProductModel.id == p_id).first()

    # Get Fetaured Products
    async def get_featured_products(db: Session, limit: int):
        return db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).where(ProductModel.featured == 1).order_by(desc(ProductModel.id)).limit(limit=limit).all()

    # Check favourite list of Products
    async def isFavourite(data: ProductModel, user_id: int, db: Session) -> ProductModel:

        if(type(data) in (tuple, list)):

            for i in data:

                # Check Favourite
                checkWish = i.wishlist
                favourite = False
                if(checkWish and checkWish.user_id == user_id):
                    favourite = True

                i.favourite = favourite

            return data
        else:
            # Check Favourite
            checkWish = data.wishlist
            favourite = False
            if(checkWish):
                checkWish = db.query(FavouriteModel).filter(
                    FavouriteModel.product_id == checkWish.product_id).filter(FavouriteModel.user_id == user_id).first()

                if(checkWish and checkWish.user_id == user_id):
                    favourite = True

            data.favourite = favourite

        return data

    # Check Stock of Product

    async def isStock(data: ProductModel) -> ProductModel:

        if(type(data) in (tuple, list)):
            for i in data:

                if(i.pricing):
                    # Check Stock
                    checkStock = i.pricing.inventory
                    stock = True
                    if(checkStock and checkStock.out_of_stock == 0):
                        stock = True
                    else:
                        stock = False

                    i.stock = stock
            return data
        else:

            if(data.pricing):
                # Check Stock
                checkStock = data.pricing.inventory
                stock = True
                if(checkStock and checkStock.out_of_stock == 0):
                    stock = True
                else:
                    stock = False

                data.stock = stock

            return data

    # Custom Static Text
    async def staticText(request: Request, db: Session, data: ProductModel) -> ProductModel:
        try:

            if(type(data) in (tuple, list)):
                for i in data:

                    today = date.today()
                    # YY-mm-dd
                    today = today.strftime("%Y/%m/%d")

                    shipping_charge = db.execute("SELECT id, rate, start_date FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                                 {"param": today}).first()

                    if(int(shipping_charge.rate) == 0):
                        dl_text = PRODUCT_FREE_DELIVERY_TEXT
                    else:
                        dl_text = PRODUCT_DELIVERY_BODY_TEXT

                    i.delivery = {
                        'title': PRODUCT_DELIVERY_HEADING_TEXT,
                        'rate': int(shipping_charge.rate),
                        'text': dl_text
                    }

                    i.payment = {
                        'title': PAYMENT_OPTION_HEADING_TEXT,
                        'text': PAYMENT_OPTION_BODY_TEXT
                    }
                    # CHECK PRODUCT RETURN DAYS
                    return_days = db.query(CategoryReturnDaysModel).filter(
                        CategoryReturnDaysModel.category_id == i.category).filter(CategoryReturnDaysModel.start_date <= today).order_by(desc(CategoryReturnDaysModel.start_date)).first()

                    if(return_days.return_days != 0):
                        r_days = i.product_return_text = RETURN_DAYS_BODY
                    else:
                        r_days = i.product_return_text = RETURN_DAYS_NOT_APPLICABLE

                    i.return_days = {
                        'title': RETURN_DAYS_HEADING,
                        'rate': return_days.return_days,
                        'text': r_days
                    }

                    # CHECK ORDER DISCOUNT
                    discount = db.execute("SELECT id, discount, start_date FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
                                          {"param": today}).first()

                    i.discount = {
                        'rate': round(discount.discount, 2),
                        'text': DISCOUNT_TEXT
                    }

                return data

            else:

                today = date.today()
            # YY-mm-dd
                today = today.strftime("%Y/%m/%d")

                shipping_charge = db.execute("SELECT id, rate, start_date FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
                                             {"param": today}).first()

                if(int(shipping_charge.rate) == 0):
                    dl_text = PRODUCT_FREE_DELIVERY_TEXT
                else:
                    dl_text = PRODUCT_DELIVERY_BODY_TEXT

                data.delivery = {
                    'title': PRODUCT_DELIVERY_HEADING_TEXT,
                    'rate': int(shipping_charge.rate),
                    'text': dl_text
                }

                data.payment = {
                    'title': PAYMENT_OPTION_HEADING_TEXT,
                    'text': PAYMENT_OPTION_BODY_TEXT
                }

                # CHECK PRODUCT RETURN DAYS
                return_days = db.query(CategoryReturnDaysModel).filter(
                    CategoryReturnDaysModel.category_id == data.category).filter(CategoryReturnDaysModel.start_date <= today).order_by(desc(CategoryReturnDaysModel.start_date)).first()

                if(return_days.return_days != 0):
                    r_days = data.product_return_text = RETURN_DAYS_BODY
                else:
                    r_days = data.product_return_text = RETURN_DAYS_NOT_APPLICABLE

                data.return_days = {
                    'title': RETURN_DAYS_HEADING,
                    'rate': return_days.return_days,
                    'text': r_days
                }

                # CHECK ORDER DISCOUNT
                discount = db.execute("SELECT id, discount, start_date FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
                                      {"param": today}).first()

                data.discount = {
                    'rate': round(discount.discount, 2),
                    'text': DISCOUNT_TEXT
                }

            return data

        except Exception as e:
            print(e)

    # Custom Product Image
    async def productImage(request: Request, db: Session, data: ProductModel) -> ProductModel:
        try:

            if(type(data) in (tuple, list)):
                for i in data:

                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == i.id).filter(ProductMediaModel.default_img == 1).filter(
                        ProductMediaModel.deleted_at.is_(None)).first()

                    if(image == None):
                        image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == i.id).filter(
                            ProductMediaModel.deleted_at.is_(None)).first()

                    if(image is not None):
                        if(image.file_path is not None):
                            img = image.file_path
                        else:
                            img = ''
                        i.image = img

                return data

            else:

                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == data.id).filter(ProductMediaModel.default_img == 1).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()

                if(image == None):
                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == data.id).filter(
                        ProductMediaModel.deleted_at.is_(None)).first()
                if(image is not None):
                    img = image.file_path

                    data.image = img

            return data

        except Exception as e:
            print(e)

    # Get Product Specification
    async def getAttributes(data: ProductModel, db: Session):

        specifications = db.query(
            ProductAttributeModel.attribute_id, ProductAttributeModel.product_id, func.count(ProductAttributeModel.attribute_id)).filter(ProductAttributeModel.product_id == data.id).group_by(ProductAttributeModel.attribute_id).all()

        attrdata = []
        for i in specifications:
            attribute = db.query(AttributeModel).filter(
                AttributeModel.id == i.attribute_id).first()
            product_attributes = db.query(ProductAttributeModel).where(
                ProductAttributeModel.product_id == i.product_id).all()

            values = ''
            for product_attribute in product_attributes:
                if(product_attribute.attribute_id == i.attribute_id):
                    values += product_attribute.attribute_value + ', '

            attr = {
                'name': attribute.name,
                'values': values.rstrip(', ')
            }
            attrdata.append(attr)
        return attrdata

    # Get Prodcut pricing attributes
    async def getPricingAttribjutes(data: ProductPricingModel, db: Session):

        pricing_attributes = db.query(ProductPricingAttributeModel).filter(
            ProductPricingAttributeModel.product_pricing_id == data.id).all()

        specifications = db.query(ProductAttributeModel.attribute_id, ProductAttributeModel.product_id).where(
            ProductAttributeModel.product_id == data.product_id).group_by(ProductAttributeModel.attribute_id).all()

        attrdata = []
        for pattributes in specifications:
            attribute = db.query(AttributeModel).filter(
                AttributeModel.id == pattributes.attribute_id).first()

            if(attribute and attribute.is_price_variable == 1 and pattributes.attribute_id == attribute.id):

                productattributes = db.query(ProductAttributeModel).where(
                    ProductAttributeModel.product_id == data.product_id).where(ProductAttributeModel.attribute_id == pattributes.attribute_id).all()

                values = ''
                for pv in pricing_attributes:

                    for pa in productattributes:

                        if(pa.attribute_value_id == pv.attribute_id):
                            attrval = db.query(AttributeValueModel).where(
                                AttributeValueModel.id == pa.attribute_value_id).first()
                            values += attrval.value+', '

                attr = {
                    'name': attribute.name,
                    'values': values.rstrip(', ')
                }

                attrdata.append(attr)
        return attrdata

    # Sorting by
    async def sortingProducts(db: Session, category_id: int, sort: str, min_price: int, max_price: int):
        try:

            if(sort == 'asc'):
                products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductCategories, ProductCategories.product_id == ProductModel.id).where(ProductCategories.category_id == category_id).where(UserModel.status == 1).where(ProductModel.status == 51).where(
                    func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) >= min_price).where(
                    func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) <= max_price).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(asc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

            else:
                products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductCategories, ProductCategories.product_id == ProductModel.id).where(ProductCategories.category_id == category_id).where(UserModel.status == 1).where(ProductModel.status == 51).where(
                    func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) >= min_price).where(
                    func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) <= max_price).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

            return products

        except Exception as e:
            return False

    # Get Product Min/Max Price
    async def productMinMaxPirce(db: Session, category_id: int):
        try:

            pricing = db.execute('SELECT MAX(product_pricing.price * product_pricing.tax / 100 + product_pricing.price) as max_price, MIN(product_pricing.price * product_pricing.tax / 100 + product_pricing.price) as min_price from product_pricing LEFT JOIN products on products.id=product_pricing.product_id LEFT JOIN product_categories on product_categories.product_id=products.id WHERE product_categories.category_id=:param', {
                "param": category_id
            }).first()

            return pricing

        except Exception as e:
            print(e)

    # Get Product Min/Max Price by tag
    async def productMinMaxPircebyTag(db: Session, tag_id: int):
        try:

            tag = db.query(TagModel).filter(TagModel.id == tag_id).first()

            pricing = db.execute('SELECT MAX(product_pricing.price * product_pricing.tax / 100 + product_pricing.price) as max_price, MIN(product_pricing.price * product_pricing.tax / 100 + product_pricing.price) as min_price from product_pricing LEFT JOIN products on products.id=product_pricing.product_id LEFT JOIN product_tags on product_tags.product_id=products.id WHERE product_tags.tag_id=:param', {
                "param": tag.id
            }).first()

            return pricing

        except Exception as e:
            print(e)

    # Get Filter Attributes
    async def filterAttributes(db: Session, category_id: int):
        try:

            attributes_id = db.query(ProductAttributeModel.attribute_id).join(ProductModel, ProductModel.id == ProductAttributeModel.product_id).join(
                ProductCategories, ProductCategories.product_id == ProductAttributeModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductCategories.category_id == category_id).group_by(ProductAttributeModel.attribute_id).all()

            attributes = []
            for attribute in attributes_id:
                attributes.append(attribute.attribute_id)

            attributes_val = db.query(ProductAttributeModel.attribute_id, ProductAttributeModel.attribute_value_id, ProductAttributeModel.attribute_value).join(ProductModel, ProductModel.id == ProductAttributeModel.product_id).join(
                ProductCategories, ProductCategories.product_id == ProductAttributeModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductCategories.category_id == category_id).group_by(ProductAttributeModel.attribute_id, ProductAttributeModel.attribute_value_id, ProductAttributeModel.attribute_value).all()

            if(len(attributes) == 0 or len(attributes_val) == 0):
                return False

            ideal_for = []
            color = []
            clothing_size = []

            filter_attributes = []
            for attr in attributes:
                attribute_data = db.query(AttributeModel).where(
                    AttributeModel.id == attr).first()
                if(attribute_data.is_filterable == 1):
                    values = []
                    for value in attributes_val:
                        if (attr == value.attribute_id):
                            v = {
                                'id': value.attribute_value_id,
                                'value': value.attribute_value
                            }
                            values.append(v)
                    if(attribute_data.code == 'ideal_for'):

                        attrs = {
                            'name': attribute_data.name,
                            'values': values
                        }

                        ideal_for.append(attrs)

                    elif(attribute_data.code == 'color_for_all_product'):
                        attrs = {
                            'name': attribute_data.name,
                            'values': values
                        }

                        color.append(attrs)

                    elif(attribute_data.code == 'clothing_size_for_all_types_of_garments_except_mens_inner_wear_products'):
                        attrs = {
                            'name': attribute_data.name,
                            'values': values
                        }

                        clothing_size.append(attrs)

                    else:
                        attrs = {
                            'name': attribute_data.name,
                            'values': values
                        }

                        filter_attributes.append(attrs)

            return ideal_for + color + clothing_size + filter_attributes

        except Exception as e:
            return False

   # Filter by Category
    async def filerSearchProducts(db: Session, category_id: int, sort: str, min_price: int, max_price: int, attributes: str):
        try:

            if(len(attributes) == 0):
                if(sort == 'asc'):
                    products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductCategories, ProductCategories.product_id == ProductModel.id).where(ProductCategories.category_id == category_id).where(UserModel.status == 1).where(ProductModel.status == 51).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) >= min_price).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) <= max_price).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(asc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                elif(sort == 'desc'):
                    products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductCategories, ProductCategories.product_id == ProductModel.id).where(ProductCategories.category_id == category_id).where(UserModel.status == 1).where(ProductModel.status == 51).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) >= min_price).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) <= max_price).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                else:
                    products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductCategories, ProductCategories.product_id == ProductModel.id).where(ProductCategories.category_id == category_id).where(UserModel.status == 1).where(ProductModel.status == 51).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) >= min_price).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) <= max_price).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(ProductModel.id))

                return products
            else:

                attributess = []
                for attribute in attributes:

                    attribute_value = db.query(AttributeValueModel).filter(
                        AttributeValueModel.id == attribute).first()

                    attributess.append(attribute_value.attribute_id)

                productsattributesdata = []
                for attributedata in attributes:
                    for attribute in set(attributess):

                        product = db.execute('SELECT DISTINCT products.id FROM products LEFT JOIN product_categories on product_categories.product_id=products.id LEFT JOIN users on users.id=products.userid LEFT JOIN product_pricing ON product_pricing.product_id=products.id LEFT JOIN product_attributes ON product_attributes.product_id = products.id WHERE product_categories.category_id=:param and users.status=1 and ROUND(product_pricing.price * product_pricing.tax / 100 + product_pricing.price)  BETWEEN :param1 AND :param2 and products.status=51 and product_attributes.attribute_value_id =:param3 and product_attributes.attribute_id =:param4 ORDER BY products.id DESC', {
                            "param": category_id, "param1": min_price, "param2": max_price, "param3": attributedata, "param4": attribute}).all()

                        productsattributesdata.append(product)

                productsattributesdata = list(
                    itertools.chain(*productsattributesdata))

                productids = []
                for product in productsattributesdata:
                    productids.append(product.id)

                productids.sort()

                productsattrs = [list(v)
                                 for k, v in itertools.groupby(productids)]

                products = []
                for productsattr in productsattrs:
                    if(len(productsattr) == len(set(attributess))):

                        products.append(productsattr[0])

                if(sort == 'desc'):
                    products = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).where(
                        ProductModel.id.in_(products)).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))
                elif(sort == 'asc'):
                    products = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).where(
                        ProductModel.id.in_(products)).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(asc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                else:
                    products = db.query(ProductModel).where(ProductModel.id.in_(products)).group_by(
                        ProductModel.id).order_by(desc(ProductModel.id))
                return products

        except Exception as e:
            print(e)

    # Filter by Tag
    async def filerSearchProductsbyTags(db: Session, tag_id: int, sort: str, min_price: int, max_price: int, attributes: str):
        try:

            # tag
            tag = db.query(TagModel).filter(TagModel.id == tag_id).first()

            if(len(attributes) == 0):
                if(sort == 'asc'):
                    products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductTagModel, ProductTagModel.product_id == ProductModel.id).where(ProductTagModel.tag_id == tag.id).where(UserModel.status == 1).where(ProductModel.status == 51).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) >= min_price).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) <= max_price).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(asc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                elif(sort == 'desc'):
                    products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductTagModel, ProductTagModel.product_id == ProductModel.id).where(ProductTagModel.tag_id == tag.id).where(UserModel.status == 1).where(ProductModel.status == 51).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) >= min_price).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) <= max_price).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                else:
                    products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductTagModel, ProductTagModel.product_id == ProductModel.id).where(ProductTagModel.tag_id == tag.id).where(UserModel.status == 1).where(ProductModel.status == 51).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) >= min_price).where(
                        func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) <= max_price).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(ProductModel.id))

                return products
            else:

                attributess = []
                for attribute in attributes:

                    attribute_value = db.query(AttributeValueModel).filter(
                        AttributeValueModel.id == attribute).first()

                    attributess.append(attribute_value.attribute_id)

                productsattributesdata = []
                for attributedata in attributes:
                    for attribute in set(attributess):

                        product = db.execute('SELECT DISTINCT products.id FROM products LEFT JOIN product_tags on product_tags.product_id=products.id LEFT JOIN users on users.id=products.userid LEFT JOIN product_pricing ON product_pricing.product_id=products.id LEFT JOIN product_attributes ON product_attributes.product_id = products.id WHERE product_tags.tag_id=:param and users.status=1 and ROUND(product_pricing.price * product_pricing.tax / 100 + product_pricing.price)  BETWEEN :param1 AND :param2 and products.status=51 and product_attributes.attribute_value_id =:param3 and product_attributes.attribute_id =:param4 ORDER BY products.id DESC', {
                            "param": tag.id, "param1": min_price, "param2": max_price, "param3": attributedata, "param4": attribute}).all()

                        productsattributesdata.append(product)

                productsattributesdata = list(
                    itertools.chain(*productsattributesdata))

                productids = []
                for product in productsattributesdata:
                    productids.append(product.id)

                productids.sort()

                productsattrs = [list(v)
                                 for k, v in itertools.groupby(productids)]

                products = []
                for productsattr in productsattrs:
                    if(len(productsattr) == len(set(attributess))):

                        products.append(productsattr[0])

                if(sort == 'desc'):
                    products = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).where(
                        ProductModel.id.in_(products)).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))
                elif(sort == 'asc'):
                    products = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).where(
                        ProductModel.id.in_(products)).where(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(asc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                else:
                    products = db.query(ProductModel).where(ProductModel.id.in_(products)).group_by(
                        ProductModel.id).order_by(desc(ProductModel.id))
                return products

        except Exception as e:
            print(e)

    # Seller Wise Products

    async def sellerWiseProducts(db: Session, seller_id: int):

        products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductPricingModel.deleted_at.is_(None)).filter(
            ProductModel.userid == seller_id).group_by(ProductModel.category).order_by(desc(ProductModel.id))

        return products

    # Get Products by tags

    async def getProductsbytags(db: Session, tag_id: int):

        try:

            products = db.query(ProductModel).join(ProductTagModel, ProductTagModel.product_id ==
                                                   ProductModel.id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductTagModel.tag_id == tag_id).group_by(ProductTagModel.product_id).order_by(desc(ProductModel.id))
            return products

        except Exception as e:
            False

    # Get Filter Attributes by Tags

    async def filterAttributesbyTags(db: Session, tag_id: int):
        try:

            # tag
            tag = db.query(TagModel).where(TagModel.id == tag_id).first()

            attributes_id = db.query(ProductAttributeModel.attribute_id).join(ProductModel, ProductModel.id == ProductAttributeModel.product_id).join(
                ProductTagModel, ProductTagModel.product_id == ProductAttributeModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductTagModel.tag_id == tag.id).group_by(ProductAttributeModel.attribute_id).all()

            attributes = []
            for attribute in attributes_id:
                attributes.append(attribute.attribute_id)

            attributes_val = db.query(ProductAttributeModel.attribute_id, ProductAttributeModel.attribute_value_id, ProductAttributeModel.attribute_value).join(ProductModel, ProductModel.id == ProductAttributeModel.product_id).join(
                ProductTagModel, ProductTagModel.product_id == ProductAttributeModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductTagModel.tag_id == tag.id).group_by(ProductAttributeModel.attribute_id, ProductAttributeModel.attribute_value_id, ProductAttributeModel.attribute_value).all()

            if(len(attributes) == 0 or len(attributes_val) == 0):
                return False

            ideal_for = []
            color = []
            clothing_size = []

            filter_attributes = []
            for attr in attributes:
                attribute_data = db.query(AttributeModel).where(
                    AttributeModel.id == attr).first()
                if(attribute_data.is_filterable == 1):
                    values = []
                    for value in attributes_val:
                        if (attr == value.attribute_id):
                            v = {
                                'id': value.attribute_value_id,
                                'value': value.attribute_value
                            }
                            values.append(v)
                    if(attribute_data.code == 'ideal_for'):

                        attrs = {
                            'name': attribute_data.name,
                            'values': values
                        }

                        ideal_for.append(attrs)

                    elif(attribute_data.code == 'color_for_all_product'):
                        attrs = {
                            'name': attribute_data.name,
                            'values': values
                        }

                        color.append(attrs)

                    elif(attribute_data.code == 'clothing_size_for_all_types_of_garments_except_mens_inner_wear_products'):
                        attrs = {
                            'name': attribute_data.name,
                            'values': values
                        }

                        clothing_size.append(attrs)

                    else:
                        attrs = {
                            'name': attribute_data.name,
                            'values': values
                        }

                        filter_attributes.append(attrs)

            return ideal_for + color + clothing_size + filter_attributes

        except Exception as e:
            return False

    # Get Product Pricing list

    async def pricingList(db: Session, product_id: int):
        try:

            pricings = db.query(ProductPricingModel).filter(ProductPricingModel.product_id == product_id).filter(
                ProductPricingModel.deleted_at.is_(None)).all()

            return pricings

        except Exception as e:
            print(e)

    # Get product list category wise

    async def getProductCategoryWise(db: Session, seller_id: int, param: str):
        try:

            search = "%{}%".format(param)

            if(param != 'search'):
                return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(CategoriesModel, CategoriesModel.id == ProductModel.category).filter(UserModel.status == 1).filter(ProductModel.userid == seller_id).filter(ProductModel.userid == seller_id).filter(ProductModel.status != 98).filter().filter(CategoriesModel.name.ilike(search)).group_by(ProductModel.category)
            else:
                return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.userid == seller_id).filter(ProductModel.userid == seller_id).filter(ProductModel.status != 98).group_by(ProductModel.category)

        except Exception as e:
            print(e)

    # Get products
    async def getSellerAllProducts(db: Session, seller_id: int, search: str = '', from_date: str = '', to_date: str = ''):
        try:

            if(from_date == '' and search == '' and to_date == ''):

                return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).filter(ProductModel.userid == seller_id).filter(ProductModel.status != 98).order_by(desc(ProductModel.id))
            elif(from_date != '' and to_date != '' and search == ''):
                return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).filter(ProductModel.userid == seller_id).filter(ProductModel.status != 98).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= to_date).order_by(desc(ProductModel.id))

            elif(from_date != '' and to_date != '' and search != ''):
                search = "%{}%".format(search)
                return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).filter(ProductModel.userid == seller_id).filter(ProductModel.status != 98).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= to_date).filter(ProductModel.title.ilike(search)).order_by(desc(ProductModel.id))
            else:
                search = "%{}%".format(search)
                return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).filter(ProductModel.userid == seller_id).filter(ProductModel.status != 98).filter(ProductModel.title.ilike(search)).order_by(desc(ProductModel.id))
        except Exception as e:
            print(e)

    async def getProductList(db: Session, category_id: int, seller_id: int, status: str, search: str = '', from_date: str = '', to_date: str = ''):
        try:

            if(from_date != '' and search != ''):

                search = "%{}%".format(search)

                if(status == 'all'):
                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductCategories, ProductCategories.product_id == ProductModel.id).filter(UserModel.status == 1).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(ProductModel.status != 98).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= to_date).filter(ProductModel.title.like(search)).order_by(desc(ProductModel.id))
                elif(status == 'outstock'):

                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(
                        ProductCategories, ProductCategories.product_id == ProductModel.id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(UserModel.status == 1).filter(InventoryModel.out_of_stock == 1).filter(ProductModel.status != 98).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= to_date).filter(ProductModel.title.like(search)).group_by(ProductPricingModel.product_id).order_by(ProductModel.id.desc())
                else:

                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductCategories, ProductCategories.product_id == ProductModel.id).filter(UserModel.status == 1).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(ProductModel.status != 98).filter(ProductModel.status == status).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= to_date).filter(ProductModel.title.like(search)).order_by(desc(ProductModel.id))

            elif(from_date == '' and search != ''):

                search = "%{}%".format(search)
                if(status == 'all'):
                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductCategories, ProductCategories.product_id == ProductModel.id).filter(UserModel.status == 1).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(ProductModel.status != 98).filter(ProductModel.title.like(search)).order_by(desc(ProductModel.id))
                elif(status == 'outstock'):

                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(
                        ProductCategories, ProductCategories.product_id == ProductModel.id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(UserModel.status == 1).filter(InventoryModel.out_of_stock == 1).filter(ProductModel.status != 98).filter(ProductModel.title.like(search)).group_by(ProductPricingModel.product_id).order_by(ProductModel.id.desc())
                else:

                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductCategories, ProductCategories.product_id == ProductModel.id).filter(UserModel.status == 1).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(ProductModel.status != 98).filter(ProductModel.status == status).filter(ProductModel.title.like(search)).order_by(desc(ProductModel.id))

            elif(from_date != '' and search == ''):
                if(status == 'all'):
                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductCategories, ProductCategories.product_id == ProductModel.id).filter(UserModel.status == 1).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(ProductModel.status != 98).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= to_date).order_by(desc(ProductModel.id))
                elif(status == 'outstock'):

                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(
                        ProductCategories, ProductCategories.product_id == ProductModel.id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(UserModel.status == 1).filter(InventoryModel.out_of_stock == 1).filter(ProductModel.status != 98).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= to_date).group_by(ProductPricingModel.product_id).order_by(ProductModel.id.desc())
                else:

                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductCategories, ProductCategories.product_id == ProductModel.id).filter(UserModel.status == 1).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(ProductModel.status != 98).filter(ProductModel.status == status).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') >= from_date).filter(func.date_format(ProductModel.created_at, '%Y-%m-%d') <= to_date).order_by(desc(ProductModel.id))

            else:

                if(status == 'all'):
                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductCategories, ProductCategories.product_id == ProductModel.id).filter(UserModel.status == 1).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(ProductModel.status != 98).order_by(desc(ProductModel.id))
                elif(status == 'outstock'):

                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(
                        ProductCategories, ProductCategories.product_id == ProductModel.id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(UserModel.status == 1).filter(InventoryModel.out_of_stock == 1).filter(ProductModel.status != 98).group_by(ProductPricingModel.product_id).order_by(ProductModel.id.desc())
                else:

                    return db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductCategories, ProductCategories.product_id == ProductModel.id).filter(UserModel.status == 1).filter(ProductModel.userid == seller_id).filter(ProductCategories.category_id == category_id).filter(ProductModel.status != 98).filter(ProductModel.status == status).order_by(desc(ProductModel.id))
        except Exception as e:
            print(e)
    # def create_product(db: Session, product: CreateProduct):
    #     db_products = ProductModel(
    #         userid=product.userid, title=product.title, slug=product.slug, category=product.category, short_description=product.short_description, status=51)
    #     db.add(db_products)
    #     db.commit()
    #     db.refresh(db_products)
    #     return db_products
    # Count Stock Products

    async def countStockProducts(db: Session, user_id: int):
        try:

            out_stock_products = db.query(ProductModel.id, InventoryModel.out_of_stock).join(ProductPricingModel, ProductPricingModel.product_id ==
                                                                                             ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.deleted_at.is_(None)).filter(ProductModel.userid == user_id).filter(InventoryModel.out_of_stock != 0).count()

            in_stock_products = db.query(ProductModel.id, InventoryModel.out_of_stock).join(ProductPricingModel, ProductPricingModel.product_id ==
                                                                                            ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.deleted_at.is_(None)).filter(ProductModel.userid == user_id).filter(InventoryModel.out_of_stock != 1).count()

            return {"total_products": in_stock_products + out_stock_products, "in_stock_products": in_stock_products, "out_stock_products": out_stock_products}

        except Exception as e:
            print(e)

    # List of Stock Products

    async def listOfStockProducts(db: Session, user_id: int, stock_type: str):
        try:

            if(stock_type == 999):
                products = db.query(ProductModel.id).join(ProductPricingModel, ProductPricingModel.product_id ==
                                                          ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.deleted_at.is_(None)).filter(ProductModel.userid == user_id).filter(ProductModel.status < 98).group_by(ProductModel.id).order_by(ProductModel.id.desc())
            elif(stock_type == 998):

                products = db.query(ProductModel.id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(
                    InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.deleted_at.is_(None)).filter(ProductModel.userid == user_id).filter(InventoryModel.out_of_stock == 0).filter(InventoryModel.stock == (ProductPricingModel.items + ProductPricingModel.items)).filter(ProductModel.status < 98).group_by(ProductModel.id).order_by(ProductModel.id.desc())

            else:
                products = db.query(ProductModel.id).join(ProductPricingModel, ProductPricingModel.product_id ==
                                                          ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(ProductPricingModel.deleted_at.is_(None)).filter(ProductModel.userid == user_id).filter(InventoryModel.out_of_stock != stock_type).filter(ProductModel.status < 98).group_by(ProductModel.id).order_by(ProductModel.id.desc())

            return products
        except Exception as e:
            print(e)

    # Low Stock Products

    async def lowStockProducts(request: Request, db: Session, page: int, limit: int):
        try:

            offset = (page - 1) * limit
            products = db.execute('select PP.id, PP.product_id from products inner join product_pricing as PP on PP.product_id = products.id inner join inventories as INV on INV.pricing_id = PP.id where products.userid =:param and products.userid is not null and (INV.stock = PP.items + PP.items) group by products.id ORDER BY products.id DESC limit :param2 offset :param3', {
                "param": 65, "param2": limit, "param3": offset
            }).all()

            if(len(products) == 0):
                return {"total_products": 0, "products": []}

            total_products = db.execute('select PP.product_id from products inner join product_pricing as PP on PP.product_id = products.id inner join inventories as INV on INV.pricing_id = PP.id where products.userid =:param and products.userid is not null and (INV.stock = PP.items + PP.items) group by products.id', {
                "param": 65
            }).all()

            productdata = []
            for product in products:
                pro = db.query(ProductModel).filter(
                    ProductModel.id == product.product_id).first()

                # image
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == pro.id).filter(
                    ProductMediaModel.deleted_at.is_(None)).first()

                # price
                price = db.query(ProductPricingModel).filter(ProductPricingModel.id == product.id).filter(
                    ProductPricingModel.deleted_at.is_(None)).first()
                if(price):
                    total_price = (price.price * price.tax) / 100 + price.price

                    # Stock
                    inventory = db.query(InventoryModel).filter(
                        InventoryModel.pricing_id == product.id).first()

                    pro = {
                        'id': pro.id,
                        'name': pro.title,
                        'image': str(request.base_url)+"statc/products/"+str(image.file_name),
                        'price': round(total_price),
                        'stock': inventory.stock,

                    }

                    productdata.append(pro)

            return {"total_products": len(total_products), "products": productdata}
        except Exception as e:
            print(e)

    # Product Images

    async def ProductImages(request: Request, db: Session, product_id: int):
        try:

            # images
            images = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == product_id).filter(
                ProductMediaModel.deleted_at.is_(None)).all()
            imagedata = []
            for image in images:
                img = image.file_path
                imagedata.append(img)
            return imagedata

        except Exception as e:
            print(e)

    async def AllProductsUserWise(db: Session, user_id: int, status: str):
        return db.query(ProductModel).filter(ProductModel.status == status).filter(
            ProductModel.userid == user_id).order_by(ProductModel.id.desc())

    async def AllUserProducts(db: Session, user_id: int):
        return db.query(ProductModel).filter(ProductModel.userid == user_id).order_by(
            ProductModel.id.desc())

    async def getSimilarProducts(product_id: int, category_id: int, db: Session):
        try:
            return db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).filter(ProductModel.category == category_id).filter(ProductModel.id != product_id).filter(ProductModel.status == 51).order_by(ProductModel.id.desc())

        except Exception as e:
            print(e)

    async def AllCategoryWiseProduct(db: Session, category_id: int, status: str):
        return db.query(ProductModel).filter(ProductModel.status == status).filter(
            ProductModel.category == category_id).order_by(ProductModel.id.desc())

    async def AllCategoryProducts(db: Session, category_id: int):
        return db.query(ProductModel).filter(ProductModel.category == category_id).order_by(
            ProductModel.id.desc())
