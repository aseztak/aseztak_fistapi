from sqlalchemy.orm.session import Session
from app.db.models.aseztak_service import AseztakServiceModel


class AsezServices:

    async def aseztak_services(commission_date: str, db: Session):
        try:
            service = db.query(AseztakServiceModel).filter(
                AseztakServiceModel.commission_date <= commission_date).order_by(AseztakServiceModel.id.desc()).first()

            return service
        except Exception as e:
            print(e)
