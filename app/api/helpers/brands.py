from datetime import date, datetime

from sqlalchemy.sql.expression import desc
from sqlalchemy import asc, func
from app.api.util.service import Services

from sqlalchemy.orm import joinedload


from sqlalchemy.orm import Session
from app.db.models.brand import BrandsModel
from app.db.models.user import UserModel


from app.resources.strings import *
from app.db.models.media import ProductMediaModel
from app.db.models.products import ProductModel, ProductPricingModel
from app.db.models.attribute_values import AttributeValueModel
from app.db.models.productattribute import ProductAttributeModel
import itertools

from app.resources.strings import *


class BrandsHelper:

    async def getBrands(db: Session, user_id: int):
        try:

            return db.query(BrandsModel, UserModel.name.label('seller')).join(UserModel, UserModel.id == BrandsModel.user_id).filter(BrandsModel.user_id == user_id).filter(BrandsModel.status == 1).order_by(BrandsModel.id.desc())

        except Exception as e:
            print(e)

    async def getBrand(db: Session, brand_id: int):
        try:
            return db.query(BrandsModel).filter(BrandsModel.id == brand_id).first()
        except Exception as e:
            print(e)

     # Get Products by brands

    async def getProductsbybrands(db: Session, brand_id: int):

        try:

            products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id ==
                                                                                                                                                                                     ProductModel.id).filter(ProductModel.brand_id == brand_id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(ProductModel.id))
            return products

        except Exception as e:
            False

     # Get Product Min/Max Price by brand

    async def productMinMaxPircebyBrand(db: Session, brand_id: int):
        try:
            today = datetime.now()
            aseztak_service = Services.aseztak_services(
                today, db=db)

            brands = db.query(BrandsModel).filter(
                BrandsModel.id == brand_id).first()

            pricing = db.query(ProductPricingModel, func.max((ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)*ProductPricingModel.tax/100+(ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)).label('max_price'), func.min((ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate /
                               100)+100)/100)*ProductPricingModel.tax/100+(ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)).label('min_price')).join(ProductModel, ProductModel.id == ProductPricingModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.brand_id == brands.id).filter(ProductModel.status == 51).first()

            return pricing

        except Exception as e:
            print(e)

    # Filter by brand
    async def filerSearchProductsbyBrands(db: Session, brand_id: int, sort: str, min_price: str, max_price: str, attributes: str):
        try:

            brands = db.query(BrandsModel).filter(
                BrandsModel.id == brand_id).first()

            today = datetime.now()
            aseztak_service = Services.aseztak_services(
                today, db=db)

            if(len(attributes) == 0):
                if(sort == 'asc'):
                    if(max_price != '0'):
                        products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(ProductModel.brand_id == brands.id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(
                            func.round((ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)*ProductPricingModel.tax/100+(ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)) >= min_price).filter(
                            func.round((ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)*ProductPricingModel.tax/100+(ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)) <= max_price).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(asc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                    else:
                        products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(
                            ProductModel.brand_id == brands.id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(asc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                elif(sort == 'desc'):
                    if(max_price != '0'):
                        products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(ProductModel.brand_id == brands.id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(
                            func.round((ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)*ProductPricingModel.tax/100+(ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)) >= min_price).filter(
                            func.round((ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)*ProductPricingModel.tax/100+(ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)) <= max_price).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                    else:
                        products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(
                            ProductModel.brand_id == brands.id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                else:
                    if(max_price != '0'):
                        products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(ProductModel.brand_id == brands.id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(
                            func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) >= min_price).filter(
                            func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price) <= max_price).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(ProductModel.id))

                    else:
                        products = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(
                            ProductModel.images), joinedload(ProductModel.product_pricing)).filter(ProductModel.brand_id == brands.id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(ProductModel.id))

                return products
            else:

                attributess = []
                for attribute in attributes:

                    attribute_value = db.query(AttributeValueModel).filter(
                        AttributeValueModel.id == attribute).first()

                    attributess.append(attribute_value.attribute_id)

                productsattributesdata = []

                attributes = tuple(attributes)

                for attribute_ids in attributess:
                    if(max_price != '0'):
                        product = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductAttributeModel, ProductAttributeModel.product_id == ProductModel.id).filter(ProductModel.brand_id == brands.id).filter(UserModel.status == 1).filter(
                            func.round((ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)*ProductPricingModel.tax/100+(ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)) >= min_price).filter(
                            func.round((ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)*ProductPricingModel.tax/100+(ProductPricingModel.price*(aseztak_service.rate+(aseztak_service.rate*aseztak_service.gst_on_rate/100)+100)/100)) <= max_price).filter(ProductModel.status == 51).filter(
                            ProductAttributeModel.attribute_value_id.in_(attributes)).filter(ProductAttributeModel.attribute_id == attribute_ids).order_by(ProductModel.id.desc()).all()

                    else:
                        product = db.query(ProductModel).join(UserModel, UserModel.id == ProductModel.userid).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductAttributeModel, ProductAttributeModel.product_id == ProductModel.id).filter(ProductModel.brand_id == brands.id).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(
                            ProductAttributeModel.attribute_value_id.in_(attributes)).filter(ProductAttributeModel.attribute_id == attribute_ids).order_by(ProductModel.id.desc()).all()

                    productsattributesdata.append(product)

                productsattributesdata = list(
                    itertools.chain(*productsattributesdata))

                productids = []
                for product in productsattributesdata:
                    productids.append(product.id)

                productids.sort()

                productsattrs = [list(v)
                                 for k, v in itertools.groupby(productids)]
                products = []
                for productsattr in productsattrs:
                    # if(len(productsattr) == len(set(attributess))):
                    products.append(productsattr[0])

                if(sort == 'desc'):
                    products = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(
                        ProductModel.id.in_(products)).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(desc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))
                elif(sort == 'asc'):
                    products = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(
                        ProductModel.id.in_(products)).filter(ProductPricingModel.deleted_at.is_(None)).group_by(ProductModel.id).order_by(asc(func.round(ProductPricingModel.price * ProductPricingModel.tax / 100 + ProductPricingModel.price)))

                else:
                    products = db.query(ProductModel).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(ProductMediaModel, ProductMediaModel.model_id == ProductModel.id).options(joinedload(ProductModel.images), joinedload(ProductModel.product_pricing)).filter(ProductModel.id.in_(products)).group_by(
                        ProductModel.id).order_by(desc(ProductModel.id))
                return products

        except Exception as e:
            print(e)
