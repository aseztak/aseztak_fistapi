from datetime import date, datetime

from sqlalchemy.sql.expression import desc


from sqlalchemy.sql.functions import func
from starlette.requests import Request

from app.db.models.carts import CartModel
from app.db.models.categories import *

from sqlalchemy.orm import Session
from app.db.models.media import ProductMediaModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.orders import OrdersModel
from app.db.models.products import InventoryModel, ProductModel, ProductPricingModel
from app.db.models.user import UserModel

from app.resources.strings import *
import json
from app.api.util.calculation import orderDiscountCalculation, productPricecalculation, checkOrderPlaceDeliveryCalculation, floatingValue, orderDeliveryCalculation
from app.db.models.carts import CartModel
from app.db.models.orderdiscount import OrderDiscountModel
from app.db.models.shippingcharge import ShippingChargeModel
from app.api.helpers.discount import Discount
from app.api.helpers.services import AsezServices
from app.api.helpers.products import ProductsHelper
from operator import or_


class CartHelper:

    # Cart data on summary page new (Rahul)
    async def cart_data_on_summary_page(db: Session, user_id: int):
        try:
            cart_data = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).filter(
                CartModel.user_id == user_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).group_by(CartModel.seller_id).all()

            return cart_data
        except Exception as e:
            print(e)
    # In stock cart items on summary page new (Rahul)

    async def in_stock_cart_items_on_summary_page(db: Session, seller_id: int, user_id: int):
        try:
            in_stock_items = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(
                CartModel.user_id == user_id).filter(CartModel.seller_id == seller_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).filter(CartModel.uuid.op('regexp')(ProductPricingModel.id)).filter(ProductPricingModel.deleted_at.is_(None)).filter(InventoryModel.out_of_stock == 0).filter(or_(CartModel.quantity <= InventoryModel.stock, InventoryModel.unlimited == 1)).group_by(CartModel.product_id).all()

            return in_stock_items
        except Exception as e:
            print(e)
    # Get single cart data new (Rahul)

    async def single_cart_data(db: Session, user_id: int, product_id: int):
        try:
            single_cart_data = db.query(CartModel).where(CartModel.product_id == product_id).where(
                CartModel.user_id == user_id).first()
            return single_cart_data
        except Exception as e:
            print(e)

    # Get Cart items by product ID new (rahul)
    async def fetch_cart_item_by_product(db: Session, user_id: int, seller_id: int, product_id: int):
        try:
            cart_item = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).filter(ProductModel.status == 51).filter(
                CartModel.user_id == user_id).filter(CartModel.seller_id == seller_id).filter(CartModel.product_id == product_id).filter(CartModel.status == 1).all()
            return cart_item
        except Exception as e:
            print(e)
    # Fetch Total Cart Items

    async def fetch_total_cart_items_data(db: Session, user_id: int):
        try:
            total_cart_items = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(
                CartModel.user_id == user_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).filter(CartModel.uuid.op('regexp')(ProductPricingModel.id)).filter(ProductPricingModel.deleted_at.is_(None)).filter(InventoryModel.out_of_stock == 0).filter(or_(CartModel.quantity <= InventoryModel.stock, InventoryModel.unlimited == 1)).all()

            return total_cart_items
        except Exception as e:
            print(e)

    def getAllCarts(db: Session, user_id: int):
        return db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(CartModel.user_id == user_id).filter(CartModel.status == 1).order_by(desc(CartModel.id))

    def getAllSaveCarts(db: Session, user_id: int):
        return db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).join(UserModel, UserModel.id == ProductModel.userid).filter(UserModel.status == 1).filter(ProductModel.status == 51).filter(CartModel.user_id == user_id).filter(CartModel.status == 0).order_by(desc(CartModel.id))

    # Add to Cart data
    async def addTocart(db: Session, data, user_id: int):
        try:
            # Delete Existing Cart Data
            db.query(CartModel).filter(
                CartModel.product_id == data.carts[0].product_id).filter(CartModel.user_id == user_id).delete()
            db.commit()

            for cart in data.carts:

                product = db.query(ProductModel).where(
                    ProductModel.id == cart.product_id).first()

                today = datetime.now()

                db_cart = CartModel(
                    user_id=user_id,
                    seller_id=product.userid,
                    product_id=cart.product_id,
                    quantity=cart.quantity,
                    price=cart.price,
                    tax=cart.tax,
                    uuid=cart.uuid,
                    hsn_code=cart.hsn_code,
                    status=1,
                    attributes=json.dumps(cart.attributes),
                    created_at=today,
                    updated_at=today
                )

                db.add(db_cart)
                db.commit()
                # db.refresh(db_cart)

            return data

        except Exception as e:
            print(e)

    # Update Cart Data
    async def updateCart(db: Session, data):
        cart = db.query(CartModel).where(CartModel.id == data.id).first()
        if(cart):

            cart.quantity = data.quantity
            db.flush()
            db.commit()
            return cart
        else:
            return False

    # Delete Cart Data

    async def deleteCartData(db: Session, data):
        cart = db.query(CartModel).where(CartModel.id == data.id).first()
        if(cart):
            db.delete(cart)
            db.commit()
            return True
        else:
            return False

    async def deleteCartfromCheckout(db: Session, user_id: int, product_id: int):
        cart = db.query(CartModel).where(CartModel.user_id == user_id).where(
            CartModel.product_id == product_id).all()
        if(len(cart) > 0):
            for c in cart:
                db.delete(c)
                db.commit()
            return True
        else:
            return False

    # Product Details

    async def productTitle(request: Request, db: Session, data: CartModel) -> CartModel:
        try:
            for i in data:

                product = db.query(ProductModel).filter(
                    ProductModel.id == i.product_id).first()

                product_price_id = i.uuid.split('-')

                product_price = db.query(ProductPricingModel).filter(ProductPricingModel.id == product_price_id[1]).filter(
                    ProductPricingModel.deleted_at.is_(None)).first()
                if(product_price is not None):
                    if(product_price.default_image != 0):
                        image = db.query(ProductMediaModel).filter(ProductMediaModel.id == product_price.default_image).filter(
                            ProductMediaModel.deleted_at.is_(None)).first()

                        if(image is None):
                            image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == i.product_id).filter(
                                ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()
                    else:
                        image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == i.product_id).filter(
                            ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                    if(image is not None):
                        img = image.file_path
                    else:
                        img = ''
                    i.product_title = product.title
                    i.product_description = product.short_description

                    i.image = img

            return data

        except Exception as e:
            print(e)

    # Cart Summary

    async def getCartSummary(db: Session, user_id: int):

        carts = db.query(func.count(CartModel.seller_id), CartModel.seller_id, CartModel.user_id).join(ProductModel, ProductModel.id ==
                                                                                                       CartModel.product_id).where(ProductModel.status == 51).where(CartModel.user_id == user_id).group_by(CartModel.seller_id).all()

        cartdata = []

        for cart in carts:
            checkcart = db.query(func.count(CartModel.product_id), CartModel.product_id, CartModel.user_id, CartModel.seller_id).join(ProductModel, ProductModel.id ==
                                                                                                                                      CartModel.product_id).where(ProductModel.status == 51).where(CartModel.user_id == user_id).where(CartModel.seller_id == cart.seller_id).group_by(CartModel.product_id).all()

            cartdata.append(checkcart)

        return cartdata

    # Cart Items

    # async def getCartItems(request: Request, db: Session, data: CartModel) -> CartModel:

    #     cartitems = []
    #     total_amount = 0
    #     total_delivery = 0
    #     offerTxt = ''

    #     if(len(data) == 0):
    #         return {'status_code': HTTP_404_NOT_FOUND}

    #     for cart in data:

    #         sub_total = 0
    #         product_data = []
    #         for item in cart:

    #             # Get cart raw data
    #             cartdata = db.query(CartModel).where(CartModel.product_id == item.product_id).where(
    #                 CartModel.user_id == item.user_id).first()

    #             # Get Seller Details
    #             seller = db.query(UserModel).where(
    #                 UserModel.id == item.seller_id).first()

    #             # Get Product Details
    #             product = db.query(ProductModel).where(
    #                 ProductModel.id == item.product_id).where(ProductModel.status == 51).first()

    #             # Get cart items by product id
    #             itemsdata = db.query(CartModel).join(
    #                 ProductModel, ProductModel.id == CartModel.product_id).where(CartModel.user_id == item.user_id).where(CartModel.product_id == item.product_id).all()

    #             # Check Pricing for stock maintain
    #             itm_pricing_id = cartdata.uuid.split('-')
    #             itm_pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == itm_pricing_id[1]).where(
    #                 ProductPricingModel.deleted_at.is_(None)).first()

    #             if(itm_pricing):
    #                 # checking out of stock
    #                 checkinventory = db.query(InventoryModel).where(
    #                     InventoryModel.pricing_id == itm_pricing.id).first()

    #                 if(checkinventory.out_of_stock == 0):

    #                     total_items = []
    #                     order_total_amount = 0
    #                     for items in itemsdata:
    #                         # Check Pricing for stock maintain
    #                         item_pricing_id = items.uuid.split('-')
    #                         item_pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == item_pricing_id[1]).where(
    #                             ProductPricingModel.deleted_at.is_(None)).first()

    #                         if(item_pricing):
    #                             # checking out of stock
    #                             checkiteminventory = db.query(InventoryModel).where(
    #                                 InventoryModel.pricing_id == item_pricing.id).first()

    #                             if(checkiteminventory.out_of_stock == 0):
    #                                 # Aseztak Service
    #                                 aseztak_service = Services.aseztak_services(
    #                                     item_pricing.updated_at, db=db)

    #                                 product_price = productPricecalculation(price=item_pricing.price, tax=item_pricing.tax, commission=aseztak_service.rate,
    #                                                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

    #                                 # Sub Total
    #                                 totalprice = round(
    #                                     product_price, 2) * items.quantity

    #                                 sub_total += totalprice

    #                                 order_total_amount += totalprice
    #                                 total_items.append(items)

    #                                 if(item_pricing.default_image != 0):
    #                                     image = db.query(ProductMediaModel).filter(ProductMediaModel.id == item_pricing.default_image).filter(
    #                                         ProductMediaModel.deleted_at.is_(None)).first()
    #                                 else:
    #                                     image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == items.product_id).filter(ProductMediaModel.default_img == 1).filter(
    #                                         ProductMediaModel.deleted_at.is_(None)).first()

    #                                 if(image == None):
    #                                     image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == items.product_id).filter(
    #                                         ProductMediaModel.deleted_at.is_(None)).first()

    #                             img = image.file_path

    #                     p_data = {
    #                         'image': img,
    #                         'id': product.id,
    #                         'title': product.title,
    #                         'slug': product.slug,
    #                         'items': len(total_items),
    #                         'amount': floatingValue(round(order_total_amount, 2))
    #                     }

    #                     product_data.append(p_data)
    #             seller = {
    #                 'id': seller.id,
    #                 'name': seller.name,
    #                 'address': seller.city+', '+seller.region
    #             }

    #         if(len(product_data) > 0):
    #             # Check Delivery Charge
    #             delivery_charge = 0
    #             min_order_value = 0
    #             order_checkout_text = PRODUCT_FREE_DELIVERY_TEXT
    #             free_delivery = True

    #             today = date.today()
    #             # YY-mm-dd
    #             today = today.strftime("%Y-%m-%d")

    #             # Check Shipping Charge

    #             shipping_charge = db.execute("SELECT id, rate, start_date, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
    #                                          {"param": today}).first()

    #             if (shipping_charge.order_limit != 0):
    #                 if (round(sub_total, 2) < round(shipping_charge.order_limit, 2)):
    #                     delivery_charge = shipping_charge.rate
    #                     min_order_value = shipping_charge.order_limit
    #                     order_checkout_text = "Make this order above " + \
    #                         str(min_order_value)+" for Free Delivery"
    #                     free_delivery = False

    #             else:
    #                 delivery_charge = shipping_charge.rate
    #                 order_checkout_text = ''
    #                 free_delivery = False

    #             total_amount += round(sub_total, 2)
    #             total_delivery += delivery_charge

    #             # Grand Total
    #             grandtotal = total_amount + float(total_delivery)

    #             # Cart Items
    #             cartitems.append(
    #                 {'seller': seller, 'product_data': product_data, 'total_amount': floatingValue(round(sub_total, 2)), 'delivery_charge': floatingValue(delivery_charge), 'min_order_value': floatingValue(min_order_value), 'order_checkout_text': order_checkout_text, 'free_delivery': free_delivery})

    #             count_total_items = []
    #             total_cart_items = db.query(CartModel).filter(
    #                 CartModel.user_id == item.user_id).all()
    #             for cart_item in total_cart_items:
    #                 itmpricingid = cart_item.uuid.split('-')
    #                 itmpricing = db.query(ProductPricingModel).where(ProductPricingModel.id == itmpricingid[1]).where(
    #                     ProductPricingModel.deleted_at.is_(None)).first()
    #                 if(itmpricing is not None):
    #                     cartcheckinventory = db.query(InventoryModel).filter(
    #                         InventoryModel.pricing_id == itmpricing.id).first()

    #                     if(cartcheckinventory.out_of_stock == 0):
    #                         count_total_items.append(cart_item)

    #             # Order Summary Text
    #             order_summary = [
    #                 {
    #                     "name": "Items",
    #                     "value": str(len(count_total_items)),
    #                     "currency": False,
    #                     "visible": True,
    #                     "offer_text": ""
    #                 },
    #                 {
    #                     "name": "Total Amount",
    #                     "value": floatingValue(round(total_amount, 2)),
    #                     "currency": True,
    #                     "visible": True,
    #                     "offer_text": ""
    #                 },
    #                 {
    #                     "name": "Shipping Charge ",
    #                     "value": floatingValue(total_delivery),
    #                     "currency": True,
    #                     "visisble": True,
    #                     "offer_text": ""
    #                 }

    #             ]

    #             # Grand Total
    #             rounded_value = round(grandtotal, 0) - grandtotal
    #             grand_total = {
    #                 "title": "Grand Total",
    #                 "payment_method": 'COD',
    #                 "discount_rate": floatingValue(0.00),
    #                 "discount_amount": floatingValue(0.00),
    #                 "round_off": floatingValue(round(rounded_value, 2)),
    #                 "amount": floatingValue(round(grandtotal)),
    #                 "partial": floatingValue(0.00),
    #             }

    #             # Shipping Address
    #             shipping_addresses = db.query(ShippingAddressModel).where(
    #                 ShippingAddressModel.user_id == item.user_id).order_by(desc(ShippingAddressModel.default_address)).all()

    #             for address in shipping_addresses:
    #                 alter_phone = ''
    #                 if(address.alt_phone == None):
    #                     address.alt_phone = alter_phone

    #                 default_address = False
    #                 if(address.default_address == 1):
    #                     default_address = True

    #                 address.default_address = default_address

    #             # Payment Method
    #             payment_method = [
    #                 {
    #                     'value': 'ONLINE',
    #                     'title': 'Pay Online',
    #                     'body': 'Use Netbanking, Debit Card or Credit Card'
    #                 },
    #                 {
    #                     'value': 'COD',
    #                     'title': 'Cash on Delivery',
    #                     'body': 'Pay by Cash when your product arrives'
    #                 },
    #                 {
    #                     'value': 'PARTIAL',
    #                     'title': 'Partial Payment',
    #                     'body': 'Pay Online 2400 for now'
    #                 },
    #             ]

    #             payment_methods = []
    #             for payment in payment_method:
    #                 title = payment['title']
    #                 body = payment['body']

    #                 # CHECK ORDER DISCOUNT
    #                 checkoffer = db.execute("SELECT id, discount, start_date, payment_method FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
    #                                         {"param": today}).first()

    #                 offer = ''
    #                 discount_text = ''
    #                 discount = 0
    #                 if(checkoffer.discount != 0 and checkoffer.payment_method == payment['value']):

    #                     offer = ""

    #                     fractional, whole = math.modf(checkoffer.discount)
    #                     if(fractional == 0):
    #                         discount = round(checkoffer.discount)
    #                     else:
    #                         discount = round(checkoffer.discount, 2)

    #                     discount_text = str(discount) + \
    #                         "% Extra Discount on Online Payment"

    #                 visible = True
    #                 disabled = False

    #                 if(payment['value'] == 'PARTIAL'):
    #                     discount = 0

    #                 if(payment['value'] == 'COD'):

    #                     discount = 0
    #                     facility = db.query(UserFacilityModel).where(
    #                         UserFacilityModel.key_name == 'cod').where(UserFacilityModel.user_id == item.user_id).first()

    #                     if(facility):
    #                         facility_date = facility.updated_at.strftime(
    #                             "%Y-%m-%d")
    #                         orders = db.execute(
    #                             'SELECT id, created_at, updated_at FROM orders WHERE DATE_FORMAT(created_at, " %Y-%m-%d") >=' +
    #                             facility_date+' and payment_method = "COD" and user_id = ' +
    #                             str(item.user_id)+''
    #                         ).all()

    #                         previews_total_amount = 0

    #                         for order in orders:
    #                             previews_total_amount += OrderCalculation.calculateTotalAmount(db=db,
    #                                                                                            orderID=order.id)

    #                         previews_total_amount = round(
    #                             previews_total_amount, 0)

    #                         if(previews_total_amount > int(facility.key_value)):

    #                             restAmount = (
    #                                 previews_total_amount - int(facility.key_value))
    #                         else:

    #                             restAmount = (
    #                                 int(facility.key_value) - previews_total_amount)

    #                             restAmount = round(restAmount, 0)

    #                         if (float(round(total_amount, 2)) > float(restAmount)):
    #                             disabled = True
    #                             title = "Remaining COD limit"
    #                             body = '₹' + str(restAmount) + \
    #                                 " out of ₹" + \
    #                                 str(facility.key_value)

    #                 pmethods = {
    #                     'value': payment['value'],
    #                     'title': title,
    #                     'description': body,
    #                     'visible': visible,
    #                     'disabled': disabled,
    #                     'discount': discount,
    #                     'discount_text': discount_text,
    #                     'offer': offer
    #                 }

    #                 payment_methods.append(pmethods)

    #             # User Document Type
    #             user_document_type = []
    #             documents = [{
    #                 'name': 'GSTIN',
    #                 'value': '',
    #                 'selected': False
    #             },
    #                 {
    #                 'name': 'AADHAAR',
    #                 'value': '',
    #                 'selected': False
    #             },
    #                 {
    #                 'name': 'PAN',
    #                 'value': '',
    #                 'selected': False
    #             }]

    #             for document in documents:

    #                 checkdocument = db.query(UserProfileModel).where(
    #                     UserProfileModel.user_id == item.user_id).first()
    #                 selected = False
    #                 value = ''
    #                 if(checkdocument and checkdocument.proof_type == document['name']):
    #                     selected = True
    #                     value = checkdocument.proof

    #                 doc = {
    #                     'name': document['name'],
    #                     'value': value,
    #                     'selected': selected
    #                 }

    #                 user_document_type.append(doc)

    #     return {'status_code': HTTP_200_OK, 'offer_text': offerTxt, 'shipping_address': shipping_addresses, 'user_documents': user_document_type,  'payment_method': payment_methods, 'order_summary': order_summary, "grand_total": grand_total, 'item_data': cartitems}

    # Order Summary (RAHUL)
    # async def orderSummary(request: Request, db: Session, data: CartModel, payment_method: str) -> CartModel:

    #     cartitems = []
    #     total_amount = 0
    #     total_delivery = 0
    #     grand_total = 0.00
    #     if(payment_method.lower() == 'online'):
    #         title = 'Online Payment'
    #     else:
    #         title = 'Partial Payment'

    #     if(len(data) == 0):
    #         return {'status_code': HTTP_200_OK, "message": "No data found", "title": title, 'order_summary_text': [], "grand_total": {}}

    #     total_items = []
    #     for cart in data:

    #         for item in cart:

    #             # Get cart raw data
    #             cartdata = db.query(CartModel).where(CartModel.product_id == item.product_id).where(
    #                 CartModel.user_id == item.user_id).first()

    #             # Get cart items by product id
    #             itemsdata = db.query(CartModel).join(
    #                 ProductModel, ProductModel.id == CartModel.product_id).where(CartModel.user_id == item.user_id).where(CartModel.product_id == item.product_id).all()

    #             # Check Pricing for stock maintain
    #             itm_pricing_id = cartdata.uuid.split('-')
    #             itm_pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == itm_pricing_id[1]).where(
    #                 ProductPricingModel.deleted_at.is_(None)).first()

    #             if(itm_pricing):
    #                 # checking out of stock
    #                 checkinventory = db.query(InventoryModel).where(
    #                     InventoryModel.pricing_id == itm_pricing.id).first()

    #                 if(checkinventory.out_of_stock == 0):
    #                     sub_total = 0
    #                     for items in itemsdata:
    #                         # Check Pricing for stock maintain
    #                         item_pricing_id = items.uuid.split('-')
    #                         item_pricing = db.query(ProductPricingModel).where(ProductPricingModel.id == item_pricing_id[1]).where(
    #                             ProductPricingModel.deleted_at.is_(None)).first()

    #                         if(item_pricing):
    #                             # checking out of stock
    #                             checkiteminventory = db.query(InventoryModel).where(
    #                                 InventoryModel.pricing_id == item_pricing.id).first()

    #                             if(checkiteminventory.out_of_stock == 0):
    #                                 # Aseztak Service
    #                                 aseztak_service = Services.aseztak_services(
    #                                     item_pricing.updated_at, db=db)

    #                                 product_price = productPricecalculation(price=item_pricing.price, tax=item_pricing.tax, commission=aseztak_service.rate,
    #                                                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

    #                                 # Sub Total
    #                                 totalprice = round(
    #                                     product_price, 2) * items.quantity

    #                                 sub_total += totalprice
    #                                 total_items.append(items)

    #                     # Check Delivery Charge
    #                     today = date.today()
    #                     # YY-mm-dd
    #                     today = today.strftime("%Y-%m-%d")

    #                     # Check Shipping Charge

    #                     shipping_charge = db.execute("SELECT id, rate, start_date, payment_mode, order_limit FROM shipping_charge WHERE start_date=(SELECT max(start_date) FROM shipping_charge where start_date <=:param)",
    #                                                  {"param": today}).first()

    #                     delivery_charge = checkOrderPlaceDeliveryCalculation(
    #                         order_amount=sub_total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode='ONLINE')

    #                     total_amount += round(sub_total, 2)

    #                     total_delivery += delivery_charge

    #     # CHECK ORDER DISCOUNT
    #     checkdiscountoffer = db.execute("SELECT id, discount, start_date, payment_method FROM order_discount WHERE start_date=(SELECT max(start_date) FROM order_discount where start_date <=:param)",
    #                                     {"param": today}).first()

    #     discountamount = 0.00
    #     discountrate = 0.00
    #     offerTxt = ""

    #     if(checkdiscountoffer and checkdiscountoffer.discount != 0):
    #         dis = (float(round(total_amount, 0)) *
    #                float(checkdiscountoffer.discount) / 100)
    #         discountamount += dis

    #         discountrate = checkdiscountoffer.discount

    #         offerTxt = 'Get ₹'+str(discountamount)+' instant discount'

    #     grand_totla = round(total_amount, 2) - \
    #         round(discountamount, 2)

    #     if(payment_method.lower() == 'online'):

    #         order_summary = [
    #             {
    #                 "name": "Items",
    #                 "value": str(len(total_items)),
    #                 "currency": False,
    #                 "visible": True,
    #                 "offer_text": ""
    #             },
    #             {
    #                 "name": "Total Amount",
    #                 "value": floatingValue(round(total_amount, 2)),
    #                 "currency": True,
    #                 "visible": True,
    #                 "offer_text": ""
    #             },
    #             {
    #                 "name": "Discount",
    #                 "value": floatingValue(round(discountamount, 2)),
    #                 "currency": True,
    #                 "visible": True,
    #                 "offer_text": offerTxt
    #             },
    #             {
    #                 "name": "Shipping Charge ",
    #                 "value": floatingValue(total_delivery),
    #                 "currency": True,
    #                 "visible": True,
    #                 "offer_text": ""
    #             }
    #         ]
    #     else:
    #         order_summary = [
    #             {
    #                 "name": "Items",
    #                 "value": floatingValue(len(cartitems)),
    #                 "currency": False,
    #                 "visible": True,
    #                 "offer_text": ""

    #             },
    #             {
    #                 "name": "Total Amount",
    #                 "value": floatingValue(round(total_amount, 0)),
    #                 "currency": True,
    #                 "visible": True,
    #                 "offer_text": ""
    #             },
    #             {
    #                 "name": "Discount",
    #                 "value": floatingValue(round(discountamount)),
    #                 "currency": True,
    #                 "visible": False,
    #                 "offer_text": offerTxt
    #             },
    #             {
    #                 "name": "Shipping Charge ",
    #                 "value": floatingValue(total_delivery),
    #                 "currency": True,
    #                 "visible": True,
    #                 "offer_text": ""
    #             },
    #             {
    #                 "name": "Payable Amount",
    #                 "value": floatingValue(round(total_amount, 0)),
    #                 "currency": True,
    #                 "visible": True,
    #                 "offer_text": ""
    #             }

    #         ]

    #     # Grand Total
    #     grand_total = float(round(grand_totla, 2)) + \
    #         float(round(total_delivery, 0))
    #     rounded_value = round(grand_total, 0) - grand_total

    #     grand_total = {
    #         "title": "Grand Total",
    #         "payment_method": payment_method.upper(),
    #         "discount_rate": floatingValue(discountrate),
    #         "discount_amount": floatingValue(discountamount),
    #         "round_off": floatingValue(round(rounded_value, 2)),
    #         "amount": floatingValue(round(grand_total)),
    #         "partial": floatingValue(0.00),

    #     }

    #     return {'status_code': HTTP_200_OK, "message": "Success", "title": title, 'order_summary': order_summary, "grand_total": grand_total}


class OrderCalculation:

    async def calculateTotalAmount(db: Session, orderID: int):

        # Order
        order = db.query(OrdersModel).where(
            OrdersModel.id == orderID).first()

        date = order.created_at.strftime(
            "%Y-%m-%d")
        # Items
        items = db.query(OrderItemsModel).where(OrderItemsModel.order_id == orderID).where(
            OrderItemsModel.status != 980).all()

        # Calculate Total Price
        total_amount = 0
        for item in items:
            pricingdata = item.uuid.split('-')
            pricingdata = db.query(ProductPricingModel).filter(
                ProductPricingModel.id == pricingdata[1]).first()
            # Product Object
            productdata = db.query(ProductModel).filter(
                ProductModel.id == item.product_id).first()

            # aseztak_service = Services.aseztak_services(
            #     item.created_at, db=db)
            today_date = item.created_at.strftime('%Y-%m-%d')
            aseztak_service = await AsezServices.aseztak_services(commission_date=today_date, db=db)
            if(aseztak_service is None):

                # Calculate Product Price
                product_price = productPricecalculation(price=item.price, tax=item.tax, commission=0,
                                                        gst_on_commission=0, tds=0, tcs=0, round_off=0, app_version=order.app_version)
            else:
                product_price: ProductModel = await ProductsHelper.getPrice(db, productdata, pricingdata, asez_service=aseztak_service, app_version=order.app_version, order_item_id=item.id)
                # product_price = productPricecalculation(price=item.price, tax=item.tax, commission=aseztak_service.rate,
                #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off, app_version=order.app_version)

            total_amount += product_price * item.quantity

        total_amount = round(total_amount, 0)

        discount_amount = 0

        if(order.discount != 0):
            # CHECK ORDER DISCOUNT
            order_discount = db.query(OrderDiscountModel).filter(
                OrderDiscountModel.start_date <= date).order_by(OrderDiscountModel.id.desc()).first()

            discount_rate = order_discount.discount
            if(order.discount_rate != 0):
                discount_rate = order.discount_rate

            discount_amount = orderDiscountCalculation(app_version=order.app_version,
                                                       order_amount=total_amount, discount_amount=order.discount, discount_rate=discount_rate)

        # Check Shipping Charge
        # delivery_charge = 0
        # if(order.delivery_charge != 0):
        shipping_charge = db.query(ShippingChargeModel).filter(
            ShippingChargeModel.start_date <= date).order_by(ShippingChargeModel.id.desc()).first()

        delivery_charge = orderDeliveryCalculation(db=db, free_delivery=order.free_delivery, user_id=order.user_id, order_date=order.created_at,
                                                   app_version=order.app_version, order_amount=total_amount, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=order.payment_method)

        final_amount = ((total_amount +
                        delivery_charge) - discount_amount)

        return final_amount
