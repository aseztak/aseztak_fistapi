from app.resources.strings import *

import boto3
client = boto3.client("s3", **linode_obj_config)


class MediaHelper:

    def uploadPhoto(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='products',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    def uploadCategoryPhoto(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='categories',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    def uploadBrandPhoto(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='brands',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    def uploadBannersPhoto(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='banners',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    def uploadWidgetsPhoto(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='widgets',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    # courier invoice pdf file upload
    def uploadSlipPdf(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='slip',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    def uploadInvoicePdf(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='invoices',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    def uploadFulfillmentInvoicePdf(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='fulfillment_invoices',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    # Delete File

    def deletePhoto(bucket_name, file_name):
        client.delete_object(
            Bucket=bucket_name,
            Key=file_name)

    def uploadAccountTransaction(uploaded_file, file_name):
        client.upload_file(
            Filename=uploaded_file,
            Bucket='transaction',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    # Upload Seller Invoice List
    def uploadSellerInvoiceList(uploaded_file, file_name):
        client.upload_file(
            Filename=uploaded_file,
            Bucket='seller_invoice_list',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    def deleteFile(bucket_name, file_name):
        client.delete_object(
            Bucket=bucket_name,
            Key=file_name)

    # Locked payments upload csv
    def uploadLockedPaymentsCsv(uploaded_file, file_name):
        client.upload_file(
            Filename=uploaded_file,
            Bucket='locked_payments',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    # Paid payments upload csv
    def uploadPaidPaymentsCsv(uploaded_file, file_name):
        client.upload_file(
            Filename=uploaded_file,
            Bucket='paid_payments',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    # Update Seller Commission PDF
    def uploadCommissionPdf(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='commission',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    # Paid payments upload csv
    def uploadBuyerPaymentsCsv(uploaded_file, file_name):
        client.upload_file(
            Filename=uploaded_file,
            Bucket='buyer_payments',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    def uploadOrderReturnProof(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='proof',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    def uploadUserDocument(uploaded_file, file_name):

        client.upload_file(
            Filename=uploaded_file,
            Bucket='userdocument',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    # Upload Notification IMAGE
    def uploadNotificationImage(uploaded_file, file_name):
        client.upload_file(
            Filename=uploaded_file,
            Bucket='notification',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    # Upload Image Packed (Orders)
    def uploadImagePacked(uploaded_file, file_name):
        client.upload_file(
            Filename=uploaded_file,
            Bucket='proof',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})

    # Buyer List upload csv
    def uploadBuyerListCsv(uploaded_file, file_name):
        client.upload_file(
            Filename=uploaded_file,
            Bucket='buyer_list',
            Key=file_name,
            ExtraArgs={'ACL': 'public-read'})
