from app.db.models.widgets import WidgetsModel
from app.db.models.widget_images import WidgetImagesModel
from sqlalchemy.orm import Session
from sqlalchemy.orm import joinedload


class WidgetsHelper:
    async def get_widgets(db: Session):
        data = db.query(WidgetsModel).order_by(WidgetsModel.id.asc()).all()

        table = []
        for wdgt in data:
            image = db.query(WidgetImagesModel).filter(
                WidgetImagesModel.widget_id == wdgt.id).all()

            imgs = []
            for images in image:
                img = {
                    'image': images.image
                }
                imgs.append(img)

            wdt = {
                'id': wdgt.id,
                'title': wdgt.title,
                'description': wdgt.description,
                'no_of_images': wdgt.no_of_images,
                'link': wdgt.link,
                'images': imgs,

            }
            table.append(wdt)

        return table

    async def get_home_widgets(db: Session):
        return db.query(WidgetsModel).join(WidgetImagesModel, WidgetImagesModel.widget_id == WidgetsModel.id).options(joinedload(WidgetsModel.widget_images)).filter(
            WidgetsModel.widget_type == 'home').filter(WidgetsModel.slno != 0).filter(WidgetsModel.enabled == True).order_by(WidgetsModel.slno.asc()).all()

     # NEW CHANGES RAHUL
    async def get_home_new_widgets(db: Session, param: str = ''):
        if(param == 'HOME'):
            return db.query(WidgetsModel).join(WidgetImagesModel, WidgetImagesModel.widget_id == WidgetsModel.id).options(joinedload(WidgetsModel.widget_images)).filter(
                WidgetsModel.widget_type != 'INNER').filter(WidgetsModel.slno != 0).filter(WidgetsModel.enabled == True).order_by(WidgetsModel.slno.asc()).all()

        else:
            return db.query(WidgetsModel).join(WidgetImagesModel, WidgetImagesModel.widget_id == WidgetsModel.id).options(joinedload(WidgetsModel.widget_images)).filter(
                WidgetsModel.widget_type == param).filter(WidgetsModel.slno != 0).filter(WidgetsModel.enabled == True).order_by(WidgetsModel.slno.asc()).all()
