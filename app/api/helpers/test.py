
from sqlalchemy.orm.session import Session
from sqlalchemy.sql.expression import false
from app.db.models.test import TestModel
from app.db.schemas.test_schema import TestSchema


def create_test(db: Session, test: TestSchema):

    try:
        db_test = TestModel(
            name=test.name,
            mobile=test.mobile,
            status=test.status
        )

        db.add(db_test)
        db.commit()
        db.refresh(db_test)

        return db_test

    except:
        db.rollback
