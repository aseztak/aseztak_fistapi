
from sqlalchemy.sql.expression import desc
from starlette.requests import Request
from app.db.models.categories import CategoriesModel, ProductCategories
from sqlalchemy.orm import Session

from app.db.models.products import ProductModel


class CategoriesHelper:

    # @staticmethod
    def get_categories(db: Session, parent_id: int = 1):

        # return db.query(CategoriesModel).filter_by(parent_id=parent_id).all()
        return db.query(CategoriesModel).join(ProductCategories, ProductCategories.category_id == CategoriesModel.id).join(ProductModel, ProductModel.id == ProductCategories.product_id).filter(ProductModel.status == 51).filter(CategoriesModel.parent_id == parent_id).filter(CategoriesModel.status == 1).all()

    def get_category(db: Session, id: int):
        return db.query(CategoriesModel).filter(CategoriesModel.id == id).one()

    # Check Child Categories

    async def hasChildCategories(request: Request, db: Session, data: CategoriesModel) -> CategoriesModel:
        for i in data:
            category = db.query(CategoriesModel).where(
                CategoriesModel.parent_id == i.id).join(ProductCategories, ProductCategories.category_id == CategoriesModel.id).join(ProductModel, ProductModel.id == ProductCategories.product_id).filter(ProductModel.status == 51).where(CategoriesModel.status == 1).first()

            if(i.image == None):
                image = ''
            else:

                image = i.image_path

            i.image = image
            if(category):
                i.haschild = True

        return data

    # Get Selectabale Categories
    async def selectableCategories(db: Session, search: str):
        if(search.lower() == 'search'):
            return db.query(CategoriesModel).filter(CategoriesModel.selectable == 1).filter(CategoriesModel.status == 1).order_by(desc(CategoriesModel.id))
        else:
            search = "%{}%".format(search)
            return db.query(CategoriesModel).filter(CategoriesModel.selectable == 1).filter(CategoriesModel.status == 1).order_by(desc(CategoriesModel.id)).filter(CategoriesModel.name.ilike(search))

    def v4_get_categories(db: Session, search: str, parent_id: int = 1):
        search = search.replace("+", "%")
        search = search.rstrip()
        search = "%{}%".format(search)
        if(parent_id == 'parent_id' and search != 'search'):
            return db.query(CategoriesModel).join(ProductCategories, ProductCategories.category_id == CategoriesModel.id).join(ProductModel, ProductModel.id == ProductCategories.product_id).filter(ProductModel.status == 51).filter(CategoriesModel.parent_id == parent_id).filter(CategoriesModel.status == 1).all()
        else:
            # return db.query(CategoriesModel).filter_by(parent_id=parent_id).all()
            return db.query(CategoriesModel).join(ProductCategories, ProductCategories.category_id == CategoriesModel.id).join(ProductModel, ProductModel.id == ProductCategories.product_id).filter(ProductModel.status == 51).filter(CategoriesModel.parent_id == parent_id).filter(
                CategoriesModel.name.like(search)).filter(CategoriesModel.status == 1).all()

    # For New Home (rahul)
    def get_parent_categories(db: Session, param: str = 'all'):
        parent_category = db.query(CategoriesModel).filter(
            CategoriesModel.parent_id == 0).filter(CategoriesModel.name.like('%'+str(param)+'%')).filter(CategoriesModel.status == 1).first()

        parent_categories = db.query(CategoriesModel).join(ProductCategories, ProductCategories.category_id == CategoriesModel.id).join(
            ProductModel, ProductModel.id == ProductCategories.product_id).filter(CategoriesModel.status == 1).filter(CategoriesModel.parent_id == parent_category.id).filter(ProductModel.status == 51).all()

        return parent_categories

    # For New Home (rahul)

    def get_parent_has_child(db: Session, parent_id: int):
        child_category = db.query(CategoriesModel).join(ProductCategories, ProductCategories.category_id == CategoriesModel.id).join(
            ProductModel, ProductModel.id == ProductCategories.product_id).filter(CategoriesModel.status == 1).filter(CategoriesModel.parent_id == parent_id).filter(ProductModel.status == 51).count()

        has_child = True
        if (child_category == 0):
            has_child = False

        return has_child
