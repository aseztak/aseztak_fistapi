

from sqlalchemy.orm import Session
from sqlalchemy.sql.functions import random
from app.db.models.user import UserModel, UserProfileModel
from app.resources.strings import *
from app.api.helpers.auth import *
import requests
# @staticmethod

# Get Seller Data


async def fetch_seller_data(db: Session, seller_id: int):
    try:
        seller = db.query(UserModel).filter(
            UserModel.id == seller_id).first()
        return seller
    except Exception as e:
        print(e)

# Fetch Buyer Profile Data


async def fetch_user_profile_data(db: Session, user_id: int):
    try:
        checkdocument = db.query(UserProfileModel).where(
            UserProfileModel.user_id == user_id).first()

        return checkdocument
    except Exception as e:
        print(e)


def get_users(db: Session, skip: int = 0, limit: int = 100):

    return db.query(UserModel).offset(skip).limit(limit).all()


# @staticmethod
async def me(db: Session, mobile: str):

    query = db.query(UserModel).where(UserModel.mobile == mobile).first()

    if query:
        return query


async def userExist(db: Session, mobile: str):

    query = db.query(UserModel).where(UserModel.mobile == mobile).first()

    if query:
        return True
    else:
        return False

# Update Profile (Seller)


async def sellerProfileUpdate(db: Session, data, user_id: int):

    try:
        user = db.query(UserModel).where(UserModel.id ==
                                         user_id).first()

        if(user):
            # Update user data
            user.name = data.name
            user.email = data.email
            user.country = data.country
            user.region = data.region
            user.city = data.city
            user.pincode = data.pincode
            db.flush()
            db.commit()

        # Update User Profile Data
            profile = db.query(UserProfileModel).where(
                UserProfileModel.user_id == user_id).first()

            if(profile):
                profile.business_type = data.business_type
                profile.proof = data.proof
                profile.proof_type = "GSTIN"
                db.flush()
                db.commit()
            else:

                profile = UserProfileModel(
                    user_id=user_id, business_type=data.business_type, proof=data.proof, proof_type='GSTIN')

                db.add(profile)
                db.commit()

            userdata = {
                "name": user.name,
                "email": user.email,
                "mobile": user.mobile,
                "country": user.country,
                "region": user.region,
                "pincode": user.pincode,
                "city": user.city,
                "business_type": profile.business_type,
                "proof_type": profile.proof_type,
                "proof": profile.proof,
                "status": user.status

            }
            return userdata

        return False
    except Exception as e:
        print(e)

# Change Mobile Number (Seller)


async def SellerChangeMobileNumber(db: Session, data, mobile: str):
    try:
        user = db.query(UserModel).where(UserModel.mobile ==
                                         data.mobile).first()

        if(user):

            profile = db.query(UserProfileModel).where(
                UserProfileModel.user_id == user.id).first()

            userdata = {
                "name": user.name,
                "email": user.email,
                "mobile": user.mobile,
                "country": user.country,
                "region": user.region,
                "pincode": user.pincode,
                "city": user.city,
                "business_type": profile.business_type,
                "proof_type": profile.proof_type,
                "proof": profile.proof,
                "status": user.status


            }

            return {"status": False, "user": userdata}

        else:

            user = db.query(UserModel).where(UserModel.mobile ==
                                             mobile).first()

            signature = data.signature
            otp = random.randint(746008, 999999)
            extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
                AUTHKEY+'&template_id='+TEMPLATE_ID + \
                '&mobile='+str(91)+mobile+'&otp='+str(otp)

            sendurl = OTP_SEND_URL+extra_param
            requests.get(sendurl)

            user.otp = otp
            db.flush()
            db.commit()

            profile = db.query(UserProfileModel).where(
                UserProfileModel.user_id == user.id).first()

            userdata = {
                "name": user.name,
                "email": user.email,
                "mobile": user.mobile,
                "country": user.country,
                "region": user.region,
                "pincode": user.pincode,
                "city": user.city,
                "business_type": profile.business_type,
                "proof_type": profile.proof_type,
                "proof": profile.proof,
                "status": user.status

            }

            return {"status":  True, "user": userdata}

    except Exception as e:
        print(e)

# Update Mobile Number (Seller)


async def SellerupdateMobileNumber(db: Session, data, mobile: str):
    try:
        user = db.query(UserModel).where(UserModel.mobile ==
                                         mobile).where(UserModel.otp == data.otp).first()
        if(user):
            user.mobile = data.mobile
            db.flush()
            db.commit()

            profile = db.query(UserProfileModel).where(
                UserProfileModel.user_id == user.id).first()

            userdata = {
                "name": user.name,
                "email": user.email,
                "mobile": user.mobile,
                "country": user.country,
                "region": user.region,
                "pincode": user.pincode,
                "city": user.city,
                "business_type": profile.business_type,
                "proof_type": profile.proof_type,
                "proof": profile.proof,
                "status": user.status

            }

            return {"status": True, "user": userdata}

        else:
            return {"status": False, "user": {}}

    except Exception as e:
        print(e)


# Change Mobile Number (Buyer)
async def BuyerChangeMobileNumber(db: Session, data, mobile: str):
    try:
        user = db.query(UserModel).where(UserModel.mobile ==
                                         data.mobile).first()

        if(user):

            profile = db.query(UserProfileModel).where(
                UserProfileModel.user_id == user.id).first()

            userdata = {
                "name": user.name,
                "email": user.email,
                "mobile": user.mobile,
                "country": user.country,
                "region": user.region,
                "pincode": user.pincode,
                "city": user.city,
                "business_type": profile.business_type,
                "proof_type": profile.proof_type,
                "proof": profile.proof,
                "status": user.status


            }

            return {"status": False, "user": userdata}

        else:

            user = db.query(UserModel).where(UserModel.mobile ==
                                             mobile).first()

            signature = data.signature
            otp = random.randint(746008, 999999)
            extra_param = 'extra_param={"SIGNATURE":"'+signature+'"}&authkey=' + \
                AUTHKEY+'&template_id='+TEMPLATE_ID + \
                '&mobile='+str(91)+mobile+'&otp='+str(otp)

            sendurl = OTP_SEND_URL+extra_param
            requests.get(sendurl)

            user.otp = otp
            db.flush()
            db.commit()

            profile = db.query(UserProfileModel).where(
                UserProfileModel.user_id == user.id).first()

            userdata = {
                "name": user.name,
                "email": user.email,
                "mobile": user.mobile,
                "country": user.country,
                "region": user.region,
                "pincode": user.pincode,
                "city": user.city,
                "business_type": profile.business_type,
                "proof_type": profile.proof_type,
                "proof": profile.proof,
                "status": user.status

            }

            return {"status":  True, "user": userdata}

    except Exception as e:
        print(e)

# Update Mobile Number (Buyer)


async def BuyerupdateMobileNumber(db: Session, data, mobile: str):
    try:
        user = db.query(UserModel).where(UserModel.mobile ==
                                         mobile).where(UserModel.otp == data.otp).first()
        if(user):
            user.mobile = data.mobile
            db.flush()
            db.commit()

            profile = db.query(UserProfileModel).where(
                UserProfileModel.user_id == user.id).first()

            userdata = {
                "name": user.name,
                "email": user.email,
                "mobile": user.mobile,
                "country": user.country,
                "region": user.region,
                "pincode": user.pincode,
                "city": user.city,
                "business_type": profile.business_type,
                "proof_type": profile.proof_type,
                "proof": profile.proof,
                "status": user.status

            }

            return {"status": True, "user": userdata}

        else:
            return {"status": False, "user": {}}

    except Exception as e:
        print(e)


# Update Profile (Buyer)
async def updateProfile(db: Session, data, user_id: int):

    try:
        user = db.query(UserModel).where(UserModel.id ==
                                         user_id).first()

        if(user.email == ''):
            email = str(user.mobile)+'@aseztak.com'
        else:
            email = user.email

        if(user):
            # Update user data
            user.name = data.name
            user.email = email
            user.country = data.country
            user.region = data.region
            user.city = data.city
            user.pincode = data.pincode
            user.status = 1
            db.flush()
            db.commit()

        # Update User Profile Data
            profile = db.query(UserProfileModel).where(
                UserProfileModel.user_id == user_id).first()

            if(profile):
                profile.proof = data.proof
                profile.proof_type = data.proof_type
                db.flush()
                db.commit()
            else:
                profile = UserProfileModel(
                    user_id=user_id, proof=data.proof, proof_type=data.proof_type)

                db.add(profile)
                db.commit()
                db.refresh(profile)

            userdata = {
                'name': user.name,
                'email': user.email,
                'mobile': user.mobile,
                'country': user.country,
                'region': user.region,
                'city': user.city,
                'business_type': '',
                'proof_type': profile.proof_type,
                'proof': profile.proof,
                'status': user.status
            }
            return userdata

        return False
    except Exception as e:
        print(e)


async def updateProof(db: Session, user_id: int, data):
    try:

        # user = db.query(UserProfileModel).where(
        #     UserProfileModel.user_id == user_id).first()

        # if(user):
        #     user.proof = data.proof
        #     user.proof_type = data.proof_type
        #     db.flush()
        #     db.commit()
        return True
        # else:
        #     return False
    except Exception as e:
        print(e)


# Admin Helper

# get all users
async def getAllusers(db: Session):
    try:
        return db.query(UserModel).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
    except Exception as e:
        print(e)

# deleted all user


async def AllDeletedUsers(db: Session):
    try:

        return db.query(UserModel).filter(UserModel.deleted_at.isnot(None)).order_by(UserModel.id.desc())
    except Exception as e:
        print(e)


# Suspended all user
async def AllSuspendedUsers(db: Session):
    try:

        return db.query(UserModel).filter(UserModel.status == 90).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
    except Exception as e:
        print(e)


# get all buyers


async def getAllBuyers(db: Session, status: str):
    try:

        if(status == 'all'):
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())

        else:
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).filter(UserModel.status == status).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())

    except Exception as e:
        print(e)


# get all buyers search
async def getAllBuyersSearch(db: Session, status: str, search: str):
    try:

        # User all list
        search = "%{}%".format(search)
        if(status == 'all'):
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).filter(UserModel.deleted_at.is_(None)).filter((UserModel.name.ilike(search)) | (UserModel.mobile.ilike(search))).order_by(UserModel.id.desc())

        else:
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).filter(UserModel.status == status).filter(UserModel.deleted_at.is_(None)).filter((UserModel.name.ilike(search)) | (UserModel.mobile.ilike(search))).order_by(UserModel.id.desc())

    except Exception as e:
        print(e)

# Deleted Buyers


async def deletedUsers(db: Session, role: int):
    return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == role).filter(UserModel.deleted_at.isnot(None)).order_by(UserModel.id.desc())


# get all seller
async def getAllSellers(db: Session, status: str):
    try:

        if(status == 'all'):
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
        else:
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.status == status).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
    except Exception as e:
        print(e)


# get all seller search
async def getAllSellersSearch(db: Session, status: str, search: str):
    try:
        search = "%{}%".format(search)
        if(status == 'all'):
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.deleted_at.is_(None)).filter((UserModel.name.ilike(search)) | (UserModel.mobile.ilike(search))).order_by(UserModel.id.desc())
        else:
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.status == status).filter(UserModel.deleted_at.is_(None)).filter((UserModel.name.ilike(search)) | (UserModel.mobile.ilike(search))).order_by(UserModel.id.desc())
    except Exception as e:
        print(e)


# -------------------------------------------------------------------------------------------

async def getAllUsersAdmin(db: Session, status: str, page: int, limit: int):

    try:
        if(status == 'all'):
            return db.query(UserModel).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())

        if(status == 'buyer'):
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())

        if(status == 'seller'):
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())

        if(status == '90'):
            return db.query(UserModel).filter(UserModel.status == 90).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())

        if(status == 'delete'):
            return db.query(UserModel).filter(UserModel.deleted_at.isnot(None)).order_by(UserModel.id.desc())

    except Exception as e:
        print(e)


# all users
async def getAdminAllusers(db: Session, status: str, type: str):
    try:

        if(type == 'all'):
            if(status == 'all'):
                return db.query(UserModel).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
            elif(status == 'deleted'):
                return db.query(UserModel).filter(UserModel.deleted_at.isnot(None)).order_by(UserModel.id.desc())

            else:
                return db.query(UserModel).filter(UserModel.status == status).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())

        else:
            if(type == 'seller'):
                role = 6
            if(type == 'buyer'):
                role = 5

            if(status == 'all'):
                return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == role).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
            elif(status == 'deleted'):
                return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
                    UserRoleModel.role_id == role).filter(UserModel.deleted_at.isnot(None)).order_by(UserModel.id.desc())

            else:
                return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == role).filter(UserModel.status == status).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())

    except Exception as e:
        print(e)


# all buyer
async def getAdminAllSellers(db: Session, status: str):
    try:

        if(status == 'all'):
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
        elif(status == 'deleted'):
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
                UserRoleModel.role_id == 6).filter(UserModel.deleted_at.isnot(None)).order_by(UserModel.id.desc())

        else:
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 6).filter(UserModel.status == status).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
    except Exception as e:
        print(e)


# all seller

async def getAdminAllBuyers(db: Session, status: str):
    try:

        if(status == 'all'):
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
        else:
            return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(UserRoleModel.role_id == 5).filter(UserModel.status == status).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
    except Exception as e:
        print(e)


async def AdmindeletedUsers(db: Session, role: int):

    return db.query(UserModel).join(UserRoleModel, UserRoleModel.model_id == UserModel.id).filter(
        UserRoleModel.role_id == role).filter(UserModel.deleted_at.isnot(None)).order_by(UserModel.id.desc())


async def AdminAllDeletedUsers(db: Session):
    try:

        return db.query(UserModel).filter(UserModel.deleted_at.isnot(None)).order_by(UserModel.id.desc())
    except Exception as e:
        print(e)


# Suspended all user
async def AdminAllSuspendedUsers(db: Session):
    try:

        return db.query(UserModel).filter(UserModel.status == 90).filter(UserModel.deleted_at.is_(None)).order_by(UserModel.id.desc())
    except Exception as e:
        print(e)
