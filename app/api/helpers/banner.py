from app.db.models.banners import BannersModel
from sqlalchemy.orm import Session


class BannerHelper:
    async def get_banner(db: Session):
        return db.query(BannersModel).order_by(BannersModel.id.desc()).all()

