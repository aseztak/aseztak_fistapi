from datetime import date, datetime
from operator import or_

# from sqlalchemy.sql.expression import desc, false, outerjoin, select
# from sqlalchemy import null


from sqlalchemy.sql.functions import func
from app.api.helpers.calculation import Calculations
# from app.api.helpers.orderstaticstatus import OrderStatus
from app.db.config import get_db
from app.db.models.acconts import AccountsModel
from app.db.models.carts import CartModel
from app.db.models.categories import *

from sqlalchemy.orm import Session, query
from app.db.models.media import ProductMediaModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.order_status import OrderStatusModel
# from app.db.models.orderdiscount import OrderDiscountModel
from app.db.models.orders import OrdersModel
from app.db.models.products import InventoryModel, ProductModel, ProductPricingModel
from app.db.models.returndays import CategoryReturnDaysModel
# from app.db.models.reverse import OrderRevreseModel
# from app.db.models.shipping import OrderShippingModel
from app.db.models.shipping_address import ShippingAddressModel
from app.db.models.shippingcharge import ShippingChargeModel
# from app.db.models.transaction import TransactionsModel
from app.db.models.user import UserModel
import uuid
# import math
# from starlette.requests import Request
import os
from app.resources.strings import *
import json
from app.api.util.calculation import Calculations, checkOrderPlaceDeliveryCalculation
from app.db.models.category_commission import CategoryCommissionModel
from app.api.util.service import Services
from app.api.util.calculation import productPricecalculation
# from app.db.models.invoice_items import InvoiceItemsModel
from app.db.models.pincode import PincodeModel
# import requests
from app.api.util.message import Message
from app.core import config
from app.db.models.user_facility import UserFacilityModel
from app.api.helpers.discount import Discount
from app.api.helpers.services import AsezServices
from app.api.helpers.products import ProductsHelper
# from app.db.models.product_discount import ProductDiscountModel


class CheckOutHelper:
    # Order Checkout RAHUL
    async def checkOut(db: Session, data: OrdersModel, user_id: int, txn_status: str = 'TXN_SUCCESS'):
        try:
            # Today Date
            today = date.today()
            # YY-mm-dd
            today = today.strftime("%Y-%m-%d")

            # Check buyer confirm
            checkConfirmBuyer = db.query(UserModel).filter(
                UserModel.id == user_id).first()

            # Check Delivery Charger for new user
            new_free_delivery = False
            if(config.FREE_DELIVERY == "True"):
                checkUserExistOrder = db.query(OrdersModel).filter(
                    OrdersModel.user_id == user_id).count()
                # Check Cancel Order
                checkCancelOrder = db.query(OrdersModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
                    OrdersModel.user_id == user_id).having(func.max(OrderStatusModel.status) == 980).group_by(OrderStatusModel.order_id).count()
                if(checkUserExistOrder == 0):
                    new_free_delivery = True
                else:
                    if(checkUserExistOrder == 1 and checkCancelOrder == 1):
                        new_free_delivery = True

             # Check User facility
            buyer_facility = db.query(UserFacilityModel).filter(UserFacilityModel.user_id == user_id).filter(
                UserFacilityModel.key_name == 'free_delivery').order_by(UserFacilityModel.id.desc()).first()

            if('No' in str(checkConfirmBuyer.confirm)):
                ord_status = 0
            else:
                ord_status = 1
            # Check Seller Id exist or not
            check_seller_id = data.seller_id
            if(len(check_seller_id) > 0):
                cartitemdata = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).filter(
                    CartModel.user_id == user_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).filter(CartModel.seller_id.in_(check_seller_id)).group_by(CartModel.seller_id).all()
            else:
                cartitemdata = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).filter(
                    CartModel.user_id == user_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).group_by(CartModel.seller_id).all()
            finalcartdata = []
            for cartitems in cartitemdata:

                in_stock_items = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).join(ProductPricingModel, ProductPricingModel.product_id == ProductModel.id).join(InventoryModel, InventoryModel.pricing_id == ProductPricingModel.id).filter(
                    CartModel.user_id == user_id).filter(CartModel.seller_id == cartitems.seller_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).filter(CartModel.uuid.op('regexp')(ProductPricingModel.id)).filter(ProductPricingModel.deleted_at.is_(None)).filter(InventoryModel.out_of_stock == 0).filter(or_(CartModel.quantity <= InventoryModel.stock, InventoryModel.unlimited == 1)).group_by(CartModel.product_id).all()

                cartdata = []
                for cartitem in in_stock_items:
                    cartdata.append(cartitem)

                finalcartdata.append(cartdata)

            check_orders_count = len(finalcartdata)

            # Maximum Ref ID
            maxref = db.query(func.max(OrdersModel.reff),
                              ).one()
            maxref = maxref[0] + 1

            # Address Meta
            address = db.query(ShippingAddressModel).where(
                ShippingAddressModel.id == data.address_id).first()
            shipping_phone = address.phone
            address = {
                'address': {
                    'ship_to': address.ship_to,
                    'address': address.address,
                    'city': address.city,
                    'phone': address.phone,
                    'alternative_phone': address.alt_phone,
                    'state': address.state,
                    'locality': address.locality,
                    'pincode': address.pincode,
                    'country': 'India'
                }
            }

            if(len(finalcartdata) > 0):
                item_counts = []
                total_order_amount = 0

                for total_items in finalcartdata:
                    sub_total = 0
                    # Sub total with discount
                    sub_total_with_discount = 0
                    orders_items = []
                    tax = 0
                    seller_products = []

                    seller_amount = 0
                    order_total_amount = 0

                    for oitem in total_items:

                        oitem = db.query(CartModel).filter(
                            CartModel.id == oitem.id).first()

                        # Get pricing Detail
                        price_id = oitem.uuid.split('-')
                        price = db.query(ProductPricingModel).where(ProductPricingModel.id == price_id[1]).where(
                            ProductPricingModel.deleted_at.is_(None)).first()
                        if(price):
                            # Checking Pricing Stock
                            # CLOSED BY RAHUL (START)
                            # check_stock = db.query(InventoryModel).where(
                            #     InventoryModel.pricing_id == price.id).first()
                            # CLOSED BY RAHUL (END)
                            # CLOSED BY RAHUL (START)
                            # if(check_stock.out_of_stock == 0):
                            # CLOSED BY RAHUL (END)
                            # Get cart items by product id
                            itemsdata = db.query(CartModel).join(ProductModel, ProductModel.id == CartModel.product_id).filter(CartModel.user_id == user_id).filter(
                                CartModel.seller_id == oitem.seller_id).filter(CartModel.product_id == oitem.product_id).filter(ProductModel.status == 51).filter(CartModel.status == 1).all()

                            for itemdata in itemsdata:
                                # NEW CHANGES RAHUL (START)
                                pprice_id = itemdata.uuid.split('-')
                                ppprice = db.query(ProductPricingModel).where(ProductPricingModel.id == pprice_id[1]).where(
                                    ProductPricingModel.deleted_at.is_(None)).first()

                                check_stock = db.query(InventoryModel).where(
                                    InventoryModel.pricing_id == ppprice.id).first()
                                if(check_stock.out_of_stock == 0):
                                    # NEW CHANGES RAHUL (END)
                                    # Get Product
                                    product = db.query(ProductModel).where(
                                        ProductModel.id == itemdata.product_id).where(ProductModel.status == 51).first()

                                    # Append Product data
                                    seller_products.append(product)

                                    # Seller Account Maintain
                                    # CategoryCommission
                                    category_commission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id == product.category).filter(
                                        CategoryCommissionModel.start_date <= today).order_by(CategoryCommissionModel.id.desc()).first()

                                    seller_account_maintain = await Calculations.calculateSellerPayment(cat_commission=category_commission.commission, commission_tax=category_commission.commission_tax, price=ppprice.price, tax=ppprice.tax, quantity=itemdata.quantity, tcs=TCS, tds=TDS)

                                    seller_amount += seller_account_maintain

                                    # Item total price
                                    # item price calculation
                                    # Aseztak Service
                                    # Get pricing Detail

                                    # aseztak_service = Services.aseztak_services(
                                    #     ppprice.updated_at, db=db)
                                    # today_date = ppprice.updated_at.strftime(
                                    #     '%Y-%m-%d')
                                    aseztak_service = await AsezServices.aseztak_services(commission_date=today, db=db)
                                    product_price: ProductModel = await ProductsHelper.getPrice(db, product, ppprice, asez_service=aseztak_service, app_version='V4')
                                    product_price_with_discount: ProductModel = await ProductsHelper.chekcOutPrice(db, product, ppprice, asez_service=aseztak_service, app_version='V4')
                                    # product_price = productPricecalculation(price=ppprice.price, tax=ppprice.tax, commission=aseztak_service.rate,
                                    #                                         gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                                    # Sub Total
                                    sub_total += (round(product_price, 2)
                                                  * itemdata.quantity)

                                    # Sub Total
                                    sub_total_with_discount += (round(product_price_with_discount, 2)
                                                                * itemdata.quantity)

                                    # Order Total Amount
                                    total_order_amount += (round(product_price, 2) *
                                                           itemdata.quantity)

                                    # Count Total Items
                                    orders_items.append(itemdata)
                                    item_counts.append(itemdata)

                                    # Total tax calculate
                                    pp = float(round(product_price, 2)) - \
                                        float(itemdata.price)

                                    tax += (round(pp, 2) * itemdata.quantity)

                    # Get Shipping Charge

                    shipping_charge = db.query(ShippingChargeModel).filter(
                        ShippingChargeModel.start_date <= today).order_by(ShippingChargeModel.id.desc()).first()

                    # Check Seller Discount
                    check_seller_discount = await Discount.checkSellerDiscount(db=db, seller_id=total_items[0].seller_id, today_date=today, order_amount=float(round(sub_total, 2)))
                    if(check_seller_discount != 0):
                        sub_total = (float(round(sub_total_with_discount, 2)))
                    # User facility (free delivery)
                    facility_free_delivery = False
                    if(new_free_delivery == False):

                        # Check Buyer Facility
                        if(buyer_facility):
                            freedeliveryamount = int(buyer_facility.key_value)
                            if(round(sub_total, 2) < round(freedeliveryamount, 2)):
                                if(int(buyer_facility.key_value) != 0):
                                    delivery_charge = shipping_charge.rate
                            else:
                                facility_free_delivery = True
                                delivery_charge = 0

                        else:
                            delivery_charge = checkOrderPlaceDeliveryCalculation(
                                order_amount=sub_total, order_limit=shipping_charge.order_limit, delivery_charge=shipping_charge.rate, shipping_payment_mode=shipping_charge.payment_mode, order_payment_mode=data.payment_type)
                    else:
                        delivery_charge = 0
                    # Check Discount
                    discount_amount = 0

                    if(data.discount_rate != 0):
                        c_discount = (float(round(sub_total, 2))
                                      * float(data.discount_rate)) / 100
                        discount_amount += c_discount

                        subtotal = round(sub_total, 2) - \
                            round(discount_amount, 2)
                    else:
                        subtotal = round(sub_total, 2)

                    # Grand Total
                    # New Changes Wallet
                    check_wallet_amount = 0
                    if(data.wallet_amount != "0.00"):
                        check_wallet_amount = (
                            round(sub_total, 2) * 1) / 100
                        check_wallet_amount = int(check_wallet_amount)
                    # New Changes Wallet
                    grand_total = float(
                        round(subtotal, 2)) + float(delivery_charge)
                    # For Transaction Table
                    order_total_amount += grand_total

                    # Rounded Value
                    if(check_orders_count > 1):

                        rounded_value = grand_total - round(
                            grand_total)

                    else:
                        rounded_value = round(grand_total) - grand_total

                    # Insert Order
                    paymentstatus = 1
                    if(data.payment_type == 'ONLINE' and txn_status == 'TXN_SUCCESS'):
                        paymentstatus = 51

                        # Random Generate Order Number

                    order_number = uuid.uuid4().hex.upper()[0:13]
                    if(sub_total > 0):

                        # Update Free Delivery
                        update_free_delivery = 'No'
                        if(new_free_delivery == True):
                            update_free_delivery = 'Yes'

                        if(facility_free_delivery == True):
                            update_free_delivery = 'Yes'

                        order_grand_total = round(grand_total, 0)
                        if(data.partial_amount != 0):  # new Changes #Check Partial
                            if(check_wallet_amount != 0):
                                w_sub_total = (
                                    float(round(sub_total, 2)) - float(int(check_wallet_amount)))
                                # Chek Discount
                                w_discount_amount = 0
                                if(data.discount_rate != 0):
                                    w_discount_amount = (
                                        float(round(w_sub_total, 2)) * float(data.discount_rate)) / 100
                                    w_discount_amount = round(
                                        w_discount_amount, 2)
                                w_sub_total = (
                                    float(round(w_sub_total, 2)) - float(w_discount_amount))
                                w_sub_total = (
                                    float(round(w_sub_total, 2)) + float(delivery_charge))
                                partial_amount = (
                                    round(w_sub_total, 2) * 20) / 100
                                data.partial_amount = round(partial_amount, 2)
                            else:
                                # Check Discount
                                w_sub_total = round(sub_total, 2)
                                w_discount_amount = 0
                                if(data.discount_rate != 0):
                                    w_discount_amount = (
                                        float(round(w_sub_total, 2)) * float(data.discount_rate)) / 100
                                    w_discount_amount = round(
                                        w_discount_amount, 2)
                                w_sub_total = (
                                    float(round(w_sub_total, 2)) - float(w_discount_amount))
                                w_sub_total = (
                                    float(round(w_sub_total, 2)) + float(delivery_charge))
                                partial_amount = (
                                    round(w_sub_total, 2) * 20) / 100
                                data.partial_amount = round(partial_amount, 2)
                        # New Changes
                        cod_amount = 0.00
                        if(data.partial_amount != 0 and data.txn_order_id != ''):  # New Changes
                            if(check_wallet_amount != 0):
                                w_sub_total = (
                                    float(round(sub_total, 2)) - float(int(check_wallet_amount)))
                                # Chek Discount
                                w_discount_amount = 0
                                if(data.discount_rate != 0):
                                    w_discount_amount = (
                                        float(round(w_sub_total, 2)) * float(data.discount_rate)) / 100
                                    w_discount_amount = round(
                                        w_discount_amount, 2)
                                w_sub_total = (
                                    float(round(w_sub_total, 2)) - float(w_discount_amount))
                                w_sub_total = (
                                    float(round(w_sub_total, 2)) + float(delivery_charge))
                                partial_amount = (
                                    round(w_sub_total, 2) * 20) / 100
                                partial_amount = round(partial_amount, 2)

                                cod_amount = float(
                                    round(w_sub_total, 2)) - float(partial_amount)
                                cod_amount = round(cod_amount, 0)
                            else:
                                w_sub_total = round(sub_total, 2)
                                # Chek Discount
                                w_discount_amount = 0
                                if(data.discount_rate != 0):
                                    w_discount_amount = (
                                        float(round(w_sub_total, 2)) * float(data.discount_rate)) / 100
                                    w_discount_amount = round(
                                        w_discount_amount, 2)
                                w_sub_total = (
                                    float(round(w_sub_total, 2)) - float(w_discount_amount))
                                w_sub_total = (
                                    float(round(w_sub_total, 2)) + float(delivery_charge))
                                partial_amount = (
                                    round(w_sub_total, 2) * 20) / 100
                                partial_amount = round(partial_amount, 2)

                                cod_amount = float(
                                    round(w_sub_total, 2)) - float(partial_amount)
                                cod_amount = round(cod_amount, 0)
                        if(data.payment_type == 'COD' and data.txn_order_id == ''):  # New Changes
                            cod_amount = order_grand_total
                        if(data.partial_amount != 0 and data.txn_order_id != ''):  # New Changes
                            data.payment_type = 'PARTIAL'
                        dborder = OrdersModel(
                            reff=maxref,
                            order_number="ORD-"+str(order_number),
                            user_id=user_id,
                            round_off=round(rounded_value, 2),
                            grand_total=order_grand_total,
                            # Partial Amount (New)
                            partial_amount=data.partial_amount,
                            # New Changes Cod Amount
                            cod_amount=cod_amount,
                            # wallet_amount=data.wallet_amount,
                            total_tax=round(tax, 0),
                            discount_rate=round(data.discount_rate, 2),
                            discount=round(discount_amount, 2),
                            delivery_charge=delivery_charge,
                            item_count=len(orders_items),
                            payment_status=paymentstatus,
                            payment_method=data.payment_type,
                            meta_data=json.dumps(address),
                            address_id=data.address_id,
                            app_version='V4',
                            free_delivery=update_free_delivery,
                            status=ord_status,
                            created_at=datetime.now(),
                            updated_at=datetime.now()
                        )
                        db.add(dborder)
                        db.flush()
                        # db.commit()
                        # db.refresh(dborder)

                        # Insert Order Status
                        dborderstatus = OrderStatusModel(
                            order_id=dborder.id,
                            status=0,
                            created_at=datetime.now(),
                            updated_at=datetime.now()
                        )
                        db.add(dborderstatus)
                        # db.commit()
                        # db.refresh(dborderstatus)

                        # Insert order Items
                        # New Changes Wallet
                        # if(len(orders_items) > 1):  # New Changes Wallet
                        #     recalculate_delivery_charge = (float(
                        #         delivery_charge) / len(orders_items))
                        # else:
                        #     recalculate_delivery_charge = float(
                        #         delivery_charge)
                        for orderitem in orders_items:
                            # Get pricing Detail
                            product_price_id = orderitem.uuid.split('-')
                            product_price = db.query(ProductPricingModel).where(ProductPricingModel.id == product_price_id[1]).where(
                                ProductPricingModel.deleted_at.is_(None)).first()

                            # Check Seller State for GST IGST
                            sellerdata = db.query(ProductModel).filter(
                                ProductModel.id == orderitem.product_id).first()
                            sellerdetail = db.query(PincodeModel).filter(
                                PincodeModel.pincode == sellerdata.seller.pincode).first()

                            # Buyer State for GST IGST
                            buyerdata = db.query(UserModel).filter(
                                UserModel.id == orderitem.user_id).first()

                            shipping_address = db.query(ShippingAddressModel).filter(
                                ShippingAddressModel.user_id == orderitem.user_id).filter(ShippingAddressModel.id == data.address_id).first()

                            if(shipping_address is not None):
                                buyerdetails = db.query(PincodeModel).filter(
                                    PincodeModel.pincode == shipping_address.pincode).first()

                            imagedata = 0
                            if(txn_status == 'TXN_SUCCESS'):
                                if(product_price):
                                    # Checking Inventory for product Stock
                                    inventory = db.query(InventoryModel).where(
                                        InventoryModel.pricing_id == product_price.id).first()

                                    if(inventory.unlimited == 0 and inventory.out_of_stock == 0):
                                        total_stock = (
                                            int(inventory.stock) - int(orderitem.quantity))

                                        inventory.stock = total_stock
                                        db.flush()
                                        # db.commit()

                                    if(inventory.stock == 0 and inventory.unlimited == 0):
                                        inventory.out_of_stock = 1
                                        db.flush()
                                        # db.commit()

                            # Store Images
                            images = 0
                            # Products
                            prpdo = db.query(ProductMediaModel).filter(
                                ProductMediaModel.model_id == orderitem.product_id).filter(ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                            images = prpdo.id

                            price_images = 0
                            if (product_price.default_image != 0):
                                checkImg = db.query(ProductMediaModel).filter(
                                    ProductMediaModel.id == product_price.default_image).filter(ProductMediaModel.deleted_at.is_(None)).first()

                                if(checkImg):
                                    price_images = product_price.default_image

                            if(price_images == 0):
                                imagedata = images
                            else:
                                imagedata = price_images

                            # Aseztak Service
                            # aseztak_service = Services.aseztak_services(
                            #     product_price.updated_at, db=db)
                            # today_date = product_price.updated_at.strftime(
                            #     '%Y-%m-%d')
                            aseztak_service = await AsezServices.aseztak_services(commission_date=today, db=db)
                            # For Invoice
                            # Check Discount
                            invoice_discount_amount = 0
                            savediscountPrice = 0
                            if(data.discount_rate != 0):

                                # Asez Service Amount
                                azservice_amount = (
                                    float(
                                        product_price.price) * aseztak_service.rate) / 100

                                azservice_amount = round(
                                    azservice_amount, 2)

                                # Gst on Aseztak Service
                                gstonaz_service = (
                                    azservice_amount * aseztak_service.gst_on_rate) / 100

                                gstonaz_service = round(
                                    gstonaz_service, 2)

                                aseztotalservice = (
                                    azservice_amount + gstonaz_service)

                                # Gst on base price
                                pricetax = (float(
                                    product_price.price) *
                                    product_price.tax) / 100

                                productprice = (
                                    product_price.price + aseztotalservice + pricetax)

                                # Save Every Item Discount Amount
                                savediscountPrice = float(
                                    product_price.price) * float(data.discount_rate) / 100

                                savediscountPrice = savediscountPrice * orderitem.quantity

                                c_discount = float(
                                    product_price.price) * float(data.discount_rate) / 100

                                proprice = round(product_price.price, 2) - \
                                    c_discount

                                invoice_discount_amount = float(
                                    product_price.price * orderitem.quantity) * float(data.discount_rate) / 100

                                invoice_discount_amount = invoice_discount_amount
                            else:
                                proprice = round(
                                    product_price.price, 2)

                            # item price calculation

                            taxable_total_amount = (
                                proprice * orderitem.quantity)

                            taxable_total_amount = round(
                                taxable_total_amount, 2)

                            # Gst on base price
                            price_tax = (taxable_total_amount *
                                         product_price.tax) / 100

                            # gst_on_price_tax = round(price_tax, 2)
                            # Changes by Me
                            gstonpricetax = (price_tax / 2)

                            if(buyerdetails is not None):

                                if(str(sellerdetail.statename) == str(buyerdetails.statename)):
                                    # CGST ON PRICE TAX
                                    cgst_on_price_tax = gstonpricetax

                                    # SGST ON PRICE TAX
                                    sgst_on_price_tax = gstonpricetax

                                    # IGST ON PRICE TAX
                                    igst_on_price_tax = 0.00
                                else:
                                    # CGST ON PRICE TAX
                                    cgst_on_price_tax = 0.00

                                    # SGST ON PRICE TAX
                                    sgst_on_price_tax = 0.00

                                    # IGST ON PRICE TAX
                                    igst_on_price_tax = price_tax
                            else:

                                # CGST ON PRICE TAX
                                cgst_on_price_tax = 0.00

                                # SGST ON PRICE TAX
                                sgst_on_price_tax = 0.00

                                # IGST ON PRICE TAX
                                igst_on_price_tax = 0.00

                            # Asez Service Amount
                            aseztak_service_amount = (
                                taxable_total_amount * aseztak_service.rate) / 100

                            # aseztak_service_amount = round(
                            #     aseztak_service_amount, 2)

                            # Gst on Aseztak Service
                            gst_on_asez_service = (
                                aseztak_service_amount * aseztak_service.gst_on_rate) / 100

                            # gst_on_asez_service = round(
                            #     gst_on_asez_service, 2)
                            # CHANGES by Me
                            gst_on_asez_service = gst_on_asez_service

                            gstonasezservice = (
                                gst_on_asez_service / 2)

                            if(buyerdetails is not None):
                                if(str(sellerdetail.statename) == str(buyerdetails.statename)):
                                    # CGST ON ASEZ SERVICE
                                    cgst_on_asez_service = gstonasezservice

                                    # SGST ON ASEZ SERVICE
                                    sgst_on_asez_service = gstonasezservice

                                    # ISGST ON ASEZ SERVICE
                                    igst_on_asez_service = 0.00
                                else:
                                    # CGST ON ASEZ SERVICE
                                    cgst_on_asez_service = 0.00

                                    # SGST ON ASEZ SERVICE
                                    sgst_on_asez_service = 0.00

                                    # ISGST ON ASEZ SERVICE
                                    igst_on_asez_service = gst_on_asez_service
                            else:
                                # CGST ON ASEZ SERVICE
                                cgst_on_asez_service = 0.00

                                # SGST ON ASEZ SERVICE
                                sgst_on_asez_service = 0.00

                                # ISGST ON ASEZ SERVICE
                                igst_on_asez_service = 0.00

                            # invoice_price: ProductModel = await ProductsHelper.getPrice(db, sellerdata, product_price, asez_service=aseztak_service, app_version='V4')
                            invoice_price = productPricecalculation(price=proprice, tax=product_price.tax, commission=aseztak_service.rate,
                                                                    gst_on_commission=aseztak_service.gst_on_rate, tds=aseztak_service.tds_rate, tcs=aseztak_service.tcs_rate, round_off=aseztak_service.round_off)

                            invoice_total_amount = (
                                invoice_price * orderitem.quantity)

                            # Item Total Amount
                            item_total_amount = (
                                product_price.price * orderitem.quantity)

                            item_total_amount = round(
                                item_total_amount, 2)

                            # Seller Commission
                            prproduct = db.query(ProductModel).filter(
                                ProductModel.id == orderitem.product_id).first()
                            commission_seller = Services.commission_seller(
                                commission_date=today, category_id=prproduct.category, db=db)

                            # Save Return Expiry
                            category_return_days = db.query(ProductModel).filter(
                                ProductModel.id == orderitem.product_id).first()
                            category_return_days = db.query(CategoryReturnDaysModel).filter(
                                CategoryReturnDaysModel.category_id == category_return_days.category).filter(CategoryReturnDaysModel.start_date <= today).order_by(CategoryReturnDaysModel.start_date.desc()).first()

                            # Check Seller Discount
                            product_discount = 0
                            if(check_seller_discount != 0):
                                product_discount = float(check_seller_discount)
                            # # Check Seller Product Discount
                            # check_product_discount = prproduct.product_discount.filter(func.date_format(
                            #     ProductDiscountModel.valid_upto, "%Y-%m-%d") >= today).order_by(ProductDiscountModel.id.desc()).first()
                            # product_discount = 0
                            # if(check_product_discount is not None):
                            #     product_discount = check_product_discount.discount

                            # New Changes Wallet
                            if(data.wallet_amount != "0.00"):
                                if(product_discount == 0):
                                    itm_price = float(product_price.price)
                                    itm_price = (
                                        float(itm_price) * float(orderitem.quantity))

                                    itm_gst = (float(itm_price) *
                                               float(product_price.tax) / 100)
                                    itm_asz_service = (
                                        float(itm_price) * float(aseztak_service.rate) / 100)
                                    itm_asz_service_tax = (
                                        float(itm_asz_service) * float(aseztak_service.gst_on_rate) / 100)
                                    item_total_amount_with_product_discount = (
                                        float(itm_price) + float(itm_gst) + float(itm_asz_service) + float(itm_asz_service_tax))
                                    item_wallet_amount = ((float(
                                        item_total_amount_with_product_discount)) * 1) / 100
                                else:
                                    itm_price = (
                                        float(product_price.price) * float(product_discount)) / 100
                                    itm_price = float(
                                        round(product_price.price, 2) - float(round(itm_price, 2)))
                                    itm_price = (
                                        float(itm_price) * float(orderitem.quantity))

                                    itm_gst = (float(itm_price) *
                                               float(product_price.tax) / 100)
                                    itm_asz_service = (
                                        float(itm_price) * float(aseztak_service.rate) / 100)
                                    itm_asz_service_tax = (
                                        float(itm_asz_service) * float(aseztak_service.gst_on_rate) / 100)
                                    item_total_amount_with_product_discount = (
                                        float(itm_price) + float(itm_gst) + float(itm_asz_service) + float(itm_asz_service_tax))
                                    item_wallet_amount = ((float(
                                        item_total_amount_with_product_discount)) * 1) / 100
                            else:
                                item_wallet_amount = 0
                            if(txn_status == 'TXN_FAILURE'):
                                item_wallet_amount = 0

                            dborderitem = OrderItemsModel(
                                order_id=dborder.id,
                                product_id=orderitem.product_id,
                                quantity=orderitem.quantity,
                                price=product_price.price,
                                product_discount=product_discount,
                                discount_amount=savediscountPrice,
                                discount_rate=data.discount_rate,
                                cgst_on_tax=cgst_on_price_tax,
                                sgst_on_tax=sgst_on_price_tax,
                                igst_on_tax=igst_on_price_tax,
                                commission_buyer=aseztak_service.rate,
                                commission_buyer_tax=aseztak_service.gst_on_rate,
                                asez_service_amount=aseztak_service_amount,
                                asez_service_on_cgst=cgst_on_asez_service,
                                asez_service_on_sgst=sgst_on_asez_service,
                                asez_service_on_igst=igst_on_asez_service,
                                taxable_total_amount=item_total_amount,
                                commission_seller=commission_seller.commission,
                                commission_seller_tax=commission_seller.commission_tax,
                                tds_rate=TDS,
                                tcs_rate=TCS,
                                wallet_amount=int(item_wallet_amount),
                                item_total_amount=invoice_total_amount,
                                tax=product_price.tax,
                                attributes=orderitem.attributes,
                                uuid=orderitem.uuid,
                                hsn_code=orderitem.hsn_code,
                                images="["+str(imagedata)+"]",
                                status=0,
                                return_declined_by=0,  # New Changes
                                created_at=datetime.now(),
                                updated_at=datetime.now(),
                                return_expiry=category_return_days.return_days
                            )
                            db.add(dborderitem)
                            # db.commit()
                            # db.refresh(dborderitem)

                        # Seller Account Maintain
                        seller = db.query(UserModel).where(
                            UserModel.id == seller_products[0].userid).first()

                        if(txn_status == 'TXN_FAILURE'):
                            txn_description = 'FAILED (' + \
                                str("ORD-"+order_number) + ')'
                        else:
                            txn_description = 'ORDERED (' + \
                                str("ORD-"+order_number) + ')'

                        dbaccount = AccountsModel(
                            user_id=seller.id,
                            txn_date=today,
                            head='ORDERED',
                            txn_description=str(txn_description),
                            txn_type="DR",
                            txn_amount=seller_amount,
                            created_at=datetime.now(),
                            updated_at=datetime.now()
                        )
                        db.add(dbaccount)
                        # db.commit()
                        # db.refresh(dbaccount)

                        # # Insert Transaction for every Order
                        # if(data.payment_type == 'COD'):
                        #     dbtransaction = TransactionsModel(
                        #         payment_method='cod',
                        #         order_ref_id=maxref,
                        #         amount=order_total_amount,
                        #         status="TXN_SUCCESS"
                        #     )

                        #     db.add(dbtransaction)
                        #     db.commit()
                        #     db.refresh(dbtransaction)

                        # Delete Cart Items
                        if(txn_status == 'TXN_SUCCESS'):
                            for deletecartitems in orders_items:
                                db.query(CartModel).filter(
                                    CartModel.id == deletecartitems.id).delete()
                                # db.commit()

                        db.commit()
                        # Sending Message and Notification
                        # COUNT ITEMS
                        counttotalitems = len(orders_items)
                        product_title = db.query(ProductModel).filter(
                            ProductModel.id == orders_items[0].product_id).first()

                        notification_image = db.query(ProductMediaModel).filter(
                            ProductMediaModel.model_id == product_title.id).filter(ProductMediaModel.deleted_at.is_(None)).first()

                        if(counttotalitems > 1):
                            counttotalitems = (counttotalitems - 1)

                            product_title = str(
                                product_title.title)

                            product_title = product_title[:15]

                            product_title = str(
                                product_title)+' and '+str(counttotalitems)+' more'

                        else:
                            product_title = str(product_title.title)
                            product_title = product_title[:15]

                        if(txn_status == 'TXN_SUCCESS'):

                            # Send Whatsapp Msz
                            if(data.payment_type == 'ONLINE'):
                                payableamount = 'PREPAID'
                            else:
                                payableamount = cod_amount
                            await Message.sendorderconfirmationwhatsappmsz(name=buyerdata.name, mobile=shipping_phone, order_number=dborder.order_number, payable_amount=payableamount, payment_method=data.payment_type)
                            # # Send Messag to Buyer
                            await Message.SendOrderPlaceMessagetoBuyer(
                                mobile=buyerdata.mobile, message=product_title, order_number=dborder.order_number)

                            # Send Notification to Buyer
                            b_notification_body = 'Order for ' + str(product_title) + ' with Order ID ' + str(
                                dborder.order_number) + ' has been successfully placed.'
                            b_notification_title = 'Order Placed'
                            b_path = '/order-detail'
                            b_notification_image = notification_image.file_path

                            if(buyerdata.fcm_token is not None):
                                await Message.SendOrderNotificationtoBuyer(user_token=buyerdata.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=dborder.id)

                            # Sedn Message to Seller
                            if('Yes' in str(buyerdata.confirm)):
                                await Message.SendOrderPlaceMessagetoSeller(
                                    mobile=sellerdata.seller.mobile, message=product_title, order_number=dborder.order_number)

                                # Send Notification to Seller
                                s_notification_body = 'You have a new order request for ' + \
                                    str(product_title) + ' with Order ID ' + \
                                    str(dborder.order_number)
                                s_notification_title = 'New Order'
                                s_path = '/edit-pending-page'
                                s_notification_image = notification_image.file_path

                                if(sellerdata.seller.fcm_token is not None):
                                    await Message.SendOrderNotificationtoSeller(user_token=sellerdata.seller.fcm_token, body=s_notification_body, title=s_notification_title, path=s_path, image=s_notification_image, order_id=dborder.id)

                # if(new_free_delivery == True and txn_status == 'TXN_SUCCESS'):
                #     # Send FREE DELIVERY Messag to Buyer
                #     await Message.SendFreeDeliveryMessagetoBuyer(
                #         mobile=checkConfirmBuyer.mobile, username=checkConfirmBuyer.name)

                return {'max_reff': maxref, 'txn_status': txn_status}

            else:
                return False

        except Exception as e:
            db.rollback()
            print(e)
