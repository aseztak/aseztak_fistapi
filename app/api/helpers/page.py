from app.db.models.page import PageModel
from sqlalchemy.orm import Session


class PageHelper:
    async def get_page(db: Session, slug: str):

        return db.query(PageModel).filter(PageModel.slug == slug).first()
