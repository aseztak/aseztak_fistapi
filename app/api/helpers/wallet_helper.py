from datetime import date
from app.db.models.categories import *

from sqlalchemy.orm import Session
from app.db.models.wallet import WalletModel
from app.db.models.wallet_uses_slab import WalletUsesSlabModel
from app.resources.strings import *


class WalletHelper:
    # Check Wallet Balance
    async def check_wallet_balnce(db: Session, user_id: int):
        try:
            today = date.today()
            today = today.strftime("%Y-%m-%d")
            wallet_amount = db.query(WalletModel).filter(
                WalletModel.user_id == user_id).filter(WalletModel.status == 1).order_by(WalletModel.id.desc()).all()
            wallet_closing_balance = 0
            wallet_debit_balance = 0
            wallet_credit_balance = 0
            if(len(wallet_amount) > 0):
                for wallet in wallet_amount:
                    if('DR' in str(wallet.type)):
                        wallet_debit_balance += wallet.amount
                    else:
                        if(wallet.valid_upto != None):
                            valid_upto = wallet.valid_upto.strftime('%Y-%m-%d')
                            if(valid_upto >= today):
                                wallet_credit_balance += wallet.amount
                wallet_closing_balance = wallet_credit_balance - wallet_debit_balance

            return wallet_closing_balance
        except Exception as e:
            print(e)

    async def check_usable_balnce(db: Session, item_total: int):
        try:
            check_usable_balance = db.query(WalletUsesSlabModel).order_by(
                WalletUsesSlabModel.id.asc()).all()
            use_balance = 0
            for slab in check_usable_balance:
                if(item_total >= slab.from_amount and item_total <= slab.to_amount):
                    use_balance = ((item_total)*(slab.percentage / 100))

            return use_balance
        except Exception as e:
            print(e)
