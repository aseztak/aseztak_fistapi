import math
from sqlalchemy.orm.session import Session
from datetime import datetime
from app.db.models.orders import OrdersModel
from app.db.models.user_facility import UserFacilityModel
from sqlalchemy import func


class Calculations:
    async def calculateSellerPayment(cat_commission: float, commission_tax: float, price: float, tax: float, quantity: int, tcs: float, tds: float):

        # Get Price
        # product_price = (price * tax) / 100 + price

        product_price = price

        taxable_value = round(product_price, 2) * quantity

        afterTaxValue = (taxable_value * tax) / 100 + taxable_value

        # TCS AMOUNT
        tcs_amount = (taxable_value * tcs) / 100

        # TDS AMOUNT
        tds_amount = (afterTaxValue * tds) / 100

        # Category Commission
        commission = (taxable_value * cat_commission) / 100

        commission = (commission * commission_tax) / 100 + commission

        final_amount = (afterTaxValue)

        # final = {
        #     "seller_amount": final_price,
        #     "commission_amount": total_commision
        # }
        return final_amount


# ROUND OF MATH
def roundOf(price: float):
    if (float(price) % 1) >= 0.5:
        price = math.ceil(price)
    else:
        price = round(price)

    return price


# Showing Amount 2 decimal value
def floatingValue(number):
    format_float = "{:.2f}".format(number)
    return format_float


# Product Price Calculation
def productPricecalculation(price: float, tax: float, commission: float, gst_on_commission: float, tds: float, tcs: float, round_off: float, app_version: str = 'V4'):
    try:
        if(commission != 0):
            if(round_off == 1):

                commission_rate = (
                    commission * gst_on_commission) / 100 + commission

                total_commission = (commission_rate + tds + tcs) + 100

                product_price = (price * total_commission) / 100

                product_price = roundOf(product_price)

                product_price = product_price + (product_price * tax) / 100

                product_price = roundOf(product_price)

                return product_price

            else:

                commission_rate = (
                    commission * gst_on_commission) / 100 + commission

                total_commission = (commission_rate + tds + tcs)
                # Asez Service
                asez_service = (price * total_commission) / 100

                # GST ON Product
                gst_on_product = (price * tax) / 100

                product_price = (asez_service + gst_on_product + price)

                if(app_version == 'V4'):
                    product_price = round(product_price, 2)
                else:
                    product_price = roundOf(product_price)

                return product_price
        else:
            price = (price * tax) / 100 + price
            product_price = roundOf(price)

            return product_price

    except Exception as e:
        print(e)


# Order Discount Calculation
def orderDiscountCalculation(app_version: str, order_amount: float, discount_amount: float, discount_rate: float):
    try:

        if(app_version == 'V4'):
            order_amount = round(order_amount, 2)
            discount_amount = (
                order_amount * float(discount_rate)) / 100
            discount_amount = round(discount_amount, 2)

        else:
            order_amount = roundOf(order_amount)

            discount_amount = (
                order_amount * float(discount_rate)) / 100

            discount_amount = roundOf(discount_amount)

        return discount_amount

    except Exception as e:
        print(e)

# Order Delivery Calculation


def orderDeliveryCalculation(db: Session, free_delivery: str, user_id: int, order_date: str, app_version: str, order_amount: float, order_limit: float, delivery_charge: float, shipping_payment_mode: str, order_payment_mode: str):
    try:

        # Order Details
        if('Yes' in str(free_delivery)):

            delivery_charge = 0

        else:
            # CheckUser facility
            order_date = order_date.strftime("%Y-%m-%d")
            userfacility = db.query(UserFacilityModel).filter(UserFacilityModel.user_id == user_id).filter(
                UserFacilityModel.key_name == 'free_delivery').filter(func.date_format(UserFacilityModel.created_at, "%Y-%m-%d") <= order_date).order_by(UserFacilityModel.id.desc()).first()

            if(userfacility):
                freedeliveryamount = int(userfacility.key_value)
                if(round(order_amount, 2) < round(freedeliveryamount, 2)):
                    delivery_charge = delivery_charge
            else:
                if(app_version == 'V4'):
                    if('BOTH' in str(shipping_payment_mode) or order_payment_mode in str(shipping_payment_mode)):
                        if(order_limit != 0):
                            if(round(order_amount, 2) >= float(order_limit)):
                                delivery_charge = 0
                            else:
                                delivery_charge = delivery_charge
                        else:
                            delivery_charge = delivery_charge
                    else:
                        delivery_charge = 0
                else:
                    if('BOTH' in str(shipping_payment_mode) or order_payment_mode in str(shipping_payment_mode)):
                        if(order_limit != 0):
                            if(roundOf(order_amount) >= float(order_limit)):
                                delivery_charge = 0
                            else:
                                delivery_charge = delivery_charge
                        else:
                            delivery_charge = delivery_charge
                    else:
                        delivery_charge = 0

        return delivery_charge

    except Exception as e:
        print(e)


def checkOrderPlaceDeliveryCalculation(order_amount: float, order_limit: float, delivery_charge: float, shipping_payment_mode: str, order_payment_mode: str):
    try:

        if('BOTH' in str(shipping_payment_mode) or order_payment_mode in str(shipping_payment_mode)):
            if(order_limit != 0):
                if(round(order_amount, 2) >= float(order_limit)):
                    delivery_charge = 0
                else:
                    delivery_charge = delivery_charge
            else:
                delivery_charge = delivery_charge
        else:
            delivery_charge = 0

        return delivery_charge

    except Exception as e:
        print(e)


# Order price calculation
def calculateTaxableAmount(price: float, quantity: int):
    try:
        taxable_amount = (price * quantity)

        taxable_amount = round(taxable_amount, 2)

        return taxable_amount

    except Exception as e:
        print(e)


def calculateGstAmount(price: float, tax: float):
    try:
        gstamount = (price * tax) / 100

        gstamount = round(gstamount, 2)
        return gstamount

    except Exception as e:
        print(e)


def calculateCommissionSeller(amount: float, commission: float):
    try:

        service_charge = (amount * commission) / 100
        service_charge = round(service_charge, 2)
        return service_charge

    except Exception as e:
        print(e)


def calculateCommissionTaxSeller(amount: float, tax: float):
    try:

        service_charge_tax = (amount * tax) / 100
        service_charge_tax = round(service_charge_tax, 2)
        return service_charge_tax

    except Exception as e:
        print(e)


def calculateSellerTcsAmount(amount: float, tax: float):
    try:
        tcs_amount = (amount * tax) / 100
        tcs_amount = round(tcs_amount, 2)
        return tcs_amount

    except Exception as e:
        print(e)


def calculateSellerTdsAmount(amount: float, tax: float):
    try:
        tds_amount = (amount * tax) / 100
        tds_amount = round(tds_amount, 2)
        return tds_amount
    except Exception as e:
        print(e)

# Calculate Old Taxable Amount


def calculateOldTaxableAmount(price: float, tax: float, quantity: int):
    try:
        # Price
        taxable_amount = (price * tax) / 100 + price

        taxable_amount = roundOf(taxable_amount)

        taxable_amount = roundOf(taxable_amount) * 100 / (100 + tax)

        taxable_amount = round((taxable_amount * quantity), 2)
        return taxable_amount

    except Exception as e:
        print(e)


# Old App Data (Calculate old seller payments)
def calculateWithoutTaxAmount(price: float, tax: float, quantity: float, commission: float, commission_tax: float):
    try:
        # Price
        cprice = (price * tax) / 100 + price

        cprice = roundOf(cprice)

        cprice = cprice * 100 / (100 + tax)

        cprice = round((cprice * quantity), 2)

        # Commission
        commission = (cprice * commission) / 100
        total_commission = (commission * commission_tax) / 100

        thiscommission = (commission + total_commission)

        final_price = (cprice - thiscommission)

        pprice = (price * tax) / 100 + price

        pprice = roundOf(pprice)

        pprice = (pprice * 100) / (100 + tax)
        tota_amount = round((pprice * quantity), 2)

        total_commision = (tota_amount - final_price)
        total_commision = round(total_commision, 2)

        return {"total_amount": tota_amount, "total_commisison": total_commision}
    except Exception as e:
        print(e)


def calculateTotalWithTax(price: float, tax: float, quantity: float, commission: float, commission_tax: float):
    try:
        cat_commission = commission
        cat_commission_tax = commission_tax

        # Get Commission
        cprice = (price * tax) / 100 + price
        cprice = (round(cprice) * 100) / (100 + tax)
        cprice = round(cprice, 2)

        cprice = cprice + (cprice * tax / 100)
        cprice = roundOf(cprice) * quantity

        commission = (cprice * commission) / 100

        total_commission = (commission * 18) / 100

        marketplace = (cprice * 5) / 100
        marketplace_fee = (marketplace * 18) / 100
        thismarketplace = (marketplace + marketplace_fee)

        thiscommission = (commission + total_commission)
        final_price = (cprice - thiscommission - thismarketplace)

        pprice = (price * tax) / 100 + price
        pprice = (roundOf(pprice) * 100) / (100 + tax)
        pprice = round(pprice, 2)

        pprice = pprice + (pprice * tax / 100)
        tota_amount = round(pprice) * quantity

        total_commision = (tota_amount - final_price)

        # Commission Amount
        comprice = (price * tax) / 100 + price
        comprice = (roundOf(comprice) * 100) / (100 + tax)

        com_totat_amount = comprice * quantity
        com_totat_amount = round(com_totat_amount, 2)

        commission_amount = (com_totat_amount * cat_commission) / 100
        commission_amount = commission_amount

        # Commission Tax Amount
        tax_amount_on_commission = (commission_amount * 18) / 100
        tax_amount_on_commission = tax_amount_on_commission

        return {"tota_amount": tota_amount, "total_commision": total_commision, "commission_amount": commission_amount, "tax_amount_on_commission": tax_amount_on_commission}

    except Exception as e:
        print(e)


def calculateTotal(price: float, tax: float, quantity: float, commission: float, commission_tax: float):
    try:

        cat_commission = commission
        cat_commission_tax = commission_tax

        # Get Commission
        cprice = (price * tax) / 100 + price

        cprice = roundOf(cprice)

        cprice = (cprice * 100) / (100 + tax)
        cprice = round(cprice, 2)

        cprice = cprice + (cprice * tax / 100)

        cprice = roundOf(cprice)

        cprice = cprice * quantity

        commission = (cprice * commission) / 100

        total_commission = (commission * 18) / 100

        thiscommission = (commission + total_commission)

        final_price = (cprice - thiscommission)

        pprice = (price * tax) / 100 + price

        pprice = roundOf(pprice)

        pprice = (pprice * 100) / (100 + tax)
        pprice = round(pprice, 2)

        pprice = pprice + (pprice * tax / 100)

        pprice = roundOf(pprice)

        tota_amount = pprice * quantity

        total_commision = (tota_amount - final_price)

        # Commission Amount
        comprice = (price * tax) / 100 + price

        comprice = roundOf(comprice)

        comprice = (comprice * 100) / (100 + tax)

        com_totat_amount = comprice * quantity
        com_totat_amount = round(com_totat_amount, 2)

        commission_amount = (com_totat_amount * cat_commission) / 100
        commission_amount = commission_amount

        # Commission Tax Amount
        tax_amount_on_commission = (commission_amount * 18) / 100
        tax_amount_on_commission = tax_amount_on_commission
        return {"tota_amount": tota_amount, "total_commision": total_commision, "commission_amount": commission_amount, "tax_amount_on_commision": tax_amount_on_commission}
    except Exception as e:
        print(e)


# Calculate Taxable Amount for Buyer Item Wise
def calculateTaxableAmountItemWise(app_version: str, price: float, tax: float, quantity: int, commission: float, gst_on_commission: float, tcs_rate: float, tds_rate: float, round_of: int):

    if(app_version == 'V4'):
        new_total_commission = (
            commission * gst_on_commission) / 100 + commission
        new_total_commission = (
            new_total_commission + tcs_rate + tds_rate) + 100

        asezCommission = (price * new_total_commission) / 100
        PriceTax = (price * tax) / 100
        pprice = (price + asezCommission + PriceTax)

        pprice = round(pprice, 2)

        pprice = pprice * quantity

    else:

        if(round_of == 1):

            new_total_commission = (
                commission * gst_on_commission) / 100 + commission
            new_total_commission = (
                new_total_commission + tcs_rate + tds_rate) + 100

            newproductprice = (price * new_total_commission) / 100
            newproductprice = roundOf(newproductprice)
            pprice = newproductprice + (newproductprice * tax / 100)
            pprice = roundOf(pprice)
            pprice = (pprice * 100) / (tax + new_total_commission)

            pprice = round(pprice, 2)

            pprice = pprice * quantity

        else:

            new_total_commission = (
                commission * gst_on_commission) / 100 + commission
            new_total_commission = (
                new_total_commission + tcs_rate + tds_rate) + 100

            asezCommission = (price * new_total_commission) / 100
            PriceTax = (price * tax) / 100
            pprice = (price + asezCommission + PriceTax)

            pprice = round(pprice, 2)

            pprice = pprice * quantity

    return pprice


# Calculate Discount Amount on Taxable Product Value
def calculateDiscountAmountItemWise(app_version: str, total_amount: float, discount_rate: float):
    try:

        if(app_version == 'V4'):

            discount_amount = (total_amount * discount_rate) / 100
            discount_amount = round(discount_amount, 2)
        else:
            discount_amount = (total_amount * discount_rate) / 100
            discount_amount = round(discount_amount, 2)

        return discount_amount
    except Exception as e:
        print(e)


# Calculate GST ON ITEMS
def calculateGstOnItems(app_version: str, total_amount: float, tax: float):
    try:
        if(app_version == 'V4'):
            gst_on_product = (total_amount * tax) / 100
            gst_on_product = round(gst_on_product, 2)
        else:
            gst_on_product = (total_amount * tax) / 100
            gst_on_product = round(gst_on_product, 2)

        return gst_on_product
    except Exception as e:
        print(e)

# Aseztak Service on Items


def aseztakServiceOnItems(app_version: str, total_amount: float, rate: float, gst_on_rate: float, tcs_rate: float, tds_rate: float):
    try:
        if(app_version == 'V4'):

            asez_service = (total_amount * rate) / 100
            asez_service_gst_on_rate = (
                round(asez_service, 2) * gst_on_rate) / 100

            asez_service_tcs = (total_amount * tcs_rate) / 100
            asez_service_tds = (total_amount * tds_rate) / 100
            asez_service = (asez_service + asez_service_tds + asez_service_tcs)

        else:

            asez_service = (total_amount * rate) / 100
            asez_service_gst_on_rate = (
                round(asez_service, 2) * gst_on_rate) / 100

            asez_service_tcs = (total_amount * tcs_rate) / 100
            asez_service_tds = (total_amount * tds_rate) / 100
            asez_service = (asez_service + asez_service_tds + asez_service_tcs)

            return {"asez_service": round(asez_service, 2), "gst_on_service": round(asez_service_gst_on_rate, 2)}

    except Exception as e:
        print(e)


def currentDate(date):
    currentdate = datetime.strptime(
        date, "%Y-%m-%d %H:%M:%S.%f").strftime('%Y-%m-%d')
    return currentdate
