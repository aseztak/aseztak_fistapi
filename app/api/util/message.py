

from app.resources.strings import *
import requests
import json

WP_API_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0ZGRjNWJkMTU4ZTMxMWIyOWJlZDA0MiIsIm5hbWUiOiJBc2V6dGFrIEJyb2FkY2FzdGluZyIsImFwcE5hbWUiOiJBaVNlbnN5IiwiY2xpZW50SWQiOiI2NGNhNTJlNWEzOTc5MTUyYzkxNDYxMjkiLCJhY3RpdmVQbGFuIjoiQkFTSUNfTU9OVEhMWSIsImlhdCI6MTY5MjI2MzczN30.PG_Lit0zm35qiy4rmfzjir-qb8YgUSiDuwf0BFI9gVI"


class Message:

    #SEND ORDER FAILED MSZ ON WHATSAPP
    async def sendorderfailedmsz(name: str, mobile: str):
        try:
            payload = {
                "apiKey": WP_API_KEY,
                "campaignName": "ORDERFAILED",
                "destination": str('+91')+str(mobile),
                "userName": str(name),
                "source": "",
                "media": {
                    "url": "",
                    "filename": ""
                },
                "templateParams": [
                    str(name)
                ],
                "tags": [

                ],
                "attributes": {
                    "attribute_name": ""
                }
            }
            headers = {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }

            response = requests.request(
                "POST", 'https://backend.aisensy.com/campaign/t1/api', json=payload, headers=headers)
            return response.text
        except Exception as e:
            print(e)

    # SEND WHATSAPP MSZ
    async def sendorderconfirmationwhatsappmsz(name: str, mobile: str, order_number: str, payable_amount: str, payment_method: str):
        try:
            payload = {
                "apiKey": WP_API_KEY,
                "campaignName": "ORDER_CONFIRMATION",
                "destination": str('+91')+str(mobile),
                "userName": str(name),
                "source": "",
                "media": {
                    "url": "",
                    "filename": ""
                },
                "templateParams": [
                    str(name),
                    str(order_number),
                    str(payable_amount),
                    str(payment_method),
                    "7479111222",
                    "mail@aseztak.com"

                ],
                "tags": [

                ],
                "attributes": {
                    "attribute_name": ""
                }
            }
            headers = {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }

            response = requests.request(
                "POST", 'https://backend.aisensy.com/campaign/t1/api', json=payload, headers=headers)
            return response.text
        except Exception as e:
            print(e)

    # Send MEssage to Buyer
    async def SendOrderPlaceMessagetoBuyer(mobile: str, message: str, order_number: str):
        try:
            # Send Messag to Buyer
            message_data = {
                "flow_id": ORDER_MESSAGE_BUYER_TEMPLATE_ID,
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "PRODUCT_NAME": message,
                "ORDER_ID": order_number
            }
            message_response = requests.post(MESSAGE_API_ENDPOINT_MSG91, json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str(AUTHKEY)}).json()

            return message_response

        except Exception as e:
            print(e)

    # Send Free Delivery Message to Buyer
    async def SendFreeDeliveryMessagetoBuyer(mobile: str, username: str):
        try:
            # Send Messag to Buyer
            message_data = {
                "flow_id": FREE_DELIVERY_MESSAGE_TO_BUYER_TEMPLATE_ID,
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "USER_NAME": username,
                "APP_LINK": "on Aseztak Wholesale App:- https://bit.ly/aseztak-home"
            }
            message_response = requests.post(MESSAGE_API_ENDPOINT_MSG91, json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str(AUTHKEY)}).json()

            return message_response

        except Exception as e:
            print(e)

    # Send Notification to Buyer
    async def SendOrderNotificationtoBuyer(user_token: str, body: str, title: str, path: str, image: str, order_id: int = 0):
        try:

            payload = {
                "notification": {
                    "body": body,
                    "title": title,
                    "image": image
                },
                "priority": "high",
                "data": {
                    "id": order_id,
                    "page": path,
                    "image": image
                },
                "to": user_token,
                "content_availaable": True,
                "apns-priority": 5,
            }

            url = 'https://fcm.googleapis.com/fcm/send'
            fcm_token = FCM_TOKEN

            headers = {
                "Authorization": fcm_token[0],
                "Content-Type": "application/json",
                "Accept": "application/json"
            }

            response = requests.request(
                "POST", url, json=payload, headers=headers)

            response = json.loads(response.content.decode('utf-8'))

            return response
        except Exception as e:
            print(e)

    # Send MEssage to Seller

    async def SendOrderPlaceMessagetoSeller(mobile: str, message: str, order_number: str):
        try:
            # Send Messag to Buyer
            message_data = {
                "flow_id": ORDER_MESSAGE_SELLER_TEMPLATE_ID,
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "PRODUCT_NAME": message,
                "ORDER_ID": order_number
            }
            message_response = requests.post(MESSAGE_API_ENDPOINT_MSG91, json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str(AUTHKEY)}).json()

            return message_response

        except Exception as e:
            print(e)

    # Send Notification to Seller

    async def SendOrderNotificationtoSeller(user_token: str, body: str, title: str, path: str, image: str, order_id: int = 0):
        try:

            payload = {
                "notification": {
                    "body": body,
                    "title": title,
                    "image": image
                },
                "priority": "high",
                "data": {
                    "id": order_id,
                    "page": path,
                    "image": image
                },
                "to": user_token,
                "content_availaable": True,
                "apns-priority": 5,
            }

            url = 'https://fcm.googleapis.com/fcm/send'
            fcm_token = FCM_TOKEN

            headers = {
                "Authorization": fcm_token[0],
                "Content-Type": "application/json",
                "Accept": "application/json"
            }

            response = requests.request(
                "POST", url, json=payload, headers=headers)

            response = json.loads(response.content.decode('utf-8'))

            return response
        except Exception as e:
            print(e)

    # Send Order Cancelled Message to Buyer

    async def SendOrderCancelMessagetoBuyer(mobile: str, message: str, order_number: str, buyer: str):
        try:
            # Send Messag to Buyer

            message_data = {
                "flow_id": ORDER_CANCELLED_TEMPLATE_ID,
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "PRODUCT_NAME": message,
                "ORDER_ID": order_number,
                "REASON": buyer
            }
            message_response = requests.post(MESSAGE_API_ENDPOINT_MSG91, json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str(AUTHKEY)}).json()

            return message_response

        except Exception as e:
            print(e)

    async def SendOrderItemCancelMessagetoBuyer(mobile: str, message: str, order_number: str, buyer: str):
        try:
            # Send Messag to Buyer

            message_data = {
                "flow_id": ORDER_CANCELLED_ITEM_TEMPLATE_ID,
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "ITEM_NAME": message,
                "ORDER_ID": order_number,
                "REASON": buyer
            }
            message_response = requests.post(MESSAGE_API_ENDPOINT_MSG91, json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str(AUTHKEY)}).json()

            return message_response

        except Exception as e:
            print(e)

    # Ordered Delivered Message
    async def SendOrderDeliveredMessagetoBuyer(mobile: str, message: str, order_number: str, delivered_date: str):
        try:
            # Send Messag to Buyer
            message_data = {
                "flow_id": ORDER_DELIVERED_TEMPLATE_ID,
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "PRODUCT_NAME": message,
                "ORDER_ID": order_number,
                "DATE_TIME": delivered_date
            }
            message_response = requests.post(MESSAGE_API_ENDPOINT_MSG91, json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str(AUTHKEY)}).json()

            return message_response

        except Exception as e:
            print(e)

    # Ordered Shipped Message
    async def SendOrderShippedMessagetoBuyer(mobile: str, message: str, order_number: str):
        try:
            # Send Messag to Buyer
            message_data = {
                "flow_id": ORDER_SHIPPED_TEMPLATE_ID,
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "PRODUCT_NAME": message,
                "ORDER_ID": order_number,

            }
            message_response = requests.post(MESSAGE_API_ENDPOINT_MSG91, json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str(AUTHKEY)}).json()

            return message_response

        except Exception as e:
            print(e)

    # New Order Message to New Order Seller
    #New Order: Dear ##USER_NAME## you have new order request, Kindly process it. ##LINK##
    async def SendNewOrderMessageToSeller(mobile: str, user_name: str):
        try:
            # Send Messag to Buyer
            message_data = {
                "flow_id": NEW_ORDER_MSZ_TEMPLATE_ID,
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "USER_NAME": user_name,
                "LINK": 'http://onelink.to/aseztakseller',

            }
            message_response = requests.post(MESSAGE_API_ENDPOINT_MSG91, json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str(AUTHKEY)}).json()

            return message_response

        except Exception as e:
            print(e)

    # Send return declined message
    async def SendOrderReturnDeclinedMessageToBuyer(mobile: str, product_name: str, order_id: str):
        try:
            # Send Messag to Buyer
            message_data = {
                "flow_id": RETURN_DECLINED_MESSAGE,
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "PRODUCT_NAME": str(product_name),
                "ORDER_ID": str(order_id)

            }
            message_response = requests.post(MESSAGE_API_ENDPOINT_MSG91, json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str(AUTHKEY)}).json()

            return message_response

        except Exception as e:
            print(e)
