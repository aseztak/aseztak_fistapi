

from fastapi import Depends
from starlette.requests import Request
from requests import Session
from app.services.auth import auth
from app.db.config import get_db
from app.db.models.products import ProductModel
from app.db.models.order_items import OrderItemsModel
from app.db.models.orders import OrdersModel
from app.db.models.admin import AdminModel


class CheckUser:

    def checkSeller(request: Request, product_id: int, db: Session):

        # User
        checkuser = auth(request=request)

        # Product
        product = db.query(ProductModel).filter(ProductModel.userid == checkuser['id']).filter(
            ProductModel.id == product_id).first()

        if(product is None):
            return False
        else:
            return True

    def CheckSellerOrder(request: Request, order_id: int, db: Session):
        # User
        checkuser = auth(request=request)

        # Order
        checkOrder = db.query(OrdersModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(
            ProductModel, ProductModel.id == OrderItemsModel.product_id).filter(OrdersModel.id == order_id).filter(ProductModel.userid == checkuser['id']).first()

        if(checkOrder is None):
            return False
        else:
            return True

    def CheckBuyerOrder(request: Request, order_id: int, db: Session):
        # Check user
        checkuser = auth(request=request)

        # Order
        checkOrder = db.query(OrdersModel).filter(OrdersModel.id == order_id).filter(
            OrdersModel.user_id == checkuser['id']).first()

        if(checkOrder is None):
            return False
        else:
            return True

    def checkAdmin(request: Request, db: Session):
        # check Admin
        checkUser = auth(request=request)

        # Check Admin
        checkAdmin = db.query(AdminModel).filter(
            AdminModel.email == checkUser['email']).first()

        if(checkAdmin is None):
            return False
        else:
            return True
