
from sqlalchemy.orm.session import Session
from app.db.models.aseztak_service import AseztakServiceModel
from app.db.models.category_commission import CategoryCommissionModel


class Services:

    def aseztak_services(commission_date: str, db: Session):
        commission_date = commission_date.strftime("%Y-%m-%d")

        try:
            service = db.query(AseztakServiceModel).filter(
                AseztakServiceModel.commission_date <= commission_date).order_by(AseztakServiceModel.id.desc()).first()

            return service
        except Exception as e:
            print(e)

    def commission_seller(commission_date: str, category_id: int, db: Session):

        try:
            commission = db.query(CategoryCommissionModel).filter(CategoryCommissionModel.category_id == category_id).filter(
                CategoryCommissionModel.start_date <= commission_date).order_by(CategoryCommissionModel.id.desc()).first()

            return commission
        except Exception as e:
            print(e)
