from datetime import datetime
from urllib.parse import quote
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker
# from sqlalchemy.ext.declarative import as_declarative
from sqlalchemy import Column, Integer, String, TIMESTAMP, ForeignKey, func, Float
import requests
import json
import pymysql
from sqlalchemy.sql.elements import Null
import asyncio
from sqlalchemy.orm.session import Session


pymysql.install_as_MySQLdb()

PWD = quote("Asez@0107#")
USR = "asez"
HOST = "172.105.62.141"
PORT = 3030
DATABASE = "aseztak"
# PWD = quote("Asez@321")
# USR = "root"
# HOST = "192.168.0.106"
# PORT = 3030
# DATABASE = "aseztak_fastapi7"

# UPDATE ORDER STATUS EVERY 30 MINUTES

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}/{}'.format(
    USR, PWD, HOST, DATABASE)

engine = create_engine(SQLALCHEMY_DATABASE_URI, future=True,
                       echo=True, pool_pre_ping=True, pool_recycle=300)


SessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine, expire_on_commit=False)


Base = declarative_base()

# TABLES (MODELS)


class UserModel(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String, default='')
    email = Column(String, default='')
    region = Column(String, default='')
    city = Column(String, default='')
    country = Column(String, default='')
    pincode = Column(String, default='')
    otp = Column(String)
    mobile = Column(String, unique=True, index=True)
    fcm_token = Column(String)


class OrdersModel(Base):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer)
    order_number = Column(String)
    payment_status = Column(Integer)
    payment_method = Column(String)
    address_id = Column(Integer)
    created_at = Column(TIMESTAMP)


class OrderStatusModel(Base):
    __tablename__ = 'order_status'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)


class OrderShippingModel(Base):
    __tablename__ = 'shipping'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)

    wbns = Column(String)

    courier_partner = Column(String)

    slip = Column(String)

    invoice = Column(String)

    status = Column(Integer)

    remark = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)


class OrderItemsModel(Base):
    __tablename__ = 'order_items'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)
    product_id = Column(Integer)
    images = Column(String, default=Null)
    status = Column(Integer)
    refund = Column(Integer, default=0)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)


class ShippingAddressModel(Base):
    __tablename__ = 'buyer_address'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    user_id = Column(Integer, ForeignKey('users.id'), index=True)

    ship_to = Column(String)

    phone = Column(String)

    alt_phone = Column(String)

    pincode = Column(String)

    address = Column(String)

    locality = Column(String)

    city = Column(String)

    default_address = Column(Integer)

    state = Column(String)

    country = Column(String)

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)


class ProductModel(Base):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    userid = Column(Integer, ForeignKey('users.id'), index=True)
    brand_id = Column(Integer, default=None)
    title = Column(String)
    slug = Column(String, unique=True)
    category = Column(Integer)
    short_description = Column(String)
    status = Column(Integer, default=1)
    featured = Column(Integer, default=0)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)


class ProductPricingModel(Base):
    __tablename__ = 'product_pricing'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    product_id = Column(Integer, ForeignKey('products.id'), index=True)
    price = Column(Float)
    mrp = Column(Float)
    tax = Column(Float)
    moq = Column(Integer)
    items = Column(Integer)
    unit = Column(String)
    hsn = Column(String)
    description = Column(String)
    default_image = Column(Integer, default=0)
    deleted_at = Column(TIMESTAMP, default=None)


class ProductMediaModel(Base):
    __tablename__ = 'media'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    model_id = Column(Integer, ForeignKey('products.id'), index=True)

    collection_name = Column(String)

    name = Column(String)

    file_name = Column(String)

    file_path = Column(String)

    default_img = Column(Integer, default=0)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)

    deleted_at = Column(TIMESTAMP)


class OrderRevreseModel(Base):
    __tablename__ = 'reverse_order'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)

    wbns = Column(String)

    courier_partner = Column(String)

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)


# Messaging
class Message:
    # Send Out for Delivery Message to Buyer
    def SendOutforDeliveryMessagetoBuyer(mobile: str, product: str, order_number: str, delivered_at: str, tracking_link: str):
        try:
            # Send Messag to Buyer
            message_data = {
                "flow_id": '61b5c9bd030d3b6631021cd4',
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "PRODUCT_NAME": product,
                "ORDER_ID": order_number,
                "DATE_TIME": delivered_at,
                'LINK': tracking_link
            }
            message_response = requests.post('https://api.msg91.com/api/v5/flow/', json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str('347198A1zlxIECi5fb23f5aP1')}).json()

            return message_response

        except Exception as e:
            print(e)

    # Send Order Delivered Message to Buyer
    def SendOrderDeliveredMessagetoBuyer(mobile: str, product: str, order_number: str, delivered_at: str):
        try:
            # Send Messag to Buyer
            message_data = {
                "flow_id": '606af414de02f4145d000d52',
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "PRODUCT_NAME": product,
                "ORDER_ID": order_number,
                "DATE_TIME": delivered_at
            }
            message_response = requests.post('https://api.msg91.com/api/v5/flow/', json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str('347198A1zlxIECi5fb23f5aP1')}).json()

            return message_response

        except Exception as e:
            print(e)

    # Send Shipped Order Message to Buyer
    def SendOrderShippedMessagetoBuyer(mobile: str, message: str, order_number: str):
        try:
            # Send Messag to Buyer
            message_data = {
                "flow_id": '606ac16f5eab5f3d004d8f79',
                "sender": 'ASEZTK',
                "mobiles": '91'+str(mobile),
                "PRODUCT_NAME": message,
                "ORDER_ID": order_number,

            }
            message_response = requests.post('https://api.msg91.com/api/v5/flow/', json.dumps(message_data), headers={
                "Content-type": "application/json",
                "authkey": str('347198A1zlxIECi5fb23f5aP1')}).json()

            return message_response

        except Exception as e:
            print(e)

# Notification
# Send Notification to Buyer


FCM_TOKEN = 'key=AAAAzdN3OzE:APA91bHpBCu-SDR_ThhTau0ipewzis-z8rzxsd2jLdjU0KL25hADh4xw-tdVeE-2RotY6-_sp4UpYeH_PJP6vmfdROKt24xiIP6hdRCERAcI0QE0AyH-8THDB7hWFYjOB8AQ27bq6RKh',

WP_API_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0ZGRjNWJkMTU4ZTMxMWIyOWJlZDA0MiIsIm5hbWUiOiJBc2V6dGFrIEJyb2FkY2FzdGluZyIsImFwcE5hbWUiOiJBaVNlbnN5IiwiY2xpZW50SWQiOiI2NGNhNTJlNWEzOTc5MTUyYzkxNDYxMjkiLCJhY3RpdmVQbGFuIjoiQkFTSUNfTU9OVEhMWSIsImlhdCI6MTY5MjI2MzczN30.PG_Lit0zm35qiy4rmfzjir-qb8YgUSiDuwf0BFI9gVI"


class WhatsappMsz:

    async def sendshippedwpmsz(name: str, mobile: str, order_number: str, tracking_number: str, tracking_link: str):
        try:
            payload = {
                "apiKey": WP_API_KEY,
                "campaignName": "ORDERSHIPPED",
                "destination": str('+91')+str(mobile),
                "userName": str(name),
                "source": "",
                "media": {
                    "url": "",
                    "filename": ""
                },
                "templateParams": [
                    str(name),
                    str(order_number),
                    str(tracking_number),
                    str(tracking_link),
                    "7479111222",
                    "mail@aseztak.com"

                ],
                "tags": [

                ],
                "attributes": {
                    "attribute_name": ""
                }
            }
            headers = {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }

            response = requests.request(
                "POST", 'https://backend.aisensy.com/campaign/t1/api', json=payload, headers=headers)
            return response.text
        except Exception as e:
            print(e)

    async def senddeliverywpmsz(mobile: str, name: str, order_number: str, product: str, payable_amount: str):
        try:
            payload = {
                "apiKey": WP_API_KEY,
                "campaignName": "OUT_FOR_DELIVERY",
                "destination": str('+91')+str(mobile),
                "userName": str(name),
                "source": "",
                "media": {
                    "url": "",
                    "filename": ""
                },
                "templateParams": [
                    str(name),
                    str(order_number),
                    str(product),
                    str(payable_amount),
                    "7479111222",
                    "mail@aseztak.com"

                ],
                "tags": [

                ],
                "attributes": {
                    "attribute_name": ""
                }
            }
            headers = {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }

            response = requests.request(
                "POST", 'https://backend.aisensy.com/campaign/t1/api', json=payload, headers=headers)
            return response.text
        except Exception as e:
            print(e)


class Notification:
    async def SendOrderNotificationtoBuyer(user_token: str, body: str, title: str, path: str, image: str, order_id: int = 0):
        try:

            payload = {
                "notification": {
                    "body": body,
                    "title": title,
                    "image": image
                },
                "priority": "high",
                "data": {
                    "id": order_id,
                    "page": path,
                    "image": image
                },
                "to": user_token,
                "content_availaable": True,
                "apns-priority": 5,
            }

            url = 'https://fcm.googleapis.com/fcm/send'
            fcm_token = FCM_TOKEN

            headers = {
                "Authorization": fcm_token[0],
                "Content-Type": "application/json",
                "Accept": "application/json"
            }

            response = requests.request(
                "POST", url, json=payload, headers=headers)

            response = json.loads(response.content.decode('utf-8'))

            return response
        except Exception as e:
            print(e)


# Check Packed Order Status
def check_packed_order_status_every_hours():
    db: Session = SessionLocal()
    try:
        check_shipped_orders = db.query(OrdersModel, OrderShippingModel).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).join(
            OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
            OrderShippingModel.courier_partner == 'DELHIVERY').having(func.max(OrderStatusModel.status) == 30).group_by(OrderStatusModel.order_id).all()

        for order in check_shipped_orders:
            # Order Items
            order_items = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == order.OrdersModel.id).filter(OrderItemsModel.status != 980)
            count_order_items = order_items.count()

            # Single Image Item
            imageitem = order_items.first()
            productdata = db.query(ProductModel).filter(
                ProductModel.id == imageitem.product_id).first()

            # BUYER ADDRESS
            buyerAddress = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.id == order.OrdersModel.address_id).first()

            # Buyer
            user = db.query(UserModel).filter(
                UserModel.id == order.OrdersModel.user_id).first()

            # Order Items
            count_items = 0
            if (count_order_items > 1):
                count_items = count_order_items - 1

            # Customize Product Image
            filename = ''
            if (imageitem.images != None):
                image = db.query(ProductMediaModel).filter(
                    ProductMediaModel.id == imageitem.images.strip("[]")).first()

                if (image is not None):
                    filename = image.file_path
                else:
                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == imageitem.product_id).filter(
                        ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                    filename = image.file_path

            else:
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == imageitem.product_id).filter(
                    ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                if (image is not None):
                    filename = image.file_path

            img = filename
            if (img is None):
                img = ''

            product_title = productdata.title[:16]
            if (count_items == 0):
                product_title = product_title
            else:
                product_title = str(product_title)+' and ' + \
                    str(count_items) + str(' more')

            if (order.OrderShippingModel.wbns is not None):

                url = 'https://track.delhivery.com/api/v1/packages/json/?waybill='
                token = '4b7022d7f350abad9312bbf36e400af9f834fde1'

                packing_slip_url = f"{url}"+order.OrderShippingModel.wbns

                ShipmentData = requests.get(
                    packing_slip_url, headers={
                        "Content-type": "application/json",
                        "Authorization": 'Token '+str(token)}).json()

                if ('ShipmentData' in str(ShipmentData)):
                    shipment = ShipmentData['ShipmentData'][0]['Shipment']['Status']
                    if (shipment['Status'] == 'In Transit'):

                        # Check Order latest status
                        check_status = db.query(OrderStatusModel).filter(
                            OrderStatusModel.order_id == order.OrdersModel.id).order_by(OrderStatusModel.status.desc()).first()

                        if (check_status.status == 20 or check_status.status == 30):
                            db_status = OrderStatusModel(
                                order_id=order.OrdersModel.id,
                                status=40,
                                created_at=datetime.now(),
                                updated_at=datetime.now()
                            )
                            db.add(db_status)
                            db.commit()

                            # Send Notification to Buyer
                            b_notification_body = 'Order for ' + str(product_title) + ' with Order ID ' + str(
                                order.OrdersModel.order_number) + ' has been packed.'
                            b_notification_title = 'Order Packed'
                            b_path = '/order-detail'
                            b_notification_image = img

                            if (user.fcm_token is not None):
                                asyncio.run(Notification.SendOrderNotificationtoBuyer(
                                    user_token=user.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=order.OrdersModel.id))

        return "SUCCESSFULLY UPDATED"
    except Exception as e:
        print(e)
    finally:
        db.close()    


# Check Shipped Order Status
def check_shipped_order_status_every_hours():
    db: Session = SessionLocal()
    try:
        check_shipped_orders = db.query(OrdersModel, OrderShippingModel).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).filter(
            OrderShippingModel.courier_partner == 'DELHIVERY').having(func.max(OrderStatusModel.status) == 40).group_by(OrderStatusModel.order_id).all()

        for order in check_shipped_orders:
            # Order Items
            order_items = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == order.OrdersModel.id).filter(OrderItemsModel.status != 980)
            count_order_items = order_items.count()

            # Single Image Item
            imageitem = order_items.first()
            productdata = db.query(ProductModel).filter(
                ProductModel.id == imageitem.product_id).first()

            # Seller
            seller = db.query(UserModel).filter(
                UserModel.id == productdata.userid).first()

            # BUYER ADDRESS
            buyerAddress = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.id == order.OrdersModel.address_id).first()

            # Buyer
            user = db.query(UserModel).filter(
                UserModel.id == order.OrdersModel.user_id).first()

            # Order Items
            count_items = 0
            if (count_order_items > 1):
                count_items = count_order_items - 1

            # Customize Product Image
            filename = ''
            if (imageitem.images != None):
                image = db.query(ProductMediaModel).filter(
                    ProductMediaModel.id == imageitem.images.strip("[]")).first()

                if (image is not None):
                    filename = image.file_path
                else:
                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == imageitem.product_id).filter(
                        ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                    filename = image.file_path

            else:
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == imageitem.product_id).filter(
                    ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                if (image is not None):
                    filename = image.file_path

            img = filename
            if (img is None):
                img = ''

            product_title = productdata.title[:16]
            if (count_items == 0):
                product_title = product_title
            else:
                product_title = str(product_title)+' and ' + \
                    str(count_items) + str(' more')

            if (order.OrderShippingModel.wbns is not None):
                url = 'https://track.delhivery.com/api/v1/packages/json/?waybill='
                token = '4b7022d7f350abad9312bbf36e400af9f834fde1'

                packing_slip_url = f"{url}"+order.OrderShippingModel.wbns

                ShipmentData = requests.get(
                    packing_slip_url, headers={
                        "Content-type": "application/json",
                        "Authorization": 'Token '+str(token)}).json()

                if ('ShipmentData' in str(ShipmentData)):
                    shipment = ShipmentData['ShipmentData'][0]['Shipment']['Status']
                    if (shipment['Status'] == 'In Transit'):
                        # Update Order Status
                        db.add_all([
                            OrderStatusModel(
                                order_id=order.OrdersModel.id,
                                status=50,
                                created_at=datetime.now(),
                                updated_at=datetime.now()
                            ),

                            OrderStatusModel(
                                order_id=order.OrdersModel.id,
                                status=60,
                                created_at=datetime.now(),
                                updated_at=datetime.now()
                            )
                        ])
                        db.commit()

                        # Send Notification to Buyer
                        if (user.fcm_token is not None):
                            b_notification_body = 'Order for ' + str(product_title) + ' with Order ID ' + str(
                                order.OrdersModel.order_number) + ' has been Shipped.'
                            b_notification_title = 'Order Shipped'
                            b_path = '/order-detail'
                            b_notification_image = img

                            asyncio.run(Notification.SendOrderNotificationtoBuyer(
                                user_token=user.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=order.OrdersModel.id))

                        # SEND WP MSZ
                        tracking_link = "https://www.delhivery.com/track/package/" + \
                            str(order.OrderShippingModel.wbns)
                        asyncio.run(WhatsappMsz.sendshippedwpmsz(
                            name=buyerAddress.ship_to, mobile=buyerAddress.phone, order_number=order.OrdersModel.order_number, tracking_number=order.OrderShippingModel.wbns, tracking_link=tracking_link))

                        # Send Notification to Seller
                        if (seller.fcm_token is not None):
                            b_notification_body = 'Order for ' + str(product_title) + ' with Order ID ' + str(
                                order.OrdersModel.order_number) + ' has been Shipped.'
                            b_notification_title = 'Order Shipped'
                            b_path = '/order-detail'
                            b_notification_image = img

                            asyncio.run(Notification.SendOrderNotificationtoBuyer(
                                user_token=seller.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=order.OrdersModel.id))

                        # Send Message
                        Message.SendOrderShippedMessagetoBuyer(
                            mobile=buyerAddress.phone, message=product_title, order_number=order.OrdersModel.order_number)

        return "SUCCESSFULLY UPDATED"
    except Exception as e:
        print(e)
    finally:
        db.close()    
# Check Delivered Order Status


def check_delivered_order_status_every_hours():
    db: Session = SessionLocal()
    try:
        # Get Shipped Orders
        shipped_orders = db.query(OrdersModel, OrderShippingModel).join(OrderStatusModel, OrderStatusModel.order_id == OrdersModel.id).join(OrderShippingModel, OrderShippingModel.order_id == OrdersModel.id).filter(OrderShippingModel.courier_partner == 'DELHIVERY').having(
            func.max(OrderStatusModel.status) == 60).group_by(OrderStatusModel.order_id).all()

        for order in shipped_orders:
            # Order Items
            order_items = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == order.OrdersModel.id).filter(OrderItemsModel.status != 980)
            count_order_items = order_items.count()

            # Single Image Item
            imageitem = order_items.first()
            productdata = db.query(ProductModel).filter(
                ProductModel.id == imageitem.product_id).first()

            # Seller
            seller = db.query(UserModel).filter(
                UserModel.id == productdata.userid).first()

            # BUYER ADDRESS
            buyerAddress = db.query(ShippingAddressModel).filter(
                ShippingAddressModel.id == order.OrdersModel.address_id).first()

            # Buyer
            user = db.query(UserModel).filter(
                UserModel.id == order.OrdersModel.user_id).first()

            # Order Items
            count_items = 0
            if (count_order_items > 1):
                count_items = count_order_items - 1

            # Customize Product Image
            filename = ''
            if (imageitem.images != None):
                image = db.query(ProductMediaModel).filter(
                    ProductMediaModel.id == imageitem.images.strip("[]")).first()

                if (image is not None):
                    filename = image.file_path
                else:
                    image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == imageitem.product_id).filter(
                        ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                    filename = image.file_path

            else:
                image = db.query(ProductMediaModel).filter(ProductMediaModel.model_id == imageitem.product_id).filter(
                    ProductMediaModel.deleted_at.is_(None)).order_by(ProductMediaModel.default_img.desc()).first()

                if (image is not None):
                    filename = image.file_path

            img = filename
            if (img is None):
                img = ''

            product_title = productdata.title[:16]
            wp_product_title = productdata.title
            if (count_items == 0):
                product_title = product_title
                wp_product_title = wp_product_title
            else:
                product_title = str(product_title)+' and ' + \
                    str(count_items) + str(' more')
                wp_product_title = str(wp_product_title) + \
                    ' and '+str(count_items) + str(' more.')

            if (order.OrderShippingModel.wbns is not None):
                url = 'https://track.delhivery.com/api/v1/packages/json/?waybill='
                token = '4b7022d7f350abad9312bbf36e400af9f834fde1'

                packing_slip_url = f"{url}"+order.OrderShippingModel.wbns

                ShipmentData = requests.get(
                    packing_slip_url, headers={
                        "Content-type": "application/json",
                        "Authorization": 'Token '+str(token)}).json()
                # Delivered
                if ('ShipmentData' in str(ShipmentData)):
                    shipment = ShipmentData['ShipmentData'][0]['Shipment']['Status']

                    # Out for Delivery
                    if (order.OrderShippingModel.remark != 'Dispatched'):
                        if (shipment['Status'] == 'Dispatched' and shipment['StatusType'] == 'UD'):
                            tracking_link = "https://www.delhivery.com/track/package/" + \
                                str(order.OrderShippingModel.wbns)

                            # Send Notification
                            b_notification_body = 'Your order for ' + str(product_title) + ' with Order ID ' + str(
                                order.OrdersModel.order_number) + ' is out for delivery, be ready to take the order.'
                            b_notification_title = 'Arriving Today'
                            b_path = '/order-detail'
                            b_notification_image = img

                            if (user.fcm_token is not None):
                                asyncio.run(Notification.SendOrderNotificationtoBuyer(
                                    user_token=user.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=order.OrdersModel.id))

                            payable_amount = 'Pay by Cash/UPI.'
                            if('ONLINE' in order.OrdersModel.payment_method):
                                payable_amount = 'Will be delivered before 9pm today.'

                            # SEND WHATSAPP MSZ
                            asyncio.run(WhatsappMsz.senddeliverywpmsz(
                                mobile=buyerAddress.phone, name=buyerAddress.ship_to, order_number=order.OrdersModel.order_number, product=wp_product_title, payable_amount=payable_amount))

                            # Send Message
                            Message.SendOutforDeliveryMessagetoBuyer(
                                mobile=buyerAddress.phone, product=product_title, order_number=order.OrdersModel.order_number, delivered_at=shipment['StatusDateTime'], tracking_link=tracking_link)

                            # update Dispatched
                            order.OrderShippingModel.remark = 'Dispatched'
                            db.flush()
                            db.commit()

                    if (shipment['Status'] == 'Delivered'):

                        # Update order data
                        order.OrdersModel.payment_status = 51
                        db.flush()
                        db.commit()

                        # Update Status
                        # Check Order Status Delivered
                        db_check_status = db.query(OrderStatusModel).filter(
                            OrderStatusModel.order_id == order.OrdersModel.id).order_by(OrderStatusModel.status.desc()).first()
                        if (db_check_status.status != 70):
                            db_status = OrderStatusModel(
                                order_id=order.OrdersModel.id,
                                status=70,
                                created_at=datetime.now(),
                                updated_at=datetime.now()
                            )
                            db.add(db_status)
                            db.commit()
                            db.refresh(db_status)

                            # Update Order Item Status
                            db.query(OrderItemsModel).filter(OrderItemsModel.order_id == order.OrdersModel.id).filter(
                                OrderItemsModel.status != 980).update({'status': 70})
                            db.commit()

                            # Send Notification to Buyer
                            if (user.fcm_token is not None):
                                b_notification_body = 'Order for ' + str(product_title) + ' with Order ID ' + str(
                                    order.OrdersModel.order_number) + ' has been Delivered'
                                b_notification_title = 'Order Delivered'
                                b_path = '/order-detail'
                                b_notification_image = img
                                asyncio.run(Notification.SendOrderNotificationtoBuyer(
                                    user_token=user.fcm_token, body=b_notification_body, title=b_notification_title, path=b_path, image=b_notification_image, order_id=order.OrdersModel.id))

                            # Send Notification to Seller
                            if (seller.fcm_token is not None):
                                s_notification_body = 'Order for ' + str(product_title) + ' with Order ID ' + str(
                                    order.OrdersModel.order_number) + ' has been Delivered'
                                s_notification_title = 'Order Delivered'
                                s_path = '/order-detail'
                                s_notification_image = img
                                asyncio.run(Notification.SendOrderNotificationtoBuyer(
                                    user_token=seller.fcm_token, body=s_notification_body, title=s_notification_title, path=s_path, image=s_notification_image, order_id=order.OrdersModel.id))

                            # Send Message to Buyer
                            Message.SendOrderDeliveredMessagetoBuyer(
                                mobile=buyerAddress.phone, product=product_title, order_number=order.OrdersModel.order_number, delivered_at=db_status.created_at)

                            # Send Message to Seller
                            Message.SendOrderDeliveredMessagetoBuyer(
                                mobile=seller.mobile, product=product_title, order_number=order.OrdersModel.order_number, delivered_at=db_status.created_at)

        return "SUCCESSFULLY UPDATED"
    except Exception as e:
        print(e)
    finally:
        db.close()    
# Check Return Order Status


def check_return_order_status_every_hours():
    db: Session = SessionLocal()
    try:
        # Get Shipped Orders
        return_orders = db.query(OrdersModel, OrderRevreseModel).join(OrderItemsModel, OrderItemsModel.order_id == OrdersModel.id).join(OrderRevreseModel, OrderRevreseModel.order_id ==
                                                                                                                                        OrdersModel.id).filter(OrderRevreseModel.courier_partner == 'DELHIVERY').filter(OrderItemsModel.status == 90).group_by(OrderItemsModel.order_id).all()

        for order in return_orders:

            # Order Items
            order_items = db.query(OrderItemsModel).filter(
                OrderItemsModel.order_id == order.OrdersModel.id).filter(OrderItemsModel.status == 90)
            count_order_items = order_items.count()

            # Single Image Item
            imageitem = order_items.first()
            productdata = db.query(ProductModel).filter(
                ProductModel.id == imageitem.product_id).first()

            # Order Items
            count_items = 0
            if (count_order_items > 1):
                count_items = count_order_items - 1

            product_title = productdata.title[:16]
            if (count_items == 0):
                product_title = product_title
            else:
                product_title = str(product_title)+' and ' + \
                    str(count_items) + str(' more')

            if (order.OrderRevreseModel.wbns is not None):
                url = 'https://track.delhivery.com/api/v1/packages/json/?waybill='
                token = '4b7022d7f350abad9312bbf36e400af9f834fde1'

                packing_slip_url = f"{url}"+order.OrderRevreseModel.wbns

                ShipmentData = requests.get(
                    packing_slip_url, headers={
                        "Content-type": "application/json",
                        "Authorization": 'Token '+str(token)}).json()
                # Delivered
                if ('ShipmentData' in str(ShipmentData)):
                    shipment = ShipmentData['ShipmentData'][0]['Shipment']['Status']

                    order.OrderRevreseModel.status = shipment['Status']
                    db.flush()
                    db.commit()

                    # Out for Delivery
                    if (shipment['StatusType'] == 'DL'):

                        # Check Current Status
                        check_current_status = db.query(OrderStatusModel).filter(
                            OrderStatusModel.order_id == order.OrdersModel.id).order_by(OrderStatusModel.status.desc()).first()

                        if (check_current_status.status > 70):
                            db_status = OrderStatusModel(
                                order_id=order.OrdersModel.id,
                                status=100,
                                created_at=shipment['StatusDateTime'],
                                updated_at=shipment['StatusDateTime']
                            )
                            db.add(db_status)
                            db.commit()

                            for item in order_items:
                                if (item.refund == 1):
                                    item.status = 110
                                else:
                                    item.status = 100
                                db.flush()
                                db.commit()

                            # Check Item Refund
                            checkItemrefund = db.query(OrderItemsModel).filter(
                                OrderItemsModel.order_id == order.OrdersModel.id).filter(OrderItemsModel.refund == 1).first()

                            if (checkItemrefund):
                                db_statuss = OrderStatusModel(
                                    order_id=order.OrdersModel.id,
                                    status=110,
                                    created_at=datetime.now(),
                                    updated_at=datetime.now()
                                )
                                db.add(db_statuss)
                                db.commit()

                                for item in order_items:
                                    if (item.refund == 1):
                                        item.status = 110
                                        db.flush()
                                        db.commit()

                        else:
                            for item in order_items:
                                if (item.refund == 1):
                                    item.status = 110
                                else:
                                    item.status = 100
                                    item.updated_at = shipment['StatusDateTime']

                                db.flush()
                                db.commit()

                        # Send Message and Notification (PENDING)

        return "SUCCESSFULLY UPDATED"
    except Exception as e:
        print(e)
    finally:
        db.close()    

class TestInsertDataModel(Base):

    __tablename__ = 'test'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    mobile = Column(String)
    status = Column(Integer)
    created_at = Column(TIMESTAMP)


def insertTestData():
    db: Session = SessionLocal()
    try:
        insert_data = TestInsertDataModel(
            name='rahul test',
            mobile='8617518041',
            status=1,
            created_at=datetime.now()
        )
        db.add(insert_data)
        db.commit()
        db.refresh(insert_data)
        return "Added"
    except Exception as e:
        print(e)
    finally:
        db.close()    


if __name__ == '__main__':
    # insertTestData()
    check_packed_order_status_every_hours()
    check_shipped_order_status_every_hours()
    check_delivered_order_status_every_hours()
    check_return_order_status_every_hours()
    print("UPDATED")
