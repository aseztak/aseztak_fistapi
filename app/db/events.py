from fastapi import FastAPI
from app.db.config import engine
import logging

logger = logging.getLogger(__name__)


async def connect_to_db(app: FastAPI) -> None:

    try:
        engine.connect()

    except Exception as e:
        logger.warn("--- DB  CONNECTION ERROR ---")
        logger.warn(e)

    else:
        logger.info("--- DB  CONNECTED SUCCESSFULLY ---")


async def close_db_connection(app: FastAPI) -> None:

    try:
        with engine.connect() as connection:
            if connection:
                connection.close()

    except Exception as e:
        logger.warn("--- DB DISCONNECT ERROR ---")
        logger.warn(e)

    else:
        logger.info("--- DB  CONNECTION CLOSE ---")
