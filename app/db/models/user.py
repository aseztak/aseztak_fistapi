

from sqlalchemy.orm import relationship
from sqlalchemy.sql.elements import Null
from sqlalchemy.sql.sqltypes import TIMESTAMP, Date
from app.db.config import Base
from sqlalchemy import Column, Integer, String, ForeignKey, Enum
from app.db.models.products import ProductModel
from app.db.models.marketing_users_client import MarketerUsersClientModel
from app.db.models.bank_accounts import BankAccountsModel
import enum


class confirmdValue(enum.Enum):
    Yes = 'Yes'
    No = 'No'


class UserModel(Base):

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String, default='')
    email = Column(String, default='')
    region = Column(String, default='')
    city = Column(String, default='')
    country = Column(String, default='')
    pincode = Column(String, default='')
    otp = Column(String)
    mobile = Column(String, unique=True, index=True)
    premium = Column(Enum('0', '1'), default='0')

    status = Column(Integer, index=True)
    confirm = Column(Enum(confirmdValue), default='No')
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    deleted_at = Column(TIMESTAMP, default=None)
    fcm_token = Column(String)
    app_version = Column(String)
    last_login_at = Column(TIMESTAMP)
    last_login_ip = Column(String)
    platform = Column(String, default='')

    role = relationship('UserRoleModel',  back_populates='user', uselist=False)
    profile = relationship(
        'UserProfileModel',  back_populates='user', uselist=False, )

    product = relationship(
        ProductModel, back_populates='seller', uselist=False)

    bank_account = relationship(
        BankAccountsModel, back_populates='user', uselist=False
    )

    marketing_client = relationship(
        MarketerUsersClientModel, back_populates='user', uselist=False)


class Roles(Base):

    __tablename__ = 'roles'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)


class UserRoleModel(Base):

    __tablename__ = 'model_has_roles'

    # id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    role_id = Column(Integer, ForeignKey('roles.id'), primary_key=True)
    model_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    user = relationship('UserModel',  back_populates='role')


class UserProfileModel(Base):

    __tablename__ = 'profiles'

    id = Column(Integer, index=True, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    business_type = Column(String)
    account_type = Column(String)
    account_holder_name = Column(String, default=Null)
    bank = Column(String, default=Null)
    branch = Column(String, default=Null)
    acc_no = Column(String, default=Null)
    ifsc_code = Column(String, default=Null)
    proof_type = Column(String, default=Null)
    proof = Column(String, default='')
    user = relationship('UserModel',  back_populates='profile')


class UserDocumentModel(Base):
    __tablename__ = 'users_document'

    id = Column(Integer, index=True, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    doc_type = Column(String)
    filename = Column(String)
    file_path = Column(String)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    deleted_at = Column(TIMESTAMP)
