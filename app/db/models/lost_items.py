
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class LostItemModel(Base):
    __tablename__ = 'lost_items'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    item_id = Column(Integer, ForeignKey('order_items.id'), index=True)

    remarks = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
