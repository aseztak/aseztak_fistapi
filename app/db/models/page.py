

import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class TypeValue(enum.Enum):
    Seller = 'Seller'
    Buyer = 'Buyer'
    Both = 'Both'


class PageModel(Base):
    __tablename__ = 'pages'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)
    name = Column(String)
    slug = Column(String)
    content = Column(String)
    is_menu = Column(Integer)
    type = Column(Enum(TypeValue), default='Buyer')
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
