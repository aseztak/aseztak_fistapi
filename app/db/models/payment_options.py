

import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, TIMESTAMP, Enum


class payment_option_type(enum.Enum):
    ONLINE = 'ONLINE'
    COD = 'COD'
    PARTIAL = 'PARTIAL'


class PaymentOptionModel(Base):
    __tablename__ = 'payment_options'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    prefix = Column(Enum(payment_option_type), default='ONLINE')

    discount = Column(Float)

    title = Column(String)

    payment_type = Column(String)

    description = Column(String)

    start_date = Column(TIMESTAMP)

    end_date = Column(TIMESTAMP)
