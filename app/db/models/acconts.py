
import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class AccountType(enum.Enum):
    CR = 'CR'
    DR = 'DR'


class AccountHeadType(enum.Enum):
    ORDERED = 'ORDERED'
    CANCELLED = 'CANCELLED'
    RETURNED = 'RETURNED'
    REVERSED = 'REVERSED'
    PAYMENT = 'PAYMENT'
    COMMISION = 'COMMISION'
    TCS = 'TCS'
    TDS = 'TDS'
    LOST = 'LOST'
    DEDUCTION = 'DEDUCTION'
    ADDITION = 'ADDITION'


class AccountsModel(Base):
    __tablename__ = 'accounts'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    user_id = Column(Integer, ForeignKey('users.id'), index=True)

    txn_date = Column(TIMESTAMP)
    head = Column(Enum(AccountHeadType), default='ORDERED')
    txn_description = Column(String)
    remarks = Column(String)
    txn_type = Column(Enum(AccountType), default='DR')

    txn_amount = Column(Float, default=0)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
