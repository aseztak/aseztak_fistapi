from sqlalchemy import Column, Integer, String, ForeignKey

from app.db.config import Base



class LoginModel(Base):

    __tablename__ = 'login'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(256), nullable=True)
    surname = Column(String(256), nullable=True)
    email = Column(String, index=True, nullable=False)
    hashed_password = Column(String, nullable=False)
