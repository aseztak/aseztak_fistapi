from app.db.config import Base
from sqlalchemy import TIMESTAMP, Column, Integer, String, ForeignKey
from app.db.config import Base


class TagModel(Base):

    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    slug = Column(String)
    counter = Column(Integer, default=0)
    status = Column(Integer)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
