from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class AdminModel(Base):
    __tablename__ = 'admins'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    name = Column(String)

    level = Column(String)

    email = Column(String)

    password = Column(String)

    remember_token = Column(String)

    last_login_at = Column(TIMESTAMP)

    last_login_ip = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
