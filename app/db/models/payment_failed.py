

from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class PaymentFailedModel(Base):
    __tablename__ = 'payment_failed'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_fail_id = Column(Integer, ForeignKey('order_fails.id'), index=True)

    # orderfail = relationship(
    #     'OrderFailsModel',  back_populates='payment_failed', uselist=False)

    txn_payment_id = Column(String, default=None)

    txn_order_id = Column(String, default=None)

    txn_status = Column(String, default=None)

    reason = Column(String, default=None)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
