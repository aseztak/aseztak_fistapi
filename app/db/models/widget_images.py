
from sqlalchemy.orm import relationship
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class WidgetImagesModel(Base):
    __tablename__ = 'widget_images'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    widget_id = Column(Integer, ForeignKey('widgets.id'), index=True)

    widgets = relationship(
        'WidgetsModel', back_populates='widget_images', uselist=False)

    image = Column(String)

    go_to = Column(String)

    go_to_id = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
