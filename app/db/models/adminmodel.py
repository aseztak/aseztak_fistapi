from sqlalchemy import Column, Integer, String, ForeignKey, TIMESTAMP, DateTime


from app.db.config import Base


class AdminModel(Base):

    # __tablename__ = 'admins'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(191), nullable=True)
    level = Column(Integer, nullable=True)
    email = Column(String(191), index=True, nullable=False)
    email_verified_at = Column(TIMESTAMP)
    password = Column(String(191), nullable=False)
    remember_token = Column(String(100))
    last_login_at = Column(DateTime)
    last_login_ip = Column(String(20))
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
