
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class SellerHelperModel(Base):
    __tablename__ = 'seller_helper'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    title = Column(String)

    description = Column(String)

    video = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
