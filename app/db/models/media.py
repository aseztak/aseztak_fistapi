
from sqlalchemy.orm import relationship
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum, BLOB


class ProductMediaModel(Base):
    __tablename__ = 'media'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    model_id = Column(Integer, ForeignKey('products.id'), index=True)

    product = relationship(
        'ProductModel', back_populates='images', uselist=False)

    collection_name = Column(String)

    name = Column(String)

    file_name = Column(String)

    file_path = Column(String)

    default_img = Column(Integer, default=0)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)

    deleted_at = Column(TIMESTAMP)
