from sqlalchemy.sql.elements import Null
from sqlalchemy.sql.sqltypes import TIMESTAMP, Date
from app.db.config import Base
import enum
from sqlalchemy import Column, Integer, String, ForeignKey, Enum


class FaqsValue(enum.Enum):
    Seller = 'Seller'
    Buyer = 'Buyer'


class FaqsModel(Base):

    __tablename__ = 'faqs'

    id = Column(Integer, index=True, primary_key=True)

    flag = Column(Enum(FaqsValue), default='Seller')

    question = Column(String, default=Null)

    answer = Column(String)

    video = Column(String)

    status = Column(String, default=Null)

    created_at = Column(TIMESTAMP, nullable=True)

    updated_at = Column(TIMESTAMP, nullable=True)

    deleted_at = Column(TIMESTAMP, nullable=True)
