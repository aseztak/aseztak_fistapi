
from app.db.config import Base
from sqlalchemy import Column, Integer, String, ForeignKey
from app.db.config import Base


class TestModel(Base):

    __tablename__ = 'test'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    mobile = Column(String)
    status = Column(Integer)
