

import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class OrderType(enum.Enum):
    RETURN = 'RETURN'
    PACKED = 'PACKED'


class OrderReturnProofModel(Base):
    __tablename__ = 'order_proof'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)

    image = Column(String)

    order_type = Column(Enum(OrderType), default='RETURN')

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
