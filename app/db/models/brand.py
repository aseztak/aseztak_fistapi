
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class BrandsModel(Base):
    __tablename__ = 'brands'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    user_id = Column(Integer, ForeignKey('users.id'), index=True)

    name = Column(String)

    slug = Column(String)

    logo = Column(String)

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
