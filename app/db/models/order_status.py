
from sqlalchemy.orm import relationship
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class OrderStatusModel(Base):
    __tablename__ = 'order_status'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)

    order = relationship(
        'OrdersModel', back_populates='order_status', uselist=False)

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
