
from sqlalchemy.orm import relationship


from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class AttributeModel(Base):
    __tablename__ = 'attributes'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    code = Column(String)
    name = Column(String)
    frontend_type = Column(String)
    is_required = Column(Integer)
    is_price_variable = Column(Integer)
    is_filterable = Column(Integer)

    # Relation with AttributeFamily
    attributefamily = relationship(
        'AttriFamilyValuesModel',  back_populates='attribute', uselist=False)

    # Relation with attributevalues
    product_attributes = relationship(
        'AttributeValueModel',  back_populates='attributes', uselist=True)
