
from sqlalchemy.orm import relationship


from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP

from app.db.models.attribute_family import AttriFamilyModel
from app.db.models.attributes import AttributeModel


class AttriFamilyValuesModel(Base):
    __tablename__ = 'attribute_family_values'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    family_id = Column(Integer, ForeignKey(
        AttriFamilyModel.id), index=True)

    attribute_id = Column(Integer, ForeignKey(AttributeModel.id), index=True)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)

    attribute = relationship(
        'AttributeModel',  back_populates='attributefamily', uselist=True)
