
from sqlalchemy.orm import relationship
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class OrderShippingModel(Base):
    __tablename__ = 'shipping'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)

    order = relationship(
        'OrdersModel', back_populates='order_shipping', uselist=False)

    wbns = Column(String)

    courier_partner = Column(String)

    slip = Column(String)

    invoice = Column(String)

    status = Column(Integer)

    remark = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
