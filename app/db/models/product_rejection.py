import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class TypeValue(enum.Enum):
    Title = 'Title'
    Image = 'Image'
    Pricing = 'Pricing'
    Other = 'Other'


class ProductRejectionModel(Base):
    __tablename__ = 'product_rejection'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    user_id = Column(Integer, ForeignKey('admins.id'), index=True)

    product_id = Column(Integer, ForeignKey('products.id'), index=True)

    type = Column(Enum(TypeValue), default='Title')

    message = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
