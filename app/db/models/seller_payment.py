
from pymysql import NULL
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class SellerPaymentModel(Base):
    __tablename__ = 'seller_payment'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    seller_id = Column(Integer, ForeignKey('users.id'), index=True)

    reff_no = Column(String)

    commission_date = Column(TIMESTAMP)

    payment_date = Column(TIMESTAMP)

    total_amount = Column(Float, default=0)

    commission_amount = Column(Float, default=0)

    commission_tax_amount = Column(Float, default=0)

    payment_csv = Column(String, default=NULL)

    buyer_payment_csv = Column(String, default=NULL)

    tcs_amount = Column(Float, default=0)

    tds_amount = Column(Float, default=0)

    total_orders = Column(Integer)

    txn_id = Column(String)

    commission_generated = Column(String)

    commission_invoice = Column(String)

    invoice_date = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
