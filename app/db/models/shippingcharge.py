


import enum

from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class EnumValue(enum.Enum):
    DELHIVERY = 'DELHIVERY'


class PaymentType(enum.Enum):
    ONLINE = 'ONLINE'
    COD = 'COD'
    BOTH = 'BOTH'


class ShippingChargeModel(Base):
    __tablename__ = 'shipping_charge'
    id = Column(Integer, primary_key=True, index=True)
    career = Column(Enum(EnumValue))
    app_version = Column(String)
    payment_mode = Column(Enum(PaymentType))
    rate = Column(Float)
    order_limit = Column(Integer)
    start_date = Column(String)
    shipping_text = Column(String)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
