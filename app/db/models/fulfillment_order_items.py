
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class FulFillmentOrdersItemsModel(Base):
    __tablename__ = 'fulfillment_order_items'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('fulfillment_orders.id'), index=True)

    price = Column(Float)

    quantity = Column(Integer)

    item_id = Column(Integer)
