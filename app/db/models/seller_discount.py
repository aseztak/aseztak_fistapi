
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class SellerDiscountModel(Base):
    __tablename__ = 'seller_discount'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    seller_id = Column(Integer, ForeignKey('users.id'), index=True)

    rate = Column(Float)
    limit_amount = Column(Float)

    valid_upto = Column(TIMESTAMP)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
