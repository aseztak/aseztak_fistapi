
from app.db.config import Base
from sqlalchemy import Boolean, Column,  Integer, Float



class WalletUsesSlabModel(Base):
    __tablename__ = 'wallet_uses_slab'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    from_amount = Column(Integer)

    to_amount = Column(Integer)

    percentage = Column(Float, default=0)
