
import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class bannerType(enum.Enum):
    INNER = 'INNER'
    FLASH = 'FLASH'
    GARMENTS = 'GARMENTS'
    FOOTWEAR = 'FOOTWEAR'


class BannersModel(Base):
    __tablename__ = 'banners'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    banner = Column(String)

    type = Column(Enum(bannerType), default='INNER')
    link = Column(String)

    link_id = Column(Integer)

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
