
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class BillingAddressModel(Base):
    __tablename__ = 'user_billing_address'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    user_id = Column(Integer, ForeignKey('users.id'), index=True)

    name = Column(String)

    address = Column(String)

    city = Column(String)

    state = Column(String)

    pincode = Column(String)
