

import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer,  ForeignKey, TIMESTAMP, Enum


class paymentTypedValue(enum.Enum):
    ONLINE = 'ONLINE'
    PARTIAL = 'PARTIAL'
    COD = 'COD'


class OrderFailsModel(Base):
    __tablename__ = 'order_fails'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)

    # order = relationship(
    #     'OrdersModel',  back_populates='order_failed', uselist=False)

    # payment_failed = relationship(
    #     'PaymentFailedModel',  back_populates='orderfail', lazy="dynamic")

    payment_type = Column(Enum(paymentTypedValue), default='ONLINE')

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
