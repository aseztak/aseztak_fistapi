
from sqlalchemy.sql.sqltypes import TIMESTAMP
# from app.db.config import Base
from sqlalchemy import Column, Integer, String, ForeignKey
from app.db.config import Base


class ProductTagModel(Base):

    __tablename__ = 'product_tags'

    id = Column(Integer, primary_key=True, index=True)
    product_id = Column(Integer, ForeignKey('products.id'), index=True)
    tag_id = Column(Integer, ForeignKey('tags.id'), index=True)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
