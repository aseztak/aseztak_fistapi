

from sqlalchemy.orm import relationship


from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP
from app.db.models.carts import CartModel


class ProductModel(Base):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    userid = Column(Integer, ForeignKey('users.id'), index=True)
    brand_id = Column(Integer, default=None)
    title = Column(String)
    slug = Column(String, unique=True)
    category = Column(Integer)
    short_description = Column(String)
    status = Column(Integer, default=1)
    featured = Column(Integer, default=0)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)

    product_discount = relationship(
        'ProductDiscountModel',  back_populates='products', lazy="dynamic")

    product_pricing = relationship("ProductPricingModel",
                                   primaryjoin="and_(ProductModel.id==ProductPricingModel.product_id, "
                                   "ProductPricingModel.deleted_at.is_(None))", order_by="asc(ProductPricingModel.price)", uselist=True)

    wishlist = relationship(
        'FavouriteModel', back_populates='product', lazy="dynamic")

    images = relationship('ProductMediaModel',
                          primaryjoin="and_(ProductModel.id==ProductMediaModel.model_id, "
                          "ProductMediaModel.deleted_at.is_(None))", order_by="desc(ProductMediaModel.default_img)", uselist=True)

    cart = relationship(
        CartModel, back_populates='product')

    seller = relationship(
        'UserModel', back_populates='product', uselist=False)

    order_items = relationship(
        'OrderItemsModel', back_populates='product', uselist=True)


class ProductPricingAttributeModel(Base):
    __tablename__ = 'product_pricing_attribute'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    product_pricing_id = Column(
        Integer, ForeignKey('product_pricing.id'), index=True)

    attribute_id = Column(Integer, ForeignKey('attributes.id'), index=True)


class ProductPricingModel(Base):
    __tablename__ = 'product_pricing'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    product_id = Column(Integer, ForeignKey('products.id'), index=True)
    price = Column(Float)
    mrp = Column(Float)
    tax = Column(Float)
    moq = Column(Integer)
    items = Column(Integer)
    unit = Column(String)
    hsn = Column(String)
    products = relationship('ProductModel',  back_populates='product_pricing')
    description = Column(String)
    seller_amount = Column(Float)
    created_at = Column(Date)
    default_image = Column(Integer, default=0)
    updated_at = Column(Date)
    deleted_at = Column(TIMESTAMP, default=None)
    inventory = relationship(
        'InventoryModel', back_populates='price', uselist=False)


class InventoryModel(Base):
    __tablename__ = 'inventories'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    pricing_id = Column(Integer, ForeignKey('product_pricing.id'), index=True)
    stock = Column(Integer)
    unlimited = Column(Integer)
    out_of_stock = Column(Integer, default=0)

    price = relationship('ProductPricingModel', back_populates='inventory')


class FavouriteModel(Base):
    __tablename__ = 'favourite'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('users.id'), index=True)
    product_id = Column(Integer, ForeignKey('products.id'), index=True)

    product = relationship('ProductModel', back_populates='wishlist')
