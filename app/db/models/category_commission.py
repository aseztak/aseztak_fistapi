
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, TIMESTAMP, DATE


class CategoryCommissionModel(Base):
    __tablename__ = "category_commissions"

    id = Column(Integer, index=True, primary_key=True)

    category_id = Column(Integer, ForeignKey('categories.id'), index=True)

    start_date = Column(DATE)

    commission = Column(Float)

    commission_tax = Column(Float)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
