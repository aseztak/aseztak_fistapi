
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class FulFillmentItemsModel(Base):
    __tablename__ = 'fulfillment_items'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    title = Column(String)

    description = Column(String)

    price = Column(Float, default=0)

    delivery = Column(Float, default=0)

    image = Column(String)

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
