
from sqlalchemy.orm import relationship


from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP

from app.db.models.attributes import AttributeModel


class AttributeValueModel(Base):
    __tablename__ = 'attribute_values'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    attribute_id = Column(Integer, ForeignKey(AttributeModel.id), index=True)

    value = Column(String)

    created_at = Column(String)

    updated_at = Column(String)

    attributes = relationship(
        'AttributeModel',  back_populates='product_attributes')
