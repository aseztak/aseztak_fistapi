


from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class CategoryReturnDaysModel(Base):
    __tablename__ = 'order_return_days'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    category_id = Column(Integer, ForeignKey('categories.id'), index=True)

    return_days = Column(String)

    start_date = Column(Date)

    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
