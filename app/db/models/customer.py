from sqlalchemy import Column, Integer, String, ForeignKey

from app.db.config import Base



class Customer(Base):

    __tablename__ = 'customers'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    email = Column(String)
    address = Column(String)
    city = Column(String)
    pincode = Column(String)
    mobile = Column(String, unique=True, index=True)