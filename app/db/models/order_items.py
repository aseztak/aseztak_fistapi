
from sqlalchemy.orm import relationship
from sqlalchemy.sql.elements import Null

import enum

from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class cancelledValue(enum.Enum):
    Yes = 'Yes'
    No = 'No'


class OrderItemsModel(Base):
    __tablename__ = 'order_items'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)

    product_id = Column(Integer, ForeignKey('products.id'), index=True)

    orders = relationship(
        'OrdersModel',  back_populates='order_items', uselist=False)

    product = relationship(
        'ProductModel',  back_populates='order_items', uselist=False)

    quantity = Column(Integer)

    price = Column(Float)

    product_discount = Column(Float)

    discount_amount = Column(Float)

    discount_rate = Column(Float)

    tax = Column(Float)

    cgst_on_tax = Column(Float)

    sgst_on_tax = Column(Float)

    igst_on_tax = Column(Float)

    commission_buyer = Column(Float)

    commission_buyer_tax = Column(Float)

    asez_service_amount = Column(Float)

    asez_service_on_cgst = Column(Float)

    asez_service_on_sgst = Column(Float)

    asez_service_on_igst = Column(Float)

    wallet_amount = Column(Float)

    taxable_total_amount = Column(Float)

    commission_seller = Column(Float)

    commission_seller_tax = Column(Float)

    tcs_rate = Column(Float)

    tds_rate = Column(Float)

    item_total_amount = Column(Float)

    attributes = Column(String)

    images = Column(String, default=Null)

    uuid = Column(String)

    hsn_code = Column(String)

    status = Column(Integer)

    cancelled_by_seller = Column(Enum(cancelledValue), default='No')

    return_declined_by = Column(Integer)

    return_declined_date = Column(TIMESTAMP)

    message = Column(String)

    weight = Column(Float, default=0)

    our_weight = Column(Float, default=0)

    refund = Column(Integer, default=0)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)

    return_expiry = Column(Integer)
