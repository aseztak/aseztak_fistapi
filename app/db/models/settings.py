from app.db.config import Base
from sqlalchemy import Column, Integer, String, ForeignKey
class SettingsModel(Base):
    
    __tablename__ = 'settings'

    id = Column(Integer, primary_key=True, index=True)
    key = Column(String,index=True)
    value = Column(String)
