from app.db.config import Base
from sqlalchemy import TIMESTAMP, Column,  Integer, String


class PromotionModel(Base):
    __tablename__ = 'promotion'
    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    link = Column(String)
    valid_upto = Column(TIMESTAMP)
