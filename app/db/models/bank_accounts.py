
from sqlalchemy.orm import relationship
import enum

from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum




class BankType(enum.Enum):
    savings = 'savings'
    current = 'current'



class BankAccountsModel(Base):
    __tablename__ = 'user_bank_details'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    user_id = Column(Integer, ForeignKey('users.id'), index=True)

    user = relationship(
        'UserModel', back_populates="bank_account", uselist=False)
    account_holder_name = Column(String)

    account_type = Column(Enum(BankType), default='savings')

    bank = Column(String)

    branch = Column(String)

    acc_no = Column(String)

    ifsc_code = Column(String)
