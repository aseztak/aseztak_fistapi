
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class TransactionsModel(Base):
    __tablename__ = 'transactions'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    txn_payment_id = Column(String, default=None)

    txn_order_id = Column(String, default=None)

    refund_txn_id = Column(String, default=None)

    order_ref_id = Column(Integer, ForeignKey('orders.reff'), index=True)

    payment_method = Column(String, default=None)

    amount = Column(Float, default=0)

    email = Column(String, default=None)

    contact = Column(String, default=None)

    status = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
