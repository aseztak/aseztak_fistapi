
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class InvoiceItemsModel(Base):
    __tablename__ = 'invoice_items'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey('orders.id'), index=True)

    product_id = Column(Integer, ForeignKey('products.id'), index=True)

    item_id = Column(Integer, ForeignKey('order_items.id'), index=True)

    quantity = Column(Integer)

    price = Column(Float)

    discount_amount = Column(Float)

    discount_rate = Column(Float)

    tax = Column(Float)

    cgst_on_tax = Column(Float)

    sgst_on_tax = Column(Float)

    igst_on_tax = Column(Float)

    asez_service = Column(Float)

    asez_gst_on_service = Column(Float)

    asez_service_amount = Column(Float)

    asez_service_on_cgst = Column(Float)

    asez_service_on_sgst = Column(Float)

    asez_service_on_igst = Column(Float)

    total_amount = Column(Float)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
