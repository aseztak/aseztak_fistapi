from app.db.config import Base
from sqlalchemy.ext.declarative import as_declarative, declared_attr
from app.db.config import Base


@as_declarative()
class BaseModel(Base):

    __name__: str

    # Generate __tablename__ automatically
    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()
