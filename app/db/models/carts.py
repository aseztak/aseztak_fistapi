

from sqlalchemy.orm import relationship


from app.db.config import Base
from sqlalchemy import Column,  Integer, VARCHAR, Float, String, ForeignKey, Date, TIMESTAMP, Text


class CartModel(Base):
    __tablename__ = 'cart_items'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('users.id'),  index=True)
    seller_id = Column(Integer, index=True)
    product_id = Column(Integer, ForeignKey('products.id'), index=True)
    product = relationship('ProductModel',
                           back_populates='cart', uselist=False)

    quantity = Column(Integer)
    price = Column(Float)
    tax = Column(Float)
    attributes = Column(Text)
    uuid = Column(Text)
    hsn_code = Column(VARCHAR)
    status = Column(Integer, default=1)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
