

from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class NotificationImageModel(Base):
    __tablename__ = 'notification_image'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    image = Column(String)

    file_path = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
