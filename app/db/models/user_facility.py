
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class UserFacilityModel(Base):
    __tablename__ = 'user_facilities'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    user_id = Column(Integer, ForeignKey('users.id'), index=True)

    key_name = Column(String)

    key_value = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
