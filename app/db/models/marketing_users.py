

from app.db.config import Base
from sqlalchemy import Column,  Integer, String, ForeignKey, TIMESTAMP


class MarketerUserModel(Base):
    __tablename__ = 'marketing_users'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    name = Column(String)
    email = Column(String)
    mobile = Column(String)
    otp = Column(String)
    address = Column(String)
    city = Column(String)
    state = Column(String)
    pincode = Column(String)
    status = Column(Integer)
    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
