
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String


class PincodeModel(Base):
    __tablename__ = 'pincodes'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)
    officename = Column(String)
    pincode = Column(String)

    taluk = Column(String)
    districtname = Column(String)
    statename = Column(String)
