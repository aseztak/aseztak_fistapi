

from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class ShippingAddressModel(Base):
    __tablename__ = 'buyer_address'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    user_id = Column(Integer, ForeignKey('users.id'), index=True)

    ship_to = Column(String)

    phone = Column(String)

    alt_phone = Column(String)

    pincode = Column(String)

    address = Column(String)

    locality = Column(String)

    city = Column(String)

    default_address = Column(Integer)

    state = Column(String)

    country = Column(String)

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
