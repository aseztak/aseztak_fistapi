
from sqlalchemy.orm import relationship
import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class TypeValue(enum.Enum):
    ALL = 'ALL'
    INDIVIDUAL = 'INDIVIDUAL'
    CATEGORY = 'CATEGORY'


class ProductDiscountModel(Base):
    __tablename__ = 'product_discount'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    seller_id = Column(Integer, ForeignKey('users.id'), index=True)

    product_id = Column(Integer, ForeignKey('products.id'), index=True)

    products = relationship(
        'ProductModel',  back_populates='product_discount', uselist=False)

    type = Column(Enum(TypeValue), default='ALL')

    category_id = Column(Integer)

    discount = Column(Float)
    valid_upto = Column(TIMESTAMP)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
