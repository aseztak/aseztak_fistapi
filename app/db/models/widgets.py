
from sqlalchemy.orm import relationship

import enum
from app.db.config import Base
from sqlalchemy import Boolean, Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class widgetType(enum.Enum):
    INNER = 'INNER'
    HOME = 'HOME'
    GARMENTS = 'GARMENTS'
    FOOTWEAR = 'FOOTWEAR'


class WidgetsModel(Base):
    __tablename__ = 'widgets'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    title = Column(String)

    description = Column(String)

    no_of_images = Column(Integer)

    widget_name = Column(String)
    slno = Column(Integer)
    widget_type = Column(Enum(widgetType), default='HOME')

    widget_images = relationship(
        'WidgetImagesModel', back_populates='widgets', uselist=True)

    enabled = Column(Integer)

    start_date = Column(TIMESTAMP)

    end_date = Column(TIMESTAMP)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
