from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class LoginModel(Base):
    __tablename__ = 'login'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    username = Column(String)

    full_name = Column(String)

    email = Column(String)

    hashed_password = Column(String)
