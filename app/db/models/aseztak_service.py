from app.db.config import Base
from sqlalchemy import DATETIME, Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class AseztakServiceModel(Base):
    __tablename__ = 'aseztak_service'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    commission_date = Column(DATETIME)

    rate = Column(Float)

    gst_on_rate = Column(Float)

    tds_rate = Column(Float)

    tcs_rate = Column(Float)

    round_off = Column(Float)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
