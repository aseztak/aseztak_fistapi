


from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class PickupLocationModel(Base):
    __tablename__ = 'seller_pickup_locations'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    seller_id = Column(Integer, ForeignKey('users.id'), index=True)

    name = Column(String)

    email = Column(String)

    phone = Column(String)

    pin = Column(String)

    city = Column(String)

    address = Column(String)

    country = Column(String)

    registered_name = Column(String)

    return_address = Column(String)

    return_pin = Column(String)

    return_city = Column(String)

    return_state = Column(String)

    return_country = Column(String)

    status = Column(Integer)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
