
import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class EnumValue(enum.Enum):
    ONLINE = 'ONLINE'
    COD = 'COD'
    BOTH = 'BOTH'


class OrderDiscountModel(Base):
    __tablename__ = 'order_discount'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    payment_method = Column(Enum(EnumValue))

    discount = Column(Float)

    start_date = Column(Date)

    discount_text = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
