

from sqlalchemy.orm import relationship
from app.db.config import Base
from sqlalchemy import TIMESTAMP, Column,  Integer, Float, String, ForeignKey


class CategoriesModel(Base):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    slug = Column(String)
    description = Column(String)
    image = Column(String)
    image_path = Column(String)
    parent_id = Column(Integer)
    family_id = Column(Integer)

    gst = Column(Float)
    discount = Column(Float)
    featured = Column(Integer)
    status = Column(Integer)
    menu = Column(Integer)
    selectable = Column(Integer)

    products = relationship(
        'ProductCategories',  back_populates='categories')


class ProductCategories(Base):
    __tablename__ = 'product_categories'

    id = Column(Integer, primary_key=True, index=True)
    category_id = Column(Integer, ForeignKey('categories.id'), index=True)
    product_id = Column(Integer, ForeignKey('products.id'), index=True)
    categories = relationship('CategoriesModel',  back_populates='products',)
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
