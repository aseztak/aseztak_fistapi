


from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class ProductAttributeModel(Base):
    __tablename__ = 'product_attributes'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    product_id = Column(Integer, ForeignKey('products.id'), index=True)

    attribute_id = Column(Integer, ForeignKey('attributes.id'), index=True)

    attribute_value_id = Column(Integer)

    attribute_value = Column(String)

    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
