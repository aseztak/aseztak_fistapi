
import enum
from app.db.config import Base
from sqlalchemy import Boolean, Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class txnType(enum.Enum):
    DR = 'DR'
    CR = 'CR'


class WalletModel(Base):
    __tablename__ = 'wallet'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    user_id = Column(Integer)
    reff_id = Column(Integer)

    reff_type = Column(String)

    description = Column(String)

    type = Column(Enum(txnType), default='CR')

    amount = Column(Float)

    status = Column(Integer)

    valid_upto = Column(Date)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
