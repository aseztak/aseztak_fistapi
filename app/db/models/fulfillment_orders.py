
from sqlalchemy.sql.elements import Null


from app.db.config import Base
from sqlalchemy import DATETIME, Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum
import enum


class paymentStatus(enum.Enum):
    SUCCESS = 'SUCCESS'
    PENDING = 'PENDING'
    FAILED = 'FAILED'


class FulFillmentOrdersModel(Base):
    __tablename__ = 'fulfillment_orders'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    seller_id = Column(Integer, ForeignKey('users.id'), index=True)

    order_number = Column(String)

    total_amount = Column(Float)

    delivery = Column(Float, default=0)

    payment_status = Column(Enum(paymentStatus), default='PENDING')

    payment_id = Column(String, default=Null)

    bank_txn_id = Column(String, default=Null)

    payment_order_id = Column(String, default=Null)

    payment_mode = Column(String, default=Null)

    invoice = Column(Integer, default=0)

    invoice_date = Column(DATETIME, default=None)

    invoice_pdf = Column(String, default=Null)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
