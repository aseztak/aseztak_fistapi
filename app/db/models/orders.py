
from pymysql import NULL
from sqlalchemy.orm import relationship

import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum


class freeDeliveryValue(enum.Enum):
    Yes = 'Yes'
    No = 'No'


class useWallet(enum.Enum):
    Yes = 'Yes'
    No = 'No'


class OrdersModel(Base):
    __tablename__ = 'orders'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_items = relationship(
        'OrderItemsModel',  back_populates='orders', lazy="dynamic")

    order_status = relationship(
        'OrderStatusModel',  back_populates='order', lazy="dynamic")

    order_shipping = relationship(
        'OrderShippingModel',  back_populates='order', lazy="dynamic")

    order_reverse = relationship(
        'OrderRevreseModel',  back_populates='order', lazy="dynamic")

    # order_failed = relationship(
    #     'OrderFailsModel',  back_populates='order', lazy="dynamic")
    # active_items = relationship("OrderItemsModel",
    #                             primaryjoin="and_(OrdersModel.id==OrderItemsModel.order_id, "
    #                             "OrderItemsModel.status.between(0, 81), OrderItemsModel.status != 61, OrderItemsModel.status != 80)", uselist=True)

    order_number = Column(String)

    reff = Column(String)

    message = Column(String)

    commission_reff = Column(String)

    user_id = Column(Integer, ForeignKey('users.id'), index=True)

    status = Column(Integer)

    round_off = Column(Float)

    grand_total = Column(Float)

    partial_amount = Column(Float)

    cod_amount = Column(Float)

    use_wallet = Column(Enum(useWallet), default='No')

    wallet_reff_id = Column(Integer)

    total_tax = Column(Float)

    discount_rate = Column(Float)

    discount = Column(Float)

    delivery_charge = Column(Float)

    item_count = Column(Integer)

    address_id = Column(Integer, ForeignKey('buyer_address.id'))

    meta_data = Column(String)

    pickup_address_id = Column(Integer, default=0)

    payment_status = Column(Integer)

    payment_method = Column(String)

    status = Column(Integer, default=0)

    invoice = Column(String, default=NULL)

    invoice_date = Column(Date, default=None)

    igst = Column(Float, default=0)

    cgst = Column(Float, default=0)

    sgst = Column(Float, default=0)

    height = Column(Float, default=0)

    width = Column(Float, default=0)

    length = Column(Float, default=0)

    app_version = Column(String)

    free_delivery = Column(Enum(freeDeliveryValue), default='No')

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
