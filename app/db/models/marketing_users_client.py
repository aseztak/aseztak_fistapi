

from app.db.config import Base
from sqlalchemy import Column,  Integer, String, ForeignKey, TIMESTAMP
from sqlalchemy.orm import relationship


class MarketerUsersClientModel(Base):
    __tablename__ = 'marketer_users_client'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    marketer_id = Column(Integer, ForeignKey('marketing_users.id'), index=True)

    user_id = Column(Integer, ForeignKey('users.id'), index=True)

    user = relationship(
        'UserModel', back_populates='marketing_client', uselist=False)

    user_mobile = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
