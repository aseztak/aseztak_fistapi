
import enum
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP, Enum



class PaymentType(enum.Enum):
    Yes = 'Yes'
    No = 'No'

class ReconcilationModel(Base):
    __tablename__ = 'reconcilation'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    order_no = Column(String, ForeignKey('orders.order_number'), index=True)

    wbns_no = Column(String, ForeignKey('shipping.wbns'), index=True)

    charge_weight = Column(Float, default= 0.00)

    deduction_amount = Column(Float, default= 0.00)

    payment = Column(Enum(PaymentType), default='No')

    payment_date = Column(String)

    deduction_date = Column(String)

    paid_amount = Column(Float, default= 0.00)

    remarks = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)