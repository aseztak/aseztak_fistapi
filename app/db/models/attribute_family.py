
from app.db.config import Base
from sqlalchemy import Column,  Integer, Float, String, ForeignKey, Date, TIMESTAMP


class AttriFamilyModel(Base):
    __tablename__ = 'attribute_families'

    id = Column(Integer, index=True, primary_key=True,
                unique=True, autoincrement=True)

    code = Column(String)

    name = Column(String)

    created_at = Column(TIMESTAMP)

    updated_at = Column(TIMESTAMP)
