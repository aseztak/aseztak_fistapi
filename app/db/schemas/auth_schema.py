
from typing import Optional
from pydantic import BaseModel, validator


# new Login
class NewLogin(BaseModel):
    mobile: int
    signature: Optional[str] = ''


# register
class RegisterSchema(BaseModel):
    name: str
    mobile: int
    email: Optional[str] = ''
    region: Optional[str] = ''
    city: Optional[str] = ''
    country: Optional[str] = ''
    pincode: Optional[str] = ''
    signature: Optional[str] = ''
    proof: Optional[str] = ''
    proof_type: Optional[str] = ''

    @validator('mobile')
    def mobile_10digit(cls, v):
        if len(str(v)) != 10:
            raise ValueError('Mobile number must be 10 digits')
        return v


class SendOtpSchema(BaseModel):
    mobile: str = ''
    signature: str = ''

    @validator('mobile')
    def mobile_onlynumeric(cls, v):
        if len(str(v)) != 10:
            raise ValueError('Mobile number must be 10 digits')
        return v

    @validator('signature')
    def check_title_required(cls, signature: str):
        if signature is '':
            raise ValueError('Signature field is missing')
        else:
            return signature


class verifyOtp(BaseModel):
    mobile: str = ''
    otp: str = ''


class VerifyOtpSellerV4(BaseModel):
    mobile: str = ''
    otp: str = ''
    ip: str = ''
    platform: str = 'Android'
    fcm_token: str = ''


class VerifyOtpV4(BaseModel):
    mobile: str = ''
    otp: str = ''
    ip: str = ''
    platform: str = 'Android'
    fcm_token: str = ''


class SuccessSchema(BaseModel):
    success: bool


class AdminLoginSchema(BaseModel):
    username: str
    password: str


class Userfcm_tokenSchema(BaseModel):
    fcm_token: str
