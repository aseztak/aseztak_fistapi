from app.db.schemas.base import AppSchema
from typing import List, Optional



class ProductDiscountSchema(AppSchema):
    type: str


class SellerProductDiscountListSchema(AppSchema):
    id: int
    name: str
    image: Optional[str] = ''
    price: str
    moq: int
    stock: Optional[bool] = False
    status: str
    created_at: str
    running_discount: Optional[str] = ''
    valid_upto: Optional[str] = ''


class SellerCategoryDiscountListSchema(AppSchema):
    id: int
    name: str
    description: str
    image: str
    total_items: int
    running_discount: Optional[str] = ''
    valid_upto: Optional[str] = ''


class SellerProductDiscountList(AppSchema):
    status_code: int
    total_products: int
    products: List[SellerProductDiscountListSchema] = []
    current_page: int
    total_pages: Optional[int] = 0


class AddProductDiscountSchema(AppSchema):
    product_id: Optional[List] = []
    category_id: Optional[List] = []
    type: str
    discount: Optional[float] = 0.00
    valid_upto: str


class AddNewDiscount(AppSchema):
    discount: Optional[float] = 0.00
    order_limit: Optional[float] = 0.00
    valid_upto: Optional[str] = ''


class RemoveProductDiscountSchema(AppSchema):
    product_id: Optional[List] = []
    category_id: Optional[List] = []
    type: str


class SellerCategoryDiscountList(AppSchema):
    status_code: int
    total_products: int
    categories: List[SellerCategoryDiscountListSchema] = []
    current_page: int
    total_pages: Optional[int] = 0


class SellerProductDiscountList(AppSchema):
    status_code: int
    total_products: int
    products: List[SellerProductDiscountListSchema] = []
    current_page: int
    total_pages: Optional[int] = 0


class ProductDiscountSearchSchema(AppSchema):
    search: str = ''
    page: int = 1
    limit: int = 10


class SellerAllDiscountListSchema(AppSchema):
    name: str
    description: str
    running_discount: Optional[str] = ''
    valid_upto: Optional[str] = ''


class SellerAllDiscountList(AppSchema):
    status_code: int
    total_products: int
    all: List[SellerAllDiscountListSchema] = []
