from typing import List
from app.db.schemas.base import AppSchema


class TagsSchema(AppSchema):
    id: int
    name: str
    slug: str
    status: int


class TagsDataSchema(AppSchema):
    tags: List[TagsSchema] = []
