
from enum import Enum
from typing import List
from app.db.schemas.base import AppSchema


class Transaction(AppSchema):
    date: str
    head: Enum
    description: str
    debit: str
    credit: str
    balance: str
    txn_type: Enum


class OpeningBalance(AppSchema):
    balance: str
    txn_type: str


class TransactionSchema(AppSchema):
    status_code: int
    opening_balance: OpeningBalance
    transactions: List[Transaction] = []
    current_page: int
    total_pages: int = 0
    message: str

# Seller Account Transaction


class SearchTransaction(AppSchema):
    from_date: str = ''
    to_date: str = ''
    page: int = 1
    limit: int = 10

# Export Account Transaction


class ExportAccountTransaction(AppSchema):
    from_date: str = ''
    to_date: str = ''
