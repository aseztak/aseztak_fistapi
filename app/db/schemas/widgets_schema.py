

from typing import List

from app.db.schemas.base import AppSchema


class WidgetsImageSchema(AppSchema):
    image: str


class WidgetSchema(AppSchema):
    title: str
    description: str
    no_of_images: int
    link: str
    images: List[WidgetsImageSchema] = []


class WidgetDataSchema(AppSchema):
    status_code: int
    widgets: List[WidgetSchema] = []


class AdminWidgetSchema(AppSchema):
    title: str
    description: str = ''
    widget_type: str
    slno: int
    enabled: bool
    no_of_images: int


class AdminWidgetsImageSchema(AppSchema):
    image: str


class AdminWidgetListSchema(AppSchema):
    id: int
    slno: int
    widget_name: str
    title: str
    description: str
    enabled: int
    no_of_images: int
    images: List[WidgetsImageSchema] = []


class AdminWidgetDataSchema(AppSchema):
    status_code: int
    widgets: List[AdminWidgetListSchema] = []
    total_records: int
    current_page: int
    total_pages: int


class AdminUpdateWidgetSchema(AppSchema):
    id: int
    title: str
    description: str = ''
    widget_type: str
    slno: int
    enabled: bool
    no_of_images: int


class AdminWidgetDeleteSchema(AppSchema):
    id: int
