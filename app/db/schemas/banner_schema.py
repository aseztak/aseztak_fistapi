
from typing import List
from app.db.schemas.base import AppSchema


class BannerList(AppSchema):
    id: int
    banner: str


class BannerListSchema(AppSchema):
    banners: List[BannerList]
