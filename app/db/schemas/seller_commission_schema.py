from datetime import date

from typing import List, Dict, Optional

from app.db.schemas.base import AppSchema


# Seller List
class SellerListSchema(AppSchema):
    id: int
    name: str
    address: str
    mobile: str
    total_count: int


class SellerListSchemaList(AppSchema):
    status_code: int
    seller_data: List[SellerListSchema] = []
    total_items: int
    current_page: int
    total_page: Optional[int] = 0


# Generated Invoice List
class commissionInvoiceList(AppSchema):
    created_at: str
    reff_no: str
    seller: str
    mobile: str
    seller_address: str
    total_orders: int
    total_amounts: str
    commission_amount: str
    seller_amount: str
    tcs_rate: int
    tcs_amount: str
    tds_rate: int
    tds_amount: str
    seller_will_get: str
    payment_date: date
    status: str
    payment_csv: str
    buyer_payment_csv: str
    commission_generated: str


class ListcommissionInvoiceList(AppSchema):
    status_code: int
    payment_list: List[commissionInvoiceList] = []
    total_items: int
    current_page: int
    total_page: Optional[int] = 0


# Commission Generated List
class commissionGeneratedPaymentList(AppSchema):
    created_at: str
    reff_no: str
    seller: str
    mobile: str
    seller_address: str
    total_orders: int
    total_amounts: str
    commission_amount: str
    invoice_no: str
    seller_amount: str
    tcs_rate: int
    tcs_amount: str
    tds_rate: int
    tds_amount: str
    seller_will_get: str
    payment_date: str = ''
    status: str
    payment_csv: str
    commission_generated: str


class ListcommissionGeneratedList(AppSchema):
    status_code: int
    seller_id: str = ''
    maxref: int
    payment_list: List[commissionGeneratedPaymentList] = []
    total_items: int
    current_page: int
    total_page: Optional[int] = 0


# ItemList
class ItemListSchema(AppSchema):
    order_id: int
    created_at: str
    reff_no: str = ''
    order_number: str
    buyer: str
    address: str
    state: str
    product: str
    buyer_GST: str
    taxable_amount: str
    gst_rate: str
    gst_amount: str
    amount_after_tax: str
    service_charge: str
    service_charge_text: str
    tcs_rate: int
    tcs_amount: str
    tds_rate: int
    tds_amount: str
    status: str
    you_will_get: str
    paid_amount: Optional[str] = 0.00


class SellerBankAccountSchema(AppSchema):
    acc_no: str
    account_holder_name: str
    bank: str
    branch: str
    ifsc_code: str
    account_type: str


class ListItemListSchema(AppSchema):
    status_code: int
    reff_no: str = ''
    txn_id: str = ''
    item_data: List[ItemListSchema] = []
    seller_bank_account: SellerBankAccountSchema
    total_items: int
    current_page: int
    total_page: Optional[int] = 0

# Generated COmmission Item List


class generatedCommissionItemListSchema(AppSchema):
    status_code: int
    reff_no: str
    txn_id: str
    txn_date: str = ''
    invoice_id: int
    item_data: List[ItemListSchema] = []
    item_total_amount: float
    seller_bank_account: SellerBankAccountSchema
    total_items: int
    current_page: int
    total_page: Optional[int] = 0


class commissionGeneratedListSchema(AppSchema):
    status_code: int
    payment_list: List[commissionInvoiceList] = []
    total_items: int
    current_page: int
    total_page: Optional[int] = 0


# Commission Invoice List Seller Wise
class ListCommissionGeneratedSellerWiseList(AppSchema):
    status_code: int
    seller_id: int
    maxref: int
    payment_list: List[commissionGeneratedPaymentList] = []
    total_items: int
    current_page: int
    total_page: Optional[int] = 0

# sellerCommission


class SellerSchema(AppSchema):
    id: int = 0
    name: str = ''


class SellerCommission(AppSchema):
    seller: Optional[SellerSchema]
    address: str
    mobile: str
    total_orders: int
    total_amount: str
    amount_will_get: str


class SellerCommissionSchema(AppSchema):
    status_code: int
    total_amount: str
    sellers: List[SellerCommission] = []
    total_items: int
    current_page: int
    total_page: Optional[int] = 0
