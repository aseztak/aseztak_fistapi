from typing import List, Dict, Optional

from app.db.schemas.base import AppSchema
from pydantic import validator


class ShippingAddressSchema(AppSchema):
    id: int
    ship_to: str
    locality: str
    address: str
    city: str
    state: str
    pincode: str
    phone: str
    alt_phone: Optional[str] = ''
    default_address: Optional[bool] = False


class AddressListSchema(AppSchema):
    status_code: int
    addresses: List[ShippingAddressSchema] = []
    total_address: Optional[int] = 0
    current_page: Optional[int] = 0
    total_pages: Optional[int] = 0


class EditShippingAddressSchema(AppSchema):
    status_code: int
    address: ShippingAddressSchema
    message: str


class AddAddressSchema(AppSchema):
    ship_to: str = ''
    locality: str = ''
    address: str = ''
    city: str = ''
    state: str = ''
    pincode: str = ''
    phone: str = ''
    alt_phone: Optional[str] = ''

    @validator('ship_to')
    def check_ship_to_required(cls, ship_to: str):
        if ship_to == '':
            raise ValueError('Ship to field is required')
        else:
            return ship_to

    @validator('locality', allow_reuse=True)
    def check_locality_required(cls, locality: str):
        if locality == '':
            raise ValueError('Locality field is required')
        else:
            return locality

    @validator('address', allow_reuse=True)
    def check_address_required(cls, address: str):
        if address == '':
            raise ValueError('Address field is required')
        else:
            return address

    @validator('city', allow_reuse=True)
    def check_city_required(cls, city: str):
        if city == '':
            raise ValueError('City field is required')
        else:
            return city

    @validator('state', allow_reuse=True)
    def check_state_required(cls, state: str):
        if state == '':
            raise ValueError('State field is required')
        else:
            return state

    @validator('pincode', allow_reuse=True)
    def check_pincode_required(cls, pincode: str):
        if pincode == '':
            raise ValueError('Pincode field is required')
        else:
            return pincode

    @validator('phone', allow_reuse=True)
    def check_phone_required(cls, phone: str):
        if phone == '':
            raise ValueError('Phone field is required')
        else:
            return phone


class UpdateAddressSchema(AppSchema):
    ship_to: str = ''
    locality: str = ''
    address: str = ''
    city: str = ''
    state: str = ''
    pincode: str = ''
    phone: str = ''
    alt_phone: str = ''
    default_address: int

    @validator('ship_to')
    def check_ship_to_required(cls, ship_to: str):
        if ship_to == '':
            raise ValueError('Ship to field is required')
        else:
            return ship_to

    @validator('locality', allow_reuse=True)
    def check_locality_required(cls, locality: str):
        if locality == '':
            raise ValueError('Locality field is required')
        else:
            return locality

    @validator('address', allow_reuse=True)
    def check_address_required(cls, address: str):
        if address == '':
            raise ValueError('Address field is required')
        else:
            return address

    @validator('city', allow_reuse=True)
    def check_city_required(cls, city: str):
        if city == '':
            raise ValueError('City field is required')
        else:
            return city

    @validator('state', allow_reuse=True)
    def check_state_required(cls, state: str):
        if state == '':
            raise ValueError('State field is required')
        else:
            return state

    @validator('pincode', allow_reuse=True)
    def check_pincode_required(cls, pincode: str):
        if pincode == '':
            raise ValueError('Pincode field is required')
        else:
            return pincode

    @validator('phone', allow_reuse=True)
    def check_phone_required(cls, phone: str):
        if phone == '':
            raise ValueError('Phone field is required')
        else:
            return phone


class DeleteAddressSchema(AppSchema):
    id: int


# Seller Pickup Location Static

class PickupLocationData(AppSchema):
    id: int
    name: str
    email: str
    phone: str
    city: str
    country: str
    pincode: str
    address: str
    registered_name: str
    return_address: str
    return_pin: str
    return_city: str
    return_state: str
    return_country: str
    created_at: str


class PickupLocationSchema(AppSchema):
    status_code: int
    total_address: int
    addresses: List[PickupLocationData] = []


class AddPickupLocationSchema(AppSchema):
    name: str
    email: str
    phone: str
    city: str
    country: str
    pincode: str
    address: str
    registered_name: str
    return_address: str
    return_pin: str
    return_city: str
    return_state: str
    return_country: str


class UpdatePickupLocationSchema(AppSchema):
    id: int
    name: str
    email: str
    phone: str
    city: str
    country: str
    pincode: str
    address: str
    registered_name: str
    return_address: str
    return_pin: str
    return_city: str
    return_state: str
    return_country: str


class DefaultAddressSchema(AppSchema):
    id: int
