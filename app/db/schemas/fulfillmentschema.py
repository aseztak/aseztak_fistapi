
from typing import List, Dict, Optional



from app.db.schemas.base import AppSchema


class FulfillmentItems(AppSchema):
    id: int
    title: str
    description: str
    price: float
    delivery: float
    quantity: int
    total: int
    image: str


class FulfillmentItemsSchema(AppSchema):
    status_code: int
    total_items: int
    items: List[FulfillmentItems]
    current_page: int
    total_pages: Optional[int] = 0


class SaveFulfillmentOrders(AppSchema):
    id: int
    price: float
    quantity: int


class SaveFulfillmentOrdersSchema(AppSchema):
    payment_id: str = ''
    payment_order_id: str = ''
    items: List[SaveFulfillmentOrders]


class Seller(AppSchema):
    id: int
    name: str
    mobile: str


class FulfillmentOrders(AppSchema):
    id: int

    order_number: str
    total_amount: float
    items: int
    invoice: str
    images: str
    status: str
    created_at: str


class FulfillmentOrdersSchema(AppSchema):
    status_code: int
    total_items: int
    items: List[FulfillmentOrders]
    current_page: int
    total_pages: int = 0
