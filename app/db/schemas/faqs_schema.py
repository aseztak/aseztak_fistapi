from enum import Enum
from typing import List
from typing import Optional
from app.db.schemas.base import AppSchema


class BuyerFaqsSchema(AppSchema):
    id: int
    flag: Enum
    question: str
    answer: str
    video: Optional[str] = ''


class FaqsSchemaList(AppSchema):
    status_code: int
    faqs: List[BuyerFaqsSchema] = []


class FaqsSchema(AppSchema):
    status_code: int
    faqs: BuyerFaqsSchema
