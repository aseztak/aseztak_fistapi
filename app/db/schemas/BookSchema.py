
from app.db.schemas.base import AppSchema


class BookSchema(AppSchema):
    name: str
    author: str
    release_year: int
