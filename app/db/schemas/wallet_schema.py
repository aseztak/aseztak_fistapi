
from typing import List, Optional
from app.db.schemas.base import AppSchema


class WalletSchema(AppSchema):
    id: int
    wallet_type: str
    description: Optional[str] = ''
    type: str
    amount: float
    created_at: str


class WalletListSchema(AppSchema):
    status_code: int
    wallets: List[WalletSchema] = []


class AddWallete(AppSchema):
    wallete_type: str
    description: str
    type: str
    amount: float
