
from typing import List
from app.db.schemas.base import AppSchema



class BannerImageSchema(AppSchema):
    id: int
    banner: str

class BannerListSchema(AppSchema):
    banners: List[BannerImageSchema]
