from typing import List
from app.db.schemas.base import AppSchema


class FaqsSchema(AppSchema):

    question: str = ''
    answer: str = ''
    video: str = ''


class FaqsDataSchema(AppSchema):
    status_code: int
    faqs: List[FaqsSchema] = []
    page: int
    total_records: int
    total_pages: int
