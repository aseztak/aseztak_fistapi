from datetime import date
from typing import List

from app.db.schemas.base import AppSchema


class PageSchema(AppSchema):
    id: int
    name: str
    slug: str
    content: str
    updated_at: date


class PageDataSchema(AppSchema):
    page: PageSchema


class Page(AppSchema):
    id: int
    name: str
    slug: str
    content: str


class PageData(AppSchema):
    status_code: str
    page: Page


class PagesDataSchema(AppSchema):
    status_code: int
    pages: List[Page]
