

from typing import Optional
from app.db.schemas.base import AppSchema


class RecentProductSchema(AppSchema):
    id: int
    name: str
    description: str
    price: float
    moq: int
    unit: str
    stock: Optional[bool] = False
    image: str


class RecentOrdersSchema(AppSchema):
    id: int
    order_number: str
    total_amount: float
    items: int
    created_at: str


class TodaySchema(AppSchema):
    orders: int
    cancelled_orders: int
    return_orders: int
    sales: float
