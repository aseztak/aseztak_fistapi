

from app.db.schemas.base import AppSchema



class AdminOrderSearchSchema(AppSchema):
    order_number: str = ''
    from_date: str = ''
    to_date: str = ''
