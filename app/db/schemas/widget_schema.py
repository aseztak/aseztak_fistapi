
from typing import List, Dict, Optional
from app.db.schemas.base import AppSchema


class WidgetImageList(AppSchema):
    image: str = []
    got_to: Optional[str] = ''
    id: Optional[int] = 0


class WidgetList(AppSchema):
    id: int
    widget_name: str
    title: str
    description: str
    no_of_images: int
    items: List[WidgetImageList] = []


class WidgetListSchema(AppSchema):
    status_code: int
    widgets: List[WidgetList]
