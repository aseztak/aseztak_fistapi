from typing import Optional

from pydantic import BaseModel





class UserCreate(BaseModel):
    name: str
    email: str
    password: str
class Login(BaseModel):
    email : str
    password: str

class User(BaseModel):
    email : str
    name: Optional[str] = None
    

class TokenData(BaseModel):
    email: Optional[str] = None


class Token(BaseModel):
    access_token : str
    token_type : str

class UserInDB(User):
    hashed_password: str