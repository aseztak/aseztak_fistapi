

from typing import List, Optional


from app.db.schemas.base import AppSchema


class Productprice(AppSchema):
    price: str
    moq: int
    unit: str


class Products(AppSchema):
    id: int
    title: str
    description: str
    deleted: Optional[bool] = False
    image: str
    pricing: Productprice


class Wishlist(AppSchema):
    id: int
    product: Products


class WishlistSchema(AppSchema):
    status_code: int
    wishlists: List[Wishlist] = []
    total_items: int
    current_page: int
    total_pages: Optional[int] = 0


class DeleteWishList(AppSchema):
    id: int


class AddWishList(AppSchema):
    product_id: int
