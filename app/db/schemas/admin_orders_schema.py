
from datetime import date
from typing import List, Dict, Optional


from pydantic.types import Json

from app.db.schemas.base import AppSchema


class OrderStatusSchema(AppSchema):
    status_id: int
    status_title:  str
    message: str
    visible: Optional[bool] = False
    created_at: str = ''


class ShippingAddressSchema(AppSchema):
    ship_to: str = ''
    address: str = ''
    phone: str = ''
    alt_phone: str = ''
    city: str = ''
    state: str = ''
    pincode: str = ''


class returnProofSchema(AppSchema):
    rejected: Optional[bool] = False
    text: str = ''


class OrderSchema(AppSchema):
    id: int
    reff: str
    order_number: str
    message: str
    image: str = ''
    products: Optional[list] = ''
    total_items: int
    item_total_amount: str
    delivery_charge: str
    discount: str
    grand_total: str
    invoice: str
    delivery_expected: str = ''
    created_at: str
    payment_method: str
    current_status: OrderStatusSchema
    return_period: str = ''
    order_refund_message: str = ''
    proof: Optional[returnProofSchema] = {'rejected': False, 'text': ''}


class OrderDataSchema(AppSchema):
    id: int
    reff: str
    order_number: str
    message: str
    image: str = ''
    invoice: str
    delivery_expected: str = ''
    created_at: str
    current_status: OrderStatusSchema
    return_period: str = ''
    order_refund_message: str = ''
    proof: Optional[returnProofSchema] = {'rejected': False, 'text': ''}


class ActionButtonSchema(AppSchema):
    code: str
    visible: Optional[bool] = False
    button_text: str


class OrderTrackingSchema(AppSchema):
    visible: Optional[bool] = False
    link: str = ''


class OrderItemsSchema(AppSchema):
    id: int
    status: int
    status_title: str
    message: str
    return_period: str
    checkbox: Optional[bool] = False
    product_id: int
    product_title: str
    product_description: str
    product_image: str
    product_deleted: Optional[bool] = False
    price: str
    item_quantity: str
    total: str
    attributes: Json


class OrderDetailSchema(AppSchema):
    status_code: int
    download_invoice: str
    order: Optional[OrderDataSchema] = {}
    order_summary: Optional[List] = []
    grand_total: Optional[object] = {}
    order_items: List[OrderItemsSchema] = []
    shipping_address: ShippingAddressSchema
    action_button: ActionButtonSchema
    order_tracking: OrderTrackingSchema
    download_invoice: str = ''
    cancel_reason: Optional[list] = []
    return_reason: Optional[list] = []
    order_status: Optional[list] = {}


class OrderListSchema(AppSchema):
    status_code: int
    order_items: List[OrderSchema] = []
    total_amount: float
    total_items: int
    current_page: int
    total_page: Optional[int] = 0


class currentStatusAdmin(AppSchema):
    status: str
    created_at: str
    message: Optional[str] = ''


class buyerForAdmin(AppSchema):
    name: str
    confirm: str


class AdminOrderSchema(AppSchema):
    id: int
    order_number: str
    total_items: int
    item_total_amount: float
    grand_total: float
    created_at: str
    payment_method: str
    current_status: currentStatusAdmin = {}
    buyer: buyerForAdmin
    seller: str
    images: Optional[List] = ''


class AdminOrderListSchema(AppSchema):
    status_code: int
    orders: List[AdminOrderSchema] = []
    total_records: int
    current_page: int
    total_page: int


class checkOutSchema(AppSchema):
    address_id: int
    payment_type: str
    proof: str = 'GSTIN007'
    proof_type: str = 'GSTIN'
    discount_rate: float
    discount_amount: float
    rounded_value: float
    total_amount: float
    partial_amount: float
    txn_order_id: Optional[str] = ''


class cancelOrderscheam(AppSchema):
    order_id: int
    message: str
    items: Optional[list] = []


class returnOrderSchema(AppSchema):
    order_id: int
    message: str
    items: Optional[list] = []
    account_type: str
    account_holder_name: str
    acc_no: str
    bank: str
    ifsc_code: str
    branch: str


class StatusSellerOrder(AppSchema):
    status_id: int
    status_title: str
    message: str
    created_at: str


class orderConfirmed(AppSchema):
    value: str
    text: str


class SellerOrders(AppSchema):
    id: int
    reff: str
    order_number: str
    order_confirmed: orderConfirmed
    message: str
    images: Optional[List] = []
    total_items: int
    total_amount: str
    invoice: str
    payment_method: str
    created_at: str
    status: StatusSellerOrder


class SellerOrdersSchema(AppSchema):
    status_code: int
    title: str
    total_orders: int
    orders: List[SellerOrders] = []
    current_page: int
    total_pages: int = 0


class CurrentStatus(AppSchema):
    status_id: int
    status_title: str
    message: str
    created_at: str


class CourierPartnerSchema(AppSchema):
    courier_partner: str
    tracking_id: str
    tracking_link: str


class SellerOrderDetail(AppSchema):
    id: int
    reff: str
    order_number: str
    order_confirmed: orderConfirmed
    message: str
    total_items: int
    total_amount: str
    invoice: Optional[str]
    payment_method: str
    current_status: CurrentStatus
    courier: CourierPartnerSchema
    created_at: str


class ItemStatusSchema(AppSchema):
    status_id: int
    status_title: str


class ProductSchema(AppSchema):
    id: int
    title: str
    description: str
    image: str
    status: int


class itemWeight(AppSchema):
    value: str
    weight_type: str


class SellerOrderItemsSchema(AppSchema):
    id: int
    status: ItemStatusSchema
    allow_process: Optional[bool] = False
    message: str
    product: ProductSchema
    price: str
    quantity: int
    total: str
    attributes: Json


class PicukpAddressSchema(AppSchema):
    id: int
    name: str


class InvoiceSchema(AppSchema):
    number: str
    date: str


class orderPackaging(AppSchema):
    lebel_pdf: str
    invoice_pdf: str


class SellerOrderDetailSchema(AppSchema):
    status_code: int
    invoice: InvoiceSchema
    item_weight_type: Optional[list] = []
    order: SellerOrderDetail
    packaging: orderPackaging
    items: List[SellerOrderItemsSchema] = []
    order_status: List[OrderStatusSchema] = []
    pickup_address: List[PicukpAddressSchema] = []
    cancel_reasons: Optional[List] = []


class AcceptOrdersSchema(AppSchema):
    item_id: int
    weight: float
    weight_type: str


class AcceptOrder(AppSchema):
    order_id: int
    invoice_number: str
    invoice_date: str
    address_id: int
    items: List[AcceptOrdersSchema]


class SellerCancelOrderSchema(AppSchema):
    id: int
    message: str = ''


class PackedOrders(AppSchema):
    order_id: int

# -----------------------------------------------------------------------


class AdminShippingAddressSchema(AppSchema):
    ship_to: str = ''
    address: str = ''
    phone: str = ''
    email: str
    city: str = ''
    state: str = ''
    pincode: str = ''
    locality: str = ''


class AdminSelleraddressSchema(AppSchema):
    seller: str = ''
    city: str = ''
    region: str = ''
    country: str = ''
    pincode: str = ''
    mobile: str = ''
    email: str = ''


class AdminItemsSchema(AppSchema):
    product_id: int
    product: str
    quantity: int
    attributes: Json
    image: str
    price: int
    total_amount: int
    status: str
    message: str = ''


class AdminOrderSummery(AppSchema):
    id: int
    order_number: str
    invoice: str
    invoice_date: str = ''
    payment_method: str
    transaction: str = ''
    order_total_amount: int
    cancel_total_amount: int
    return_total_amount: int
    delivery_charge: float
    total_tax: float
    discount: float
    discount_rate: float
    grand_total: float
    created_at: str


class AdminStatusSchema(AppSchema):
    created_at: str
    status: str
    message: str


class AdminOrderShippingSchema(AppSchema):
    wbns: Optional[str] = ''
    courier_partner: Optional[str] = ''
    tracking_link: Optional[str] = ''


class AdminOrderDetailsSchema(AppSchema):
    status_code: int
    shipping: AdminOrderShippingSchema = {}
    shipping_address: AdminShippingAddressSchema = []
    seller_address: AdminSelleraddressSchema = []
    items: List[AdminItemsSchema] = []
    order_summery: AdminOrderSummery = {}
    # current_status: str
    all_status: List[AdminStatusSchema] = []


class paymentToken(AppSchema):
    mid: str
    key_secret:  str
    website: str
    orderId: str
    amount: float
    callbackUrl: str
    custId: str
    mobile: str
    email: str
    firstName: str
    lastName: str
    mode: str


class PaymentTokenSchema(AppSchema):
    amount: float
    address_id: int
    payment_mode: str


# Seller Orders Search
class SellerOrderSearchSchema(AppSchema):
    status: int
    search: str = ''
    from_date: str = ''
    to_date: str = ''
    page: int = 1
    limit: int = 10


# return Cancel
class ReturnCancel(AppSchema):
    order_id: int


class AdminUserSearch(AppSchema):
    name: str = ''


class AdminUserschema(AppSchema):
    id: int
    name: str
    email_id: str
    address: str
    total_order: int
    confirm: str
    phone_no: str
    roles: str
    created_at: str = ''


class AdminUserListSchema(AppSchema):
    status_code: int
    users: List[AdminUserschema] = []
    total_records: int
    current_page: int
    total_page: int


class AdminOrderSearchSchema(AppSchema):

    from_date: date = ''
    to_date: date = ''


class AdminorderSearch(AppSchema):
    status: str
    from_date: Optional[str] = ''
    to_date: Optional[str] = ''
    search: Optional[str] = ''


class AdminUserWiseOrdersSchema(AppSchema):
    user_id: int
    param: str
    from_date: Optional[str] = ''
    to_date: Optional[str] = ''
    search: Optional[str] = ''


class AdminTopBuyerschema(AppSchema):
    id: int = ''
    buyer: str = ''
    email_id: str = ''
    address: str = ''
    total_order: int = ''
    phone_no: str = ''
    created_at: str = ''


class AdminTopBuyersListSchema(AppSchema):
    status_code: int
    top_buyers: List[AdminTopBuyerschema] = []
    total_records: int
    current_page: int
    total_page: int

# New Schema for marketer


class AdminMarketerSelleraddressSchema(AppSchema):
    seller: str = ''
    city: str = ''
    region: str = ''
    country: str = ''
    pincode: str = ''


class AdminMarketerOrderSummery(AppSchema):
    id: int
    order_number: str
    payment_method: str
    order_total_amount: int
    delivery_charge: float
    discount: float
    discount_rate: float
    cancel_total_amount: float
    return_total_amount: float
    grand_total: float
    created_at: str


class AdminMarketerOrderDetailsSchema(AppSchema):
    status_code: int
    shipping: AdminOrderShippingSchema = {}
    shipping_address: AdminShippingAddressSchema = []
    seller_address: AdminMarketerSelleraddressSchema = []
    item_detail: List[AdminItemsSchema] = []
    order_summery: AdminMarketerOrderSummery = {}
    order_status: List[AdminStatusSchema] = []


class AdminMarketerorderList(AppSchema):
    user_id: int
    key: str
    search: str = ''
    from_date: Optional[str] = ''
    to_date: Optional[str] = ''
    page: int = 1
    limit: int = 10


class AdminMarketerorderDetails(AppSchema):
    status: str
    search: str = ''
    from_date: Optional[str] = ''
    to_date: Optional[str] = ''
    page: int = 1
    limit: int = 10


class AdminMarketerBuyerList(AppSchema):
    search: str = ''
    from_date: Optional[str] = ''
    to_date: Optional[str] = ''
    page: int = 1
    limit: int = 10
