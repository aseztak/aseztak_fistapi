

from datetime import date
from typing import List, Optional
from app.db.schemas.base import AppSchema
from pydantic import  validator


class UserRoleSchema(AppSchema):
    role_id: int


class UserProfileSchema(AppSchema):
    id: int
    proof_type: str
    proof: str


class UserSchema(AppSchema):
    id: int
    name: str
    email: str
    city: str
    region: str
    country: str
    pincode: str
    mobile: str
    status: int
    role: UserRoleSchema
    profile: UserProfileSchema


class UpdateProfile(AppSchema):
    name: str = ''
    email: str = ''
    mobile: str = ''
    proof: str = ''
    proof_type: str = ''
    country: str = ''
    region: str = ''
    pincode: str = ''
    city: str = ''

    @validator('name')
    def check_title_required(cls, name: str):
        if name == '':
            raise ValueError('Name field is required')
        else:
            return name

    @validator('proof_type')
    def check_slug_required(cls, proof_type: str):
        if proof_type == '':
            raise ValueError('Proof Type field is required')
        else:
            return proof_type


class SellerProfileUpdateSchema(AppSchema):
    name: str = ''
    email: str = ''
    country: str = ''
    region: str = ''
    pincode: str = ''
    city: str = ''
    business_type: str = ''
    proof: str = ''

    @validator('name')
    def check_title_required(cls, name: str):
        if name == '':
            raise ValueError('Name field is required')
        else:
            return name


class UpdateMobileSchema(AppSchema):
    mobile: str = ''
    signature: str = ''

    @validator('mobile')
    def mobile_10digit(cls, v):
        if len(str(v)) != 10:
            raise ValueError('Mobile number must be 10 digits')
        return v


class UpdateMobileNumberSchema(AppSchema):
    mobile: str = ''
    otp: str = ''

    @validator('otp')
    def mobile_10digit(cls, v):
        if len(str(v)) == '':
            raise ValueError('Otp field is required')
        return v


class UserBankDetails(AppSchema):
    account_holder_name: str = ''
    account_type: str = ''
    acc_no: str
    bank: str
    branch: str
    ifsc_code: str


class UserBankDetailsSchema(AppSchema):
    status_code: int
    account: Optional[UserBankDetails] = {}


class UpdateBankDetailSchema(AppSchema):
    account_holder_name: str
    acc_no: str
    bank: str
    branch: str
    ifsc_code: str
    account_type: str


class BillingAddress(AppSchema):
    name: str
    address: str
    city: str
    state: str
    pincode: str


class BillingAddressSchema(AppSchema):
    status_code: int
    billing_address:  Optional[BillingAddress] = {}


class UpdateBillingAddressSchema(AppSchema):
    name: str
    address: str
    city: str
    state: str
    pincode: str


class UpdateUserFullDetailsSchema(AppSchema):
    id: int
    name: str
    email: str
    mobile: str
    country: str
    region: str
    city: str
    pincode: str
    role: int
    status: int
    account_holder_name: str
    acc_no: str
    bank: str
    branch: str
    ifsc_code: str


# -------------------------------------------------------------------------------

class staticcountsSchema(AppSchema):
    status: str
    name: str
    value: int


class AdminUserSchema(AppSchema):
    id: int
    name: str
    email_id: str
    address: str
    phone_no: str
    roles: str
    created_at: str
    status: str


class AdminUserListSchema(AppSchema):
    status_code: int
    users: List[AdminUserSchema]
    current_page: int
    total_records: int
    total_page: int


class AdminUserProfileSchema(AppSchema):
    business_type: str = ''
    proof_type: str
    proof: str


class AdminUserBankDetailsSchema(AppSchema):
    account_holder_name: str = ''
    bank: str = ''
    branch: str = ''
    acc_no: str = ''
    ifsc_code: str = ''


class AdminUserInfoSchema(AppSchema):
    name: str
    email: str
    country: str
    city: str
    region: str
    mobile: str
    roles: str


class AdminUserDetailsSchema(AppSchema):
    status_code: int
    user_info: AdminUserInfoSchema
    user_profile: AdminUserProfileSchema
    user_bankdetails: AdminUserBankDetailsSchema


class TopSellersList(AppSchema):
    id: int
    name: str
    address: str


class TopSellerListSchema(AppSchema):
    status_code: int
    sellers: List[TopSellersList]


class FacilitySchema(AppSchema):
    key_value: str

# New Schema for marketer


class AddUsers(AppSchema):
    mobile: str


class MarketingSchema(AppSchema):
    from_date: date = ''
    to_date: date = ''
