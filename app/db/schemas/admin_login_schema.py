from typing import Optional

from app.db.schemas.base import AppSchema


class Login(AppSchema):
    email: str
    password: str


class Token(AppSchema):
    access_token: str
    token_type: str
    status: str
    message: str


class TokenData(AppSchema):
    username: Optional[str] = None


class User(AppSchema):
    username: str
    email: Optional[str] = None
    full_name: Optional[str] = None


class UserInDB(User):
    hashed_password: str