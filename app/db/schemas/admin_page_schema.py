from datetime import date
from typing import List


from app.db.schemas.base import AppSchema


class PageSchema(AppSchema):
    id: int
    name: str
    slug: str
    content: str
    updated_at: date


class PageDataSchema(AppSchema):
    page: PageSchema



class AdminPageSchema(AppSchema):
    id: int
    name: str
    slug: str
    content: str
    created_at: date = ''
    updated_at: date = ''


class AdminPageDataListSchema(AppSchema):
    status_code: int
    pages: List[AdminPageSchema] = []
    total_records: int
    current_page: int
    total_page: int




class AdminAddPageSchema(AppSchema):
    name: str
    content: str
    is_menu: bool



class AdminUpdatePageSchema(AppSchema):
    id: int
    name: str
    content: str
