
from typing import List, Dict, Optional

from app.db.schemas.base import AppSchema


class Status(AppSchema):
    status_id: int
    status_title: str


class InvoiceList(AppSchema):
    order_id: int
    invoice_date: str
    order_reff: str
    order_number: str
    total_amount: str
    discount: Optional[str] = '0.00'
    service: Optional[str] = '0.00'
    total_items: int
    invoice: str
    invoice_pdf: str
    status: Status
    igst: str
    cgst: str
    sgst: str
    grand_total: str


class InvoiceListSchema(AppSchema):
    status_code: int
    total_items: int
    invoices: List[InvoiceList]
    current_page: int
    total_pages: Optional[int] = 0

# Search Invoice List


class SearchInvoiceListSchema(AppSchema):
    from_date: str = ''
    to_date: str = ''
    page: int = 1
    limit: int = 10

# Export Invoice List


class ExportInvoiceListSchema(AppSchema):
    from_date: str = ''
    to_date: str = ''
