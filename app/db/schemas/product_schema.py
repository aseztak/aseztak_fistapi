
from datetime import date

from typing import List, Dict, Optional

from app.db.schemas.base import AppSchema


class InventorySchema(AppSchema):
    pass


class ProductPricingSchema(AppSchema):
    id: int
    price: float
    moq: int
    unit: str


class ProductPricing(AppSchema):
    id: int
    price: str
    moq: int
    unit: str


class FavouriteSchema(AppSchema):
    pass


class SellerData(AppSchema):
    id: int
    name: str
    city: str
    region: str


class DiscountSchema(AppSchema):
    rate: float
    text: str


class DeliverySchema(AppSchema):
    title: str
    rate: int
    text: str


class ReturnSchema(AppSchema):
    title: str
    rate: int
    text: str


class PaymentTextSchema(AppSchema):
    title: str
    text: str


class ProductSchema(AppSchema):
    id: int
    title: str
    image: str = ''
    slug: str
    short_description: str
    category: int
    pricing: ProductPricing
    created_at: date
    stock: Optional[bool] = True
    favourite: bool
    status: int
    delivery: DeliverySchema
    payment: PaymentTextSchema

    return_days: ReturnSchema
    discount: DiscountSchema

    seller: SellerData


class ProductDataSchema(AppSchema):
    id: int
    title: str
    image: str = ''
    short_description: str
    category: int
    # pricing: ProductPricing
    pricing: Optional[object] = {}
    favourite: bool
    is_trending: Optional[bool] = False
    is_best_selling: Optional[bool] = False
    is_new_arrival: Optional[bool] = False
    created_at: date
    status: int
    seller_discount: Optional[str] = '0'


class filterPricingSchema(AppSchema):
    min_price: float
    max_price: float


class ProductData(AppSchema):
    status_code: int
    is_registered: Optional[bool] = False
    title: Optional[str] = ''
    products: List[ProductDataSchema] = []
    total_products: Optional[int] = 0
    current_page: Optional[int] = 0
    total_pages: Optional[int] = 0


class ProductDataTabWise(AppSchema):
    status_code: int
    is_registered: Optional[bool] = False
    type: str
    title: Optional[str] = ''
    products: List[ProductDataSchema] = []
    total_products: Optional[int] = 0
    current_page: Optional[int] = 0
    total_pages: Optional[int] = 0


class CategorySchema(AppSchema):
    id: int
    title: str


class ProductDetailsSchema(AppSchema):
    id: int
    title: str
    image: str
    pricing: ProductPricing


class SellerWiseProductsSchema(AppSchema):
    category: CategorySchema
    total_products: int
    productlist: List[ProductDetailsSchema]


class ProdcutDataSellerWiseSchema(AppSchema):
    status_code: int
    seller_name: Optional[str] = ''
    products: List[SellerWiseProductsSchema] = []
    current_page: int
    total_pages: Optional[int] = 0


class SpecificationSchema(AppSchema):
    name: str
    values: str


class ProductDetailSchema(AppSchema):
    id: int
    title: str
    images: Optional[List] = []
    slug: str
    short_description: str
    category: int
    pricing: ProductPricing
    created_at: date
    stock: Optional[bool] = True
    favourite: bool
    status: int
    delivery: DeliverySchema
    payment: PaymentTextSchema

    return_days: ReturnSchema
    discount: DiscountSchema

    seller: SellerData


class ProductDetail(AppSchema):
    status_code: int
    product: Optional[ProductDetailSchema] = {}
    specification: List[SpecificationSchema] = []
    seller_products: List[ProductDataSchema] = []
    similarproducts: List[ProductDataSchema] = []


class ProductDetailSchemaNew(AppSchema):
    id: int
    title: str
    images: Optional[List] = []
    slug: str
    short_description: str
    category: int
    pricing: Optional[object] = {}
    created_at: str
    stock: Optional[bool] = True
    favourite: bool
    assured: Optional[bool] = True
    status: int
    delivery: DeliverySchema
    payment: PaymentTextSchema

    return_days: ReturnSchema
    discount: DiscountSchema
    total_wishlist: Optional[str] = ''
    seller: Optional[object] = {}


class ProductDataSchemaNew(AppSchema):
    id: int
    title: str
    image: str = ''
    short_description: str
    category: int
    pricing: Optional[object] = {}
    favourite: bool
    is_trending: Optional[bool] = False
    is_best_selling: Optional[bool] = False
    is_new_arrival: Optional[bool] = False
    created_at: date
    status: int
    seller_discount: Optional[str] = '0'


class ProductDetailNew(AppSchema):
    status_code: int
    is_registered: Optional[bool] = False
    purchase_limit_text: Optional[str] = ''
    product: Optional[ProductDetailSchemaNew] = {}
    specification: List[SpecificationSchema] = []
    seller_products: List[ProductDataSchemaNew] = []
    similarproducts: List[ProductDataSchemaNew] = []


class ProductPricingListSchema(AppSchema):
    id: int
    product_id: int
    image: str = ''
    price: str
    moq: int
    tax: str
    quantity: int
    item_total: str
    sale_price: str
    description: str
    stock: Optional[bool] = False
    unlimited: Optional[bool] = False
    total_stock: Optional[int] = 0
    out_of_quantity: Optional[bool] = False
    message: Optional[str] = ''
    unit: str
    uuid: str
    hsn_code: str
    attributes: Optional[list] = []

    unit: str


class ProductBuySchema(AppSchema):
    id: int
    title: str
    slug: str
    short_description: str


class ProductBuyDataSchema(AppSchema):
    status_code: int
    product: Optional[ProductBuySchema] = {}
    pricing: List[ProductPricingListSchema] = []


class filterAttributes(AppSchema):
    name: str
    values: Optional[list]


class filterAttributesSchema(AppSchema):
    status_code: int
    pricing: Optional[filterPricingSchema] = {}
    filter_attributes: List[filterAttributes] = []


class filterAttributesV4Schema(AppSchema):
    status_code: int
    filter_attributes: List[filterAttributes] = []


class FilterDataSchema(AppSchema):
    id: int
    sort: str
    min_price: str
    max_price: str
    attributes: Optional[list] = []
    page: int = 1
    limit: int = 10


class FiltertagDataSchema(AppSchema):
    id: int
    sort: str
    min_price: str
    max_price: str
    attributes: Optional[list] = []
    page: int = 1
    limit: int = 10


class CreateProductSchema(AppSchema):
    title: str = ''
    category: int
    short_description: str = ''


class UpdateProductSchema(AppSchema):
    id: int
    title: str = ''
    short_description: str = ''


class AttributeSchema(AppSchema):
    code: str
    values: Optional[List] = []


class AttributeAddSchema(AppSchema):
    product_id: int
    attributes: List[AttributeSchema]


class AddPricingSchema(AppSchema):
    product_id: int
    price: float
    tax: Optional[float] = 0.00
    hsn: str
    mrp: Optional[float] = 0.00
    moq: int
    unit: str
    items: int
    description: str
    seller_amount: Optional[float] = 0.00
    stock: int = 0
    unlimited: Optional[bool] = False
    attributes: Optional[list] = []
    default_image: Optional[int] = 0


class UpdatePricingSchema(AppSchema):
    id: int
    price: float
    tax: Optional[float] = 0.00
    hsn: str
    mrp: Optional[float] = 0.00
    moq: int
    unit: str
    items: int
    description: str
    seller_amount: Optional[float] = 0.00
    stock: int = 0
    unlimited: Optional[bool] = False
    out_of_stock: Optional[bool] = False
    attributes: Optional[list] = []
    default_image: Optional[int] = 0


# {
#     "id": 264,
#     "hsn": "6107",
#     "tax": 5.0,
#     "mrp": 150,
#     "items": 10,
#     "moq": 10,
#     "unit": "Pcs",
#     "unlimited": false,
#     "stock": 10,
#     "out_of_stock": false
#     "price": 124,
#     "seller_amount": 1000
#     "description": "Set of 10 pc Multiple Color",
#     "attributes": [1092, 1091, 1077]
# }
class SellerCategoryListSchema(AppSchema):
    id: int
    name: str
    description: str
    image: str
    total_items: int


class SellerProductListCategoryWise(AppSchema):
    status_code: int
    total_products: int
    categories: List[SellerCategoryListSchema] = []
    current_page: int
    total_pages: Optional[int] = 0


class SellerProductDataSchema(AppSchema):
    id: int
    name: str
    description: Optional[str] = ''
    image: str
    price: str
    moq: int
    unit: str
    stock: Optional[bool] = False
    status: str
    product_rejection: Optional[bool] = False
    hide_product: Optional[bool] = False
    created_at: str


class SellerAllProductListSchema(AppSchema):
    status_code: int
    total_items: int
    category: str = ''

    items: List[SellerProductDataSchema]
    current_page: int
    total_pages: int


class SellerAllProductList(AppSchema):
    status_code: int
    total_items: int
    items: List[SellerProductDataSchema]
    current_page: int
    total_pages: int


class CountStockProductsSchema(AppSchema):
    status_code: int
    count_products: Optional[List] = []


class StockPricingSchema(AppSchema):
    price_id: int
    price: str
    moq: int
    unit: str
    stock: int
    unlimted: Optional[bool] = False
    price_attributes: Optional[List] = []


class StockProducts(AppSchema):
    id: int
    title: str
    description: str
    created_at: str
    image: str
    pricing: StockPricingSchema


class StockProductsSchema(AppSchema):
    status_code: int
    total_items: int
    items: List[StockProducts]
    current_page: int
    total_pages: int


class DeleteImage(AppSchema):
    id: int


class LowStockProductList(AppSchema):
    id: int
    name: str
    image: str
    price: float
    stock: int


class LowStockProductListSchema(AppSchema):
    status_code: int
    total_items: int
    items: List[LowStockProductList]
    current_page: int
    total_pages: Optional[int] = 0


class Brand(AppSchema):
    name: str
    logo: str


class ProductOverView(AppSchema):
    title: str
    category: str
    description: str
    status: str
    hide_product: Optional[bool] = False
    stock: Optional[bool] = True
    created_at: str
    brand: Brand
    images: Optional[List]
    has_attribute: bool
    attributes: Optional[List]
    pricing: Optional[List]


class ProductOverViewSchema(AppSchema):
    status_code: int
    product: ProductOverView


class DeleteProductSchema(AppSchema):
    id: int


class DeleteProducPriceSchema(AppSchema):
    id: int


class PriceStock(AppSchema):
    id: int

# Seller Product Search Category Wise


class SellerProductSearchSchema(AppSchema):
    category_id: int
    status: str = ''
    search: str = ''
    from_date: str = ''
    to_date: str = ''
    page: int = 1
    limit: int = 10

# Seller Search All Products


class SellerSearchAllProducts(AppSchema):
    search: str = ''
    from_date: str = ''
    to_date: str = ''
    page: int = 1
    limit: int = 10


class disableProductSchema(AppSchema):
    id: int


# Seller-wise-Products-schema(Tina)
class SellerDeatilsSchema(AppSchema):
    id: int
    name: str
    address: str
    brands: Optional[list] = []
    level: str = ''
    rating: str = ''
    since_year: str = ''
    rated_by_customer: str = ''
    no_of_categories: str = ''
    no_of_products: str = ''
    purchase_limit_text: Optional[str] = ''
    discount_rate: Optional[str] = '0'
    discount_amount: Optional[str] = '0'


class CategoriesSchema(AppSchema):
    id: int
    title: str
    image:  str = ''


class ProductDetailV4Schema(AppSchema):
    id: int
    title: str
    image: str
    # pricing: ProductPricing
    pricing: Optional[object] = {}


class SellerWiseProductSchema(AppSchema):
    total_products: int
    category: CategoriesSchema
    products: List[ProductDetailV4Schema]


class ProductDataSellerWiseSchema(AppSchema):
    status_code: int
    is_registered: Optional[bool] = False
    seller: SellerDeatilsSchema
    items: List[SellerWiseProductSchema] = []
    current_page: int
    total_pages: Optional[int] = 0


class RecentlyViewedProductSchema(AppSchema):
    id: Optional[List] = []


class ProductSearchschema(AppSchema):
    id: int
    title: str
    slug: str
    status: int


class ProductSearchList(AppSchema):
    products: List[ProductSearchschema] = []


class ProductSearchResult(AppSchema):
    title: str
    page: int = 1
    limit: int = 10


class SellerProductSearch(AppSchema):
    category_id: int
    status: str
    search: str = ''
    from_date: str = ''
    to_date: str = ''
    page: int = 1
    limit: int = 10


class SellerWiseProductData(AppSchema):
    status_code: int
    is_registered: Optional[bool] = False
    title: Optional[str] = ''
    seller: SellerDeatilsSchema
    products: List[ProductDataSchema] = []
    total_products: Optional[int] = 0
    current_page: Optional[int] = 0
    total_pages: Optional[int] = 0


# Seller Discounts
class SellerDiscountSchema(AppSchema):
    id: int = 0
    name: str = ''
    city: str = ''
    state: str = ''
    title: str = ''
    subtitle: str = ''
    discount: str = '0'
    amount: str = '0'
    valid_upto: str = ''
    products: List[ProductDataSchemaNew] = []


class SellerDiscountList(AppSchema):
    status_code: int
    sellers: List[SellerDiscountSchema] = []


# Top Sale Products
class topsaleproductschema(AppSchema):
    from_date: str = ''
    to_date: str = ''
    page: int = 1
    limit: int = 10

# Most Selling/Discounted/All Products


class mostsellingdiscountedproducts(AppSchema):
    type: str = ''
    category_id: int = 0
    sorting: str = ''
    page: int = 1
    limit: int = 10
