
from typing import List, Optional
from app.db.schemas.base import AppSchema



class CategorySchema(AppSchema):
    id: int
    name: str
    slug: str
    description: Optional[str] = ''
    image: Optional[str] = ''
    status: int
    haschild: Optional[bool] = False
    # products: List[ProductSchema]


class CategoryData(AppSchema):
    title: Optional[str] = ''
    categories: List[CategorySchema] = []


class SelectableCategory(AppSchema):
    id: int
    name: str
    description: Optional[str] = ''
    image: Optional[str] = ''
    status: int


class SelectableCategorySchema(AppSchema):
    status_code: int
    total_count: Optional[int] = 0
    categories: List[SelectableCategory]
    current_page: int
    total_pages: int


class CategoriesSearch(AppSchema):
    parent_id: int
    search: str


class searchselectablecategories(AppSchema):
    search: Optional[str] = ''
    page: Optional[int] = 1
    limit: Optional[int] = 10
