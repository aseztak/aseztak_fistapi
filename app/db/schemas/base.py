# from app.db.models.base import AppModel

from pydantic import BaseModel


class AppSchema(BaseModel):

    class Config():
        orm_mode = True
