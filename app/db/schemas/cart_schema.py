
from datetime import date

from typing import List, Dict, Optional
from pydantic.types import Json

from app.db.schemas.base import AppSchema


class CartSchema(AppSchema):
    id: int
    user_id: int
    product_id: int
    product_title: Optional[str] = ''
    product_description: Optional[str] = ''
    image: str = ''
    quantity: int
    moq: Optional[int] = 0
    unit: Optional[str] = ''
    price: str
    tax: float
    total: str
    stock: Optional[bool] = False
    total_stock: Optional[int] = 0
    out_of_quantity: Optional[bool] = False
    message: Optional[str] = ''
    unlimited: Optional[bool] = False
    attributes: Json
    uuid: str
    hsn_code: str
    created_at: date


class CartData(AppSchema):
    status_code: int
    place_order_button: Optional[bool] = True
    message: str
    cart_items: List[CartSchema] = []
    total_amount: str
    total_items: int


class SaveCartData(AppSchema):
    status_code: int
    cart_items: List[CartSchema] = []
    total_amount: str
    total_items: int
    current_page: int
    total_page: int


class UpdateCartSchema(AppSchema):
    id: int
    quantity: int


class DeleteCartSchema(AppSchema):
    id: int


class AddToCartSchema(AppSchema):
    product_id: int
    quantity: int
    price: float
    tax: float
    uuid: str
    hsn_code: str
    attributes: List


class AddCartSchema(AppSchema):
    carts: List[AddToCartSchema]


# class ProductdataSchema(AppSchema):
#     image: Optional[str] = ''
#     id: int
#     title: str = ''
#     slug: str = ''
#     items: int = ''
#     amount: float = ''


# class CartSummarySchema(AppSchema):
#     product_data: List[ProductdataSchema] = [{}]


# class CartItemsSchema(AppSchema):
#     item_data: List[CartSummarySchema] = []
