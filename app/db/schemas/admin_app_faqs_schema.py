from typing import List

from app.db.schemas.base import AppSchema


class AdminFaqsSchema(AppSchema):
    id: int
    flag: str
    question: str
    answer: str


class AdminFaqsDataListSchema(AppSchema):
    status_code: int
    faqs: List[AdminFaqsSchema] = []
    total_records: int
    current_page: int
    total_page: int


class AdminAddFaqsSchema(AppSchema):
    flag: str
    question: str
    answer: str


class AdminUpdateFaqsSchema(AppSchema):
    id: int
    flag: str
    question: str
    answer: str
