from typing import List
from app.db.schemas.base import AppSchema


class BannerList(AppSchema):
    id: int
    banner: str


class BannerListSchema(AppSchema):
    banners: List[BannerList]



#------------------------------------------------------------------------------------


class AdminBannerSchema(AppSchema):
    id: int
    banner: str
    type: str
    status: int



class AdminBannerListSchema(AppSchema):
    status_code: int
    banners: List[AdminBannerSchema] = []
    total_records: int
    current_page:int
    total_page: int


class AdminBannerDeleteSchema(AppSchema):
    id: int