
from typing import List, Dict, Optional
from pydantic.types import Json

from app.db.schemas.base import AppSchema


class OrderStatusSchema(AppSchema):
    status_id: int
    status_title:  str
    message: str
    visible: Optional[bool] = False
    created_at: str = ''


class ShippingAddressSchema(AppSchema):
    ship_to: str = ''
    address: str = ''
    phone: str = ''
    alt_phone: str = ''
    city: str = ''
    state: str = ''
    pincode: str = ''


class returnProofSchema(AppSchema):
    rejected: Optional[bool] = False
    text: str = ''


class OrderSchema(AppSchema):
    id: int
    reff: str
    order_number: str
    message: str
    image: str = ''
    products: Optional[list] = ''
    total_items: int
    item_total_amount: str
    delivery_charge: str
    discount: str
    grand_total: str
    invoice: str
    delivery_expected: str = ''
    created_at: str
    payment_method: str
    current_status: OrderStatusSchema
    return_period: str = ''
    order_refund_message: str = ''
    proof: Optional[returnProofSchema] = {'rejected': False, 'text': ''}


class OrderDataSchema(AppSchema):
    id: int
    reff: str
    order_number: str
    message: str
    image: str = ''
    invoice: str
    delivery_expected: str = ''
    created_at: str
    current_status: OrderStatusSchema
    return_period: str = ''
    order_refund_message: str = ''
    proof: Optional[returnProofSchema] = {'rejected': False, 'text': ''}


class ActionButtonSchema(AppSchema):
    code: str
    visible: Optional[bool] = False
    button_text: str


class OrderTrackingSchema(AppSchema):
    visible: Optional[bool] = False
    track_id: str = ''
    delivery_partner_name: str = ''
    link: str = ''


class OrderItemsSchema(AppSchema):
    id: int
    status: int
    status_title: str
    message: str
    return_period: str
    checkbox: Optional[bool] = False
    product_id: int
    product_title: str
    product_description: str
    product_image: str
    product_deleted: Optional[bool] = False
    price: str
    item_quantity: str
    total: str
    attributes: Json


class OrderDetailSchema(AppSchema):
    status_code: int
    rewards_text: Optional[str] = ''
    use_wallet_amount: Optional[float] = 0
    download_invoice: str
    order: Optional[OrderDataSchema] = {}
    order_summary: Optional[List] = []
    grand_total: Optional[object] = {}
    order_items: List[OrderItemsSchema] = []
    shipping_address: ShippingAddressSchema
    action_button: ActionButtonSchema
    order_tracking: OrderTrackingSchema
    download_invoice: str = ''
    cancel_reason: Optional[list] = []
    return_reason: Optional[list] = []
    order_status: Optional[list] = {}
    return_proof_images: Optional[list] = []
    partial_payment: Optional[object] = {}
    refund_transactions: Optional[list] = []


class SearchOrderSchema(AppSchema):
    status: str
    search: Optional[str] = 'search'
    from_date: Optional[str] = 'from_date'
    to_date: Optional[str] = 'to_date'
    page: int = 1
    limit: int = 15


class OrderListSchema(AppSchema):
    status_code: int
    title: Optional[str] = ''
    order_items: List[OrderSchema] = []
    total_amount: float
    total_items: int
    current_page: int
    total_page: Optional[int] = 0


class currentStatusAdmin(AppSchema):
    status: str
    created_at: str


class buyerForAdmin(AppSchema):
    name: str
    confirm: str


class AdminOrderSchema(AppSchema):
    id: int
    order_number: str
    total_items: int
    item_total_amount: float
    grand_total: float
    created_at: str
    payment_method: str
    current_status: currentStatusAdmin
    buyer: buyerForAdmin
    seller: str


class AdminOrderListSchema(AppSchema):
    status_code: int
    orders: List[AdminOrderSchema] = []
    current_page: int
    total_page: int


class checkOutSchema(AppSchema):
    address_id: int
    seller_id: Optional[list] = []
    payment_type: str
    proof: Optional[str] = ''
    proof_type: Optional[str] = ''
    discount_rate: float
    discount_amount: float
    rounded_value: float
    itemtotalamount: Optional[str] = '0.00'
    total_amount: float
    wallet_amount: Optional[str] = '0.00'
    partial_amount: float
    txn_order_id: Optional[str] = ''


class cancelOrderscheam(AppSchema):
    order_id: int
    message: str
    items: Optional[list] = []


class returnOrderSchema(AppSchema):
    order_id: int
    message: str
    items: Optional[list] = []
    account_type: str
    account_holder_name: str
    acc_no: str
    bank: str
    ifsc_code: str
    branch: str


class StatusSellerOrder(AppSchema):
    status_id: int
    status_title: str
    message: str
    created_at: str


class orderConfirmed(AppSchema):
    value: str
    text: str


class SellerOrders(AppSchema):
    id: int
    reff: str
    order_number: str
    order_confirmed: orderConfirmed
    message: str
    images: Optional[List] = []
    total_items: int
    total_amount: str
    invoice: str
    payment_method: str
    created_at: str
    status: StatusSellerOrder


class SellerOrdersSchema(AppSchema):
    status_code: int
    title: str
    total_orders: int
    orders: List[SellerOrders] = []
    current_page: int
    total_pages: int = 0


class CurrentStatus(AppSchema):
    status_id: int
    status_title: str
    message: str
    created_at: str


class CourierPartnerSchema(AppSchema):
    courier_partner: str
    tracking_id: str
    tracking_link: str


class SellerOrderDetail(AppSchema):
    id: int
    reff: str
    order_number: str
    order_confirmed: orderConfirmed
    message: str
    total_items: int
    total_amount: str
    invoice: Optional[str]
    payment_method: str
    current_status: CurrentStatus
    courier: CourierPartnerSchema
    created_at: str


class ItemStatusSchema(AppSchema):
    status_id: int
    status_title: str


class ProductSchema(AppSchema):
    id: int
    title: str
    description: str
    image: str
    status: int


class itemWeight(AppSchema):
    value: str
    weight_type: str


class SellerOrderItemsSchema(AppSchema):
    id: int
    status: ItemStatusSchema
    allow_process: Optional[bool] = False
    message: str
    product: ProductSchema
    price: str
    unit: Optional[str] = ''
    quantity: int
    total: str
    attributes: Json
    item_weight: Optional[object] = {
        "item_id": 0,
        "weight": 0,
        "weight_type": ""
    }


class PicukpAddressSchema(AppSchema):
    id: int
    name: str


class InvoiceSchema(AppSchema):
    number: str
    date: str


class orderPackaging(AppSchema):
    lebel_pdf: str
    invoice_pdf: str


class SellerOrderDetailSchema(AppSchema):
    status_code: int
    invoice: InvoiceSchema
    item_weight_type: Optional[list] = []
    order:  SellerOrderDetail
    packaging: orderPackaging
    items: List[SellerOrderItemsSchema] = []
    order_status: List[OrderStatusSchema] = []
    pickup_address: List[PicukpAddressSchema] = []
    cancel_reasons: Optional[List] = []


class AcceptOrdersSchema(AppSchema):
    item_id: int
    weight: float
    weight_type: str


class AcceptOrder(AppSchema):
    order_id: int
    invoice_number: str
    invoice_date: str
    address_id: int
    shipment_height: str = '0.00'
    shipment_width: str = '0.00'
    shipment_length: str = '0.00'
    items: List[AcceptOrdersSchema]


class SellerCancelOrderSchema(AppSchema):
    id: int
    message: str = ''


class PackedOrders(AppSchema):
    order_id: int

# -----------------------------------------------------------------------


class AdminShippingAddressSchema(AppSchema):
    ship_to: str = ''
    address: str = ''
    phone: str = ''
    email: str
    city: str = ''
    state: str = ''
    pincode: str = ''
    locality: str = ''


class AdminSelleraddressSchema(AppSchema):
    address: str = ''
    name: str = ''
    city: str = ''
    country: str = ''
    pin: str = ''
    phone: str = ''
    email: str = ''


class AdminItemsSchema(AppSchema):
    productid: int
    product: str
    quantity: int
    attributes: Json
    image: str
    price: int
    total_amount: int


class AdminOrderSummery(AppSchema):
    id: int
    order_number: str
    invoice: str
    invoice_date: str
    payment_method: str
    created_at: str
    status: str
    wbns: str
    total_items: int
    items_total_amount: float
    delivery_charge: float
    discount: float
    grand_total: float


class AdminStatusSchema(AppSchema):
    created_at: str
    status: str


class AdminOrderDetailsSchema(AppSchema):
    status_code: int
    shipping_address: AdminShippingAddressSchema = []
    pickup_address: AdminSelleraddressSchema = []
    items: List[AdminItemsSchema] = []
    order_summery: AdminOrderSummery
    all_status: List[AdminStatusSchema] = []


class paymentToken(AppSchema):
    mid: str
    key_secret:  str
    website: str
    orderId: str
    amount: float
    callbackUrl: str
    custId: str
    mobile: str
    email: str
    firstName: str
    lastName: str
    mode: str


class PaymentTokenSchema(AppSchema):
    amount: float
    address_id: int
    payment_mode: str


# Seller Orders Search
class SellerOrderSearchSchema(AppSchema):
    status: int
    search: str = ''
    from_date: str = ''
    to_date: str = ''
    page: int = 1
    limit: int = 10


# return Cancel
class ReturnCancel(AppSchema):
    order_id: int

# PAYMENT CSV DATE WISE


class CommissionOrderSchema(AppSchema):
    from_date: str = ''
    to_date: str = ''


#Payment Option (CheckOut)
class paymentoption(AppSchema):
    payment_type: str = ''
    payment_key: str = ''
    wallet_amount: Optional[str] = '0.00'
