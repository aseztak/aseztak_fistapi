from typing import List, Dict, Optional

from app.db.schemas.base import AppSchema


class BrandList(AppSchema):
    id: int
    name: str
    logo: str
    status: Optional[bool] = False


class BrandListSchema(AppSchema):
    status_code: int
    total_brands: int
    brands: List[BrandList]
    current_page: int
    total_pages: Optional[int] = 0


class V4SellerBrandListSchema(AppSchema):
    status_code: int
    total_brands: int
    brands: List[BrandList]


class UpdateBrandSchema(AppSchema):
    id: int
    name: str


# Buyer Brands List
class TopBrandsList(AppSchema):
    id: int
    title: str
    logo: str


class TopBrands(AppSchema):
    status_code: int
    brands: List[TopBrandsList]
