
from typing import List, Optional
from app.db.schemas.base import AppSchema
from datetime import date


class CategorySchema(AppSchema):
    id: int
    name: str
    slug: str
    description: Optional[str] = ''
    image: Optional[str] = ''
    status: int
    haschild: Optional[bool] = False


class BannerSchema(AppSchema):
    id: int
    banner: str
    link_id: Optional[int] = 0
    link: Optional[str] = ''


class WidgetImageList(AppSchema):
    image: str = []
    go_to: Optional[str] = ''
    id: Optional[int] = 0


class WidgetList(AppSchema):
    id: int
    start_date: Optional[int] = 0
    end_date: Optional[int] = 0
    widget_name: str
    title: str
    description: str
    no_of_images: int
    items: List[WidgetImageList] = []


class ProductPricing(AppSchema):
    id: int
    price: str
    moq: int
    unit: str


class ProductDataSchema(AppSchema):
    id: int
    title: str
    image: str = ''
    short_description: str
    category: int
    pricing: ProductPricing
    favourite: bool
    created_at: date
    status: int


class FeaturedProducts(AppSchema):
    title: str
    products: List[ProductDataSchema] = []


class HomeSchema(AppSchema):
    status_code: int
    offer: Optional[bool] = False
    categories: List[CategorySchema] = []
    banners: List[BannerSchema] = []
    widgets: List[WidgetList]
    featured_products: FeaturedProducts


class CartData(AppSchema):
    id: int
    product_title: Optional[str] = ''
    product_description: Optional[str] = ''
    image: str = ''
    quantity: int
    unit: Optional[str] = ''
    price: str
    total: str
    stock: Optional[bool] = False


class SellersList(AppSchema):
    id: int
    name: str
    city: str
    state: str
    title: str = ''
    subtitle: str = ''
    discount: float
    amount: Optional[float] = 0
    valid_upto: Optional[str] = ''


class V4HomeSchema(AppSchema):
    status_code: int
    offer: Optional[bool] = False
    buyer_free_delivery: Optional[str] = ''
    categories: List[CategorySchema] = []
    banners: List[BannerSchema] = []
    widgets: List[WidgetList]
    cart_data: CartData
    sellers: List[SellersList] = []
