from app.core import config
from urllib.parse import quote
from sqlalchemy.engine import create_engine
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import declarative_base, sessionmaker

import pymysql
pymysql.install_as_MySQLdb()


PWD = quote(config.MYSQL_PASSWORD)
USR = config.MYSQL_USER
HOST = config.MYSQL_HOST
PORT = 3360
DATABASE = config.MYSQL_DB

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}/{}'.format(
    USR, PWD, HOST, DATABASE)


# SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://aseztak:Asez@1801#@172.105.62.141/aseztak_fastapi'


engine = create_engine(SQLALCHEMY_DATABASE_URI, future=True,
                       echo=False, pool_pre_ping=True, pool_recycle=300)


SessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine, expire_on_commit=False)

# Dependency


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


Base = declarative_base()
