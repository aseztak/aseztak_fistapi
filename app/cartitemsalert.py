from datetime import datetime
from urllib.parse import quote
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy.orm.session import Session
# from sqlalchemy.ext.declarative import as_declarative
# from sqlalchemy import Column, Integer, String, TIMESTAMP, ForeignKey, func, Float
import requests
# import json
import pymysql
# from sqlalchemy.sql.elements import Null
import asyncio

pymysql.install_as_MySQLdb()

PWD = quote("Asez@0107#")
USR = "asez"
HOST = "172.105.62.141"
PORT = 3030
DATABASE = "aseztak"
# PWD = quote("Asez@321")
# USR = "root"
# HOST = "192.168.0.111"
# PORT = 3030
# DATABASE = "aseztak_fastapi5"

# UPDATE ORDER STATUS EVERY 30 MINUTES

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}/{}'.format(
    USR, PWD, HOST, DATABASE)

engine = create_engine(SQLALCHEMY_DATABASE_URI, future=True,
                       echo=True, pool_pre_ping=True, pool_recycle=300)


SessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine, expire_on_commit=False)


Base = declarative_base()



WP_API_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0ZGRjNWJkMTU4ZTMxMWIyOWJlZDA0MiIsIm5hbWUiOiJBc2V6dGFrIEJyb2FkY2FzdGluZyIsImFwcE5hbWUiOiJBaVNlbnN5IiwiY2xpZW50SWQiOiI2NGNhNTJlNWEzOTc5MTUyYzkxNDYxMjkiLCJhY3RpdmVQbGFuIjoiQkFTSUNfTU9OVEhMWSIsImlhdCI6MTY5MjI2MzczN30.PG_Lit0zm35qiy4rmfzjir-qb8YgUSiDuwf0BFI9gVI"


class WhatsappMsz:
    async def sendcartalertpmsz(name: str, mobile: str):
        try:
            payload = {
                "apiKey": WP_API_KEY,
                "campaignName": "CARTREMINDER",
                "destination": str('+91')+str(mobile),
                "userName": str(name),
                "source": "",
                "media": {
                    "url": "",
                    "filename": ""
                },
                "templateParams": [
                    str(name)
                ],
                "tags": [

                ],
                "attributes": {
                    "attribute_name": ""
                }
            }
            headers = {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }

            response = requests.request(
                "POST", 'https://backend.aisensy.com/campaign/t1/api', json=payload, headers=headers)
            return response.text
        except Exception as e:
            print(e)

# Send cart alert message
def sendcartalertmsz():
    db: Session = SessionLocal()
    try:
        todaydate = datetime.now()
        today = todaydate.strftime("%Y-%m-%d")
        todaytime = todaydate.strftime("%H:%M")
        checkcartitems = db.execute("SELECT * FROM cart_items WHERE date_format(cart_items.created_at, '%Y-%m-%d') =:today AND cart_items.status = 1 GROUP BY cart_items.user_id ORDER BY cart_items.id DESC",{
            "today": today
        }).all()
        if(len(checkcartitems) > 0):
            for cart in checkcartitems:
                user = db.execute("SELECT * FROM users WHERE id=:user_id",{
                    "user_id": cart.user_id
                }).first()
                # SEND WHATSAPP MSZ
                asyncio.run(WhatsappMsz.sendcartalertpmsz(
                    mobile=user.mobile, name=user.name))
        return "SEND SUCCESSFULLY"
    except Exception as e:
        print(e)
    finally:
        db.close()

if __name__ == '__main__':
    sendcartalertmsz()
    print("UPDATED")
